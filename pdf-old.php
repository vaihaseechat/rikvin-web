<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>InCorp - On Boarding Customer Due Diligence Form</title>
</head>
	
<body>
	<div class="container">
		<div class="row justify-content-center">
			<div class="col-lg-8 pdf-table">
				<div class="row">
					<img src="images/logo.png" class="mb-3 mt-5" alt="" />
				</div>
				<div class="row">
					<h3 class="mb-5 mt-5">Customer Due Diligence Acceptance Form</h3>
				</div>
				<div class="row">
					<p>Key definitions used in the document</p>
					<ul>
						<li>A "Beneficial Owner", means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or exercises control over the management.</li>
						<li>A "Politcally Exposed Person" (PEP) is a term describing someone who has been entrusted with any prominent public function in Singapore.</li>
					</ul>
				</div>
				<div class="row mt-5">
					<h4 class="mb-3 font-weight-bold">Proposed Company Information</h4>
					<table class="table table-hover" style="border-top:2px solid #000">
						<tbody>
							<tr>
								<td width="270"><span>Proposed company name</span></td>
								<td><span class="font-weight-bold">1 Tech Solutions Pte Ltd<br/>One Tech Solutions Pte Ltd<br/>Wan Tek Solutions Pte Ltd</span></td>
							</tr>
							<tr>
								<td><span>Registered address of company</span></td>
								<td><span class="font-weight-bold">60 Albert Street,<br/>
								#08-07 OG Albert Complex, <br/>
								Singapore 189969</span></td>
							</tr>
							<tr>
								<td><span>Business activities</span></td>
								<td><span class="font-weight-bold">General Wholesale Trade (Including General Importers and Exporters)</span></td>
							</tr>
							<tr>
								<td><span>Countries of operation</span></td>
								<td><span class="font-weight-bold">Singapore, UAE, Cambodia, Vietnam, Hong kong, Malaysia</span></td>
							</tr>
							<tr>
								<td><span>Source of Funds</span></td>
								<td><span class="font-weight-bold">Bank loans/own cash</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="row">
					<h4 class="mb-3 mt-5 font-weight-bold">Particulars of all Directors, Shareholders and Beneficial Owners</h4>
				</div>
				<div class="row">
					<div class="col-sm-12 pdf-title">
						<h4 class="font-weight-bold my-3">Director #1 - Wee Seok Hua Jacqueline</h4>
					</div>
					<table class="table col-lg-6 col-sm-12">
						<tbody>
							<tr>
								<td width="30%"><span>Full name</span></td>
								<td width="70%"><span class="font-weight-bold">Wee Seok Hua Jacqueline</span></td>
							</tr>
							<tr>								
								<td><span>Gender</span></td>
								<td><span class="font-weight-bold">Female</span></td>
							</tr>
							<tr>								
								<td><span>Nationality</span></td>
								<td><span class="font-weight-bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td><span>NRIC</span></td>
								<td><span class="font-weight-bold">S778888H</span></td>
							</tr>
							<tr>								
								<td><span>Birthdate</span></td>
								<td><span class="font-weight-bold">03/05/1977</span></td>
							</tr>
							<tr>
								<td><span>Birthplace</span></td>
								<td><span class="font-weight-bold">Singapore</span></td>
							</tr>
							<tr>
								<td><span>Address</span></td>
								<td><span class="font-weight-bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
							</tr>
							<tr>
								<td><span>Contact No.</span></td>
								<td><span class="font-weight-bold">97968454</span></td>
							</tr>
							<tr>
								<td><span>Email </span></td>
								<td><span class="font-weight-bold">jacqueline@one.com</span></td>
							</tr>
							<tr>
								<td><span>PEP</span></td>
								<td><span class="font-weight-bold">No</span></td>
							</tr>
						</tbody>
					</table>
					<div class="col-lg-6 col-sm-12">
						<div class="uploaded-image"><img src="images/passport-nric-front.jpg" class="img-fluid" alt="" /></div>
						<div class="uploaded-image"><img src="images/passport-nric-back.jpg" class="img-fluid" alt="" /></div>
					</div>					
					<p class="font-italic small">(Sample with uploaded documents NRIC front and back, nationality Singaporean)</p>
				</div>
				<div class="row">
					<div class="col-sm-12 pdf-title">
						<h4 class="font-weight-bold my-3">Director #2</h4>
					</div>
					<table class="table">
						<tbody>
							<tr>
								<td width="15%"><span>Full name</span></td>
								<td width="35%"><span class="font-weight-bold">Wee Seok Hua Jacqueline </span></td>
								<td width="15%"><span>Gender</span></td>
								<td width="35%"><span class="font-weight-bold">Female</span></td>
							</tr>
							<tr>
								<td><span>NRIC</span></td>
								<td><span class="font-weight-bold">S778888H</span></td>
								<td><span>Nationality</span></td>
								<td><span class="font-weight-bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td><span>Birthdate</span></td>
								<td><span class="font-weight-bold">03/05/1977</span></td>
								<td><span>Birthplace</span></td>
								<td><span class="font-weight-bold">Singapore</span></td>
							</tr>
							<tr>
								<td><span>Address</span></td>
								<td><span class="font-weight-bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
								<td><span>Contact No.</span></td>
								<td><span class="font-weight-bold">97968454</span></td>
							</tr>
							<tr>
								<td><span>Email </span></td>
								<td><span class="font-weight-bold">jacqueline@one.com</span></td>
								<td><span>PEP</span></td>
								<td><span class="font-weight-bold">No</span></td>
							</tr>
						</tbody>
					</table>
					<p class="font-italic small">(Sample without documents - entered manually, Singaporean)</p>
				</div>
				<div class="row">
					<div class="col-sm-12 pdf-title">
						<h4 class="font-weight-bold my-3">Individual Shareholder #1 </h4>
				  	</div>
				  	<table class="table col-lg-6 col-sm-12">
					  <tbody>
						<tr>
							<td width="30%"><span>Full name</span></td>
							<td width="70%"><span class="font-weight-bold">Whakaaturanga Fred John</span></td>
						</tr>
						<tr>								
							<td><span>Gender</span></td>
							<td><span class="font-weight-bold">Male</span></td>
						</tr>
						<tr>								
							<td><span>Nationality</span></td>
							<td><span class="font-weight-bold">New Zealand</span></td>
						</tr>
						<tr>
						  <td><span>Passport</span></td>
						  <td><span class="font-weight-bold">L00001148</span></td>								
						</tr>
						  <tr>
						    <td><span>Country of issue</span></td>
						    <td><span class="font-weight-bold">New Zealand</span></td>								
						</tr>
						<tr>
						  <td><span>Issue date</span></td>
						  <td><span class="font-weight-bold">05/10/2009</span></td>								
						</tr>
						<tr>
						  <td><span>Expiry date</span></td>
						  <td><span class="font-weight-bold">05/10/2014</span></td>								
						</tr>
						<tr>
							<td><span>Birthdate</span></td>
							<td><span class="font-weight-bold">25/02/1964</span></td>
						</tr>
						<tr>
							<td><span>Birthplace</span></td>
							<td><span class="font-weight-bold">Taihape</span></td>
						</tr>
						<tr>
							<td colspan="2"><span class="font-weight-bold">FIN Details</span></td>
						</tr>
						<tr>								
							<td><span>Employer</span></td>
							<td><span class="font-weight-bold">Diamond Pte Ltd</span></td>
						</tr>
						<tr>
						  <td><span>Occupation</span></td>
						  <td><span class="font-weight-bold">Managing Director</span></td>								
						</tr>
						<tr>								
							<td><span>FIN </span></td>
							<td><span class="font-weight-bold">G12345678</span></td>
						</tr>
						<tr>								
							<td><span>Type </span></td>
							<td><span class="font-weight-bold">Employment Pass</span></td>
						</tr>
						<tr>
							<td><span>Issue date</span></td>
							<td><span class="font-weight-bold">01/02/2018</span></td>
						</tr>
						<tr>								
							<td><span>Expiry date</span></td>
							<td><span class="font-weight-bold">01/02/2019</span></td>
						</tr>
						<tr>
							<td colspan="2"><span class="font-weight-bold">Contact Details</span></td>
						</tr>
						<tr>
							<td><span>Address</span></td>
							<td><span class="font-weight-bold">Hougang Street 11, <br/>#10-34 The Minton, <br/>Singapore 534079</span></td>
						</tr>
											
						<tr>
							<td><span>Contact No.</span></td>
							<td><span class="font-weight-bold">97968454</span></td>
						</tr>
						<tr>
							<td><span>Email</span></td>
							<td><span class="font-weight-bold">fred@one.com</span></td>
						</tr>
						<tr>
							<td><span>PEP</span></td>
							<td><span class="font-weight-bold">No</span></td>
						</tr>
					</tbody>
				  	</table>
					<table class="table col-lg-6 col-sm-12">
					  <tbody>
						 <tr>
							<td><span>Beneficial Owner</span></td>
							<td><span class="font-weight-bold">No</span></td>
						</tr>
						<tr>
							<td style="border-bottom: 0 !important"><span>Uploaded documents:</span></td>
							<td style="border-bottom: 0 !important"><span></span></td>
						</tr>
						<tr>								
							<td colspan="2" style="border-bottom: 0 !important"><div class="uploaded-image"><img src="images/passport.jpg" class="img-fluid" alt="" /></div>
						<div class="uploaded-image"><img src="images/ep-front.jpg" class="img-fluid" alt="" /></div>
						<div class="uploaded-image"><img src="images/ep-back.jpg" class="img-fluid" alt="" /></div></td>
						</tr>
						 <tr>
							<td style="border-bottom: 0 !important"><span>Proof of address</span></td>
							 <td style="border-bottom: 0 !important"><span class="font-weight-bold"><a href="#">fred-utility-address.pdf</a></span></td>
						</tr>	
						</tbody>
					</table>
					<p class="font-italic small">(Sample with uploaded documents, Foreigner - EP)</p>
				</div>
				<div class="row">
					<div class="col-sm-12 pdf-title">
						<h4 class="font-weight-bold my-3">Individual Shareholder #2</h4>
					</div>
					<table class="table">
						<tbody>
						<tr>
							<td width="15%"><span>Full name</span></td>
							<td width="35%"><span class="font-weight-bold">Whakaaturanga Fred John</span></td>
							<td width="20%"><span>Beneficial Owner</span></td>
							<td width="30%"><span class="font-weight-bold">No</span></td>
						</tr>
						<tr>								
							<td><span>Gender</span></td>
							<td><span class="font-weight-bold">Male</span></td>
							<td><span>Nationality</span></td>
							<td><span class="font-weight-bold">New Zealand</span></td>
						</tr>
						<tr>								
							<td><span>Passport</span></td>
							<td><span class="font-weight-bold">L00001148</span></td>
							<td><span>Country of Issue</span></td>
							<td><span class="font-weight-bold">New Zealand</span></td>
						</tr>
						<tr>								
							<td><span>Issue date</span></td>
							<td><span class="font-weight-bold">05/10/2009</span></td>
							<td><span>Expiry date</span></td>
							<td><span class="font-weight-bold">05/10/2014</span></td>
						</tr>
						<tr>
							<td><span>Birthdate</span></td>
							<td><span class="font-weight-bold">25/02/1964</span></td>
							<td><span>Birthplace</span></td>
							<td><span class="font-weight-bold">Taihape</span></td>
						</tr>
						<tr>								
							<td colspan="4"><span class="font-weight-bold">FIN Details</span></td>
						</tr>
						<tr>								
							<td><span>Employer</span></td>
							<td><span class="font-weight-bold">Diamond Pte Ltd</span></td>
							<td><span>Occupation</span></td>
							<td><span class="font-weight-bold">Managing Director</span></td>
						</tr>
						<tr>								
							<td><span>FIN </span></td>
							<td><span class="font-weight-bold">G12345678</span></td>
							<td><span>Type</span></td>
							<td><span class="font-weight-bold">Employment Pass</span></td>
						</tr>
						<tr>
							<td><span>Issue date</span></td>
							<td><span class="font-weight-bold">11/12/2015</span></td>
							<td><span>Expiry date</span></td>
							<td><span class="font-weight-bold">01/12/2018</span></td>
						</tr>
						<tr>								
							<td colspan="4"><span class="font-weight-bold">Contact Details</span></td>
						</tr>
						<tr>
							<td><span>Address</span></td>
							<td><span class="font-weight-bold">Hougang Street 11,<br/>#10-34 The Minton,<br/>Singapore 534079</span></td>
							<td><span>Proof of address</span></td>
							<td><span class="font-weight-bold"><a href="#"></a></span></td>
						</tr>
						<tr>
							<td><span>Email</span></td>
							<td><span class="font-weight-bold">fred@one.com</span></td>
							<td><span>Contact No.</span></td>
							<td><span class="font-weight-bold">00121345641</span></td>
						</tr>
						
					</tbody>
					</table>
					<p class="font-italic small">(Sample without uploaded documents, information entered manually, Foreigner - EP)</p>
				</div>
				<div class="row">
				<div class="col-sm-12 pdf-title">
						<h4 class="font-weight-bold my-3">Individual Shareholder #3 </h4>
				  </div>
				  <table class="table col-lg-6">
					  <tbody>
						<tr>
							<td><span>Full name</span></td>
							<td><span class="font-weight-bold">Mark David Aaron Base</span></td>
						</tr>
						<tr>								
							<td><span>Gender</span></td>
							<td><span class="font-weight-bold">Male</span></td>
						</tr>
						<tr>								
							<td><span>Nationality</span></td>
							<td><span class="font-weight-bold">British Citizen</span></td>
						</tr>
						<tr>
						  <td><span>Passport No.</span></td>
						  <td><span class="font-weight-bold">704902166</span></td>								
						</tr>
						  <tr>
						    <td><span>Country of issue</span></td>
						    <td><span class="font-weight-bold">UK</span></td>								
						</tr>
						<tr>
						  <td><span>Issue date</span></td>
						  <td><span class="font-weight-bold">05/10/2009</span></td>								
						</tr>
						<tr>
						  <td><span>Expiry date</span></td>
						  <td><span class="font-weight-bold">04/03/2004</span></td>								
						</tr>
						<tr>
							<td><span>Birthdate</span></td>
							<td><span class="font-weight-bold">08/03/1965</span></td>
						  </tr>
						  <tr>
							<td><span>Birthplace</span></td>
							<td><span class="font-weight-bold">Montreal</span></td>
						</tr>
						  <tr>								
							<td colspan="4"><span class="font-weight-bold">Contact Details</span></td>
						</tr>
						<tr>
							<td><span>Residential address</span></td>
							<td><span class="font-weight-bold">Hougang Street 11,<br/>#10-34 The Minton,<br/>Singapore 534079</span></td>
						  </tr>
						 
						<tr>
							<td><span>Email</span></td>
							<td><span class="font-weight-bold">base@one.com</span></td>
							</tr>
						  <tr>
							<td><span>Contact No.</span></td>
							<td><span class="font-weight-bold">00121345641</span></td>
						</tr>
					</tbody>
				  </table>
				<table class="table col-lg-6 col-sm-12">
					  <tbody>
						  <tr>
							<td><span>Beneficial Owner</span></td>
							<td><span class="font-weight-bold">No</span></td>
						</tr>
						<tr>
							<td style="border-bottom: 0 !important"><span>Uploaded documents:</span></td>
							<td style="border-bottom: 0 !important"><span></span></td>
						</tr>
						<tr>								
							<td colspan="2"><div class="uploaded-image"><img src="images/passport-4.jpg" class="img-fluid" alt="" /></div>
						</td>
						</tr>
						
						</tbody>
					</table>
					<table class="table col-lg-12">
					<tr>								
							<td colspan="4"><span class="font-weight-bold">Politically Exposed Person</span></td>
						</tr>
						<tr>
							<td width="24%"><span>PEP</span></td>
							<td><span class="font-weight-bold">Yes</span></td>
						  
							<td><span>PEP in which country</span></td>
							<td><span class="font-weight-bold">UK</span></td>
						</tr>
						<tr>
							<td><span>Role of PEP</span></td>
							<td><span class="font-weight-bold">Political Party Leadership</span></td>
						 
							<td><span>PEP since</span></td>
							<td><span class="font-weight-bold">2000 - 2005</span></td>
						</tr>
					</table>
					<p class="font-italic small">(Sample with uploaded passport but without proof of address, Foreigner)</p>
				</div>	
				
				<div class="row">
					<div class="col-sm-12 pdf-title">
						<h4 class="font-weight-bold my-3">Corporate Shareholder #1</h4>
					</div>
					<table class="table">
						<tbody>
							<tr>
								<td width="30%"><span>Name</span></td>
								<td><span class="font-weight-bold">One Tech Solutions Sdn Bhd</span></td>
								
							</tr>
							<tr>
								<td><span>Country of Incorporation</span></td>
								<td><span class="font-weight-bold">Malaysia</span></td>
							</tr>
							<tr>
								<td><span>Documents Uploaded</span></td>
								<td><span class="font-weight-bold"><a href="#">certificate-of-incorporation.pdf</a><br/><a href="#">extract-of-company.pdf</a></span></td>
							</tr>
						</tbody>
					</table>					
				</div>
				<div class="row">
					<table class="table table-bordered">
						<tbody>
							<tr class="table-active">
								<td colspan="4"><span class="font-weight-bold"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Corporate Representative</span></td>
							</tr>
							<tr>
								<td width="15%"><span>Full name</span></td>
								<td width="35%"><span class="font-weight-bold">Cheryl Lee</span></td>
								<td width="15%"><span>Gender</span></td>
								<td width="35%"><span class="font-weight-bold">Female</span></td>
							</tr>
							<tr>
								<td><span>NRIC</span></td>
								<td><span class="font-weight-bold">S010101</span></td>
								<td><span>Nationality</span></td>
								<td><span class="font-weight-bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td><span>Birthdate</span></td>
								<td><span class="font-weight-bold">03/05/1977</span></td>
								<td><span>Birthplace</span></td>
								<td><span class="font-weight-bold">Singapore</span></td>
							</tr>
							<tr>
								<td><span>Address</span></td>
								<td><span class="font-weight-bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
								<td><span>Contact No.</span></td>
								<td><span class="font-weight-bold">97968454</span></td>
							</tr>
							<tr>
								<td><span>Email </span></td>
								<td><span class="font-weight-bold">cheryl@one.com</span></td>
								<td><span></span></td>
								<td><span class="font-weight-bold"></span></td>
							</tr>
						</tbody>
					</table>
					
				</div>
				<div class="row my-5">
					<hr style="background: #666; height: 2px; width: 100%">
				</div>
				<div class="row my-3">
					<h3>Shareholding Structure</h3>
				</div>
				<div class="row">					
					<table class="table table-bordered">
						<tbody>
							<tr>
								<td><span>Currency</span></td>
								<td><span class="font-weight-bold">Singapore Dollar</span></td>
								<td><span>Paid Up capital</span></td>
								<td><span class="font-weight-bold">100,000</span></td>
								<td><span>No. of shares</span></td>
								<td><span class="font-weight-bold">100</span></td>
							</tr>							
						</tbody>
					</table>
				</div>
				<div class="row my-3">
					<h3>List of Shareholders</h3>
				</div>
				<div class="row">					
					<table class="table table-bordered2">
						<tbody style="border: 1px solid #e9ecef !important;">
							<tr class="table-secondary">
								<td width="50%"><span>Shareholders</span></td>
								<td width="17%"><span>Paid up capital</span></td>
								<td width="16%"><span>No. of shares</span></td>
								<td width="16%"><span>Beneficial owner</span></td>
							</tr>
							<tr>
								<td><span class="font-weight-bold">Whakaaturanga Fred Wiremu John</span></td>
								<td><span class="font-weight-bold">50,000</span></td>
								<td><span class="font-weight-bold">100</span></td>
								<td><span class="font-weight-bold">No</span></td>
							</tr>
							<tr>
								<td><span class="font-weight-bold">Mark David Aaron Base</span></td>
								<td><span class="font-weight-bold">50,000</span></td>
								<td><span class="font-weight-bold">100</span></td>
								<td><span class="font-weight-bold">Yes</span></td>
							</tr>
							<tr>
								<td><span  class="font-weight-bold">One Tech Solutions Sdn Bhd</span></td>
								<td><span class="font-weight-bold">50,000</span></td>
								<td><span class="font-weight-bold">100</span></td>
								<td><span>&nbsp;</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				
				<div class="row">
					<table class="table table-bordered">
						<tbody>
							<tr class="table-active">
								<td colspan="4"><span class="font-weight-bold"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Whakaaturanga Fred Wiremu John Ultimate Beneficial Owner</span></td>
							</tr>
							<tr>
								<td width="25%"><span>Full name</span></td>
								<td width="25%"><span class="font-weight-bold">Satish Bakhda </span></td>
								<td width="25%"><span>Gender</span></td>
								<td width="25%"><span class="font-weight-bold">Male</span></td>
							</tr>
							<tr>
								<td><span>NRIC</span></td>
								<td><span class="font-weight-bold">S123245678</span></td>
								<td><span>Nationality</span></td>
								<td><span class="font-weight-bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td><span>Birthdate</span></td>
								<td><span class="font-weight-bold">03/05/1977</span></td>
								<td><span>Birthplace</span></td>
								<td><span class="font-weight-bold">Singapore</span></td>
							</tr>
							<tr>
								<td><span>Address</span></td>
								<td><span class="font-weight-bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
								<td><span>Contact No.</span></td>
								<td><span class="font-weight-bold">97968454</span></td>
							</tr>
							<tr>
								<td><span>Email </span></td>
								<td><span class="font-weight-bold">satish@one.com</span></td>
								<td><span>PEP</span></td>
								<td><span class="font-weight-bold">No</span></td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="row my-5">
					<h3 class="mb-5" style="border-bottom:2px solid #000; display: block; width: 100%; padding-bottom: 10px;">Declaration</h3>
					<p>I declare that the information provided in this form is true and correct. I am aware that I may be subject to prosecution if I am found to have made any false statement which I know to be false or intentionally suppressed any material fact.</p>
				</div>
				<div class="row">					
					<table width="100%">
						<tbody>
							<tr>
								<td width="270"><span>Name of Customer/Agent</span></td>
								<td><span class="font-weight-bold">Cheryl Lee</span></td>
							</tr>					
						</tbody>
					</table>
				</div>
				<br/><br/><br/><br/><br/><br/><br/><br/>
			</div>
		</div>
	</div>
	

</body>
</html>
