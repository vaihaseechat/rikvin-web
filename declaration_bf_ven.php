<?php 
//echo '<pre>';
//print_r($_FILES);
include('includes/uploaddirectordocuments.php');
include('includes/uploadshareholderdocuments.php');
include('includes/uploaddisdocuments.php');
include('includes/uploadcorporatedocuments.php');
include('includes/constants.php');
?>
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>InCorp - On Boarding Customer Due Diligence Form</title>
	
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Roboto" rel="stylesheet">	
	<link rel="stylesheet" href="css/style.css?v3">	
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/chosen.css?v1">
	<link rel="stylesheet" href="css/prism.css">	
	<link rel="stylesheet" href="css/font-awesome.min.css">
</head>
	
<body>
	<nav id="mainNav">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-10">
					<a href="#page-top"><img src="images/logo.png" alt="" /></a>			
				</div>
			</div>
		</div>
	</nav>
	
	<header class="masthead text-white bg-header">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-10">
        			<h1 class="text-center">On Boarding Customer Due Diligence Form</h1>
				</div>
			</div>
		</div>
	</header>
	
	<header class="bg-intro border-bottom">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10 col-sm-12">
					<div class="col-lg-8 col-sm-12 float-left">
						<p>If you have a complex business structure we request you to download offline version of this form.</p>
					</div>
					<div class="col-lg-4 col-sm-12 float-left">
						<a class="btn btn-block cdd-download" href="#">Download offline version</a>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div class="container">
		<form name="declarationform" id="declarationform" method="post" enctype="multipart/form-data">
			<div class="row justify-content-center">
				<div class="col-sm-10"><h3 class="mb-5 mt-5">Shareholding Structure</h3></div>
			</div>
			<div class="row justify-content-center">
				<div class="col-sm-10">
					<div class="form-group row">
						<div class="col-sm">
							<h4>Please leave blank if currently unsure of Shareholding structure</h4>							
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4">
							<h5 class="mt-40">Total</h5>
						</div>
						<div class="col-sm-8 input-group">
							<div class="form-group row">
								<div class="col-lg-4">
									<label class="col-form-label" for="currency">Currency</label>
									<div class="selectdiv">
									<select name="currency" id="currency_drop_down" class="custom-select form-bg-2 select-w-100" data-placeholder="Please select...">
										<option value="">Please select</option>
										<option value="">Singapore Dollar</option>
										<option value="">Afghani</option>
										<option value="">Algerian Dinar</option>
										<option value="">Angolan Escudo</option>
										<option value="">Australian Dollar</option>
										<option value="">Bahamian Dollar</option>
										<option value="">Baht</option>
										<option value="">Balboa</option>
										<option value="">Belgian Franc</option>
										<option value="">Bermudan Dollar</option>
										<option value="">Bolivar</option>
										<option value="">Brunei Dollar</option>
										<option value="">Burundi Franc</option>
										<option value="">Canadian Dollar</option>
										<option value="">Cfa Franc</option>
										<option value="">Cfa-communaute Finan</option>
										<option value="">Colones</option>
										<option value="">Cordoba</option>
										<option value="">Cpa Franc</option>
										<option value="">Cyprus Pound</option>
										<option value="">Dalasi</option>
										<option value="">Denar</option>
										<option value="">Deutsche Mark</option>
										<option value="">Dinar</option>
										<option value="">Dirham</option>
										<option value="">Dollar</option>
										<option value="">Dong</option>
										<option value="">Drachma</option>
										<option value="">East Caribbean Dolla</option>
										<option value="">Egyptian Pound</option>
										<option value="">Escudo</option>
										<option value="">Ethiopian Dollar</option>
										<option value="">Euros</option>
										<option value="">Falkland Island Poun</option>
										<option value="">Fiji Dollar</option>
										<option value="">Forint</option>
										<option value="">Franc</option>
										<option value="">French Franc</option>
										<option value="">Gibraltar Pound</option>
										<option value="">Gourde</option>
										<option value="">Guarani</option>
										<option value="">Guilder</option>
										<option value="">Guyana Dollar</option>
										<option value="">Hong Kong Dollar</option>
										<option value="">Indian Rupee</option>
										<option value="">Iraqi Dinar</option>
										<option value="">Irish Pound</option>
										<option value="">Jamaican Dollar</option>
										<option value="">Jordanian Dollar</option>
										<option value="">Kenyan Shilling</option>
										<option value="">Kina</option>
										<option value="">Kip</option>
										<option value="">Koruna</option>
										<option value="">Krona</option>
										<option value="">Krone</option>
										<option value="">Kuwaiti Dinar</option>
										<option value="">Kwacha</option>
										<option value="">Kyat</option>
										<option value="">Lebanese Pound</option>
										<option value="">Lek</option>
										<option value="">Lempira</option>
										<option value="">Leone</option>
										<option value="">Leu</option>
										<option value="">Lev</option>
										<option value="">Liberian Dollar</option>
										<option value="">Libyan Dinar</option>
										<option value="">Lira</option>
										<option value="">Luxemburg Franc</option>
										<option value="">Malagasy Franc</option>
										<option value="">Malawi Kwacha</option>
										<option value="">Malaysian Dollar</option>
										<option value="">Maldivian Rupee</option>
										<option value="">Mali Franc</option>
										<option value="">Maltese Pound</option>
										<option value="">Markka</option>
										<option value="">Multi Currency</option>
										<option value="">Naira</option>
										<option value="">New Cedi</option>
										<option value="">New Cruzeiro</option>
										<option value="">New Peso</option>
										<option value="">New Taiwan Dollar</option>
										<option value="">New Zealand Dollar</option>
										<option value="">Pa'anga</option>
										<option value="">Peseta</option>
										<option value="">Peseta Guinean</option>
										<option value="">Peso</option>
										<option value="">Peso Bolivano</option>
										<option value="">Philippine Peso</option>
										<option value="">Pound</option>
										<option value="">Pound Sterling</option>
										<option value="">Qatar Riyal</option>
										<option value="">Reminbi Yuan</option>
										<option value="">Rial</option>
										<option value="">Rial Omani</option>
										<option value="">Riyal</option>
										<option value="">Rouble</option>
										<option value="">Rupee</option>
										<option value="">Rupiah</option>
										<option value="">Rwandese Franc</option>
										<option value="">S.african Rand</option>
										<option value="">Seychelles Rupee</option>
										<option value="">Shilling</option>
										<option value="">Sol</option>
										<option value="">Somali Shilling</option>
										<option value="">South African Rand</option>
										<option value="">South Arabian Dinah</option>
										<option value="">Spanish Peseta</option>
										<option value="">Sri Lankan Rupee</option>
										<option value="">Sucre</option>
										<option value="">Sudanese Pound</option>
										<option value="">Suli</option>
										<option value="">Swiss Franc</option>
										<option value="">Swiss Frank</option>
										<option value="">Syrian Pound</option>
										<option value="">Taka</option>
										<option value="">Tala</option>
										<option value="">Tanzanian Shilling</option>
										<option value="">Trinidad And Tobago</option>
										<option value="">Tugrik</option>
										<option value="">Tunisian Dinar</option>
										<option value="">U.s. Dollar</option>
										<option value="">Ugandan Shilling</option>
										<option value="">Won</option>
										<option value="">Yemei Riyal</option>
										<option value="">Yen</option>
										<option value="">Zaire</option>
										<option value="">Zloty</option>
									</select>
									</div>
								</div>
								<div class="col-lg-4">
									<label class="col-form-label" for="paidUpCapital">Paid Up Capital</label>
									<input type="text" name="paidUpCapital" class="form-control form-bg-2" id="paidUpCapital" placeholder="" />
								</div>
								<div class="col-lg-4">
									<label class="col-form-label" for="numberofShares">No. of Shares</label>
									<input type="text" name="numberofShares" class="form-control form-bg-2" id="numberofShares" placeholder="" />
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-sm border-bottom-2">&nbsp;</div>
					</div>
					<div class="form-group row mt-5">
						<div class="col-sm-12">
							<div class="form-group row">
								<div class="col-lg-4 col-sm">
									<label class="col-form-label font-weight-bold" for="">Shareholders</label>
								</div>
								<div class="col-lg-2 col-sm">
									<label class="col-form-label" for="">Paid Up Capital</label>
								</div>
								<div class="col-lg-2 col-sm">
									<label class="col-form-label" for="">No. of Shares</label>
								</div>
								<div class="col-lg-4 col-sm">
									<label class="col-form-label" for="" data-toggle="tooltip" data-placement="top" title="A Beneficial Owner means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or assets and undertakings of the customer or otherwise exercises control over the management of the customer.">Beneficial Owner <i class="fa fa-question-circle"></i></label>
								</div>
							</div>
						</div>
					</div>
					<div class="loop_block" id="share_holder_list">
				
					</div>
					
					<style>
					.btn.declare-bo {
    background: #c66668;
    color: #fff;
    font-size: 12px;
    padding: 0 20px 0 45px;
    text-align: center;
    height: 40px;
    line-height: 40px;
}
.btn.declare-bo:before {
    content: "\f2bd";
    font: normal normal normal 14px/1 FontAwesome;
    left: 33px;
    position: absolute;
    top: 13px;
    font-size: 16px;
}
					
					</style>
				
					
					
					<div class="form-group row my-5">
						<div class="col-sm-12">
							<h3 class="text-uppercase mb-3">Declaration</h3>
							<p>I declare that the information provided in this form is true and correct. I am aware that I may be subject to prosecution if I am found to have made any false statement which I know to be false or intentionally suppressed any material fact.</p>
						</div>					
					</div>

					<div class="form-group row">
						<div class="col-sm-4">
							<label class="col-form-label required" for="customerName">Name of Customer/Agent</label>
						</div>
						<div class="col-sm-8">
							<div class="selectdiv">
								<select name="customerName" id="customer_dropdown" onchange="cusSelectCheck(this)" class="custom-select form-bg-2  select-w-100" data-placeholder="Please select...">
									<option value="">Please select</option>
									<!--<option value="Individual Shareholder 1">Individual Shareholder 1</option>
									<option value="Individual Shareholder 2">Individual Shareholder 2</option>
									<option value="customerNameOthers">Others / Agent</option>-->
								</select>
								<div class="customerName-alert" id="customerName-alert" style="color:#B90D0F;"> </div>
							</div>
								
						</div>
						<div class="col-sm-12" id="others_form">
							</div>
						
					</div>

 <!--Others Module --->

<div class="col-lg-12 background-grey-2" style="padding:15px;margin-top:15px;display:none;" id="otheragentsmodule">
<div class="form-group row mt-4">
  <div class="col-lg-4"> <label class="col-form-label required" for="">Full Name</label> </div>
<div class="col-lg-8">
	 <input type="text" name="" class="form-control" id="full_name_2" placeholder=""> 
     <div class="fullName-alert" id="fullName-alert-2" style="color:#B90D0F;"> </div>
  </div>
</div>
<div class="form-group row">
  <div class="col-lg-4"> <label class="col-form-label required" for="residencyStatus" id="current_residency_status">Current Residency Status</label> </div>
  <div class="col-lg-8">
	 <div class="selectdiv">
		<select name="residencyStatus" id="residencyStatus" class="custom-select select-w-100" data-placeholder="Please select..." onchange="getotherresidencystatus(this.value)">
		   <option value="">Please select</option>
		   <option value="othernonresident" rel="nonresident">Non-resident</option>
		   <option value="othercitizenpr" rel="citizenpr">Singapore Citizen / PR</option>
		   <option value="otherpassholder" rel="passholder">FIN Card (Work Passes)</option>                                                    
		</select>
		<div id="residencyStatus-alert" style="color:#B90D0F;"> </div>
	 </div>
  </div>
</div>
<?php include('includes/otherresidencystatus.php');?>
<div class="form-group row">
  <label class="col-lg-4 col-form-label required" for="contactNumber">Contact Details</label> 
  <div class="col-lg-8">
	 <div class="row">
		<div class="col-sm">
		   <input type="text" name="email" class="form-control mb-2" id="email_2" placeholder="Email"> 
		   <div class="bizEmail1-alert" id="bizEmail1-alert-2" style="color:#B90D0F;"> </div>
		</div>
		<div class="col-sm" id="contact_div">
		   <input type="text" name="contactnumber" class="form-control mb-2" id="contactnumber_2" placeholder="Contact number"> 
		   <div class="col-sm" id="contact-alert-2" style="color:#B90D0F;"></div>
		</div>
	 </div>
  </div>
</div>
</div>

										
					<!-- Buttons -->
					<div class="form-group py-5">
						<div class="col-sm-12">
							<div class="row justify-content-end">
								<input class="btn btn-next btn-lg col-sm-12 col-lg-3 mb-3 mt-5" onclick="formSubmit()" type="button" value="Submit">
							</div>
						</div>
					</div>
					<!-- end buttons -->
					
				</div>
			</div>
		</form>
	</div><!-- /.container -->

	<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> 	
	<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
	<script src='<?php echo BASE_URL;?>/js/datepicker.js'></script>
	<!-- Bootstrap core JavaScript -->	

	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/chosen.jquery.js"></script>
	<script src="js/prism.js"></script>

        <?php include('includes/checkvalidation.php');?>
        <?php include('includes/modalbeneficialowner.php');?>
	<?php include('includes/beneficialownerscript.php');?>
        <?php include('includes/bopassportpreview.php');?>
        <?php include('includes/bopreviewpassportmanually.php');?>
        <?php include('includes/bopreviewnricmanually.php');?> 
        <?php include('includes/bopreviewfinmanually.php');?>
        <?php include('includes/bonricfrontpreview.php');?>
        <?php include('includes/bonricbackpreview.php');?>
        <?php include('includes/bofinpassportpreview.php');?>
        <?php include('includes/bofinfrontpreview.php');?>
        <?php include('includes/bofinbackpreview.php');?>       

        <?php include('includes/otherpassportpreview.php');?>
	<?php include('includes/otherpreviewpassportmanually.php');?>
        <?php include('includes/otherpreviewnricmanually.php');?> 
        <?php include('includes/otherpreviewfinmanually.php');?>
        <?php include('includes/othernricfrontpreview.php');?>
	<?php include('includes/othernricbackpreview.php');?>
        <?php include('includes/otherfinpassportpreview.php');?>
        <?php include('includes/otherfinfrontpreview.php');?>
        <?php include('includes/otherfinbackpreview.php');?> 
        <?php include('includes/otherformvalidation.php');?> 
        
	<!-- Footer -->
	<footer class="site-footer pt-4 mt-4">
		<div class="container text-center text-md-left">
			<p class="text-center"><a href="https://www.incorp.asia/" rel="home">In.Corp Global Pte Ltd</a>. <span style="white-space: pre;">All rights reserved.</span><span style="margin: 0 8px;">·</span><a href="/privacy/">Privacy Policy</a><span style="margin: 0 8px;">·</span><a href="/terms/">Terms of Use</a><span style="margin: 0 8px;">·</span><a href="/sitemap/">Sitemap</a></p>
		</div><!-- /.container -->		
	</footer>
	
	
	 
	<script>	
        var countries = [];
	var countriesOption; 
        var yearfromOption;
        var yeartoOption;   
       
        countries = ['', 'Singapore','Malaysia','Indonesia', 'India', 'China','Hong Kong','Philippines','Thailand','Vietnam','United Arab Emirates','Afghanistan', 'Aland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia, Plurinational State of', 'Bonaire, Sint Eustatius and Saba', 'Bosnia and Herzegovina', 'Botswana', 'Bouvet Island', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo', 'Congo, The Democratic Republic of The', 'Cook Islands', 'Costa Rica', 'Cote D\'ivoire', 'Croatia', 'Cuba', 'Curacao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands (Malvinas)', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Territories', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea-bissau', 'Guyana', 'Haiti', 'Heard Island and Mcdonald Islands', 'Holy See (Vatican City State)', 'Honduras',  'Hungary', 'Iceland', 'Iran, Islamic Republic of', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea, Democratic People\'s Republic of', 'Korea, Republic of', 'Kuwait', 'Kyrgyzstan', 'Lao People\'s Democratic Republic', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macao', 'Macedonia, The Former Yugoslav Republic of', 'Madagascar', 'Malawi',  'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia, Federated States of', 'Moldova, Republic of', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestinian Territory, Occupied', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Pitcairn', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Reunion', 'Romania', 'Russian Federation', 'Rwanda', 'Saint Barthelemy', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin (French part)', 'Saint Pierre and Miquelon', 'Saint Vincent and The Grenadines', 'Samoa', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Sint Maarten (Dutch part)', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Georgia and The South Sandwich Islands', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Swaziland', 'Sweden', 'Switzerland', 'Syrian Arab Republic', 'Taiwan, Province of China', 'Tajikistan', 'Tanzania, United Republic of',  'Timor-leste', 'Togo', 'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Kingdom', 'United States', 'United States Minor Outlying Islands', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela, Bolivarian Republic of', 'Virgin Islands, British', 'Virgin Islands, U.S.', 'Wallis and Futuna', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe'];  
	
        countriesOption = countries.map(function(country){
				return '<option value="' + country + '">' + country + '</option>'
			});

        years1 = ['','2018','2017','2016','2015','2014','2013','2012','2011','2010','2009','2008','2007','2006','2005','2004','2003','2002','2001','2000','1999','1998','1997','1996','1995','1994','1993','1992','1991','1990'];
                         
        years = ['','1990','1991','1992','1993','1994','1995','1996','1997','1998','1999','2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018']; 

        yearfromOption = years.map(function(years){
		return '<option value="' + years + '">' + years + '</option>'
	});
        

	yeartoOption = years1.map(function(years1){
		return '<option value="' + years1 + '">' + years1 + '</option>'
	});
        // Passport Preview 

        $('#othercntryissue').html(countriesOption);
        $('#othercntryissue').chosen(); 

        $('#othernationality').html(countriesOption);
        $('#othernationality').chosen(); 

        $('#otherpob').html(countriesOption);
        $('#otherpob').chosen();

        $('#otherdatetimepicker').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#otherdateexpiry').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#otherdob').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        // Passport Preview Manual

	$('#othermcntryissue').html(countriesOption);
        $('#othermcntryissue').chosen(); 

        $('#othermnationality').html(countriesOption);
        $('#othermnationality').chosen(); 

        $('#othermpob').html(countriesOption);
        $('#othermpob').chosen();

        $('#othermdatetimepicker').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#othermdateexpiry').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#othermdob').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        // NRIC Front

        $('#otherpreviewfrontnriccob').html(countriesOption);
        $('#otherpreviewfrontnriccob').chosen();  

        $('#otherpreviewfrontnricdoi').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        // NRIC Back

        $('#otherpreviewbacknriccob').html(countriesOption);
        $('#otherpreviewbacknriccob').chosen();  

        $('#otherpreviewbacknricdoi').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        // NRIC Manually

        $('#othermpreviewnriccob').html(countriesOption);
        $('#othermpreviewnriccob').chosen();  

        $('#othermnricdoi').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        //Fin Passport
        $('#otherfincntryissue').html(countriesOption);
        $('#otherfincntryissue').chosen(); 

        $('#otherfinnationality').html(countriesOption);
        $('#otherfinnationality').chosen(); 

        $('#otherfinpob').html(countriesOption);
        $('#otherfinpob').chosen();

        $('#otherfindatetimepicker').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#otherfindateexpiry').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#otherfindob').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        //FIN Front

        $('#otherfinfrontdoi').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#otherfinfrontdoe').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        //FIN Back

        $('#otherfinbackdoi').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#otherfinbackdoe').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        // FIN Manual 
        $('#othermfinnationality').html(countriesOption);
        $('#othermfinnationality').chosen(); 

        $('#othermfincntryissue').html(countriesOption);
        $('#othermfincntryissue').chosen(); 

        $('#othermfinpob').html(countriesOption);
        $('#othermfinpob').chosen(); 

        $('#othermfindoi').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});
         
        $('#othermfindoe').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#othermfindob').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#othermwfindoi').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	});

        $('#othermwfindoe').datepicker({   
	  beforeShow:function () {
	      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
	  }
	}); 
	
	function formSubmit(){

		var customerName_alert = '<span class="font-italic small" id="residencyStatus-alert-msg">Select a name</span>';
		var customerName = $( "#customer_dropdown option:selected" ).text();
		$("#customer_dropdown").on('change', function(e){
			if (this.value.trim() != "") {
				$("#customerName-alert").html("");
			}
		});
		var customerNm = $("#customer_dropdown").val();
		console.log("customerName: "+customerNm);
		if(customerNm == '' || customerNm == null) {
			$("#customerName-alert").html(customerName_alert);		
			return;		
		}

		
		
		if(customerName != 'Others / Agent'){

			admOptionValue = $( "#customer_dropdown option:selected" ).text();
			var currency = $( "#currency_drop_down option:selected" ).text();
			paidUpCapital = $("#paidUpCapital").val();
			numberofShares = $("#numberofShares").val();

			currency = (currency == "Please select") ? '' : currency;

			localStorage.setItem('agent_full_name', admOptionValue);
			localStorage.setItem('currency', currency);
			localStorage.setItem('paid_up_capital', paidUpCapital);
			localStorage.setItem('number_of_shares', numberofShares);
		
			for (i = 0; i < localStorage.getItem("Director_COUNT"); i++){
				if(document.getElementById("director_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('director_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('director_paid_up_capital_'+(i+1), document.getElementById("director_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("director_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('director_number_of_shares_'+(i+1), 0);
				} else {
					localStorage.setItem('director_number_of_shares_'+(i+1), document.getElementById("director_number_of_shares_"+(i+1)).value);
				}
			}
			for (i = 0; i < localStorage.getItem("Shareholder_COUNT"); i++){
				localStorage.setItem('individual_shareholder_bowner_'+(i+1),localStorage.getItem("shareholderbowner_"+(i+1)));
				if(document.getElementById("individual_shareholder_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('individual_shareholder_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('individual_shareholder_paid_up_capital_'+(i+1), document.getElementById("individual_shareholder_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("individual_shareholder_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('individual_shareholder_number_of_shares_'+(i+1), 0);
				} else {
					localStorage.setItem('individual_shareholder_number_of_shares_'+(i+1), document.getElementById("individual_shareholder_number_of_shares_"+(i+1)).value);
				}			
			}
			for (i = 0; i < localStorage.getItem("Director and Shareholder_COUNT"); i++){
				localStorage.setItem('director_shareholder_bowner_'+(i+1),localStorage.getItem("dishareholderbowner_"+(i+1)));
				if(document.getElementById("director_shareholder_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('director_shareholder_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('director_shareholder_paid_up_capital_'+(i+1), document.getElementById("director_shareholder_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("director_shareholder_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('director_shareholder_number_of_shares_'+(i+1), 0);
				} else {
					localStorage.setItem('director_shareholder_number_of_shares_'+(i+1), document.getElementById("director_shareholder_number_of_shares_"+(i+1)).value);
				}
				
			}
			for (i = 0; i < localStorage.getItem("Corporate Shareholder_COUNT"); i++){
				if(document.getElementById("corporate_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('corporate_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('corporate_paid_up_capital_'+(i+1), document.getElementById("corporate_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("corporate_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('corporate_number_of_shares_'+(i+1), 0);
				}
				else {
					localStorage.setItem('corporate_number_of_shares_'+(i+1), document.getElementById("corporate_number_of_shares_"+(i+1)).value);
				}
			}
			for (var i = 0; i < localStorage.length; i++){
	    		console.log(localStorage.key(i)+" ==> "+localStorage.getItem(localStorage.key(i)));
			}
			document.location.href="<?php echo BASE_URL;?>/thankyou.php";
			return;
		} else if(validateOtherForm() != true){
			return;
		}else {
			var currency = $( "#currency_drop_down option:selected" ).text();
		
			fullName = document.getElementById("full_name_2").value;
			email = document.getElementById("email_2").value;
			contactNo = document.getElementById("contactnumber_2").value;

			localStorage.setItem('currency', currency);
			localStorage.setItem('paid_up_capital', document.getElementById("paidUpCapital").value);
			localStorage.setItem('number_of_shares', document.getElementById("numberofShares").value);
			
			localStorage.setItem('agent_full_name', fullName);
			localStorage.setItem('agent_email', email);
			localStorage.setItem('agent_contact_no', contactNo);
			
			for (i = 0; i < localStorage.getItem("Director_COUNT"); i++){
				if(document.getElementById("director_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('director_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('director_paid_up_capital_'+(i+1), document.getElementById("director_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("director_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('director_number_of_shares_'+(i+1), 0);
				} else {
					localStorage.setItem('director_number_of_shares_'+(i+1), document.getElementById("director_number_of_shares_"+(i+1)).value);
				}
			}
			for (i = 0; i < localStorage.getItem("Shareholder_COUNT"); i++){
				localStorage.setItem('individual_shareholder_bowner_'+(i+1),localStorage.getItem("shareholderbowner_"+(i+1)));
				if(document.getElementById("individual_shareholder_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('individual_shareholder_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('individual_shareholder_paid_up_capital_'+(i+1), document.getElementById("individual_shareholder_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("individual_shareholder_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('individual_shareholder_number_of_shares_'+(i+1), 0);
				} else {
					localStorage.setItem('individual_shareholder_number_of_shares_'+(i+1), document.getElementById("individual_shareholder_number_of_shares_"+(i+1)).value);
				}			
			}
			for (i = 0; i < localStorage.getItem("Director and Shareholder_COUNT"); i++){
				localStorage.setItem('director_shareholder_bowner_'+(i+1),localStorage.getItem("dishareholderbowner_"+(i+1)));
				if(document.getElementById("director_shareholder_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('director_shareholder_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('director_shareholder_paid_up_capital_'+(i+1), document.getElementById("director_shareholder_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("director_shareholder_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('director_shareholder_number_of_shares_'+(i+1), 0);
				} else {
					localStorage.setItem('director_shareholder_number_of_shares_'+(i+1), document.getElementById("director_shareholder_number_of_shares_"+(i+1)).value);
				}
				
			}
			for (i = 0; i < localStorage.getItem("Corporate Shareholder_COUNT"); i++){
				if(document.getElementById("corporate_paid_up_capital_"+(i+1)).value==''){
					localStorage.setItem('corporate_paid_up_capital_'+(i+1), 0);
				} else {
					localStorage.setItem('corporate_paid_up_capital_'+(i+1), document.getElementById("corporate_paid_up_capital_"+(i+1)).value);
				}
				if(document.getElementById("corporate_number_of_shares_"+(i+1)).value==''){
					localStorage.setItem('corporate_number_of_shares_'+(i+1), 0);
				}
				else {
					localStorage.setItem('corporate_number_of_shares_'+(i+1), document.getElementById("corporate_number_of_shares_"+(i+1)).value);
				}
			}
			for (var i = 0; i < localStorage.length; i++){
	    		console.log(localStorage.key(i)+" ==> "+localStorage.getItem(localStorage.key(i)));
			}
			document.location.href="<?php echo BASE_URL;?>/thankyou.php";
		}
	}
	
	function ValidateEmail(mail) 
	{
		var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
		if(mail.match(mailformat))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	
	
	function cusSelectCheck() {
		
		admOptionValue = $( "#customer_dropdown option:selected" ).text();
                //admOptionValue = document.getElementById("customer_dropdown").value;
		
		
		if(admOptionValue == 'Others / Agent'){
		    //$("#others_form").append(div);
                    var direcount = localStorage.getItem("Director_COUNT");
		    var sharecount = localStorage.getItem("Shareholder_COUNT"); 
                    var diresharecount = localStorage.getItem("Director and Shareholder_COUNT");
		    var corpcount = localStorage.getItem("Corporate Shareholder_COUNT"); 
                    var bosharescount =  parseInt(sharecount) ;
			        var bodisharescount = parseInt(diresharecount);
  
		    $("#otheragentsmodule").css("display","block");
                    localStorage.setItem('bosharescount', bosharescount);
		     localStorage.setItem('bodisharescount',bodisharescount);
		}
		else{
		    //$("#others_form").empty();
                    var direcount = localStorage.getItem("Director_COUNT");
		    var sharecount = localStorage.getItem("Shareholder_COUNT"); 
                    var diresharecount = localStorage.getItem("Director and Shareholder_COUNT");
		    var corpcount = localStorage.getItem("Corporate Shareholder_COUNT"); 
                    var bosharescount =  parseInt(sharecount) ;
			        var bodisharescount = parseInt(diresharecount);
                    //alert(agentcnt);

		    $("#otheragentsmodule").css("display","none");
                    localStorage.setItem('bosharescount', bosharescount);
			        localStorage.setItem('bodisharescount',bodisharescount);
		 }
        }

	
		$(function () {
			for (var i = 0; i < localStorage.length; i++){
    		console.log(localStorage.key(i)+" ==> "+localStorage.getItem(localStorage.key(i)));
		}
		var customerNameList = new Array();
		var directorNameList = new Array();
		var indShareNameList = new Array();
		var indShareBoList = new Array();
		var dirShareNameList = new Array();	
		var dirShareBoList = new Array();
		var corporateNameList = new Array();
	var i = 0;
		for (i = 0; i < localStorage.getItem("Director_COUNT"); i++){
			directorNameList[i]=localStorage.getItem("directorSurname_"+(i+1));
		
		}
		for (i = 0; i < localStorage.getItem("Shareholder_COUNT"); i++){
			indShareNameList[i]=localStorage.getItem("shareholdersurname_"+(i+1));
			indShareBoList[i]=localStorage.getItem("shareholderbowner_"+(i+1));
		}
		for (i = 0; i < localStorage.getItem("Director and Shareholder_COUNT"); i++){
			dirShareNameList[i]=localStorage.getItem("dishareholdersurname_"+(i+1));
			dirShareBoList[i]=localStorage.getItem("dishareholderbowner_"+(i+1));
		}
		for (i = 0; i < localStorage.getItem("Corporate Shareholder_COUNT"); i++){
			corporateNameList[i] = localStorage.getItem("CorporateShareholderName_"+(i+1));
		}
		
		var customerNameList = customerNameList.concat(directorNameList); 
		var customerNameList = customerNameList.concat(indShareNameList); 
		var customerNameList = customerNameList.concat(dirShareNameList); 
		var customerNameList = customerNameList.concat(corporateNameList); 
		
		var select = document.getElementById("year");
    for(var i = 0; i < customerNameList.length; i++) {
       // var option = '<option value="Individual Shareholder 1">Individual Shareholder 1</option>'
       $("#customer_dropdown").append('<option value="Individual Shareholder 1">'+customerNameList[i]+'</option>');
	   if(i==(customerNameList.length-1)){
		    $("#customer_dropdown").append('<option value="Individual Shareholder 1">Others / Agent</option>');
	   }
    }
		
		console.log(localStorage.getItem("Director_COUNT"));
		for (var i = 0; i < localStorage.getItem("Director_COUNT"); i++){
			console.log(localStorage.getItem("Shareholder_COUNT"));
		}
		
		for (var i = 0; i < localStorage.getItem("Director_COUNT"); i++){
			//console.log("test");
    		directorNameList[i] = localStorage.getItem("directorSurname_"+(i+1));
			
			 $("#share_holder_list").append('<div class="row">'
						+'<div class="col-sm-12">'
							+'<div class="row">'
								+'<div class="col-sm-4 mb-1">'
									+'<input id="individual_shareholder_name" type="text" name="" class="form-control form-bg-2" placeholder="" value="'+localStorage.getItem("directorSurname_"+(i+1))+'" readonly />'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="director_paid_up_capital_'+(i+1)+'" />'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="director_number_of_shares_'+(i+1)+'"/>'
								+'</div>'
								+'<div class="col-sm-4">'
									+'<div class="row">'
										+'<div class="col-sm-4 mb-1">'
											
										+'</div>'
										+'<div class="col-sm-8 mb-1">'
											
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>');
		}
		
		for (var i = 0; i < localStorage.getItem("Shareholder_COUNT"); i++){

			var str = '<div class="row">'
						+'<div class="col-sm-12">'
							+'<div class="row">'
								+'<div class="col-sm-4 mb-1">'
									+'<input id="individual_shareholder_name" type="text" name="" class="form-control form-bg-2" placeholder="" value="'+localStorage.getItem("shareholdersurname_"+(i+1))+'" readonly />'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="individual_shareholder_paid_up_capital_'+(i+1)+'"/>'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="individual_shareholder_number_of_shares_'+(i+1)+'"/>'
								+'</div>'
								+'<div class="col-sm-4">'
									+'<div class="row">'
										+'<div class="col-sm-4 mb-1">'
											+'<input type="text" name="" class="form-control form-bg-2" id="" value="'+localStorage.getItem("shareholderbowner_"+(i+1))+'" readonly />'
										+'</div>';
										if(localStorage.getItem("shareholderbowner_"+(i+1))!='Yes'){
											var pos = i+1;
											var key = "shareholder";
											str += '<div class="col-sm-8 mb-1">'
												+'<a href="" class="btn declare-bo" data-toggle="modal" data-target="#modalbeneficialowner_'+pos+'_'+key+'" onclick="declare_ultimate_button('+pos+',\''+key+'\')">Declare Ultimate BO</a>'
												+'</div>';
										}
										
									str += '</div>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>';
					
			 $("#share_holder_list").append(str);
		}
		
		for (var i = 0; i < localStorage.getItem("Director and Shareholder_COUNT"); i++){
			var str = '<div class="row">'
						+'<div class="col-sm-12">'
							+'<div class="row">'
								+'<div class="col-sm-4 mb-1">'
									+'<input id="individual_shareholder_name" type="text" name="" class="form-control form-bg-2" placeholder="" value="'+localStorage.getItem("dishareholdersurname_"+(i+1))+'" readonly />'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="director_shareholder_paid_up_capital_'+(i+1)+'"/>'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="director_shareholder_number_of_shares_'+(i+1)+'" />'
								+'</div>'
								+'<div class="col-sm-4">'
									+'<div class="row">'
										+'<div class="col-sm-4 mb-1">'
											+'<input type="text" name="" class="form-control form-bg-2" id="" value="'+localStorage.getItem("dishareholderbowner_"+(i+1))+'" readonly />'
										+'</div>';
										// if(localStorage.getItem("dishareholderbowner_"+(i+1))!='Yes'){
											// var pos = i+1;
											// var key = "directorandshareholder";
										// str +='<div class="col-sm-8 mb-1">'
											// +'<a href="" class="btn declare-bo" data-toggle="modal" data-target="#modalbeneficialowner" onclick="declare_ultimate_button('+pos+','+key+')>Declare Ultimate BO</a>'
											// +'</div>';
										// }
										if(localStorage.getItem("dishareholderbowner_"+(i+1))!='Yes'){
											var pos = i+1;
											var key = "dishareholder";
											str += '<div class="col-sm-8 mb-1">'
												+'<a href="" class="btn declare-bo" data-toggle="modal" data-target="#modalbeneficialowner_'+pos+'_'+key+'" onclick="declare_ultimate_button('+pos+',\''+key+'\')">Declare Ultimate BO</a>'
												+'</div>';
										}
										
									str +='</div>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>'
			 $("#share_holder_list").append(str);
		}
		
		for (var i = 0; i < localStorage.getItem("Corporate Shareholder_COUNT"); i++){
			
			 $("#share_holder_list").append('<div class="row">'
						+'<div class="col-sm-12">'
							+'<div class="row">'
								+'<div class="col-sm-4 mb-1">'
									+'<input id="individual_shareholder_name" type="text" name="" class="form-control form-bg-2" placeholder="" value="'+localStorage.getItem("CorporateShareholderName_"+(i+1))+'" readonly />'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="corporate_paid_up_capital_'+(i+1)+'"/>'
								+'</div>'
								+'<div class="col-sm-2 mb-1">'
									+'<input type="text" name="" class="form-control form-bg-2" placeholder="" id="corporate_number_of_shares_'+(i+1)+'"/>'
								+'</div>'
								+'<div class="col-sm-4">'
									+'<div class="row">'
										+'<div class="col-sm-4 mb-1">'
											
										+'</div>'
										+'<div class="col-sm-8 mb-1">'
											
										+'</div>'
									+'</div>'
								+'</div>'
							+'</div>'
						+'</div>'
					+'</div>');
		}

		});
	</script>

<div class="modal" tabindex="-1" role="dialog" id="displaysuccessmsg" style="position:fixed;top:23%;">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#bb2121">
        <h5 class="modal-title">Success</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Data has been saved Successfully.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>
</body>
</html>
