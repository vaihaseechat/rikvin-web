<?php
 ob_start(); 
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('In Corp - Rikvin');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


// set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// convert TTF font to TCPDF format and store it on the fonts folder
//$fontname = TCPDF_FONTS::addTTFfont('../fonts/Roboto-Bold.ttf', 'TrueTypeUnicode', '', 96);
//$fontnameb = TCPDF_FONTS::addTTFfont('../fonts/Roboto-Bold.ttf', 'TrueTypeUnicode', '', 96);

// use the font
//$pdf->SetFont($fontname, '', 10, '', false);
//$pdf->SetFont($fontnameb, '', 10, '', false);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
//$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->SetFont('roboto', '', 10);
//$pdf->SetFont('times', 'BI', 10);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage('P', 'A4');

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

//print_r($_POST);
$proposedCompanyName1 = $_POST['proposedCompanyName1'];
$proposedCompanyName2 = $_POST['proposedCompanyName2'];
$proposedCompanyName3 = $_POST['proposedCompanyName3'];
$intendedAddress = $_POST['intendedAddress'];
$streetName = $_POST['streetName'];
$floor = $_POST['floor'];
$unit = $_POST['unit'];
$postalCode = $_POST['postalCode'];
$businessActivities = $_POST['businessActivities'];
$countriesOperation = $_POST['countriesOperation'];
$sourceFunds = $_POST['sourceFunds'];
$address = $_POST['address'];

$director1_surname = ucwords($_POST['directorSurname1']);//this is full name in form
$director1_resstatus = $_POST['directorresstatus1'];//directornonresident_1 directorcitizenpr_1  directorpassholder_1

//director 1 passport preview details
$director1_surname_preview = $_POST['directornonresidentpopupsurname'];
$director1_givenname_preview = $_POST['directornonresidentpopupgivenName'];

$director1_name = $director1_surname_preview.' '.$director1_givenname_preview;
$director1_gender_preview = $_POST['directornonresidentpopupgender'];
if($director1_gender_preview == 'on'){
	$director1_gender_preview = 'Male';
}else{
	$director1_gender_preview = 'Female';
}
$director1_passport_preview = $_POST['directornonresidentpopuppassportno'];
$director1_nationality_preview = $_POST['directornonresidentpopupnationality'];
$director1_country_preview = $_POST['directornonresidentpopupcountry'];
$director1_expirydate_preview = $_POST['directornonresidentpopupdateexpiry'];
$director1_dob_preview = $_POST['directornonresidentpopupdob'];

//director 1 passport manual details
$director1_gender_manual = $_POST['directornonmanuallygender'];
if($director1_gender_manual == 'on'){
	$director1_gender_manual = 'Male';
}else{
	$director1_gender_manual = 'Female';
}
$director1_passport_manual = $_POST['directornonmanuallypassportno'];
$director1_nationality_manual = $_POST['directornonmanuallynationality'];
$director1_country_manual = $_POST['directornonmanuallycountry'];
$director1_dateissued_manual = $_POST['directornonmanuallydatetimepicker'];
$director1_expirydate_manual = $_POST['directornonmanuallydateexpiry'];
$director1_birthplace_manual = $_POST['directornonmanuallyplaceofBirth'];
$director1_dob_manual = $_POST['directornonmanuallydateBirth'];

//director 1 nric front details
$director1_nric_front_no = $_POST['directorpreviewpopupfrontnricnumber'];
$director1_nric_front_gender = $_POST['directorpreviewpopupnricfrontgender'];
if($director1_nric_front_gender == 'on'){
	$director1_nric_front_gender = 'Male';
}else{
	$director1_nric_front_gender = 'Female';
}
$director1_nric_front_dob = $_POST['directorpreviewpopupnricfrontdob'];
$director1_nric_front_nation = $_POST['directorpreviewpopupnricfrontnationality'];
$director1_nric_front_postal = $_POST['directorpreviewpopupnricfrontpostcode'];
$director1_nric_front_street = $_POST['directorpreviewpopupnricfrontstreetname'];
$director1_nric_front_floor = $_POST['directorpreviewpopupnricfrontfloor'];
$director1_nric_front_unit = $_POST['directorpreviewnrpopupicfrontunit'];
$director1_nric_front_address = $director1_nric_front_street.' <br>'.$director1_nric_front_floor.' '.$director1_nric_front_unit.' <br>'.$director1_nric_front_postal;

//director 1 nric back details
$director1_nric_back_no = $_POST['directorpreviewpopupbacknricnumber'];
$director1_nric_back_gender = $_POST['directorpreviewpopupnricbackgender'];
$director1_nric_back_dob = $_POST['directorpreviewpopupnricbackdob'];
$director1_nric_back_nation = $_POST['directorpreviewpopupnricbacknationality'];
$director1_nric_back_postal = $_POST['directorpreviewpopupnricbackpostcode'];
$director1_nric_back_street = $_POST['directorpreviewpopupnricbackstreetname'];
$director1_nric_back_floor = $_POST['directorpreviewpopupnricbackfloor'];
$director1_nric_back_unit = $_POST['directorpreviewnrpopupicbackunit'];

//director 1 nric manual details
$director1_nric_no_manual = $_POST['directorpreviewmanuallynricnumber'];
$director1_nric_gender_manual = $_POST['directorpreviewmanuallynricgender'];
if($director1_nric_gender_manual == 'on'){
	$director1_nric_gender_manual = 'Male';
}else{
	$director1_nric_gender_manual = 'Female';
}
$director1_nric_dob_manual = $_POST['directorpreviewmanuallynricdob'];
$director1_nric_nation_manual = $_POST['directorpreviewmanuallynricnationality'];
$director1_nric_postal_manual = $_POST['directorpreviewmanuallynricpostcode'];
$director1_nric_street_manual = $_POST['directorpreviewmanuallynricstreetname'];
$director1_nric_floor_manual = $_POST['directorpreviewmanuallynricfloor'];
$director1_nric_unit_manual = $_POST['directorpreviewnrmanuallyicunit'];
$director1_nric_address_manual = $director1_nric_street_manual.' <br>'.$director1_nric_floor_manual.' '.$director1_nric_unit_manual.' <br>'.$director1_nric_postal_manual;

//director 1 fin popup details
$director1_fin_surname_preview = $_POST['directorfincardpopupsurname'];
$director1_fin_givenname_preview = $_POST['directorfincardpopupgivenName'];

$director1_fin_name = $director1_fin_surname_preview.' '.$director1_fin_givenname_preview;

$director1_fin_passport_preview = $_POST['directorfincardpopuppassportno'];
$director1_fin_nationality_preview = $_POST['directorfincardpopupnationality'];
$director1_fin_country_preview = $_POST['directorfincardpopupcountry'];
$director1_fin_gender_preview = $_POST['directorfincardpopupgender'];
if($director1_fin_gender_preview == 'on'){
	$director1_fin_gender_preview = 'Male';
}else{
	$director1_fin_gender_preview = 'Female';
}
$director1_fin_expirydate_preview = $_POST['directorfincardpopupdateexpiry'];
$director1_fin_dob_preview = $_POST['directorfincardpopupdob'];
$finAddress = $_POST['finAddress'];

//director 1 fin front details
$director1_fin_front_employer = $_POST['directorfincardpopupfrontemployer'];
$director1_fin_front_occupation = $_POST['directorfincardpopupfrontoccupation'];
$director1_fin_front_issueddate = $_POST['directorfincardpopupfrontdateissue'];
$director1_fin_front_expirydate = $_POST['directorfincardpopupfrontdateexpiry'];
$director1_fin_front_finno = $_POST['directorwfincardpopupfrontNumber'];

//director 1 fin manual details
$director1_fin_passport_manual = $_POST['directorfincardmanuallypassportno'];
$director1_fin_nationality_manual = $_POST['directorfincardmanuallynationality'];
$director1_fin_country_manual = $_POST['directorfincardmanuallycountry'];
$director1_fin_gender_manual = $_POST['directorfincardmanuallygender'];
if($director1_fin_gender_manual == 'on'){
	$director1_fin_gender_manual = 'Male';
}else{
	$director1_fin_gender_manual = 'Female';
}
$director1_fin_issuedate_manual = $_POST['directorfincardmanuallydatetimepicker'];
$director1_fin_expirydate_manual = $_POST['directorfincardmanuallydateexpiry'];
$director1_fin_birthplace_manual = $_POST['directorfincardmanuallyplaceofBirth'];
$director1_fin_dob_manual = $_POST['directorfincardmanuallydateBirth'];

$director1_fin_employer_manual = $_POST['directorfincardmanuallyemployer'];
$director1_fin_occupation_manual = $_POST['directorfincardmanuallyoccupation'];
$director1_fin_workpassissueddate_manual = $_POST['directorfincardmanuallyworkpassdateissue'];
$director1_fin_workpassexpirydate_manual = $_POST['directorfincardmanuallyworkpassdateexpiry'];
$director1_fin_finno_manual = $_POST['directorfincardmanuallywfincardnumber'];

//contact details
$handphone = $_POST['handphone'];
$officephone = $_POST['officephone'];
$businessemail = $_POST['businessemail'];
$personalemail = $_POST['personalemail'];
$directorpep = $_POST['directorpep'];
if($directorpep == 'directorpepyes_1'){
	$directorpep = 'Yes';
}else{
	$directorpep = 'No';
}
$pepCountry = $_POST['pepCountry'];
$pepRole = $_POST['pepRole'];
$pepFrom = $_POST['pepFrom'];
$pepTo = $_POST['pepTo'];

//==============share holders details starts here===================//

$shareholder1_surname = ucwords($_POST['shareholdersurname1']);//this is full name in form
$shareholder1_resstatus = $_POST['shareholderresstatus1'];//shareholdernonresident_1 shareholdercitizenpr_1 shareholderpassholder_1
$shareholder1_bowner = $_POST['shareholderbowner1'];
$shareholder1_address = $_POST['shareholdernonresidentresaddress'];
$shareholder1_finAddress = $_POST['shareholderFINaddress'];

//shareholder 1 passport preview details
$shareholder1_surname_preview = $_POST['shareholdernonresidentpopupsurname'];
$shareholder1_givenname_preview = $_POST['shareholdernonresidentpopupgivenName'];

$shareholder1_name = $shareholder1_surname_preview.' '.$shareholder1_givenname_preview;
$shareholder1_gender_preview = $_POST['shareholdernonresidentpopupgender'];
if($shareholder1_gender_preview == 'on'){
	$shareholder1_gender_preview = 'Male';
}else{
	$shareholder1_gender_preview = 'Female';
}
$shareholder1_passport_preview = $_POST['shareholdernonresidentpopuppassportno'];
$shareholder1_nationality_preview = $_POST['shareholdernonresidentpopupnationality'];
$shareholder1_country_preview = $_POST['shareholdernonresidentpopupcountry'];
$shareholder1_expirydate_preview = $_POST['shareholdernonresidentpopupdateexpiry'];
$shareholder1_dob_preview = $_POST['shareholdernonresidentpopupdob'];

//shareholder 1 passport manual details
$shareholder1_gender_manual = $_POST['shareholdernonmanuallygender'];
if($shareholder1_gender_manual == 'on'){
	$shareholder1_gender_manual = 'Male';
}else{
	$shareholder1_gender_manual = 'Female';
}
$shareholder1_passport_manual = $_POST['shareholdernonmanuallypassportno'];
$shareholder1_nationality_manual = $_POST['shareholdernonmanuallynationality'];
$shareholder1_country_manual = $_POST['shareholdernonmanuallycountry'];
$shareholder1_dateissued_manual = $_POST['shareholdernonmanuallydatetimepicker'];
$shareholder1_expirydate_manual = $_POST['shareholdernonmanuallydateexpiry'];
$shareholder1_birthplace_manual = $_POST['shareholdernonmanuallyplaceofBirth'];
$shareholder1_dob_manual = $_POST['shareholdernonmanuallydateBirth'];

//shareholder 1 nric front details
$shareholder1_nric_front_no = $_POST['shareholderpreviewpopupfrontnricnumber'];
$shareholder1_nric_front_gender = $_POST['shareholderpreviewpopupnricfrontgender'];
if($shareholder1_nric_front_gender == 'on'){
	$shareholder1_nric_front_gender = 'Male';
}else{
	$shareholder1_nric_front_gender = 'Female';
}
$shareholder1_nric_front_dob = $_POST['shareholderpreviewpopupnricfrontdob'];
$shareholder1_nric_front_nation = $_POST['shareholderpreviewpopupnricfrontnationality'];
$shareholder1_nric_front_postal = $_POST['shareholderpreviewpopupnricfrontpostcode'];
$shareholder1_nric_front_street = $_POST['shareholderpreviewpopupnricfrontstreetname'];
$shareholder1_nric_front_floor = $_POST['shareholderpreviewpopupnricfrontfloor'];
$shareholder1_nric_front_unit = $_POST['shareholderpreviewnrpopupicfrontunit'];
$shareholder1_nric_front_address = $shareholder1_nric_front_street.' <br>'.$shareholder1_nric_front_floor.' '.$shareholder1_nric_front_unit.' <br>'.$shareholder1_nric_front_postal;

//shareholder 1 nric back details
$shareholder1_nric_back_no = $_POST['shareholderpreviewpopupbacknricnumber'];
$shareholder1_nric_back_gender = $_POST['shareholderpreviewpopupnricbackgender'];
$shareholder1_nric_back_dob = $_POST['shareholderpreviewpopupnricbackdob'];
$shareholder1_nric_back_nation = $_POST['shareholderpreviewpopupnricbacknationality'];
$shareholder1_nric_back_postal = $_POST['shareholderpreviewpopupnricbackpostcode'];
$shareholder1_nric_back_street = $_POST['shareholderpreviewpopupnricbackstreetname'];
$shareholder1_nric_back_floor = $_POST['shareholderpreviewpopupnricbackfloor'];
$shareholder1_nric_back_unit = $_POST['shareholderpreviewnrpopupicbackunit'];

//shareholder 1 nric manual details
$shareholder1_nric_no_manual = $_POST['shareholderpreviewmanuallynricnumber'];
$shareholder1_nric_gender_manual = $_POST['shareholderpreviewmanuallynricgender'];
if($shareholder1_nric_gender_manual == 'on'){
	$shareholder1_nric_gender_manual = 'Male';
}else{
	$shareholder1_nric_gender_manual = 'Female';
}
$shareholder1_nric_dob_manual = $_POST['shareholderpreviewmanuallynricdob'];
$shareholder1_nric_nation_manual = $_POST['shareholderpreviewmanuallynricnationality'];
$shareholder1_nric_postal_manual = $_POST['shareholderpreviewmanuallynricpostcode'];
$shareholder1_nric_street_manual = $_POST['shareholderpreviewmanuallynricstreetname'];
$shareholder1_nric_floor_manual = $_POST['shareholderpreviewmanuallynricfloor'];
$shareholder1_nric_unit_manual = $_POST['shareholderpreviewnrmanuallyicunit'];
$shareholder1_nric_address_manual = $shareholder1_nric_street_manual.' <br>'.$shareholder1_nric_floor_manual.' '.$shareholder1_nric_unit_manual.' <br>'.$shareholder1_nric_postal_manual;

//shareholder 1 fin popup details
$shareholder1_fin_surname_preview = $_POST['shareholderfincardpopupsurname'];
$shareholder1_fin_givenname_preview = $_POST['shareholderfincardpopupgivenName'];

$shareholder1_fin_name = $shareholder1_fin_surname_preview.' '.$shareholder1_fin_givenname_preview;

$shareholder1_fin_passport_preview = $_POST['shareholderfincardpopuppassportno'];
$shareholder1_fin_nationality_preview = $_POST['shareholderfincardpopupnationality'];
$shareholder1_fin_country_preview = $_POST['shareholderfincardpopupcountry'];
$shareholder1_fin_gender_preview = $_POST['shareholderfincardpopupgender'];
if($shareholder1_fin_gender_preview == 'on'){
	$shareholder1_fin_gender_preview = 'Male';
}else{
	$shareholder1_fin_gender_preview = 'Female';
}
$shareholder1_fin_expirydate_preview = $_POST['shareholderfincardpopupdateexpiry'];
$shareholder1_fin_dob_preview = $_POST['shareholderfincardpopupdob'];
$shareholder1_finAddress = $_POST['shareholderFINresaddress'];

//shareholder 1 fin front details
$shareholder1_fin_front_employer = $_POST['shareholderfincardfrontemployer'];
$shareholder1_fin_front_occupation = $_POST['shareholderfincardfrontoccupation'];
$shareholder1_fin_front_issueddate = $_POST['shareholderfincardfrontdateissue'];
$shareholder1_fin_front_expirydate = $_POST['shareholderfincardfrontdateexpiry'];
$shareholder1_fin_front_finno = $_POST['shareholderwfincardfrontNumber'];

//shareholder 1 fin manual details
$shareholder1_fin_passport_manual = $_POST['shareholderfincardmanuallypassportno'];
$shareholder1_fin_nationality_manual = $_POST['shareholderfincardmanuallynationality'];
$shareholder1_fin_country_manual = $_POST['shareholderfincardmanuallycountry'];
$shareholder1_fin_gender_manual = $_POST['shareholderfincardmanuallygender'];
if($shareholder1_fin_gender_manual == 'on'){
	$shareholder1_fin_gender_manual = 'Male';
}else{
	$shareholder1_fin_gender_manual = 'Female';
}
$shareholder1_fin_issuedate_manual = $_POST['shareholderfincardmanuallydatetimepicker'];
$shareholder1_fin_expirydate_manual = $_POST['shareholderfincardmanuallydateexpiry'];
$shareholder1_fin_birthplace_manual = $_POST['shareholderfincardmanuallyplaceofBirth'];
$shareholder1_fin_dob_manual = $_POST['shareholderfincardmanuallydateBirth'];

$shareholder1_fin_employer_manual = $_POST['shareholderfincardmanuallyemployer'];
$shareholder1_fin_occupation_manual = $_POST['shareholderfincardmanuallyoccupation'];
$shareholder1_fin_workpassissueddate_manual = $_POST['shareholderfincardmanuallyworkpassdateissue'];
$shareholder1_fin_workpassexpirydate_manual = $_POST['shareholderfincardmanuallyworkpassdateexpiry'];
$shareholder1_fin_finno_manual = $_POST['shareholderfincardmanuallywfincardnumber'];

//contact details
$shareholderhandphone = $_POST['shareholderhandphone'];
$shareholderOfficephone = $_POST['shareholderOfficephone'];
$shareholderbusinessemail = $_POST['shareholderbusinessemail'];
$shareholderPersonalemail = $_POST['shareholderPersonalemail'];
$shareholderpep = $_POST['shareholderpep'];
if($shareholderpep == 'shareholderpepyes_1'){
	$shareholderpep = 'Yes';
}else{
	$shareholderpep = 'No';
}
$shareholdercountrypep = $_POST['shareholdercountrypep'];
$shareholderrolePep = $_POST['shareholderrolePep'];
$shareholderpepFrom = $_POST['shareholderpepFrom'];
$shareholderpepTo = $_POST['shareholderpepTo'];

$shareholder1_nricpicfront = $_POST['shareholderSGnricfront'];
$shareholder1_nricpicback = $_POST['shareholderSGnricback'];

//==============director ad share holders details starts here===================//

$di1_surname = ucwords($_POST['dishareholdersurname1']);//this is full name in form
$di1_resstatus = $_POST['dishareholderresstatus1'];//dinonresident_1 dicitizenpr_1 dipassholder_1
$di1_bowner = $_POST['dishareholderbowner1'];
$di1_address = $_POST['dishareholdernonresidentresaddress'];
$di1_finAddress = $_POST['dishareholderFINaddress'];

//di 1 passport preview details
$di1_surname_preview = $_POST['dinonresidentpopupsurname'];
$di1_givenname_preview = $_POST['dinonresidentpopupgivenName'];

$di1_name = $di1_surname_preview.' '.$di1_givenname_preview;
$di1_gender_preview = $_POST['dinonresidentpopupgender'];
if($di1_gender_preview == 'on'){
	$di1_gender_preview = 'Male';
}else{
	$di1_gender_preview = 'Female';
}
$di1_passport_preview = $_POST['dinonresidentpopuppassportno'];
$di1_nationality_preview = $_POST['dinonresidentpopupnationality'];
$di1_country_preview = $_POST['dinonresidentpopupcountry'];
$di1_expirydate_preview = $_POST['dinonresidentpopupdateexpiry'];
$di1_dob_preview = $_POST['dinonresidentpopupdob'];

//di 1 passport manual details
$di1_gender_manual = $_POST['dinonmanuallygender'];
if($di1_gender_manual == 'on'){
	$di1_gender_manual = 'Male';
}else{
	$di1_gender_manual = 'Female';
}
$di1_passport_manual = $_POST['dinonmanuallypassportno'];
$di1_nationality_manual = $_POST['dinonmanuallynationality'];
$di1_country_manual = $_POST['dinonmanuallycountry'];
$di1_dateissued_manual = $_POST['dinonmanuallydatetimepicker'];
$di1_expirydate_manual = $_POST['dinonmanuallydateexpiry'];
$di1_birthplace_manual = $_POST['dinonmanuallyplaceofBirth'];
$di1_dob_manual = $_POST['dinonmanuallydateBirth'];

//di 1 nric front details
$di1_nric_front_no = $_POST['dipreviewpopupfrontnricnumber'];
$di1_nric_front_gender = $_POST['dipreviewpopupnricfrontgender'];
if($di1_nric_front_gender == 'on'){
	$di1_nric_front_gender = 'Male';
}else{
	$di1_nric_front_gender = 'Female';
}
$di1_nric_front_dob = $_POST['dipreviewpopupnricfrontdob'];
$di1_nric_front_nation = $_POST['dipreviewpopupnricfrontnationality'];
$di1_nric_front_postal = $_POST['dipreviewpopupnricfrontpostcode'];
$di1_nric_front_street = $_POST['dipreviewpopupnricfrontstreetname'];
$di1_nric_front_floor = $_POST['dipreviewpopupnricfrontfloor'];
$di1_nric_front_unit = $_POST['dipreviewnrpopupicfrontunit'];
$di1_nric_front_address = $di1_nric_front_street.' <br>'.$di1_nric_front_floor.' '.$di1_nric_front_unit.' <br>'.$di1_nric_front_postal;

//di 1 nric back details
$di1_nric_back_no = $_POST['dipreviewpopupbacknricnumber'];
$di1_nric_back_gender = $_POST['dipreviewpopupnricbackgender'];
$di1_nric_back_dob = $_POST['dipreviewpopupnricbackdob'];
$di1_nric_back_nation = $_POST['dipreviewpopupnricbacknationality'];
$di1_nric_back_postal = $_POST['dipreviewpopupnricbackpostcode'];
$di1_nric_back_street = $_POST['dipreviewpopupnricbackstreetname'];
$di1_nric_back_floor = $_POST['dipreviewpopupnricbackfloor'];
$di1_nric_back_unit = $_POST['dipreviewnrpopupicbackunit'];

//di 1 nric manual details
$di1_nric_no_manual = $_POST['dipreviewmanuallynricnumber'];
$di1_nric_gender_manual = $_POST['dipreviewmanuallynricgender'];
if($di1_nric_gender_manual == 'on'){
	$di1_nric_gender_manual = 'Male';
}else{
	$di1_nric_gender_manual = 'Female';
}
$di1_nric_dob_manual = $_POST['dipreviewmanuallynricdob'];
$di1_nric_nation_manual = $_POST['dipreviewmanuallynricnationality'];
$di1_nric_postal_manual = $_POST['dipreviewmanuallynricpostcode'];
$di1_nric_street_manual = $_POST['dipreviewmanuallynricstreetname'];
$di1_nric_floor_manual = $_POST['dipreviewmanuallynricfloor'];
$di1_nric_unit_manual = $_POST['dipreviewnrmanuallyicunit'];
$di1_nric_address_manual = $di1_nric_street_manual.' <br>'.$di1_nric_floor_manual.' '.$di1_nric_unit_manual.' <br>'.$di1_nric_postal_manual;

//di 1 fin popup details
$di1_fin_surname_preview = $_POST['difincardpopupsurname'];
$di1_fin_givenname_preview = $_POST['difincardpopupgivenName'];

$di1_fin_name = $di1_fin_surname_preview.' '.$di1_fin_givenname_preview;

$di1_fin_passport_preview = $_POST['difincardpopuppassportno'];
$di1_fin_nationality_preview = $_POST['difincardpopupnationality'];
$di1_fin_country_preview = $_POST['difincardpopupcountry'];
$di1_fin_gender_preview = $_POST['difincardpopupgender'];
if($di1_fin_gender_preview == 'on'){
	$di1_fin_gender_preview = 'Male';
}else{
	$di1_fin_gender_preview = 'Female';
}
$di1_fin_expirydate_preview = $_POST['difincardpopupdateexpiry'];
$di1_fin_dob_preview = $_POST['difincardpopupdob'];
$di1_finAddress = $_POST['dishareholderFINresaddress'];

//di 1 fin front details
$di1_fin_front_employer = $_POST['difincardfrontemployer'];
$di1_fin_front_occupation = $_POST['difincardfrontoccupation'];
$di1_fin_front_issueddate = $_POST['difincardfrontdateissue'];
$di1_fin_front_expirydate = $_POST['difincardfrontdateexpiry'];
$di1_fin_front_finno = $_POST['diwfincardfrontNumber'];

//di 1 fin manual details
$di1_fin_passport_manual = $_POST['difincardmanuallypassportno'];
$di1_fin_nationality_manual = $_POST['difincardmanuallynationality'];
$di1_fin_country_manual = $_POST['difincardmanuallycountry'];
$di1_fin_gender_manual = $_POST['difincardmanuallygender'];
if($di1_fin_gender_manual == 'on'){
	$di1_fin_gender_manual = 'Male';
}else{
	$di1_fin_gender_manual = 'Female';
}
$di1_fin_issuedate_manual = $_POST['difincardmanuallydatetimepicker'];
$di1_fin_expirydate_manual = $_POST['difincardmanuallydateexpiry'];
$di1_fin_birthplace_manual = $_POST['difincardmanuallyplaceofBirth'];
$di1_fin_dob_manual = $_POST['difincardmanuallydateBirth'];

$di1_fin_employer_manual = $_POST['difincardmanuallyemployer'];
$di1_fin_occupation_manual = $_POST['difincardmanuallyoccupation'];
$di1_fin_workpassissueddate_manual = $_POST['difincardmanuallyworkpassdateissue'];
$di1_fin_workpassexpirydate_manual = $_POST['difincardmanuallyworkpassdateexpiry'];
$di1_fin_finno_manual = $_POST['difincardmanuallywfincardnumber'];

//contact details
$dihandphone = $_POST['dishareholderhandphone'];
$diOfficephone = $_POST['dishareholderOfficephone'];
$dibusinessemail = $_POST['dishareholderbusinessemail'];
$diPersonalemail = $_POST['dishareholderPersonalemail'];
$dipep = $_POST['dishareholderpep'];
if($dipep == 'dishareholderpepyes_1'){
	$dipep = 'Yes';
}else{
	$dipep = 'No';
}
$dicountrypep = $_POST['dishareholdercountrypep'];
$dirolePep = $_POST['dishareholderrolePep'];
$dipepFrom = $_POST['dishareholderpepFrom'];
$dipepTo = $_POST['dishareholderpepTo'];

//==============corporate shareholders details starts here===================//

$corporate1_surname = ucwords($_POST['CorporateShareholderName']);//this is full name in form
$corporate1_country = $_POST['CorporateCountry'];//corporatenonresident_1 corporatecitizenpr_1 corporatepassholder_1
$corporate1_contact = $_POST['CorporatecontactNumber'];
$corporate1_email = $_POST['CorporateEmail'];
$corporate1_address = $_POST['corporatenonresidentaddress'];
$corporate1_resaddress = $_POST['corporatenonresidentresaddress'];
$corporate1_finAddress = $_POST['corporateFINresaddress'];
$corporate1_resstatus = $_POST['corporateresstatus1'];
$corporate1_certificate = $_POST['CorporateCertificate'];
$corporate1_certificate1 = $_POST['CorporateCertificate1'];
$corporate1_representname = $_POST['CorporateRepresentativeName'];

//corporate 1 passport preview details
$corporate1_surname_preview = $_POST['corporatenonresidentpopupsurname'];
$corporate1_givenname_preview = $_POST['corporatenonresidentpopupgivenName'];

$corporate1_name = $corporate1_surname_preview.' '.$corporate1_givenname_preview;
$corporate1_gender_preview = $_POST['corporatenonresidentpopupgender'];
if($corporate1_gender_preview == 'on'){
	$corporate1_gender_preview = 'Male';
}else{
	$corporate1_gender_preview = 'Female';
}
$corporate1_passport_preview = $_POST['corporatenonresidentpopuppassportno'];
$corporate1_nationality_preview = $_POST['corporatenonresidentpopupnationality'];
$corporate1_country_preview = $_POST['corporatenonresidentpopupcountry'];
$corporate1_expirydate_preview = $_POST['corporatenonresidentpopupdateexpiry'];
$corporate1_dob_preview = $_POST['corporatenonresidentpopupdob'];
$corporate1_birthplace_preview = $_POST['corporatenonresidentpopupplaceofBirth'];

//corporate 1 passport manual details
$corporate1_gender_manual = $_POST['corporatenonmanuallygender'];
if($corporate1_gender_manual == 'on'){
	$corporate1_gender_manual = 'Male';
}else{
	$corporate1_gender_manual = 'Female';
}
$corporate1_passport_manual = $_POST['corporatenonmanuallypassportno'];
$corporate1_nationality_manual = $_POST['corporatenonmanuallynationality'];
$corporate1_country_manual = $_POST['corporatenonmanuallycountry'];
$corporate1_dateissued_manual = $_POST['corporatenonmanuallydatetimepicker'];
$corporate1_expirydate_manual = $_POST['corporatenonmanuallydateexpiry'];
$corporate1_birthplace_manual = $_POST['corporatenonmanuallyplaceofBirth'];
$corporate1_dob_manual = $_POST['corporatenonmanuallydateBirth'];

//corporate shareholder 1 nric front details
$corporate1_nric_front_no = $_POST['corporatepreviewpopupfrontnricnumber'];
$corporate1_nric_front_gender = $_POST['corporatepreviewpopupnricfrontgender'];
if($corporate1_nric_front_gender == 'on'){
	$corporate1_nric_front_gender = 'Male';
}else{
	$corporate1_nric_front_gender = 'Female';
}
$corporate1_nric_front_dob = $_POST['corporatepreviewpopupnricfrontdob'];
$corporate1_nric_front_nation = $_POST['corporatepreviewpopupnricfrontnationality'];
$corporate1_nric_front_postal = $_POST['corporatepreviewpopupnricfrontpostcode'];
$corporate1_nric_front_street = $_POST['corporatepreviewpopupnricfrontstreetname'];
$corporate1_nric_front_floor = $_POST['corporatepreviewpopupnricfrontfloor'];
$corporate1_nric_front_unit = $_POST['corporatepreviewnrpopupicfrontunit'];
$corporate1_nric_front_address = $corporate1_nric_front_street.' <br>'.$corporate1_nric_front_floor.' '.$corporate1_nric_front_unit.' <br>'.$corporate1_nric_front_postal;

//corporate shareholder 1 nric back details
$corporate1_nric_back_no = $_POST['corporatepreviewpopupbacknricnumber'];
$corporate1_nric_back_gender = $_POST['corporatepreviewpopupnricbackgender'];
$corporate1_nric_back_dob = $_POST['corporatepreviewpopupnricbackdob'];
$corporate1_nric_back_nation = $_POST['corporatepreviewpopupnricbacknationality'];
$corporate1_nric_back_postal = $_POST['corporatepreviewpopupnricbackpostcode'];
$corporate1_nric_back_street = $_POST['corporatepreviewpopupnricbackstreetname'];
$corporate1_nric_back_floor = $_POST['corporatepreviewpopupnricbackfloor'];
$corporate1_nric_back_unit = $_POST['corporatepreviewnrpopupicbackunit'];

//corporate shareholder 1 nric manual details
$corporate1_nric_no_manual = $_POST['corporatepreviewmanuallynricnumber'];
$corporate1_nric_gender_manual = $_POST['corporatepreviewmanuallynricgender'];
if($corporate1_nric_gender_manual == 'on'){
	$corporate1_nric_gender_manual = 'Male';
}else{
	$corporate1_nric_gender_manual = 'Female';
}
$corporate1_nric_dob_manual = $_POST['corporatepreviewmanuallynricdob'];
$corporate1_nric_nation_manual = $_POST['corporatepreviewmanuallynricnationality'];
$corporate1_nric_postal_manual = $_POST['corporatepreviewmanuallynricpostcode'];
$corporate1_nric_street_manual = $_POST['corporatepreviewmanuallynricstreetname'];
$corporate1_nric_floor_manual = $_POST['corporatepreviewmanuallynricfloor'];
$corporate1_nric_unit_manual = $_POST['corporatepreviewnrmanuallyicunit'];
$corporate1_nric_address_manual = $corporate1_nric_street_manual.' <br>'.$corporate1_nric_floor_manual.' '.$corporate1_nric_unit_manual.' <br>'.$corporate1_nric_postal_manual;

//corporate shareholder 1 fin popup details
$corporate1_fin_surname_preview = $_POST['corporatefincardpopupsurname'];
$corporate1_fin_givenname_preview = $_POST['corporatefincardpopupgivenName'];
$corporate1_fin_name = $corporate1_fin_surname_preview.' '.$corporate1_fin_givenname_preview;
$corporate1_fin_passport_preview = $_POST['corporatefincardpopuppassportno'];
$corporate1_fin_nationality_preview = $_POST['corporatefincardpopupnationality'];
$corporate1_fin_country_preview = $_POST['corporatefincardpopupcountry'];
$corporate1_fin_gender_preview = $_POST['corporatefincardpopupgender'];
if($corporate1_fin_gender_preview == 'on'){
	$corporate1_fin_gender_preview = 'Male';
}else{
	$corporate1_fin_gender_preview = 'Female';
}
$corporate1_fin_expirydate_preview = $_POST['corporatefincardpopupdateexpiry'];
$corporate1_fin_dob_preview = $_POST['corporatefincardpopupdob'];
$corporate1_finresAddress = $_POST['corporateshareholderFINresaddress'];

//corporate shareholder 1 fin front details
$corporate1_fin_front_employer = $_POST['corporatefincardfrontemployer'];
$corporate1_fin_front_occupation = $_POST['corporatefincardfrontoccupation'];
$corporate1_fin_front_issueddate = $_POST['corporatefincardfrontdateissue'];
$corporate1_fin_front_expirydate = $_POST['corporatefincardfrontdateexpiry'];
$corporate1_fin_front_finno = $_POST['corporatewfincardfrontNumber'];

//corporate shareholder 1 fin manual details
$corporate1_fin_passport_manual = $_POST['corporatefincardmanuallypassportno'];
$corporate1_fin_nationality_manual = $_POST['corporatefincardmanuallynationality'];
$corporate1_fin_country_manual = $_POST['corporatefincardmanuallycountry'];
$corporate1_fin_gender_manual = $_POST['corporatefincardmanuallygender'];
if($corporate1_fin_gender_manual == 'on'){
	$corporate1_fin_gender_manual = 'Male';
}else{
	$corporate1_fin_gender_manual = 'Female';
}
$corporate1_fin_issuedate_manual = $_POST['corporatefincardmanuallydatetimepicker'];
$corporate1_fin_expirydate_manual = $_POST['corporatefincardmanuallydateexpiry'];
$corporate1_fin_birthplace_manual = $_POST['corporatefincardmanuallyplaceofBirth'];
$corporate1_fin_dob_manual = $_POST['corporatefincardmanuallydateBirth'];
$corporate1_fin_employer_manual = $_POST['corporatefincardmanuallyemployer'];
$corporate1_fin_occupation_manual = $_POST['corporatefincardmanuallyoccupation'];
$corporate1_fin_workpassissueddate_manual = $_POST['corporatefincardmanuallyworkpassdateissue'];
$corporate1_fin_workpassexpirydate_manual = $_POST['corporatefincardmanuallyworkpassdateexpiry'];
$corporate1_fin_finno_manual = $_POST['corporatefincardmanuallywfincardnumber'];

//declaration page details
$currency = $_POST['currency'];
$paid_up_capital = $_POST['paid_up_capital'];
$number_of_shares = $_POST['number_of_shares'];
$agent_full_name = $_POST['agent_full_name'];
$agent_email = $_POST['agent_email'];
$agent_contact_no = $_POST['agent_contact_no'];

//to check manual entry
$manual_director_passport = $_POST['manual_director_passport'];
$manual_director_nric = $_POST['manual_director_nric'];
$manual_director_fin = $_POST['manual_director_fin'];


//if conditions for EOD

//proposed company names section
if($proposedCompanyName2 !='null'){
	$proposed_co_2 = '<br>'.$proposedCompanyName2;
}
if($proposedCompanyName3 !='null'){
	$proposed_co_2 = '<br>'.$proposedCompanyName3;
}

//registerd company address section
if($intendedAddress == 'serviceProviderAddress'){
	$reg_co_address = '<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">30 Cecil Street,<br>
			#19-08 Prudential Tower,<br>
			Singapore 049712.</span></td>';
}else{
	$reg_co_address = '<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$floor.' '.$streetName.',<br>
				'.$unit.',<br>Singapore '.$postalCode.'.</span></td>';
}

//director non resident passport preview section
if($director1_resstatus == 'directornonresident_1'){
	if($director1_surname_preview != '' && $director1_surname_preview != 'null'){
	$mail_name = $director1_name;
	$director1_nr_pass_preview = '<h4>Director #1 - '.$director1_name.'</h4>
		
	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_name.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_gender_preview.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nationality_preview.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_passport_preview.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_country_preview.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_expirydate_preview.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_dob_preview.'</span></td>								
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$address.'</span></td>
		</tr>';
	//director non resident passport manual section
	}elseif($director1_passport_manual != '' && $director1_passport_manual != 'null'){
	$mail_name = $director1_surname;
	$director1_nr_pass_manual = '<h4>Director #1 - '.$director1_surname.'</h4>
		
	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_gender_manual.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nationality_manual.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_passport_manual.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_country_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_dateissued_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_expirydate_manual.'</span></td>								
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_birthplace_manual.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_dob_manual.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$address.'</span></td>
		</tr>';
	}
}

//director NRIC preview section
if($director1_resstatus == 'directorcitizenpr_1'){
	if($director1_nric_front_no != '' && $director1_nric_front_no != 'null'){
	$mail_name = $director1_surname;
	$director1_nric_preview =  '<h4>Director #1 - '.$director1_surname.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_front_gender.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_front_no.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_front_nation.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_front_dob.'</span></td>
		</tr>
		<tr>
		 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_front_address.'</span></td>					
		</tr>';
	//director nric manual
	}elseif($director1_nric_no_manual != '' && $director1_nric_no_manual != 'null'){
	$mail_name = $director1_surname;
	$director1_nric_manual = '<h4>Director #1 - '.$director1_surname.'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_surname.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_no_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_nation_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_dob_manual.'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_nric_address_manual.'</span></td>					
			</tr>';
	}
}

//director FIN preview
if($director1_resstatus == 'directorpassholder_1'){
	if($director1_fin_surname_preview != '' && $director1_fin_surname_preview != 'null'){
	$mail_name = $director1_fin_name;

	$director1_fin_preview = '<h4>Director #1 - '.$director1_fin_name.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_name.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_gender_preview.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_nationality_preview.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_passport_preview.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_country_preview.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_expirydate_preview.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_dob_preview.'</span></td>								
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$finAddress.'</span></td>
		</tr>

		<tr class="row">
			<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_front_employer.'</span></td>
		</tr>
		<tr>
		<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_front_occupation.'</span></td>								
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_front_finno.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_front_issueddat.'e</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_front_expirydate.'</span></td>
		</tr>';
	//director fin manual
	}elseif($director1_fin_passport_manual != '' && $director1_fin_passport_manual != 'null'){
	$mail_name = $director1_surname;
	$director1_fin_manual = '<h4>Director #1 - '.$director1_surname.'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_surname.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_nationality_manual.'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_passport_manual.'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_country_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_expirydate_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_dob_manual.'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$finAddress.'</span></td>
			</tr>		
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_employer_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_occupation_manual.'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_finno_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_workpassissueddate_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director1_fin_workpassexpirydate_manual.'</span></td>
			</tr>';
	}
}

//director contact details
if($director1_resstatus == 'directornonresident_1' || $director1_resstatus == 'directorcitizenpr_1' || $director1_resstatus == 'directorpassholder_1'){
	$director1_contact = '<tr class="row">
			<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$handphone.' <br>'.$officephone.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$businessemail.' <br>'.$personalemail.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$directorpep.'</span></td>
		</tr>';
	
	if($directorpep == 'Yes'){
		$director1_contact .= '<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP in which country</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$pepCountry.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Role of PEP</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$pepRole.'</span></td>
		</tr>
		<tr>					 
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP since</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$pepFrom.' - '.$pepTo.'</span></td>
		</tr>';
	}
	
	$director1_contact .= '</tbody>
	</table></td>
				<td colspan="2" width="50%" style="text-align:center"><div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-front.jpg" class="img-fluid" alt=""></div>
		<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-back.jpg" class="img-fluid" alt=""></div></td>
			</tr>
			</tbody>							
			</table>

	<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents NRIC front and back, nationality Singaporean)</p>';
}

//shareholders non resident passport preview section
if($shareholder1_resstatus == 'shareholdernonresident_1'){
	if($shareholder1_surname_preview != '' && $shareholder1_surname_preview != 'null'){
	$mail_name = $shareholder1_name;
	$shareholder1_nr_pass_preview = '<h4>Individual Shareholder #1 - '.$shareholder1_name.'</h4>
	
	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_name.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_gender_preview.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nationality_preview.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_passport_preview.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_country_preview.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_expirydate_preview.'</span></td>								
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_dob_preview.'</span></td>
		</tr>';
	//shareholder non resident passport manual section
	}elseif($shareholder1_passport_manual != '' && $shareholder1_passport_manual != 'null'){
	$mail_name = $shareholder1_surname;
	$shareholder1_nr_pass_manual = '<h4>Shareholder #1 - '.$shareholder1_surname.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_gender_manual.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nationality_manual.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_passport_manual.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_country_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_dateissued_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_expirydate_manual.'</span></td>								
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_birthplace_manual.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_dob_manual.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_address.'</span></td>
		</tr>';
	}
}

//shareholder nric preview section
if($shareholder1_resstatus == 'shareholdercitizenpr_1'){
	if($shareholder1_nric_front_no != '' && $shareholder1_nric_front_no != 'null'){
	$mail_name = $shareholder1_surname;
	$shareholder1_nric_preview = '<h4>Individual Shareholder #1 - '.$shareholder1_surname.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_front_gender.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_front_no.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_front_nation.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_front_dob.'</span></td>
		</tr>
		<tr>
		 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_front_address.'</span></td>					
		</tr>';
	
	//shareholder nric manual
	}elseif($shareholder1_nric_no_manual != '' && $shareholder1_nric_no_manual != 'null'){
	$mail_name = $shareholder1_surname;
	$shareholder1_nric_manual = '<h4>Individual Shareholder #1 - '.$shareholder1_surname.'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_surname.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_no_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_nation_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_dob_manual.'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_nric_address_manual.'</span></td>					
			</tr>';
	}
}

//shareholders fin part starts here
if($shareholder1_resstatus == 'shareholderpassholder_1'){

	if($shareholder1_fin_surname_preview != '' && $shareholder1_fin_surname_preview != 'null'){
	$mail_name = $shareholder1_fin_name;			
	$shareholder1_fin_preview = '<h4>Individual Shareholder #1 - '.$shareholder1_fin_name.'</h4>
		
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_name.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_gender_preview.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_nationality_preview.'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_passport_preview.'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_country_preview.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_expirydate_preview.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_dob_preview.'</span></td>								
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_finAddress.'</span></td>
				</tr>
		
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_front_employer.'</span></td>
				</tr>
				<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_front_occupation.'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_front_finno.'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_front_issueddate.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_front_expirydate.'</span></td>
				</tr>';

	}elseif($shareholder1_fin_passport_manual != '' && $shareholder1_fin_passport_manual != 'null'){
		$mail_name = $shareholder1_surname;
		$shareholder1_fin_manual = '<h4>Individual Shareholder #1 - '.$shareholder1_surname.'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_surname.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_nationality_manual.'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_passport_manual.'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_country_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_expirydate_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_dob_manual.'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_finAddress.'</span></td>
			</tr>
		
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_employer_manual.'</span></td>
			</tr>
			<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_occupation_manual.'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_finno_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_workpassissueddate_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder1_fin_workpassexpirydate_manual.'</span></td>
			</tr>';
	}

}
//shareholders fin part ends here

//shareholders contact details
if($shareholder1_resstatus == 'shareholdernonresident_1' || $shareholder1_resstatus == 'shareholdercitizenpr_1' || $shareholder1_resstatus == 'shareholderpassholder_1'){
		$shareholder1_contact = '<tr class="row">
				<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholderhandphone.' <br>'.$shareholderOfficephone.'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholderbusinessemail.' <br>'.$shareholderPersonalemail.'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholderpep.'</span></td>
			</tr>';
			
		if($shareholderpep == 'Yes'){
		$shareholder1_contact .= '<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP in which country</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholdercountrypep.'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Role of PEP</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholderrolePep.'</span></td>
			</tr>
			<tr>					 
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP since</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholderpepFrom.' - '.$shareholderpepTo.'</span></td>
			</tr>';
		}

		$shareholder1_contact .= '</tbody>
				  	</table></td>
				<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
		  <tbody>
			 <tr>
				<td style="border-bottom:1px solid #ccc"><span>Beneficial Owner</span></td>
				<td style="border-bottom:1px solid #ccc"><span style="font-weight:bold">'.$shareholder1_bowner.'</span></td>
			</tr>
			<tr>
				<td><span>Uploaded documents:</span></td>
				<td><span></span></td>
			</tr>
			<tr>								
				<td colspan="2" style="border-bottom:1px solid #ccc">
				<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/ep-back.jpg" class="img-fluid" alt=""></div>
			<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport.jpg" class="img-fluid" alt=""></div>
			<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/ep-front.jpg" class="img-fluid" alt=""></div></td>
			</tr>
			 <tr>
				<td><span>Proof of address</span></td>
				 <td><span style="font-weight:bold"><a href="https://www.winimy.ai/rikvinnew/pdf.php#">fred-utility-address.pdf</a></span></td>
			</tr>	
			</tbody>
		</table></td>
			</tr>
			  </tbody>
			</table>
		<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';
}

//=================director and shareholder part starts here======================//
//director and shareholders non resident passport preview section
if($di1_resstatus == 'dinonresident_1'){
	if($di1_surname_preview != '' && $di1_surname_preview != 'null'){
	$mail_name = $di1_name;
	$di1_nr_pass_preview = '<h4>Director and Shareholder #1 - '.$di1_name.'</h4>
	
	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_name.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_gender_preview.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nationality_preview.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_passport_preview.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_country_preview.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_expirydate_preview.'</span></td>								
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_dob_preview.'</span></td>
		</tr>';
	//director and shareholder non resident passport manual section
	}elseif($di1_passport_manual != '' && $di1_passport_manual != 'null'){
	$mail_name = $di1_surname;
	$di1_nr_pass_manual = '<h4>Director and Shareholder #1 - '.$di1_surname.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_gender_manual.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nationality_manual.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_passport_manual.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_country_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_dateissued_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_expirydate_manual.'</span></td>								
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_birthplace_manual.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_dob_manual.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_address.'</span></td>
		</tr>';
	}
}

//director and shareholder nric preview section
if($di1_resstatus == 'dicitizenpr_1'){
	if($di1_nric_front_no != '' && $di1_nric_front_no != 'null'){
	$mail_name = $di1_surname;
	$di1_nric_preview = '<h4>Director and Shareholder #1 - '.$di1_surname.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_front_gender.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_front_no.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_front_nation.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_front_dob.'</span></td>
		</tr>
		<tr>
		 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_front_address.'</span></td>					
		</tr>';
	
	//director and shareholder nric manual
	}elseif($di1_nric_no_manual != '' && $di1_nric_no_manual != 'null'){
	$mail_name = $di1_surname;
	$di1_nric_manual = '<h4>Director and Shareholder #1 - '.$di1_surname.'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_surname.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_no_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_nation_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_dob_manual.'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_nric_address_manual.'</span></td>					
			</tr>';
	}
}

//director and shareholders fin part starts here
if($di1_resstatus == 'dipassholder_1'){

	if($di1_fin_surname_preview != '' && $di1_fin_surname_preview != 'null'){
		$mail_name = $di1_fin_name;		
		$di1_fin_preview = '<h4>Director and Shareholder #1 - '.$di1_fin_name.'</h4>
		
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_name.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_gender_preview.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_nationality_preview.'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_passport_preview.'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_country_preview.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_expirydate_preview.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_dob_preview.'</span></td>								
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_finAddress.'</span></td>
				</tr>
		
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_front_employer.'</span></td>
				</tr>
				<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_front_occupation.'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_front_finno.'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_front_issueddate.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_front_expirydate.'</span></td>
				</tr>';

	}elseif($di1_fin_passport_manual != '' && $di1_fin_passport_manual != 'null'){
		$mail_name = $di1_surname;
		$di1_fin_manual = '<h4>Director and Shareholder #1 - '.$di1_surname.'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_surname.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_nationality_manual.'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_passport_manual.'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_country_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_expirydate_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_dob_manual.'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_finAddress.'</span></td>
			</tr>
		
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_employer_manual.'</span></td>
			</tr>
			<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_occupation_manual.'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_finno_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_workpassissueddate_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di1_fin_workpassexpirydate_manual.'</span></td>
			</tr>';
	}

}

//director and shareholders contact details
if($di1_resstatus == 'dinonresident_1' || $di1_resstatus == 'dicitizenpr_1' || $di1_resstatus == 'dipassholder_1'){
		$di1_contact = '<tr class="row">
				<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$dihandphone.' <br>'.$diOfficephone.'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$dibusinessemail.' <br>'.$diPersonalemail.'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dipep.'</span></td>
			</tr>';
			
		if($dipep == 'Yes'){
		$di1_contact .= '<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP in which country</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dicountrypep.'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Role of PEP</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dirolePep.'</span></td>
			</tr>
			<tr>					 
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP since</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dipepFrom.' - '.$dipepTo.'</span></td>
			</tr>';
		}

		$di1_contact .= '</tbody>
				  	</table></td>
				<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
		  <tbody>
			 <tr>
				<td style="border-bottom:1px solid #ccc"><span>Beneficial Owner</span></td>
				<td style="border-bottom:1px solid #ccc"><span style="font-weight:bold">'.$di1_bowner.'</span></td>
			</tr>
			<tr>
				<td><span>Uploaded documents:</span></td>
				<td><span></span></td>
			</tr>
			<tr>								
				<td colspan="2" style="border-bottom:1px solid #ccc">
				<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/ep-back.jpg" class="img-fluid" alt=""></div>
			<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport.jpg" class="img-fluid" alt=""></div>
			<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/ep-front.jpg" class="img-fluid" alt=""></div></td>
			</tr>
			 <tr>
				<td><span>Proof of address</span></td>
				 <td><span style="font-weight:bold"><a href="https://www.winimy.ai/rikvinnew/pdf.php#">fred-utility-address.pdf</a></span></td>
			</tr>	
			</tbody>
		</table></td>
			</tr>
			  </tbody>
			</table>
		<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';

		$d1_listofshare = '
			<h3 style="margin:25px 0 10px 0">List of Shareholders</h3>				
			<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; width:100%;">
			<tbody style="border: 1px solid #e9ecef !important;">
			<tr class="table-secondary row">
				<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Shareholders</span></td>
				<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Paid up capital</span></td>
				<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>No. of shares</span></td>
				<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Beneficial owner</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$di1_surname.'</span></td>
				<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$paid_up_capital.'</span></td>
				<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$number_of_shares.'</span></td>
				<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$di1_bowner.'</span></td>
			</tr>';
}

//=================corporate shareholder part starts here======================//
//corporate shareholders non resident passport preview section
if($corporate1_resstatus == 'corporatenonresident_1'){
	if($corporate1_surname_preview != '' && $corporate1_surname_preview != 'null'){
	$mail_name = $corporate1_surname;
	$corporate1_nr_pass_preview = '<h4>Corporate Shareholder #1 - '.$corporate1_surname.'</h4>
	
	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of Incorporation</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country.'</span></td>
		</tr>		
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_certificate.' '.$corporate1_certificate1.'</span></td>
		</tr>
		<tr class="row">
			<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_representname.'</span></td>								
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_gender_preview.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nationality_preview.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_passport_preview.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country_preview.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_expirydate_preview.'</span></td>								
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_birthplace_preview.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_dob_preview.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_resaddress.'</span></td>
		</tr>';
	//corporate shareholder non resident passport manual section
	}elseif($corporate1_passport_manual != '' && $corporate1_passport_manual != 'null'){
	$mail_name = $corporate1_surname;
	$corporate1_nr_pass_manual = '<h4>Director and Shareholder #1 - '.$corporate1_surname.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of Incorporation</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country.'</span></td>
		</tr>		
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_certificate.' '.$corporate1_certificate1.'</span></td>
		</tr>
		<tr class="row">
			<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_representname.'</span></td>								
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_gender_manual.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nationality_manual.'</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_passport_manual.'</span></td>								
		</tr>
		  <tr>
		    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
		   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_dateissued_manual.'</span></td>								
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_expirydate_manual.'</span></td>								
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_birthplace_manual.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_dob_manual.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_resaddress.'</span></td>
		</tr>';
	}
}

//corporate shareholder nric preview section
if($corporate1_resstatus == 'corporatecitizenpr_1'){
	if($corporate1_nric_front_no != '' && $corporate1_nric_front_no != 'null'){
	$mail_name = $corporate1_surname;
	$corporate1_nric_preview = '<h4>Corporate Shareholder #1 - '.$corporate1_surname.'</h4>

	<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
	  <tbody>
		<tr>
			<td width="50%" valign="top">	
		<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
	  <tbody>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of Incorporation</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country.'</span></td>
		</tr>		
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_certificate.' '.$corporate1_certificate1.'</span></td>
		</tr>
		<tr class="row">
			<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_representname.'</span></td>								
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_front_gender.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_front_no.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_front_nation.'</span></td>
		</tr>
		<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_front_dob.'</span></td>
		</tr>
		<tr>
		 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_front_address.'</span></td>					
		</tr>';
	
	//director and shareholder nric manual
	}elseif($corporate1_nric_no_manual != '' && $corporate1_nric_no_manual != 'null'){
	$mail_name = $corporate1_surname;
	$corporate1_nric_manual = '<h4>Corporate Shareholder #1 - '.$corporate1_surname.'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Name</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_surname.'</span></td>
		</tr>
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of Incorporation</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country.'</span></td>
		</tr>		
		<tr>								
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_certificate.' '.$corporate1_certificate1.'</span></td>
		</tr>
		<tr class="row">
			<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
		</tr>
		<tr>
		 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
		  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_representname.'</span></td>								
		</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_no_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_nation_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_dob_manual.'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_nric_address_manual.'</span></td>					
			</tr>';
	}
}

//corporate shareholders fin part starts here
if($corporate1_resstatus == 'corporatepassholder_1'){

	if($corporate1_fin_front_finno != '' && $corporate1_fin_front_finno != 'null'){
		$mail_name = $corporate1_surname;			
		$corporate1_fin_preview = '<h4>Corporate Shareholder #1 - '.$corporate1_surname.'</h4>
		
			<table border="0" cellpadcorporateng="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadcorporateng="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_surname.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of Incorporation</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country.'</span></td>
				</tr>		
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_certificate.' '.$corporate1_certificate1.'</span></td>
				</tr>
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_representname.'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_gender_preview.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_nationality_preview.'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_passport_preview.'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_country_preview.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_expirydate_preview.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_dob_preview.'</span></td>								
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_finAddress.'</span></td>
				</tr>
		
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padcorporateng:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_front_employer.'</span></td>
				</tr>
				<tr>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Occupation</span></td>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_front_occupation.'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_front_finno.'</span></td>
				</tr>
				<tr>								
				<tr>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_front_issueddate.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_front_expirydate.'</span></td>
				</tr>';

	}elseif($corporate1_fin_passport_manual != '' && $corporate1_fin_passport_manual != 'null'){
		$mail_name = $corporate1_surname;
		$corporate1_fin_manual = '<h4>Corporate Shareholder #1 - '.$corporate1_surname.'</h4>
		
		<table border="0" cellpadcorporateng="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadcorporateng="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_surname.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of Incorporation</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_country.'</span></td>
			</tr>		
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_certificate.' '.$corporate1_certificate1.'</span></td>
			</tr>
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_representname.'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_gender_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_nationality_manual.'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_passport_manual.'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_country_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_expirydate_manual.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_dob_manual.'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_finAddress.'</span></td>
			</tr>
		
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padcorporateng:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_employer_manual.'</span></td>
			</tr>
			<tr>
			<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Occupation</span></td>
			 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_occupation_manual.'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_finno_manual.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_workpassissueddate_manual.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate1_fin_workpassexpirydate_manual.'</span></td>
			</tr>';
	}

}

//corporate shareholder contact details
if($corporate1_resstatus == 'corporatenonresident_1' || $corporate1_resstatus == 'corporatecitizenpr_1' || $corporate1_resstatus == 'corporatepassholder_1'){
	$corporate1_contact = '<tr class="row">
			<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_contact.'</span></td>
		</tr>
		<tr>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
			<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate1_email.'</span></td>
		</tr>
		</tbody>
	</table></td>
				<td colspan="2" width="50%" style="text-align:center"><div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-front.jpg" class="img-fluid" alt=""></div>
		<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-back.jpg" class="img-fluid" alt=""></div></td>
			</tr>
			</tbody>							
			</table>

	<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents NRIC front and back, nationality Singaporean)</p>';
}

// Set some content to print
$html = <<<EOD
<!DOCTYPE html>
<!-- saved from url=(0039)https://www.winimy.ai/rikvinnew/pdf.php -->
<html lang="en" class="gr__winimy_ai"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>InCorp - On Boarding Customer Due Diligence Form</title>
	<style>
  .row{
   background-color:#999999;
   -webkit-print-color-adjust: exact; 
   }
 </style>
</head>
	
<body data-gr-c-s-loaded="true" style="font-family: 'Roboto', sans-serif !important;">


	<div class="container" style="width:800px; margin:0 auto">
	
				<img src="https://www.winimy.ai/rikvinnew/images/logo.png" style="margin:25px 0">
					<h3 style="margin:25px 0">Customer Due Diligence Acceptance Form</h3>
					<p>Key definitions used in the document</p>
					<ul style="line-height:25px;">
						<li>A "Beneficial Owner", means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or exercises control over the management.</li>
						<li>A "Politcally Exposed Person" (PEP) is a term describing someone who has been entrusted with any prominent public function in Singapore.</li>
					</ul>
					
					<h4 style="border-bottom:2px solid #000; padding-bottom:10px;">Proposed Company Information</h4>
		
	
		<table  cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; border-bottom:none; width:100%;">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Proposed company name</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$proposedCompanyName1
EOD;
					$html .= $proposed_co_2;
					$html .= $proposed_co_3;
	
					$html .= <<<EOD
					</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Registered address of company</span></td>
EOD;

					$html .= $reg_co_address;

					$html .= <<<EOD
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Business activities</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$businessActivities</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Countries of operation</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$countriesOperation</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Source of Funds</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$sourceFunds</span></td>
					</tr>
				</tbody>
			</table>
		
					<h4 class="mb-3 mt-5 font-weight-bold">Particulars of all Directors, Shareholders and Beneficial Owners</h4>
EOD;

				$html .= $director1_nr_pass_preview;

				$html .= $director1_nr_pass_manual;

				$html .= $director1_nric_preview;	
				
				$html .= $director1_nric_manual;	

				$html .= $director1_fin_preview;

				$html .= $director1_fin_manual;
					
				$html .= $director1_contact;	
					
				$html .= $shareholder1_nr_pass_preview;
					
				$html .= $shareholder1_nr_pass_manual;
					
				$html .= $shareholder1_nric_preview;
					
				$html .= $shareholder1_nric_manual;
					
				$html .= $shareholder1_fin_preview;

				$html .= $shareholder1_fin_manual;

				$html .= $shareholder1_contact;

				$html .= $di1_nr_pass_preview;

				$html .= $di1_nr_pass_manual;

				$html .= $di1_nric_preview;

				$html .= $di1_nric_manual;

				$html .= $di1_fin_preview;

				$html .= $di1_fin_manual;

				$html .= $di1_contact;

				$html .= $corporate1_nr_pass_preview;

				$html .= $corporate1_nr_pass_manual;

				$html .= $corporate1_nric_preview;

				$html .= $corporate1_nric_manual;

				$html .= $corporate1_fin_preview;

				$html .= $corporate1_fin_manual;

				$html .= $corporate1_contact;
					
				$html .= <<<EOD
					<br>
				<hr style="background: #666; height: 2px; width: 100%; margin:25px 0 0 0;">
				<br><br>
					<h3 style="margin:25px 0 10px 0">Shareholding Structure</h3>				
					<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; width:100%;">
						<tbody>
							<tr>
								<td style="border-right:1px solid #ccc"><span>Currency</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">$currency</span></td>
								<td style="border-right:1px solid #ccc"><span>Paid Up capital</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">$paid_up_capital</span></td>
								<td style="border-right:1px solid #ccc"><span>No. of shares</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">$number_of_shares</span></td>
							</tr>							
						</tbody>
					</table>
				
			
				
EOD;
		
					//$html .= $d1_listofshare; 													
						$html .= <<<EOD
						</tbody>
					</table>
				
				<br>
				
					<h3 class="mb-5" style="border-bottom:2px solid #000; display: block; width: 100%; margin:35px 0 0 0;
					padding-bottom: 10px;">Declaration</h3>
					<table width="100%">
						<p>I declare that the information provided in this form is true and correct. I am aware that I may be subject to prosecution if I am found to have made any false statement which I know to be false or intentionally suppressed any material fact.</p>
					</table>
				<table width="100%">
						<tbody>
							<tr>
								<td width="270"><span>Name of Customer/Agent</span></td>
								<td><span style="font-weight:bold">$agent_full_name</span></td>
							</tr>					
						</tbody>
					</table>
					
			
	<br>
</body></html>
EOD;


// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------

// Close and output PDF document
// This method has several options, check the source code documentation for more information.
ob_end_clean();
//$path = '/var/www/html/rikvinnew/pdftest/';
$ro = $_SERVER['DOCUMENT_ROOT'].'rikvinnew/pdftest/';
$doc = $pdf->Output($ro . 'incorp.pdf', 'F');

// Settings
$name        = "In Corp - Rikvin";
$email       = "rikvin.marketing@gmail.com";
$to          = "$name <$email>";
$fromEmailAddress        = "digitalmarketing@rikvin.com";
$fromEmailName = "Tech Team";
$ccEmailName1 = "Winimy Tech";
$ccEmailAddress1 = "tech@winimy.com";
$ccEmailName2 = "Bhargavi";
$ccEmailAddress2 = "bhargavi@rikvin.com";

$subject     = "InCorp PDF";
$mainMessage = "Thank you for filling out (".$proposedCompanyName1.") information!. \n\n We’ve sent you an email with the information that you just filled at the email address. Our relationship manager will get over the details and will get back to you, if any more documents are required.";
$fileatt     = $ro."incorp.pdf"; //file location
//$fileatt = $pdf->Output('incorp.pdf', 'E');
$fileatttype = "application/pdf";
$fileattname = "incorp.pdf"; //name that you want to use to send or you can use the same name

$headers = "From: " . $fromEmailName . " <" . $fromEmailAddress . ">\n";
$headers.= "cc: " . $ccEmailName1 . " <" . $ccEmailAddress1 . ">\n";
$headers.= "cc: " . $ccEmailName2 . " <" . $ccEmailAddress2 . ">";

// File
$file = fopen($fileatt, 'rb');
$data = fread($file, filesize($fileatt));
fclose($file);

// This attaches the file
$semi_rand     = md5(time());
$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x";
$headers      .= "\nMIME-Version: 1.0\n" .
  "Content-Type: multipart/mixed;\n" .
  " boundary=\"{$mime_boundary}\"";
  $message = "This is a multi-part message in MIME format.\n\n" .
  "--{$mime_boundary}\n" .
  "Content-Type: text/plain; charset=\"iso-8859-1\n" .
  "Content-Transfer-Encoding: 7bit\n\n" .
  $mainMessage  . "\n\n";

$data = chunk_split(base64_encode($data));
$message .= "--{$mime_boundary}\n" .
  "Content-Type: {$fileatttype};\n" .
  " name=\"{$fileattname}\"\n" .
  "Content-Disposition: attachment;\n" .
  " filename=\"{$fileattname}\"\n" .
  "Content-Transfer-Encoding: base64\n\n" .
$data . "\n\n" .
 "--{$mime_boundary}--\n";

// Send the email
if(mail($to, $subject, $message, $headers)) {

  echo "The email was sent.";
  unlink($fileatt);
  header("Location: https://winimy.ai/rikvinnew/thankyou.php");	
}
else {

  echo "There was an error sending the mail.";
unlink($fileatt);
}


//============================================================+
// END OF FILE
//============================================================+
