<?php
session_start();
$error=array();
$extension=array("jpeg","jpg","png","gif","PDF","pdf");

$_SESSION['id'] = session_id();
if(!is_dir("directordocuments/")) {
	mkdir("directordocuments/");
}
if(!is_dir("directordocuments/". $_SESSION["id"] ."/")) {
	mkdir("directordocuments/". $_SESSION['id'] ."/");
}
// Upload section for director nonresident

if($_FILES["directornonresidentuploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["directornonresidentuploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directornonresidentuploadPassport"]["name"][$key];
	$file_tmp = $_FILES["directornonresidentuploadPassport"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{		    
	
		    if(!file_exists("directordocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for director non resident address

if($_FILES["directornonresidentaddress"]["tmp_name"] != '') {

	foreach($_FILES["directornonresidentaddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directornonresidentaddress"]["name"][$key];
	$file_tmp = $_FILES["directornonresidentaddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("directordocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for director fin passport

if($_FILES["directoruploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["directoruploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directoruploadPassport"]["name"][$key];
	$file_tmp = $_FILES["directoruploadPassport"]["tmp_name"][$key];
	$ext = pathinfo($file_name,PATHINFO_EXTENSION);

		if(in_array($ext,$extension))
		{
		    if(!file_exists("directordocuments/". $_SESSION['id'] .$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for director fin front

if($_FILES["directorfincardfront"]["tmp_name"] != '') {

	foreach($_FILES["directorfincardfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directorfincardfront"]["name"][$key];
	$file_tmp = $_FILES["directorfincardfront"]["tmp_name"][$key];
	$ext = pathinfo($file_name,PATHINFO_EXTENSION);

		if(in_array($ext,$extension))
		{
		    if(!file_exists("directordocuments/". $_SESSION['id'] .$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for director fin back
if($_FILES["directorfincardback"]["tmp_name"] != '') {

	foreach($_FILES["directorfincardback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directorfincardback"]["name"][$key];
	$file_tmp = $_FILES["directorfincardback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("directordocuments/".$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for director address

if($_FILES["directoraddress"]["tmp_name"] != '') {

	foreach($_FILES["directoraddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directoraddress"]["name"][$key];
	$file_tmp = $_FILES["directoraddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("directordocuments/".$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for director NRIC front

if($_FILES["directornricfront"]["tmp_name"] != '') {

	foreach($_FILES["directornricfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directornricfront"]["name"][$key];
	$file_tmp = $_FILES["directornricfront"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("directordocuments/".$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for director NRIC back

if($_FILES["directornricback"]["tmp_name"] != '') {

	foreach($_FILES["directornricback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["directornricback"]["name"][$key];
	$file_tmp = $_FILES["directornricback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("directordocuments/".$file_name))
		    {
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"directordocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}
?>

