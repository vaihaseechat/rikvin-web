<script>
function storedata(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);

//Storing corporate Non Resident

if(getresidencystatus == 'corporatenonresident'){

var corporatenonresidentsurname = document.getElementById("corporatenonresidentsurname_"+getcounter).value;
var corporatenonresidentgivenname = document.getElementById("corporatenonresidentgivenName_"+getcounter).value;
var corporatenonresidentpassportno = document.getElementById("corporatenonresidentpassportNumber_"+getcounter).value;
var corporatenonresidentnationality = $('#corporatenonresidentnationality_'+getcounter+'_chosen span').text();
var corporatenonresidentcountry = $('#corporatenonresidentcountry_'+getcounter+'_chosen span').text();
var corporatenonresidentgender = $("input[name='corporatenonresidentpgender[]']:checked").val();
var corporatenonresidentdatetimepicker = document.getElementById("corporatenonresidentpdatetimepicker_"+getcounter).value;
var corporatenonresidentdateexpiry = document.getElementById("corporatenonresidentpdateexpiry_"+getcounter).value;
var corporatenonresidentdob = document.getElementById("corporatenonresidentpdob_"+getcounter).value;
var corporatenonresidentplaceofBirth = $('#corporatenonresidentplaceofBirth_'+getcounter+'_chosen span').text();

//alert(corporatenonresidentdob);

localStorage.setItem("corporatenonresidentpopupsurname_"+getcounter,corporatenonresidentsurname);
localStorage.setItem("corporatenonresidentpopupgivenName_"+getcounter,corporatenonresidentgivenname);
localStorage.setItem("corporatenonresidentpopuppassportno_"+getcounter,corporatenonresidentpassportno);
localStorage.setItem("corporatenonresidentpopupnationality_"+getcounter,corporatenonresidentnationality);
localStorage.setItem("corporatenonresidentpopupcountry_"+getcounter,corporatenonresidentcountry);
localStorage.setItem("corporatenonresidentpopupgender_"+getcounter,corporatenonresidentgender);
localStorage.setItem("corporatenonresidentpopupdatetimepicker_"+getcounter,corporatenonresidentdatetimepicker);
localStorage.setItem("corporatenonresidentpopupdateexpiry_"+getcounter,corporatenonresidentdateexpiry);
localStorage.setItem("corporatenonresidentpopupplaceofBirth_"+getcounter,corporatenonresidentplaceofBirth);
localStorage.setItem("corporatenonresidentpopupdob_"+getcounter,corporatenonresidentdob);

	if(corporatenonresidentsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
	else if(corporatenonresidentgivenname == ''){

           //alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
           $("#displaysuccessmsg").modal("show");   

	}
	else if(corporatenonresidentpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show");  

	}
	else if(corporatenonresidentnationality == ''){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(corporatenonresidentcountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenonresidentgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenonresidentdatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenonresidentdateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	} 
        else if(corporatenonresidentdob == ''){
           
              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	} 
        else if(corporatenonresidentplaceofBirth == ''){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}    
	else{
	   //alert("Saved Succesfully");
	   $('#corporatenonresidentpreviewpassport_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}
}

if(getresidencystatus == 'corporatenricfront'){

var corporatepreviewfrontnricnumber = document.getElementById("corporatepreviewfrontnricnumber_"+getcounter).value;
var corporatepreviewnricfrontgender = $("input[name='corporatepreviewnricfrontgender[]']:checked").val();
var corporatepreviewnricfrontdob = document.getElementById("corporatepreviewnricfrontdob_"+getcounter).value;
var corporatepreviewnricfrontnationality = $('#corporatepreviewnricfrontnationality_'+getcounter+'_chosen span').text();
var corporatepreviewnricfrontpostcode = document.getElementById("corporatepreviewnricfrontpostcode_"+getcounter).value;
var corporatepreviewnricfrontstreetname = document.getElementById("corporatepreviewnricfrontstreetname_"+getcounter).value;
var corporatepreviewnricfrontfloor = document.getElementById("corporatepreviewnricfrontfloor_"+getcounter).value;
var corporatepreviewnricfrontunit = document.getElementById("corporatepreviewnricfrontunit_"+getcounter).value;

localStorage.setItem("corporatepreviewpopupfrontnricnumber_"+getcounter,corporatepreviewfrontnricnumber);
localStorage.setItem("corporatepreviewpopupnricfrontgender_"+getcounter,corporatepreviewnricfrontgender);
localStorage.setItem("corporatepreviewpopupnricfrontdob_"+getcounter,corporatepreviewnricfrontdob);
localStorage.setItem("corporatepreviewpopupnricfrontnationality_"+getcounter,corporatepreviewnricfrontnationality);
localStorage.setItem("corporatepreviewpopupnricfrontpostcode_"+getcounter,corporatepreviewnricfrontpostcode);
localStorage.setItem("corporatepreviewpopupnricfrontstreetname_"+getcounter,corporatepreviewnricfrontstreetname);
localStorage.setItem("corporatepreviewpopupnricfrontfloor_"+getcounter,corporatepreviewnricfrontfloor);
localStorage.setItem("corporatepreviewnrpopupicfrontunit_"+getcounter,corporatepreviewnricfrontunit);


        if(corporatepreviewfrontnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
	else if(corporatepreviewnricfrontgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show");  

	}
	else if(corporatepreviewnricfrontdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");  

	}
	else if(corporatepreviewnricfrontnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show");    

	}
        else if(corporatepreviewnricfrontpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatepreviewnricfrontstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatepreviewnricfrontfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatepreviewnricfrontunit == ''){
           
              //alert("Unit cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
           $("#displaysuccessmsg").modal("show");  

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#corporatenricfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");  
	}


}

if(getresidencystatus == 'corporatenricback'){

var corporatepreviewbacknricnumber = document.getElementById("corporatepreviewbacknricnumber_"+getcounter).value;
var corporatepreviewnricbackgender = $("input[name='corporatepreviewnricbackgender[]']:checked").val();
var corporatepreviewnricbackdob = document.getElementById("corporatepreviewnricbackdob_"+getcounter).value;
var corporatepreviewnricbacknationality = $('#corporatepreviewnricbacknationality_'+getcounter+'_chosen span').text();
var corporatepreviewnricbackpostcode = document.getElementById("corporatepreviewnricbackpostcode_"+getcounter).value;
var corporatepreviewnricbackstreetname = document.getElementById("corporatepreviewnricbackstreetname_"+getcounter).value;
var corporatepreviewnricbackfloor = document.getElementById("corporatepreviewnricbackfloor_"+getcounter).value;
var corporatepreviewnricbackunit = document.getElementById("corporatepreviewnricbackunit_"+getcounter).value;

localStorage.setItem("corporatepreviewpopupbacknricnumber_"+getcounter,corporatepreviewbacknricnumber);
localStorage.setItem("corporatepreviewpopupnricbackgender_"+getcounter,corporatepreviewnricbackgender);
localStorage.setItem("corporatepreviewpopupnricbackdob_"+getcounter,corporatepreviewnricbackdob);
localStorage.setItem("corporatepreviewpopupnricbacknationality_"+getcounter,corporatepreviewnricbacknationality);
localStorage.setItem("corporatepreviewpopupnricbackpostcode_"+getcounter,corporatepreviewnricbackpostcode);
localStorage.setItem("corporatepreviewpopupnricbackstreetname_"+getcounter,corporatepreviewnricbackstreetname);
localStorage.setItem("corporatepreviewpopupnricbackfloor_"+getcounter,corporatepreviewnricbackfloor);
localStorage.setItem("corporatepreviewnrpopupicbackunit_"+getcounter,corporatepreviewnricbackunit);

        if(corporatepreviewbacknricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
	else if(corporatepreviewnricbackgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show");  

	}
	else if(corporatepreviewnricbackdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");  

	}
	else if(corporatepreviewnricbacknationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show");    

	}
        else if(corporatepreviewnricbackpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatepreviewnricbackstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatepreviewnricbackfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatepreviewnricbackunit == ''){
           
              //alert("Unit cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
           $("#displaysuccessmsg").modal("show");  

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#corporatenricbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");  
	}


}
if(getresidencystatus == 'corporatefinpassport'){


var corporatefincardsurname = document.getElementById("corporatefincardsurname_"+getcounter).value;
var corporatefincardgivenname = document.getElementById("corporatefincardgivenName_"+getcounter).value;
var corporatefincardpassportno = document.getElementById("corporatefincardppassportNumber_"+getcounter).value;
var corporatefincardnationality = $('#corporatefincardpnationality_'+getcounter+'_chosen span').text();
var corporatefincardcountry = $('#corporatefincardpcountry_'+getcounter+'_chosen span').text();
var corporatefincardgender = $("input[name='corporatefincardgender[]']:checked").val();
var corporatefincarddatetimepicker = document.getElementById("corporatefincardpdatetimepicker_"+getcounter).value;
var corporatefincarddateexpiry = document.getElementById("corporatefincardpdateexpiry_"+getcounter).value;
var corporatefincardplaceofBirth = $('#corporatefincardplaceofBirth_'+getcounter+'_chosen span').text();
var corporatefincarddob = document.getElementById("corporatefincardpdob_"+getcounter).value;
//alert(corporatefincardnationality);return false;

//alert(corporatesurname);

localStorage.setItem("corporatefincardpopupsurname_"+getcounter,corporatefincardsurname);
localStorage.setItem("corporatefincardpopupgivenName_"+getcounter,corporatefincardgivenname);
localStorage.setItem("corporatefincardpopuppassportno_"+getcounter,corporatefincardpassportno);
localStorage.setItem("corporatefincardpopupnationality_"+getcounter,corporatefincardnationality);
localStorage.setItem("corporatefincardpopupcountry_"+getcounter,corporatefincardcountry);
localStorage.setItem("corporatefincardpopupgender_"+getcounter,corporatefincardgender);
localStorage.setItem("corporatefincardpopupdatetimepicker_"+getcounter,corporatefincarddatetimepicker);
localStorage.setItem("corporatefincardpopupdateexpiry_"+getcounter,corporatefincarddateexpiry);
localStorage.setItem("corporatefincardpopupplaceofBirth_"+getcounter,corporatefincardplaceofBirth);
localStorage.setItem("corporatefincardpopupdob_"+getcounter,corporatefincarddob);

        if(corporatefincardsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(corporatefincardgivenname == ''){

           //alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatefincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatefincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(corporatefincardcountry == ''){

            //alert("Country cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatefincardgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatefincarddatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
              $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatefincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	} 
        else if(corporatefincardplaceofBirth == ''){

              //alert("Place of Birth cannot be empty"); 
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}    
	else{
	   //alert("Saved Succesfully");
	   $('#corporatefincardpreviewpassport_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == 'corporatefincardfront'){

var corporatefincardfrontemployer = document.getElementById("corporatefincardfrontemployer_"+getcounter).value;
var corporatefincardfrontoccupation = document.getElementById("corporatefincardfrontoccupation_"+getcounter).value;
var corporatefincardfrontdateissue = document.getElementById("corporatefincardfrontdateissue_"+getcounter).value;
var corporatefincardfrontdateexpiry = document.getElementById("corporatefincardfrontdateexpiry_"+getcounter).value;
var corporatewfincardfrontNumber = document.getElementById("corporatewfincardfrontNumber_"+getcounter).value;

localStorage.setItem("corporatefincardfrontemployer_"+getcounter,corporatefincardfrontemployer);
localStorage.setItem("corporatefincardfrontoccupation_"+getcounter,corporatefincardfrontoccupation);
localStorage.setItem("corporatefincardfrontdateissue_"+getcounter,corporatefincardfrontdateissue);
localStorage.setItem("corporatefincardfrontdateexpiry_"+getcounter,corporatefincardfrontdateexpiry);
localStorage.setItem("corporatewfincardfrontNumber_"+getcounter,corporatewfincardfrontNumber);

        if(corporatefincardfrontemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(corporatefincardfrontoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatefincardfrontdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatefincardfrontdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(corporatewfincardfrontNumber == ''){

            //alert("Fin Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#corporatefincardfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == 'corporatefincardback'){

var corporatefincardbackemployer = document.getElementById("corporatefincardbackemployer_"+getcounter).value;
var corporatefincardbackoccupation = document.getElementById("corporatefincardbackoccupation_"+getcounter).value;
var corporatefincardbackdateissue = document.getElementById("corporatefincardbackdateissue_"+getcounter).value;
var corporatefincardbackdateexpiry = document.getElementById("corporatefincardbackdateexpiry_"+getcounter).value;
var corporatewfincardbackNumber = document.getElementById("corporatewfincardbackNumber_"+getcounter).value;

localStorage.setItem("corporatefincardbackemployer_"+getcounter,corporatefincardbackemployer);
localStorage.setItem("corporatefincardbackoccupation_"+getcounter,corporatefincardbackoccupation);
localStorage.setItem("corporatefincardbackdateissue_"+getcounter,corporatefincardbackdateissue);
localStorage.setItem("corporatefincardbackdateexpiry_"+getcounter,corporatefincardbackdateexpiry);
localStorage.setItem("corporatewfincardbackNumber_"+getcounter,corporatewfincardbackNumber);

        if(corporatefincardbackemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(corporatefincardbackoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatefincardbackdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatefincardbackdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(corporatewfincardbackNumber == ''){

            //alert("Fin Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#corporatefincardbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate FIN  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

}
</script>
