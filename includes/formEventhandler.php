<?php 
/**/
?>
<script type="text/javascript">
$(document).on('change keyup paste', 'input[name="proposedCompanyName"]', function(e){
	if (this.value.trim() != "") {
		showProposedCompanyEmpty(false);	
	}
});
$("#postal-code").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showOwnAddressInvalid("", false);		
	}
});
$("#floor").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showOwnAddressInvalid("", false);		
	}
});
/*
$(document).on('change keyup paste', 'input[name="intendedAddress"]', function(e){
	if (this.value.trim() != "") {
		showProposedCompanyAddressEmpty(false);	
	}
});
$("#postal-code").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showOwnAddressEmpty("", false);		
	}
});

$("#street-name").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showOwnAddressEmpty("", false);		
	}
});
$("#floor").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showOwnAddressEmpty("", false);		
	}
});
$("#unit").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showOwnAddressEmpty("", false);		
	}
});

$(document).on('change keyup paste', 'textarea[name="businessActivities"]', function(e){
	if (this.value.trim() != "") {
		showBussinessActivitiesEmpty(false);	
	}
});

$("#countriesOperation").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showCountriesOperationEmpty(false);	
	}
});
*/

/*Role*/
$(document).on('change keyup paste', 'select[name="roleSelector"]', function(e){
	if (this.value.trim() != "") {
		showRoleSelectorEmpty(false);	
	}
});

/*Role*/
$("#addroleSelector").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showAddRoleSelectorEmpty(false);	
	}
});


function startEventsDirector(formNo){
	/*Director*/
	/*Full Name*/
	$("#surname_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showFullNameEmpty(ROLE_DIRECTOR, formNo, false);	
		}
	});
	/*Current Residency Status*/
	$("#directorresstatus_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showResStatusEmpty(ROLE_DIRECTOR, formNo, false);	
		}
	});

	
	/*Nonresident*/
	/*Upload Passpor*/
	$("#directornonresidentuploadpassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPassportEmpty(ROLE_DIRECTOR, formNo, false);	
		showPassportDataEmpty(ROLE_DIRECTOR, formNo, false);
		showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, false);
	}
	});
	
	
	$("#directorbuttonpassportmanually_"+formNo).on('click', function(e){
		
		showPassportDataEmpty(ROLE_DIRECTOR, formNo, false);
		showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, false);	
	
	});
	
	$("#bdirectornonresidentpreviewpassport_"+formNo).on('click', function(e){
		
		showPassportDataEmpty(ROLE_DIRECTOR, formNo, false);
		showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, false);	
	
	});
	
	$("#directorbuttonnricmanually_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			showNRICDataEmpty(ROLE_DIRECTOR, formNo,true, false);
			showNRICDataEmpty(ROLE_DIRECTOR, formNo,false, false);
			
	
	});
	
	$("#bdirectornricfrontpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			showNRICDataEmpty(ROLE_DIRECTOR, formNo,true, false);
			showNRICDataEmpty(ROLE_DIRECTOR, formNo,false, false);
			
	});
	$("#bdirectornricbackpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			showNRICDataEmpty(ROLE_DIRECTOR, formNo,true, false);
			showNRICDataEmpty(ROLE_DIRECTOR, formNo,false, false);
			
	});
	
	$("#directorbuttonfincardmanually_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			
	});
	
	$("#bdirectorfincardpreviewpassport_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			
	});
	
	$("#bdirectorfincardfrontpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			
	});
	
	$("#bdirectorfincardbackpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, false);
	});
	
	
	/*Upload proof of address*/
	$("#directornonresidentaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressProofEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});
	/*Upload proof of address - textarea*/
	$("#directornonresidentresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});

	/*SG*/
	/*SG Upload NRIC - front*/
	$("#directornricfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_DIRECTOR, formNo, true, false);	
	}
	});
	/*Upload NRIC - back*/
	$("#directornricback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_DIRECTOR, formNo, false, false);	
	}
	});
	/*FIN*/
	/*Upload Passport*/
	
	$("#directoruploadPassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showFinPassportEmpty(ROLE_DIRECTOR, formNo, false);
		}
	});
	
	/*Upload FIN - front*/
	
	$("#directorfincardfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_DIRECTOR, formNo, true, false);	
	}
	});
	
	/*Upload FIN - back*/
	
	$("#directorfincardback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_DIRECTOR, formNo, false, false);	
	}
	});
	
	/*Upload proof of address*/
	$("#directoraddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressProofEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});
	/*Upload proof of address - textarea*/
	$("#directorresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});


	/*Contact Numbers - hand*/
	$("#directorhandphone_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showContactNumbersEmpty(ROLE_DIRECTOR, formNo, false);
		showOfficeNumberInvalid(ROLE_DIRECTOR, formNo, false);	
	}
	});

	/*Contact Numbers - office*/
	/*
	$("offnumber_1").on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		(ROLE_DIRECTOR, formNo, false);	
	}
	});
	*/

	/*Email Address - biz*/
	$("#directorbusinessemail_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBizEmailEmpty(ROLE_DIRECTOR, formNo, false);
		showPersonalEmailInvalidFormat(ROLE_DIRECTOR, formNo, false);	
	}
	});
	/*Office Number*/
	$("#offnumber_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showContactNumbersEmpty(ROLE_DIRECTOR, formNo, false);
		showOfficeNumberInvalid(ROLE_DIRECTOR, formNo, false);		
		}
	});
	/*PersonalEmail Address*/
	$("#personalemail_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showBizEmailEmpty(ROLE_DIRECTOR, formNo, false);
			showPersonalEmailInvalidFormat(ROLE_DIRECTOR, formNo, false);		
		}
	});
	/*Email Address - personal*/
	/*
	$("directorresstatus_1").on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showResStatusEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});
	*/
	/*PEP in which country*/
	$("#countrypep_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepCountryEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});
	/*Role of PEP*/
	$("#rolePep_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepRoleEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});
	/*PEP since - from*/
	$("#pepFrom_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepFromEmpty(ROLE_DIRECTOR, formNo, false);
		showPepToEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});
	/*PEP since - to*/
	$("#pepTo_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepFromEmpty(ROLE_DIRECTOR, formNo, false);
		showPepToEmpty(ROLE_DIRECTOR, formNo, false);	
	}
	});

}

function startEventsShareHolder(formNo) {

	/*Individual Shareholder*/

	/*Full Name*/
	$("#shareholdersurname_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFullNameEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Are you the Beneficial Owner*/
	/*
	$("").on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		(ROLE_SHAREHOLDER, formNo, true);		
	}
	});
	*/
	/*Current Residency Status*/
	$("#shareholderresstatus_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showResStatusEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload Passport*/
	$("#shareholdernonresidentuploadpassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPassportEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	
	
	
	$("#shareholderbuttonpassportmanually_"+formNo).on('click', function(e){
		showPassportDataEmpty(ROLE_SHAREHOLDER, formNo, false);
		showPassportDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);	
	});
	
	$("#bshareholdernonresidentpreviewpassport_"+formNo).on('click', function(e){
		showPassportDataEmpty(ROLE_SHAREHOLDER, formNo, false);
		showPassportDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);	
	});
	
	$("#shareholderbuttonnricmanually_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_SHAREHOLDER, formNo,false, false);
			
	});
	
	$("#bshareholdernricfrontpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_SHAREHOLDER, formNo,false, false);
			
	});
	$("#bshareholdernricbackpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_SHAREHOLDER, formNo,false, false);
			
	});
	
	$("#shareholderbuttonfincardmanually_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bshareholderfincardpreviewpassport_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bshareholderfincardfrontpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bshareholderfincardbackpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_SHAREHOLDER, formNo, false);
	});
	
	
	
	/*Upload proof of address*/
	$("#shareholdernonresidentaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressProofEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload proof of address - textarea*/
	$("#shareholdernonresidentresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload NRIC - front*/
	$("#shareholdernricfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_SHAREHOLDER, formNo, true, false);		
	}
	});
	/*Upload NRIC - back*/
	$("#shareholdernricback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_SHAREHOLDER, formNo, false, false);		
	}
	});

	/*Upload Passport*/
	
	$("#shareholderuploadPassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showFinPassportEmpty(ROLE_SHAREHOLDER, formNo, false);
		}
	});

	/*FIN - front*/
	$("#shareholderfincardfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_SHAREHOLDER, formNo, true, false);		
	}
	});
	/*FIN - back*/
	$("#shareholderfincardback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_SHAREHOLDER, formNo, false, false);		
	}
	});
	/*Upload proof of address*/
	$("#shareholderaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressProofEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload proof of address - textarea*/
	$("#shareholderresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Contact Numbers*/
	$("#shareholderhandphone_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showContactNumbersEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Email Address*/
	$("#shareholderbusinessemail_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBizEmailEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Office Number*/
	$("#shareholderoffnumber_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showContactNumbersEmpty(ROLE_SHAREHOLDER, formNo, false);
		showOfficeNumberInvalid(ROLE_SHAREHOLDER, formNo, false);		
		}
	});
	/*PersonalEmail Address*/
	$("#shareholderpersonalemail_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showBizEmailEmpty(ROLE_SHAREHOLDER, formNo, false);
			showPersonalEmailInvalidFormat(ROLE_SHAREHOLDER, formNo, false);		
		}
	});
	/*PEP*/
	/*
	$("").on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		(ROLE_SHAREHOLDER, formNo, true);		
	}
	});
	*/
	/*PEP in which country*/
	$("#shareholdercountrypep_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepCountryEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Role of PEP*/
	$("#shareholderrolePep_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepRoleEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*PEP since - from*/
	$("#shareholderpepFrom_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepFromEmpty(ROLE_SHAREHOLDER, formNo, false);
		showPepToEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
	/*PEP since - to*/
	$("#shareholderpepTo_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepFromEmpty(ROLE_SHAREHOLDER, formNo, false);
		showPepToEmpty(ROLE_SHAREHOLDER, formNo, false);		
	}
	});
}


function startEventsDirectorShareholder(formNo) {

	/*Director and Individual Shareholder*/

	/*Full Name*/
	$("#dishareholdersurname_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFullNameEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*Are you the Beneficial Owner*/
	/*
	$("").on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);		
	}
	});
	*/
	/*Current Residency Status*/
	$("#dishareholderresstatus_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showResStatusEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload Passport*/
	$("#dinonresidentuploadpassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPassportEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	
	
	$("#dibuttonpassportmanually_"+formNo).on('click', function(e){
		showPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
		showPassportDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);	
	});
	
	$("#bdinonresidentpreviewpassport_"+formNo).on('click', function(e){
		showPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
		showPassportDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);	
	});
	
	$("#dibuttonnricmanually_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, false);
			
	});
	
	$("#bdinricfrontpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, false);
			
	});
	$("#bdinricbackpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, false);
			
	});
	
	$("#dibuttonfincardmanually_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bdifincardpreviewpassport_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bdifincardfrontpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bdifincardbackpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
	});
	
	
	
	
	
	/*Upload proof of address*/
	$("#dinonresidentaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressProofEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload proof of address - textarea*/
	$("#dinonresidentresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload NRIC - front*/
	$("#dinricfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true, false);		
	}
	});
	/*Upload NRIC - back*/
	$("#dinricback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false, false);		
		}
	});
	/*Upload Passport*/
	
	$("#diuploadPassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showFinPassportEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
		}
	});

	/*FIN - front*/
	$("#difincardfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true, false);		
	}
	});
	/*FIN - back*/
	$("#difincardback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false, false);		
	}
	});


	/*Upload proof of address*/
	$("#diaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressProofEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload proof of address - textarea*/
	$("#diresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*Contact Numbers*/
	$("#dishareholderhandphone_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showContactNumbersEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
			showOfficeNumberInvalid(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
		}
	});
	
	/*Email Address*/
	$("#dishareholderbusinessemail_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showBizEmailEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
		}
	});
	/*Office Number*/
	$("#dishareholderoffnumber_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showContactNumbersEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
		showOfficeNumberInvalid(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
		}
	});
	/*PersonalEmail Address*/
	$("#dishareholderpersonalemail_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showBizEmailEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
			showPersonalEmailInvalidFormat(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
		}
	});
	
	/*PEP*/
	/*
	$("").on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);		
	}
	});
	*/
	/*PEP in which country*/
	$("#dishareholdercountrypep_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepCountryEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*Role of PEP*/
	$("#dishareholderrolePep_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepRoleEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});
	/*PEP since - from*/
	$("#dishareholderpepFrom_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepFromEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);	
		showPepToEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);	
	}
	});
	/*PEP since - to*/
	$("#dishareholderpepTo_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPepFromEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);
		showPepToEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false);		
	}
	});

}

function startEventsCorporateShareholder(formNo) {

	/*Corporate Shareholder*/
	/*Name of Corporate Shareholder*/
	$("#corporatename_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFullNameEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Country of Incorporation*/
	$("#corporatecountry_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showCountryEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});




	/*Current Residency Status*/
	$("#corporateresstatus_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showResStatusEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload Passport*/
	$("#corporatenonresidentuploadpassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showPassportEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});
	
	$("#corporatebuttonpassportmanually_"+formNo).on('click', function(e){
		showPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
		showPassportDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);	
	});
	
	$("#bcorporatenonresidentpreviewpassport_"+formNo).on('click', function(e){
		showPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
		showPassportDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);	
	});
	
	$("#corporatebuttonnricmanually_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, false);
			
	});
	
	$("#bcorporatenricfrontpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, false);
			
	});
	$("#bcorporatenricbackpreview_"+formNo).on('click', function(e){
			
			showNRICDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true, false);
			showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, false);
			
	});
	
	$("#corporatebuttonfincardmanually_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bcorporatefincardpreviewpassport_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bcorporatefincardfrontpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			
	});
	
	$("#bcorporatefincardbackpreview_"+formNo).on('click', function(e){
			
			showFINDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true, false);
			showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, false);
			showFinPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
			showPassportDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
	});
	
	
	
	
	
	
	
	
	/*Upload proof of address*/
	$("#corporatenonresidentaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressProofEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload proof of address - textarea*/
	$("#corporatenonresidentresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showAddressEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Contact Numbers*/
	$("#corporatecontactnumber_"+formNo).on('change keyup paste', function(e){
		/*if (this.value.trim() != "") {*/
		showContactNumbersInvalid(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
		/*}*/
	});
	/*Email Address*/
	$("#corporateemail_"+formNo).on('change keyup paste', function(e){
		/*if (this.value.trim() != "") {*/
		showBizEmailInvalidFormat(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
		/*}*/
	});
	/*Upload NRIC - front*/
	$("#corporatenricfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true, false);		
	}
	});
	/*Upload NRIC - back*/
	$("#corporatenricback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showNRICEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false, false);		
	}
	});
	/*Upload Passport*/
	
	$("#corporateuploadPassport_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showFinPassportEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);
		}
	});

	/*FIN - front*/
	$("#corporatefincardfront_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true, false);		
	}
	});
	/*FIN - back*/
	$("#corporatefincardback_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFINCardEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false, false);		
	}
	});


	/*Upload proof of address*/
	$("#corporateaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressProofEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});
	/*Upload proof of address - textarea*/
	$("#corporateresaddress_"+formNo).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showFinAddressEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false);		
	}
	});
}





</script>
