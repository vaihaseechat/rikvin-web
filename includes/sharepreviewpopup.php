<script>
var parsecount = 20;
function shareholdernonuploadpassportfile(getdata,getcounter,filename){

$("#shareuploadpassportfileloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
//alert(filename);

        $.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error; 


                         if(Errorstatus == 'Poor Image Quality') {                           
                           
                           $("#displaysuccessmsg .modal-title").html("Shareholder - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
                           $("#bshareholdernonresidentpreviewpassport_"+getcounter).css("display", "none");
                           $("#shareuploadpassportfileloadingImage_"+getcounter).css("display", "none");
                           $("#shareholdernonresidentlabel_"+getcounter).text();

                         } else{
                            //alert("fdfdfdf");
                            $("#bshareholdernonresidentpreviewpassport_"+getcounter).css("display", "block");
                            $("#shareuploadpassportfileloadingImage_"+getcounter).css("display", "none");
                            $("#shareholdernonresidentlabel_"+getcounter).text(finallabel);


                            var fieldHTML ='<div class="modal fade" id="shareholdernonresidentpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fieldHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fieldHTML += '<div class="modal-content row">';
				fieldHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - Non Resident - Shareholder #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fieldHTML += '<div class="col-lg-12 background-grey-2">';
				fieldHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="shareholdernonresidentpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="shareholdernonresidentsurname[]" class="form-control" id="shareholdernonresidentsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="shareholdernonresidentgivenName[]" class="form-control" id="shareholdernonresidentgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="shareholdernonresidentpassportNumber[]" class="form-control" id="shareholdernonresidentpassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="shareholdernonresidentnationality[]" id="shareholdernonresidentnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholdernonresidentcountry_'+getcounter+'" name="shareholdernonresidentcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="shareholdernonresidentmgender_'+getcounter+'" name="shareholdernonresidentpgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="shareholdernonresidentpgender[]" id="shareholdernonresidentfgender_'+getcounter+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholdernonresidentpdob_'+getcounter+'" class="form-control" name="shareholdernonresidentpdob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholdernonresidentdateexpiry_'+getcounter+'" class="form-control" name="shareholdernonresidentdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 /*fieldHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholdernonresidentdob_'+getcounter+'" class="form-control" name="shareholdernonresidentdob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholdernonresidentplaceofBirth_'+getcounter+'" name="shareholdernonresidentplaceofBirth[]"></select></div></div></div></div>';*/

                                 fieldHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="shareholderpopup('+getcounter+',\'shareholdernonresident\');">Save</a></div></div></div>';
				fieldHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fieldHTML);

                                 //Passport Preview - Non resident

				$('#shareholdernonresidentnationality_'+getcounter).html(countriesOption);
				$('#shareholdernonresidentnationality_'+getcounter).chosen(); 

				$('#shareholdernonresidentcountry_'+getcounter).html(countriesOption);
				$('#shareholdernonresidentcountry_'+getcounter).chosen(); 

				//$('#shareholdernonresidentplaceofBirth_'+getcounter).html(countriesOption);
				//$('#shareholdernonresidentplaceofBirth_'+getcounter).chosen(); 

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 //var DateofIssue = ''; 
                                 //var PlaceofBirth = '';                 
		                 
                                 $('#shareholdernonresidentpdob_'+getcounter).datepicker({   
		                     beforeShow:function () {
		                     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		                     }
	                         });

                                 $('#shareholdernonresidentdateexpiry_'+getcounter).datepicker({   
					beforeShow:function () {
					setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
					}
				});
                                 // Displaying values from OCR - API

                                 $('#shareholdernonresidentgivenName_'+getcounter).val(gname);
                                 $('#shareholdernonresidentsurname_'+getcounter).val(Surname);
                                 $('#shareholdernonresidentpassportNumber_'+getcounter).val(passportno);
                                 //$('#shareholdernonresidentdatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#shareholdernonresidentdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#shareholdernonresidentpdob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#shareholdernonresidentmgender_'+getcounter).prop('checked', true);
                                  $('#shareholdernonresidentfgender_'+getcounter).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#shareholdernonresidentfgender_'+getcounter).prop('checked', true);
                                  $('#shareholdernonresidentmgender_'+getcounter).prop('checked', false); 
                                 }
                                 $('#shareholdernonresidentnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#shareholdernonresidentcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 //$('#shareholdernonresidentplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth); 
                                 $('#shareholdernonresidentpreviewpassport_'+getcounter).modal('show');
                         

                         }     

                    }

        }); 
 
}

//NRIC front

function shareholdernricfront(getdata,getcounter,filename){

$("#shareuploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

	$.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bshareholdernricfrontpreview_"+getcounter).css("display", "none");
                    $("#shareuploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#shareholdernricfrontlabel_"+getcounter).text();

		 } else{

                   $("#bshareholdernricfrontpreview_"+getcounter).css("display", "block");
                   $("#shareuploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#shareholdernricfrontlabel_"+getcounter).text(finallabel);  

                   var nricfrontfield = '<div class="modal fade" id="shareholdernricfrontpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfrontfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfrontfield += '<div class="modal-content row">';
                     nricfrontfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Front Information - Shareholder#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfrontfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="shareholdernricfrontimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="shareholderpreviewfrontnricnumber[]" class="form-control" id="shareholderpreviewfrontnricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="shareholderpreviewnricfrontmgender_'+getcounter+'" name="shareholderpreviewnricfrontgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="shareholderpreviewnricfrontfgender_'+getcounter+'" name="shareholderpreviewnricfrontgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfrontfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="shareholderpreviewnricfrontdob_'+getcounter+'" class="form-control" name="shareholderpreviewnricfrontdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="shareholderpreviewnricfrontnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholderpreviewnricfrontnationality_'+getcounter+'"></select></div></div></div></div>';

                     nricfrontfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholderpreviewnricfrontpostcode_'+getcounter+'" name="shareholderpreviewnricfrontpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="shareholderpreviewnricfrontstreetname[]" id="shareholderpreviewnricfrontstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholderpreviewnricfrontfloor_'+getcounter+'" name="shareholderpreviewnricfrontfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholderpreviewnricfrontunit_'+getcounter+'" name="shareholderpreviewnricfrontunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfrontfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="shareholderpopup('+getcounter+',\'shareholdernricfront\');">Save</a></div></div></div>';

                     nricfrontfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfrontfield);

                     $('#shareholderpreviewnricfrontnationality_'+getcounter).html(countriesOption);
                     $('#shareholderpreviewnricfrontnationality_'+getcounter).chosen();  
                     
                      $('#shareholderpreviewnricfrontdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      }); 
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         if(sex == 'Male'){
                          $('#shareholderpreviewnricfrontmgender_'+getcounter).prop('checked', true);
                          $('#shareholderpreviewnricfrontfgender_'+getcounter).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#shareholderpreviewnricfrontfgender_'+getcounter).prop('checked', true);
                          $('#shareholderpreviewnricfrontmgender_'+getcounter).prop('checked', false); 
                         }

                         $('#shareholderpreviewfrontnricnumber_'+getcounter).val(nricno);
                         $('#shareholderpreviewnricfrontnationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#shareholderpreviewnricfrontdob_'+getcounter).val(dobs);
                         $('#shareholdernricfrontpreview_'+getcounter).modal('show');
                      

		 }
	    }
	}); 
}

//NRIC back

function shareholdernricback(getdata,getcounter,filename){

$("#shareuploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bshareholdernricbackpreview_"+getcounter).css("display", "none");
                    $("#shareuploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#shareholdernricbacklabel_"+getcounter).text();

		 } else{

                   $("#bshareholdernricbackpreview_"+getcounter).css("display", "block"); 
                   $("#shareuploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#shareholdernricbacklabel_"+getcounter).text(finallabel);

                   var nricbackfield = '<div class="modal fade" id="shareholdernricbackpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricbackfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricbackfield += '<div class="modal-content row">';
                     nricbackfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Back Information - Shareholder#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricbackfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="shareholdernricbackimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="shareholderpreviewbacknricnumber[]" class="form-control" id="shareholderpreviewbacknricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="shareholderpreviewnricbackmgender_'+getcounter+'" name="shareholderpreviewnricbackgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="shareholderpreviewnricbackfgender_'+getcounter+'" name="shareholderpreviewnricbackgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricbackfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="shareholderpreviewnricbackdob_'+getcounter+'" class="form-control" name="shareholderpreviewnricbackdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="shareholderpreviewnricbacknationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholderpreviewnricbacknationality_'+getcounter+'"></select></div></div></div></div>';

                     nricbackfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholderpreviewnricbackpostcode_'+getcounter+'" name="shareholderpreviewnricbackpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="shareholderpreviewnricbackstreetname[]" id="shareholderpreviewnricbackstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholderpreviewnricbackfloor_'+getcounter+'" name="shareholderpreviewnricbackfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholderpreviewnricbackunit_'+getcounter+'" name="shareholderpreviewnricbackunit[]" placeholder="Unit" type="text"></div></div>';

                     nricbackfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="shareholderpopup('+getcounter+',\'shareholdernricback\');">Save</a></div></div></div>';

                     nricbackfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricbackfield);

                     $('#shareholderpreviewnricbacknationality_'+getcounter).html(countriesOption);
                     $('#shareholderpreviewnricbacknationality_'+getcounter).chosen();   

                     $('#shareholderpreviewnricbackdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      });

                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         if(sex == 'Male'){
                          $('#shareholderpreviewnricbackmgender_'+getcounter).prop('checked', true);
                          $('#shareholderpreviewnricbackfgender_'+getcounter).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#shareholderpreviewnricbackfgender_'+getcounter).prop('checked', true);
                          $('#shareholderpreviewnricbackmgender_'+getcounter).prop('checked', false);
                         }

                         $('#shareholderpreviewbacknricnumber_'+getcounter).val(nricno);
                         $('#shareholderpreviewnricbacknationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#shareholderpreviewnricbackdob_'+getcounter).val(dobs);
                         $('#shareholdernricbackpreview_'+getcounter).modal('show');   
 
		 }
	    }
   }); 

}

//FIn Passport

function shareholderfinuploadpassportfile(getdata,getcounter,filename){

$("#shareuploadPassportloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/passport/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bshareholderfincardpreviewpassport_"+getcounter).css("display", "none");
                    $("#shareuploadPassportloadingImage_"+getcounter).css("display", "none"); 
                    $("#shareholderfinpassportlabel_"+getcounter).text();

		 } else{

                   $("#bshareholderfincardpreviewpassport_"+getcounter).css("display", "block"); 
                   $("#shareuploadPassportloadingImage_"+getcounter).css("display", "none");
                   $("#shareholderfinpassportlabel_"+getcounter).text(finallabel);


                    var fincardHTML ='<div class="modal fade" id="shareholderfincardpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fincardHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fincardHTML += '<div class="modal-content row">';
				fincardHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - WorkPass - Shareholder #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fincardHTML += '<div class="col-lg-12 background-grey-2">';
				fincardHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="shareholderfincardpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="shareholderfincardsurname[]" class="form-control" id="shareholderfincardsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="shareholderfincardgivenName[]" class="form-control" id="shareholderfincardgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="shareholderfincardpassportNumber[]" class="form-control" id="shareholderfincardppassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="shareholderfincardpnationality[]" id="shareholderfincardpnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholderfincardpcountry_'+getcounter+'" name="shareholderfincardpcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="shareholderfincardmgender_'+getcounter+'" name="shareholderfincardpgender[]" class="radio-red">Male</label><label class="radio-inline"><input type="radio" name="shareholderfincardpgender[]" id="shareholderfincardfgender_'+getcounter+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholderfincardpdob_'+getcounter+'" class="form-control" name="shareholderfincarddob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholderfincardpdateexpiry_'+getcounter+'" class="form-control" name="shareholderfincarddateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 /*fincardHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholderfincarddob_'+getcounter+'" class="form-control" name="shareholderfincarddob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholderfincardplaceofBirth_'+getcounter+'" name="shareholderfincardplaceofBirth[]"></select></div></div></div></div>';*/

                                 fincardHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="shareholderpopup('+getcounter+',\'shareholderfinpassport\');">Save</a></div></div></div>';
				fincardHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fincardHTML);

                                 //Passport Preview - Work Pass

				$('#shareholderfincardpnationality_'+getcounter).html(countriesOption);
				$('#shareholderfincardpnationality_'+getcounter).chosen(); 

				$('#shareholderfincardpcountry_'+getcounter).html(countriesOption);
				$('#shareholderfincardpcountry_'+getcounter).chosen(); 

				$('#shareholderfincardplaceofBirth_'+getcounter).html(countriesOption);
				$('#shareholderfincardplaceofBirth_'+getcounter).chosen(); 

                                $('#shareholderfincardpdob_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 });

                                 $('#shareholderfincardpdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 }); 

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = ''; 
                                 var PlaceofBirth = '';                 
		                 

                                 // Displaying values from OCR - API

                                 $('#shareholderfincardgivenName_'+getcounter).val(gname);
                                 $('#shareholderfincardsurname_'+getcounter).val(Surname);
                                 $('#shareholderfincardppassportNumber_'+getcounter).val(passportno);
                                 $('#shareholderfincarddatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#shareholderfincardpdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#shareholderfincardpdob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#shareholderfincardmgender_'+getcounter).prop('checked', true);
                                  $('#shareholderfincardfgender_'+getcounter).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#shareholderfincardfgender_'+getcounter).prop('checked', true);
                                  $('#shareholderfincardmgender_'+getcounter).prop('checked', false);  
                                 }
                                 $('#shareholderfincardpnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#shareholderfincardpcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 $('#shareholderfincardplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth);   
                                 $('#shareholderfincardpreviewpassport_'+getcounter).modal('show');    
 
		 }
	    }
   }); 

}

// Fin front

function shareholderfincardfront(getdata,getcounter,filename){

$("#shareuploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bshareholderfincardfrontpreview_"+getcounter).css("display", "none"); 
                    $("#shareuploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#shareholderfincardfrontlabel_"+getcounter).text();

		 } else{

                   $("#bshareholderfincardfrontpreview_"+getcounter).css("display", "block"); 
                   $("#shareuploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#shareholderfincardfrontlabel_"+getcounter).text(finallabel);


                var fincardfrontHTML ='<div class="modal fade" id="shareholderfincardfrontpreview_'+getcounter+'" tabindex="-1" role="dialog">';
		fincardfrontHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincardfrontHTML += '<div class="modal-content row">';
		fincardfrontHTML += '<div class="modal-header"><h5 class="modal-title" id="shareholderfincardfrontpreview">Shareholder #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		 fincardfrontHTML += '<div class="col-lg-12 background-grey-2">';

                 fincardfrontHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="shareholderfincardfrontpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="shareholderfincardfrontemployer[]" class="form-control" id="shareholderfincardfrontemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="shareholderfincardfrontoccupation[]" class="form-control" id="shareholderfincardfrontoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

                  fincardfrontHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholderfincardfrontdateissue_'+getcounter+'" class="form-control" name="shareholderfincardfrontdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholderfincardfrontdateexpiry_'+getcounter+'" class="form-control" name="shareholderfincardfrontdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="shareholderwfincardfrontNumber[]" class="form-control" id="shareholderwfincardfrontNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

                 fincardfrontHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="shareholderpopup('+getcounter+',\'shareholderfincardfront\');">Save</a></div></div></div>';
		fincardfrontHTML += '</div></div></div></div>';   
                   
                 $('body').find('#additionalRole:last').after(fincardfrontHTML);

                 $('#shareholderfincardfrontdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 }); 

                  $('#shareholderfincardfrontdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 }); 


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                          $('#shareholderfincardfrontdateissue_'+getcounter).val(DateofIssue);
						 $('#shareholderfincardfrontemployer_'+getcounter).val(Employer);
                         $('#shareholderfincardfrontoccupation_'+getcounter).val(Occupation);
                        
                         $('#shareholderfincardfrontdateexpiry_'+getcounter).val(DateofExpiry);
                         $('#shareholderwfincardfrontNumber_'+getcounter).val(FIN); 
                   
						 
						 
                         $('#shareholderfincardfrontpreview_'+getcounter).modal('show');
 
		 }
	    }
   }); 

}

// Fin back

function shareholderfincardback(getdata,getcounter,filename){

$("#shareuploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bshareholderfincardbackpreview_"+getcounter).css("display", "none"); 
                    $("#shareuploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#shareholderfincardbacklabel_"+getcounter).text();

		 } else{

                   $("#bshareholderfincardbackpreview_"+getcounter).css("display", "block");  
                   $("#shareuploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#shareholderfincardbacklabel_"+getcounter).text(finallabel);


                   var fincardbackHTML ='<div class="modal fade" id="shareholderfincardbackpreview_'+getcounter+'" tabindex="-1" role="dialog">';
			fincardbackHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
			fincardbackHTML += '<div class="modal-content row">';
			fincardbackHTML += '<div class="modal-header"><h5 class="modal-title" id="shareholderfincardbackpreview">Shareholder #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
			 fincardbackHTML += '<div class="col-lg-12 background-grey-2">';

                         fincardbackHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="shareholderfincardbackpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
		     
		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="shareholderfincardbackemployer[]" class="form-control" id="shareholderfincardbackemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="shareholderfincardbackoccupation[]" class="form-control" id="shareholderfincardbackoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

		          fincardbackHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

		          fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholderfincardbackdateissue_'+getcounter+'" class="form-control" name="shareholderfincardbackdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="shareholderfincardbackdateexpiry_'+getcounter+'" class="form-control" name="shareholderfincardbackdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="shareholderwfincardbackNumber[]" class="form-control" id="shareholderwfincardbackNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

		         fincardbackHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="shareholderpopup('+getcounter+',\'shareholderfincardback\');">Save</a></div></div></div>';
			fincardbackHTML += '</div></div></div></div>';   
		           
		         $('body').find('#additionalRole:last').after(fincardbackHTML);

                         $('#shareholderfincardbackdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         $('#shareholderfincardbackdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#shareholderfincardbackdateissue_'+getcounter).val(DateofIssue);
                         $('#shareholderfincardbackpreview_'+getcounter).modal('show');
 
		 }
	    }
   }); 

}



</script>
