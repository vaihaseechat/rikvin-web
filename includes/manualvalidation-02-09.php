<script>
function displaymanualpopup(rolename,getcounter,getresidencystatus){
//alert(rolename);
var getrolename = rolename.toUpperCase();

if(getresidencystatus == ''+rolename+'passportmanually'){

        if( $('#'+rolename+'nonresidentpreviewpassportmanually_'+getcounter).hasClass('modal fade show') ) {
	    console.log("Modal is open");
            $('body').css("overflow","hidden");
            $('#'+rolename+'nonresidentpreviewpassportmanually_'+getcounter).css("overflow","auto");
	}

//$('#'+rolename+'nonresidentpreviewpassportmanually_'+getcounter).modal('hide');
var nonpassportno = document.getElementById(""+rolename+"nonpassportNumber_"+getcounter).value;
var nonnationality = $('#'+rolename+'nonnationality_'+getcounter+'_chosen span').text();
var noncountry = $('#'+rolename+'noncountry_'+getcounter+'_chosen span').text();
var nongender = $("input[name='"+rolename+"nongender[]']:checked").val();
var nondatetimepicker = document.getElementById(""+rolename+"nondateissue_"+getcounter).value;
console.log("nondatetimepicker Id: "+rolename+"nondateissue_"+getcounter+ ", val: "+nondatetimepicker);
var nondateexpiry = document.getElementById(""+rolename+"nondateexpiry_"+getcounter).value;
var nonplaceofBirth = $('#'+rolename+'nondateplace_'+getcounter+'_chosen span').text();
var nondateBirth = document.getElementById(""+rolename+"nondatebirth_"+getcounter).value;
console.log("nondatetimepicker: "+nondatetimepicker);

//alert(nondatetimepicker);

var getnondatetimepicker = formatingdata(nondatetimepicker);
var getnondateexpiry = formatingdata(nondateexpiry);
var getnondateBirth = formatingdata(nondateBirth);

//alert(getnondatetimepicker);

//var startdate = new Date(nondatetimepicker.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(nondateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var cur_date = new Date();
//var dob1 = $(""+rolename+"nondatebirth_"+getcounter).data('datepicker');
var dob = $('#'+rolename+'nondatebirth_'+getcounter).datepicker('getDate');

var startdate = $('#'+rolename+'nondateissue_'+getcounter).datepicker('getDate');
var enddate = $('#'+rolename+'nondateexpiry_'+getcounter).datepicker('getDate');

localStorage.setItem(""+rolename+"nonmanuallypassportno_"+getcounter,nonpassportno);
localStorage.setItem(""+rolename+"nonmanuallynationality_"+getcounter,nonnationality);
localStorage.setItem(""+rolename+"nonmanuallycountry_"+getcounter,noncountry);
localStorage.setItem(""+rolename+"nonmanuallygender_"+getcounter,nongender);
localStorage.setItem(""+rolename+"nonmanuallydatetimepicker_"+getcounter,nondatetimepicker);
localStorage.setItem(""+rolename+"nonmanuallydateexpiry_"+getcounter,nondateexpiry);
localStorage.setItem(""+rolename+"nonmanuallyplaceofBirth_"+getcounter,nonplaceofBirth);
localStorage.setItem(""+rolename+"nonmanuallydateBirth_"+getcounter,nondateBirth);

        if(nonpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	   $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
	   $("#displaysuccessmsg").modal("show");
           return false;

	}
	else if(nonnationality == 'Please select'){

            //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	   $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
	   $("#displaysuccessmsg").modal("show");
           return false;


	}
        else if(noncountry == 'Please select'){

           //alert("Country cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	   $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
	   $("#displaysuccessmsg").modal("show");
           return false; 

	}
        else if(nongender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	     $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
	     $("#displaysuccessmsg").modal("show");
             return false;
	}
        else if(nondatetimepicker == ''){
             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	     $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
	     $("#displaysuccessmsg").modal("show");
             return false;
	}
        else if(nondateexpiry == ''){           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
	      $("#displaysuccessmsg").modal("show");
              return false;

	} 
        else if(nonplaceofBirth == 'Please select'){
              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	      $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");
              return false; 
	}
        else if(nondateBirth == ''){
              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");    
	}else if(nondatetimepicker.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } else if(nondateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } else if(nondateBirth.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(nondatetimepicker != '' && getnondatetimepicker == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(nondateexpiry != '' && getnondateexpiry == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(nondateBirth != '' && getnondateBirth == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(startdate >= enddate){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" Manual");
             $("#displaysuccessmsg .modal-body").html("Date of issue should not exceed or be equal to the Date of expiry.");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be future date");
             $("#displaysuccessmsg").modal("show"); 
        } 
	else{
           
           var getdata = rolename;
	   //alert("Saved Succesfully");
           if(getdata == 'director'){
             localStorage.setItem(MANUAL_DIRECTOR_NONRESIDENT_PASSPORT, true);
             showPassportEmpty(ROLE_DIRECTOR, getcounter, false);
           }else if(getdata == 'shareholder'){
             localStorage.setItem(MANUAL_SHAREHOLDER_NONRESIDENT_PASSPORT, true);
             showPassportEmpty(ROLE_SHAREHOLDER, getcounter, false);
           }else if(getdata == 'di'){
             localStorage.setItem(MANUAL_DISHAREHOLDER_NONRESIDENT_PASSPORT, true);
             showPassportEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, false);
           }else if(getdata == 'corporate'){
             localStorage.setItem(MANUAL_CORPORATE_NONRESIDENT_PASSPORT, true);
             showPassportEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, false);
           }
	   $('#'+rolename+'nonresidentpreviewpassportmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
	   $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
           $('body').css("overflow","auto");
	   $("#displaysuccessmsg").modal("show");   
	}

}//end of passport Manual

if(getresidencystatus == ''+rolename+'nricmanually'){

        if( $('#'+rolename+'nonresidentpreviewnricmanually_'+getcounter).hasClass('modal fade show') ) {
	    console.log("Modal is open");
            $('body').css("overflow","hidden");
            $('#'+rolename+'nonresidentpreviewnricmanually_'+getcounter).css("overflow","auto");
	}

var nonresidentnricnumber = document.getElementById(""+rolename+"nonresidentnricnumber_"+getcounter).value;
var nonresidentgender = $("input[name='"+rolename+"nonresidentgender[]']:checked").val();
var nonresidentdob = document.getElementById(""+rolename+"nonresidentdob_"+getcounter).value;
var nricnationality = $('#'+rolename+'nricnationality_'+getcounter+'_chosen span').text();
var nricpostcode = document.getElementById(""+rolename+"nricpostcode_"+getcounter).value;
var nricstreetname = document.getElementById(""+rolename+"nricstreetname_"+getcounter).value;
var nricfloor = document.getElementById(""+rolename+"nricfloor_"+getcounter).value;
var nricunit = document.getElementById(""+rolename+"nricunit_"+getcounter).value;
var getnonresidentdob = formatingdata(nonresidentdob);

var cur_date = new Date();
//var dob =  checkfuturedata(nonresidentdob);
var dob1 = $('#'+rolename+'nonresidentdob_'+getcounter).datepicker('getDate');



localStorage.setItem(""+rolename+"previewmanuallynricnumber_"+getcounter,nonresidentnricnumber);
localStorage.setItem(""+rolename+"previewmanuallynricgender_"+getcounter,nonresidentgender);
localStorage.setItem(""+rolename+"previewmanuallynricdob_"+getcounter,nonresidentdob);
localStorage.setItem(""+rolename+"previewmanuallynricnationality_"+getcounter,nricnationality);
localStorage.setItem(""+rolename+"previewmanuallynricpostcode_"+getcounter,nricpostcode);
localStorage.setItem(""+rolename+"previewmanuallynricstreetname_"+getcounter,nricstreetname);
localStorage.setItem(""+rolename+"previewmanuallynricfloor_"+getcounter,nricfloor);
localStorage.setItem(""+rolename+"previewmanuallynricunit_"+getcounter,nricunit);

        if(nonresidentnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(nonresidentgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Gender cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(nonresidentdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Date of birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(nricnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	  else if(nricpostcode == ''){

		    //alert("Postcode cannot be empty");
		    $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
		    $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
		    $("#displaysuccessmsg").modal("show");
		}else if(isNaN(nricpostcode)){

		    //alert("Postcode cannot be empty");
		    $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
		    $("#displaysuccessmsg .modal-body").html("Invalid postcode");
		    $("#displaysuccessmsg").modal("show");
	  }
  else if(nricstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
             $("#displaysuccessmsg .modal-body").html("Street name cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}else if(nricfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}else if(isNaN(nricfloor)){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
             $("#displaysuccessmsg .modal-body").html("Invalid floor");
             $("#displaysuccessmsg").modal("show");
        }else if(nricunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}
        else if(nonresidentdob.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(nonresidentdob != '' && getnonresidentdob == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(dob1 >= cur_date){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be future date");
             $("#displaysuccessmsg").modal("show"); 
        }          
	else{
	   
           var getdata = rolename;
	   //alert("Saved Succesfully");
           if(getdata == 'director'){
             localStorage.setItem(MANUAL_DIRECTOR_CITIZENPR_NRIC, true);
             showNRICEmpty(ROLE_DIRECTOR, getcounter, true, false);
	     showNRICEmpty(ROLE_DIRECTOR, getcounter, false, false);
           }else if(getdata == 'shareholder'){
             localStorage.setItem(MANUAL_SHAREHOLDER_CITIZENPR_NRIC, true);
             showNRICEmpty(ROLE_SHAREHOLDER, getcounter, true, false);
	     showNRICEmpty(ROLE_SHAREHOLDER, getcounter, false, false);
           }else if(getdata == 'di'){
             localStorage.setItem(MANUAL_DISHAREHOLDER_CITIZENPR_NRIC, true);
             showNRICEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, true, false);
	     showNRICEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, false, false);
           }else if(getdata == 'corporate'){
             localStorage.setItem(MANUAL_CORPORATE_CITIZENPR_NRIC, true);
             showNRICEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, true, false);
	     showNRICEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, false, false);
           }
	   $('#'+rolename+'nonresidentpreviewnricmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
           $('body').css("overflow","auto");
           $("#displaysuccessmsg").modal("show");
	}


}//end of nric Manual
if((getresidencystatus == ''+rolename+'finmanually') || (getresidencystatus == ''+rolename+'fincardmanually')){

        if( $('#'+rolename+'fincardmanually_'+getcounter).hasClass('modal fade show') ) {
	    console.log("Modal is open");
            $('body').css("overflow","hidden");
            $('#'+rolename+'fincardmanually_'+getcounter).css("overflow","auto");
	}

var fincardpassportno = document.getElementById(""+rolename+"fincardpassportNumber_"+getcounter).value;
var fincardnationality = $('#'+rolename+'fincardnationality_'+getcounter+'_chosen span').text();
var fincardcountry = $('#'+rolename+'fincardcountry_'+getcounter+'_chosen span').text();
var fincardgender = $("input[name='"+rolename+"fincardgender[]']:checked").val();
var fincarddatetimepicker = document.getElementById(""+rolename+"fincarddateissue_"+getcounter).value;
var fincarddateexpiry = document.getElementById(""+rolename+"fincarddateexpiry_"+getcounter).value;
var fincardplaceofBirth = $('#'+rolename+'fincarddateplace_'+getcounter+'_chosen span').text();
var fincarddateBirth = document.getElementById(""+rolename+"fincarddatebirth_"+getcounter).value;

//var startdate = new Date(fincarddatetimepicker.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(fincarddateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var startdate = $('#'+rolename+'fincarddateissue_'+getcounter).datepicker('getDate');
var enddate = $('#'+rolename+'fincarddateexpiry_'+getcounter).datepicker('getDate');
//return false;

var fincardemployer = document.getElementById(""+rolename+"fincardemployer_"+getcounter).value;
var fincardoccupation = document.getElementById(""+rolename+"fincardoccupation_"+getcounter).value;
var workpassdateissue = document.getElementById(""+rolename+"workpassdateissue_"+getcounter).value;
var workpassdateexpiry = document.getElementById(""+rolename+"workpassdateexpiry_"+getcounter).value;
var wfincardNumber = document.getElementById(""+rolename+"wfincardNumber_"+getcounter).value;

//var startdate1 = new Date(workpassdateissue.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate1 =  new Date(workpassdateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var startdate1 = $('#'+rolename+'workpassdateissue_'+getcounter).datepicker('getDate');
var enddate1 = $('#'+rolename+'workpassdateexpiry_'+getcounter).datepicker('getDate');

var getfincarddatetimepicker = formatingdata(fincarddatetimepicker);
var getfincarddateexpiry = formatingdata(fincarddateexpiry);
var getfincarddateBirth = formatingdata(fincarddateBirth);
var getworkpassdateissue = formatingdata(workpassdateissue);
var getworkpassdateexpiry = formatingdata(workpassdateexpiry);

//var dob =  checkfuturedata(fincarddateBirth);
var cur_date = new Date();
var dob = $('#'+rolename+'fincarddatebirth_'+getcounter).datepicker('getDate');

//alert(dob);

localStorage.setItem(""+rolename+"fincardmanuallypassportno_"+getcounter,fincardpassportno);
localStorage.setItem(""+rolename+"fincardmanuallynationality_"+getcounter,fincardnationality);
localStorage.setItem(""+rolename+"fincardmanuallycountry_"+getcounter,fincardcountry);
localStorage.setItem(""+rolename+"fincardmanuallygender_"+getcounter,fincardgender);
localStorage.setItem(""+rolename+"fincardmanuallydatetimepicker_"+getcounter,fincarddatetimepicker);
localStorage.setItem(""+rolename+"fincardmanuallydateexpiry_"+getcounter,fincarddateexpiry);
localStorage.setItem(""+rolename+"fincardmanuallyplaceofBirth_"+getcounter,fincardplaceofBirth);
localStorage.setItem(""+rolename+"fincardmanuallydateBirth_"+getcounter,fincarddateBirth);

localStorage.setItem(""+rolename+"fincardmanuallyemployer_"+getcounter,fincardemployer);
localStorage.setItem(""+rolename+"fincardmanuallyoccupation_"+getcounter,fincardoccupation);
localStorage.setItem(""+rolename+"fincardmanuallyworkpassdateissue_"+getcounter,workpassdateissue);
localStorage.setItem(""+rolename+"fincardmanuallyworkpassdateexpiry_"+getcounter,workpassdateexpiry);
localStorage.setItem(""+rolename+"fincardmanuallywfincardnumber_"+getcounter,wfincardNumber);

        if(fincardpassportno == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");           

	}
	else if(fincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");   

	}
        else if(fincardcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");  
	}
        else if(fincardgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");  
	}
        else if(fincarddatetimepicker == ''){

             //passport date of issue");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");  
	}else if(fincarddateexpiry == ''){
           
              //passport date of expiry
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");  

	} 
        else if(fincardplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        else if(fincarddateBirth == ''){

              //alert("Date of Birth cannot be empty");  
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	} 
        else if(fincardemployer == ''){

              //alert("Employer cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        else if(fincardoccupation == ''){

              //alert("Occupation cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        /*else if(workpassdateissue == ''){

              //alert("Date of Issue cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        else if(workpassdateexpiry == ''){

              //alert("Date of Expiry cannot be empty");  
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}*/
        else if(wfincardNumber == ''){
              //alert("Fin Number cannot be empty");  
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(fincarddatetimepicker.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(fincarddateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(fincarddateBirth.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }        
        else if(fincarddatetimepicker != '' && getfincarddatetimepicker == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(fincarddateexpiry != '' && getfincarddateexpiry == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(fincarddateBirth != ''  && getfincarddateBirth == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(workpassdateissue != '' && getworkpassdateissue == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(workpassdateexpiry != '' && getworkpassdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(startdate >= enddate){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of issue should not exceed or be equal to the Date of expiry.");
             $("#displaysuccessmsg").modal("show"); 
        } else if((workpassdateissue != '') && (workpassdateexpiry != '') &&(startdate1 >= enddate1)){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of issue should not exceed or be equal to the Date of expiry.");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be future date");
             $("#displaysuccessmsg").modal("show"); 
        }     
	else{
	   //alert("Saved Succesfully");    
            var getdata = rolename;
	   //alert("Saved Succesfully");
           if(getdata == 'director'){
             localStorage.setItem(MANUAL_DIRECTOR_FIN_PASSPORT, true);
             showFINCardEmpty(ROLE_DIRECTOR, getcounter, true, false);
	     showFINCardEmpty(ROLE_DIRECTOR, getcounter, false, false);
           }else if(getdata == 'shareholder'){
             localStorage.setItem(MANUAL_SHAREHOLDER_FIN_PASSPORT, true);
             showFINCardEmpty(ROLE_SHAREHOLDER, getcounter, true, false);
	     showFINCardEmpty(ROLE_SHAREHOLDER, getcounter, false, false);
           }else if(getdata == 'di'){
             localStorage.setItem(MANUAL_DISHAREHOLDER_FIN_PASSPORT, true);
             showFINCardEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, true, false);
	     showFINCardEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, false, false);
           }else if(getdata == 'corporate'){
             localStorage.setItem(MANUAL_CORPORATE_FIN_PASSPORT, true);
             showFINCardEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, true, false);
	     showFINCardEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, false, false);
           }   
	    $('#'+rolename+'fincardmanually_'+getcounter).modal('hide');
            $('body').css("overflow","auto");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $("#displaysuccessmsg").modal("show");  
	}






}//end of fin Manual



}
</script>
