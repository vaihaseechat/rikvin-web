<script>
function bofinbackpopup(getcounter,getboname){

if( $('#bofincardbackpreview_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden"); 
    $('#bofincardbackpreview_'+getcounter+"_"+getboname).css("overflow","auto");
}

var bofincardbackemployer = document.getElementById("bofincardbackemployer_"+getcounter+"_"+getboname).value;
var bofincardbackoccupation = document.getElementById("bofincardbackoccupation_"+getcounter+"_"+getboname).value;
var bofincardbackdateissue = document.getElementById("bofincardbackdateissue_"+getcounter+"_"+getboname).value;
var bofincardbackdateexpiry = document.getElementById("bofincardbackdateexpiry_"+getcounter+"_"+getboname).value;
var bowfincardbackNumber = document.getElementById("bowfincardbackNumber_"+getcounter+"_"+getboname).value;

var startdate = $("bofincardbackdateissue_"+getcounter+"_"+getboname).datepicker('getDate');
var enddate = $("bofincardbackdateexpiry_"+getcounter+"_"+getboname).datepicker('getDate');

var getfincardbackdateissue = formatingdata(bofincardbackdateissue);
var getfincardbackdateexpiry = formatingdata(bofincardbackdateexpiry);

localStorage.setItem("bofincardbackemployer_"+getcounter+"_"+getboname,bofincardbackemployer);
localStorage.setItem("bofincardbackoccupation_"+getcounter+"_"+getboname,bofincardbackoccupation);
localStorage.setItem("bofincardbackdateissue_"+getcounter+"_"+getboname,bofincardbackdateissue);
localStorage.setItem("bofincardbackdateexpiry_"+getcounter+"_"+getboname,bofincardbackdateexpiry);
localStorage.setItem("bowfincardbackNumber_"+getcounter+"_"+getboname,bowfincardbackNumber);

        /*if(bofincardbackemployer == ''){
           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(bofincardbackoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(bofincardbackdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(bofincardbackdateexpiry == ''){

           //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}*/
        
        if(bofincardbackdateissue != "" && getfincardbackdateissue == true){
             $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(bofincardbackdateexpiry != "" && getfincardbackdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((bofincardbackdateissue != "") && (bofincardbackdateissue.length < 10)){
             $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((bofincardbackdateexpiry != "") && (bofincardbackdateexpiry.length < 10)){
             $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if((bofincardbackdateissue != "") && (bofincardbackdateexpiry != "") && (startdate >= enddate)){
             $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bowfincardbackNumber == ''){

           // alert("Fin Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#bofincardbackpreview_'+getcounter+"_"+getboname).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Work Pass Back Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show"); 
        }


}

</script>

