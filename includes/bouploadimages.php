<script src='<?php echo BASE_URL;?>/js/pdfcore2.js'></script>
<script>
var parsecount = 20;
function checkPdfParse(myImages,getcounter,getboname,callbackfunction)
{

//pdfjsLib.GlobalWorkerOptions.workerSrc="https://mozilla.github.io/pdf.js/build/pdf.worker.js";

var myImage = myImages[0];
var filename = myImage.name;
var fileReader = new FileReader();
var scale = 1.0;
var ext=myImages[0].name.split('.').pop().toLowerCase();
if(ext=="pdf")
{
try{	
console.log(" Getting image data from pdf");

fileReader.onload = function() {

	//Step 4:turn array buffer into typed array
	var typedarray = new Uint8Array(this.result);

	//Step 5:PDFJS should be able to read this
	pdfjsLib.getDocument(typedarray).then(function(pdf) {
		 pdf.getPage(1).then(function(page) {
	 
			var viewport = page.getViewport(scale);
			var canvas = document.createElement('canvas') , ctx = canvas.getContext('2d');
			var renderContext = { canvasContext: ctx, viewport: viewport };
			canvas.height = viewport.height;
			canvas.width = viewport.width;
			page.render(renderContext).then(function() {									
				var file_blob=[];
				file_blob.push(dataURItoBlob(canvas.toDataURL()));
				callbackfunction(file_blob,getcounter,filename);	
			});
		});
	});


};
fileReader.readAsArrayBuffer(myImage);
}catch (err) {

	console.log(err);
	callbackfunction(myImages,getcounter,getboname,filename);
}
}
else{
	//alert(callbackfunction);
        var extension =myImages[0].name.split('.').pop().toLowerCase(); 
        console.log(extension);
        if((extension == "png") || (extension == "jpg") || (extension == "jpeg") || (extension == "gif"))
        {
	   callbackfunction(myImages,getcounter,getboname,filename);
        }
        else{
           $("#displaysuccessmsg .modal-title").html("Error");
	   $("#displaysuccessmsg .modal-body").html("Invalid File type");
	   $("#displaysuccessmsg").modal("show");
        } 
}
	
}




function dataURItoBlob(dataURI) {
// convert base64/URLEncoded data component to raw binary data held in a string
var byteString;
if (dataURI.split(',')[0].indexOf('base64') >= 0)
    byteString = atob(dataURI.split(',')[1]);
else
    byteString = unescape(dataURI.split(',')[1]);
// separate out the mime component
var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
// write the bytes of the string to a typed array
var ia = new Uint8Array(byteString.length);
for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
}
return new Blob([ia], {type:mimeString});
}

var parsecount = 20;

function bononuploadpassportfile(getdata,getcounter,getboname,filename){

$("#bouploadpassportfileloadingImage_"+getcounter+"_"+getboname).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
//alert(filename);

        $.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error; 


                         if(Errorstatus == 'Poor Image Quality') {                           
                           
                           $("#displaysuccessmsg .modal-title").html("Beneficial Owner - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
                           $("#bbononresidentpreviewpassport_"+getcounter+"_"+getboname).css("display", "none");
                           $("#bouploadpassportfileloadingImage_"+getcounter+"_"+getboname).css("display", "none");
                           $("#bononresidentlabel_"+getcounter+"_"+getboname).text();

                         } else{
                            //alert("fdfdfdf");
                            $("#bbononresidentpreviewpassport_"+getcounter+"_"+getboname).css("display", "block");
                            $("#bouploadpassportfileloadingImage_"+getcounter+"_"+getboname).css("display", "none");
                            $("#bononresidentlabel_"+getcounter+"_"+getboname).text(finallabel);


                            var fieldHTML ='<div class="modal fade" id="bononresidentpreviewpassport_'+getcounter+"_"+getboname+'" tabindex="-1" role="dialog">';
				fieldHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fieldHTML += '<div class="modal-content row">';
				fieldHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - Non Resident - Beneficial Owner</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fieldHTML += '<div class="col-lg-12 background-grey-2 popheight">';
				fieldHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="bononresidentpassportimage_'+getcounter+"_"+getboname+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="bononresidentsurname[]" class="form-control" id="bononresidentsurname_'+getcounter+"_"+getboname+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="bononresidentgivenName[]" class="form-control" id="bononresidentgivenName_'+getcounter+"_"+getboname+'"  value="" /></div></div></div></div>';	

				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="bononresidentpassportNumber[]" class="form-control" id="bononresidentpassportNumber_'+getcounter+"_"+getboname+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="bononresidentnationality[]" id="bononresidentnationality_'+getcounter+"_"+getboname+'"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bononresidentcountry_'+getcounter+"_"+getboname+'" name="bononresidentcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="bononresidentmgender_'+getcounter+"_"+getboname+'" name="bononresidentpgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="bononresidentpgender[]" id="bononresidentfgender_'+getcounter+"_"+getboname+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bononresidentpdob_'+getcounter+"_"+getboname+'" class="form-control" name="bononresidentpdob[]" value="" ></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bononresidentdateexpiry_'+getcounter+"_"+getboname+'" class="form-control" name="bononresidentdateexpiry[]" value="" ></div></div></div></div></div>';

                                 fieldHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bopassportsavepreview('+getcounter+',\''+getboname+'\');">Save</a></div></div></div>';
				fieldHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#bopreviewpassport:last').after(fieldHTML);

                                 //Passport Preview - Non resident

				$('#bononresidentnationality_'+getcounter+"_"+getboname).html(countriesOption);
				$('#bononresidentnationality_'+getcounter+"_"+getboname).chosen(); 

				$('#bononresidentcountry_'+getcounter+"_"+getboname).html(countriesOption);
				$('#bononresidentcountry_'+getcounter+"_"+getboname).chosen(); 

				//$('#bononresidentplaceofBirth_'+getcounter+"_"+getboname).html(countriesOption);
				//$('#bononresidentplaceofBirth_'+getcounter+"_"+getboname).chosen(); 

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = obj.DateofIssue; 
                                 var PlaceofBirth = obj.PlaceofBirth;                 
		                 
                                 $('#bononresidentpdob_'+getcounter+"_"+getboname).datepicker({   
		                     beforeShow:function () {
		                     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		                     }
	                         });

                                 $('#bononresidentdateexpiry_'+getcounter+"_"+getboname).datepicker({   
					beforeShow:function () {
					setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
					}
				});
                                 // Displaying values from OCR - API

                                 $('#bononresidentgivenName_'+getcounter+"_"+getboname).val(gname);
                                 $('#bononresidentsurname_'+getcounter+"_"+getboname).val(Surname);
                                 $('#bononresidentpassportNumber_'+getcounter+"_"+getboname).val(passportno);
                                 //$('#bononresidentdatetimepicker_'+getcounter+"_"+getboname).val(DateofIssue);
                                 $('#bononresidentdateexpiry_'+getcounter+"_"+getboname).val(DateofExpiry);
                                 $('#bononresidentpdob_'+getcounter+"_"+getboname).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#bononresidentmgender_'+getcounter+"_"+getboname).prop('checked', true);
                                  $('#bononresidentfgender_'+getcounter+"_"+getboname).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#bononresidentfgender_'+getcounter+"_"+getboname).prop('checked', true);
                                  $('#bononresidentmgender_'+getcounter+"_"+getboname).prop('checked', false); 
                                 }
                                 $('#bononresidentnationality_'+getcounter+"_"+getboname+'_chosen span').text(Nationality);
                                 $('#bononresidentcountry_'+getcounter+"_"+getboname+'_chosen span').text(CountryofIssue);
                                 //$('#bononresidentplaceofBirth_'+getcounter+"_"+getboname+'_chosen span').text(PlaceofBirth); 
                                 $('#bononresidentpreviewpassport_'+getcounter+"_"+getboname).modal('show');

                                 $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				}); 
                         

                         }     

                    }

        }); 
 
}

//NRIC front

function bonricfront(getdata,getcounter,getboname,filename){

$("#bouploadnricloadingImage_"+getcounter+"_"+getboname).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

	$.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Beneficial Owner - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bbonricfrontpreview_"+getcounter+"_"+getboname).css("display", "none");
                    $("#bouploadnricloadingImage_"+getcounter+"_"+getboname).css("display", "none"); 
                    $("#bonricfrontlabel_"+getcounter+"_"+getboname).text();

		 } else{

                   $("#bbonricfrontpreview_"+getcounter+"_"+getboname).css("display", "block");
                   $("#bouploadnricloadingImage_"+getcounter+"_"+getboname).css("display", "none");
                   $("#bonricfrontlabel_"+getcounter+"_"+getboname).text(finallabel);  

                   var nricfrontfield = '<div class="modal fade" id="bonricfrontpreview_'+getcounter+"_"+getboname+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfrontfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfrontfield += '<div class="modal-content row">';
                     nricfrontfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Front Information - Beneficial Owner</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfrontfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="bonricfrontimage_'+getcounter+"_"+getboname+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="bopreviewfrontnricnumber[]" class="form-control" id="bopreviewfrontnricnumber_'+getcounter+"_"+getboname+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="bopreviewnricfrontmgender_'+getcounter+"_"+getboname+'" name="bopreviewnricfrontgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="bopreviewnricfrontfgender_'+getcounter+"_"+getboname+'" name="bopreviewnricfrontgender[]" class="radio-red" type="radio" checked> Female</label></div></div></div></div>';

                     nricfrontfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input id="bopreviewnricfrontdob_'+getcounter+"_"+getboname+'" class="form-control" name="bopreviewnricfrontdob[]" placeholder="DD/MM/YY" type="text" ></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="bopreviewnricfrontnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bopreviewnricfrontnationality_'+getcounter+"_"+getboname+'"></select></div></div></div></div>';

                     nricfrontfield += '<div class="form-group row" style="display:none;"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bopreviewnricfrontpostcode_'+getcounter+"_"+getboname+'" name="bopreviewnricfrontpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="bopreviewnricfrontstreetname[]" id="bopreviewnricfrontstreetname_'+getcounter+"_"+getboname+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bopreviewnricfrontfloor_'+getcounter+"_"+getboname+'" name="bopreviewnricfrontfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bopreviewnricfrontunit_'+getcounter+"_"+getboname+'" name="bopreviewnricfrontunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfrontfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bonricfrontsavepreview('+getcounter+',\''+getboname+'\');">Save</a></div></div></div>';

                     nricfrontfield += '</div></div></div></div>'; 
                     $('body').find('#bopreviewpassport:last').before(nricfrontfield);

                     $('#bopreviewnricfrontnationality_'+getcounter+"_"+getboname).html(countriesOption);
                     $('#bopreviewnricfrontnationality_'+getcounter+"_"+getboname).chosen();  
                     
                      $('#bopreviewnricfrontdob_'+getcounter+"_"+getboname).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      }); 
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;
                         var streetname = obj.Street;
                         var postcode = obj.Postal;
                         var flooring = obj.Floor;
                         var unit = obj.Unit;

                         if(sex == 'Male'){
                          $('#bopreviewnricfrontmgender_'+getcounter+"_"+getboname).prop('checked', true);
                          $('#bopreviewnricfrontfgender_'+getcounter+"_"+getboname).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#bopreviewnricfrontfgender_'+getcounter+"_"+getboname).prop('checked', true);
                          $('#bopreviewnricfrontmgender_'+getcounter+"_"+getboname).prop('checked', false); 
                         }

                         $('#bopreviewfrontnricnumber_'+getcounter+"_"+getboname).val(nricno);
                         $('#bopreviewnricfrontnationality_'+getcounter+"_"+getboname+'_chosen span').text(Cntryofbirth); 
                         $('#bopreviewnricfrontdob_'+getcounter+"_"+getboname).val(dobs);
                         $('#bopreviewnricfrontpostcode_'+getcounter+"_"+getboname).val(postcode);
                         $('#bopreviewnricfrontstreetname_'+getcounter+"_"+getboname).val(streetname);
                         $('#bopreviewnricfrontfloor_'+getcounter+"_"+getboname).val(flooring);
                         $('#bopreviewnricfrontunit_'+getcounter+"_"+getboname).val(unit);  
                         $('#bonricfrontpreview_'+getcounter+"_"+getboname).modal('show');

                         $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
		         }); 
                      

		 }
	    }
	}); 
}

//NRIC back

function bonricback(getdata,getcounter,getboname,filename){

$("#bouploadnricloadingImage_"+getcounter+"_"+getboname).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Beneficial Owner - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bbonricbackpreview_"+getcounter+"_"+getboname).css("display", "none");
                    $("#bouploadnricloadingImage_"+getcounter+"_"+getboname).css("display", "none"); 
                    $("#bonricbacklabel_"+getcounter+"_"+getboname).text();

		 } else{

                   $("#bbonricbackpreview_"+getcounter+"_"+getboname).css("display", "block"); 
                   $("#bouploadnricloadingImage_"+getcounter+"_"+getboname).css("display", "none");
                   $("#bonricbacklabel_"+getcounter+"_"+getboname).text(finallabel);

                   var nricbackfield = '<div class="modal fade" id="bonricbackpreview_'+getcounter+"_"+getboname+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricbackfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricbackfield += '<div class="modal-content row">';
                     nricbackfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Back Information - Beneficial Owner</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricbackfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="bonricbackimage_'+getcounter+"_"+getboname+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricbackfield += '<div class="row mt-3" style="display:none;"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="bopreviewbacknricnumber[]" class="form-control" id="bopreviewbacknricnumber_'+getcounter+"_"+getboname+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="bopreviewnricbackmgender_'+getcounter+"_"+getboname+'" name="bopreviewnricbackgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="bopreviewnricbackfgender_'+getcounter+"_"+getboname+'" name="bopreviewnricbackgender[]" class="radio-red" type="radio" checked> Female</label></div></div></div></div>';

                     nricbackfield += '<div class="row" style="display:none;"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="bopreviewnricbackdob_'+getcounter+"_"+getboname+'" class="form-control" name="bopreviewnricbackdob[]" placeholder="DD/MM/YYYY" type="text"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="bopreviewnricbacknationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bopreviewnricbacknationality_'+getcounter+"_"+getboname+'"></select></div></div></div></div>';

                     nricbackfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bopreviewnricbackpostcode_'+getcounter+"_"+getboname+'" name="bopreviewnricbackpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="bopreviewnricbackstreetname[]" id="bopreviewnricbackstreetname_'+getcounter+"_"+getboname+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bopreviewnricbackfloor_'+getcounter+"_"+getboname+'" name="bopreviewnricbackfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bopreviewnricbackunit_'+getcounter+"_"+getboname+'" name="bopreviewnricbackunit[]" placeholder="Unit" type="text"></div></div>';

                     nricbackfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bonricbacksavepreview('+getcounter+',\''+getboname+'\');">Save</a></div></div></div>';

                     nricbackfield += '</div></div></div></div>'; 
                     $('body').find('#bopreviewpassport:last').before(nricbackfield);

                     $('#bopreviewnricbacknationality_'+getcounter+"_"+getboname).html(countriesOption);
                     $('#bopreviewnricbacknationality_'+getcounter+"_"+getboname).chosen();   

                     $('#bopreviewnricbackdob_'+getcounter+"_"+getboname).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      });

                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;
                         var streetname = obj.Street;
                         var postcode = obj.Postal;
                         var flooring = obj.Floor;
                         var unit = obj.Unit;

                         if(sex == 'Male'){
                          $('#bopreviewnricbackmgender_'+getcounter+"_"+getboname).prop('checked', true);
                          $('#bopreviewnricbackfgender_'+getcounter+"_"+getboname).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#bopreviewnricbackfgender_'+getcounter+"_"+getboname).prop('checked', true);
                          $('#bopreviewnricbackmgender_'+getcounter+"_"+getboname).prop('checked', false);
                         }

                         $('#bopreviewbacknricnumber_'+getcounter+"_"+getboname).val(nricno);
                         $('#bopreviewnricbacknationality_'+getcounter+"_"+getboname+'_chosen span').text(Cntryofbirth); 
                         $('#bopreviewnricbackdob_'+getcounter+"_"+getboname).val(dobs);
                         $('#bopreviewnricbackpostcode_'+getcounter+"_"+getboname).val(postcode);
                         $('#bopreviewnricbackstreetname_'+getcounter+"_"+getboname).val(streetname);
                         $('#bopreviewnricbackfloor_'+getcounter+"_"+getboname).val(flooring);
                         $('#bopreviewnricbackunit_'+getcounter+"_"+getboname).val(unit);  
                         $('#bonricbackpreview_'+getcounter+"_"+getboname).modal('show');  

                         $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				});  
 
		 }
	    }
   }); 

}

//FIn Passport

function bofinuploadpassportfile(getdata,getcounter,getboname,filename){

$("#bouploadPassportloadingImage_"+getcounter+"_"+getboname).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/passport/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Beneficial Owner - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bbofincardpreviewpassport_"+getcounter+"_"+getboname).css("display", "none");
                    $("#bouploadPassportloadingImage_"+getcounter+"_"+getboname).css("display", "none"); 
                    $("#bofinpassportlabel_"+getcounter+"_"+getboname).text();

		 } else{

                   $("#bbofincardpreviewpassport_"+getcounter+"_"+getboname).css("display", "block"); 
                   $("#bouploadPassportloadingImage_"+getcounter+"_"+getboname).css("display", "none");
                   $("#bofinpassportlabel_"+getcounter+"_"+getboname).text(finallabel);


                    var fincardHTML ='<div class="modal fade" id="bofincardpreviewpassport_'+getcounter+"_"+getboname+'" tabindex="-1" role="dialog">';
				fincardHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fincardHTML += '<div class="modal-content row">';
				fincardHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - WorkPass - Beneficial Owner</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fincardHTML += '<div class="col-lg-12 background-grey-2">';
				fincardHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="bofincardpassportimage_'+getcounter+"_"+getboname+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="bofincardsurname[]" class="form-control" id="bofincardsurname_'+getcounter+"_"+getboname+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="bofincardgivenName[]" class="form-control" id="bofincardgivenName_'+getcounter+"_"+getboname+'"  value="" /></div></div></div></div>';	

				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="bofincardpassportNumber[]" class="form-control" id="bofincardppassportNumber_'+getcounter+"_"+getboname+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="bofincardpnationality[]" id="bofincardpnationality_'+getcounter+"_"+getboname+'"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bofincardpcountry_'+getcounter+"_"+getboname+'" name="bofincardpcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="bofincardmgender_'+getcounter+"_"+getboname+'" name="bofincardpgender[]" class="radio-red">Male</label><label class="radio-inline"><input type="radio" name="bofincardpgender[]" id="bofincardfgender_'+getcounter+"_"+getboname+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincardpdob_'+getcounter+"_"+getboname+'" class="form-control" name="bofincarddob[]" value="" ></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincardpdateexpiry_'+getcounter+"_"+getboname+'" class="form-control" name="bofincarddateexpiry[]" value="" ></div></div></div></div></div>';

                                 /*fincardHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="bofincarddob_'+getcounter+"_"+getboname+'" class="form-control" name="bofincarddob[]" value=""></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bofincardplaceofBirth_'+getcounter+"_"+getboname+'" name="bofincardplaceofBirth[]"></select></div></div></div></div>';*/

                                 fincardHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bofinsavepreviewpopup('+getcounter+',\''+getboname+'\');">Save</a></div></div></div>';
				fincardHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#bopreviewpassport:last').after(fincardHTML);

                                 //Passport Preview - Work Pass

				$('#bofincardpnationality_'+getcounter+"_"+getboname).html(countriesOption);
				$('#bofincardpnationality_'+getcounter+"_"+getboname).chosen(); 

				$('#bofincardpcountry_'+getcounter+"_"+getboname).html(countriesOption);
				$('#bofincardpcountry_'+getcounter+"_"+getboname).chosen(); 

				$('#bofincardplaceofBirth_'+getcounter+"_"+getboname).html(countriesOption);
				$('#bofincardplaceofBirth_'+getcounter+"_"+getboname).chosen(); 

                                $('#bofincardpdob_'+getcounter+"_"+getboname).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 });

                                 $('#bofincardpdateexpiry_'+getcounter+"_"+getboname).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 }); 

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = obj.DateofIssue; 
                                 var PlaceofBirth = obj.PlaceofBirth;                 
		                 

                                 // Displaying values from OCR - API

                                 $('#bofincardgivenName_'+getcounter+"_"+getboname).val(gname);
                                 $('#bofincardsurname_'+getcounter+"_"+getboname).val(Surname);
                                 $('#bofincardppassportNumber_'+getcounter+"_"+getboname).val(passportno);
                                 $('#bofincarddatetimepicker_'+getcounter+"_"+getboname).val(DateofIssue);
                                 $('#bofincardpdateexpiry_'+getcounter+"_"+getboname).val(DateofExpiry);
                                 $('#bofincardpdob_'+getcounter+"_"+getboname).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#bofincardmgender_'+getcounter+"_"+getboname).prop('checked', true);
                                  $('#bofincardfgender_'+getcounter+"_"+getboname).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#bofincardfgender_'+getcounter+"_"+getboname).prop('checked', true);
                                  $('#bofincardmgender_'+getcounter+"_"+getboname).prop('checked', false);  
                                 }
                                 $('#bofincardpnationality_'+getcounter+"_"+getboname+'_chosen span').text(Nationality);
                                 $('#bofincardpcountry_'+getcounter+"_"+getboname+'_chosen span').text(CountryofIssue);
                                 $('#bofincardplaceofBirth_'+getcounter+"_"+getboname+'_chosen span').text(PlaceofBirth);   
                                 $('#bofincardpreviewpassport_'+getcounter+"_"+getboname).modal('show');   

                                 $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				});  
 
		 }
	    }
   }); 

}

// Fin front

function bofincardfront(getdata,getcounter,getboname,filename){

$("#bouploadfincardloadingImage_"+getcounter+"_"+getboname).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Beneficial Owner - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bbofincardfrontpreview_"+getcounter+"_"+getboname).css("display", "none"); 
                    $("#bouploadfincardloadingImage_"+getcounter+"_"+getboname).css("display", "none"); 
                    $("#bofincardfrontlabel_"+getcounter+"_"+getboname).text();

		 } else{

                   $("#bbofincardfrontpreview_"+getcounter+"_"+getboname).css("display", "block"); 
                   $("#bouploadfincardloadingImage_"+getcounter+"_"+getboname).css("display", "none");
                   $("#bofincardfrontlabel_"+getcounter+"_"+getboname).text(finallabel);


                var fincardfrontHTML ='<div class="modal fade" id="bofincardfrontpreview_'+getcounter+"_"+getboname+'" tabindex="-1" role="dialog">';
		fincardfrontHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincardfrontHTML += '<div class="modal-content row">';
		fincardfrontHTML += '<div class="modal-header"><h5 class="modal-title" id="bofincardfrontpreview">Beneficial Owner WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		 fincardfrontHTML += '<div class="col-lg-12 background-grey-2">';

                 fincardfrontHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="bofincardfrontpassportimage_'+getcounter+"_"+getboname+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="bofincardfrontemployer[]" class="form-control" id="bofincardfrontemployer_'+getcounter+"_"+getboname+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="bofincardfrontoccupation[]" class="form-control" id="bofincardfrontoccupation_'+getcounter+"_"+getboname+'" value="" type="text"></div></div></div></div>'; 

                  fincardfrontHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label ">Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincardfrontdateissue_'+getcounter+"_"+getboname+'" class="form-control" name="bofincardfrontdateissue[]" value="" ></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincardfrontdateexpiry_'+getcounter+"_"+getboname+'" class="form-control" name="bofincardfrontdateexpiry[]" value="" ></div></div></div></div></div>';

                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="bowfincardfrontNumber[]" class="form-control" id="bowfincardfrontNumber_'+getcounter+"_"+getboname+'"  value="" /></div></div></div></div>';        

                 fincardfrontHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bofinfrontpopup('+getcounter+',\''+getboname+'\');">Save</a></div></div></div>';
		fincardfrontHTML += '</div></div></div></div>';   
                   
                 $('body').find('#bopreviewpassport:last').after(fincardfrontHTML);

                 $('#bofincardfrontdateissue_'+getcounter+"_"+getboname).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 }); 

                  $('#bofincardfrontdateexpiry_'+getcounter+"_"+getboname).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 }); 


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#bofincardfrontemployer_'+getcounter+"_"+getboname).val(Employer);
                         $('#bofincardfrontoccupation_'+getcounter+"_"+getboname).val(Occupation);
                         $('#bofincardfrontdateissue_'+getcounter+"_"+getboname).val(DateofIssue);
                         $('#bofincardfrontdateexpiry_'+getcounter+"_"+getboname).val(DateofExpiry);
                         $('#bowfincardfrontNumber_'+getcounter+"_"+getboname).val(FIN);  
                         $('#bofincardfrontpreview_'+getcounter+"_"+getboname).modal('show');

                         $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
			}); 
 
		 }
	    }
   }); 

}

// Fin back

function bofincardback(getdata,getcounter,getboname,filename){

$("#bouploadfincardloadingImage_"+getcounter+"_"+getboname).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Beneficial Owner - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bbofincardbackpreview_"+getcounter+"_"+getboname).css("display", "none"); 
                    $("#bouploadfincardloadingImage_"+getcounter+"_"+getboname).css("display", "none"); 
                    $("#bofincardbacklabel_"+getcounter+"_"+getboname).text();

		 } else{

                   $("#bbofincardbackpreview_"+getcounter+"_"+getboname).css("display", "block");  
                   $("#bouploadfincardloadingImage_"+getcounter+"_"+getboname).css("display", "none");
                   $("#bofincardbacklabel_"+getcounter+"_"+getboname).text(finallabel);


                   var fincardbackHTML ='<div class="modal fade" id="bofincardbackpreview_'+getcounter+"_"+getboname+'" tabindex="-1" role="dialog">';
			fincardbackHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
			fincardbackHTML += '<div class="modal-content row">';
			fincardbackHTML += '<div class="modal-header"><h5 class="modal-title" id="bofincardbackpreview">Beneficial Owner WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
			 fincardbackHTML += '<div class="col-lg-12 background-grey-2">';

                         fincardbackHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="bofincardbackpassportimage_'+getcounter+"_"+getboname+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
		     
		         fincardbackHTML += '<div class="row" style="display:none;"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="bofincardbackemployer[]" class="form-control" id="bofincardbackemployer_'+getcounter+"_"+getboname+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="bofincardbackoccupation[]" class="form-control" id="bofincardbackoccupation_'+getcounter+"_"+getboname+'" value="" type="text"></div></div></div></div>'; 

		          fincardbackHTML += '<div class="row mb-2" style="display:none;"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

		          fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincardbackdateissue_'+getcounter+"_"+getboname+'" class="form-control" name="bofincardbackdateissue[]" value="" ></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincardbackdateexpiry_'+getcounter+"_"+getboname+'" class="form-control" name="bofincardbackdateexpiry[]" value="" ></div></div></div></div></div>';

		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="bowfincardbackNumber[]" class="form-control" id="bowfincardbackNumber_'+getcounter+"_"+getboname+'"  value="" /></div></div></div></div>';        

		         fincardbackHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bofinbackpopup('+getcounter+',\''+getboname+'\');">Save</a></div></div></div>';
			fincardbackHTML += '</div></div></div></div>';   
		           
		         $('body').find('#bopreviewpassport:last').after(fincardbackHTML);

                         $('#bofincardbackdateissue_'+getcounter+"_"+getboname).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         $('#bofincardbackdateexpiry_'+getcounter+"_"+getboname).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#bofincardbackemployer_'+getcounter+"_"+getboname).val(Employer);
                         $('#bofincardbackoccupation_'+getcounter+"_"+getboname).val(Occupation);
                         $('#bofincardbackdateissue_'+getcounter+"_"+getboname).val(DateofIssue);
                         $('#bofincardbackdateexpiry_'+getcounter+"_"+getboname).val(DateofExpiry);
                         $('#bowfincardbackNumber_'+getcounter+"_"+getboname).val(FIN);
                         $('#bofincardbackpreview_'+getcounter+"_"+getboname).modal('show');


                         $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				}); 
 
		 }
	    }
   }); 

}



</script>
