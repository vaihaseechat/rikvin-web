<script>
function disstoredata(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);


if(getresidencystatus == 'dishareholderpassportmanually'){


	var dinonpassportno = document.getElementById("dinonpassportNumber_"+getcounter).value;
	var dinonnationality = $('#dinonnationality_'+getcounter+'_chosen span').text();
	var dinoncountry = $('#dinoncountry_'+getcounter+'_chosen span').text();
	var dinongender = $("input[name='dinongender[]']:checked").val();
	var dinondatetimepicker = document.getElementById("dinondateissue_"+getcounter).value;
	var dinondateexpiry = document.getElementById("dinondateexpiry_"+getcounter).value;
	var dinonplaceofBirth = $('#dinondateplace_'+getcounter+'_chosen span').text();
        var dinondateBirth = document.getElementById("dinondatebirth_"+getcounter).value;

	localStorage.setItem("dinonmanuallypassportno_"+getcounter,dinonpassportno);
	localStorage.setItem("dinonmanuallynationality_"+getcounter,dinonnationality);
	localStorage.setItem("dinonmanuallycountry_"+getcounter,dinoncountry);
	localStorage.setItem("dinonmanuallygender_"+getcounter,dinongender);
	localStorage.setItem("dinonmanuallydatetimepicker_"+getcounter,dinondatetimepicker);
	localStorage.setItem("dinonmanuallydateexpiry_"+getcounter,dinondateexpiry);
	localStorage.setItem("dinonmanuallyplaceofBirth_"+getcounter,dinonplaceofBirth);
        localStorage.setItem("dinonmanuallydateBirth_"+getcounter,dinondateBirth);


        if(dinonpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(dinonnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
        else if(dinoncountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(dinongender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(dinondatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(dinondateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show"); 

	}         
        else if(dinondateBirth == ''){

              //alert("Date of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(dinonplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
	      $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");   
	}    
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_DISHAREHOLDER_NONRESIDENT_PASSPORT, true);
           showPassportEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, false);
	   $('#dinonresidentpreviewpassportmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

	
}

if(getresidencystatus == 'dishareholdernricmanually'){

var dinonresidentnricnumber = document.getElementById("dinonresidentnricnumber_"+getcounter).value;
var dinonresidentgender = $("input[name='dinonresidentgender[]']:checked").val();
var dinonresidentdob = document.getElementById("dinonresidentdob_"+getcounter).value;
var dinricnationality = $('#dinricnationality_'+getcounter+'_chosen span').text();
var dinricpostcode = document.getElementById("dinricpostcode_"+getcounter).value;
var dinricstreetname = document.getElementById("dinricstreetname_"+getcounter).value;
var dinricfloor = document.getElementById("dinricfloor_"+getcounter).value;
var dinricunit = document.getElementById("dinricunit_"+getcounter).value;

localStorage.setItem("dipreviewmanuallynricnumber_"+getcounter,dinonresidentnricnumber);
localStorage.setItem("dipreviewmanuallynricgender_"+getcounter,dinonresidentgender);
localStorage.setItem("dipreviewmanuallynricdob_"+getcounter,dinonresidentdob);
localStorage.setItem("dipreviewmanuallynricnationality_"+getcounter,dinricnationality);
localStorage.setItem("dipreviewmanuallynricpostcode_"+getcounter,dinricpostcode);
localStorage.setItem("dipreviewmanuallynricstreetname_"+getcounter,dinricstreetname);
localStorage.setItem("dipreviewmanuallynricfloor_"+getcounter,dinricfloor);
localStorage.setItem("dipreviewnrmanuallyicunit_"+getcounter,dinricunit);


        if(dinonresidentnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(dinonresidentgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(dinonresidentdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(dinricnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
        else if(dinricpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(dinricstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(dinricfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(dinricunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show"); 

	}           
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_DISHAREHOLDER_CITIZENPR_NRIC, true);
	   showNRICEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, true, false);
	   showNRICEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, false, false);
	   $('#dinonresidentpreviewnricmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == 'dishareholderfinmanually'){

var difincardpassportno = document.getElementById("difincardpassportNumber_"+getcounter).value;
var difincardnationality = $('#difincardnationality_'+getcounter+'_chosen span').text();
var difincardcountry = $('#difincardcountry_'+getcounter+'_chosen span').text();
var difincardgender = $("input[name='difincardgender[]']:checked").val();
var difincarddatetimepicker = document.getElementById("difincarddateissue_"+getcounter).value;
var difincarddateexpiry = document.getElementById("difincarddateexpiry_"+getcounter).value;
var difincardplaceofBirth = $('#difincarddateplace_'+getcounter+'_chosen span').text();
var difincarddateBirth = document.getElementById("difincarddatebirth_"+getcounter).value;
//return false;
var difincardemployer = document.getElementById("difincardemployer_"+getcounter).value;
var difincardoccupation = document.getElementById("difincardoccupation_"+getcounter).value;
var diworkpassdateissue = document.getElementById("diworkpassdateissue_"+getcounter).value;
var diworkpassdateexpiry = document.getElementById("diworkpassdateexpiry_"+getcounter).value;
var diwfincardNumber = document.getElementById("diwfincardNumber_"+getcounter).value;



localStorage.setItem("difincardmanuallypassportno_"+getcounter,difincardpassportno);
localStorage.setItem("difincardmanuallynationality_"+getcounter,difincardnationality);
localStorage.setItem("difincardmanuallycountry_"+getcounter,difincardcountry);
localStorage.setItem("difincardmanuallygender_"+getcounter,difincardgender);
localStorage.setItem("difincardmanuallydatetimepicker_"+getcounter,difincarddatetimepicker);
localStorage.setItem("difincardmanuallydateexpiry_"+getcounter,difincarddateexpiry);
localStorage.setItem("difincardmanuallyplaceofBirth_"+getcounter,difincardplaceofBirth);
localStorage.setItem("difincardmanuallydateBirth_"+getcounter,difincarddateBirth);

localStorage.setItem("difincardmanuallyemployer_"+getcounter,difincardemployer);
localStorage.setItem("difincardmanuallyoccupation_"+getcounter,difincardoccupation);
localStorage.setItem("difincardmanuallyworkpassdateissue_"+getcounter,diworkpassdateissue);
localStorage.setItem("difincardmanuallyworkpassdateexpiry_"+getcounter,diworkpassdateexpiry);
localStorage.setItem("difincardmanuallywfincardnumber_"+getcounter,diwfincardNumber);


        if(difincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show");    

	}
	else if(difincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");    


	}
        else if(difincardcountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");    

	}
        else if(difincardgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");    

	}
        else if(difincarddatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");    

	}
        else if(difincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");    


	} 
        else if(difincardplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
              $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");    
  
	}
        else if(difincarddateBirth == ''){

              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");    
  
	}
        else if(difincardemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show");    

	}
	else if(difincardoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show");    
 

	}
	else if(diworkpassdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show");    


	}
	else if(diworkpassdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
            $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
            $("#displaysuccessmsg").modal("show");    


	}
        else if(diwfincardNumber == ''){

            //alert("Fin Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
            $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
            $("#displaysuccessmsg").modal("show");    

	}     
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_DISHAREHOLDER_FIN_PASSPORT, true);
           showFinPassportEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, false);
	   showFINCardEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, true, false);
	   showFINCardEmpty(ROLE_DIRECTOR_SHAREHOLDER, getcounter, false, false);
	   $('#difincardmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director & Shareholder  - FIN Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");    

	}




}



}
</script>
