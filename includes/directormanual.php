<script>
function directorstoredata(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);


if(getresidencystatus == 'directorpassportmanually'){


	var directornonpassportno = document.getElementById("directornonpassportNumber_"+getcounter).value;
	var directornonnationality = $('#directornonnationality_'+getcounter+'_chosen span').text();
	var directornoncountry = $('#directornoncountry_'+getcounter+'_chosen span').text();
	var directornongender = $("input[name='directornongender[]']:checked").val();
	var directornondatetimepicker = document.getElementById("directornondateissue_"+getcounter).value;
	var directornondateexpiry = document.getElementById("directornondateexpiry_"+getcounter).value;
	var directornonplaceofBirth = $('#directornondateplace_'+getcounter+'_chosen span').text();
        var directornondateBirth = document.getElementById("directornondatebirth_"+getcounter).value;

	localStorage.setItem("directornonmanuallypassportno_"+getcounter,directornonpassportno);
	localStorage.setItem("directornonmanuallynationality_"+getcounter,directornonnationality);
	localStorage.setItem("directornonmanuallycountry_"+getcounter,directornoncountry);
	localStorage.setItem("directornonmanuallygender_"+getcounter,directornongender);
	localStorage.setItem("directornonmanuallydatetimepicker_"+getcounter,directornondatetimepicker);
	localStorage.setItem("directornonmanuallydateexpiry_"+getcounter,directornondateexpiry);
	localStorage.setItem("directornonmanuallyplaceofBirth_"+getcounter,directornonplaceofBirth);
        localStorage.setItem("directornonmanuallydateBirth_"+getcounter,directornondateBirth);


        if(directornonpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director - Manual");
	   $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
	   $("#displaysuccessmsg").modal("show");
           return false;

	}
	else if(directornonnationality == 'Please select'){

            //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director - Manual");
	   $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
	   $("#displaysuccessmsg").modal("show");
           return false;


	}
        else if(directornoncountry == 'Please select'){

           //alert("Country cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director - Manual");
	   $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
	   $("#displaysuccessmsg").modal("show");
           return false; 

	}
        else if(directornongender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Director - Manual");
	     $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
	     $("#displaysuccessmsg").modal("show");
             return false;
	}
        else if(directornondatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director - Manual");
	     $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
	     $("#displaysuccessmsg").modal("show");
             return false;
	}
        else if(directornondateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director - Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
	      $("#displaysuccessmsg").modal("show");
              return false;

	} 
        else if(directornonplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director - Manual");
	      $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");
              return false; 
	}
        else if(directornondateBirth == 'Please select'){

              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director - Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");    
	}    
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_DIRECTOR_NONRESIDENT_PASSPORT, true);
           showPassportEmpty(ROLE_DIRECTOR, getcounter, false);
	   $('#directornonresidentpreviewpassportmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director - Manual");
	   $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
	   $("#displaysuccessmsg").modal("show");   
	}

	
}

if(getresidencystatus == 'directornricmanually'){

var directornonresidentnricnumber = document.getElementById("directornonresidentnricnumber_"+getcounter).value;
var directornonresidentgender = $("input[name='directornonresidentgender[]']:checked").val();
var directornonresidentdob = document.getElementById("directornonresidentdob_"+getcounter).value;
var directornricnationality = $('#directornricnationality_'+getcounter+'_chosen span').text();
var directornricpostcode = document.getElementById("directornricpostcode_"+getcounter).value;
var directornricstreetname = document.getElementById("directornricstreetname_"+getcounter).value;
var directornricfloor = document.getElementById("directornricfloor_"+getcounter).value;
var directornricunit = document.getElementById("directornricunit_"+getcounter).value;

localStorage.setItem("directorpreviewmanuallynricnumber_"+getcounter,directornonresidentnricnumber);
localStorage.setItem("directorpreviewmanuallynricgender_"+getcounter,directornonresidentgender);
localStorage.setItem("directorpreviewmanuallynricdob_"+getcounter,directornonresidentdob);
localStorage.setItem("directorpreviewmanuallynricnationality_"+getcounter,directornricnationality);
localStorage.setItem("directorpreviewmanuallynricpostcode_"+getcounter,directornricpostcode);
localStorage.setItem("directorpreviewmanuallynricstreetname_"+getcounter,directornricstreetname);
localStorage.setItem("directorpreviewmanuallynricfloor_"+getcounter,directornricfloor);
localStorage.setItem("directorpreviewnrmanuallyicunit_"+getcounter,directornricunit);


        if(directornonresidentnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(directornonresidentgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Gender cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directornonresidentdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Date of birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directornricnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(directornricpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(directornricstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
             $("#displaysuccessmsg .modal-body").html("Street name cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(directornricfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(directornricunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}           
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_DIRECTOR_CITIZENPR_NRIC, true);
	   showNRICEmpty(ROLE_DIRECTOR, getcounter, true, false);
	   showNRICEmpty(ROLE_DIRECTOR, getcounter, false, false);
	   $('#directornonresidentpreviewnricmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
           $("#displaysuccessmsg").modal("show");
	}

}

if(getresidencystatus == 'directorfinmanually'){

var directorfincardpassportno = document.getElementById("directorfincardpassportNumber_"+getcounter).value;
var directorfincardnationality = $('#directorfincardnationality_'+getcounter+'_chosen span').text();
var directorfincardcountry = $('#directorfincardcountry_'+getcounter+'_chosen span').text();
var directorfincardgender = $("input[name='directorfincardgender[]']:checked").val();
var directorfincarddatetimepicker = document.getElementById("directorfincarddateissue_"+getcounter).value;
var directorfincarddateexpiry = document.getElementById("directorfincarddateexpiry_"+getcounter).value;
var directorfincardplaceofBirth = $('#directorfincarddateplace_'+getcounter+'_chosen span').text();
var directorfincarddateBirth = document.getElementById("directorfincarddatebirth_"+getcounter).value;

//return false;

var directorfincardemployer = document.getElementById("directorfincardemployer_"+getcounter).value;
var directorfincardoccupation = document.getElementById("directorfincardoccupation_"+getcounter).value;
var directorworkpassdateissue = document.getElementById("directorworkpassdateissue_"+getcounter).value;
var directorworkpassdateexpiry = document.getElementById("directorworkpassdateexpiry_"+getcounter).value;
var directorwfincardNumber = document.getElementById("directorwfincardNumber_"+getcounter).value;

localStorage.setItem("directorfincardmanuallypassportno_"+getcounter,directorfincardpassportno);
localStorage.setItem("directorfincardmanuallynationality_"+getcounter,directorfincardnationality);
localStorage.setItem("directorfincardmanuallycountry_"+getcounter,directorfincardcountry);
localStorage.setItem("directorfincardmanuallygender_"+getcounter,directorfincardgender);
localStorage.setItem("directorfincardmanuallydatetimepicker_"+getcounter,directorfincarddatetimepicker);
localStorage.setItem("directorfincardmanuallydateexpiry_"+getcounter,directorfincarddateexpiry);
localStorage.setItem("directorfincardmanuallyplaceofBirth_"+getcounter,directorfincardplaceofBirth);
localStorage.setItem("directorfincardmanuallydateBirth_"+getcounter,directorfincarddateBirth);

localStorage.setItem("directorfincardmanuallyemployer_"+getcounter,directorfincardemployer);
localStorage.setItem("directorfincardmanuallyoccupation_"+getcounter,directorfincardoccupation);
localStorage.setItem("directorfincardmanuallyworkpassdateissue_"+getcounter,directorworkpassdateissue);
localStorage.setItem("directorfincardmanuallyworkpassdateexpiry_"+getcounter,directorworkpassdateexpiry);
localStorage.setItem("directorfincardmanuallywfincardnumber_"+getcounter,directorwfincardNumber);

        if(directorfincardpassportno == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");           

	}
	else if(directorfincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");   

	}
        else if(directorfincardcountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");  
	}
        else if(directorfincardgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");  
	}
        else if(directorfincarddatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");  
	}
        else if(directorfincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");  

	} 
        else if(directorfincardplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        else if(directorfincarddateBirth == ''){

              //alert("Date of Birth cannot be empty");  
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	} 
        else if(directorfincardemployer == ''){

              //alert("Employer cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        else if(directorfincardoccupation == ''){

              //alert("Occupation cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        else if(directorworkpassdateissue == ''){

              //alert("Date of Issue cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
              $("#displaysuccessmsg").modal("show");   
	}
        else if(directorworkpassdateexpiry == ''){

              //alert("Date of Expiry cannot be empty");  
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(directorwfincardNumber == ''){

              //alert("Fin Number cannot be empty");  
              $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}    
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_DIRECTOR_FIN_PASSPORT, true);
           showFinPassportEmpty(ROLE_DIRECTOR, getcounter, false);
	   showFINCardEmpty(ROLE_DIRECTOR, getcounter, true, false);
	   showFINCardEmpty(ROLE_DIRECTOR, getcounter, false, false);
	   $('#directorfincardmanually_'+getcounter).modal('hide');
            $("#displaysuccessmsg .modal-title").html("Director Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $("#displaysuccessmsg").modal("show");  
	}




}



}
</script>
