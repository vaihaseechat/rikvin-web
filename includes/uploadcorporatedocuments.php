<?php
session_start();
$error=array();
$extension=array("jpeg","jpg","png","gif","PDF","pdf");

$_SESSION['id'] = session_id();
if(!is_dir("corporatedocuments/")) {
	mkdir("corporatedocuments/");
}
if(!is_dir("corporatedocuments/". $_SESSION['id'] ."/")) {
	mkdir("corporatedocuments/". $_SESSION['id'] ."/");
}
// Upload section for corporate nonresident

if($_FILES["corporatenonresidentuploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["corporatenonresidentuploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatenonresidentuploadPassport"]["name"][$key];
	$file_tmp = $_FILES["corporatenonresidentuploadPassport"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate non resident address

if($_FILES["corporatenonresidentaddress"]["tmp_name"] != '') {

	foreach($_FILES["corporatenonresidentaddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatenonresidentaddress"]["name"][$key];
	$file_tmp = $_FILES["corporatenonresidentaddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate fin passport

if($_FILES["corporateuploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["corporateuploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporateuploadPassport"]["name"][$key];
	$file_tmp = $_FILES["corporateuploadPassport"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate fin front

if($_FILES["corporatefincardfront"]["tmp_name"] != '') {

	foreach($_FILES["corporatefincardfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatefincardfront"]["name"][$key];
	$file_tmp = $_FILES["corporatefincardfront"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate fin back
if($_FILES["corporatefincardback"]["tmp_name"] != '') {

	foreach($_FILES["corporatefincardback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatefincardback"]["name"][$key];
	$file_tmp = $_FILES["corporatefincardback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate address

if($_FILES["corporateaddress"]["tmp_name"] != '') {

	foreach($_FILES["corporateaddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporateaddress"]["name"][$key];
	$file_tmp = $_FILES["corporateaddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate NRIC front

if($_FILES["corporatenricfront"]["tmp_name"] != '') {

	foreach($_FILES["corporatenricfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatenricfront"]["name"][$key];
	$file_tmp = $_FILES["corporatenricfront"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate NRIC back

if($_FILES["corporatenricback"]["tmp_name"] != '') {

	foreach($_FILES["corporatenricback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatenricback"]["name"][$key];
	$file_tmp = $_FILES["corporatenricback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate corporatecertificate

if($_FILES["corporatecertificate"]["tmp_name"] != '') {

	foreach($_FILES["corporatecertificate"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatecertificate"]["name"][$key];
	$file_tmp = $_FILES["corporatecertificate"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for corporate corporatecertificate docs

if($_FILES["corporatecertifcate1"]["tmp_name"] != '') {

	foreach($_FILES["corporatecertifcate1"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["corporatecertifcate1"]["name"][$key];
	$file_tmp = $_FILES["corporatecertifcate1"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("corporatedocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"corporatedocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

?>

