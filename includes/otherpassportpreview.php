<div class="modal fade"  data-keyboard="true"   id="otherpreviewpassport" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">Passport Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">
					<div class="row mt-3">
						<div class="col-sm">
							<div class="form-group row">
								<div class="col-sm-12">
									Please check details below.
								</div>
								<div class="col-sm-12">
									<div class="passport-scan-copy" id="otherpassportimage"><img src="" alt="" /></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="surname">Surname</label>
								<div class="col-sm-12"><input name="othersurname" class="form-control" id="othersurname"  value="" type="text"></div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="givenName">Given name</label>
								<div class="col-sm-12"><input name="othergivenName" class="form-control" id="othergivenName"  value="" type="text"></div>
							</div>
						</div>
                                          </div>
                                          <div class="row">
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="passportNumber">Passport Number</label>
								<div class="col-sm-12"><input name="otherpassportNumber" class="form-control" id="otherpassportNumber"  value="" type="text"></div>
							</div>
						</div>
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="nationality">Nationality</label>
								<select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othernationality" name="othernationality"></select>
							</div>
						</div> 
					</div>					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Country of Issue</label>
								<select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othercntryissue" name="othercntryissue"></select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Gender</label>
								<div class="col-sm-12">
									<label class="radio-inline mr-3">
									  <input name="otheroptradio" id="othermgender" class="radio-red" checked type="radio" value="Male"> Male
									</label>
									<label class="radio-inline">
									  <input name="otheroptradio" id="otherfgender" class="radio-red" type="radio" value="Female"> Female
									</label>
								</div>
							</div>
						</div>
                                          </div>
                                          <div class="row">
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfBirth">Date of Birth</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
										<input id="otherdob" class="form-control" name="otherdob" value="" type="text" >
										
									</div>
								</div>	
							</div>
						</div>
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfExpiry">Date of Expiry</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
										<input id="otherdateexpiry" class="form-control" name="otherdateexpiry" value="" type="text" >
										
									</div>
								</div>	
							</div>
						</div>
						
					</div>
					
					<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
                                                                <input type="hidden" name="otherpassportfilename" id="otherpassportfilename" value=""> 
								<button class="btn btn-next" onclick="savepreview();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savepreview(){

if( $('#otherpreviewpassport').hasClass('modal fade show') ) {
console.log("Modal is open");
$('body').css("overflow","hidden");
$('#otherpreviewpassport').css("overflow","auto");
}


var othersurname = document.getElementById("othersurname").value;
var othergivenname = document.getElementById("othergivenName").value;
var otherpassportnumber = document.getElementById("otherpassportNumber").value;
var othernationality = $('#othernationality_chosen span').text();
var othercountry = $('#othercntryissue_chosen span').text();
var othergender = $("input[name='otheroptradio']:checked").val();
var otherdob = document.getElementById("otherdob").value;
var otherdateexpiry = document.getElementById("otherdateexpiry").value;
var otherpassportfilename = document.getElementById("otherpassportfilename").value;

var getpreviewdob = formatingdata(otherdob);
var getnonresidentdateexpiry = formatingdata(otherdateexpiry);

var cur_date = new Date();
//var dob =  checkfuturedata(getpreviewdob);
var dob = $('#otherdob').datepicker('getDate');
//alert(othernationality);

        if(othersurname == ''){
           //alert("Surname cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
	else if(othergivenname == ''){

           //alert("Given name cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(otherpassportnumber == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(othernationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(othernationality == ''){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(othercountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
         else if(othercountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(otherdob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(otherdateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");

	} 
        else if(getpreviewdob == true){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Invalid Date of Birth");
              $("#displaysuccessmsg").modal("show");

	} 
        else if(getnonresidentdateexpiry == true){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Invalid Date of Expiry");
              $("#displaysuccessmsg").modal("show");

	}else if(otherdob.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if(otherdateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
       else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        }        
	else{
	   //alert("Saved Succesfully");
	    $('#otherpreviewpassport').modal('hide');
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#otherpreviewmanual").prop('disabled', true);
            $("#displaysuccessmsg").modal("show");
	}




localStorage.setItem("othersurname",othersurname);
localStorage.setItem("othergivenName",othergivenname);
localStorage.setItem("otherpassportnumber",otherpassportnumber);
localStorage.setItem("othernationality",othernationality);
localStorage.setItem("othercntry",othercountry);
localStorage.setItem("othergender",othergender);
localStorage.setItem("otherdob",otherdob);
localStorage.setItem("otherdateexpiry",otherdateexpiry);

}
</script>

