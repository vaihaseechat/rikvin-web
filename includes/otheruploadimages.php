<script src='<?php echo BASE_URL;?>/js/pdfcore2.js'></script>
<script>
var parsecount = 20;
function checkotherPdfParse(myImages,getcounter,callbackfunction)
{

//pdfjsLib.GlobalWorkerOptions.workerSrc="https://mozilla.github.io/pdf.js/build/pdf.worker.js";

var myImage = myImages[0];
var filename = myImage.name;
var fileReader = new FileReader();
var scale = 1.0;
var ext=myImages[0].name.split('.').pop().toLowerCase();
if(ext=="pdf")
{
try{	
console.log(" Getting image data from pdf");

fileReader.onload = function() {

	//Step 4:turn array buffer into typed array
	var typedarray = new Uint8Array(this.result);

	//Step 5:PDFJS should be able to read this
	pdfjsLib.getDocument(typedarray).then(function(pdf) {
		 pdf.getPage(1).then(function(page) {
	 
			var viewport = page.getViewport(scale);
			var canvas = document.createElement('canvas') , ctx = canvas.getContext('2d');
			var renderContext = { canvasContext: ctx, viewport: viewport };
			canvas.height = viewport.height;
			canvas.width = viewport.width;
			page.render(renderContext).then(function() {									
				var file_blob=[];
				file_blob.push(dataURItoBlob(canvas.toDataURL()));
				callbackfunction(file_blob,getcounter,filename);	
			});
		});
	});


};
fileReader.readAsArrayBuffer(myImage);
}catch (err) {

	console.log(err);
	callbackfunction(myImages,getcounter,filename);
}
}
else{
	//alert(callbackfunction);
	var extension =myImages[0].name.split('.').pop().toLowerCase(); 
        console.log(extension);
        if((extension == "png") || (extension == "jpg") || (extension == "jpeg") || (extension == "gif"))
        {
	  callbackfunction(myImages,getcounter,filename);
        }else{
           $("#displaysuccessmsg .modal-title").html("Error");
	   $("#displaysuccessmsg .modal-body").html("Invalid File type");
	   $("#displaysuccessmsg").modal("show");
        }
}
	
}




function dataURItoBlob(dataURI) {
// convert base64/URLEncoded data component to raw binary data held in a string
var byteString;
if (dataURI.split(',')[0].indexOf('base64') >= 0)
    byteString = atob(dataURI.split(',')[1]);
else
    byteString = unescape(dataURI.split(',')[1]);
// separate out the mime component
var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
// write the bytes of the string to a typed array
var ia = new Uint8Array(byteString.length);
for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
}
return new Blob([ia], {type:mimeString});
}

function othernonuploadpassportfile(getdata,getcounter,filename){

$("#othernonpassportloadingImage").css("display","block");
//alert(getcounter);
var file = getdata[0];  
var form = new FormData();
form.append('image', file);

//alert(filename);
//alert(URL.createObjectURL(file));

var srcfilename = URL.createObjectURL(file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");


           $.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error;
                         //print_r($_FILES);
 
                         if(Errorstatus == 'Poor Image Quality') {                           
                           
                            //alert(Errorstatus);
                            $("#displaysuccessmsg .modal-title").html("Passport Preview - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
                            $("#othernonpassport").css("display", "none");
                            $("#othernonpassportloadingImage").css("display", "none");
                            $("#othernonresidentlabel").text();

                         } else{
                            //alert("fdfdfdf");
                            $("#othernonpassport").css("display", "block");
                            $("#othernonpassportloadingImage").css("display", "none");
                            $("#othernonresidentlabel").text(finallabel);
                            $("#otherpassportimage img").attr("src", srcfilename);

                                // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = obj.DateofIssue; 
                                 var PlaceofBirth = obj.PlaceofBirth;                 
		                 

                                 // Displaying values from OCR - API

                                 $('#othergivenName').val(gname);
                                 $('#othersurname').val(Surname);
                                 $('#otherpassportfilename').val(filename);
                                 $('#otherpassportNumber').val(passportno);
                                 //$('#othernonresidentdatetimepicker').val(DateofIssue);
                                 $('#otherdateexpiry').val(DateofExpiry);
                                 $('#otherdob').val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#othermgender').prop('checked', true);
                                  $('#otherfgender').prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#otherfgender').prop('checked', true);
                                  $('#othermgender').prop('checked', false);
                                 }
                                 $('#othernationality_chosen span').text(Nationality);
                                 $('#othercntryissue_chosen span').text(CountryofIssue);
                                 //$('#othernonresidentplaceofBirthchosen span').text(PlaceofBirth);

                                 $('#otherpreviewpassport').modal('show');

                                 $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				});
                         

                         }     

                    }

        });

}

function othernricfrontupload(getdata,getcounter,filename){

$("#othernricloadingImage").css("display","block");
//alert(getcounter);
var file = getdata[0];  
var form = new FormData();
form.append('image', file);
//alert(filename);

var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

$.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		  console.log(URL.createObjectURL(file));
		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("NRIC - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#othernriccardfrontpreview").css("display", "none");
                    $("#othernricloadingImage").css("display", "none"); 
                    $("#othernricfrontlabel").text();

		 } else{

                   $("#othernriccardfrontpreview").css("display", "block");
                   $("#othernricloadingImage").css("display", "none");
                   $("#othernricfrontlabel").text(finallabel); 
                   $("#othernricfrontimage img").attr("src", srcfilename);     
                   $('#othernricfrontfilename').val(filename);
                                   
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;
                         var streetname = obj.Street;
                         var postcode = obj.Postal;
                         var flooring = obj.Floor;
                         var unit = obj.Unit; 

                         if(sex == 'Male'){
                          $('#otherpreviewnricfrontmgender').prop('checked', true);
                          $('#otherpreviewnricfrontfgender').prop('checked', false); 
                         }
                         else if(sex == 'Female'){
                          $('#otherpreviewnricfrontfgender').prop('checked', true);
                          $('#otherpreviewnricfrontmgender').prop('checked', false);
                         }
                         $('#otherpreviewfrontnricnumber').val(nricno);
                         $('#otherpreviewfrontnriccob_chosen span').text(Cntryofbirth); 
                         $('#otherpreviewfrontnricdoi').val(dobs);
                         $('#otherpreviewfrontnricpostcode').val(postcode);
                         $('#otherpreviewfrontnricstreetname').val(streetname);
                         $('#otherpreviewfrontnricfloor').val(flooring);
                         $('#otherpreviewfrontnricunit').val(unit);
                         $('#othernricfrontpreview').modal('show');
                     
                          $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
			  });  
                      

		 }
	    }
}); 


}

function othernricbackupload(getdata,getcounter,filename){

$("#othernricloadingImage").css("display","block");
//alert(getcounter);
var file = getdata[0];  
var form = new FormData();
form.append('image', file);
//alert(filename);

var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

$.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		  console.log(URL.createObjectURL(file));
		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("NRIC - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#othernriccardbackpreview").css("display", "none");
                    $("#othernricloadingImage").css("display", "none"); 
                    $("#othernricbacklabel").text();

		 } else{

                   $("#othernriccardbackpreview").css("display", "block");
                   $("#othernricloadingImage").css("display", "none");
                   $("#othernricbacklabel").text(finallabel);                    
                   $("#othernricbackimage img").attr("src", srcfilename);  
                   $('#othernricbackfilename').val(filename);
                                   
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;
                         var streetname = obj.Street;
                         var postcode = obj.Postal;
                         var flooring = obj.Floor;
                         var unit = obj.Unit; 

                         if(sex == 'Male'){
                          $('#otherpreviewnricbackmgender').prop('checked', true);
                          $('#otherpreviewnricbackfgender').prop('checked', false); 
                         }
                         else if(sex == 'Female'){
                          $('#otherpreviewnricbackfgender').prop('checked', true);
                          $('#otherpreviewnricbackmgender').prop('checked', false);
                         }
                         $('#otherpreviewbacknricnumber').val(nricno);
                         $('#otherpreviewbacknriccob_chosen span').text(Cntryofbirth); 
                         $('#otherpreviewbacknricdoi').val(dobs);
                         $('#otherpreviewbacknricpostcode').val(postcode);
                         $('#otherpreviewbacknricstreetname').val(streetname);
                         $('#otherpreviewbacknricfloor').val(flooring);
                         $('#otherpreviewbacknricunit').val(unit);
                         $('#othernricbackpreview').modal('show');

                          $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
			 });
                      

		 }
	    }
}); 


}

function otherfinuploadpassportfile(getdata,getcounter,filename){

$("#otherfinloadingImage").css("display","block");
//alert(getcounter);
var file = getdata[0];  
var form = new FormData();
form.append('image', file);
//alert(filename);


var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

           $.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error;
 
                         if(Errorstatus == 'Poor Image Quality') {                           
                           
                            //alert(Errorstatus);
                            $("#displaysuccessmsg .modal-title").html("Passport Preview - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
                            $("#otherpassportfin").css("display", "none");
                            $("#otherfinloadingImage").css("display", "none");
                            $("#finuploadPassportlabel").text();

                         } else{
                            //alert("fdfdfdf");
                            $("#otherpassportfin").css("display", "block");
                            $("#otherfinloadingImage").css("display", "none");
                            $("#finuploadPassportlabel").text(finallabel);
                            $("#otherfinimage img").attr("src", srcfilename); 

                                // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = obj.DateofIssue; 
                                 var PlaceofBirth = obj.PlaceofBirth;                  
		                 

                                 // Displaying values from OCR - API
                                 $('#otherfinpassportfilename').val(filename);
                                 $('#otherfingivenName').val(gname);
                                 $('#otherfinsurname').val(Surname);
                                 $('#otherfinpassportNumber').val(passportno);
                                 //$('#othernonresidentdatetimepicker').val(DateofIssue);
                                 $('#otherfindateexpiry').val(DateofExpiry);
                                 $('#otherfindob').val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#otherfinmgender').prop('checked', true);
                                  $('#otherfinfgender').prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#otherfinfgender').prop('checked', true);
                                  $('#otherfinmgender').prop('checked', false);
                                 }
                                 $('#otherfinnationality_chosen span').text(Nationality);
                                 $('#otherfincntryissue_chosen span').text(CountryofIssue);
                                 //$('#othernonresidentplaceofBirthchosen span').text(PlaceofBirth);
                                 $('#otherpreviewfinpassport').modal('show');

                                  $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				});
                         

                         }     

                    }

        });

}

function otherfinfrontfile(getdata,getcounter,filename){

$("#otherfincardloadingImage").css("display","block");
//alert(getcounter);
var file = getdata[0];  
var form = new FormData();
form.append('image', file);
//alert(filename);

var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

	$.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/ep-front/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
		  
			 console.log(response);
			 var obj = JSON.parse(response);                         
			 var Errorstatus = obj.Error; 

			 if(Errorstatus == 'Poor Image Quality') { 

		            $("#displaysuccessmsg .modal-title").html("FIN - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
		            $("#otherfincardfrontpreview").css("display", "none"); 
		            $("#otherfincardloadingImage").css("display", "none"); 
		            $("#otherfinfrontuploadlabel").text();

			 } else{

		           $("#otherfincardfrontpreview").css("display", "block"); 
		           $("#otherfincardloadingImage").css("display", "none");
		           $("#otherfinfrontuploadlabel").text(finallabel);
                           $("#otherfinfrontimage img").attr("src", srcfilename);
                           $('#otherfinfrontfilename').val(filename);

		                 var Occupation = obj.Occupation;
			         var Name = obj.Name;
			         var DateofIssue = obj.DateofIssue;
			         var DateofApplication = obj.DateofApplication;
		                 var DateofExpiry = obj.DateofExpiry;
			         var FIN = obj.FIN; 		               
			         var Employer = obj.Employer;


		                 $('#otherfinfrontemployer').val(Employer);
		                 $('#otherfinfrontoccupation').val(Occupation);
		                 $('#otherfinfrontdoi').val(DateofIssue);
		                 $('#otherfinfrontdoe').val(DateofExpiry);
		                 $('#otherfinfrontnumber').val(FIN); 
		                 $('#otherfinfrontpreview').modal('show');

                                  $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				});
	 
			 }
		    }
	   }); 

}

function otherfinbackfile(getdata,getcounter,filename){

$("#otherfincardloadingImage").css("display","block");
//alert(getcounter);
var file = getdata[0];  
var form = new FormData();
form.append('image', file);
//alert(filename);

var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

	$.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/ep-back/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
		  
			 console.log(response);
			 var obj = JSON.parse(response);                         
			 var Errorstatus = obj.Error; 

			 if(Errorstatus == 'Poor Image Quality') { 

		            $("#displaysuccessmsg .modal-title").html("FIN - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
		            $("#otherfincardbackpreview").css("display", "none"); 
		            $("#otherfincardloadingImage").css("display", "none"); 
		            $("#otherfinbackuploadlabel").text();

			 } else{

		           $("#otherfincardbackpreview").css("display", "block"); 
		           $("#otherfincardloadingImage").css("display", "none");
		           $("#otherfinbackuploadlabel").text(finallabel);
                           $("#otherfinbackimage img").attr("src", srcfilename);
                           $('#otherfinbackfilename').val(filename);

		                 var Occupation = obj.Occupation;
			         var Name = obj.Name;
			         var DateofIssue = obj.DateofIssue;
			         var DateofApplication = obj.DateofApplication;
		                 var DateofExpiry = obj.DateofExpiry;
			         var FIN = obj.FIN; 		               
			         var Employer = obj.Employer;


		                 $('#otherfinbackemployer').val(Employer);
		                 $('#otherfinbackoccupation').val(Occupation);
		                 $('#otherfinbackdoi').val(DateofIssue);
		                 $('#otherfinbackdoe').val(DateofExpiry);
		                 $('#otherfinbacknumber').val(FIN); 
		                 $('#otherfinbackpreview').modal('show');

                                $.ajax({
				    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
				    type: 'post',
				    data: form,
				    contentType: false,
				    processData: false,
				    success: function(response){
					
				    },
				});
	 
			 }
		    }
	   }); 

}

</script>
