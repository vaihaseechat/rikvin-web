<script>
function directorpopup(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);

//Storing Director Non Resident

if(getresidencystatus == 'directornonresident'){

var directornonresidentsurname = document.getElementById("directornonresidentpsurname_"+getcounter).value;
var directornonresidentgivenname = document.getElementById("directornonresidentpgivenName_"+getcounter).value;
var directornonresidentpassportno = document.getElementById("directornonresidentppassportNumber_"+getcounter).value;
var directornonresidentnationality = $('#directornonresidentpnationality_'+getcounter+'_chosen span').text();
var directornonresidentcountry = $('#directornonresidentpcountry_'+getcounter+'_chosen span').text();
var directornonresidentgender = $("input[name='directornonresidentpgender[]']:checked").val();
var directorpreviewdob = document.getElementById("directornonresidentpdob_"+getcounter).value;
var directornonresidentdateexpiry = document.getElementById("directornonresidentpdateexpiry_"+getcounter).value;


localStorage.setItem("directornonresidentpopupsurname_"+getcounter,directornonresidentsurname);
localStorage.setItem("directornonresidentpopupgivenName_"+getcounter,directornonresidentgivenname);
localStorage.setItem("directornonresidentpopuppassportno_"+getcounter,directornonresidentpassportno);
localStorage.setItem("directornonresidentpopupnationality_"+getcounter,directornonresidentnationality);
localStorage.setItem("directornonresidentpopupcountry_"+getcounter,directornonresidentcountry);
localStorage.setItem("directornonresidentpopupgender_"+getcounter,directornonresidentgender);
localStorage.setItem("directornonresidentpopupdob_"+getcounter,directorpreviewdob);
localStorage.setItem("directornonresidentpopupdateexpiry_"+getcounter,directornonresidentdateexpiry);

	if(directornonresidentsurname == ''){
           //alert("Surname cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director - Preview");
            $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
	else if(directornonresidentgivenname == ''){

           //alert("Given name cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director - Preview");
            $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(directornonresidentpassportno == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director - Preview");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(directornonresidentnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(directornonresidentcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(directornonresidentgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Director - Preview");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");
	}
        else if(directorpreviewdob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(directornonresidentdateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");

	} 
        /*else if(directornonresidentplaceofBirth == ''){

              alert("Place of Birth cannot be empty");  
	}*/    
	else{
	   //alert("Saved Succesfully");
	    $('#directornonresidentpreviewpassport_'+getcounter).modal('hide');
            $("#displaysuccessmsg .modal-title").html("Director - Preview");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $("#displaysuccessmsg").modal("show");
	}
}

if(getresidencystatus == 'directornricfront'){

var directorpreviewfrontnricnumber = document.getElementById("directorpreviewfrontnricnumber_"+getcounter).value;
var directorpreviewnricfrontgender = $("input[name='directorpreviewnricfrontgender[]']:checked").val();
var directorpreviewnricfrontdob = document.getElementById("directorpreviewnricfrontdob_"+getcounter).value;
var directorpreviewnricfrontnationality = $('#directorpreviewnricfrontnationality_'+getcounter+'_chosen span').text();
var directorpreviewnricfrontpostcode = document.getElementById("directorpreviewnricfrontpostcode_"+getcounter).value;
var directorpreviewnricfrontstreetname = document.getElementById("directorpreviewnricfrontstreetname_"+getcounter).value;
var directorpreviewnricfrontfloor = document.getElementById("directorpreviewnricfrontfloor_"+getcounter).value;
var directorpreviewnricfrontunit = document.getElementById("directorpreviewnricfrontunit_"+getcounter).value;

localStorage.setItem("directorpreviewpopupfrontnricnumber_"+getcounter,directorpreviewfrontnricnumber);
localStorage.setItem("directorpreviewpopupnricfrontgender_"+getcounter,directorpreviewnricfrontgender);
localStorage.setItem("directorpreviewpopupnricfrontdob_"+getcounter,directorpreviewnricfrontdob);
localStorage.setItem("directorpreviewpopupnricfrontnationality_"+getcounter,directorpreviewnricfrontnationality);
localStorage.setItem("directorpreviewpopupnricfrontpostcode_"+getcounter,directorpreviewnricfrontpostcode);
localStorage.setItem("directorpreviewpopupnricfrontstreetname_"+getcounter,directorpreviewnricfrontstreetname);
localStorage.setItem("directorpreviewpopupnricfrontfloor_"+getcounter,directorpreviewnricfrontfloor);
localStorage.setItem("directorpreviewnrpopupicfrontunit_"+getcounter,directorpreviewnricfrontunit);


        if(directorpreviewfrontnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(directorpreviewnricfrontgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directorpreviewnricfrontdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directorpreviewnricfrontnationality == 'Please select'){

           //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
        else if(directorpreviewnricfrontpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(directorpreviewnricfrontstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(directorpreviewnricfrontfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(directorpreviewnricfrontunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#directornricfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}


}

if(getresidencystatus == 'directornricback'){

var directorpreviewbacknricnumber = document.getElementById("directorpreviewbacknricnumber_"+getcounter).value;
var directorpreviewnricbackgender = $("input[name='directorpreviewnricbackgender[]']:checked").val();
var directorpreviewnricbackdob = document.getElementById("directorpreviewnricbackdob_"+getcounter).value;
var directorpreviewnricbacknationality = $('#directorpreviewnricbacknationality_'+getcounter+'_chosen span').text();
var directorpreviewnricbackpostcode = document.getElementById("directorpreviewnricbackpostcode_"+getcounter).value;
var directorpreviewnricbackstreetname = document.getElementById("directorpreviewnricbackstreetname_"+getcounter).value;
var directorpreviewnricbackfloor = document.getElementById("directorpreviewnricbackfloor_"+getcounter).value;
var directorpreviewnricbackunit = document.getElementById("directorpreviewnricbackunit_"+getcounter).value;

localStorage.setItem("directorpreviewpopupbacknricnumber_"+getcounter,directorpreviewbacknricnumber);
localStorage.setItem("directorpreviewpopupnricbackgender_"+getcounter,directorpreviewnricbackgender);
localStorage.setItem("directorpreviewpopupnricbackdob_"+getcounter,directorpreviewnricbackdob);
localStorage.setItem("directorpreviewpopupnricbacknationality_"+getcounter,directorpreviewnricbacknationality);
localStorage.setItem("directorpreviewpopupnricbackpostcode_"+getcounter,directorpreviewnricbackpostcode);
localStorage.setItem("directorpreviewpopupnricbackstreetname_"+getcounter,directorpreviewnricbackstreetname);
localStorage.setItem("directorpreviewpopupnricbackfloor_"+getcounter,directorpreviewnricbackfloor);
localStorage.setItem("directorpreviewnrpopupicbackunit_"+getcounter,directorpreviewnricbackunit);


        if(directorpreviewbacknricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(directorpreviewnricbackgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directorpreviewnricbackdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directorpreviewnricbacknationality == 'Please select'){

           //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
        else if(directorpreviewnricbackpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(directorpreviewnricbackstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(directorpreviewnricbackfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(directorpreviewnricbackunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#directornricbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}


}
if(getresidencystatus == 'directorfinpassport'){


var directorfincardsurname = document.getElementById("directorfincardpsurname_"+getcounter).value;
var directorfincardgivenname = document.getElementById("directorfincardpgivenName_"+getcounter).value;
var directorfincardpassportno = document.getElementById("directorfincardppassportNumber_"+getcounter).value;
var directorfincardnationality = $('#directorfincardpnationality_'+getcounter+'_chosen span').text();
var directorfincardcountry = $('#directorfincardpcountry_'+getcounter+'_chosen span').text();
var directorfincardgender = $("input[name='directorfincardpgender[]']:checked").val();
//var directorfincarddatetimepicker = document.getElementById("directorfincarddatetimepicker_"+getcounter).value;
var directorfincarddateexpiry = document.getElementById("directorfincardpdateexpiry_"+getcounter).value;
//var directorfincardplaceofBirth = $('#directorfincardplaceofBirth_'+getcounter+'_chosen span').text();
var directorfincarddob = document.getElementById("directorfincardpdob_"+getcounter).value;
//alert(directorfincardnationality);return false;

//alert(directorsurname);

localStorage.setItem("directorfincardpopupsurname_"+getcounter,directorfincardsurname);
localStorage.setItem("directorfincardpopupgivenName_"+getcounter,directorfincardgivenname);
localStorage.setItem("directorfincardpopuppassportno_"+getcounter,directorfincardpassportno);
localStorage.setItem("directorfincardpopupnationality_"+getcounter,directorfincardnationality);
localStorage.setItem("directorfincardpopupcountry_"+getcounter,directorfincardcountry);
localStorage.setItem("directorfincardpopupgender_"+getcounter,directorfincardgender);
//localStorage.setItem("directorfincardpopupdatetimepicker_"+getcounter,directorfincarddatetimepicker);
localStorage.setItem("directorfincardpopupdateexpiry_"+getcounter,directorfincarddateexpiry);
//localStorage.setItem("directorfincardpopupplaceofBirth_"+getcounter,directorfincardplaceofBirth);
localStorage.setItem("directorfincardpopupdob_"+getcounter,directorfincarddob);

        if(directorfincardsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(directorfincardgivenname == ''){

           //alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directorfincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(directorfincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(directorfincardcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}        
        else if(directorfincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show");
	} 
        else if(directorfincarddob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}        
	else{
	   //alert("Saved Succesfully");
	   $('#directorfincardpreviewpassport_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}

}

if(getresidencystatus == 'directorfincardfront'){

var directorfincardfrontemployer = document.getElementById("directorfincardfrontemployer_"+getcounter).value;
var directorfincardfrontoccupation = document.getElementById("directorfincardfrontoccupation_"+getcounter).value;
var directorfincardfrontdateissue = document.getElementById("directorfincardfrontdateissue_"+getcounter).value;
var directorfincardfrontdateexpiry = document.getElementById("directorfincardfrontdateexpiry_"+getcounter).value;
var directorwfincardfrontNumber = document.getElementById("directorwfincardfrontNumber_"+getcounter).value;

localStorage.setItem("directorfincardpopupfrontemployer_"+getcounter,directorfincardfrontemployer);
localStorage.setItem("directorfincardpopupfrontoccupation_"+getcounter,directorfincardfrontoccupation);
localStorage.setItem("directorfincardpopupfrontdateissue_"+getcounter,directorfincardfrontdateissue);
localStorage.setItem("directorfincardpopupfrontdateexpiry_"+getcounter,directorfincardfrontdateexpiry);
localStorage.setItem("directorwfincardpopupfrontNumber_"+getcounter,directorwfincardfrontNumber);

        if(directorfincardfrontemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director FIN FRONT - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(directorfincardfrontoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director FIN FRONT - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(directorfincardfrontdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director FIN FRONT - Preview");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show");  

	}
	else if(directorfincardfrontdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director FIN FRONT - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
             $("#displaysuccessmsg").modal("show"); 

	}
        else if(directorwfincardfrontNumber == ''){

            //alert("Fin Number cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director FIN FRONT - Preview");
             $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#directorfincardfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director FIN FRONT - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == 'directorfincardback'){

var directorfincardbackemployer = document.getElementById("directorfincardbackemployer_"+getcounter).value;
var directorfincardbackoccupation = document.getElementById("directorfincardbackoccupation_"+getcounter).value;
var directorfincardbackdateissue = document.getElementById("directorfincardbackdateissue_"+getcounter).value;
var directorfincardbackdateexpiry = document.getElementById("directorfincardbackdateexpiry_"+getcounter).value;
var directorwfincardbackNumber = document.getElementById("directorwfincardbackNumber_"+getcounter).value;

localStorage.setItem("directorfincardpopupbackemployer_"+getcounter,directorfincardbackemployer);
localStorage.setItem("directorfincardpopupbackoccupation_"+getcounter,directorfincardbackoccupation);
localStorage.setItem("directorfincardpopupbackdateissue_"+getcounter,directorfincardbackdateissue);
localStorage.setItem("directorfincardpopupbackdateexpiry_"+getcounter,directorfincardbackdateexpiry);
localStorage.setItem("directorwfincardpopupbackNumber_"+getcounter,directorwfincardbackNumber);

        if(directorfincardbackemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(directorfincardbackoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(directorfincardbackdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director FIN back - Preview");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show");  

	}
	else if(directorfincardbackdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director FIN back - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
             $("#displaysuccessmsg").modal("show"); 

	}
        else if(directorwfincardbackNumber == ''){

            //alert("Fin Number cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director FIN back - Preview");
             $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#directorfincardbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

}
</script>
