<script>

var maxGroup = 5;
var counter = 0;
var counter1 = 0;
var counter2 = 0;
var counter3 = 0;

function additionalselector(getvalue, isFirst){

//alert(getvalue);
if(isFirst == false){
   if(validateForm() != true){
      $(getvalue).val("");
      return;
   }
}
if(isFirst == true){
   counter = 0;
   counter1 = 0;
   counter2 = 0;
   counter3 = 0;
}

var sel = document.getElementById('roleSelector');
var roleoptions = sel.options[sel.selectedIndex].value;

if(roleoptions != ''){
   if(isFirst == false){
      $('#roleSelector').attr("disabled","disabled");
   }
} else{
   $("#additionalRole").hide(); 
}
   
	if(getvalue.value == 'Director') {
      counter = (isFirst==true) ? 1 : counter+1;
      /*counter++;*/          
		if(counter <= maxGroup){
		    var fieldHTML = '<div id="director_'+counter+'" class="divclass">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Director #'+counter+' </h5></div></div>'; // end of background-grey1

                     fieldHTML += '<div class="col-lg-12 background-grey-2">';
                     fieldHTML += '<div class="form-group row mt-4" id="names_'+counter+'"><div class="col-lg-4"><label class="col-form-label required">Full Name</label></div><div class="col-lg-8"><input name="surname[]" class="form-control" id="surname_'+counter+'" placeholder="" type="text"><div class="fullName'+counter+'-alert" id="fullName'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="fullName'+counter+'-alert-msg"></span></div></div></div>'; // end of fullname 

                     fieldHTML += '<div class="form-group row" id="directorstatus_'+counter+'"><div class="col-lg-4"><label class="col-form-label required">Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="directorresstatus_'+counter+'" name="directorresidencyStatus[]" onchange="residencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="directornonresident_'+counter+'" rel="nonresident">Non-resident</option><option value="directorcitizenpr_'+counter+'" rel="citizenpr">Singapore Citizen / PR</option><option value="directorpassholder_'+counter+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="residencyStatus'+counter+'-alert" id="residencyStatus'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="residencyStatus'+counter+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                     fieldHTML += '<div id="directorpassholder_'+counter+'" class="choose-file form-group" style="display: none;">';

                     fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directoruploadPassport[]" class="custom-file-input" id="directoruploadPassport_'+counter+'" type="file" onchange="checkPdfParse(this.files,'+counter+',finuploadpassportfile,\'director\');"><label class="custom-file-label dashed-lines" id="directorfinpassportlabel_'+counter+'">Passport</label></div></div><div class="directoruploadPassport'+counter+'-alert" id="directoruploadPassport'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directoruploadPassport'+counter+'-alert-msg"></span></div><div class="row"><div id="bdirectorfincardpreviewpassport_'+counter+'" class="col-sm-12" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#directorfincardpreviewpassport_'+counter+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your Passport details</button></div></div><div class="row" id="directoruploadPassportloadingImage_'+counter+'" style="display:none;"><div class="col-sm-6 text-center"><img src="images/image-loading.gif"></div></div></div>OR<div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#directorfincardmanually_'+counter+'" id="directorbuttonfincardmanually_'+counter+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div>';

	            fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directorfincardfront[]" class="custom-file-input" id="directorfincardfront_'+counter+'" type="file" onchange="checkPdfParse(this.files,'+counter+',fincardfront,\'director\');"><label class="custom-file-label dashed-lines" id="directorfincardfrontlabel_'+counter+'">FIN Card Front</label></div></div><div class="directorfincardfront'+counter+'-alert" id="directorfincardfront'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directorfincardfront'+counter+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directorfincardback[]" class="custom-file-input" id="directorfincardback_'+counter+'" type="file" onchange="checkPdfParse(this.files,'+counter+',fincardback,\'director\');"><label class="custom-file-label dashed-lines" id="directorfincardbacklabel_'+counter+'">FIN Card Back</label></div></div><div class="directorfincardback'+counter+'-alert" id="directorfincardback'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directorfincardback'+counter+'-alert-msg"></span></div></div></div><div class="row"><div id="bdirectorfincardfrontpreview_'+counter+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#directorfincardfrontpreview_'+counter+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your front fin details</button></div><div id="bdirectorfincardbackpreview_'+counter+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#directorfincardbackpreview_'+counter+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your fin back details</button></div></div><div class="row" id="directoruploadfincardloadingImage_'+counter+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


	           fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" name="directoraddress[]" class="custom-file-input" id="directoraddress_'+counter+'" type="file" onchange="directoraddress(this.files,'+counter+');"><label class="custom-file-label dashed-lines" id="directoraddresslabel_'+counter+'">Address</label></div></div><div class="directoraddress'+counter+'-alert" id="directoraddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directoraddress'+counter+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="directorresaddress[]" id="directorresaddress_'+counter+'" rows="2" placeholder="Please key in residential address"></textarea><div class="directorresaddress'+counter+'-alert" id="directorresaddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directorresaddress'+counter+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="directorcitizenpr_'+counter+'" class="choose-file" style="display: none;">';

                     fieldHTML +='<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directornricfront[]" class="custom-file-input" id="directornricfront_'+counter+'" type="file" onchange="checkPdfParse(this.files,'+counter+',nricfront,\'director\');"><label class="custom-file-label dashed-lines" id="directornricfrontlabel_'+counter+'">NRIC Front</label></div></div><div class="directornricfront'+counter+'-alert" id="directornricfront'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directornricfront'+counter+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directornricback[]" class="custom-file-input" id="directornricback_'+counter+'" type="file" onchange="checkPdfParse(this.files,'+counter+',nricback,\'director\');"><label class="custom-file-label dashed-lines" id="directornricbacklabel_'+counter+'">NRIC Back</label></div></div><div class="nricback'+counter+'-alert" id="directornricback'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directornricback'+counter+'-alert-msg"></span></div></div></div><div class="row"><div id="bdirectornricfrontpreview_'+counter+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#directornricfrontpreview_'+counter+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Front details</button></div><div id="bdirectornricbackpreview_'+counter+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#directornricbackpreview_'+counter+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Back details</button></div></div><div class="row" id="directoruploadnricloadingImage_'+counter+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div><div class="row"><div class="col-sm-6"><button type="button" id="directorbuttonnricmanually_'+counter+'" class="btn btn-style-1" data-toggle="modal" data-target="#directornonresidentpreviewnricmanually_'+counter+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="directornonresident_'+counter+'" class="choose-file" style="display:none;">';

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" >Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directornonresidentuploadPassport[]" class="custom-file-input" id="directornonresidentuploadpassport_'+counter+'" type="file" onchange="checkPdfParse(this.files,'+counter+',nonuploadpassportfile,\'director\');"><label class="custom-file-label dashed-lines" id="directornonresidentlabel_'+counter+'">Passport</label></div></div><div class="directornonresidentuploadpassport'+counter+'-alert" id="directornonresidentuploadpassport'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directornonresidentuploadpassport'+counter+'-alert-msg"></span></div></div>OR<div class="col-sm"><button id="directorbuttonpassportmanually_'+counter+'" type="button" class="btn btn-style-1" data-toggle="modal" data-target="#directornonresidentpreviewpassportmanually_'+counter+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div  id="bdirectornonresidentpreviewpassport_'+counter+'" class="col-sm-6"  style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#directornonresidentpreviewpassport_'+counter+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div><div class="row" id="directoruploadpassportfileloadingImage_'+counter+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" name="directornonresidentaddress[]" class="custom-file-input" id="directornonresidentaddress_'+counter+'" type="file" onchange="directornonresidentaddress(this.files,'+counter+');"><label class="custom-file-label dashed-lines" id="directornonresidentaddresslabel_'+counter+'">Address</label></div></div><div class="directornonresidentaddress'+counter+'-alert" id="directornonresidentaddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directornonresidentaddress'+counter+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="directornonresidentresaddress[]" id="directornonresidentresaddress_'+counter+'" rows="2" placeholder="Please key in residential address"></textarea><div class="directornonresidentresaddress'+counter+'-alert" id="directornonresidentresaddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directornonresidentresaddress'+counter+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident

                     fieldHTML += '<div class="form-group row" id="phnumbs_'+counter+'"><label class="col-lg-4 col-form-label required" >Contact Numbers</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="directorhandphone_'+counter+'" name="directorhandPhone[]" class="form-control mb-2" placeholder="Hand phone" type="text"><div class="contactNumber'+counter+'-alert" id="contactNumber'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="contactNumber'+counter+'-alert-msg"></span></div></div><div class="col-sm"><input id="offnumber_'+counter+'" name="officeNumber[]" class="form-control mb-2" placeholder="Office number" type="text"><div class="officeNumber'+counter+'-alert" id="officeNumber'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="officeNumber'+counter+'-alert-msg"></span></div></div></div></div> </div>'; // end of contact numbers

                     fieldHTML += '<div class="form-group row" id="emailportion_'+counter+'"><label class="col-lg-4 col-form-label required" >Email Address(es) </label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="directorbusinessemail_'+counter+'" name="directorbusinessEmail[]" class="form-control mb-2" placeholder="Business email"  type="text"><div class="bizEmail'+counter+'-alert" id="bizEmail'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bizEmail'+counter+'-alert-msg"></span></div></div><div class="col-sm"><input id="personalemail_'+counter+'" name="personalEmail[]" class="form-control mb-2" id="" placeholder="Personal email" type="text"><div class="personalEmail'+counter+'-alert" id="personalEmail'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="personalEmail'+counter+'-alert-msg"></span></div></div></div></div></div>'; // end of email addresses  

                     fieldHTML += '<div class="form-group row" id="politicallyexposed_'+counter+'"><label class="col-lg-4 col-form-label required" data-toggle="tooltip" data-placement="right"  title="Politically Exposed Persons (PEPs) are individuals who are or have been entrusted with prominent public functions in a foreign country, for example Heads of State or of government, senior politicians, senior government, judicial or High Ranking military officials, senior executives of state owned corporations, important political party officials. Business relationships with family members or close associates of PEP.">Are you a Politically Exposed Person (PEP) <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input name="directorpep_'+counter+'" id="directorpep_'+counter+'" value="directorpepyes_'+counter+'" type="radio" onclick="ShowHidedDiv(this.value)"><span class="custom-radio-label">Yes</span></label><div class="politicallyExposed'+counter+'-alert" id="politicallyExposed'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="politicallyExposed'+counter+'-alert-msg"></span></div></div><div class="col-sm"><label class="custom-radio-field"><input id="directorpep_'+counter+'" name="directorpep_'+counter+'" value="notdirectorpep_'+counter+'" checked="checked" type="radio" onclick="ShowHidedDiv(this.value)"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // end of pep form 

                     fieldHTML += '<div class="show-when-pep_'+counter+'" id="directorpepform_'+counter+'" style="display:none;">'; // start of director pep form 
                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">PEP in which country</label></div><div class="col-lg-8"><div class="selectdiv"><select name="countrypep[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="countrypep_'+counter+'"><option value="">Please select</option></select><div class="countrypep'+counter+'-alert" id="countrypep'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="countrypep'+counter+'-alert-msg"></span></div></div></div></div>'; // Country of pep form

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">Role of PEP</label></div><div class="col-lg-8"><div class="selectdiv"><select class="custom-select select-w-100" name="peprole[]" id="rolePep_'+counter+'"><option value="" selected="selected">Please select</option><option value="Ambassadors">Ambassadors</option><option value="Senior Military Officers">Senior Military Officers</option><option value="Political Party Leadership">Political Party Leadership</option><option value="Members of Parliament/Senate">Members of Parliament/Senate</option><option value="Heads of government agencies">Heads of government agencies</option><option value="Key leaders of state-owned enterprises">Key leaders of state-owned enterprises</option><option value="Key Senior Government Functionaries Judiciary/Legislature">Key Senior Government Functionaries Judiciary/Legislature</option><option value="Private companies, trusts or foundations owned or co-owned by PEPs, whether directly or indirectly">Private companies, trusts or foundations owned or co-owned by PEPs</option></select><div class="rolePep'+counter+'-alert" id="rolePep'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="rolePep'+counter+'-alert-msg"></span></div></div></div></div>'; // Role of pep form

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">PEP since</label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><select class="custom-select select-w-100" id="pepFrom_'+counter+'" name="pepFrom[]_'+counter+'" data-placeholder="From"><option value="">Please select</option></select><div class="pepFrom'+counter+'-alert" id="pepFrom'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="pepFrom'+counter+'-alert-msg"></span></div></div><div class="col-sm"><select class="custom-select select-w-100" id="pepTo_'+counter+'" name="pepTo_'+counter+'" data-placeholder="To"><option value="">To</option></select><div class="pepTo'+counter+'-alert" id="pepTo'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="pepTo'+counter+'-alert-msg"></span></div></div></div></div></div>';
                     fieldHTML += '</div>'; // end of directorpepform 
                     fieldHTML += '</div>'; // end of background-grey2  
                     fieldHTML += '</div>'; // end of row mt-5 
                     fieldHTML += '</div>'; // end of divclass  
                     if(isFirst == true){
                        $('#firstrole').html(fieldHTML);   
                     }else{   
                        $('body').find('#additionalRole:last').before(fieldHTML); 
                     }

                 // PassportManually

                 var pmanually ='<div class="modal fade" data-keyboard="true"  id="directornonresidentpreviewpassportmanually_'+counter+'" tabindex="-1" role="dialog">';
		pmanually += '<div class="modal-dialog modal-dialog-centered" role="document">';
		pmanually += '<div class="modal-content row">';
		pmanually += '<div class="modal-header"><h5 class="modal-title" id="directornonresidentmanually">Passport Information Manually - Director #'+counter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		pmanually += '<div class="col-lg-12 background-grey-2 popheight">';
		
		pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="directornonpassportNumber[]" class="form-control" id="directornonpassportNumber_'+counter+'"  value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Nationality</label><div class="col-sm-12"><select name="directornonnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directornonnationality_'+counter+'"></select></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="directornoncountry[]" id="directornoncountry_'+counter+'"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="directornongender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="directornongender[]" class="radio-red"> Female</label></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input placeholder="DD-MM-YYYY" type="text" id="directornondateissue_'+counter+'" class="form-control" name="directornondateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="directornondateexpiry_'+counter+'" class="form-control" name="directornondateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                 pmanually +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="directornondatebirth_'+counter+'" class="form-control" name="directornondatebirth[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directornondateplace_'+counter+'" name="directornondateplace[]"></select></div></div></div></div>';

                 pmanually += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'director\','+counter+',\'directorpassportmanually\');">Save</a></div></div></div>';
		pmanually += '</div></div></div></div>'; 
                     
                     $('body').find('#additionalRole:last').before(pmanually); 
                     
                    

                 //nric manually
                                 
		 var nricfield = '<div class="modal fade" data-keyboard="true"  id="directornonresidentpreviewnricmanually_'+counter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfield += '<div class="modal-content row">';
                     nricfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC and Address Information - Director #'+counter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfield += '<div class="col-lg-12 background-grey-2 popheight">'; 
                      nricfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="directornonresidentnricnumber[]" class="form-control" id="directornonresidentnricnumber_'+counter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input name="directornonresidentgender[]" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input name="directornonresidentgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input id="directornonresidentdob_'+counter+'" class="form-control" name="directornonresidentdob[]"  type="text"   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Birth</label><div class="col-sm-12"><select name="directornricnationality" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directornricnationality_'+counter+'"></select></div></div></div></div>';

                     nricfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directornricpostcode_'+counter+'" name="directornricpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea id="directornricstreetname_'+counter+'" name="directornricstreetname[]" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directornricfloor_'+counter+'" name="directornricfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directornricunit_'+counter+'" name="directornricunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'director\','+counter+',\'directornricmanually\');">Save</a></div></div></div>';

                     nricfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfield); 

                     //fincard manuallyy
               var fincard ='<div class="modal fade"  data-keyboard="true" id="directorfincardmanually_'+counter+'" tabindex="-1" role="dialog">';
		fincard += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincard += '<div class="modal-content row">';
		fincard += '<div class="modal-header"><h5 class="modal-title" id="directorfincardmanually">Passport and WorkPass Information - Director #'+counter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		fincard += '<div class="col-lg-12 background-grey-2 popheight">';
		
		fincard += '<div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="directorfincardpassportNumber[]" class="form-control" id="directorfincardpassportNumber_'+counter+'"  value="" /></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directorfincardnationality_'+counter+'" name="directorfincardnationality[]"></select></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="directorfincardcountry[]" id="directorfincardcountry_'+counter+'"></select></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="directorfincardgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="directorfincardgender[]" class="radio-red"> Female</label></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="directorfincarddateissue_'+counter+'" class="form-control" name="directorfincarddateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="directorfincarddateexpiry_'+counter+'" class="form-control" name="directorfincarddateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div>';

                 fincard +='<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="directorfincarddatebirth_'+counter+'" class="form-control" name="directorfincarddatebirth[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directorfincarddateplace_'+counter+'" name="directorfincarddateplace[]"></select></div></div></div></div>';

                 fincard +='<div class="row my-1"><div class="col-sm"><h4 class="font-weight-bold">Work Pass</h4></div></div>';
        
                 fincard +='<div class="row">';
                
                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="directorfincardemployer[]" class="form-control" id="directorfincardemployer_'+counter+'" placeholder="" value="" type="text"></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="directorfincardoccupation[]" class="form-control" id="directorfincardoccupation_'+counter+'" value="" type="text"></div></div></div>';
 
                  //fincard += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div>';

                  fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="directorworkpassdateissue_'+counter+'" class="form-control" name="directorworkpassdateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="directorworkpassdateexpiry_'+counter+'" class="form-control" name="directorworkpassdateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-12"><input type="text" name="directorwfincardNumber[]" class="form-control" id="directorwfincardNumber_'+counter+'"  value="" /></div></div></div></div>';  

                  fincard += '<div class="row my-2"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'director\','+counter+',\'directorfinmanually\');">Save</a></div></div></div>';

                  fincard +='</div>'; 
  
		  fincard += '</div></div></div></div>';         
                $('body').find('#additionalRole:last').before(fincard); 

                }else{
		    alert('Maximum '+maxGroup+' Director are allowed.');
		}

                //Passport Manually - Non resident

                $('#directornonnationality_'+counter).html(countriesOption);
                $('#directornonnationality_'+counter).chosen(); 

                $('#directornoncountry_'+counter).html(countriesOption);
                $('#directornoncountry_'+counter).chosen(); 

                $('#directornondateplace_'+counter).html(countriesOption);
                $('#directornondateplace_'+counter).chosen(); 

                //nric Manually

                $('#directornricnationality_'+counter).html(countriesOption);
                $('#directornricnationality_'+counter).chosen(); 

                // fincard Manually
                $('#directorfincardnationality_'+counter).html(countriesOption);
                $('#directorfincardnationality_'+counter).chosen(); 
               
                $('#directorfincardcountry_'+counter).html(countriesOption);
                $('#directorfincardcountry_'+counter).chosen(); 
              
                $('#directorfincarddateplace_'+counter).html(countriesOption);
                $('#directorfincarddateplace_'+counter).chosen(); 

                // Director form fields building chosen jquery
   
                $('#countrypep_'+counter).html(countriesOption);
                $('#countrypep_'+counter).chosen(); 
                $('#pepFrom_'+counter).html(yearfromOption);
                $('#pepFrom_'+counter).chosen();
                $('#pepTo_'+counter).html(yeartoOption);
                $('#pepTo_'+counter).chosen();

                 
                $('#directornondateissue_'+counter).datepicker({                  
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#directornondateexpiry_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#directornondatebirth_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#directornonresidentdob_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#directorfincarddateissue_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#directorfincarddateexpiry_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#directorfincarddatebirth_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#directorworkpassdateissue_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#directorworkpassdateexpiry_'+counter).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                
                $('#addroleSelector').prop('selectedIndex',0);
                $("#additionalRole").show(); 
	}

        else if(getvalue.value == 'Shareholder') {
                 /*counter1++;*/
                 counter1 = (isFirst==true) ? 1 : counter1+1;
		if(counter1 <= maxGroup){

                     var fieldHTML = '<div id="shareholder_'+counter1+'" class="divclass">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Individual Shareholder #'+counter1+'</h5></div></div>'; // end of background-grey1

                     fieldHTML += '<div class="col-lg-12 background-grey-2">';
                     fieldHTML += '<div class="form-group row mt-4" id="names_'+counter1+'"><div class="col-lg-4"><label class="col-form-label required">Full Name</label></div><div class="col-lg-8"><input name="shareholdersurname[]" class="form-control" id="shareholdersurname_'+counter1+'" placeholder="" type="text"><div class="shareholderfullName'+counter1+'-alert" id="shareholderfullName'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderfullName'+counter1+'-alert-msg"></span></div></div></div>'; // end of fullname 

                     fieldHTML += '<div class="form-group row"><label class="col-lg-4 col-form-label required" data-toggle="tooltip" data-placement="right"  title="A Beneficial Owner means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or assets and undertakings of the customer or otherwise exercises control over the management of the customer.">Are you the Beneficial Owner <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input id="shareholderbowner_'+counter1+'" name="shareholderbowner_'+counter1+'" value="Yes" checked="checked" type="radio"><span class="custom-radio-label">Yes</span></label></div><div class="col-sm"><label class="custom-radio-field"><input id="shareholderbowner_'+counter1+'" name="shareholderbowner_'+counter1+'" value="No" type="radio"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // bowner    

                     fieldHTML += '<div class="form-group row" id="shareholderstatus_'+counter1+'"><div class="col-lg-4"><label class="col-form-label required">Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="shareholderresstatus_'+counter1+'" name="shareholderresidencyStatus[]" onchange="shareholderresidencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="shareholdernonresident_'+counter1+'" rel="nonresident">Non-resident</option><option value="shareholdercitizenpr_'+counter1+'" rel="citizenpr">Singapore Citizen / PR</option><option value="shareholderpassholder_'+counter1+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="shareholderresidencyStatus'+counter1+'-alert" id="shareholderresidencyStatus'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderresidencyStatus'+counter1+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                     fieldHTML += '<div id="shareholderpassholder_'+counter1+'" class="choose-file form-group" style="display: none;">';

                     fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholderuploadPassport[]" class="custom-file-input" id="shareholderuploadPassport_'+counter1+'" type="file" onchange="checkPdfParse(this.files,'+counter1+',finuploadpassportfile,\'shareholder\');"><label class="custom-file-label dashed-lines" id="shareholderfinpassportlabel_'+counter1+'">Passport</label></div></div><div class="shareholderuploadPassport'+counter1+'-alert" id="shareholderuploadPassport'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderuploadPassport'+counter1+'-alert-msg"></span></div><div class="row"><div id="bshareholderfincardpreviewpassport_'+counter1+'" class="col-sm-12" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#shareholderfincardpreviewpassport_'+counter1+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your Passport Details</button></div></div><div class="row" id="shareholderuploadPassportloadingImage_'+counter1+'" style="display:none;"><div class="col-sm-6 text-center"><img src="images/image-loading.gif"></div></div></div>OR<div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#shareholderfincardmanually_'+counter1+'" id="shareholderbuttonfincardmanually_'+counter1+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div>';

	            fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholderfincardfront[]" class="custom-file-input" id="shareholderfincardfront_'+counter1+'" type="file" onchange="checkPdfParse(this.files,'+counter1+',fincardfront,\'shareholder\');"><label class="custom-file-label dashed-lines" id="shareholderfincardfrontlabel_'+counter1+'">FIN Card Front</label></div></div><div class="shareholderfincardfront'+counter1+'-alert" id="shareholderfincardfront'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderfincardfront'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholderfincardback[]" class="custom-file-input" id="shareholderfincardback_'+counter1+'" type="file" onchange="checkPdfParse(this.files,'+counter1+',fincardback,\'shareholder\');"><label class="custom-file-label dashed-lines" id="shareholderfincardbacklabel_'+counter1+'">FIN Card Back</label></div></div><div class="shareholderfincardback'+counter1+'-alert" id="shareholderfincardback'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderfincardback'+counter1+'-alert-msg"></span></div></div></div><div class="row"><div id="bshareholderfincardfrontpreview_'+counter1+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#shareholderfincardfrontpreview_'+counter1+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your front fin details</button></div><div id="bshareholderfincardbackpreview_'+counter1+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#shareholderfincardbackpreview_'+counter1+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your fin back details</button></div></div><div class="row" id="shareholderuploadfincardloadingImage_'+counter1+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


	           fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" name="shareholderaddress[]" class="custom-file-input" id="shareholderaddress_'+counter1+'" type="file" onchange="shareholderaddress(this.files,'+counter1+');"><label class="custom-file-label dashed-lines" id="shareholderaddresslabel_'+counter1+'">Address</label></div></div><div class="shareholderaddress'+counter1+'-alert" id="shareholderaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderaddress'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="shareholderresaddress[]" id="shareholderresaddress_'+counter1+'" rows="2" placeholder="Please key in residential address"></textarea><div class="shareholderresaddress'+counter1+'-alert" id="shareholderresaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderresaddress'+counter1+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="shareholdercitizenpr_'+counter1+'" class="choose-file" style="display: none;">';

                     fieldHTML +='<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernricfront[]" class="custom-file-input" id="shareholdernricfront_'+counter1+'" type="file" onchange="checkPdfParse(this.files,'+counter1+',nricfront,\'shareholder\');"><label class="custom-file-label dashed-lines" id="shareholdernricfrontlabel_'+counter1+'">NRIC Front</label></div></div><div class="shareholdernricfront'+counter1+'-alert" id="shareholdernricfront'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernricfront'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernricback[]" class="custom-file-input" id="shareholdernricback_'+counter1+'" type="file" onchange="checkPdfParse(this.files,'+counter1+',nricback,\'shareholder\');"><label class="custom-file-label dashed-lines" id="shareholdernricbacklabel_'+counter1+'">NRIC Back</label></div></div><div class="shareholdernricback'+counter1+'-alert" id="shareholdernricback'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernricback'+counter1+'-alert-msg"></span></div></div></div><div class="row"><div id="bshareholdernricfrontpreview_'+counter1+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#shareholdernricfrontpreview_'+counter1+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Front details</button></div><div id="bshareholdernricbackpreview_'+counter1+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#shareholdernricbackpreview_'+counter1+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Back details</button></div></div><div class="row" id="shareholderuploadnricloadingImage_'+counter1+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div><div class="row"><div class="col-sm-6"><button type="button" data-target="#shareholdernonresidentpreviewnricmanually_'+counter1+'" class="btn btn-style-1" data-toggle="modal" id="shareholderbuttonnricmanually_'+counter1+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="shareholdernonresident_'+counter1+'" class="choose-file" style="display:none;">';

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" >Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernonresidentuploadPassport[]" class="custom-file-input" id="shareholdernonresidentuploadpassport_'+counter1+'" type="file" onchange="checkPdfParse(this.files,'+counter1+',nonuploadpassportfile,\'shareholder\');"><label class="custom-file-label dashed-lines" id="shareholdernonresidentlabel_'+counter1+'">Passport</label></div></div><div class="shareholdernonresidentuploadpassport'+counter1+'-alert" id="shareholdernonresidentuploadpassport'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernonresidentuploadpassport'+counter1+'-alert-msg"></span></div></div>OR<div class="col-sm"><button id="shareholderbuttonpassportmanually_'+counter1+'" type="button" class="btn btn-style-1" data-toggle="modal" data-target="#shareholdernonresidentpreviewpassportmanually_'+counter1+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div  id="bshareholdernonresidentpreviewpassport_'+counter1+'" class="col-sm-6"  style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#shareholdernonresidentpreviewpassport_'+counter1+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div><div class="row" id="shareholderuploadpassportfileloadingImage_'+counter1+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernonresidentaddress[]" class="custom-file-input" id="shareholdernonresidentaddress_'+counter1+'" type="file" accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" onchange="shareholdernonresidentaddress(this.files,'+counter1+');"><label class="custom-file-label dashed-lines" id="shareholdernonresidentaddresslabel_'+counter1+'">Address</label></div></div><div class="shareholdernonresidentaddress'+counter1+'-alert" id="shareholdernonresidentaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernonresidentaddress'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="shareholdernonresidentresaddress[]" id="shareholdernonresidentresaddress_'+counter1+'" rows="2" placeholder="Please key in residential address"></textarea><div class="shareholdernonresidentresaddress'+counter1+'-alert" id="shareholdernonresidentresaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernonresidentresaddress'+counter1+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident


                     fieldHTML += '<div class="form-group row" id="shareholderphnumbs_'+counter1+'"><label class="col-lg-4 col-form-label required">Contact Numbers</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="shareholderhandphone_'+counter1+'" name="shareholderhandPhone[]" class="form-control mb-2" placeholder="Hand phone *"  type="text"><div class="shareholdercontactNumber'+counter1+'-alert" id="shareholdercontactNumber'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdercontactNumber'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><input id="shareholderoffnumber_'+counter1+'" name="shareholderofficeNumber[]" class="form-control mb-2" placeholder="Office number" type="text"><div class="shareholderofficeNumber'+counter1+'-alert" id="shareholderofficeNumber'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderofficeNumber'+counter1+'-alert-msg"></span></div></div></div></div> </div>'; // end of contact numbers

                     fieldHTML += '<div class="form-group row" id="shareholderemailportion_'+counter1+'"><label class="col-lg-4 col-form-label required">Email Address(es) </label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="shareholderbusinessemail_'+counter1+'" name="shareholderbusinessEmail[]" class="form-control mb-2" placeholder="Business email *"  type="text"><div class="shareholderbizEmail'+counter1+'-alert" id="shareholderbizEmail'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderbizEmail'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><input id="shareholderpersonalemail_'+counter1+'" name="shareholderpersonalEmail[]" class="form-control mb-2" placeholder="Personal email" type="text"><div class="shareholderpersonalEmail'+counter1+'-alert" id="shareholderpersonalEmail'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderpersonalEmail'+counter1+'-alert-msg"></span></div></div></div></div></div>'; // end of email addresses  

                     fieldHTML += '<div class="form-group row" id="shareholderpoliticallyexposed_'+counter1+'"><label class="col-lg-4 col-form-label required" data-toggle="tooltip" data-placement="right"  title="Politically Exposed Persons (PEPs) are individuals who are or have been entrusted with prominent public functions in a foreign country, for example Heads of State or of government, senior politicians, senior government, judicial or High Ranking military officials, senior executives of state owned corporations, important political party officials. Business relationships with family members or close associates of PEP.">Are you a Politically Exposed Person (PEP) <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input name="shareholderpep_'+counter1+'" id="shareholderpep_'+counter1+'" value="shareholderpepyes_'+counter1+'" type="radio" onclick="ShowHidesDiv(this.value)"><span class="custom-radio-label">Yes</span></label><div class="shareholderpoliticallyExposed'+counter1+'-alert" id="shareholderpoliticallyExposed'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="politicallyExposed'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><label class="custom-radio-field"><input id="shareholderpep_'+counter1+'" name="shareholderpep_'+counter1+'" value="notshareholderpep_'+counter1+'" checked="checked" type="radio" onclick="ShowHidesDiv(this.value)"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // end of pep form 

                     fieldHTML += '<div class="show-when-shareholderpep_'+counter1+'" id="shareholderpepform_'+counter1+'" style="display:none;">'; // start of shareholder pep form 
                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">PEP in which country</label></div><div class="col-lg-8"><div class="selectdiv"><select name="shareholdercountrypep[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="shareholdercountrypep_'+counter1+'"><option value="">Please select</option></select><div class="shareholdercountrypep'+counter1+'-alert" id="shareholdercountrypep'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdercountrypep'+counter1+'-alert-msg"></span></div></div></div></div>'; // Country of pep form

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">Role of PEP</label></div><div class="col-lg-8"><div class="selectdiv"><select class="custom-select select-w-100" name="shareholderpeprole[]" id="shareholderrolePep_'+counter1+'"><option value="" selected="selected">Please select</option><option value="Ambassadors">Ambassadors</option><option value="Senior Military Officers">Senior Military Officers</option><option value="Political Party Leadership">Political Party Leadership</option><option value="Members of Parliament/Senate">Members of Parliament/Senate</option><option value="Heads of government agencies">Heads of government agencies</option><option value="Key leaders of state-owned enterprises">Key leaders of state-owned enterprises</option><option value="Key Senior Government Functionaries Judiciary/Legislature">Key Senior Government Functionaries Judiciary/Legislature</option><option value="Private companies, trusts or foundations owned or co-owned by PEPs, whether directly or indirectly">Private companies, trusts or foundations owned or co-owned by PEPs</option></select><div class="shareholderrolePep'+counter1+'-alert" id="shareholderrolePep'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderrolePep'+counter1+'-alert-msg"></span></div></div></div></div>'; // Role of pep form

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">PEP since</label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><select class="custom-select select-w-100" id="shareholderpepFrom_'+counter1+'" name="shareholderpepFrom[]_'+counter1+'" data-placeholder="From"><option value="" selected>From</option></select><div class="shareholderpepFrom'+counter1+'-alert" id="shareholderpepFrom'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderpepFrom'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><select class="custom-select select-w-100" id="shareholderpepTo_'+counter1+'" name="shareholderpepTo_'+counter1+'" data-placeholder="To"><option value="">To</option></select><div class="shareholderpepTo'+counter1+'-alert" id="shareholderpepTo'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderpepTo'+counter1+'-alert-msg"></span></div></div></div></div></div>';
                     fieldHTML += '</div>'; // end of shareholderpepform 
                     fieldHTML += '</div>'; // end of background-grey2  
                     fieldHTML += '</div>'; // end of row mt-5 
                     fieldHTML += '</div>'; // end of divclass  

                     if(isFirst == true){
                        $('#firstrole').html(fieldHTML);   
                     }else{
                        $('body').find('#additionalRole:last').before(fieldHTML);             
                     }
		     

                     // PassportManually

                 var pmanually ='<div class="modal fade"  data-keyboard="true" id="shareholdernonresidentpreviewpassportmanually_'+counter1+'" tabindex="-1" role="dialog">';
		pmanually += '<div class="modal-dialog modal-dialog-centered" role="document">';
		pmanually += '<div class="modal-content row">';
		pmanually += '<div class="modal-header"><h5 class="modal-title" id="shareholdernonresidentmanually">Passport Information Manually - Shareholder #'+counter1+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		pmanually += '<div class="col-lg-12 background-grey-2 popheight">';
		
		pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="shareholdernonpassportNumber[]" class="form-control" id="shareholdernonpassportNumber_'+counter1+'"  value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Nationality</label><div class="col-sm-12"><select name="shareholdernonnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholdernonnationality_'+counter1+'"></select></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="shareholdernoncountry[]" id="shareholdernoncountry_'+counter1+'"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="shareholdernongender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="shareholdernongender[]" class="radio-red"> Female</label></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholdernondateissue_'+counter1+'" class="form-control" name="shareholdernondateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholdernondateexpiry_'+counter1+'" class="form-control" name="shareholdernondateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                 pmanually +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholdernondatebirth_'+counter1+'" class="form-control" name="shareholdernondatebirth[]" placeholder="DD-MM-YYYY" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholdernondateplace_'+counter1+'" name="shareholdernondateplace[]"></select></div></div></div></div>';

                 pmanually += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'shareholder\','+counter1+',\'shareholderpassportmanually\');">Save</a></div></div></div>';
		pmanually += '</div></div></div></div>'; 
         
                $('body').find('#additionalRole:last').before(pmanually);    

                 //nric manually
                                 
		 var nricfield = '<div class="modal fade"  data-keyboard="true" id="shareholdernonresidentpreviewnricmanually_'+counter1+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfield += '<div class="modal-content row">';
                     nricfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC and Address Information - Shareholder #'+counter1+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfield += '<div class="col-lg-12 background-grey-2 popheight">'; 
                      nricfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="shareholdernonresidentnricnumber[]" class="form-control" id="shareholdernonresidentnricnumber_'+counter1+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input name="shareholdernonresidentgender[]" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input name="shareholdernonresidentgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input id="shareholdernonresidentdob_'+counter1+'" class="form-control" name="shareholdernonresidentdob[]"  type="text" placeholder="DD-MM-YYYY"   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="shareholdernricnationality" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholdernricnationality_'+counter1+'"></select></div></div></div></div>';

                     nricfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholdernricpostcode_'+counter1+'" name="shareholdernricpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="shareholdernricstreetname[]" id="shareholdernricstreetname_'+counter1+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholdernricfloor_'+counter1+'" name="shareholdernricfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="shareholdernricunit_'+counter1+'" name="shareholdernricunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'shareholder\','+counter1+',\'shareholdernricmanually\');">Save</a></div></div></div>';

                     nricfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfield); 

                     //fincard manuallyy
               var fincard ='<div class="modal fade"  data-keyboard="true" id="shareholderfincardmanually_'+counter1+'" tabindex="-1" role="dialog">';
		fincard += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincard += '<div class="modal-content row">';
		fincard += '<div class="modal-header"><h5 class="modal-title" id="shareholderfincardmanually">Passport and WorkPass Information Shareholder #'+counter1+' </h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		fincard += '<div class="col-lg-12 background-grey-2 popheight">';
		
		fincard += '<div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="shareholderfincardpassportNumber[]" class="form-control" id="shareholderfincardpassportNumber_'+counter1+'"  value="" /></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholderfincardnationality_'+counter1+'" name="shareholderfincardnationality[]"></select></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="shareholderfincardcountry[]" id="shareholderfincardcountry_'+counter1+'"></select></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="shareholderfincardgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="shareholderfincardgender[]" class="radio-red"> Female</label></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholderfincarddateissue_'+counter1+'" class="form-control" name="shareholderfincarddateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholderfincarddateexpiry_'+counter1+'" class="form-control" name="shareholderfincarddateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div>';

                 fincard +='<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholderfincarddatebirth_'+counter1+'" class="form-control" name="shareholderfincarddatebirth[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="shareholderfincarddateplace_'+counter1+'" name="shareholderfincarddateplace[]"></select></div></div></div></div>';

                 fincard +='<div class="row my-1"><div class="col-sm"><h4 class="font-weight-bold">Work Pass</h4></div></div>';
                
                 fincard += '<div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="shareholderfincardemployer[]" class="form-control" id="shareholderfincardemployer_'+counter1+'" placeholder="" value="" type="text"></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="shareholderfincardoccupation[]" class="form-control" id="shareholderfincardoccupation_'+counter1+'" value="" type="text"></div></div></div>'; 

                  //fincard += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholderworkpassdateissue_'+counter1+'" class="form-control" name="shareholderworkpassdateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label " >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="shareholderworkpassdateexpiry_'+counter1+'" class="form-control" name="shareholderworkpassdateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-12"><input type="text" name="shareholderwfincardNumber[]" class="form-control" id="shareholderwfincardNumber_'+counter1+'"  value="" /></div></div></div></div>';        

                 fincard += '<div class="row my-1"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'shareholder\','+counter1+',\'shareholderfincardmanually\');">Save</a></div></div></div>';
		fincard += '</div></div></div></div>';         
                $('body').find('#additionalRole:last').before(fincard);  
                    
                      
		}else{
		    alert('Maximum '+maxGroup+' Shareholder are allowed.');
		}


                $('#shareholdernonnationality_'+counter1).html(countriesOption);
                $('#shareholdernonnationality_'+counter1).chosen(); 

                $('#shareholdernoncountry_'+counter1).html(countriesOption);
                $('#shareholdernoncountry_'+counter1).chosen(); 

                $('#shareholdernondateplace_'+counter1).html(countriesOption);
                $('#shareholdernondateplace_'+counter1).chosen(); 

                //nric Manually

                $('#shareholdernricnationality_'+counter1).html(countriesOption);
                $('#shareholdernricnationality_'+counter1).chosen(); 

                // fincard Manually
                $('#shareholderfincardnationality_'+counter1).html(countriesOption);
                $('#shareholderfincardnationality_'+counter1).chosen(); 
               
                $('#shareholderfincardcountry_'+counter1).html(countriesOption);
                $('#shareholderfincardcountry_'+counter1).chosen(); 
              
                $('#shareholderfincarddateplace_'+counter1).html(countriesOption);
                $('#shareholderfincarddateplace_'+counter1).chosen();  

                $('#shareholdernondateissue_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#shareholdernondateexpiry_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#shareholdernondatebirth_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#shareholdernonresidentdob_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#shareholderfincarddateissue_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#shareholderfincarddateexpiry_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#shareholderfincarddatebirth_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#shareholderworkpassdateissue_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#shareholderworkpassdateexpiry_'+counter1).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });
                
                $('#shareholdercountrypep_'+counter1).html(countriesOption);
                $('#shareholdercountrypep_'+counter1).chosen(); 
                $('#shareholderpepFrom_'+counter1).html(yearfromOption);
                $('#shareholderpepFrom_'+counter1).chosen();
                $('#shareholderpepTo_'+counter1).html(yeartoOption);
                $('#shareholderpepTo_'+counter1).chosen();
                $('#addroleSelector').prop('selectedIndex',0);   
                $("#additionalRole").show(); 
	}

        else if(getvalue.value == 'Director and Shareholder') {
         /*counter2++;*/
         counter2 = (isFirst==true) ? 1 : counter2+1;
		if(counter2 <= maxGroup){
		     var fieldHTML = '<div id="dishareholder_'+counter2+'" class="divclass">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Director and Individual Shareholder #'+counter2+'</h5></div></div>'; // end of background-grey1

                     fieldHTML += '<div class="col-lg-12 background-grey-2">';
                     fieldHTML += '<div class="form-group row mt-4" id="names_'+counter2+'"><div class="col-lg-4"><label class="col-form-label required" >Full Name</label></div><div class="col-lg-8"><input name="dishareholdersurname[]" class="form-control" id="dishareholdersurname_'+counter2+'" placeholder="" type="text"><div class="dishareholderfullName'+counter2+'-alert" id="dishareholderfullName'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderfullName'+counter2+'-alert-msg"></span></div></div></div>'; // end of fullname 

                     fieldHTML += '<div class="form-group row"><label class="col-lg-4 col-form-label required" data-toggle="tooltip" data-placement="right"  title="A Beneficial Owner means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or assets and undertakings of the customer or otherwise exercises control over the management of the customer.">Are you the Beneficial Owner <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input id="dishareholderbowner_'+counter2+'" name="dishareholderbowner_'+counter2+'" value="Yes" checked="checked" type="radio"><span class="custom-radio-label">Yes</span></label></div><div class="col-sm"><label class="custom-radio-field"><input id="dishareholderbowner_'+counter2+'" name="dishareholderbowner_'+counter2+'" value="No" type="radio"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // bowner    

                     fieldHTML += '<div class="form-group row" id="dishareholderstatus_'+counter2+'"><div class="col-lg-4"><label class="col-form-label required" >Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="dishareholderresstatus_'+counter2+'" name="dishareholderresidencyStatus[]" onchange="dishareholderresidencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="dinonresident_'+counter2+'" rel="nonresident">Non-resident</option><option value="dicitizenpr_'+counter2+'" rel="citizenpr">Singapore Citizen / PR</option><option value="dipassholder_'+counter2+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="dishareholderresidencyStatus'+counter2+'-alert" id="dishareholderresidencyStatus'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderresidencyStatus'+counter2+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                     fieldHTML += '<div id="dipassholder_'+counter2+'" class="choose-file form-group" style="display: none;">';

                     fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="diuploadPassport[]" class="custom-file-input" id="diuploadPassport_'+counter2+'" type="file" onchange="checkPdfParse(this.files,'+counter2+',finuploadpassportfile,\'di\');"><label class="custom-file-label dashed-lines" id="difinpassportlabel_'+counter2+'">Passport</label></div></div><div class="diuploadPassport'+counter2+'-alert" id="diuploadPassport'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="diuploadPassport'+counter2+'-alert-msg"></span></div><div class="row"><div id="bdifincardpreviewpassport_'+counter2+'" class="col-sm-12" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#difincardpreviewpassport_'+counter2+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your Passport Details</button></div></div><div class="row" id="diuploadPassportloadingImage_'+counter2+'" style="display:none;"><div class="col-sm-6 text-center"><img src="images/image-loading.gif"></div></div></div>OR<div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#difincardmanually_'+counter2+'" id="dibuttonfincardmanually_'+counter2+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div>';

	            fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="difincardfront[]" class="custom-file-input" id="difincardfront_'+counter2+'" type="file" onchange="checkPdfParse(this.files,'+counter2+',fincardfront,\'di\');"><label class="custom-file-label dashed-lines" id="difincardfrontlabel_'+counter2+'">FIN Card Front</label></div></div><div class="difincardfront'+counter2+'-alert" id="difincardfront'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="difincardfront'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="difincardback[]" class="custom-file-input" id="difincardback_'+counter2+'" type="file" onchange="checkPdfParse(this.files,'+counter2+',fincardback,\'di\');"><label class="custom-file-label dashed-lines" id="difincardbacklabel_'+counter2+'">FIN Card Back</label></div></div><div class="difincardback'+counter2+'-alert" id="difincardback'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="difincardback'+counter2+'-alert-msg"></span></div></div></div><div class="row"><div id="bdifincardfrontpreview_'+counter2+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#difincardfrontpreview_'+counter2+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your front fin details</button></div><div id="bdifincardbackpreview_'+counter2+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#difincardbackpreview_'+counter2+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your fin back details</button></div></div><div class="row" id="diuploadfincardloadingImage_'+counter2+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


	           fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" name="diaddress[]" class="custom-file-input" id="diaddress_'+counter2+'" type="file" onchange="diaddress(this.files,'+counter2+');"><label class="custom-file-label dashed-lines" id="diaddresslabel_'+counter2+'">Address</label></div></div><div class="diaddress'+counter2+'-alert" id="diaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="diaddress'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="diresaddress[]" id="diresaddress_'+counter2+'" rows="2" placeholder="Please key in residential address"></textarea><div class="diresaddress'+counter2+'-alert" id="diresaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="diresaddress'+counter2+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="dicitizenpr_'+counter2+'" class="choose-file" style="display: none;">';

                     fieldHTML +='<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dinricfront[]" class="custom-file-input" id="dinricfront_'+counter2+'" type="file" onchange="checkPdfParse(this.files,'+counter2+',nricfront,\'di\');"><label class="custom-file-label dashed-lines" id="dinricfrontlabel_'+counter2+'">NRIC Front</label></div></div><div class="dinricfront'+counter2+'-alert" id="dinricfront'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dinricfront'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholdernricback[]" class="custom-file-input" id="dinricback_'+counter2+'" type="file" onchange="checkPdfParse(this.files,'+counter2+',nricback,\'di\');"><label class="custom-file-label dashed-lines" id="dinricbacklabel_'+counter2+'">NRIC Back</label></div></div><div class="dinricback'+counter2+'-alert" id="dinricback'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dinricback'+counter2+'-alert-msg"></span></div></div></div><div class="row"><div id="bdinricfrontpreview_'+counter2+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#dinricfrontpreview_'+counter2+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Front details</button></div><div id="bdinricbackpreview_'+counter2+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#dinricbackpreview_'+counter2+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Back details</button></div></div><div class="row" id="diuploadnricloadingImage_'+counter2+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div><div class="row"><div class="col-sm-6"><button data-target="#dinonresidentpreviewnricmanually_'+counter2+'" type="button" class="btn btn-style-1" data-toggle="modal" id="dibuttonnricmanually_'+counter2+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="dinonresident_'+counter2+'" class="choose-file" style="display:none;">';

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" >Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dinonresidentuploadPassport[]" class="custom-file-input" id="dinonresidentuploadpassport_'+counter2+'" type="file" onchange="checkPdfParse(this.files,'+counter2+',nonuploadpassportfile,\'di\');"><label class="custom-file-label dashed-lines" id="dinonresidentlabel_'+counter2+'">Passport</label></div></div><div class="dinonresidentuploadpassport'+counter2+'-alert" id="dinonresidentuploadpassport'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dinonresidentuploadpassport'+counter2+'-alert-msg"></span></div></div>OR<div class="col-sm"><button data-target="#dinonresidentpreviewpassportmanually_'+counter2+'" type="button" class="btn btn-style-1" data-toggle="modal" id="dibuttonpassportmanually_'+counter2+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div  id="bdinonresidentpreviewpassport_'+counter2+'" class="col-sm-6"  style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#dinonresidentpreviewpassport_'+counter2+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div><div class="row" id="diuploadpassportfileloadingImage_'+counter2+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" name="dinonresidentaddress[]" class="custom-file-input" id="dinonresidentaddress_'+counter2+'" type="file" onchange="dinonresidentaddress(this.files,'+counter2+');"><label class="custom-file-label dashed-lines" id="dinonresidentaddresslabel_'+counter2+'">Address</label></div></div><div class="dinonresidentaddress'+counter2+'-alert" id="dinonresidentaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dinonresidentaddress'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="dinonresidentresaddress[]" id="dinonresidentresaddress_'+counter2+'" rows="2" placeholder="Please key in residential address"></textarea><div class="dinonresidentresaddress'+counter2+'-alert" id="dinonresidentresaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dinonresidentresaddress'+counter2+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident

                     fieldHTML += '<div class="form-group row" id="dishareholderphnumbs_'+counter2+'"><label class="col-lg-4 col-form-label required" >Contact Numbers</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="dishareholderhandphone_'+counter2+'" name="dishareholderhandPhone[]" class="form-control mb-2" placeholder="Hand phone *"  type="text"><div class="dishareholdercontactNumber'+counter2+'-alert" id="dishareholdercontactNumber'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholdercontactNumber'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><input id="dishareholderoffnumber_'+counter2+'" name="dishareholderofficeNumber[]" class="form-control mb-2" placeholder="Office number" type="text"><div class="dishareholderofficeNumber'+counter2+'-alert" id="dishareholderofficeNumber'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderofficeNumber'+counter2+'-alert-msg"></span></div></div></div></div> </div>'; // end of contact numbers

                     fieldHTML += '<div class="form-group row" id="dishareholderemailportion_'+counter2+'"><label class="col-lg-4 col-form-label required">Email Address(es) </label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="dishareholderbusinessemail_'+counter2+'" name="dishareholderbusinessEmail[]" class="form-control mb-2" placeholder="Business email *"  type="text"><div class="dishareholderbizEmail'+counter2+'-alert" id="dishareholderbizEmail'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderbizEmail'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><input id="dishareholderpersonalemail_'+counter2+'" name="dishareholderpersonalEmail[]" class="form-control mb-2" placeholder="Personal email" type="text"><div class="dishareholderpersonalEmail'+counter2+'-alert" id="dishareholderpersonalEmail'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderpersonalEmail'+counter2+'-alert-msg"></span></div></div></div></div></div>'; // end of email addresses  

                     fieldHTML += '<div class="form-group row" id="dishareholderpoliticallyexposed_'+counter2+'"><label class="col-lg-4 col-form-label required" data-toggle="tooltip" data-placement="right"  title="Politically Exposed Persons (PEPs) are individuals who are or have been entrusted with prominent public functions in a foreign country, for example Heads of State or of government, senior politicians, senior government, judicial or High Ranking military officials, senior executives of state owned corporations, important political party officials. Business relationships with family members or close associates of PEP.">Are you a Politically Exposed Person (PEP) <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input name="dishareholderpep_'+counter2+'" id="dishareholderpep_'+counter2+'" value="dishareholderpepyes_'+counter2+'" type="radio" onclick="ShowHidesdDiv(this.value)"><span class="custom-radio-label">Yes</span></label><div class="dishareholderpoliticallyExposed'+counter2+'-alert" id="dishareholderpoliticallyExposed'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="politicallyExposed'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><label class="custom-radio-field"><input id="dishareholderpep_'+counter2+'" name="dishareholderpep_'+counter2+'" value="notdishareholderpep_'+counter2+'" checked="checked" type="radio" onclick="ShowHidesdDiv(this.value)"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // end of pep form 

                     fieldHTML += '<div class="show-when-dishareholderpep_'+counter2+'" id="dishareholderpepform_'+counter2+'" style="display:none;">'; // start of dishareholder pep form 
                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" >PEP in which country</label></div><div class="col-lg-8"><div class="selectdiv"><select name="dishareholdercountrypep[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="dishareholdercountrypep_'+counter2+'"><option value="">Please select</option></select><div class="dishareholdercountrypep'+counter2+'-alert" id="dishareholdercountrypep'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholdercountrypep'+counter2+'-alert-msg"></span></div></div></div></div>'; // Country of pep form

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" >Role of PEP</label></div><div class="col-lg-8"><div class="selectdiv"><select class="custom-select select-w-100" name="dishareholderpeprole[]" id="dishareholderrolePep_'+counter2+'"><option value="" selected="selected">Please select</option><option value="Ambassadors">Ambassadors</option><option value="Senior Military Officers">Senior Military Officers</option><option value="Political Party Leadership">Political Party Leadership</option><option value="Members of Parliament/Senate">Members of Parliament/Senate</option><option value="Heads of government agencies">Heads of government agencies</option><option value="Key leaders of state-owned enterprises">Key leaders of state-owned enterprises</option><option value="Key Senior Government Functionaries Judiciary/Legislature">Key Senior Government Functionaries Judiciary/Legislature</option><option value="Private companies, trusts or foundations owned or co-owned by PEPs, whether directly or indirectly">Private companies, trusts or foundations owned or co-owned by PEPs</option></select><div class="dishareholderrolePep'+counter2+'-alert" id="dishareholderrolePep'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderrolePep'+counter2+'-alert-msg"></span></div></div></div></div>'; // Role of pep form

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">PEP since</label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><select class="custom-select select-w-100" id="dishareholderpepFrom_'+counter2+'" name="dishareholderpepFrom[]_'+counter2+'" data-placeholder="From"><option value="" selected>From</option></select><div class="dishareholderpepFrom'+counter2+'-alert" id="dishareholderpepFrom'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderpepFrom'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><select class="custom-select select-w-100" id="dishareholderpepTo_'+counter2+'" name="dishareholderpepTo_'+counter2+'" data-placeholder="To"><option value="">To</option></select><div class="dishareholderpepTo'+counter2+'-alert" id="dishareholderpepTo'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderpepTo'+counter2+'-alert-msg"></span></div></div></div></div></div>';
                     fieldHTML += '</div>'; // end of dishareholderpepform 
                     fieldHTML += '</div>'; // end of background-grey2  
                     fieldHTML += '</div>'; // end of row mt-5 
                     fieldHTML += '</div>'; // end of divclass 

                     if(isFirst == true){
                        $('#firstrole').html(fieldHTML);   
                     }else{
                        $('body').find('#additionalRole:last').before(fieldHTML);             
                     }
		     

                     // PassportManually

                 var pmanually ='<div class="modal fade"  data-keyboard="true" id="dinonresidentpreviewpassportmanually_'+counter2+'" tabindex="-1" role="dialog">';
		pmanually += '<div class="modal-dialog modal-dialog-centered" role="document">';
		pmanually += '<div class="modal-content row">';
		pmanually += '<div class="modal-header"><h5 class="modal-title" id="dinonresidentmanually">Passport Information Manually -di #'+counter2+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		pmanually += '<div class="col-lg-12 background-grey-2 popheight">';
		
		pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="dinonpassportNumber[]" class="form-control" id="dinonpassportNumber_'+counter2+'"  value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Nationality</label><div class="col-sm-12"><select name="dinonnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="dinonnationality_'+counter2+'"></select></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="dinoncountry[]" id="dinoncountry_'+counter2+'"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="dinongender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="dinongender[]" class="radio-red"> Female</label></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="dinondateissue_'+counter2+'" class="form-control" name="dinondateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="dinondateexpiry_'+counter2+'" class="form-control" name="dinondateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                 pmanually +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="dinondatebirth_'+counter2+'" class="form-control" name="dinondatebirth[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="dinondateplace_'+counter2+'" name="dinondateplace[]"></select></div></div></div></div>';

                 pmanually += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'di\','+counter2+',\'dipassportmanually\');">Save</a></div></div></div>';
		pmanually += '</div></div></div></div>'; 
         
                $('body').find('#additionalRole:last').before(pmanually);    

                 //nric manually
                                 
		 var nricfield = '<div class="modal fade"  data-keyboard="true" id="dinonresidentpreviewnricmanually_'+counter2+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfield += '<div class="modal-content row">';
                     nricfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC and Address Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfield += '<div class="col-lg-12 background-grey-2 popheight">'; 
                      nricfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="dinonresidentnricnumber[]" class="form-control" id="dinonresidentnricnumber_'+counter2+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input name="dinonresidentgender[]" class="radio-red" type="radio" id="dinonresidentgender_'+counter2+'" checked> Male</label><label class="radio-inline"><input id="dinonresidentgender_'+counter2+'" name="dinonresidentgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input id="dinonresidentdob_'+counter2+'" class="form-control" name="dinonresidentdob[]"  type="text"   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="dinricnationality" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="dinricnationality_'+counter2+'"></select></div></div></div></div>';

                     nricfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dinricpostcode_'+counter2+'" name="dinricpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea id="dinricstreetname_'+counter2+'" name="dinricstreetname[]" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dinricfloor_'+counter2+'" name="dinricfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dinricunit_'+counter2+'" name="dinricunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'di\','+counter2+',\'dinricmanually\');">Save</a></div></div></div>';

                     nricfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfield); 

                     //fincard manuallyy
               var fincard ='<div class="modal fade"  data-keyboard="true" id="difincardmanually_'+counter2+'" tabindex="-1" role="dialog">';
		fincard += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincard += '<div class="modal-content row">';
		fincard += '<div class="modal-header"><h5 class="modal-title" id="difincardmanually">Passport and WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		fincard += '<div class="col-lg-12 background-grey-2 popheight">';
		
		fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="difincardpassportNumber[]" class="form-control" id="difincardpassportNumber_'+counter2+'"  value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="difincardnationality_'+counter2+'" name="difincardnationality[]"></select></div></div></div></div>';

                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="difincardcountry[]" id="difincardcountry_'+counter2+'"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="difincardgender_'+counter2+'" type="radio" name="difincardgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" id="difincardgender_'+counter2+'"  name="difincardgender[]" class="radio-red"> Female</label></div></div></div></div>';

                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="difincarddateissue_'+counter2+'" class="form-control" name="difincarddateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="difincarddateexpiry_'+counter2+'" class="form-control" name="difincarddateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                 fincard +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="difincarddatebirth_'+counter2+'" class="form-control" name="difincarddatebirth[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="difincarddateplace_'+counter2+'" name="difincarddateplace[]"></select></div></div></div></div>';

                 fincard +='<div class="row my-3"><div class="col-sm"><h4 class="font-weight-bold">Work Pass</h4></div></div>';
                
                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="difincardemployer[]" class="form-control" id="difincardemployer_'+counter2+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="difincardoccupation[]" class="form-control" id="difincardoccupation_'+counter2+'" value="" type="text"></div></div></div></div>'; 
                  fincard += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="diworkpassdateissue_'+counter2+'" class="form-control" name="diworkpassdateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="diworkpassdateexpiry_'+counter2+'" class="form-control" name="diworkpassdateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="diwfincardNumber[]" class="form-control" id="diwfincardNumber_'+counter2+'"  value="" /></div></div></div></div>';        

                 fincard += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'di\','+counter2+',\'difinmanually\');">Save</a></div></div></div>';
		fincard += '</div></div></div></div>';         
                $('body').find('#additionalRole:last').before(fincard); 



		}else{
		    alert('Maximum '+maxGroup+' Director and Shareholder are allowed.');
		}


                //Passport Manually - Non resident

                $('#dinonnationality_'+counter2).html(countriesOption);
                $('#dinonnationality_'+counter2).chosen(); 

                $('#dinoncountry_'+counter2).html(countriesOption);
                $('#dinoncountry_'+counter2).chosen(); 

                $('#dinondateplace_'+counter2).html(countriesOption);
                $('#dinondateplace_'+counter2).chosen(); 

                //nric Manually

                $('#dinricnationality_'+counter2).html(countriesOption);
                $('#dinricnationality_'+counter2).chosen(); 

                // fincard Manually
                $('#difincardnationality_'+counter2).html(countriesOption);
                $('#difincardnationality_'+counter2).chosen(); 
               
                $('#difincardcountry_'+counter2).html(countriesOption);
                $('#difincardcountry_'+counter2).chosen(); 
              
                $('#difincarddateplace_'+counter2).html(countriesOption);
                $('#difincarddateplace_'+counter2).chosen(); 

                $('#dishareholdercountrypep_'+counter2).html(countriesOption);
                $('#dishareholdercountrypep_'+counter2).chosen(); 
                $('#dishareholderpepFrom_'+counter2).html(yearfromOption);
                $('#dishareholderpepFrom_'+counter2).chosen();
                $('#dishareholderpepTo_'+counter2).html(yeartoOption);
                $('#dishareholderpepTo_'+counter2).chosen();

                $('#dinondateissue_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#dinondateexpiry_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#dinondatebirth_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#dinonresidentdob_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#difincarddateissue_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#difincarddateexpiry_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#difincarddatebirth_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#diworkpassdateissue_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#diworkpassdateexpiry_'+counter2).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });


                $('#addroleSelector').prop('selectedIndex',0);
                $("#additionalRole").show(); 

	}

        else if(getvalue.value == 'Corporate Shareholder') {
         /*counter3++;*/
         counter3 = (isFirst==true) ? 1 : counter3+1;
		if(counter3 <= maxGroup){
		    var fieldHTML = '<div id="corporateshareholder_'+counter3+'">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Corporate Shareholder #'+counter3+'</h5></div></div>'; // end of background-grey1
		    fieldHTML += '<div class="col-lg-12 background-grey-2">';
                    fieldHTML += '<div class="form-group row mt-4"><div class="col-lg-4"><label class="col-form-label required">Name of Corporate Shareholder</label></div><div class="col-lg-8"><input type="text" name="corporatename[]" class="form-control" id="corporatename_'+counter3+'"><div class="corporatename'+counter3+'-alert" id="corporatename'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatename'+counter3+'-alert-msg"></span></div></div></div>'; //name of corporate shareholder

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required">Country of Incorporation</label></div><div class="col-lg-8"><div class="selectdiv"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="corporatecountry_'+counter3+'" name="corporatecountry[]"></select><div class="corporatecountry'+counter3+'-alert" id="corporatecountry'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatecountry'+counter3+'-alert-msg"></span></div></div></div></div>'; // Country of Incorporation

                   fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">Upload Document</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" type="file" name="corporatecertificate[]" class="custom-file-input" id="corporatecertificate_'+counter3+'" onchange="corporatecertify(this.files,'+counter3+');"><label id="corporatecertificatelabel_'+counter3+'" class="custom-file-label dashed-lines"></label></div></div><div class="corporatecertificate'+counter3+'-alert" id="corporatecertificate'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatecertificate'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" type="file" name="corporatecertifcate1[]" class="custom-file-input" id="corporatecertificate1_'+counter3+'" onchange="corporatecertify1(this.files,'+counter3+');"><label id="corporatecertificate1label_'+counter3+'" class="custom-file-label dashed-lines"></label></div></div><div class="corporatecertificate1'+counter3+'-alert" id="corporatecertificate1'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatecertificate1'+counter3+'-alert-msg"></span></div></div></div><div class="row"><div class="col-sm"><p class="small font-italic">Certificate of Incorporation</span></div><div class="col-sm"><p class="small font-italic">Extract of the companys details from the Registrar of Companies - Business Profile / Certificate of Incumbency  Registry of Shareholders</span></div></div></div><div class="col-sm-12 border-bottom mb-3">&nbsp;</div></div>'; // Upload Document Section

                    fieldHTML += '<div class="form-group row mt-4"><div class="col-lg-4"><label class="col-form-label">Name of Corporate Representative</label></div><div class="col-lg-8"><input type="text" name="representativename[]" class="form-control" id="representativename_'+counter3+'"><div class="representativename'+counter3+'-alert" id="representativename'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="representativename'+counter3+'-alert-msg"></span></div></div></div>'; // Corporate Representative

                    fieldHTML += '<div class="form-group row" id="corporatestatus_'+counter3+'"><div class="col-lg-4"><label class="col-form-label" >Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="corporateresstatus_'+counter3+'" name="corporateresidencyStatus[]" onchange="corporateresidencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="corporatenonresident_'+counter3+'" rel="nonresident">Non-resident</option><option value="corporatecitizenpr_'+counter3+'" rel="citizenpr">Singapore Citizen / PR</option><option value="corporatepassholder_'+counter3+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="corporateresidencyStatus'+counter3+'-alert" id="corporateresidencyStatus'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateresidencyStatus'+counter3+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                    fieldHTML += '<div id="corporatepassholder_'+counter3+'" class="choose-file form-group" style="display: none;">';

                     fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporateuploadPassport[]" class="custom-file-input" id="corporateuploadPassport_'+counter3+'" type="file" onchange="checkPdfParse(this.files,'+counter3+',finuploadpassportfile,\'corporate\');"><label class="custom-file-label dashed-lines" id="corporatefinpassportlabel_'+counter3+'">Passport</label></div></div><div class="corporateuploadPassport'+counter3+'-alert" id="corporateuploadPassport'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateuploadPassport'+counter3+'-alert-msg"></span></div><div class="row"><div id="bcorporatefincardpreviewpassport_'+counter3+'" class="col-sm-12" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#corporatefincardpreviewpassport_'+counter3+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your Passport Details</button></div></div><div class="row" id="corporateuploadPassportloadingImage_'+counter3+'" style="display:none;"><div class="col-sm-6 text-center"><img src="images/image-loading.gif"></div></div></div>OR<div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#corporatefincardmanually_'+counter3+'" id="corporatebuttonfincardmanually_'+counter3+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div>';

	            fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatefincardfront[]" class="custom-file-input" id="corporatefincardfront_'+counter3+'" type="file" onchange="checkPdfParse(this.files,'+counter3+',fincardfront,\'corporate\');"><label class="custom-file-label dashed-lines" id="corporatefincardfrontlabel_'+counter3+'">FIN Card Front</label></div></div><div class="corporatefincardfront'+counter3+'-alert" id="corporatefincardfront'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatefincardfront'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatefincardback[]" class="custom-file-input" id="corporatefincardback_'+counter3+'" type="file" onchange="checkPdfParse(this.files,'+counter3+',fincardback,\'corporate\');"><label class="custom-file-label dashed-lines" id="corporatefincardbacklabel_'+counter3+'">FIN Card Back</label></div></div><div class="corporatefincardback'+counter3+'-alert" id="corporatefincardback'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatefincardback'+counter3+'-alert-msg"></span></div></div></div><div class="row"><div id="bcorporatefincardfrontpreview_'+counter3+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#corporatefincardfrontpreview_'+counter3+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your front fin details</button></div><div id="bcorporatefincardbackpreview_'+counter3+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#corporatefincardbackpreview_'+counter3+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your fin back details</button></div></div><div class="row" id="corporateuploadfincardloadingImage_'+counter3+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


	           fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right" title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporateaddress[]" class="custom-file-input" id="corporateaddress_'+counter3+'" type="file" onchange="corporateaddress(this.files,'+counter3+');" accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx"><label class="custom-file-label dashed-lines" id="corporateaddresslabel_'+counter3+'">Address</label></div></div><div class="corporateaddress'+counter3+'-alert" id="corporateaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateaddress'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="corporateresaddress[]" id="corporateresaddress_'+counter3+'" rows="2" placeholder="Please key in residential address"></textarea><div class="corporateresaddress'+counter3+'-alert" id="corporateresaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateresaddress'+counter3+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="corporatecitizenpr_'+counter3+'" class="choose-file" style="display: none;">';

                     fieldHTML +='<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatenricfront[]" class="custom-file-input" id="corporatenricfront_'+counter3+'" type="file" onchange="checkPdfParse(this.files,'+counter3+',nricfront,\'corporate\');"><label class="custom-file-label dashed-lines" id="corporatenricfrontlabel_'+counter3+'">NRIC Front</label></div></div><div class="corporatenricfront'+counter3+'-alert" id="corporatenricfront'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenricfront'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatenricback[]" class="custom-file-input" id="corporatenricback_'+counter3+'" type="file" onchange="checkPdfParse(this.files,'+counter3+',nricback,\'corporate\');"><label class="custom-file-label dashed-lines" id="corporatenricbacklabel_'+counter3+'">NRIC Back</label></div></div><div class="corporatenricback'+counter3+'-alert" id="corporatenricback'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenricback'+counter3+'-alert-msg"></span></div></div></div><div class="row"><div id="bcorporatenricfrontpreview_'+counter3+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#corporatenricfrontpreview_'+counter3+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Front details</button></div><div id="bcorporatenricbackpreview_'+counter3+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#corporatenricbackpreview_'+counter3+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Back details</button></div></div><div class="row" id="corporateuploadnricloadingImage_'+counter3+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div><div class="row"><div class="col-sm-6"><button data-target="#corporatenonresidentpreviewnricmanually_'+counter3+'" type="button" class="btn btn-style-1" data-toggle="modal" id="corporatebuttonnricmanually_'+counter3+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="corporatenonresident_'+counter3+'" class="choose-file" style="display:none;">';

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" >Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatenonresidentuploadPassport[]" class="custom-file-input" id="corporatenonresidentuploadpassport_'+counter3+'" type="file" onchange="checkPdfParse(this.files,'+counter3+',nonuploadpassportfile,\'corporate\');"><label class="custom-file-label dashed-lines" id="corporatenonresidentlabel_'+counter3+'">Passport</label></div></div><div class="corporatenonresidentuploadpassport'+counter3+'-alert" id="corporatenonresidentuploadpassport'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenonresidentuploadpassport'+counter3+'-alert-msg"></span></div></div>OR<div class="col-sm"><button id="corporatebuttonpassportmanually_'+counter3+'" type="button" class="btn btn-style-1" data-toggle="modal" data-target="#corporatenonresidentpreviewpassportmanually_'+counter3+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div  id="bcorporatenonresidentpreviewpassport_'+counter3+'" class="col-sm-6"  style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#corporatenonresidentpreviewpassport_'+counter3+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div><div class="row" id="corporateuploadpassportfileloadingImage_'+counter3+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" name="corporatenonresidentaddress[]" class="custom-file-input" id="corporatenonresidentaddress_'+counter3+'" type="file" onchange="corporatenonresidentaddress(this.files,'+counter3+');"><label class="custom-file-label dashed-lines" id="corporatenonresidentaddresslabel_'+counter3+'">Address</label></div></div><div class="corporatenonresidentaddress'+counter3+'-alert" id="corporatenonresidentaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenonresidentaddress'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="corporatenonresidentresaddress[]" id="corporatenonresidentresaddress_'+counter3+'" rows="2" placeholder="Please key in residential address"></textarea><div class="corporatenonresidentresaddress'+counter3+'-alert" id="corporatenonresidentresaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenonresidentresaddress'+counter3+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident
 

                    fieldHTML += '<div class="form-group row"><label class="col-lg-4 col-form-label" >Contact Details</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input type="text" name="corporateemail[]" class="form-control mb-2" id="corporateemail_'+counter3+'" placeholder="Email" /><div class="corporatebizEmail'+counter3+'-alert" id="corporatebizEmail'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatebizEmail'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><input type="text" name="corporatecontactnumber[]" class="form-control mb-2" id="corporatecontactnumber_'+counter3+'" placeholder="Contact number"/><div class="corporatecontactNumber'+counter3+'-alert" id="corporatecontactNumber'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatecontactNumber'+counter3+'-alert-msg"></span></div><input type="hidden" name="corporatechartname[]" class="form-control mb-2" id="corporatechartname_'+counter3+'"/></div></div></div></div>'; // Contact Details

                    fieldHTML += '<div class="form-group row notice"><div class="col-lg-8">We will need to know the individual ultimate beneficiary owner of the company. Please upload shareholding chart.<a href="#" class="color-red font-italic" data-toggle="modal" data-target="#shareholdingchart">(See sample)</a>.</div><div class="col-lg-4"><label id="chartname_'+counter3+'" class="custom-file-upload-button btn btn-next btn-lg select-w-100"><i class="fa fa-cloud-upload"></i> Upload Chart</label><input accept=".jpg,.jpeg,.png,.psd,.pdf,.zip,.xls,.rar,.doc,.docx" id="chartname" type="file" onchange="chartupload(this.files,'+counter3+');" style="position:absolute;left:20px;opacity:0;cursor:pointer;"/></div></div>'; // Chart Portions

                     fieldHTML += '<div class="row" id="corporatechartloadingImage_'+counter3+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>'; // Chart LOading Portions

                    fieldHTML += '</div>'; // end of background-grey2 
                    fieldHTML += '</div>'; // end of row mt-5 
                    fieldHTML += '</div>'; // end of divclass 

                    if(isFirst == true){
                        $('#firstrole').html(fieldHTML);   
                     }else{
                        $('body').find('#additionalRole:last').before(fieldHTML);             
                     }
		    

                 // PassportManually

                 var pmanually ='<div class="modal fade"  data-keyboard="true" id="corporatenonresidentpreviewpassportmanually_'+counter3+'" tabindex="-1" role="dialog">';
		pmanually += '<div class="modal-dialog modal-dialog-centered" role="document">';
		pmanually += '<div class="modal-content row">';
		pmanually += '<div class="modal-header"><h5 class="modal-title" id="corporatenonresidentmanually">Passport Information Manually -corporate #'+counter3+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		pmanually += '<div class="col-lg-12 background-grey-2 popheight">';
		
		pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="corporatenonpassportNumber[]" class="form-control" id="corporatenonpassportNumber_'+counter3+'"  value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Nationality</label><div class="col-sm-12"><select name="corporatenonnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatenonnationality_'+counter3+'"></select></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="corporatenoncountry[]" id="corporatenoncountry_'+counter3+'"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="corporatenongender[]" class="radio-red" id="corporatenonmgender_'+counter3+'" checked>Male</label><label class="radio-inline"><input type="radio" name="corporatenongender[]" class="radio-red" id="corporatenonfgender_'+counter3+'"> Female</label></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="corporatenondateissue_'+counter3+'" class="form-control" name="corporatenondateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"   maxlength="10" placeholder="DD-MM-YYYY"><input type="text" id="corporatenondateexpiry_'+counter3+'" class="form-control" name="corporatenondateexpiry[]" value=""></div></div></div></div></div>';

                 pmanually +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="corporatenondatebirth_'+counter3+'" class="form-control" name="corporatenondatebirth[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatenondateplace_'+counter3+'" name="corporatenondateplace[]"></select></div></div></div></div>';

                 pmanually += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'corporate\','+counter3+',\'corporatepassportmanually\');">Save</a></div></div></div>';
		pmanually += '</div></div></div></div>'; 
         
                $('body').find('#additionalRole:last').before(pmanually);    

                 //nric manually
                                 
		 var nricfield = '<div class="modal fade"  data-keyboard="true" id="corporatenonresidentpreviewnricmanually_'+counter3+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfield += '<div class="modal-content row">';
                     nricfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC and Address Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfield += '<div class="col-lg-12 background-grey-2 popheight">'; 
                      nricfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="corporatenonresidentnricnumber[]" class="form-control" id="corporatenonresidentnricnumber_'+counter3+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input name="corporatenonresidentgender[]" id="corporatenonresidentmgender_'+counter3+'" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input id="corporatenonresidentfgender_'+counter3+'" name="corporatenonresidentgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input id="corporatenonresidentdob_'+counter3+'" class="form-control" name="corporatenonresidentdob[]"  type="text"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required"   maxlength="10" placeholder="DD-MM-YYYY">Country of Birth</label><div class="col-sm-12"><select name="corporatenricnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatenricnationality_'+counter3+'"></select></div></div></div></div>';

                     nricfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatenricpostcode_'+counter3+'" name="corporatenricpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea id="corporatenricstreetname_'+counter3+'" name="corporatenricstreetname[]" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatenricfloor_'+counter3+'" name="corporatenricfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatenricunit_'+counter3+'" name="corporatenricunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'corporate\','+counter3+',\'corporatenricmanually\');">Save</a></div></div></div>';

                     nricfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfield); 

                     //fincard manuallyy
               var fincard ='<div class="modal fade"  data-keyboard="true" id="corporatefincardmanually_'+counter3+'" tabindex="-1" role="dialog">';
		fincard += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincard += '<div class="modal-content row">';
		fincard += '<div class="modal-header"><h5 class="modal-title" id="corporatefincardmanually">Passport and WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		fincard += '<div class="col-lg-12 background-grey-2 popheight">';
		
		fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="corporatefincardpassportNumber[]" class="form-control" id="corporatefincardpassportNumber_'+counter3+'"  value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatefincardnationality_'+counter3+'" name="corporatefincardnationality[]"></select></div></div></div></div>';

                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="corporatefincardcountry[]" id="corporatefincardcountry_'+counter3+'"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="corporatefincardgender[]" class="radio-red" id="corporatefincardgender_'+counter3+'" checked>Male</label><label class="radio-inline"><input type="radio" name="corporatefincardgender[]" class="radio-red" id="corporatefincardgender_'+counter3+'"> Female</label></div></div></div></div>';

                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="corporatefincarddateissue_'+counter3+'" class="form-control" name="corporatefincarddateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="corporatefincarddateexpiry_'+counter3+'" class="form-control" name="corporatefincarddateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                 fincard +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="corporatefincarddatebirth_'+counter3+'" class="form-control" name="corporatefincarddatebirth[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatefincarddateplace_'+counter3+'" name="corporatefincarddateplace[]"></select></div></div></div></div>';

                 fincard +='<div class="row my-3"><div class="col-sm"><h4 class="font-weight-bold">Work Pass</h4></div></div>';
                
                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="corporatefincardemployer[]" class="form-control" id="corporatefincardemployer_'+counter3+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="corporatefincardoccupation[]" class="form-control" id="corporatefincardoccupation_'+counter3+'" value="" type="text"></div></div></div></div>'; 
                  fincard += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="corporateworkpassdateissue_'+counter3+'" class="form-control" name="corporateworkpassdateissue[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="corporateworkpassdateexpiry_'+counter3+'" class="form-control" name="corporateworkpassdateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                 fincard += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="corporatewfincardNumber[]" class="form-control" id="corporatewfincardNumber_'+counter3+'"  value="" /></div></div></div></div>';        

                 fincard += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaymanualpopup(\'corporate\','+counter3+',\'corporatefincardmanually\');">Save</a></div></div></div>';
		fincard += '</div></div></div></div>';         
                $('body').find('#additionalRole:last').before(fincard); 

		}else{
		    alert('Maximum '+maxGroup+' Corporate Shareholder are allowed.');
		}
                
                //Passport Manually - Non resident

                $('#corporatenonnationality_'+counter3).html(countriesOption);
                $('#corporatenonnationality_'+counter3).chosen(); 

                $('#corporatenoncountry_'+counter3).html(countriesOption);
                $('#corporatenoncountry_'+counter3).chosen(); 

                $('#corporatenondateplace_'+counter3).html(countriesOption);
                $('#corporatenondateplace_'+counter3).chosen(); 

                //nric Manually

                $('#corporatenricnationality_'+counter3).html(countriesOption);
                $('#corporatenricnationality_'+counter3).chosen(); 

                // fincard Manually
                $('#corporatefincardnationality_'+counter3).html(countriesOption);
                $('#corporatefincardnationality_'+counter3).chosen(); 
               
                $('#corporatefincardcountry_'+counter3).html(countriesOption);
                $('#corporatefincardcountry_'+counter3).chosen(); 
              
                $('#corporatefincarddateplace_'+counter3).html(countriesOption);
                $('#corporatefincarddateplace_'+counter3).chosen(); 
 
                $('#corporatecountry_'+counter3).html(countriesOption);
                $('#corporatecountry_'+counter3).chosen();  

                $('#corporatenondateissue_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#corporatenondateexpiry_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#corporatenondatebirth_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#corporatenonresidentdob_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#corporatefincarddateissue_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#corporatefincarddateexpiry_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#corporatefincarddatebirth_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#corporateworkpassdateissue_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#corporateworkpassdateexpiry_'+counter3).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#addroleSelector').prop('selectedIndex',0);
                $("#additionalRole").show(); 

	}
        else if(getvalue.value == 'Nomore') {

              //alert("Are you ok to proceed further as you selected No More Additional Role")
              $("#displaysuccessmsg .modal-title").html("Role");
              $("#displaysuccessmsg .modal-body").html("Are you OK to proceed further? You have selected 'No more additional role'.");
              $("#displaysuccessmsg").modal("show"); 
              $("#additionalRole").show(); 

        }else {

             //$("#additionalRole").hide(); 

        }
        

}

// For Director only
function residencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "directornonresident_"+arr[1]) {
          //alert("fddf");

	   $("#directornonresident_"+arr[1]).css("display", "block");
	   $("#directorcitizenpr_"+arr[1]).css("display", "none");
	   $("#directorpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "directorcitizenpr_"+arr[1]) {

	   $("#directornonresident_"+arr[1]).css("display", "none");
	   $("#directorcitizenpr_"+arr[1]).css("display", "block");
	   $("#directorpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "directorpassholder_"+arr[1]) {

	   $("#directornonresident_"+arr[1]).css("display", "none");
	   $("#directorcitizenpr_"+arr[1]).css("display", "none");
	   $("#directorpassholder_"+arr[1]).css("display", "block");
	}
        else {
 
           uploadFields.hide();

        }

}


// For Shareholder only
function shareholderresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "shareholdernonresident_"+arr[1]) {
          //alert("fddf");

	   $("#shareholdernonresident_"+arr[1]).css("display", "block");
	   $("#shareholdercitizenpr_"+arr[1]).css("display", "none");
	   $("#shareholderpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "shareholdercitizenpr_"+arr[1]) {

	   $("#shareholdernonresident_"+arr[1]).css("display", "none");
	   $("#shareholdercitizenpr_"+arr[1]).css("display", "block");
	   $("#shareholderpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "shareholderpassholder_"+arr[1]) {

	   $("#shareholdernonresident_"+arr[1]).css("display", "none");
	   $("#shareholdercitizenpr_"+arr[1]).css("display", "none");
	   $("#shareholderpassholder_"+arr[1]).css("display", "block");
	}
        else {
 
            uploadFields.hide();

        }

}


// For director & Shareholder only
function dishareholderresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "dinonresident_"+arr[1]) {
          //alert("fddf");

	   $("#dinonresident_"+arr[1]).css("display", "block");
	   $("#dicitizenpr_"+arr[1]).css("display", "none");
	   $("#dipassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "dicitizenpr_"+arr[1]) {

	   $("#dinonresident_"+arr[1]).css("display", "none");
	   $("#dicitizenpr_"+arr[1]).css("display", "block");
	   $("#dipassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "dipassholder_"+arr[1]) {

	   $("#dinonresident_"+arr[1]).css("display", "none");
	   $("#dicitizenpr_"+arr[1]).css("display", "none");
	   $("#dipassholder_"+arr[1]).css("display", "block");
	}
        else {
 
           uploadFields.hide();
  
        }

}

// For Corporate only
function corporateresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "corporatenonresident_"+arr[1]) {
          //alert("fddf");

	   $("#corporatenonresident_"+arr[1]).css("display", "block");
	   $("#corporatecitizenpr_"+arr[1]).css("display", "none");
	   $("#corporatepassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "corporatecitizenpr_"+arr[1]) {

	   $("#corporatenonresident_"+arr[1]).css("display", "none");
	   $("#corporatecitizenpr_"+arr[1]).css("display", "block");
	   $("#corporatepassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "corporatepassholder_"+arr[1]) {

	   $("#corporatenonresident_"+arr[1]).css("display", "none");
	   $("#corporatecitizenpr_"+arr[1]).css("display", "none");
	   $("#corporatepassholder_"+arr[1]).css("display", "block");
	}
        else {
 
           //alert("ddf");
            uploadFields.hide();
           
        }

}


function ShowHidedDiv(getdirector){

var arr = getdirector.split('_'); 

	if (getdirector == "directorpepyes_"+arr[1]) {
	   $(".show-when-pep_"+arr[1]).css("display", "block");
	} else {
	   $(".show-when-pep_"+arr[1]).css("display", "none");
	}

}


function ShowHidesDiv(getshareholder){

var arr = getshareholder.split('_'); 

	if (getshareholder == "shareholderpepyes_"+arr[1]) {
                $(".show-when-shareholderpep_"+arr[1]).css("display", "block");
	} else {
		$(".show-when-shareholderpep_"+arr[1]).css("display", "none");
	}

}

function ShowHidesdDiv(getdshareholder){

var arr = getdshareholder.split('_'); 

	if (getdshareholder == "dishareholderpepyes_"+arr[1]) {
                $(".show-when-dishareholderpep_"+arr[1]).css("display", "block");
	} else {
		$(".show-when-dishareholderpep_"+arr[1]).css("display", "none");
	}

}
</script>

<?php include('includes/checkvalidation.php');?>
<?php include('includes/previewpopup.php');?>
<?php include('includes/previewvalidation.php');?>
<?php include('includes/manualvalidation.php');?>
<?php include('includes/addressection.php');?>



<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src='<?php echo BASE_URL;?>/js/datepicker.js?v6'></script>



