<script>
function bonricsavemanually(getcounter,getboname){

        if( $('#bopreviewnricmanually_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
	    console.log("Modal is open");
            $('body').css("overflow","hidden");
            $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden"); 
            $('#bopreviewnricmanually_'+getcounter+"_"+getboname).css("overflow","auto");
	}

var bononresidentnricnumber = document.getElementById("bononresidentnricnumber_"+getcounter+"_"+getboname).value;
var bononresidentgender = $("input[name='bononresidentgender[]']:checked").val();
var bononresidentdob = document.getElementById("bononresidentdob_"+getcounter+"_"+getboname).value;
var bonricnationality = $('#bonricnationality_'+getcounter+'_'+getboname+'_chosen span').text();
var bonricpostcode = document.getElementById("bonricpostcode_"+getcounter+"_"+getboname).value;
var bonricstreetname = document.getElementById("bonricstreetname_"+getcounter+"_"+getboname).value;
var bonricfloor = document.getElementById("bonricfloor_"+getcounter+"_"+getboname).value;
var bonricunit = document.getElementById("bonricunit_"+getcounter+"_"+getboname).value;
var getnonresidentdob = formatingdata(bononresidentdob);

localStorage.setItem("bopreviewmanuallynricnumber_"+getcounter+"_"+getboname,bononresidentnricnumber);
localStorage.setItem("bopreviewmanuallynricgender_"+getcounter+"_"+getboname,bononresidentgender);
localStorage.setItem("bopreviewmanuallynricdob_"+getcounter+"_"+getboname,bononresidentdob);
localStorage.setItem("bopreviewmanuallynricnationality_"+getcounter+"_"+getboname,bonricnationality);
localStorage.setItem("bopreviewmanuallynricpostcode_"+getcounter+"_"+getboname,bonricpostcode);
localStorage.setItem("bopreviewmanuallynricstreetname_"+getcounter+"_"+getboname,bonricstreetname);
localStorage.setItem("bopreviewmanuallynricfloor_"+getcounter+"_"+getboname,bonricfloor);
localStorage.setItem("bopreviewnrmanuallyicunit_"+getcounter+"_"+getboname,bonricunit);

var cur_date = new Date();
//var dob =  new Date(bononresidentdob.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
var dob = $("#bononresidentdob_"+getcounter+"_"+getboname).datepicker('getDate');
//alert(bononresidentgender);

        if(bononresidentnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(bononresidentgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("Gender cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(bononresidentdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("Date of birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(bonricnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bonricpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}else if(isNaN(bonricpostcode)){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
            $("#displaysuccessmsg .modal-body").html("Invalid Postcode");
            $("#displaysuccessmsg").modal("show");
        }
        else if(bonricstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Street name cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(bonricfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}else if(isNaN(bonricfloor)){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Invalid Floor");
             $("#displaysuccessmsg").modal("show");
        }
        else if(bonricunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}
        else if(bononresidentdob.length < 10){
              $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bononresidentdob != '' && getnonresidentdob == true){
             $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot exceed the current date");
             $("#displaysuccessmsg").modal("show"); 
        }           
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_BO_CITIZENPR_NRIC, true);
	   //showNRICEmpty(ROLE_bo, getcounter, true, false);
	   //showNRICEmpty(ROLE_bo, getcounter, false, false);
           $('#bopreviewnricmanually_'+getcounter+"_"+getboname).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  NRIC Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
           $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show");
	}




}

</script>
