<script>
function bopassportsavemanually(getcounter,getboname){

        if( $('#bopreviewpassportmanually_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
	    console.log("Modal is open");
            $('body').css("overflow","hidden");
            $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden"); 
            $('#bopreviewpassportmanually_'+getcounter+"_"+getboname).css("overflow","auto");
	}

        var bononpassportno = document.getElementById("bopassportNumber_"+getcounter+"_"+getboname).value;
	var bononnationality = $('#bononnationality_'+getcounter+'_'+getboname+'_chosen span').text();
	var bononcountry = $('#bononcountry_'+getcounter+'_'+getboname+'_chosen span').text();
	var bonongender = $("input[name='bonongender[]']:checked").val();
	var bonondatetimepicker = document.getElementById("bonondateissue_"+getcounter+"_"+getboname).value;
	var bonondateexpiry = document.getElementById("bonondateexpiry_"+getcounter+"_"+getboname).value;
	var bononplaceofBirth = $('#bonondateplace_'+getcounter+'_'+getboname+'_chosen span').text();
        var bonondateBirth = document.getElementById("bonondatebirth_"+getcounter+"_"+getboname).value;

	//var startdate = new Date(bonondatetimepicker.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
	//var enddate =  new Date(bonondateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

	localStorage.setItem("bononmanuallypassportno_"+getcounter+"_"+getboname,bononpassportno);
	localStorage.setItem("bononmanuallynationality_"+getcounter+"_"+getboname,bononnationality);
	localStorage.setItem("bononmanuallycountry_"+getcounter+"_"+getboname,bononcountry);
	localStorage.setItem("bononmanuallygender_"+getcounter+"_"+getboname,bonongender);
	localStorage.setItem("bononmanuallydatetimepicker_"+getcounter+"_"+getboname,bonondatetimepicker);
	localStorage.setItem("bononmanuallydateexpiry_"+getcounter+"_"+getboname,bonondateexpiry);
	localStorage.setItem("bononmanuallyplaceofBirth_"+getcounter+"_"+getboname,bononplaceofBirth);
        localStorage.setItem("bononmanuallydateBirth_"+getcounter+"_"+getboname,bonondateBirth);

        //var cur_date = new Date();
        //var dob =  new Date(bonondateBirth.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

        var cur_date = new Date();
	var dob = $("#bonondatebirth_"+getcounter+"_"+getboname).datepicker('getDate');
	var startdate = $("#bonondateissue_"+getcounter+"_"+getboname).datepicker('getDate');
	var enddate = $("#bonondateexpiry_"+getcounter+"_"+getboname).datepicker('getDate');

        var getnondatetimepicker = formatingdata(bonondatetimepicker);
	var getnondateexpiry = formatingdata(bonondateexpiry);
	var getnondateBirth = formatingdata(bonondateBirth);

        if(bononpassportno == ''){
          
           //alert("Pass port Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	   $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
	   $("#displaysuccessmsg").modal("show");

	}
	else if(bononnationality == 'Please select'){

            //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	   $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
	   $("#displaysuccessmsg").modal("show");


	}
        else if(bononcountry == 'Please select'){

           //alert("Country cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	   $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
	   $("#displaysuccessmsg").modal("show");

	}
        else if(bonongender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	     $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
	     $("#displaysuccessmsg").modal("show");
	}
        else if(bonondatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	     $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
	     $("#displaysuccessmsg").modal("show");
	}
        else if(bonondateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
	      $("#displaysuccessmsg").modal("show");

	}
        else if(bonondateBirth == ''){

              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");    
	}  
        else if(bononplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	      $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 
	}
        else if(bonondatetimepicker != '' && getnondatetimepicker == true){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(bonondateexpiry != '' && getnondateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bonondateBirth != '' && getnondateBirth == true){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bonondatetimepicker.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }

        else if(bonondateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }

        else if(bonondateBirth.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(startdate >= enddate){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        } else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        }            
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_BO_NONRESIDENT_PASSPORT, true);
           //showPassportEmpty(ROLE_bo, getcounter, false);
	   $('#bopreviewpassportmanually_'+getcounter+"_"+getboname).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Beneficial Owner -  Manual");
	   $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
           $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
	   $("#displaysuccessmsg").modal("show");   

       }
}

</script>
