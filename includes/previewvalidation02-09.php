<script>
function displaypreviewpopup(rolename,getcounter,getresidencystatus){

var getrolename = rolename.toUpperCase();

if(getresidencystatus == ''+rolename+'nonresident'){

if( $('#'+rolename+'nonresidentpreviewpassport_'+getcounter).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#'+rolename+'nonresidentpreviewpassport_'+getcounter).css("overflow","auto");
}

var nonresidentsurname = document.getElementById(""+rolename+"nonresidentpsurname_"+getcounter).value;
var nonresidentgivenname = document.getElementById(""+rolename+"nonresidentpgivenName_"+getcounter).value;
var nonresidentpassportno = document.getElementById(""+rolename+"nonresidentppassportNumber_"+getcounter).value;
var nonresidentnationality = $('#'+rolename+'nonresidentpnationality_'+getcounter+'_chosen span').text();
var nonresidentcountry = $('#'+rolename+'nonresidentpcountry_'+getcounter+'_chosen span').text();
var nonresidentgender = $("input[name='"+rolename+"nonresidentpgender[]']:checked").val();
var previewdob = document.getElementById(""+rolename+"nonresidentpdob_"+getcounter).value;
var nonresidentdateexpiry = document.getElementById(""+rolename+"nonresidentpdateexpiry_"+getcounter).value;

var getpreviewdob = formatingdata(previewdob);
var getnonresidentdateexpiry = formatingdata(nonresidentdateexpiry);

var cur_date = new Date();
//var dob =  checkfuturedata(previewdob);
var dob = $('#'+rolename+'nonresidentpdob_'+getcounter).datepicker('getDate');

localStorage.setItem(""+rolename+"nonresidentpopupsurname_"+getcounter,nonresidentsurname);
localStorage.setItem(""+rolename+"nonresidentpopupgivenName_"+getcounter,nonresidentgivenname);
localStorage.setItem(""+rolename+"nonresidentpopuppassportno_"+getcounter,nonresidentpassportno);
localStorage.setItem(""+rolename+"nonresidentpopupnationality_"+getcounter,nonresidentnationality);
localStorage.setItem(""+rolename+"nonresidentpopupcountry_"+getcounter,nonresidentcountry);
localStorage.setItem(""+rolename+"nonresidentpopupgender_"+getcounter,nonresidentgender);
localStorage.setItem(""+rolename+"nonresidentpopupdob_"+getcounter,previewdob);
localStorage.setItem(""+rolename+"nonresidentpopupdateexpiry_"+getcounter,nonresidentdateexpiry);

	if(nonresidentsurname == ''){
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
            $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
	else if(nonresidentgivenname == ''){

            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
            $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(nonresidentpassportno == ''){
          

            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(nonresidentnationality == 'Please select'){


            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(nonresidentcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(nonresidentgender == ''){


             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");
	}
        else if(previewdob == ''){

             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(nonresidentdateexpiry == ''){           

              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");
	} 
        else if(previewdob != '' && getpreviewdob == true){           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
              $("#displaysuccessmsg .modal-body").html("Invalid Date of Birth");
              $("#displaysuccessmsg").modal("show");
	} 
        else if(nonresidentdateexpiry != '' && getnonresidentdateexpiry == true){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
              $("#displaysuccessmsg .modal-body").html("Invalid Date of Expiry");
              $("#displaysuccessmsg").modal("show");

	}
        else if(previewdob.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(nonresidentdateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be future date");
             $("#displaysuccessmsg").modal("show"); 
        }       
	else{
	   //alert("Saved Succesfully");
	    $('#'+rolename+'nonresidentpreviewpassport_'+getcounter).modal('hide');
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Preview");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto"); 
            $("#"+rolename+"buttonpassportmanually_"+getcounter).prop('disabled', true);
            $("#displaysuccessmsg").modal("show");
	}
}

if(getresidencystatus == ''+rolename+'nricfront'){

if( $('#'+rolename+'nricfrontpreview_'+getcounter).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#'+rolename+'nricfrontpreview_'+getcounter).css("overflow","auto");
}

var previewfrontnricnumber = document.getElementById(""+rolename+"previewfrontnricnumber_"+getcounter).value;
var previewnricfrontgender = $("input[name='"+rolename+"previewnricfrontgender[]']:checked").val();
var previewnricfrontdob = document.getElementById(""+rolename+"previewnricfrontdob_"+getcounter).value;
var previewnricfrontnationality = $('#'+rolename+'previewnricfrontnationality_'+getcounter+'_chosen span').text();
var getdirectornricdob = formatingdata(previewnricfrontdob);

var cur_date = new Date();
//var dob =  checkfuturedata(previewnricfrontdob);
var dob = $('#'+rolename+'previewnricfrontdob_'+getcounter).datepicker('getDate');

localStorage.setItem(""+getrolename+"previewpopupfrontnricnumber_"+getcounter,previewfrontnricnumber);
localStorage.setItem(""+getrolename+"previewpopupnricfrontgender_"+getcounter,previewnricfrontgender);
localStorage.setItem(""+getrolename+"previewpopupnricfrontdob_"+getcounter,previewnricfrontdob);
localStorage.setItem(""+getrolename+"previewpopupnricfrontnationality_"+getcounter,previewnricfrontnationality);


        if(previewfrontnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(previewnricfrontgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show");

	}
	else if(previewnricfrontdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
        else if(previewnricfrontdob != '' && getdirectornricdob == true){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Invalid Date of Birth");
           $("#displaysuccessmsg").modal("show");

	}
	else if(previewnricfrontnationality == 'Please select'){

           //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
        else if(previewnricfrontdob.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
         else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be future date");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else{
	   //alert("Saved Succesfully");
	   $('#'+rolename+'nricfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" - NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('body').css("overflow","auto"); 
           $("#"+rolename+"buttonnricmanually_"+getcounter).prop('disabled', true);
           $("#displaysuccessmsg").modal("show");
	}


}

if(getresidencystatus == ''+rolename+'nricback'){

if( $('#'+rolename+'nricbackpreview_'+getcounter).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#'+rolename+'nricbackpreview_'+getcounter).css("overflow","auto");
}


var previewnricbackpostcode = document.getElementById(""+rolename+"previewnricbackpostcode_"+getcounter).value;
var previewnricbackstreetname = document.getElementById(""+rolename+"previewnricbackstreetname_"+getcounter).value;
var previewnricbackfloor = document.getElementById(""+rolename+"previewnricbackfloor_"+getcounter).value;
var previewnricbackunit = document.getElementById(""+rolename+"previewnricbackunit_"+getcounter).value;


localStorage.setItem(""+rolename+"previewpopupnricbackpostcode_"+getcounter,previewnricbackpostcode);
localStorage.setItem(""+rolename+"previewpopupnricbackstreetname_"+getcounter,previewnricbackstreetname);
localStorage.setItem(""+rolename+"previewpopupnricbackfloor_"+getcounter,previewnricbackfloor);
localStorage.setItem(""+rolename+"previewnrpopupicbackunit_"+getcounter,previewnricbackunit);

        if(previewnricbackpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Preview");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}else if(isNaN(previewnricbackpostcode)){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Preview");
            $("#displaysuccessmsg .modal-body").html("Invalid Postcode");
            $("#displaysuccessmsg").modal("show");
  }          
        else if(previewnricbackstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(previewnricbackfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}else if(isNaN(previewnricbackfloor)){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Invalid Floor");
             $("#displaysuccessmsg").modal("show");
  }
        else if(previewnricbackunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#'+rolename+'nricbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#"+rolename+"buttonnricmanually_"+getcounter).prop('disabled', true);
           $('body').css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show");
	}


}
if(getresidencystatus == ''+rolename+'finpassport'){

if( $('#'+rolename+'fincardpreviewpassport_'+getcounter).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#'+rolename+'fincardpreviewpassport_'+getcounter).css("overflow","auto");
}

var fincardsurname = document.getElementById(""+rolename+"fincardpsurname_"+getcounter).value;
var fincardgivenname = document.getElementById(""+rolename+"fincardpgivenName_"+getcounter).value;
var fincardpassportno = document.getElementById(""+rolename+"fincardppassportNumber_"+getcounter).value;
var fincardnationality = $('#'+rolename+'fincardpnationality_'+getcounter+'_chosen span').text();
var fincardcountry = $('#'+rolename+'fincardpcountry_'+getcounter+'_chosen span').text();
var fincardgender = $("input[name='"+rolename+"fincardpgender[]']:checked").val();
var fincarddateexpiry = document.getElementById(""+rolename+"fincardpdateexpiry_"+getcounter).value;
var fincarddob = document.getElementById(""+rolename+"fincardpdob_"+getcounter).value;

var getfincarddateexpiry = formatingdata(fincarddateexpiry);
var getfincarddob = formatingdata(fincarddob);

var cur_date = new Date();
//var dob =  checkfuturedata(fincarddob);
var dob = $('#'+rolename+'fincardpdob_'+getcounter).datepicker('getDate');

//alert(directorsurname);

localStorage.setItem(""+rolename+"fincardpopupsurname_"+getcounter,fincardsurname);
localStorage.setItem(""+rolename+"fincardpopupgivenName_"+getcounter,fincardgivenname);
localStorage.setItem(""+rolename+"fincardpopuppassportno_"+getcounter,fincardpassportno);
localStorage.setItem(""+rolename+"fincardpopupnationality_"+getcounter,fincardnationality);
localStorage.setItem(""+rolename+"fincardpopupcountry_"+getcounter,fincardcountry);
localStorage.setItem(""+rolename+"fincardpopupgender_"+getcounter,fincardgender);
localStorage.setItem(""+rolename+"fincardpopupdateexpiry_"+getcounter,fincarddateexpiry);
localStorage.setItem(""+rolename+"fincardpopupdob_"+getcounter,fincarddob);

        if(fincardsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(fincardgivenname == ''){

           //alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(fincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Passport number cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(fincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(fincardcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}        
        else if(fincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show");
	} 
        else if(fincarddob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(fincarddateexpiry != '' && getfincarddateexpiry == true){
           
              //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
           $("#displaysuccessmsg").modal("show");
	} 
        else if(fincarddob != '' && getfincarddob == true){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show");
	}
        else if(fincarddob.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(fincarddateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Passport - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be future date");
             $("#displaysuccessmsg").modal("show"); 
        }         
	else{
	   //alert("Saved Succesfully");
	   $('#'+rolename+'fincardpreviewpassport_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('body').css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show");
	}

}

if(getresidencystatus == ''+rolename+'fincardfront'){

if( $('#'+rolename+'fincardfrontpreview_'+getcounter).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#'+rolename+'fincardfrontpreview_'+getcounter).css("overflow","auto");
}

var fincardfrontemployer = document.getElementById(""+rolename+"fincardfrontemployer_"+getcounter).value;
var fincardfrontoccupation = document.getElementById(""+rolename+"fincardfrontoccupation_"+getcounter).value;
var fincardfrontdateissue = document.getElementById(""+rolename+"fincardfrontdateissue_"+getcounter).value;
var fincardfrontdateexpiry = document.getElementById(""+rolename+"fincardfrontdateexpiry_"+getcounter).value;
var wfincardfrontNumber = document.getElementById(""+rolename+"wfincardfrontNumber_"+getcounter).value;

localStorage.setItem(""+rolename+"fincardpopupfrontemployer_"+getcounter,fincardfrontemployer);
localStorage.setItem(""+rolename+"fincardpopupfrontoccupation_"+getcounter,fincardfrontoccupation);
localStorage.setItem(""+rolename+"fincardpopupfrontdateissue_"+getcounter,fincardfrontdateissue);
localStorage.setItem(""+rolename+"fincardpopupfrontdateexpiry_"+getcounter,fincardfrontdateexpiry);
localStorage.setItem(""+rolename+"wfincardpopupfrontNumber_"+getcounter,wfincardfrontNumber);

var getfincardfrontdateissue = formatingdata(fincardfrontdateissue);
var getfincardfrontdateexpiry = formatingdata(fincardfrontdateexpiry);

//var startdate = new Date(fincardfrontdateissue.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(fincardfrontdateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var startdate = $('#'+rolename+'fincardfrontdateissue_'+getcounter).datepicker('getDate');
var enddate = $('#'+rolename+'fincardfrontdateexpiry_'+getcounter).datepicker('getDate');

        if(fincardfrontemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN FRONT - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(fincardfrontoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN FRONT - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(fincardfrontdateissue != "" && getfincardfrontdateissue == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN FRONT - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(fincardfrontdateexpiry != "" && getfincardfrontdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN FRONT - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if((fincardfrontdateissue != '') && (fincardfrontdateissue.length < 10)){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if((fincardfrontdateexpiry != "") && (fincardfrontdateexpiry.length < 10)){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((fincardfrontdateissue != '') && (fincardfrontdateexpiry != '') && (startdate >= enddate)){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN FRONT - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(wfincardfrontNumber == ''){
            //alert("Fin Number cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN FRONT - Preview");
             $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}          
	else{
	   //alert("Saved Succesfully");
	   $('#'+rolename+'fincardfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN FRONT - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('body').css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == ''+rolename+'fincardback'){

if( $('#'+rolename+'fincardbackpreview_'+getcounter).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#'+rolename+'fincardbackpreview_'+getcounter).css("overflow","auto");
}

var fincardbackemployer = document.getElementById(""+rolename+"fincardbackemployer_"+getcounter).value;
var fincardbackoccupation = document.getElementById(""+rolename+"fincardbackoccupation_"+getcounter).value;
var fincardbackdateissue = document.getElementById(""+rolename+"fincardbackdateissue_"+getcounter).value;
var fincardbackdateexpiry = document.getElementById(""+rolename+"fincardbackdateexpiry_"+getcounter).value;
var wfincardbackNumber = document.getElementById(""+rolename+"wfincardbackNumber_"+getcounter).value;

//var startdate = new Date(fincardbackdateissue.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(fincardbackdateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var startdate = $('#'+rolename+'fincardbackdateissue_'+getcounter).datepicker('getDate');
var enddate = $('#'+rolename+'fincardbackdateexpiry_'+getcounter).datepicker('getDate');

localStorage.setItem(""+rolename+"fincardpopupbackemployer_"+getcounter,fincardbackemployer);
localStorage.setItem(""+rolename+"fincardpopupbackoccupation_"+getcounter,fincardbackoccupation);
localStorage.setItem(""+rolename+"fincardpopupbackdateissue_"+getcounter,fincardbackdateissue);
localStorage.setItem(""+rolename+"fincardpopupbackdateexpiry_"+getcounter,fincardbackdateexpiry);
localStorage.setItem(""+rolename+"wfincardpopupbackNumber_"+getcounter,wfincardbackNumber);

var getfincardbackdateissue = formatingdata(fincardbackdateissue);
var getfincardbackdateexpiry = formatingdata(fincardbackdateexpiry);

        if(fincardbackdateissue != "" && getfincardbackdateissue == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN back - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(fincardbackdateexpiry != "" && getfincardbackdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN back - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if((fincardbackdateissue != '') && (fincardbackdateissue.length < 10)){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if((fincardbackdateexpiry != "") && (fincardbackdateexpiry.length < 10)){
              $("#displaysuccessmsg .modal-title").html(""+getrolename+" - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((fincardbackdateissue != '') && (fincardbackdateexpiry != '') && (startdate >= enddate)){
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN back - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(wfincardbackNumber == ''){
            //alert("Fin Number cannot be empty");
             $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN back - Preview");
             $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}          
	else{
	   //alert("Saved Succesfully");
	   $('#'+rolename+'fincardbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html(""+getrolename+" FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('body').css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show"); 
	}

   }
}
</script>
