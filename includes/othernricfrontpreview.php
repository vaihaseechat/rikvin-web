<div class="modal fade"  data-keyboard="true"   id="othernricfrontpreview" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">NRIC  Front Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">					
					<div class="row mt-3">
						<div class="col-sm">
							<div class="form-group row">
								<div class="col-sm-12">
									Please check details below.
								</div>
								<div class="col-sm-12">
									<div class="passport-scan-copy" id="othernricfrontimage"><img src="" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group row">
						        <label class="col-sm-12 col-form-label required">NRIC Number</label>
							<div class="col-sm-12">
							<input name="otherpreviewfrontnricnumber" class="form-control" id="otherpreviewfrontnricnumber" value="" type="text">
							</div>
						</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12">
							<label class="radio-inline mr-3"><input id="otherpreviewnricfrontmgender" name="otherpreviewfrontnricgender" class="radio-red" type="radio" value="Male"> Male</label>
							<label class="radio-inline"><input id="otherpreviewnricfrontfgender" name="otherpreviewfrontnricgender" class="radio-red" checked type="radio" value="Female"> Female</label>
							</div>
						</div>
						</div>
						                                            
					</div>
                                        <div class="row">
                                                  <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label" for="dateofissue">Date of Birth</label>
								<div class="col-sm-12">
                                                                 <div class="input-group dateicon">
                                                                  <input name="otherpreviewfrontnricdoi" class="form-control" id="otherpreviewfrontnricdoi"  value="" type="text" >
                                                                </div></div>
							</div>
						  </div>  
		                                  <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Country of Birth</label>
							<div class="col-sm-12">
		                                        <select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="otherpreviewfrontnriccob" name="otherpreviewfrontnriccob"></select>
		                                        </div>
							</div>
						   </div>
                                        </div>
                                        <div class="row my-3" style="display:none;">
						<div class="col-sm">
							<h4 class="font-weight-otherld">Residencial Address</h4>
						</div>
					</div>				
					<div class="row" style="display:none;">
		                                  <div class="col-md-3">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Postcode</label>
								<div class="col-sm-12">	
                                                                <input class="form-control" id="otherpreviewfrontnricpostcode" name="otherpreviewfrontnricpostcode" placeholder="Postal Code" type="text">			                                
				                                </div>
							</div>
						   </div>

                                                    <div class="col-md-3">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Streetname</label>
								<div class="col-sm-12">
				                                <textarea name="otherpreviewfrontnricstreetname" id="otherpreviewfrontnricstreetname" class="form-control" rows="3" placeholder="Street Name"></textarea> 
				                                </div>
							</div>
						   </div>
                                                   <div class="col-md-3">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Floor</label>
								<div class="col-sm-12">
				                                <input class="form-control" id="otherpreviewfrontnricfloor" name="otherpreviewfrontnricfloor" placeholder="Floor" type="text">
				                                </div>
							</div>
						   </div>
                                                   <div class="col-md-3">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Unit</label>
								<div class="col-sm-12">
				                                <input class="form-control" id="otherpreviewfrontnricunit" name="otherpreviewfrontnricunit" placeholder="Unit" type="text">
				                                </div>
							</div>
						   </div>
                                        </div>
		        		<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
                                                                <input type="hidden" name="othernricfrontfilename" id="othernricfrontfilename" value="">   
								<button class="btn btn-next" onclick="savenricfront();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savenricfront(){

if( $('#othernricfrontpreview').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#othernricfrontpreview').css("overflow","auto");
}

var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

var othernricfrontfilename = document.getElementById("othernricfrontfilename").value;
var otherpreviewfrontnricnumber = document.getElementById("otherpreviewfrontnricnumber").value;
//var othergivenname = document.getElementById("othergivenName").value;
var otherpreviewfrontnricdoi = document.getElementById("otherpreviewfrontnricdoi").value;
var otherpreviewfrontnriccob = $('#otherpreviewfrontnriccob_chosen span').text();
var otherpreviewfrontnricpostcode = document.getElementById("otherpreviewfrontnricpostcode").value;
var otherpreviewfrontnricstreetname = document.getElementById("otherpreviewfrontnricstreetname").value;
var otherpreviewfrontnricfloor = document.getElementById("otherpreviewfrontnricfloor").value;
var otherpreviewfrontnricunit = document.getElementById("otherpreviewfrontnricunit").value;
var otherpreviewfrontnricgender = $("input[name='otherpreviewfrontnricgender']:checked").val();

var getdirectornricdob = formatingdata(otherpreviewfrontnricdoi);

var cur_date = new Date();
//var dob =  new Date(otherpreviewfrontnricdoi.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
var dob = $('#otherpreviewfrontnricdoi').datepicker('getDate');

        if(otherpreviewfrontnricnumber == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewfrontnricdoi == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(otherpreviewfrontnriccob == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}else if(getdirectornricdob == true){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
           $("#displaysuccessmsg .modal-body").html("Invalid Date of Birth");
           $("#displaysuccessmsg").modal("show");

	}	
        else if(otherpreviewfrontnricdoi.length < 10){
             $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }

         else if(dob >= cur_date){
		 $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
		 $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
		 $("#displaysuccessmsg").modal("show"); 
	}
	
        else{
	   //alert("Saved Succesfully");
	    $('#othernricfrontpreview').modal('hide');
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#othernricmanual").prop('disabled', true);
            $("#displaysuccessmsg").modal("show");
	}


localStorage.setItem("otherpreviewfrontnricnumber",otherpreviewfrontnricnumber);
localStorage.setItem("otherpreviewfrontnricdoi",otherpreviewfrontnricdoi);
localStorage.setItem("otherpreviewfrontnriccob",otherpreviewfrontnriccob);
localStorage.setItem("otherpreviewfrontnricpostcode",otherpreviewfrontnricpostcode);
localStorage.setItem("otherpreviewfrontnricstreetname",otherpreviewfrontnricstreetname);
localStorage.setItem("otherpreviewfrontnricfloor",otherpreviewfrontnricfloor);
localStorage.setItem("otherpreviewfrontnricunit",otherpreviewfrontnricunit);
localStorage.setItem("otherpreviewfrontnricgender",otherpreviewfrontnricgender);


}

</script>
