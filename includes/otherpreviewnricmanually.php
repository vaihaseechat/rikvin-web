<div class="modal fade"  data-keyboard="true"   id="otherpreviewnricmanually" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">NRIC  Information - Manual</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">					
					
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group row">
						        <label class="col-sm-12 col-form-label required">NRIC Number</label>
							<div class="col-sm-12">
							<input name="othermpreviewnricnumber" class="form-control" id="othermpreviewnricnumber" value="" type="text">
							</div>
						</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12">
							<label class="radio-inline mr-3"><input id="othermpreviewnricmgender" name="othermpreviewnricgender" class="radio-red" type="radio" value="Male"> Male</label>
							<label class="radio-inline"><input id="othermpreviewnricfgender" name="othermpreviewnricgender" class="radio-red" checked type="radio" value="Female"> Female</label>
							</div>
						</div>
						</div>					                                           
					</div>
                                        <div class="row">
		                                  <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Country of Birth</label>
							<div class="col-sm-12">
		                                        <select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othermpreviewnriccob" name="othermpreviewnriccob"></select>
		                                        </div>
							</div>
						   </div>
                                                   <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label" for="dateofissue">Date of Birth</label>
								<div class="col-sm-12">
                                                                 <div class="input-group dateicon">
                                                                  <input name="othermnricdoi" class="form-control" id="othermnricdoi"  value="" type="text" placeholder="DD-MM-YYYY" maxlength="10">
                                                                </div></div>
							</div>
						</div>   
                                        </div>
                                        <div class="row my-3">
						<div class="col-sm">
							<h4 class="font-weight-otherld">Residencial Address</h4>
						</div>
					</div>				
					<div class="row">
		                                  <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Postcode</label>
								<div class="col-sm-12">	
                                                                <input class="form-control" id="othermpreviewnricpostcode" name="othermpreviewnricpostcode" placeholder="Postal Code" type="text">			                                
				                                </div>
							</div>
						   </div>

                                                    <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Streetname</label>
								<div class="col-sm-12">
				                                <textarea name="othermpreviewnricstreetname" id="othermpreviewnricstreetname" class="form-control" rows="3" placeholder="Street Name"></textarea> 
				                                </div>
							</div>
						   </div>
                                         </div>
                                         <div class="row">
                                                   <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Floor</label>
								<div class="col-sm-12">
				                                <input class="form-control" id="othermpreviewnricfloor" name="othermpreviewnricfloor" placeholder="Floor" type="text">
				                                </div>
							</div>
						   </div>
                                                   <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Unit</label>
								<div class="col-sm-12">
				                                <input class="form-control" id="othermpreviewnricunit" name="othermpreviewnricunit" placeholder="Unit" type="text">
				                                </div>
							</div>
						   </div>
                                        </div>
		        		<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
								<button class="btn btn-next" onclick="savenricmanually();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savenricmanually(){

if( $('#otherpreviewnricmanually').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#otherpreviewnricmanually').css("overflow","auto");
}

var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

var otherpreviewnricnumber = document.getElementById("othermpreviewnricnumber").value;
//var othergivenname = document.getElementById("othergivenName").value;
var otherpreviewnricdoi = document.getElementById("othermnricdoi").value;
var otherpreviewnriccob = $('#othermpreviewnriccob_chosen span').text();
var otherpreviewnricpostcode = document.getElementById("othermpreviewnricpostcode").value;
var otherpreviewnricstreetname = document.getElementById("othermpreviewnricstreetname").value;
var otherpreviewnricfloor = document.getElementById("othermpreviewnricfloor").value;
var otherpreviewnricunit = document.getElementById("othermpreviewnricunit").value;
var otherpreviewnricgender = $("input[name='othermpreviewnricgender']:checked").val();

var getnonresidentdob = formatingdata(otherpreviewnricdoi);

var cur_date = new Date();
//var dob =  new Date(otherpreviewnricdoi.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
var dob = $('#othermnricdoi').datepicker('getDate');

        if(otherpreviewnricnumber == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewnricdoi == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(otherpreviewnriccob == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewnricpostcode == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");

	} else if(isNaN(otherpreviewnricpostcode)){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Invalid Postcode");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewnricstreetname == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewnricfloor == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}else if(isNaN(otherpreviewnricfloor)){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Invalid Floor");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewnricunit == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}else if(getnonresidentdob == true){
             $("#displaysuccessmsg .modal-title").html("NRIC  Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(otherpreviewnricdoi.length < 10){
              $("#displaysuccessmsg .modal-title").html("NRIC  Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("NRIC  Information");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else{
	   //alert("Saved Succesfully");
            localStorage.setItem(MANUAL_OTHER_CITIZEN_NRIC, true);
	    $('#otherpreviewnricmanually').modal('hide');
            $("#displaysuccessmsg .modal-title").html("NRIC  Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#displaysuccessmsg").modal("show");
	}


localStorage.setItem("otherpreviewnricnumber",otherpreviewnricnumber);
localStorage.setItem("otherpreviewnricdoi",otherpreviewnricdoi);
localStorage.setItem("otherpreviewnriccob",otherpreviewnriccob);
localStorage.setItem("otherpreviewnricpostcode",otherpreviewnricpostcode);
localStorage.setItem("otherpreviewnricstreetname",otherpreviewnricstreetname);
localStorage.setItem("otherpreviewnricfloor",otherpreviewnricfloor);
localStorage.setItem("otherpreviewnricunit",otherpreviewnricunit);
localStorage.setItem("otherpreviewnricgender",otherpreviewnricgender);



}

</script>
