<script src='https://www.winimy.ai/rikvinnew/js/pdfcore.js'></script>
<script>

function checkPdfParse(myImages,getcounter,callbackfunction)
{

var myImage = myImages[0];
var fileReader = new FileReader();
var scale = 1.0;
var ext=myImages[0].name.split('.').pop().toLowerCase();
if(ext=="pdf")
{
try{	
console.log(" Getting image data from pdf");

fileReader.onload = function() {

	//Step 4:turn array buffer into typed array
	var typedarray = new Uint8Array(this.result);

	//Step 5:PDFJS should be able to read this
	pdfjsLib.getDocument(typedarray).then(function(pdf) {
		 pdf.getPage(1).then(function(page) {
	 
			var viewport = page.getViewport(scale);
			var canvas = document.createElement('canvas') , ctx = canvas.getContext('2d');
			var renderContext = { canvasContext: ctx, viewport: viewport };
			canvas.height = viewport.height;
			canvas.width = viewport.width;
			page.render(renderContext).then(function() {									
				var img = new Image();
				img.src = canvas.toDataURL();
				callbackfunction(img,getcounter);
					
			});
		});
	});


};
fileReader.readAsArrayBuffer(myImage);
}catch (err) {

	console.log(err);
	callbackfunction(myImages,getcounter);
}
}
else{
	//alert(callbackfunction);
	callbackfunction(myImages,getcounter);
}
	
}





function directornonuploadpassportfile(getdata,getcounter){

$("#duploadpassportfileloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
var filename = file.name;
var form = new FormData();
form.append('image', file);
//alert(filename);

        $.ajax({
		    url: "https://www.winimy.ai/rikvinapi/ocr/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error;
 
                         if(Errorstatus == 'Poor Image Quality') {                           
                           
                           alert(Errorstatus);
                           $("#bdirectornonresidentpreviewpassport_"+getcounter).css("display", "none");
                           $("#duploadpassportfileloadingImage_"+getcounter).css("display", "none");
                           $("#directornonresidentlabel_"+getcounter).text();

                         } else{
                            //alert("fdfdfdf");
                            $("#bdirectornonresidentpreviewpassport_"+getcounter).css("display", "block");
                            $("#duploadpassportfileloadingImage_"+getcounter).css("display", "none");
                            $("#directornonresidentlabel_"+getcounter).text(filename);


                            var fieldHTML ='<div class="modal fade" id="directornonresidentpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fieldHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fieldHTML += '<div class="modal-content row">';
				fieldHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - Non Resident - Director #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fieldHTML += '<div class="col-lg-12 background-grey-2">';
				fieldHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="directornonresidentpassportimage_'+getcounter+'"><img src="images/passport.jpg" alt="" /></div></div></div></div></div>';
				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Surname</label><div class="col-sm-12"><input type="text" name="directornonresidentsurname[]" class="form-control" id="directornonresidentsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Given name</label><div class="col-sm-12"><input type="text" name="directornonresidentgivenName[]" class="form-control" id="directornonresidentgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Passport Number</label><div class="col-sm-12"><input type="text" name="directornonresidentpassportNumber[]" class="form-control" id="directornonresidentpassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="directornonresidentnationality[]" id="directornonresidentnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directornonresidentcountry_'+getcounter+'" name="directornonresidentcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="directornonresidentmgender_'+getcounter+'" name="directornonresidentgender[]" class="radio-red">Male</label><label class="radio-inline"><input type="radio" name="directornonresidentgender[]" id="directornonresidentfgender_'+getcounter+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directornonresidentdatetimepicker_'+counter+'" class="form-control" name="directornonresidentdatetimepicker[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directornonresidentdateexpiry_'+counter+'" class="form-control" name="directornonresidentdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 fieldHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directornonresidentdob_'+getcounter+'" class="form-control" name="directornonresidentdob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directornonresidentplaceofBirth_'+getcounter+'" name="directornonresidentplaceofBirth[]"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'directornonresident\');">Save</a></div></div></div>';
				fieldHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fieldHTML);

                                 //Passport Preview - Non resident

				$('#directornonresidentnationality_'+getcounter).html(countriesOption);
				$('#directornonresidentnationality_'+getcounter).chosen(); 

				$('#directornonresidentcountry_'+getcounter).html(countriesOption);
				$('#directornonresidentcountry_'+getcounter).chosen(); 

				$('#directornonresidentplaceofBirth_'+getcounter).html(countriesOption);
				$('#directornonresidentplaceofBirth_'+getcounter).chosen(); 

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = ''; 
                                 var PlaceofBirth = '';                 
		                 

                                 // Displaying values from OCR - API

                                 $('#directornonresidentgivenName_'+getcounter).val(gname);
                                 $('#directornonresidentsurname_'+getcounter).val(Surname);
                                 $('#directornonresidentpassportNumber_'+getcounter).val(passportno);
                                 $('#directornonresidentdatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#directornonresidentdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#directornonresidentdob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'male'){
                                  $('#directornonresidentmgender_'+getcounter).prop('checked', true);
                                 }
                                 else if(sex == 'female'){
                                  $('#directornonresidentfgender_'+getcounter).prop('checked', true);
                                 }
                                 $('#directornonresidentnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#directornonresidentcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 $('#directornonresidentplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth); 

                         

                         }     

                    }

        }); 
 
}

//NRIC front

function directornricfront(getdata,getcounter){

$("#duploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
var filename = file.name;
var form = new FormData();
form.append('image', file);

	$.ajax({
	    url: "https://www.winimy.ai/rikvinapi/ocr/nric-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    alert(Errorstatus);
                    $("#bdirectornricfrontpreview_"+getcounter).css("display", "none");
                    $("#duploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#directornricfrontlabel_"+getcounter).text();

		 } else{

                   $("#bdirectornricfrontpreview_"+getcounter).css("display", "block");
                   $("#duploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#directornricfrontlabel_"+getcounter).text(filename);  

                   var nricfrontfield = '<div class="modal fade" id="directornricfrontpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfrontfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfrontfield += '<div class="modal-content row">';
                     nricfrontfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Front Information - Director#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfrontfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="directornricfrontimage_'+getcounter+'"><img src="images/passport.jpg" alt="" /></div></div></div></div></div>';
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">NRIC Number</label><div class="col-sm-12"><input name="directorpreviewfrontnricnumber[]" class="form-control" id="directorpreviewfrontnricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="directorpreviewnricfrontmgender_'+getcounter+'" name="directorpreviewnricfrontgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="directorpreviewnricfrontfgender_'+getcounter+'" name="directorpreviewnricfrontgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfrontfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="directorpreviewnricfrontdob_'+getcounter+'" class="form-control" name="directorpreviewnricfrontdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Country of Birth</label><div class="col-sm-12"><select name="directorpreviewnricfrontnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directorpreviewnricfrontnationality_'+getcounter+'"></select></div></div></div></div>';

                     nricfrontfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directorpreviewnricfrontpostcode_'+getcounter+'" name="directorpreviewnricfrontpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="directorpreviewnricfrontstreetname[]" id="directorpreviewnricfrontstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directorpreviewnricfrontfloor_'+getcounter+'" name="directorpreviewnricfrontfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directorpreviewnricfrontunit_'+getcounter+'" name="directorpreviewnricfrontunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfrontfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'directornricfront\');">Save</a></div></div></div>';

                     nricfrontfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfrontfield);

                     $('#directorpreviewnricfrontnationality_'+counter).html(countriesOption);
                     $('#directorpreviewnricfrontnationality_'+counter).chosen();   
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         /*if(sex == 'male'){
                          $('#directorpreviewnricfrontmgender_'+getcounter).prop('checked', true);
                         }
                         else if(sex == 'female'){
                          $('#directorpreviewnricfrontfgender_'+getcounter).prop('checked', true);
                         }*/

                         $('#directorpreviewfrontnricnumber_'+getcounter).val(nricno);
                         $('#directorpreviewnricfrontnationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#directorpreviewnricfrontdob_'+getcounter).val(dobs);
                      

		 }
	    }
	}); 
}

//NRIC back

function directornricback(getdata,getcounter){

$("#duploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
var filename = file.name;
var form = new FormData();
form.append('image', file);

   $.ajax({
	    url: "https://www.winimy.ai/rikvinapi/ocr/nric-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    alert(Errorstatus);
                    $("#bdirectornricbackpreview_"+getcounter).css("display", "none");
                    $("#duploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#directornricbacklabel_"+getcounter).text();

		 } else{

                   $("#bdirectornricbackpreview_"+getcounter).css("display", "block"); 
                   $("#duploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#directornricbacklabel_"+getcounter).text(filename);

                   var nricbackfield = '<div class="modal fade" id="directornricbackpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricbackfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricbackfield += '<div class="modal-content row">';
                     nricbackfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Back Information - Director#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricbackfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="directornricbackimage_'+getcounter+'"><img src="images/passport.jpg" alt="" /></div></div></div></div></div>';
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">NRIC Number</label><div class="col-sm-12"><input name="directorpreviewbacknricnumber[]" class="form-control" id="directorpreviewbacknricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="directorpreviewnricbackmgender_'+getcounter+'" name="directorpreviewnricbackgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="directorpreviewnricbackfgender_'+getcounter+'" name="directorpreviewnricbackgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricbackfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="directorpreviewnricbackdob_'+getcounter+'" class="form-control" name="directorpreviewnricbackdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Country of Birth</label><div class="col-sm-12"><select name="directorpreviewnricbacknationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directorpreviewnricbacknationality_'+getcounter+'"></select></div></div></div></div>';

                     nricbackfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directorpreviewnricbackpostcode_'+getcounter+'" name="directorpreviewnricbackpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="directorpreviewnricbackstreetname[]" id="directorpreviewnricbackstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directorpreviewnricbackfloor_'+getcounter+'" name="directorpreviewnricbackfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="directorpreviewnricbackunit_'+getcounter+'" name="directorpreviewnricbackunit[]" placeholder="Unit" type="text"></div></div>';

                     nricbackfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'directornricback\');">Save</a></div></div></div>';

                     nricbackfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricbackfield);

                     $('#directorpreviewnricbacknationality_'+counter).html(countriesOption);
                     $('#directorpreviewnricbacknationality_'+counter).chosen();   
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         /*if(sex == 'male'){
                          $('#directorpreviewnricbackmgender_'+getcounter).prop('checked', true);
                         }
                         else if(sex == 'female'){
                          $('#directorpreviewnricbackfgender_'+getcounter).prop('checked', true);
                         }*/

                         $('#directorpreviewbacknricnumber_'+getcounter).val(nricno);
                         $('#directorpreviewnricbacknationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#directorpreviewnricbackdob_'+getcounter).val(dobs);
 
		 }
	    }
   }); 

}

//FIn Passport

function directorfinuploadpassportfile(getdata,getcounter){

$("#duploadPassportloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
var filename = file.name;
var form = new FormData();
form.append('image', file);

   $.ajax({
	    url: "https://www.winimy.ai/rikvinapi/ocr/passport/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    alert(Errorstatus);
                    $("#bdirectorfincardpreviewpassport_"+getcounter).css("display", "none");
                    $("#duploadPassportloadingImage_"+getcounter).css("display", "none"); 
                    $("#directorfinpassportlabel_"+getcounter).text();

		 } else{

                   $("#bdirectorfincardpreviewpassport_"+getcounter).css("display", "block"); 
                   $("#duploadPassportloadingImage_"+getcounter).css("display", "none");
                   $("#directorfinpassportlabel_"+getcounter).text(filename);


                    var fincardHTML ='<div class="modal fade" id="directorfincardpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fincardHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fincardHTML += '<div class="modal-content row">';
				fincardHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - WorkPass - Director #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fincardHTML += '<div class="col-lg-12 background-grey-2">';
				fincardHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="directorfincardpassportimage_'+getcounter+'"><img src="images/passport.jpg" alt="" /></div></div></div></div></div>';
				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Surname</label><div class="col-sm-12"><input type="text" name="directorfincardsurname[]" class="form-control" id="directorfincardsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Given name</label><div class="col-sm-12"><input type="text" name="directorfincardgivenName[]" class="form-control" id="directorfincardgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Passport Number</label><div class="col-sm-12"><input type="text" name="directorfincardpassportNumber[]" class="form-control" id="directorfincardpassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="directorfincardpnationality[]" id="directorfincardpnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directorfincardpcountry_'+getcounter+'" name="directorfincardpcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="directorfincardmgender_'+getcounter+'" name="directorfincardgender[]" class="radio-red">Male</label><label class="radio-inline"><input type="radio" name="directorfincardgender[]" id="directorfincardfgender_'+getcounter+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directorfincarddatetimepicker_'+counter+'" class="form-control" name="directorfincarddatetimepicker[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directorfincarddateexpiry_'+counter+'" class="form-control" name="directorfincarddateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 fincardHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directorfincarddob_'+getcounter+'" class="form-control" name="directorfincarddob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="directorfincardplaceofBirth_'+getcounter+'" name="directorfincardplaceofBirth[]"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'directorfinpassport\');">Save</a></div></div></div>';
				fincardHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fincardHTML);

                                 //Passport Preview - Work Pass

				$('#directorfincardpnationality_'+getcounter).html(countriesOption);
				$('#directorfincardpnationality_'+getcounter).chosen(); 

				$('#directorfincardpcountry_'+getcounter).html(countriesOption);
				$('#directorfincardpcountry_'+getcounter).chosen(); 

				$('#directorfincardplaceofBirth_'+getcounter).html(countriesOption);
				$('#directorfincardplaceofBirth_'+getcounter).chosen(); 

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = ''; 
                                 var PlaceofBirth = '';                 
		                 

                                 // Displaying values from OCR - API

                                 $('#directorfincardgivenName_'+getcounter).val(gname);
                                 $('#directorfincardsurname_'+getcounter).val(Surname);
                                 $('#directorfincardpassportNumber_'+getcounter).val(passportno);
                                 $('#directorfincarddatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#directorfincarddateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#directorfincarddob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'male'){
                                  $('#directorfincardmgender_'+getcounter).prop('checked', true);
                                 }
                                 else if(sex == 'female'){
                                  $('#directorfincardfgender_'+getcounter).prop('checked', true);
                                 }
                                 $('#directorfincardpnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#directorfincardpcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 $('#directorfincardplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth);       
 
		 }
	    }
   }); 

}

// Fin front

function directorfincardfront(getdata,getcounter){

$("#duploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
var filename = file.name;
var form = new FormData();
form.append('image', file);

   $.ajax({
	    url: "https://www.winimy.ai/rikvinapi/ocr/ep-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    alert(Errorstatus);
                    $("#bdirectorfincardfrontpreview_"+getcounter).css("display", "none"); 
                    $("#duploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#directorfincardfrontlabel_"+getcounter).text();

		 } else{

                   $("#bdirectorfincardfrontpreview_"+getcounter).css("display", "block"); 
                   $("#duploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#directorfincardfrontlabel_"+getcounter).text(filename);


                var fincardfrontHTML ='<div class="modal fade" id="directorfincardfrontpreview_'+getcounter+'" tabindex="-1" role="dialog">';
		fincardfrontHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincardfrontHTML += '<div class="modal-content row">';
		fincardfrontHTML += '<div class="modal-header"><h5 class="modal-title" id="directorfincardfrontpreview">Director #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		 fincardfrontHTML += '<div class="col-lg-12 background-grey-2">';

                 fincardfrontHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="directorfincardfrontpassportimage_'+getcounter+'"><img src="images/passport.jpg" alt="" /></div></div></div></div></div>';
                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="directorfincardfrontemployer[]" class="form-control" id="directorfincardfrontemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="directorfincardfrontoccupation[]" class="form-control" id="directorfincardfrontoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

                  fincardfrontHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directorfincardfrontdateissue_'+getcounter+'" class="form-control" name="directorfincardfrontdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directorfincardfrontdateexpiry_'+getcounter+'" class="form-control" name="directorfincardfrontdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Fincard Number</label><div class="col-sm-6"><input type="text" name="directorwfincardfrontNumber[]" class="form-control" id="directorwfincardfrontNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

                 fincardfrontHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'directorfincardfront\');">Save</a></div></div></div>';
		fincardfrontHTML += '</div></div></div></div>';   
                   
                 $('body').find('#additionalRole:last').after(fincardfrontHTML);


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#directorfincardfrontemployer_'+getcounter).val(Employer);
                         $('#directorfincardfrontoccupation_'+getcounter).val(Occupation);
                         $('#directorfincardfrontdateissue_'+getcounter).val(DateofIssue);
                         $('#directorfincardfrontdateexpiry_'+getcounter).val(DateofExpiry);
                         $('#directorwfincardfrontNumber_'+getcounter).val(FIN); 
 
		 }
	    }
   }); 

}

// Fin back

function directorfincardback(getdata,getcounter){

$("#duploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
var filename = file.name;
var form = new FormData();
form.append('image', file);

   $.ajax({
	    url: "https://www.winimy.ai/rikvinapi/ocr/ep-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    alert(Errorstatus);
                    $("#bdirectorfincardbackpreview_"+getcounter).css("display", "none"); 
                    $("#duploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#directorfincardbacklabel_"+getcounter).text();

		 } else{

                   $("#bdirectorfincardbackpreview_"+getcounter).css("display", "block");  
                   $("#duploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#directorfincardbacklabel_"+getcounter).text(filename);


                   var fincardbackHTML ='<div class="modal fade" id="directorfincardbackpreview_'+getcounter+'" tabindex="-1" role="dialog">';
			fincardbackHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
			fincardbackHTML += '<div class="modal-content row">';
			fincardbackHTML += '<div class="modal-header"><h5 class="modal-title" id="directorfincardbackpreview">Director #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
			 fincardbackHTML += '<div class="col-lg-12 background-grey-2">';

                         fincardbackHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="directorfincardbackpassportimage_'+getcounter+'"><img src="images/passport.jpg" alt="" /></div></div></div></div></div>';
		     
		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="directorfincardbackemployer[]" class="form-control" id="directorfincardbackemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="directorfincardbackoccupation[]" class="form-control" id="directorfincardbackoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

		          fincardbackHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

		          fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directorfincardbackdateissue_'+getcounter+'" class="form-control" name="directorfincardbackdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="directorfincardbackdateexpiry_'+getcounter+'" class="form-control" name="directorfincardbackdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Fincard Number</label><div class="col-sm-6"><input type="text" name="directorwfincardbackNumber[]" class="form-control" id="directorwfincardbackNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

		         fincardbackHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'directorfincardback\');">Save</a></div></div></div>';
			fincardbackHTML += '</div></div></div></div>';   
		           
		         $('body').find('#additionalRole:last').after(fincardbackHTML);


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#directorfincardbackemployer_'+getcounter).val(Employer);
                         $('#directorfincardbackoccupation_'+getcounter).val(Occupation);
                         $('#directorfincardbackdateissue_'+getcounter).val(DateofIssue);
                         $('#directorfincardbackdateexpiry_'+getcounter).val(DateofExpiry);
                         $('#directorwfincardbackNumber_'+getcounter).val(FIN); 
 
		 }
	    }
   }); 

}



</script>
