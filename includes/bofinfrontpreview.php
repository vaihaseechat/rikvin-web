<script>
function bofinfrontpopup(getcounter,getboname){

if( $('#bofincardfrontpreview_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden"); 
    $('#bofincardfrontpreview_'+getcounter+"_"+getboname).css("overflow","auto");
}

var bofincardfrontemployer = document.getElementById("bofincardfrontemployer_"+getcounter+"_"+getboname).value;
var bofincardfrontoccupation = document.getElementById("bofincardfrontoccupation_"+getcounter+"_"+getboname).value;
var bofincardfrontdateissue = document.getElementById("bofincardfrontdateissue_"+getcounter+"_"+getboname).value;
var bofincardfrontdateexpiry = document.getElementById("bofincardfrontdateexpiry_"+getcounter+"_"+getboname).value;
var bowfincardfrontNumber = document.getElementById("bowfincardfrontNumber_"+getcounter+"_"+getboname).value;

localStorage.setItem("bofincardfrontemployer_"+getcounter+"_"+getboname,bofincardfrontemployer);
localStorage.setItem("bofincardfrontoccupation_"+getcounter+"_"+getboname,bofincardfrontoccupation);
localStorage.setItem("bofincardfrontdateissue_"+getcounter+"_"+getboname,bofincardfrontdateissue);
localStorage.setItem("bofincardfrontdateexpiry_"+getcounter+"_"+getboname,bofincardfrontdateexpiry);
localStorage.setItem("bowfincardfrontNumber_"+getcounter+"_"+getboname,bowfincardfrontNumber);

var getfincardfrontdateissue = formatingdata(bofincardfrontdateissue);
var getfincardfrontdateexpiry = formatingdata(bofincardfrontdateexpiry);

//var startdate = new Date(bofincardfrontdateissue.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(bofincardfrontdateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var startdate = $("bofincardfrontdateissue_"+getcounter+"_"+getboname).datepicker('getDate');
var enddate = $("bofincardfrontdateexpiry_"+getcounter+"_"+getboname).datepicker('getDate');

        if(bofincardfrontemployer == ''){
           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(bofincardfrontoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	/*else if(bofincardfrontdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(bofincardfrontdateexpiry == ''){

           //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}*/
        else if(bofincardfrontdateissue != "" && getfincardfrontdateissue == true){
             $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(bofincardfrontdateexpiry != "" && getfincardfrontdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((bofincardfrontdateissue != "") && (bofincardfrontdateissue.length < 10)){
             $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((bofincardfrontdateexpiry != "") && (bofincardfrontdateexpiry.length < 10)){
             $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if((bofincardfrontdateissue != "") && (bofincardfrontdateexpiry != "") && (startdate >= enddate)){
             $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        }
        
        else if(bowfincardfrontNumber == ''){

           // alert("Fin Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#bofincardfrontpreview_'+getcounter+"_"+getboname).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Work Pass Front Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show"); 
        }


}

</script>

