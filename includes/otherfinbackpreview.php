<div class="modal fade"  data-keyboard="true"   id="otherfinbackpreview" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">WorkPass Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">
					<div class="row mt-3">
						<div class="col-sm">
							<div class="form-group row">
								<div class="col-sm-12">
									Please check details below.
								</div>
								<div class="col-sm-12">
									<div class="passport-scan-copy" id="otherfinbackimage"><img src="" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6" style="display:none;">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="employer">Employer</label>
								<div class="col-sm-12"><input name="otherfinbackemployer" class="form-control" id="otherfinbackemployer"  value="" type="text"></div>
							</div>
						</div>
						<div class="col-md-6" style="display:none;">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="givenName">Occupation</label>
								<div class="col-sm-12"><input name="otherfinbackoccupation" class="form-control" id="otherfinbackoccupation"  value="" type="text"></div>
							</div>
						</div>                                                
                              
					</div>					
					<div class="row">
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label" for="dateofissue">Date of Issue</label>
								<div class="col-sm-12">
                                                                 <div class="input-group dateicon">
                                                                  <input name="otherfinbackdoi" class="form-control" id="otherfinbackdoi"  value="" type="text" placeholder="DD-MM-YYYY" maxlength="10">
                                                                </div></div>
							</div>
						</div> 
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label" for="dateofissue">Date of Expiry</label>
								<div class="col-sm-12">
                                                                 <div class="input-group dateicon">
                                                                  <input name="otherfinbackdoe" class="form-control" id="otherfinbackdoe"  value="" type="text" placeholder="DD-MM-YYYY" maxlength="10">
                                                                </div></div>
							</div>
						</div>
                                          </div>
					  <div class="row">
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="finnumber">FIN Number</label>
								<div class="col-sm-12">
									<div class="input-group">
										<input id="otherfinbacknumber" class="form-control" name="otherfinbacknumber" value="" type="text">
										
									</div>
								</div>								
							</div>
						</div>
                                                
						
					</div>
					
					
					
					<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
                                                                <input type="hidden" name="otherfinbackfilename" id="otherfinbackfilename" value="">   
								<button class="btn btn-next" onclick="savefinback();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savefinback(){

if( $('#otherfinbackpreview').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#otherfinbackpreview').css("overflow","auto");
}

var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

var otherfinbackfilename = document.getElementById("otherfinbackfilename").value;
var otherfinbackemployer = document.getElementById("otherfinbackemployer").value;
var otherfinbackoccupation = document.getElementById("otherfinbackoccupation").value;
var otherwfinbackdoi = document.getElementById("otherfinbackdoi").value;
var otherwfinbackdoe = document.getElementById("otherfinbackdoe").value;
var otherfinbacknumber = document.getElementById("otherfinbacknumber").value;

var getfincardbackdateissue = formatingdata(otherwfinbackdoi);
var getfincardbackdateexpiry = formatingdata(otherwfinbackdoe);

//var startdate = new Date(otherwfinbackdoi.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(otherwfinbackdoe.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var startdate = $('#otherfinbackdoi').datepicker('getDate');
var enddate = $('#otherfinbackdoe').datepicker('getDate');

        /*if(otherfinbackemployer == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(otherfinbackoccupation == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(otherwfinbackdoi == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(otherwfinbackdoe == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}*/
        if(otherwfinbackdoi != "" && getfincardbackdateissue == true){
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(otherwfinbackdoe != "" && getfincardbackdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((otherwfinbackdoi != '') && (otherwfinbackdoi.length < 10)){
              $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if((otherwfinbackdoe != '') && (otherwfinbackdoe.length < 10)){
              $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((otherwfinbackdoi != '') && (otherwfinbackdoe != '') && (startdate >= enddate)){
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(otherfinbacknumber == ''){
             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("FIN Number cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else{
	   //alert("Saved Succesfully");
	    $('#otherfinbackpreview').modal('hide');
            $("#displaysuccessmsg .modal-title").html("WorkPass Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#displaysuccessmsg").modal("show");
	}

localStorage.setItem("otherfinbackemployer",otherfinbackemployer);
localStorage.setItem("otherfinbackoccupation",otherfinbackoccupation);
localStorage.setItem("otherwfinbackdoi",otherwfinbackdoi);
localStorage.setItem("otherwfinbackdoe",otherwfinbackdoe);
localStorage.setItem("otherfinbacknumber",otherfinbacknumber);



}

</script>

