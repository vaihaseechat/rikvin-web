<script>
function corporatestoredata(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);


if(getresidencystatus == 'corporatepassportmanually'){


	var corporatenonpassportno = document.getElementById("corporatenonpassportNumber_"+getcounter).value;
	var corporatenonnationality = $('#corporatenonnationality_'+getcounter+'_chosen span').text();
	var corporatenoncountry = $('#corporatenoncountry_'+getcounter+'_chosen span').text();
	var corporatenongender = $("input[name='corporatenongender[]']:checked").val();
	var corporatenondatetimepicker = document.getElementById("corporatenondateissue_"+getcounter).value;
	var corporatenondateexpiry = document.getElementById("corporatenondateexpiry_"+getcounter).value;
	var corporatenonplaceofBirth = $('#corporatenondateplace_'+getcounter+'_chosen span').text();
        var corporatenondateBirth = document.getElementById("corporatenondatebirth_"+getcounter).value;

	localStorage.setItem("corporatenonmanuallypassportno_"+getcounter,corporatenonpassportno);
	localStorage.setItem("corporatenonmanuallynationality_"+getcounter,corporatenonnationality);
	localStorage.setItem("corporatenonmanuallycountry_"+getcounter,corporatenoncountry);
	localStorage.setItem("corporatenonmanuallygender_"+getcounter,corporatenongender);
	localStorage.setItem("corporatenonmanuallydatetimepicker_"+getcounter,corporatenondatetimepicker);
	localStorage.setItem("corporatenonmanuallydateexpiry_"+getcounter,corporatenondateexpiry);
	localStorage.setItem("corporatenonmanuallyplaceofBirth_"+getcounter,corporatenonplaceofBirth);
        localStorage.setItem("corporatenonmanuallydateBirth_"+getcounter,corporatenondateBirth);


        if(corporatenonpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show");  

	}
	else if(corporatenonnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
        else if(corporatenoncountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenongender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenondatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenondateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show"); 

	}         
        else if(corporatenondateBirth == ''){

              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");     
	} 
        else if(corporatenonplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty");  
              $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
              $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show"); 
	}   
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_CORPORATE_NONRESIDENT_PASSPORT, true); 
           showPassportEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, false);
	   $('#corporatenonresidentpreviewpassportmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate Passport  - Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

	
}

if(getresidencystatus == 'corporatenricmanually'){

var corporatenonresidentnricnumber = document.getElementById("corporatenonresidentnricnumber_"+getcounter).value;
var corporatenonresidentgender = $("input[name='corporatenonresidentgender[]']:checked").val();
var corporatenonresidentdob = document.getElementById("corporatenonresidentdob_"+getcounter).value;
var corporatenricnationality = $('#corporatenricnationality_'+getcounter+'_chosen span').text();
var corporatenricpostcode = document.getElementById("corporatenricpostcode_"+getcounter).value;
var corporatenricstreetname = document.getElementById("corporatenricstreetname_"+getcounter).value;
var corporatenricfloor = document.getElementById("corporatenricfloor_"+getcounter).value;
var corporatenricunit = document.getElementById("corporatenricunit_"+getcounter).value;

localStorage.setItem("corporatepreviewmanuallynricnumber_"+getcounter,corporatenonresidentnricnumber);
localStorage.setItem("corporatepreviewmanuallynricgender_"+getcounter,corporatenonresidentgender);
localStorage.setItem("corporatepreviewmanuallynricdob_"+getcounter,corporatenonresidentdob);
localStorage.setItem("corporatepreviewmanuallynricnationality_"+getcounter,corporatenricnationality);
localStorage.setItem("corporatepreviewmanuallynricpostcode_"+getcounter,corporatenricpostcode);
localStorage.setItem("corporatepreviewmanuallynricstreetname_"+getcounter,corporatenricstreetname);
localStorage.setItem("corporatepreviewmanuallynricfloor_"+getcounter,corporatenricfloor);
localStorage.setItem("corporatepreviewnrmanuallyicunit_"+getcounter,corporatenricunit);

//alert(corporatenonresidentgender);

        if(corporatenonresidentnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(corporatenonresidentgender == "undefined"){
   
           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatenonresidentdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatenricnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
        else if(corporatenricpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenricstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
             $("#displaysuccessmsg .modal-body").html("Street name cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenricfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatenricunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");  

	}           
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_CORPORATE_CITIZENPR_NRIC, true);
           showNRICEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, true, false);
	   showNRICEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, false, false);
	   $('#corporatenonresidentpreviewnricmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate NRIC  - Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == 'corporatefincardmanually'){

var corporatefincardpassportno = document.getElementById("corporatefincardpassportNumber_"+getcounter).value;
var corporatefincardnationality = $('#corporatefincardnationality_'+getcounter+'_chosen span').text();
var corporatefincardcountry = $('#corporatefincardcountry_'+getcounter+'_chosen span').text();
var corporatefincardgender = $("input[name='corporatefincardgender[]']:checked").val();
var corporatefincarddatetimepicker = document.getElementById("corporatefincarddateissue_"+getcounter).value;
var corporatefincarddateexpiry = document.getElementById("corporatefincarddateexpiry_"+getcounter).value;
var corporatefincardplaceofBirth = $('#corporatefincarddateplace_'+getcounter+'_chosen span').text();
var corporatefincarddateBirth = document.getElementById("corporatefincarddatebirth_"+getcounter).value;

var corporatefincardemployer = document.getElementById("corporatefincardemployer_"+getcounter).value;
var corporatefincardoccupation = document.getElementById("corporatefincardoccupation_"+getcounter).value;
var corporateworkpassdateissue = document.getElementById("corporateworkpassdateissue_"+getcounter).value;
var corporateworkpassdateexpiry = document.getElementById("corporateworkpassdateexpiry_"+getcounter).value;
var corporatewfincardNumber = document.getElementById("corporatewfincardNumber_"+getcounter).value;

localStorage.setItem("corporatefincardmanuallypassportno_"+getcounter,corporatefincardpassportno);
localStorage.setItem("corporatefincardmanuallynationality_"+getcounter,corporatefincardnationality);
localStorage.setItem("corporatefincardmanuallycountry_"+getcounter,corporatefincardcountry);
localStorage.setItem("corporatefincardmanuallygender_"+getcounter,corporatefincardgender);
localStorage.setItem("corporatefincardmanuallydatetimepicker_"+getcounter,corporatefincarddatetimepicker);
localStorage.setItem("corporatefincardmanuallydateexpiry_"+getcounter,corporatefincarddateexpiry);
localStorage.setItem("corporatefincardmanuallyplaceofBirth_"+getcounter,corporatefincardplaceofBirth);
localStorage.setItem("corporatefincardmanuallydateBirth_"+getcounter,corporatefincarddateBirth);

localStorage.setItem("corporatefincardmanuallyemployer_"+getcounter,corporatefincardemployer);
localStorage.setItem("corporatefincardmanuallyoccupation_"+getcounter,corporatefincardoccupation);
localStorage.setItem("corporatefincardmanuallyworkpassdateissue_"+getcounter,corporateworkpassdateissue);
localStorage.setItem("corporatefincardmanuallyworkpassdateexpiry_"+getcounter,corporateworkpassdateexpiry);
localStorage.setItem("corporatefincardmanuallywfincardnumber_"+getcounter,corporatewfincardNumber);


        if(corporatefincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporatefincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");  

	}
        else if(corporatefincardcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatefincardgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show"); 
	}
        else if(corporatefincarddatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatefincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show"); 

	}         
        else if(corporatefincarddateBirth == ''){

              //alert("Date of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatefincardplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
              $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(corporatefincardemployer == ''){

            //alert("Employer cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
            $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
	else if(corporatefincardoccupation == ''){

           //alert("Occupation cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
            $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporateworkpassdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
	else if(corporateworkpassdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
            $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
            $("#displaysuccessmsg").modal("show");   

	}
        else if(corporatewfincardNumber == ''){

            //alert("Fin Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
            $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}   
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_CORPORATE_FIN_PASSPORT, true);
           showFinPassportEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, false);
	   showFINCardEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, true, false);
	   showFINCardEmpty(ROLE_CORPORATE_SHAREHOLDER, getcounter, false, false);
	   $('#corporatefincardmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Corporate Passport/FIN  - Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}




}



}
</script>
