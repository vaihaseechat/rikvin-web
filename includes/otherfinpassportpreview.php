<div class="modal fade"  data-keyboard="true"   id="otherpreviewfinpassport" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">Passport Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">
					<div class="row mt-3">
						<div class="col-sm">
							<div class="form-group row">
								<div class="col-sm-12">
									Please check details below.
								</div>
								<div class="col-sm-12">
									<div class="passport-scan-copy" id="otherfinimage"><img src="" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="surname">Surname</label>
								<div class="col-sm-12"><input name="otherfinsurname" class="form-control" id="otherfinsurname"  value="" type="text"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="givenName">Given name</label>
								<div class="col-sm-12"><input name="otherfingivenName" class="form-control" id="otherfingivenName"  value="" type="text"></div>
							</div>
						</div>
                                         </div>
                                         <div class="row"> 
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="passportNumber">Passport Number</label>
								<div class="col-sm-12"><input name="otherfinpassportNumber" class="form-control" id="otherfinpassportNumber"  value="" type="text"></div>
							</div>
						</div>
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="nationality">Nationality</label>
								<select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="otherfinnationality" name="otherfinnationality"></select>
							</div>
						</div> 
					</div>					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Country of Issue</label>
								<select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="otherfincntryissue" name="otherfincntryissue"></select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Gender</label>
								<div class="col-sm-12">
									<label class="radio-inline mr-3">
									  <input name="otherfinoptradio" id="otherfinmgender" class="radio-red" checked type="radio" value="Male"> Male
									</label>
									<label class="radio-inline">
									  <input name="otherfinoptradio" id="otherfinfgender" class="radio-red" type="radio" value="Female"> Female
									</label>
								</div>
							</div>
						</div>                                              
                                                
						
					</div>
					
					
					<div class="row">
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfExpiry">Date of Expiry</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
										<input id="otherfindateexpiry" class="form-control" name="otherfindateexpiry" value="" type="text" placeholder="DD-MM-YYYY" maxlength="10">
										
									</div>
								</div>	
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfBirth">Date of Birth</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
										<input id="otherfindob" class="form-control" name="otherfindob" value="" type="text" placeholder="DD-MM-YYYY" maxlength="10">
										
									</div>
								</div>	
							</div>
						</div>
						
									
					</div>
					<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
                                                                <input type="hidden" name="otherfinpassportfilename" id="otherfinpassportfilename" value="">   
								<button class="btn btn-next" onclick="savefinpreview();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savefinpreview(){

if( $('#otherpreviewfinpassport').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#otherpreviewfinpassport').css("overflow","auto");
}


var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

var otherfinpassportfilename = document.getElementById("otherfinpassportfilename").value;
var otherfinsurname = document.getElementById("otherfinsurname").value;
var otherfingivenname = document.getElementById("otherfingivenName").value;
var otherfinpassportno = document.getElementById("otherfinpassportNumber").value;
var otherfinnationality = $('#otherfinnationality_chosen span').text();
var otherfincountry = $('#otherfincntryissue_chosen span').text();
var otherfindatetimepicker = '';
var otherfingender = $("input[name='otherfinoptradio']:checked").val();
var otherfindob = document.getElementById("otherfindob").value;
var otherfindateexpiry = document.getElementById("otherfindateexpiry").value;
var otherfinpob = "";

var getfincarddateexpiry = formatingdata(otherfindateexpiry);
var getfincarddob = formatingdata(otherfindob);

var cur_date = new Date();
//var dob =  checkfuturedata(otherfindob);

var dob = $('#otherfindob').datepicker('getDate');

       if(otherfinsurname == ''){
           //alert("Surname cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
	else if(otherfingivenname == ''){

           //alert("Given name cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(otherfinpassportno == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(otherfinnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherfinnationality == ''){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherfincountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(otherfincountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}                
        else if(otherfindateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}
        else if(otherfindob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(getfincarddateexpiry == true){           
              //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Passport Information");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
           $("#displaysuccessmsg").modal("show");
	} 
        else if(getfincarddob == true){
             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show");
	} else if((otherfindateexpiry != '') && (otherfindateexpiry.length < 10)){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if((otherfindob != '') && (otherfindob.length < 10)){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        }         
	else{
	   //alert("Saved Succesfully");
	    $('#otherpreviewfinpassport').modal('hide');
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto"); 
            $("#displaysuccessmsg").modal("show");
	}


localStorage.setItem("otherfinsurname",otherfinsurname);
localStorage.setItem("otherfingivenname",otherfingivenname);
localStorage.setItem("otherfinpassportno",otherfinpassportno);
localStorage.setItem("otherfinnationality",otherfinnationality);
localStorage.setItem("otherfincountry",otherfincountry);
localStorage.setItem("otherfindatetimepicker",otherfindatetimepicker);
localStorage.setItem("otherfindateexpiry",otherfindateexpiry);
localStorage.setItem("otherfindob",otherfindob);
localStorage.setItem("otherfinpob",otherfinpob);
localStorage.setItem("otherfingender",otherfingender);

}

</script>

