<script>
function bofinsavepreviewpopup(getcounter,getboname){

if( $('#bofincardpreviewpassport_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden"); 
    $('#bofincardpreviewpassport_'+getcounter+"_"+getboname).css("overflow","auto");
}

var bofincardsurname = document.getElementById("bofincardsurname_"+getcounter+"_"+getboname).value;
var bofincardgivenname = document.getElementById("bofincardgivenName_"+getcounter+"_"+getboname).value;
var bofincardpassportno = document.getElementById("bofincardppassportNumber_"+getcounter+"_"+getboname).value;
var bofincardnationality = $('#bofincardpnationality_'+getcounter+'_'+getboname+'_chosen span').text();
var bofincardcountry = $('#bofincardpcountry_'+getcounter+'_'+getboname+'_chosen span').text();
var bofincardgender = $("input[name='bofincardpgender[]']:checked").val();
var bofincarddateexpiry = document.getElementById("bofincardpdateexpiry_"+getcounter+"_"+getboname).value;
var bofincarddob = document.getElementById("bofincardpdob_"+getcounter+"_"+getboname).value;
//alert(bofincardnationality);return false;

var getfincarddateexpiry = formatingdata(bofincarddateexpiry);
var getfincarddob = formatingdata(bofincarddob);

var cur_date = new Date();
//var dob =  checkfuturedata(bofincarddob);
var dob = $('#bofincardpdob_'+getcounter+"_"+getboname).datepicker('getDate');

localStorage.setItem("bofincardsurname_"+getcounter+"_"+getboname,bofincardsurname);
localStorage.setItem("bofincardgivenName_"+getcounter+"_"+getboname,bofincardgivenname);
localStorage.setItem("bofincardpassportno_"+getcounter+"_"+getboname,bofincardpassportno);
localStorage.setItem("bofincardnationality_"+getcounter+"_"+getboname,bofincardnationality);
localStorage.setItem("bofincardcountry_"+getcounter+"_"+getboname+getcounter,bofincardcountry);
localStorage.setItem("bofincardgender_"+getcounter+"_"+getboname+getcounter,bofincardgender);
//localStorage.setItem("bofincarddatetimepicker_"+getcounter,bofincarddatetimepicker);
localStorage.setItem("bofincarddateexpiry_"+getcounter+"_"+getboname+getcounter,bofincarddateexpiry);
//localStorage.setItem("bofincardplaceofBirth_"+getcounter,bofincardplaceofBirth);
localStorage.setItem("bofincarddob_"+getcounter+"_"+getboname,bofincarddob);

        if(bofincardsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(bofincardgivenname == ''){

           //alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(bofincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(bofincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bofincardcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(bofincardgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");
	}        
        else if(bofincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");  

	} 
        else if(bofincarddob == ''){

             //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(getfincarddateexpiry == true){
           
              //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
           $("#displaysuccessmsg").modal("show");
	} 
        else if(getfincarddob == true){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show");
	}
         else if(bofincarddob.length < 10){
              $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(bofincarddateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        } 
	else{
	   //alert("Saved Succesfully");
	   $('#bofincardpreviewpassport_'+getcounter+"_"+getboname).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Bo Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
           $("#displaysuccessmsg").modal("show"); 
       }

}

</script>

