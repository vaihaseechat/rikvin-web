<div id="otherpassholder" class="choose-file" style="display:none;">
	<div class="row">
		<div class="col-lg-4">
			<label class="col-form-label" for="uploadPassport">Upload Passport</label>
		</div>
		<div class="col-lg-8">
			<div class="row mb-1">
				<div class="col-sm">
					<div class="input-group">
						<div class="custom-file">
							<input name="finuploadPassport" class="custom-file-input" id="finuploadPassport" type="file" onchange="checkotherPdfParse(this.files,1,otherfinuploadpassportfile);">
							<label class="custom-file-label dashed-lines" id="finuploadPassportlabel">Passport</label>
						</div>
					</div>
					<div class="finuploadPassport-alert" id="finuploadPassport-alert" style="color:#B90D0F;"><span class="font-italic small" id="finuploadPassport-alert-msg"></span></div>
				</div>
				<div class="col-sm">
					<button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#otherpreviewpassportfinmanually"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button>
				</div>
			</div>
                        <div class="row" id="otherfinloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
			<div class="row">
				<div class="col-sm-6" id="otherpassportfin" style="display:none;">
					<button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#otherpreviewfinpassport"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button>
				</div>
			</div>
	         </div>
	</div>
<div class="row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="">Upload FIN</label>
	</div>
	<div class="col-lg-8">
		<div class="row mb-1">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="finfrontupload" class="custom-file-input" id="finfrontupload" type="file" onchange="checkotherPdfParse(this.files,1,otherfinfrontfile);">
						<label class="custom-file-label dashed-lines" id="otherfinfrontuploadlabel">FIN Card Front</label>
					</div>
				</div>
				<div class="finfrontupload-alert" id="finfrontupload-alert" style="color:#B90D0F;"><span class="font-italic small" id="finfrontupload-alert-msg"></span></div>
			</div>
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="finbackupload" class="custom-file-input" id="finbackupload" type="file" onchange="checkotherPdfParse(this.files,1,otherfinbackfile);">
						<label class="custom-file-label dashed-lines" id="otherfinbackuploadlabel">FIN Card Back</label>
					</div>
				</div>
				<div class="finbackupload-alert" id="finbackupload-alert" style="color:#B90D0F;"><span class="font-italic small" id="finbackupload-alert-msg"></span></div>
			</div>
		</div>
                <div class="row" id="otherfincardloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
                <div class="row">
		        <div id="otherfincardfrontpreview" class="col-sm-6" style="display:none;">
		          <button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#otherfinfrontpreview" style="margin-otherttom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your FIN Front details</button>
		         </div>
		         <div id="otherfincardbackpreview" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#otherfinbackpreview" style="margin-otherttom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your FIN Back details</button>
		         </div>
                </div>
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label>
	</div>
	<div class="col-lg-8">
		<div class="row">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="finaddress" class="custom-file-input" id="finaddress" type="file" onchange="getotherfinaddress(this.files);">
						<label class="custom-file-label dashed-lines" id="finaddresslabel">Address</label>
					</div>
				</div>
				<div class="finaddress-alert" id="finaddress-alert" style="color:#B90D0F;"><span class="font-italic small" id="finaddress-alert-msg"></span></div>
			</div>
			<div class="col-sm">
				<textarea class="form-control col-sm-12" name="finresaddress" id="finresaddress" rows="2" placeholder="Please key in residential address" ></textarea>
			</div>
		</div>
	</div>
</div>
</div>

<?php //Citizen PR ?>

<div id="othercitizenpr" class="choose-file" style="display: none;">
<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="">Upload NRIC</label>
	</div>
	<div class="col-lg-8">
		<div class="row mb-1">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="othernricfrontup" class="custom-file-input" id="othernricfrontup" type="file" onchange="checkotherPdfParse(this.files,1,othernricfrontupload);">
						<label class="custom-file-label dashed-lines" id="othernricfrontlabel">NRIC Front</label>
					</div>
				</div>
				<div class="othernricfrontup-alert" id="othernricfrontup-alert" style="color:#B90D0F;"><span class="font-italic small" id="othernricfrontup-alert-msg"></span></div>
			</div>
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="othernricbackup" class="custom-file-input" id="othernricbackup" type="file" onchange="checkotherPdfParse(this.files,1,othernricbackupload);">
						<label class="custom-file-label dashed-lines" id="othernricbacklabel">NRIC Back</label>
					</div>
				</div>
				<div class="othernricbackup-alert" id="othernricbackup-alert" style="color:#B90D0F;"><span class="font-italic small" id="othernricbackup-alert-msg"></span></div>
			</div>
		</div>
                <div class="row" id="othernricloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
                <div class="row">
		        <div id="othernriccardfrontpreview" class="col-sm-6" style="display: none;">
		          <button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#othernricfrontpreview" style="margin-otherttom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Front details</button>
		         </div>
		         <div id="othernriccardbackpreview" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#othernricbackpreview" style="margin-otherttom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Back details</button>
		         </div>
                </div>
		<div class="row">
			<div class="col-sm-6">
				<button id="othernricmanual" type="button" class="btn btn-style-1" data-toggle="modal" data-target="#otherpreviewnricmanually"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button>
			</div>
		</div>

	</div>
</div>
</div>

<?php // Non resident ?>

<div id="othernonresident" class="choose-file" style="display:none;">
<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="uploadPassport">Upload Passport</label>
	</div>
	<div class="col-lg-8">
		<div class="row mb-1">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="otheruploadpassport" class="custom-file-input" id="otheruploadpassport" type="file" onchange="checkotherPdfParse(this.files,1,othernonuploadpassportfile);">
						<label id="othernonresidentlabel" class="custom-file-label dashed-lines" for="">Passport</label>
					</div>
				</div>
				<div class="otheruploadpassport-alert" id="otheruploadpassport-alert" style="color:#B90D0F;"><span class="font-italic small" id="otheruploadpassport-alert-msg"></span></div>
			</div>
			<div class="col-sm">
				<button type="button" class="btn btn-style-1" id="otherpreviewmanual" data-toggle="modal" data-target="#otherpreviewpassportmanually"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button>
			</div>
		</div>
                <div class="row" id="othernonpassportloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
		<div class="row">
			<div class="col-sm-6" id="othernonpassport" style="display:none;">
				<button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#otherpreviewpassport"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button>
			</div>
		</div>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label>
	</div>
	<div class="col-lg-8">
		<div class="row">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="otheraddress" class="custom-file-input" id="otheraddress" type="file" onchange="getotheraddress(this.files);">
						<label class="custom-file-label dashed-lines" id="otheraddresslabel">Address</label>
					</div>
				</div>
				<div class="otheraddress-alert" id="otheraddress-alert" style="color:#B90D0F;"><span class="font-italic small" id="otheraddress-alert-msg"></span></div>
			</div>
			<div class="col-sm">
				<textarea class="form-control col-sm-12" name="" id="otherresaddress" rows="2" placeholder="Please key in residential address" ></textarea>
				<div class="otherresaddress-alert" id="otherresaddress-alert" style="color:#B90D0F;"><span class="font-italic small" id="otherresaddress-alert-msg"></span></div>
			</div>
			
		</div>
	</div>
</div>
</div>

<script>

// For other Residencystatus only
function getotherresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "othernonresident") {
          //alert("fddf");

	   $("#othernonresident").css("display", "block");
	   $("#othercitizenpr").css("display", "none");
	   $("#otherpassholder").css("display", "none");
	}

	else if(getvalue == "othercitizenpr") {

	   $("#othernonresident").css("display", "none");
	   $("#othercitizenpr").css("display", "block");
	   $("#otherpassholder").css("display", "none");
	}

	else if(getvalue == "otherpassholder") {

	   $("#othernonresident").css("display", "none");
	   $("#othercitizenpr").css("display", "none");
	   $("#otherpassholder").css("display", "block");
	}
        else {
 
           uploadFields.hide();

        }

}

</script>
<script>
function getotheraddress(getdata){
//alert(getdata[0]);
var addrcount = 20;
var myImage = getdata[0];
var filename = myImage.name;
var finallabel = filename.slice(0, addrcount) + (filename.length > addrcount ? "..." : "");
//alert(filename);

var extension=getdata[0].name.split('.').pop().toLowerCase();

if((extension == "png") || (extension == "jpg") || (extension == "jpeg") || (extension == "gif") || (extension == "pdf") || (extension == "doc") || (extension == "docx") || (extension == "tiff"))
{
 $("#otheraddresslabel").text(finallabel);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
	$.ajax({
	    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
	    type: 'post',
	    data: form,
	    contentType: false,
	    processData: false,
	    success: function(response){
	
	    },
	});
}else{
   $("#displaysuccessmsg .modal-title").html("Error");
   $("#displaysuccessmsg .modal-body").html("Invalid File type");
   $("#displaysuccessmsg").modal("show");
}


}

function getotherfinaddress(getdata){

var addrcount = 20;
var myImage = getdata[0];
var filename = myImage.name;
var finallabel = filename.slice(0, addrcount) + (filename.length > addrcount ? "..." : "");
//alert(filename);
var extension=getdata[0].name.split('.').pop().toLowerCase();

if((extension == "png") || (extension == "jpg") || (extension == "jpeg") || (extension == "gif") || (extension == "pdf") || (extension == "doc") || (extension == "docx") || (extension == "tiff"))
{
 $("#finaddresslabel").text(finallabel);

var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
	$.ajax({
	    url: '<?php echo BASE_URL;?>/includes/uploadotherdocuments.php',
	    type: 'post',
	    data: form,
	    contentType: false,
	    processData: false,
	    success: function(response){
	
	    },
	});

}else{
   $("#displaysuccessmsg .modal-title").html("Error");
   $("#displaysuccessmsg .modal-body").html("Invalid File type");
   $("#displaysuccessmsg").modal("show");
}

}
</script>

<?php include('includes/otheruploadimages.php');?>
