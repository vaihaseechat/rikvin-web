<script>

function declare_ultimate_button(pos, key) {
	console.log('position' + pos);
	console.log('key' + key);
	localStorage.setItem('bo_insert_position', pos);
	localStorage.setItem('bo_insert_key_name', key);
 

        var fieldHTML = '<div class="modal fade"  data-keyboard="true"   id="modalbeneficialowner_'+pos+'_'+key+'" tabindex="-1" role="dialog">';
	fieldHTML += '<div class="modal-dialog modal-dialog-centered modal-lg" role="document">';
	fieldHTML += '<div class="modal-content row">';
	fieldHTML += '<div class="modal-header"><h5 class="modal-title" id="">Ultimate Beneficial Owner</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
	fieldHTML += '<div class="col-lg-12 background-grey-2 popheight">';
	fieldHTML += '<div class="form-group row mt-4"><div class="col-lg-4"><label class="col-form-label required" for="">Full Name</label></div><div class="col-lg-8"><input type="text" name="bofullname" class="form-control" id="bofullname_'+pos+'_'+key+'" placeholder="" /><div class="bofullName-alert" id="bofullName-alert_'+pos+'_'+key+'" style="color:#B90D0F;"></div></div>';
        fieldHTML += '</div>';
        fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="residencyStatus" id="bo_residency_status_'+pos+'_'+key+'">Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select name="boresidencyStatus" id="boresidencystatus_'+pos+'_'+key+'" class="custom-select select-w-100" data-placeholder="Please select..." onchange="getboresidencystatus(this.value)"><option value="">Please select</option><option value="bononresident_'+pos+'_'+key+'" rel="nonresident">Non-resident</option><option value="bocitizenpr_'+pos+'_'+key+'" rel="citizenpr">Singapore Citizen / PR</option><option value="bopassholder_'+pos+'_'+key+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="boresidencyStatus-alert" id="boresidencyStatus-alert_'+pos+'_'+key+'" style="color:#B90D0F;"></div></div></div></div>';

        fieldHTML += '<div id="bopassholder_'+pos+'_'+key+'" class="choose-file form-group" style="display: none;">';

                     fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm-6"><div class="input-group"><div class="custom-file"><input name="bouploadPassport" class="custom-file-input" id="bouploadPassport_'+pos+'_'+key+'" type="file" onchange="checkPdfParse(this.files,'+pos+',\''+key+'\',bofinuploadpassportfile);"><label class="custom-file-label dashed-lines" id="bofinpassportlabel_'+pos+'_'+key+'">Passport</label></div></div><div class="bouploadPassport_'+pos+'_'+key+'-alert" id="bouploadPassport_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bouploadPassport_'+pos+'_'+key+'-alert-msg"></span></div><div class="row"><div id="bbofincardpreviewpassport_'+pos+'_'+key+'" class="col-sm-12" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bofincardpreviewpassport_'+pos+'_'+key+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your Passport Details</button></div></div><div class="row" id="bouploadPassportloadingImage_'+pos+'_'+key+'" style="display:none;"><div class="col-sm-6 text-center"><img src="images/image-loading.gif"></div></div></div><span style="margin: 5px 0 17px 0;">OR</span><div class="col-sm-8"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#bofincardmanually_'+pos+'_'+key+'" id="bomanually_'+pos+'_'+key+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div>';

	            fieldHTML += '<div class="row"><div class="col-lg-4"><label class="col-form-label required">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="bofincardfront" class="custom-file-input" id="bofincardfront_'+pos+'_'+key+'" type="file" onchange="checkPdfParse(this.files,'+pos+',\''+key+'\',bofincardfront);"><label class="custom-file-label dashed-lines" id="bofincardfrontlabel_'+pos+'_'+key+'">FIN Card Front</label></div></div><div class="bofincardfront_'+pos+'_'+key+'-alert" id="bofincardfront_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bofincardfront_'+pos+'_'+key+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="bofincardback" class="custom-file-input" id="bofincardback_'+pos+'_'+key+'" type="file" onchange="checkPdfParse(this.files,'+pos+',\''+key+'\',bofincardback);"><label class="custom-file-label dashed-lines" id="bofincardbacklabel_'+pos+'_'+key+'">FIN Card Back</label></div></div><div class="bofincardback_'+pos+'_'+key+'-alert" id="bofincardback_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bofincardback_'+pos+'_'+key+'-alert-msg"></span></div></div></div><div class="row"><div id="bbofincardfrontpreview_'+pos+'_'+key+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bofincardfrontpreview_'+pos+'_'+key+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your front fin details</button></div><div id="bbofincardbackpreview_'+pos+'_'+key+'" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bofincardbackpreview_'+pos+'_'+key+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your fin back details</button></div></div><div class="row" id="bouploadfincardloadingImage_'+pos+'_'+key+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


	           fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="boaddress" class="custom-file-input" id="boaddress_'+pos+'_'+key+'" type="file" onchange="boaddress(this.files);"><label class="custom-file-label dashed-lines" id="boaddresslabel_'+pos+'_'+key+'">Address</label></div></div><div class="boaddress_'+pos+'_'+key+'-alert" id="boaddress_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="boaddress_'+pos+'_'+key+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="boresaddress" id="boresaddress_'+pos+'_'+key+'" rows="2" placeholder="Please key in residential address"></textarea><div class="boresaddress_'+pos+'_'+key+'-alert" id="boresaddress_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="boresaddress_'+pos+'_'+key+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                 
                     fieldHTML +='<div id="bocitizenpr_'+pos+'_'+key+'" class="choose-file" style="display: none;">';

                     fieldHTML +='<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="bonricfront" class="custom-file-input" id="bonricfront_'+pos+'_'+key+'" type="file" onchange="checkPdfParse(this.files,'+pos+',\''+key+'\',bonricfront);"><label class="custom-file-label dashed-lines" id="bonricfrontlabel_'+pos+'_'+key+'">NRIC Front</label></div></div><div class="bonricfront_'+pos+'_'+key+'-alert" id="bonricfront_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bonricfront_'+pos+'_'+key+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="bonricback" class="custom-file-input" id="bonricback_'+pos+'_'+key+'" type="file" onchange="checkPdfParse(this.files,'+pos+',\''+key+'\',bonricback);"><label class="custom-file-label dashed-lines" id="bonricbacklabel_'+pos+'_'+key+'">NRIC Back</label></div></div><div class="bonricback_'+pos+'_'+key+'-alert" id="bonricback_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bonricback_'+pos+'_'+key+'-alert-msg"></span></div></div></div><div class="row"><div id="bbonricfrontpreview_'+pos+'_'+key+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bonricfrontpreview_'+pos+'_'+key+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Front details</button></div><div id="bbonricbackpreview_'+pos+'_'+key+'" class="col-sm-6" style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bonricbackpreview_'+pos+'_'+key+'" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Back details</button></div></div><div class="row" id="bouploadnricloadingImage_'+pos+'_'+key+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div><div class="row"><div class="col-sm-6"><button type="button" class="btn btn-style-1" data-toggle="modal" id="bononresidentpreviewnricmanually_'+pos+'_'+key+'" data-target="#bopreviewnricmanually_'+pos+'_'+key+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="bononresident_'+pos+'_'+key+'" class="choose-file" style="display:none;">';

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" >Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm-6"><div class="input-group"><div class="custom-file"><input name="bononresidentuploadPassport" class="custom-file-input" id="bononresidentuploadpassport_'+pos+'_'+key+'" type="file" onchange="checkPdfParse(this.files,'+pos+',\''+key+'\',bononuploadpassportfile);"><label class="custom-file-label dashed-lines" id="bononresidentlabel_'+pos+'_'+key+'">Passport</label></div></div><div class="bononresidentuploadpassport_'+pos+'_'+key+'-alert" id="bononresidentuploadpassport_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bononresidentuploadpassport_'+pos+'_'+key+'-alert-msg"></span></div></div><span style="margin: 5px 0 17px 0;">OR</span><div class="col-sm-6"><button type="button" class="btn btn-style-1" data-toggle="modal" id="bononresidentpreviewpassportmanually_'+pos+'_'+key+'" data-target="#bopreviewpassportmanually_'+pos+'_'+key+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i>Enter Passport information manually</button></div></div><div class="row"><div  id="bbononresidentpreviewpassport_'+pos+'_'+key+'" class="col-sm-6"  style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bononresidentpreviewpassport_'+pos+'_'+key+'"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div><div class="row" id="bouploadpassportfileloadingImage_'+pos+'_'+key+'" style="display:none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div></div></div>';


                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" data-toggle="tooltip" data-placement="right"  title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="bononresidentaddress" class="custom-file-input" id="bononresidentaddress_'+pos+'_'+key+'" type="file" onchange="bononresidentaddress(this.files);"><label class="custom-file-label dashed-lines" id="bononresidentaddresslabel_'+pos+'_'+key+'">Address</label></div></div><div class="bononresidentaddress_'+pos+'_'+key+'-alert" id="bononresidentaddress_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bononresidentaddress_'+pos+'_'+key+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="bononresidentresaddress" id="bononresidentresaddress_'+pos+'_'+key+'" rows="2" placeholder="Please key in residential address"></textarea><div class="bononresidentresaddress_'+pos+'_'+key+'-alert" id="bononresidentresaddress_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bononresidentresaddress_'+pos+'_'+key+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident

        fieldHTML += '<div class="form-group row"><label class="col-lg-4 col-form-label required" for="contactNumber">Contact Details</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input type="text" name="boemail" class="form-control mb-2" id="boemail_'+pos+'_'+key+'" placeholder="Email" required /><div class="boemail-alert" id="boemail-alert_'+pos+'_'+key+'" style="color:#B90D0F;"></div></div><div class="col-sm" ><input type="text" name="bocontactnumber" class="form-control mb-2" id="bocontactnumber_'+pos+'_'+key+'" placeholder="Contact number"/><div class="bocontact-alert" id="bocontact-alert_'+pos+'_'+key+'" style="color:#B90D0F;"></div></div></div></div></div>';

        fieldHTML += '<div class="form-group row" id="bopoliticallyexposed_'+pos+'_'+key+'"><label class="col-lg-4 col-form-label required" data-toggle="tooltip" data-placement="right"  title="Politically Exposed Persons (PEPs) are individuals who are or have been entrusted with prominent public functions in a foreign country, for example Heads of State or of government, senior politicians, senior government, judicial or High Ranking military officials, senior executives of state owned corporations, important political party officials. Business relationships with family members or close associates of PEP.">Are you a Politically Exposed Person (PEP) <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input name="bopep_'+pos+'_'+key+'" id="bopep_'+pos+'_'+key+'" value="bopepyes_'+pos+'_'+key+'" type="radio" onclick="ShowBOHidesDiv(this.value)"><span class="custom-radio-label">Yes</span></label><div class="bopoliticallyExposed_'+pos+'_'+key+'-alert" id="bopoliticallyExposed_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="politicallyExposed_'+pos+'_'+key+'-alert-msg"></span></div></div><div class="col-sm"><label class="custom-radio-field"><input id="bopep_'+pos+'_'+key+'" name="bopep_'+pos+'_'+key+'" value="notbopep_'+pos+'_'+key+'" checked="checked" type="radio" onclick="ShowBOHidesDiv(this.value)"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // end of pep form 

        fieldHTML += '<div class="show-when-bopep_'+pos+'_'+key+'" id="bopepform_'+pos+'_'+key+'" style="display:none;">'; // start of bo pep form 
        fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">PEP in which country</label></div><div class="col-lg-8"><div class="selectdiv"><select name="bocountrypep[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="bocountrypep_'+pos+'_'+key+'"><option value="">Please select</option></select><div class="bocountrypep'+pos+'_'+key+'-alert" id="bocountrypep_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bocountrypep_'+pos+'_'+key+'-alert-msg"></span></div></div></div></div>'; // Country of pep form

         fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">Role of PEP</label></div><div class="col-lg-8"><div class="selectdiv"><select class="custom-select select-w-100" name="bopeprole[]" id="borolePep_'+pos+'_'+key+'"><option value="" selected="selected">Please select</option><option value="Ambassadors">Ambassadors</option><option value="Senior Military Officers">Senior Military Officers</option><option value="Political Party Leadership">Political Party Leadership</option><option value="Members of Parliament/Senate">Members of Parliament/Senate</option><option value="Heads of government agencies">Heads of government agencies</option><option value="Key leaders of state-owned enterprises">Key leaders of state-owned enterprises</option><option value="Key Senior Government Functionaries Judiciary/Legislature">Key Senior Government Functionaries Judiciary/Legislature</option><option value="Private companies, trusts or foundations owned or co-owned by PEPs, whether directly or indirectly">Private companies, trusts or foundations owned or co-owned by PEPs</option></select><div class="borolePep'+pos+'_'+key+'-alert" id="borolePep_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="borolePep_'+pos+'_'+key+'-alert-msg"></span></div></div></div></div>'; // Role of pep form

        fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label">PEP since</label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><select class="custom-select select-w-100" id="bopepFrom_'+pos+'_'+key+'" name="bopepFrom[]_'+pos+'_'+key+'" data-placeholder="From"><option value="" selected>From</option></select><div class="bopepFrom'+pos+'_'+key+'-alert" id="bopepFrom_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bopepFrom_'+pos+'_'+key+'-alert-msg"></span></div></div><div class="col-sm"><select class="custom-select select-w-100" id="bopepTo_'+pos+'_'+key+'" name="bopepTo_'+pos+'_'+key+'" data-placeholder="To"><option value="">To</option></select><div class="bopepTo'+pos+'_'+key+'-alert" id="bopepTo_'+pos+'_'+key+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bopepTo_'+pos+'_'+key+'-alert-msg"></span></div></div></div></div></div>';
        
        fieldHTML += '</div>'; // end of bopepform 
        
        fieldHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><button class="btn btn-next" onclick="myFunction('+pos+',\''+key+'\')">Save</button></div></div></div>';
	fieldHTML += '</div>';
	fieldHTML += '</div>';
	fieldHTML += '</div>';
        fieldHTML += '</div>';      
         
          // PassportManually

         var pmanually ='<div class="modal fade"  data-keyboard="true"   id="bopreviewpassportmanually_'+pos+'_'+key+'" tabindex="-1" role="dialog">';
		pmanually += '<div class="modal-dialog modal-dialog-centered" role="document">';
		pmanually += '<div class="modal-content row">';
		pmanually += '<div class="modal-header"><h5 class="modal-title" id="bononresidentmanually">Passport Information Manually</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		pmanually += '<div class="col-lg-12 background-grey-2 popheight">';
		
		pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="bononpassportNumber[]" class="form-control" id="bopassportNumber_'+pos+'_'+key+'"  value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Nationality</label><div class="col-sm-12"><select name="bononnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bononnationality_'+pos+'_'+key+'"></select></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="bononcountry[]" id="bononcountry_'+pos+'_'+key+'"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="bonongender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="bonongender[]" class="radio-red"> Female</label></div></div></div></div>';

                 pmanually += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bonondateissue_'+pos+'_'+key+'" class="form-control" name="bonondateissue[]" value="" placeholder="DD-MM-YYYY" maxlength="10"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bonondateexpiry_'+pos+'_'+key+'" class="form-control" name="bonondateexpiry[]" value="" ></div></div></div></div></div>';

                 pmanually +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bonondatebirth_'+pos+'_'+key+'" class="form-control" name="bonondatebirth[]" value="" placeholder="DD-MM-YYYY" maxlength="10"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bonondateplace_'+pos+'_'+key+'" name="bonondateplace[]"></select></div></div></div></div>';

                 pmanually += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bopassportsavemanually('+pos+',\''+key+'\');">Save</a></div></div></div>';
		pmanually += '</div></div></div></div>';  

                //nric manually
                                 
		 var nricfield = '<div class="modal fade"  data-keyboard="true"   id="bopreviewnricmanually_'+pos+'_'+key+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfield += '<div class="modal-content row">';
                     nricfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC and Address Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfield += '<div class="col-lg-12 background-grey-2 popheight">'; 
                      nricfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="bononresidentnricnumber[]" class="form-control" id="bononresidentnricnumber_'+pos+'_'+key+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input name="bononresidentgender[]" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input name="bononresidentgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input id="bononresidentdob_'+pos+'_'+key+'" class="form-control" name="bononresidentdob[]" placeholder="DD-MM-YYYY" maxlength="10" type="text" ></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="bonricnationality" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bonricnationality_'+pos+'_'+key+'"></select></div></div></div></div>';

                     nricfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bonricpostcode_'+pos+'_'+key+'" name="bonricpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="bonricstreetname[]" id="bonricstreetname_'+pos+'_'+key+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bonricfloor_'+pos+'_'+key+'" name="bonricfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="bonricunit_'+pos+'_'+key+'" name="bonricunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bonricsavemanually('+pos+',\''+key+'\');">Save</a></div></div></div>';

                     nricfield += '</div></div></div></div>'; 

                 //Fin manually
                  //fincard manuallyy
               var fincard ='<div class="modal fade"  data-keyboard="true"   id="bofincardmanually_'+pos+'_'+key+'" tabindex="-1" role="dialog">';
		fincard += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincard += '<div class="modal-content row">';
		fincard += '<div class="modal-header"><h5 class="modal-title" id="bofincardmanually">Passport and WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		fincard += '<div class="col-lg-12 background-grey-2 popheight">';
		
		fincard += '<div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Passport Number</label><div class="col-sm-12"><input type="text" name="bofincardpassportNumber[]" class="form-control" id="bofincardpassportNumber_'+pos+'_'+key+'"  value="" /></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bofincardnationality_'+pos+'_'+key+'" name="bofincardnationality[]"></select></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="bofincardcountry[]" id="bofincardcountry_'+pos+'_'+key+'"></select></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" name="bofincardgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="bofincardgender[]" class="radio-red"> Female</label></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincarddateissue_'+pos+'_'+key+'" class="form-control" name="bofincarddateissue[]" value="" placeholder="DD-MM-YYYY" maxlength="10"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincarddateexpiry_'+pos+'_'+key+'" class="form-control" name="bofincarddateexpiry[]" value="" placeholder="DD-MM-YYYY" maxlength="10"></div></div></div></div>';

                 fincard +='<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="bofincarddatebirth_'+pos+'_'+key+'" class="form-control" name="bofincarddatebirth[]" value="" placeholder="DD-MM-YYYY" maxlength="10"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="bofincarddateplace_'+pos+'_'+key+'" name="bofincarddateplace[]"></select></div></div></div></div>';

                 fincard +='<div class="row my-1"><div class="col-sm"><h4 class="font-weight-bold">Work Pass</h4></div></div>';
                
                 fincard += '<div class="row"><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="bofincardemployer[]" class="form-control" id="bofincardemployer_'+pos+'_'+key+'" placeholder="" value="" type="text"></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="bofincardoccupation[]" class="form-control" id="bofincardoccupation_'+pos+'_'+key+'" value="" type="text"></div></div></div>'; 

                  //fincard += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="boworkpassdateissue_'+pos+'_'+key+'" class="form-control" name="boworkpassdateissue[]" value="" placeholder="DD-MM-YYYY" maxlength="10"></div></div></div></div><div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="boworkpassdateexpiry_'+pos+'_'+key+'" class="form-control" name="boworkpassdateexpiry[]" value="" placeholder="DD-MM-YYYY" maxlength="10"></div></div></div></div>';

                 fincard += '<div class="col-md-6"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-12"><input type="text" name="bowfincardNumber[]" class="form-control" id="bowfincardNumber_'+pos+'_'+key+'"  value="" /></div></div></div></div>';        

                 fincard += '<div class="row my-1"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="bofinsavemanually('+pos+',\''+key+'\');">Save</a></div></div></div>';
		fincard += '</div></div></div></div>';    
                
                 var benefit = $('#modalbeneficialowner_'+pos+'_'+key);
                 var bownerdata = benefit.length;

                 //alert(bownerdata);

                 //if()  
		 //$('#modalbeneficialowner_'+pos+'_'+key).remove(); 

                 if(bownerdata == 0) {

		 $('body').find('#bopreviewpassport:last').before(fieldHTML); 
		 $('body').find('#bopreviewpassport:last').before(pmanually); 
		 $('body').find('#bopreviewpassport:last').before(nricfield);
                 $('body').find('#bopreviewpassport:last').before(fincard);   

                 }
              
                $('#bocountrypep_'+pos+'_'+key).html(countriesOption);
                $('#bocountrypep_'+pos+'_'+key).chosen(); 

                $('#bopepFrom_'+pos+'_'+key).html(yearfromOption);
                $('#bopepFrom_'+pos+'_'+key).chosen();

                $('#bopepTo_'+pos+'_'+key).html(yeartoOption);
                $('#bopepTo_'+pos+'_'+key).chosen();

                $('#bononnationality_'+pos+'_'+key).html(countriesOption);
                $('#bononnationality_'+pos+'_'+key).chosen(); 

                $('#bononcountry_'+pos+'_'+key).html(countriesOption);
                $('#bononcountry_'+pos+'_'+key).chosen(); 

                $('#bonondateplace_'+pos+'_'+key).html(countriesOption);
                $('#bonondateplace_'+pos+'_'+key).chosen(); 

                //nric Manually

                $('#bonricnationality_'+pos+'_'+key).html(countriesOption);
                $('#bonricnationality_'+pos+'_'+key).chosen(); 

                // fincard Manually
                $('#bofincardnationality_'+pos+'_'+key).html(countriesOption);
                $('#bofincardnationality_'+pos+'_'+key).chosen(); 
               
                $('#bofincardcountry_'+pos+'_'+key).html(countriesOption);
                $('#bofincardcountry_'+pos+'_'+key).chosen(); 
              
                $('#bofincarddateplace_'+pos+'_'+key).html(countriesOption);
                $('#bofincarddateplace_'+pos+'_'+key).chosen();  

                $('#bonondateissue_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#bonondateexpiry_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#bonondatebirth_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#bononresidentdob_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#bofincarddateissue_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#bofincarddateexpiry_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                $('#bofincarddatebirth_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#boworkpassdateissue_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

                 $('#boworkpassdateexpiry_'+pos+'_'+key).datepicker({   
		  beforeShow:function () {
		      setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		  }
	        });

	}






// For bo only
function getboresidencystatus(getvalue){
//alert(getvalue);
var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");
//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "bononresident_"+arr[1]+"_"+arr[2]) {
          //alert("fddf");

	   $("#bononresident_"+arr[1]+"_"+arr[2]).css("display", "block");
	   $("#bocitizenpr_"+arr[1]+"_"+arr[2]).css("display", "none");
	   $("#bopassholder_"+arr[1]+"_"+arr[2]).css("display", "none");
	}

	else if(getvalue == "bocitizenpr_"+arr[1]+"_"+arr[2]) {

	   $("#bononresident_"+arr[1]+"_"+arr[2]).css("display", "none");
	   $("#bocitizenpr_"+arr[1]+"_"+arr[2]).css("display", "block");
	   $("#bopassholder_"+arr[1]+"_"+arr[2]).css("display", "none");
	}

	else if(getvalue == "bopassholder_"+arr[1]+"_"+arr[2]) {

	   $("#bononresident_"+arr[1]+"_"+arr[2]).css("display", "none");
	   $("#bocitizenpr_"+arr[1]+"_"+arr[2]).css("display", "none");
	   $("#bopassholder_"+arr[1]+"_"+arr[2]).css("display", "block");
	}
        else {
 
            uploadFields.hide();

        }

}

</script>

<script>
var addrcount = 20;

function bononresidentaddress(getdata){

//alert(getdata);
var boposition = localStorage.getItem("bo_insert_position");
var bokey = localStorage.getItem("bo_insert_key_name");

var myImage = getdata[0];
var filename = myImage.name;
var finallabel = filename.slice(0, addrcount) + (filename.length > addrcount ? "..." : "");
//alert(filename);
 $('#bononresidentaddresslabel_'+boposition+'_'+bokey).text(finallabel);

var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
$.ajax({
    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
    type: 'post',
    data: form,
    contentType: false,
    processData: false,
    success: function(response){

    },
});


}

function boaddress(getdata){

//alert(getdata);
var boposition = localStorage.getItem("bo_insert_position");
var bokey = localStorage.getItem("bo_insert_key_name");

var myImage = getdata[0];
var filename = myImage.name;
var finallabel = filename.slice(0, addrcount) + (filename.length > addrcount ? "..." : "");
//alert(filename);
 $('#boaddresslabel_'+boposition+'_'+bokey).text(finallabel);

 var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
$.ajax({
    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
    type: 'post',
    data: form,
    contentType: false,
    processData: false,
    success: function(response){

    },
});

}
</script>

<script>

function ShowBOHidesDiv(getbo){

var arr = getbo.split('_'); 

//alert(arr[0]);
//alert(arr[1]);
//alert(arr[2]);
	if (getbo == "bopepyes_"+arr[1]+"_"+arr[2]) {
                $(".show-when-bopep_"+arr[1]+"_"+arr[2]).css("display", "block");
	} else {
		$(".show-when-bopep_"+arr[1]+"_"+arr[2]).css("display", "none");
	}

}

</script>

<?php include('includes/bouploadimages.php');?>


