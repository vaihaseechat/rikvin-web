<?php
session_start();
$error=array();
$extension=array("jpeg","jpg","png","gif","PDF","pdf");

$_SESSION['id'] = session_id();
if(!is_dir("didocuments/")) {
	mkdir("didocuments/");
}
if(!is_dir("didocuments/". $_SESSION['id'] ."/")) {
	mkdir("didocuments/". $_SESSION['id'] ."/");
}
// Upload section for di nonresident

if($_FILES["dinonresidentuploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["dinonresidentuploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["dinonresidentuploadPassport"]["name"][$key];
	$file_tmp = $_FILES["dinonresidentuploadPassport"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for di non resident address

if($_FILES["dinonresidentaddress"]["tmp_name"] != '') {

	foreach($_FILES["dinonresidentaddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["dinonresidentaddress"]["name"][$key];
	$file_tmp = $_FILES["dinonresidentaddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for di fin passport

if($_FILES["diuploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["diuploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["diuploadPassport"]["name"][$key];
	$file_tmp = $_FILES["diuploadPassport"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for di fin front

if($_FILES["difincardfront"]["tmp_name"] != '') {

	foreach($_FILES["difincardfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["difincardfront"]["name"][$key];
	$file_tmp = $_FILES["difincardfront"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for di fin back
if($_FILES["difincardback"]["tmp_name"] != '') {

	foreach($_FILES["difincardback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["difincardback"]["name"][$key];
	$file_tmp = $_FILES["difincardback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for di address

if($_FILES["diaddress"]["tmp_name"] != '') {

	foreach($_FILES["diaddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["diaddress"]["name"][$key];
	$file_tmp = $_FILES["diaddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for di NRIC front

if($_FILES["dinricfront"]["tmp_name"] != '') {

	foreach($_FILES["dinricfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["dinricfront"]["name"][$key];
	$file_tmp = $_FILES["dinricfront"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for di NRIC back

if($_FILES["dishareholdernricback"]["tmp_name"] != '') {

	foreach($_FILES["dishareholdernricback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["dishareholdernricback"]["name"][$key];
	$file_tmp = $_FILES["dishareholdernricback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("didocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"didocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}
?>

