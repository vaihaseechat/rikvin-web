<script>
function bonricfrontsavepreview(getcounter,getboname){

if( $('#bonricfrontpreview_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
    console.log("Modal is open");
    $('body').css("overflow","hidden");
    $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden"); 
    $('#bonricfrontpreview_'+getcounter+"_"+getboname).css("overflow","auto");
}

var bopreviewnricfrontnumber = document.getElementById("bopreviewfrontnricnumber_"+getcounter+"_"+getboname).value;
//var bogivenname = document.getElementById("bogivenName").value;
var bopreviewnricfrontdoi = document.getElementById("bopreviewnricfrontdob_"+getcounter+"_"+getboname).value;
var bopreviewnricfrontcob = $('#bopreviewnricfrontnationality_'+getcounter+'_'+getboname+'_chosen span').text();
var bopreviewnricfrontpostcode = document.getElementById("bopreviewnricfrontpostcode_"+getcounter+"_"+getboname).value;
var bopreviewnricfrontstreetname = document.getElementById("bopreviewnricfrontstreetname_"+getcounter+"_"+getboname).value;
var bopreviewnricfrontfloor = document.getElementById("bopreviewnricfrontfloor_"+getcounter+"_"+getboname).value;
var bopreviewnricfrontunit = document.getElementById("bopreviewnricfrontunit_"+getcounter+"_"+getboname).value;
var bopreviewnricfrontgender = $("input[name='bopreviewnricfrontgender[]']:checked").val();

var getdirectornricdob = formatingdata(bopreviewnricfrontdoi);

var cur_date = new Date();
var dob = $('#bopreviewnricfrontdob_'+getcounter+"_"+getboname).datepicker('getDate');
//var dob =  new Date(bopreviewnricfrontdoi.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

        if(bopreviewnricfrontnumber == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bopreviewnricfrontdoi == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(getdirectornricdob == true){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
           $("#displaysuccessmsg .modal-body").html("Invalid Date of Birth");
           $("#displaysuccessmsg").modal("show");

	}
	else if(bopreviewnricfrontcob == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bopreviewnricfrontdoi.length < 10){
              $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        } 
        /*else if(bopreviewnricfrontpostcode == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bopreviewnricfrontstreetname == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bopreviewnricfrontfloor == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bopreviewnricfrontunit == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}*/ 
        else{
	   //alert("Saved Succesfully");
	    $('#bonricfrontpreview_'+getcounter+"_"+getboname).modal('hide');
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('#bononresidentpreviewnricmanually_'+getcounter+"_"+getboname).prop('disabled', true);
            $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
            $("#displaysuccessmsg").modal("show");
	}


localStorage.setItem("bopreviewnricfrontnumber_"+getcounter+"_"+getboname,bopreviewnricfrontnumber);
localStorage.setItem("bopreviewnricfrontdoi_"+getcounter+"_"+getboname,bopreviewnricfrontdoi);
localStorage.setItem("bopreviewnricfrontcob_"+getcounter+"_"+getboname,bopreviewnricfrontcob);
localStorage.setItem("bopreviewnricfrontpostcode_"+getcounter+"_"+getboname,bopreviewnricfrontpostcode);
localStorage.setItem("bopreviewnricfrontstreetname_"+getcounter+"_"+getboname,bopreviewnricfrontstreetname);
localStorage.setItem("bopreviewnricfrontfloor_"+getcounter+"_"+getboname,bopreviewnricfrontfloor);
localStorage.setItem("bopreviewnricfrontunit_"+getcounter+"_"+getboname,bopreviewnricfrontunit);
localStorage.setItem("bopreviewnricfrontgender_"+getcounter+"_"+getboname,bopreviewnricfrontgender);



}

</script>
