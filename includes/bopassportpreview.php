<div id="bopreviewpassport">&nbsp;</div>
		
<script>
function bopassportsavepreview(getcounter,getboname){

        if( $('#bononresidentpreviewpassport_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
	    console.log("Modal is open");
            $('body').css("overflow","hidden");
            $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden"); 
            $('#bononresidentpreviewpassport_'+getcounter+"_"+getboname).css("overflow","auto");
	}

//console.log('getdata' + getdata);
//console.log('getcounter' + getcounter);

var bosurname = document.getElementById("bononresidentsurname_"+getcounter+"_"+getboname).value;
var bogivenname = document.getElementById("bononresidentgivenName_"+getcounter+"_"+getboname).value;
var bopassportno = document.getElementById("bononresidentpassportNumber_"+getcounter+"_"+getboname).value;
var bonationality = $('#bononresidentnationality_'+getcounter+'_'+getboname+'_chosen span').text();
var bocountry = $('#bononresidentcountry_'+getcounter+'_'+getboname+'_chosen span').text();
var bogender = $("input[name='bononresidentpgender[]']:checked").val();
var bodateexpiry = document.getElementById("bononresidentdateexpiry_"+getcounter+"_"+getboname).value;
var bodob = document.getElementById("bononresidentdateexpiry_"+getcounter+"_"+getboname).value;

var getpreviewdob = formatingdata(bodob);
var getnonresidentdateexpiry = formatingdata(bodateexpiry);

var cur_date = new Date();
//var dob =  checkfuturedata(bodob);
var dob = $('#bononresidentdateexpiry_'+getcounter+"_"+getboname).datepicker('getDate');

        if(bosurname == ''){
           //alert("Surname cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
	else if(bogivenname == ''){

           //alert("Given name cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(bopassportno == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(bonationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bonationality == ''){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(bocountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
         else if(bocountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(bodob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(bodateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");

	} 
        else if(getpreviewdob == true){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Invalid Date of Birth");
              $("#displaysuccessmsg").modal("show");

	} 
        else if(getnonresidentdateexpiry == true){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Invalid Date of Expiry");
              $("#displaysuccessmsg").modal("show");

	} 
        else if(bodob.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(bodateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        }      
	else{
	   //alert("Saved Succesfully");
	    $('#bononresidentpreviewpassport_'+getcounter+"_"+getboname).modal('hide');
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('#bononresidentpreviewpassportmanually_'+getcounter+"_"+getboname).prop('disabled', true);
            $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
            $("#displaysuccessmsg").modal("show");
	}


localStorage.setItem("bosurname_"+getcounter+"_"+getboname,bosurname);
localStorage.setItem("bogivenName_"+getcounter+"_"+getboname,bogivenname);
localStorage.setItem("bopassortno_"+getcounter+"_"+getboname,bopassportno);
localStorage.setItem("bonationality_"+getcounter+"_"+getboname,bonationality);
localStorage.setItem("bocntry_"+getcounter+"_"+getboname,bocountry);
localStorage.setItem("bogender_"+getcounter+"_"+getboname,bogender);
localStorage.setItem("bodob_"+getcounter+"_"+getboname,bodob);
localStorage.setItem("bodateexpiry_"+getcounter+"_"+getboname,bodateexpiry);




}
</script>

