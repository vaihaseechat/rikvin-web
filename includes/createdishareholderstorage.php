<script>
function dishareholderpopup(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);

//Storing di Non Resident

if(getresidencystatus == 'dinonresident'){

var dinonresidentsurname = document.getElementById("dinonresidentpsurname_"+getcounter).value;
var dinonresidentgivenname = document.getElementById("dinonresidentpgivenName_"+getcounter).value;
var dinonresidentpassportno = document.getElementById("dinonresidentppassportNumber_"+getcounter).value;
var dinonresidentnationality = $('#dinonresidentpnationality_'+getcounter+'_chosen span').text();
var dinonresidentcountry = $('#dinonresidentpcountry_'+getcounter+'_chosen span').text();
var dinonresidentgender = $("input[name='dinonresidentpgender[]']:checked").val();
var dinonresidentdob = document.getElementById("dinonresidentpdob_"+getcounter).value;
var dinonresidentdateexpiry = document.getElementById("dinonresidentpdateexpiry_"+getcounter).value;
//var dinonresidentplaceofBirth = $('#dinonresidentplaceofBirth_'+getcounter+'_chosen span').text();

//alert(disurname);

localStorage.setItem("dinonresidentpopupsurname_"+getcounter,dinonresidentsurname);
localStorage.setItem("dinonresidentpopupgivenName_"+getcounter,dinonresidentgivenname);
localStorage.setItem("dinonresidentpopuppassportno_"+getcounter,dinonresidentpassportno);
localStorage.setItem("dinonresidentpopupnationality_"+getcounter,dinonresidentnationality);
localStorage.setItem("dinonresidentpopupcountry_"+getcounter,dinonresidentcountry);
localStorage.setItem("dinonresidentpopupgender_"+getcounter,dinonresidentgender);
//localStorage.setItem("dinonresidentpopupdatetimepicker_"+getcounter,dinonresidentdatetimepicker);
localStorage.setItem("dinonresidentpopupdateexpiry_"+getcounter,dinonresidentdateexpiry);
localStorage.setItem("dinonresidentpopupdob_"+getcounter,dinonresidentdob);
//localStorage.setItem("dinonresidentpopupplaceofBirth_"+getcounter,dinonresidentplaceofBirth);

	if(dinonresidentsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(dinonresidentgivenname == ''){

           //alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
           $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(dinonresidentpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(dinonresidentnationality == ''){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(dinonresidentcountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(dinonresidentgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");
	}        
        else if(dinonresidentdob == ''){
           
              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}
        else if(dinonresidentdateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}             
	else{
	   //alert("Saved Succesfully");
	   $('#dinonresidentpreviewpassport_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}
}

if(getresidencystatus == 'dinricfront'){

var dipreviewfrontnricnumber = document.getElementById("dipreviewfrontnricnumber_"+getcounter).value;
var dipreviewnricfrontgender = $("input[name='dipreviewnricfrontgender[]']:checked").val();
var dipreviewnricfrontdob = document.getElementById("dipreviewnricfrontdob_"+getcounter).value;
var dipreviewnricfrontnationality = $('#dipreviewnricfrontnationality_'+getcounter+'_chosen span').text();
var dipreviewnricfrontpostcode = document.getElementById("dipreviewnricfrontpostcode_"+getcounter).value;
var dipreviewnricfrontstreetname = document.getElementById("dipreviewnricfrontstreetname_"+getcounter).value;
var dipreviewnricfrontfloor = document.getElementById("dipreviewnricfrontfloor_"+getcounter).value;
var dipreviewnricfrontunit = document.getElementById("dipreviewnricfrontunit_"+getcounter).value;

localStorage.setItem("dipreviewpopupfrontnricnumber_"+getcounter,dipreviewfrontnricnumber);
localStorage.setItem("dipreviewpopupnricfrontgender_"+getcounter,dipreviewnricfrontgender);
localStorage.setItem("dipreviewpopupnricfrontdob_"+getcounter,dipreviewnricfrontdob);
localStorage.setItem("dipreviewpopupnricfrontnationality_"+getcounter,dipreviewnricfrontnationality);
localStorage.setItem("dipreviewpopupnricfrontpostcode_"+getcounter,dipreviewnricfrontpostcode);
localStorage.setItem("dipreviewpopupnricfrontstreetname_"+getcounter,dipreviewnricfrontstreetname);
localStorage.setItem("dipreviewpopupnricfrontfloor_"+getcounter,dipreviewnricfrontfloor);
localStorage.setItem("dipreviewnrpopupicfrontunit_"+getcounter,dipreviewnricfrontunit);


        if(dipreviewfrontnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(dipreviewnricfrontgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show");

	}
	else if(dipreviewnricfrontdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(dipreviewnricfrontnationality == ''){

              //alert("Nationality cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}
        else if(dipreviewnricfrontpostcode == ''){

              //alert("Postcode cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
              $("#displaysuccessmsg").modal("show");
	}
        else if(dipreviewnricfrontstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
              $("#displaysuccessmsg").modal("show");
	}
        else if(dipreviewnricfrontfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(dipreviewnricfrontunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#dinricfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}


}

if(getresidencystatus == 'dinricback'){

var dipreviewbacknricnumber = document.getElementById("dipreviewbacknricnumber_"+getcounter).value;
var dipreviewnricbackgender = $("input[name='dipreviewnricbackgender[]']:checked").val();
var dipreviewnricbackdob = document.getElementById("dipreviewnricbackdob_"+getcounter).value;
var dipreviewnricbacknationality = $('#dipreviewnricbacknationality_'+getcounter+'_chosen span').text();
var dipreviewnricbackpostcode = document.getElementById("dipreviewnricbackpostcode_"+getcounter).value;
var dipreviewnricbackstreetname = document.getElementById("dipreviewnricbackstreetname_"+getcounter).value;
var dipreviewnricbackfloor = document.getElementById("dipreviewnricbackfloor_"+getcounter).value;
var dipreviewnricbackunit = document.getElementById("dipreviewnricbackunit_"+getcounter).value;

localStorage.setItem("dipreviewpopupbacknricnumber_"+getcounter,dipreviewbacknricnumber);
localStorage.setItem("dipreviewpopupnricbackgender_"+getcounter,dipreviewnricbackgender);
localStorage.setItem("dipreviewpopupnricbackdob_"+getcounter,dipreviewnricbackdob);
localStorage.setItem("dipreviewpopupnricbacknationality_"+getcounter,dipreviewnricbacknationality);
localStorage.setItem("dipreviewpopupnricbackpostcode_"+getcounter,dipreviewnricbackpostcode);
localStorage.setItem("dipreviewpopupnricbackstreetname_"+getcounter,dipreviewnricbackstreetname);
localStorage.setItem("dipreviewpopupnricbackfloor_"+getcounter,dipreviewnricbackfloor);
localStorage.setItem("dipreviewnrpopupicbackunit_"+getcounter,dipreviewnricbackunit);

        if(dipreviewbacknricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(dipreviewnricbackgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show");

	}
	else if(dipreviewnricbackdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(dipreviewnricbacknationality == ''){

              //alert("Nationality cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}
        else if(dipreviewnricbackpostcode == ''){

              //alert("Postcode cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
              $("#displaysuccessmsg").modal("show");
	}
        else if(dipreviewnricbackstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
              $("#displaysuccessmsg").modal("show");
	}
        else if(dipreviewnricbackfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(dipreviewnricbackunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#dinricbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}


}
if(getresidencystatus == 'difinpassport'){


var difincardsurname = document.getElementById("difincardsurname_"+getcounter).value;
var difincardgivenname = document.getElementById("difincardgivenName_"+getcounter).value;
var difincardpassportno = document.getElementById("difincardppassportNumber_"+getcounter).value;
var difincardnationality = $('#difincardpnationality_'+getcounter+'_chosen span').text();
var difincardcountry = $('#difincardpcountry_'+getcounter+'_chosen span').text();
var difincardgender = $("input[name='difincardgender[]']:checked").val();
var difincarddatetimepicker = document.getElementById("difincarddatetimepicker_"+getcounter).value;
var difincarddateexpiry = document.getElementById("difincardpdateexpiry_"+getcounter).value;
var difincardplaceofBirth = $('#difincardplaceofBirth_'+getcounter+'_chosen span').text();
var difincarddob = document.getElementById("difincarddob_"+getcounter).value;
//alert(difincardnationality);return false;

//alert(disurname);

localStorage.setItem("difincardpopupsurname_"+getcounter,difincardsurname);
localStorage.setItem("difincardpopupgivenName_"+getcounter,difincardgivenname);
localStorage.setItem("difincardpopuppassportno_"+getcounter,difincardpassportno);
localStorage.setItem("difincardpopupnationality_"+getcounter,difincardnationality);
localStorage.setItem("difincardpopupcountry_"+getcounter,difincardcountry);
localStorage.setItem("difincardpopupgender_"+getcounter,difincardgender);
localStorage.setItem("difincardpopupdatetimepicker_"+getcounter,difincarddatetimepicker);
localStorage.setItem("difincardpopupdateexpiry_"+getcounter,difincarddateexpiry);
localStorage.setItem("difincardpopupplaceofBirth_"+getcounter,difincardplaceofBirth);
localStorage.setItem("difincardpopupdob_"+getcounter,difincarddob);

        if(difincardsurname == ''){
           //alert("Surname cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
	      $("#displaysuccessmsg").modal("show");  
	}
	else if(difincardgivenname == ''){

           //alert("Given name cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 

	}
	else if(difincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 

	}
	else if(difincardnationality == ''){

            //alert("Nationality cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder NRIC - Preview");
	      $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 

	}
        else if(difincardcountry == ''){

            //alert("Country cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 
	}
        else if(difincardgender == ''){

             //alert("Please select any one of the gender");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
	      $("#displaysuccessmsg").modal("show"); 
	}
        else if(difincarddatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 
	}
        else if(difincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 

	} 
        else if(difincardplaceofBirth == ''){

	      //alert("Place of Birth cannot be empty");
	      $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
	      $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");  
	}    
	else{
	   //alert("Saved Succesfully");
	   $('#difincardpreviewpassport_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder Passport - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}

}

if(getresidencystatus == 'difincardfront'){

var difincardfrontemployer = document.getElementById("difincardfrontemployer_"+getcounter).value;
var difincardfrontoccupation = document.getElementById("difincardfrontoccupation_"+getcounter).value;
var difincardfrontdateissue = document.getElementById("difincardfrontdateissue_"+getcounter).value;
var difincardfrontdateexpiry = document.getElementById("difincardfrontdateexpiry_"+getcounter).value;
var diwfincardfrontNumber = document.getElementById("diwfincardfrontNumber_"+getcounter).value;

localStorage.setItem("difincardfrontemployer_"+getcounter,difincardfrontemployer);
localStorage.setItem("difincardfrontoccupation_"+getcounter,difincardfrontoccupation);
localStorage.setItem("difincardfrontdateissue_"+getcounter,difincardfrontdateissue);
localStorage.setItem("difincardfrontdateexpiry_"+getcounter,difincardfrontdateexpiry);
localStorage.setItem("diwfincardfrontNumber_"+getcounter,diwfincardfrontNumber);

        if(difincardfrontemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN Front - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(difincardfrontoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN Front - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(difincardfrontdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN Front - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(difincardfrontdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN Front - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
        else if(diwfincardfrontNumber == ''){

            alert("Fin Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN Front - Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#difincardfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN Front - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}

}

if(getresidencystatus == 'difincardback'){

var difincardbackemployer = document.getElementById("difincardbackemployer_"+getcounter).value;
var difincardbackoccupation = document.getElementById("difincardbackoccupation_"+getcounter).value;
var difincardbackdateissue = document.getElementById("difincardbackdateissue_"+getcounter).value;
var difincardbackdateexpiry = document.getElementById("difincardbackdateexpiry_"+getcounter).value;
var diwfincardbackNumber = document.getElementById("diwfincardbackNumber_"+getcounter).value;

localStorage.setItem("difincardbackemployer_"+getcounter,difincardbackemployer);
localStorage.setItem("difincardbackoccupation_"+getcounter,difincardbackoccupation);
localStorage.setItem("difincardbackdateissue_"+getcounter,difincardbackdateissue);
localStorage.setItem("difincardbackdateexpiry_"+getcounter,difincardbackdateexpiry);
localStorage.setItem("diwfincardbackNumber_"+getcounter,diwfincardbackNumber);

        if(difincardbackemployer == ''){

           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(difincardbackoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(difincardbackdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(difincardbackdateexpiry == ''){

            //alert("Date of Expiry cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
        else if(diwfincardbackNumber == ''){

            //alert("Fin Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#difincardbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Director and Shareholder FIN back - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show");
	}

}

}
</script>
