<script>
function formatingdata(getdatavalue){
  	
  var text = getdatavalue;
  var comp = text.indexOf('/') !== -1 ? text.split('/') : text.split('-');
  var m = parseInt(comp[1], 10);
  var d = parseInt(comp[0], 10);
  var y = parseInt(comp[2], 10);
  var date = new Date(y, m - 1, d);
  if ((date.getFullYear() == y || date.getFullYear().toString().substr(-2)) && date.getMonth() + 1 == m && date.getDate() == d) {
    console.log(new Date(date), ' is Valid date');
    return false;
  } else {
    console.log(date, ' is Invalid date');
    return true;
  }

}

function checkfuturedata(fetchdata){


var cur_date = new Date();

var previewdob = fetchdata;
var comp = previewdob.indexOf('/') !== -1 ? previewdob.split('/') : previewdob.split('-');
var d = parseInt(comp[0], 10);
var m = parseInt(comp[1], 10);
var y = parseInt(comp[2], 10);

var cur_year = cur_date.getFullYear();
var cy = (""+cur_year).substr(-2);

y = (y > parseInt(cy)) ? "19"+y : "20"+y;

var dob =  new Date(y, m-1, d);

if(dob >= cur_date){    
     return true;    
}else{
    return false;
}


}
</script>

<script type="text/javascript">

function isValidNumber(num, role, formNo, component){
	if(isNaN(num)){
		showNumbersInvalid(role, formNo, component, true);
		return false;
	}
	return true;
}
function showNumbersInvalid(role, formNo, component, show){
	var alert;
	var alertMsg;

	var roleName = "";
	var compName = "";
	
	if(role == ''){
		if(component == "TOT_PAID_UP_CAPITAL"){
			alert = $('#paidUpCapital_msg');
		}
		if(component == "TOT_NUMBER_OF_SHARES"){
			alert = $('#numberofShares_msg');
		}
	}else{	
		if(component == "PAID_UP_CAPITAL"){
			compName = "paid_up_capital";
		}else if(component == "NUMBER_OF_SHARES"){
			compName = "number_of_shares";
		}

		if(role == ROLE_DIRECTOR){
			roleName = "director";		
		}else if(role == ROLE_SHAREHOLDER){
			roleName = "individual_shareholder";
		}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
			roleName = "director_shareholder";
		}else if(role == ROLE_CORPORATE_SHAREHOLDER){
			roleName = "corporate";
		}
		if(show == true){
			alert = $('#'+roleName+'_'+compName+'_msg_'+ formNo);
			console.log("ID ============> "+'#'+roleName+'_'+compName+'_msg_'+ formNo);
		}else{
			alert = $('#'+formNo);
			console.log("ID ============> "+'#'+formNo);
		}
		
	}

	if (show == true) {
		alert.text(ERR_MSG_INVALID_NUMBER);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function addEvents(){
	$("#paidUpCapital").on('change keyup paste', function(e){
		
		showNumbersInvalid('', '', 'TOT_PAID_UP_CAPITAL', false);	
		
	});
	$("#numberofShares").on('change keyup paste', function(e){
		
		showNumbersInvalid('', '', 'TOT_NUMBER_OF_SHARES', false);	
		
	});	
	var counter = localStorage.getItem("Director_COUNT");
	var counter1 = localStorage.getItem("Shareholder_COUNT");
	var counter2 = localStorage.getItem("Director and Shareholder_COUNT");
	var counter3 = localStorage.getItem("Corporate Shareholder_COUNT");
	var paidUpCapitalId = "";
	var numberOfSharesId = "";
	var i = 0;
	for (i = 1; i <= counter; i++){
		paidUpCapitalId = getID(ROLE_DIRECTOR, "PAID_UP_CAPITAL", i);
		numberOfSharesId = getID(ROLE_DIRECTOR, "NUMBER_OF_SHARES", i);

		$(paidUpCapitalId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id);
				console.log("ID: "+id);
				showNumbersInvalid(ROLE_DIRECTOR, id, 'PAID_UP_CAPITAL', false);	
			
		});

		$(numberOfSharesId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id);
				console.log("ID: "+id);
				showNumbersInvalid(ROLE_DIRECTOR, id, 'NUMBER_OF_SHARES', false);	
			
		});	
	}
	for (i = 1; i <= counter1; i++){
		paidUpCapitalId = getID(ROLE_SHAREHOLDER, "PAID_UP_CAPITAL", i);
		numberOfSharesId = getID(ROLE_SHAREHOLDER, "NUMBER_OF_SHARES", i);

		$(paidUpCapitalId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id);
				showNumbersInvalid(ROLE_SHAREHOLDER, id, 'PAID_UP_CAPITAL', false);	
			
		});
		$(numberOfSharesId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id);
				showNumbersInvalid(ROLE_SHAREHOLDER, id, 'NUMBER_OF_SHARES', false);	
			
		});	
	}
	for (i = 1; i <= counter2; i++){
		paidUpCapitalId = getID(ROLE_DIRECTOR_SHAREHOLDER, "PAID_UP_CAPITAL", i);
		numberOfSharesId = getID(ROLE_DIRECTOR_SHAREHOLDER, "NUMBER_OF_SHARES", i);

		$(paidUpCapitalId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id);
				showNumbersInvalid(ROLE_DIRECTOR_SHAREHOLDER, id, 'PAID_UP_CAPITAL', false);	
			
		});
		$(numberOfSharesId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id);
				showNumbersInvalid(ROLE_DIRECTOR_SHAREHOLDER, id, 'NUMBER_OF_SHARES', false);	
			
		});	
	}
	var counter3 = localStorage.getItem("Corporate Shareholder_COUNT");
	for (i = 1; i <= counter3; i++){
		paidUpCapitalId = getID(ROLE_CORPORATE_SHAREHOLDER, "PAID_UP_CAPITAL", i);
		numberOfSharesId = getID(ROLE_CORPORATE_SHAREHOLDER, "NUMBER_OF_SHARES", i);

		$(paidUpCapitalId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id);
				showNumbersInvalid(ROLE_CORPORATE_SHAREHOLDER, id, 'PAID_UP_CAPITAL', false);	
			
		});
		$(numberOfSharesId).on('change keyup paste', function(e){
				var id = $(this).attr("id");
				id = getTriggeredId(id); 
				showNumbersInvalid(ROLE_CORPORATE_SHAREHOLDER, id, 'NUMBER_OF_SHARES', false);	
			
		});	
	}		
}
function getTriggeredId(id){
	var fno = id.substr(id.length-1);
	var bId = id.substr(0, id.length-1);
	id = bId+"msg_"+fno;
	return id; 
}
function getID(role, component, formNo){
	if(component == "PAID_UP_CAPITAL"){
			compName = "paid_up_capital";
		}else if(component == "NUMBER_OF_SHARES"){
			compName = "number_of_shares";
		}

		if(role == ROLE_DIRECTOR){
			roleName = "director";		
		}else if(role == ROLE_SHAREHOLDER){
			roleName = "individual_shareholder";
		}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
			roleName = "director_shareholder";
		}else if(role == ROLE_CORPORATE_SHAREHOLDER){
			roleName = "corporate";
		}

		ID = '#'+roleName+'_'+compName+'_'+ formNo;
		return ID;
}

</script>
