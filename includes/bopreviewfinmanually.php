<script>
function bofinsavemanually(getcounter,getboname){

        if( $('#bofincardmanually_'+getcounter+"_"+getboname).hasClass('modal fade show') ) {
	    console.log("Modal is open");
            $('body').css("overflow","hidden");
            $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","hidden");
            $('#bofincardmanually_'+getcounter+"_"+getboname).css("overflow","auto");
	}

var bofincardpassportno = document.getElementById("bofincardpassportNumber_"+getcounter+"_"+getboname).value;
var bofincardnationality = $('#bofincardnationality_'+getcounter+'_'+getboname+'_chosen span').text();
var bofincardcountry = $('#bofincardcountry_'+getcounter+'_'+getboname+'_chosen span').text();
var bofincardgender = $("input[name='bofincardgender[]']:checked").val();
var bofincarddatetimepicker = document.getElementById("bofincarddateissue_"+getcounter+"_"+getboname).value;
var bofincarddateexpiry = document.getElementById("bofincarddateexpiry_"+getcounter+"_"+getboname).value;
var bofincardplaceofBirth = $('#bofincarddateplace_'+getcounter+'_'+getboname+'_chosen span').text();
var bofincarddateBirth = document.getElementById("bofincarddatebirth_"+getcounter+"_"+getboname).value;

var bofincardemployer = document.getElementById("bofincardemployer_"+getcounter+"_"+getboname).value;
var bofincardoccupation = document.getElementById("bofincardoccupation_"+getcounter+"_"+getboname).value;
var boworkpassdateissue = document.getElementById("boworkpassdateissue_"+getcounter+"_"+getboname).value;
var boworkpassdateexpiry = document.getElementById("boworkpassdateexpiry_"+getcounter+"_"+getboname).value;
var bowfincardNumber = document.getElementById("bowfincardNumber_"+getcounter+"_"+getboname).value;

//var startdate = new Date(bofincarddatetimepicker.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(bofincarddateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

//var startdate1 = new Date(boworkpassdateissue.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate1 =  new Date(boworkpassdateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var getbonondatetimepicker = formatingdata(bofincarddatetimepicker);
var getbonondateexpiry = formatingdata(bofincarddateexpiry);
var getboworkpassdateissue = formatingdata(boworkpassdateissue);
var getboworkpassdateexpiry = formatingdata(boworkpassdateexpiry);
var getbofincarddateBirth = formatingdata(bofincarddateBirth);

//var cur_date = new Date();
//var dob =  new Date(bofincarddateBirth.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var cur_date = new Date();
var dob = $("#bofincarddatebirth_"+getcounter+"_"+getboname).datepicker('getDate');
var startdate = $("#bofincarddateissue_"+getcounter+"_"+getboname).datepicker('getDate');
var enddate =  $("#bofincarddateexpiry_"+getcounter+"_"+getboname).datepicker('getDate');
var startdate1 = $("#boworkpassdateissue_"+getcounter+"_"+getboname).datepicker('getDate');
var enddate1 = $("#boworkpassdateexpiry_"+getcounter+"_"+getboname).datepicker('getDate');

localStorage.setItem("bofincardmanuallypassportno_"+getcounter+"_"+getboname,bofincardpassportno);
localStorage.setItem("bofincardmanuallynationality_"+getcounter+"_"+getboname,bofincardnationality);
localStorage.setItem("bofincardmanuallycountry_"+getcounter+"_"+getboname,bofincardcountry);
localStorage.setItem("bofincardmanuallygender_"+getcounter+"_"+getboname,bofincardgender);
localStorage.setItem("bofincardmanuallydatetimepicker_"+getcounter+"_"+getboname,bofincarddatetimepicker);
localStorage.setItem("bofincardmanuallydateexpiry_"+getcounter+"_"+getboname,bofincarddateexpiry);
localStorage.setItem("bofincardmanuallyplaceofBirth_"+getcounter+"_"+getboname,bofincardplaceofBirth);
localStorage.setItem("bofincardmanuallydateBirth_"+getcounter+"_"+getboname,bofincarddateBirth);

localStorage.setItem("bofincardmanuallyemployer_"+getcounter+"_"+getboname,bofincardemployer);
localStorage.setItem("bofincardmanuallyoccupation_"+getcounter+"_"+getboname,bofincardoccupation);
localStorage.setItem("bofincardmanuallyworkpassdateissue_"+getcounter+"_"+getboname,boworkpassdateissue);
localStorage.setItem("bofincardmanuallyworkpassdateexpiry_"+getcounter+"_"+getboname,boworkpassdateexpiry);
localStorage.setItem("bofincardmanuallywfincardnumber_"+getcounter+"_"+getboname,bowfincardNumber);


        if(bofincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
	else if(bofincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
        else if(bofincardcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(bofincardgender == 'undefined'){

             //alert("Please select any one of the gender");
            $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(bofincarddatetimepicker == ''){

            //alert("Date of Issue cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show");   
	}else if(bofincarddatetimepicker != '' && getbonondatetimepicker == true){

            //alert("Date of Issue cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Invalid Date of Issue");
            $("#displaysuccessmsg").modal("show");   
        }
        else if((bofincarddatetimepicker != '') && (bofincarddatetimepicker.length < 10)){
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bofincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show"); 

	}else if(bofincarddateexpiry != '' && getbonondateexpiry == true){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Invalid Date of Expiry");
              $("#displaysuccessmsg").modal("show"); 

        } else if((bofincarddateexpiry != '') && (bofincarddateexpiry.length < 10)){
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bofincardplaceofBirth == 'Please select'){

               //alert("Place of Birth cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
               $("#displaysuccessmsg").modal("show");  
	}
        else if(bofincarddateBirth == ''){

              //alert("Date of Birth cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
               $("#displaysuccessmsg").modal("show");   
	} 
        else if(bofincarddateBirth != '' && getbofincarddateBirth == true){

              //alert("Date of Birth cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Invalid Date of Birth");
               $("#displaysuccessmsg").modal("show");   
	}
        else if(bofincarddateBirth.length < 10){
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bofincardemployer == ''){

              //alert("Employer cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(bofincardoccupation == ''){

              //alert("Occupation cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
               $("#displaysuccessmsg").modal("show");    
	}
       else if(boworkpassdateissue != '' && getboworkpassdateissue == true){

              //alert("Date of Issue cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
               $("#displaysuccessmsg").modal("show");  
	}
       else if(boworkpassdateexpiry != '' && getboworkpassdateexpiry == true){

              //alert("Date of Expiry cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
               $("#displaysuccessmsg").modal("show");  
	}else if((boworkpassdateissue != '') && (boworkpassdateissue.length < 10)){
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((boworkpassdateexpiry != '') && (boworkpassdateexpiry.length < 10)){
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(bowfincardNumber == ''){

              //alert("Fin Number cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
              $("#displaysuccessmsg").modal("show");    
	}
        else if(startdate >= enddate){
             $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((boworkpassdateissue != '') && (boworkpassdateexpiry != '') && (startdate1 >= enddate1)){
             $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        } 
         else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Bo Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        }   
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_BO_FIN_PASSPORT, true);
	   //showFinPassportEmpty(ROLE_bo, getcounter, false);
	   //showFINCardEmpty(ROLE_bo, getcounter, true, false);
	   //showFINCardEmpty(ROLE_bo, getcounter, false, false);           
	   $('#bofincardmanually_'+getcounter+"_"+getboname).modal('hide');
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $('#modalbeneficialowner_'+getcounter+"_"+getboname).css("overflow","auto"); 
           $('#displaysuccessmsg').modal('show');
	}

}

</script>
