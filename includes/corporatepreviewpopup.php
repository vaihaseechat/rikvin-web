<script>
var parsecount = 20;
function corporatenonuploadpassportfile(getdata,getcounter,filename){

$("#corporateuploadpassportfileloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
//alert(filename);

        $.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error; 


                         if(Errorstatus == 'Poor Image Quality') {                           
                           
                            $("#displaysuccessmsg .modal-title").html("Corporate - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
                           $("#bcorporatenonresidentpreviewpassport_"+getcounter).css("display", "none");
                           $("#corporateuploadpassportfileloadingImage_"+getcounter).css("display", "none");
                           $("#corporatenonresidentlabel_"+getcounter).text();

                         } else{
                            //alert("fdfdfdf");
                            $("#bcorporatenonresidentpreviewpassport_"+getcounter).css("display", "block");
                            $("#corporateuploadpassportfileloadingImage_"+getcounter).css("display", "none");
                            $("#corporatenonresidentlabel_"+getcounter).text(finallabel);


                            var fieldHTML ='<div class="modal fade" id="corporatenonresidentpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fieldHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fieldHTML += '<div class="modal-content row">';
				fieldHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - Non Resident - Corporate #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fieldHTML += '<div class="col-lg-12 background-grey-2">';
				fieldHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="corporatenonresidentpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="corporatenonresidentsurname[]" class="form-control" id="corporatenonresidentsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="corporatenonresidentgivenName[]" class="form-control" id="corporatenonresidentgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="corporatenonresidentpassportNumber[]" class="form-control" id="corporatenonresidentpassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="corporatenonresidentnationality[]" id="corporatenonresidentnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatenonresidentcountry_'+getcounter+'" name="corporatenonresidentcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="corporatenonresidentmgender_'+getcounter+'" name="corporatenonresidentpgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="corporatenonresidentpgender[]" id="corporatenonresidentfgender_'+getcounter+'" class="radio-red"> Female</label></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatenonresidentpdatetimepicker_'+getcounter+'" class="form-control" name="corporatenonresidentdatetimepicker[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatenonresidentpdateexpiry_'+getcounter+'" class="form-control" name="corporatenonresidentdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 fieldHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatenonresidentpdob_'+getcounter+'" class="form-control" name="corporatenonresidentpdob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatenonresidentplaceofBirth_'+getcounter+'" name="corporatenonresidentplaceofBirth[]"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'corporatenonresident\');">Save</a></div></div></div>';
				fieldHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fieldHTML);

                                 //Passport Preview - Non resident

				$('#corporatenonresidentnationality_'+getcounter).html(countriesOption);
				$('#corporatenonresidentnationality_'+getcounter).chosen(); 

				$('#corporatenonresidentcountry_'+getcounter).html(countriesOption);
				$('#corporatenonresidentcountry_'+getcounter).chosen(); 

				$('#corporatenonresidentplaceofBirth_'+getcounter).html(countriesOption);
				$('#corporatenonresidentplaceofBirth_'+getcounter).chosen(); 

                                 // Declaring variables for preview
                                $('#corporatenonresidentpdob_'+getcounter).datepicker({   
		                     beforeShow:function () {
		                     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		                     }
	                         });

				$('#corporatenonresidentpdateexpiry_'+getcounter).datepicker({   
					beforeShow:function () {
					setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
					}
				});

                                $('#corporatenonresidentpdatetimepicker_'+getcounter).datepicker({   
					beforeShow:function () {
					setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
					}
				});
                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue =  obj.CountryofIssue;   
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = ''; 
                                 var PlaceofBirth = '';                 
		                 

                                 // Displaying values from OCR - API

                                 $('#corporatenonresidentgivenName_'+getcounter).val(gname);
                                 $('#corporatenonresidentsurname_'+getcounter).val(Surname);
                                 $('#corporatenonresidentpassportNumber_'+getcounter).val(passportno);
                                 $('#corporatenonresidentpdatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#corporatenonresidentpdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#corporatenonresidentpdob_'+getcounter).val(DateofBirth);                       
                                 if(sex == 'Male'){
                                  $('#corporatenonresidentmgender_'+getcounter).prop('checked', true);
                                  $('#corporatenonresidentfgender_'+getcounter).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#corporatenonresidentfgender_'+getcounter).prop('checked', true);
                                  $('#corporatenonresidentmgender_'+getcounter).prop('checked', false);
                                 }
                                 $('#corporatenonresidentnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#corporatenonresidentcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 $('#corporatenonresidentplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth); 
                                 $('#corporatenonresidentpreviewpassport_'+getcounter).modal('show');

                         

                         }     

                    }

        }); 
 
}

//NRIC front

function corporatenricfront(getdata,getcounter,filename){

$("#corporateuploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

	$.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Corporate - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bcorporatenricfrontpreview_"+getcounter).css("display", "none");
                    $("#corporateuploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#corporatenricfrontlabel_"+getcounter).text();

		 } else{

                   $("#bcorporatenricfrontpreview_"+getcounter).css("display", "block");
                   $("#corporateuploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#corporatenricfrontlabel_"+getcounter).text(finallabel);  

                   var nricfrontfield = '<div class="modal fade" id="corporatenricfrontpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfrontfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfrontfield += '<div class="modal-content row">';
                     nricfrontfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Front Information - Corporate#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfrontfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="corporatenricfrontimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="corporatepreviewfrontnricnumber[]" class="form-control" id="corporatepreviewfrontnricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="corporatepreviewnricfrontmgender_'+getcounter+'" name="corporatepreviewnricfrontgender[]" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input id="corporatepreviewnricfrontfgender_'+getcounter+'" name="corporatepreviewnricfrontgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfrontfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="corporatepreviewnricfrontdob_'+getcounter+'" class="form-control" name="corporatepreviewnricfrontdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="corporatepreviewnricfrontnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatepreviewnricfrontnationality_'+getcounter+'"></select></div></div></div></div>';

                     nricfrontfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatepreviewnricfrontpostcode_'+getcounter+'" name="corporatepreviewnricfrontpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="corporatepreviewnricfrontstreetname[]" id="corporatepreviewnricfrontstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatepreviewnricfrontfloor_'+getcounter+'" name="corporatepreviewnricfrontfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatepreviewnricfrontunit_'+getcounter+'" name="corporatepreviewnricfrontunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfrontfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'corporatenricfront\');">Save</a></div></div></div>';

                     nricfrontfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfrontfield);

                     $('#corporatepreviewnricfrontnationality_'+getcounter).html(countriesOption);
                     $('#corporatepreviewnricfrontnationality_'+getcounter).chosen(); 
                    
                     $('#corporatepreviewnricfrontdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      });  
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         if(sex == 'Male'){
                          $('#corporatepreviewnricfrontmgender_'+getcounter).prop('checked', true);
                          $('#corporatepreviewnricfrontfgender_'+getcounter).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#corporatepreviewnricfrontfgender_'+getcounter).prop('checked', true);
                          $('#corporatepreviewnricfrontmgender_'+getcounter).prop('checked', false);
                         }

                         $('#corporatepreviewfrontnricnumber_'+getcounter).val(nricno);
                         $('#corporatepreviewnricfrontnationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#corporatepreviewnricfrontdob_'+getcounter).val(dobs);
                         $('#corporatenricfrontpreview_'+getcounter).modal('show');
                      

		 }
	    }
	}); 
}

//NRIC back

function corporatenricback(getdata,getcounter,filename){

$("#corporateuploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Corporate - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
                    $("#bcorporatenricbackpreview_"+getcounter).css("display", "none");
                    $("#corporateuploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#corporatenricbacklabel_"+getcounter).text();

		 } else{

                   $("#bcorporatenricbackpreview_"+getcounter).css("display", "block"); 
                   $("#corporateuploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#corporatenricbacklabel_"+getcounter).text(finallabel);

                   var nricbackfield = '<div class="modal fade" id="corporatenricbackpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricbackfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricbackfield += '<div class="modal-content row">';
                     nricbackfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Back Information - Corporate#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricbackfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="corporatenricbackimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="corporatepreviewbacknricnumber[]" class="form-control" id="corporatepreviewbacknricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="corporatepreviewnricbackmgender_'+getcounter+'" name="corporatepreviewnricbackgender[]" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input id="corporatepreviewnricbackfgender_'+getcounter+'" name="corporatepreviewnricbackgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricbackfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="corporatepreviewnricbackdob_'+getcounter+'" class="form-control" name="corporatepreviewnricbackdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="corporatepreviewnricbacknationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatepreviewnricbacknationality_'+getcounter+'"></select></div></div></div></div>';

                     nricbackfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatepreviewnricbackpostcode_'+getcounter+'" name="corporatepreviewnricbackpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="corporatepreviewnricbackstreetname[]" id="corporatepreviewnricbackstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatepreviewnricbackfloor_'+getcounter+'" name="corporatepreviewnricbackfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="corporatepreviewnricbackunit_'+getcounter+'" name="corporatepreviewnricbackunit[]" placeholder="Unit" type="text"></div></div>';

                     nricbackfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'corporatenricback\');">Save</a></div></div></div>';

                     nricbackfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricbackfield);

                     $('#corporatepreviewnricbacknationality_'+getcounter).html(countriesOption);
                     $('#corporatepreviewnricbacknationality_'+getcounter).chosen();   

                      $('#corporatepreviewnricbackdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      });
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         if(sex == 'Male'){
                          $('#corporatepreviewnricbackmgender_'+getcounter).prop('checked', true);
                          $('#corporatepreviewnricbackfgender_'+getcounter).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#corporatepreviewnricbackfgender_'+getcounter).prop('checked', true);
                          $('#corporatepreviewnricbackmgender_'+getcounter).prop('checked', false);
                         }

                         $('#corporatepreviewbacknricnumber_'+getcounter).val(nricno);
                         $('#corporatepreviewnricbacknationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#corporatepreviewnricbackdob_'+getcounter).val(dobs);
                         $('#corporatenricbackpreview_'+getcounter).modal('hide');
 
		 }
	    }
   }); 

}

//FIn Passport

function corporatefinuploadpassportfile(getdata,getcounter,filename){

$("#corporateuploadPassportloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/passport/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Corporate - Error");
			    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
			    $("#displaysuccessmsg").modal("show");
                    $("#bcorporatefincardpreviewpassport_"+getcounter).css("display", "none");
                    $("#corporateuploadPassportloadingImage_"+getcounter).css("display", "none"); 
                    $("#corporatefinpassportlabel_"+getcounter).text();

		 } else{

                   $("#bcorporatefincardpreviewpassport_"+getcounter).css("display", "block"); 
                   $("#corporateuploadPassportloadingImage_"+getcounter).css("display", "none");
                   $("#corporatefinpassportlabel_"+getcounter).text(finallabel);


                    var fincardHTML ='<div class="modal fade" id="corporatefincardpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fincardHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fincardHTML += '<div class="modal-content row">';
				fincardHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - WorkPass - Corporate #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fincardHTML += '<div class="col-lg-12 background-grey-2">';
				fincardHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="corporatefincardpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="corporatefincardsurname[]" class="form-control" id="corporatefincardsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="corporatefincardgivenName[]" class="form-control" id="corporatefincardgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="corporatefincardppassportNumber[]" class="form-control" id="corporatefincardppassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="corporatefincardpnationality[]" id="corporatefincardpnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatefincardpcountry_'+getcounter+'" name="corporatefincardpcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="corporatefincardmgender_'+getcounter+'" name="corporatefincardgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="corporatefincardgender[]" id="corporatefincardfgender_'+getcounter+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatefincardpdatetimepicker_'+getcounter+'" class="form-control" name="corporatefincardpdatetimepicker[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatefincardpdateexpiry_'+getcounter+'" class="form-control" name="corporatefincardpdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 fincardHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatefincardpdob_'+getcounter+'" class="form-control" name="corporatefincarddob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="corporatefincardplaceofBirth_'+getcounter+'" name="corporatefincardplaceofBirth[]"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'corporatefinpassport\');">Save</a></div></div></div>';
				fincardHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fincardHTML);

                                 //Passport Preview - Work Pass

				$('#corporatefincardpnationality_'+getcounter).html(countriesOption);
				$('#corporatefincardpnationality_'+getcounter).chosen(); 

				$('#corporatefincardpcountry_'+getcounter).html(countriesOption);
				$('#corporatefincardpcountry_'+getcounter).chosen(); 

				$('#corporatefincardplaceofBirth_'+getcounter).html(countriesOption);
				$('#corporatefincardplaceofBirth_'+getcounter).chosen(); 

                                $('#corporatefincardpdob_'+getcounter).datepicker({   
		                     beforeShow:function () {
		                     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		                     }
	                         });

				$('#corporatefincardpdateexpiry_'+getcounter).datepicker({   
					beforeShow:function () {
					setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
					}
				});

                                $('#corporatefincardpdatetimepicker_'+getcounter).datepicker({   
					beforeShow:function () {
					setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
					}
				});

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = ''; 
                                 var PlaceofBirth = '';                 
		                 

                                 // Displaying values from OCR - API

                                 $('#corporatefincardgivenName_'+getcounter).val(gname);
                                 $('#corporatefincardsurname_'+getcounter).val(Surname);
                                 $('#corporatefincardppassportNumber_'+getcounter).val(passportno);
                                 $('#corporatefincardpdatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#corporatefincardpdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#corporatefincardpdob_'+getcounter).val(DateofBirth);                     
                                 if(sex == 'Male'){
                                  $('#corporatefincardmgender_'+getcounter).prop('checked', true);
                                  $('#corporatefincardfgender_'+getcounter).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#corporatefincardfgender_'+getcounter).prop('checked', true);
                                  $('#corporatefincardmgender_'+getcounter).prop('checked', false); 
                                 }
                                 $('#corporatefincardpnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#corporatefincardpcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 $('#corporatefincardplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth); 
                                 $('#corporatefincardpreviewpassport_'+getcounter).modal('show');      
 
		 }
	    }
   }); 

}

// Fin front

function corporatefincardfront(getdata,getcounter,filename){

$("#corporateuploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Corporate - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bcorporatefincardfrontpreview_"+getcounter).css("display", "none"); 
                    $("#corporateuploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#corporatefincardfrontlabel_"+getcounter).text();

		 } else{

                   $("#bcorporatefincardfrontpreview_"+getcounter).css("display", "block"); 
                   $("#corporateuploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#corporatefincardfrontlabel_"+getcounter).text(finallabel);


                var fincardfrontHTML ='<div class="modal fade" id="corporatefincardfrontpreview_'+getcounter+'" tabindex="-1" role="dialog">';
		fincardfrontHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincardfrontHTML += '<div class="modal-content row">';
		fincardfrontHTML += '<div class="modal-header"><h5 class="modal-title" id="corporatefincardfrontpreview">Corporate #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		 fincardfrontHTML += '<div class="col-lg-12 background-grey-2">';

                 fincardfrontHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="corporatefincardfrontpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="corporatefincardfrontemployer[]" class="form-control" id="corporatefincardfrontemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="corporatefincardfrontoccupation[]" class="form-control" id="corporatefincardfrontoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

                  fincardfrontHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatefincardfrontdateissue_'+getcounter+'" class="form-control" name="corporatefincardfrontdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatefincardfrontdateexpiry_'+getcounter+'" class="form-control" name="corporatefincardfrontdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="corporatewfincardfrontNumber[]" class="form-control" id="corporatewfincardfrontNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

                 fincardfrontHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'corporatefincardfront\');">Save</a></div></div></div>';
		fincardfrontHTML += '</div></div></div></div>';   
                   
                 $('body').find('#additionalRole:last').after(fincardfrontHTML);

                 $('#corporatefincardfrontdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 }); 

                         $('#corporatefincardfrontdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#corporatefincardfrontdateissue_'+getcounter).val(DateofIssue);
                         $('#corporatefincardfrontpreview_'+getcounter).modal('show');
 
		 }
	    }
   }); 

}

// Fin back

function corporatefincardback(getdata,getcounter,filename){

$("#corporateuploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("Corporate - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bcorporatefincardbackpreview_"+getcounter).css("display", "none"); 
                    $("#corporateuploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#corporatefincardbacklabel_"+getcounter).text();

		 } else{

                   $("#bcorporatefincardbackpreview_"+getcounter).css("display", "block");  
                   $("#corporateuploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#corporatefincardbacklabel_"+getcounter).text(finallabel);


                   var fincardbackHTML ='<div class="modal fade" id="corporatefincardbackpreview_'+getcounter+'" tabindex="-1" role="dialog">';
			fincardbackHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
			fincardbackHTML += '<div class="modal-content row">';
			fincardbackHTML += '<div class="modal-header"><h5 class="modal-title" id="corporatefincardbackpreview">Corporate #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
			 fincardbackHTML += '<div class="col-lg-12 background-grey-2">';

                         fincardbackHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="corporatefincardbackpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
		     
		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="corporatefincardbackemployer[]" class="form-control" id="corporatefincardbackemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="corporatefincardbackoccupation[]" class="form-control" id="corporatefincardbackoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

		          fincardbackHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

		          fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatefincardbackdateissue_'+getcounter+'" class="form-control" name="corporatefincardbackdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="corporatefincardbackdateexpiry_'+getcounter+'" class="form-control" name="corporatefincardbackdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="corporatewfincardbackNumber[]" class="form-control" id="corporatewfincardbackNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

		         fincardbackHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="storedata('+getcounter+',\'corporatefincardback\');">Save</a></div></div></div>';
			fincardbackHTML += '</div></div></div></div>';   
		           
		         $('body').find('#additionalRole:last').after(fincardbackHTML);

                         $('#corporatefincardbackdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         $('#corporatefincardbackdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#corporatefincardbackdateissue_'+getcounter).val(DateofIssue);
                         $('#corporatefincardbackpreview_'+getcounter).modal('show');
 
		 }
	    }
   }); 

}



</script>
