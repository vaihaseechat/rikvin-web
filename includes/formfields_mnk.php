<script>

var maxGroup = 5;
var counter = 1;
var counter1 = 1;
var counter2 = 1;
var counter3 = 1;

function additionalselector(getvalue){

//alert(getvalue);

var sel = document.getElementById('roleSelector');
var roleoptions = sel.options[sel.selectedIndex].value;

if(roleoptions != ''){
   $('#roleSelector').attr("disabled","disabled");
} else{
   $("#additionalRole").hide(); 
}

	if(getvalue == 'Director') {		
                
		if(counter <= maxGroup){
		    var fieldHTML = '<div id="director_'+counter+'" class="divclass">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Director #'+counter+'</h5></div></div>'; // end of background-grey1

                     fieldHTML += '<div class="col-lg-12 background-grey-2">';
                     fieldHTML += '<div class="form-group row mt-4" id="names_'+counter+'"><div class="col-lg-4"><label class="col-form-label required" for="fullName">Full Name</label></div><div class="col-lg-8"><input name="surname[]" class="form-control" id="surname_'+counter+'" placeholder="" type="text"><div class="fullName'+counter+'-alert" id="fullName'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="fullName'+counter+'-alert-msg"></span></div></div></div>'; // end of fullname 

                     fieldHTML += '<div class="form-group row" id="directorstatus_'+counter+'"><div class="col-lg-4"><label class="col-form-label required" for="residencyStatus">Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="directorresstatus_'+counter+'" name="directorresidencyStatus[]" onchange="residencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="directornonresident_'+counter+'" rel="nonresident">Non-resident</option><option value="directorcitizenpr_'+counter+'" rel="citizenpr">Singapore Citizen / PR</option><option value="directorpassholder_'+counter+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="residencyStatus'+counter+'-alert" id="residencyStatus'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="residencyStatus'+counter+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                     fieldHTML += '<div id="directorpassholder_'+counter+'" class="choose-file" style="display: none;"><div class="row"><div class="col-lg-4"><label class="col-form-label" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directoruploadPassport[]" class="custom-file-input" id="directoruploadPassport_'+counter+'" type="file" onclick="directoruploadpassportfile('+counter+');"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div><div class="directoruploadPassport'+counter+'-alert" id="directoruploadPassport'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directoruploadPassport'+counter+'-alert-msg"></span></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#directorpreviewpassportfinmanually_'+counter+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div><div class="row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directorfincardfront[]" class="custom-file-input" id="directorfincardfront_'+counter+'" type="file" onclick="directorfincardfront('+counter+');"><label class="custom-file-label dashed-lines" for="">FIN Card Front</label></div></div><div class="directorfincardfront'+counter+'-alert" id="directorfincardfront'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directorfincardfront'+counter+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directorfincardback[]" class="custom-file-input" id="directorfincardback_'+counter+'" type="file" onclick="directorfincardback('+counter+');"><label class="custom-file-label dashed-lines" for="">FIN Card Back</label></div></div><div class="directorfincardback'+counter+'-alert" id="directorfincardback'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directorfincardback'+counter+'-alert-msg"></span></div></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directoraddress[]" class="custom-file-input" id="directoraddress_'+counter+'" type="file" onclick="directoraddress('+counter+');"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="directoraddress'+counter+'-alert" id="directoraddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directoraddress'+counter+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="directorresaddress[]" id="directorresaddress_'+counter+'" rows="2" placeholder="Please key in residential address"></textarea><div class="directorresaddress'+counter+'-alert" id="directorresaddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directorresaddress'+counter+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="directorcitizenpr_'+counter+'" class="choose-file" style="display: none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directornricfront[]" class="custom-file-input" id="directornricfront_'+counter+'" type="file" onclick="directornricfront('+counter+');"><label class="custom-file-label dashed-lines" for="">NRIC Front</label></div></div><div class=" nricfront'+counter+'-alert" id="nricfront'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="nricfront'+counter+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directornricback[]" class="custom-file-input" id="directornricback_'+counter+'" type="file" onclick="directornricback('+counter+');"><label class="custom-file-label dashed-lines" for="">NRIC Back</label></div></div><div class="nricback'+counter+'-alert" id="nricback'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="nricback'+counter+'-alert-msg"></span></div></div></div><div class="row"><div class="col-sm-6"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#directorpreviewnricmanually_'+counter+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="directornonresident_'+counter+'" class="choose-file" style="display:none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directornonresidentuploadPassport[]" class="custom-file-input" id="directornonresidentuploadpassport_'+counter+'" type="file" onchange="directornonuploadpassportfile(this,'+counter+');"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div><div class="directornonresidentuploadpassport'+counter+'-alert" id="directornonresidentuploadpassport'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directornonresidentuploadpassport'+counter+'-alert-msg"></span></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#directornonresidentpreviewpassportmanually_'+counter+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div  id="directornonresidentpreviewpassport_'+counter+'" class="col-sm-6"  style="display:none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#directornonresidentpreviewpassport_'+counter+'"><i class="fa fa-id-card-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="directornonresidentaddress[]" class="custom-file-input" id="directornonresidentaddress_'+counter+'" type="file" onclick="directornonresidentaddress('+counter+');"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="direaddress'+counter+'-alert" id="direaddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="direaddress'+counter+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="directornonresidentresaddress[]" id="directornonresidentresaddress_'+counter+'" rows="2" placeholder="Please key in residential address"></textarea><div class="directornonresidentresaddress'+counter+'-alert" id="directornonresidentresaddress'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="directornonresidentresaddress'+counter+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident

                     fieldHTML += '<div class="form-group row" id="phnumbs_'+counter+'"><label class="col-lg-4 col-form-label required" for="contactNumber">Contact Numbers</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="handphone_'+counter+'" name="handPhone[]" class="form-control mb-2" id="handPhone" placeholder="Hand phone *" required="" type="text"><div class="contactNumber'+counter+'-alert" id="contactNumber'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="contactNumber'+counter+'-alert-msg"></span></div></div><div class="col-sm"><input id="offnumber_'+counter+'" name="officeNumber[]" class="form-control mb-2" id="officeNumber" placeholder="Office number" type="text"></div></div></div> </div>'; // end of contact numbers

                     fieldHTML += '<div class="form-group row" id="emailportion_'+counter+'"><label class="col-lg-4 col-form-label required" for="emailAddress">Email Address(es) </label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="businessemail_'+counter+'" name="businessEmail[]" class="form-control mb-2" id="" placeholder="Business email *" required="" type="text"><div class="bizEmail'+counter+'-alert" id="bizEmail'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="bizEmail'+counter+'-alert-msg"></span></div></div><div class="col-sm"><input id="personalemail_'+counter+'" name="personalEmail[]" class="form-control mb-2" id="" placeholder="Personal email" type="text"></div></div></div></div>'; // end of email addresses  

                     fieldHTML += '<div class="form-group row" id="politicallyexposed_'+counter+'"><label class="col-lg-4 col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="Politically Exposed Persons (PEPs) are individuals who are or have been entrusted with prominent public functions in a foreign country, for example Heads of State or of government, senior politicians, senior government, judicial or High Ranking military officials, senior executives of state owned corporations, important political party officials. Business relationships with family members or close associates of PEP.">Are you a Politically Exposed Person (PEP) <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input name="directorpep_'+counter+'" id="directorpep_'+counter+'" value="directorpepyes_'+counter+'" type="radio" onclick="ShowHidedDiv(this.value)"><span class="custom-radio-label">Yes</span></label><div class="politicallyExposed'+counter+'-alert" id="politicallyExposed'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="politicallyExposed'+counter+'-alert-msg"></span></div></div><div class="col-sm"><label class="custom-radio-field"><input id="directorpep_'+counter+'" name="directorpep_'+counter+'" value="notdirectorpep_'+counter+'" checked="checked" type="radio" onclick="ShowHidedDiv(this.value)"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // end of pep form 

                     fieldHTML += '<div class="show-when-pep_'+counter+'" id="directorpepform_'+counter+'" style="display:none;">'; // start of director pep form 
                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="countryPep">PEP in which country</label></div><div class="col-lg-8"><div class="selectdiv"><select name="countrypep[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="countrypep_'+counter+'"><option value="">Please select</option></select><div class="countrypep'+counter+'-alert" id="countrypep'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="countrypep'+counter+'-alert-msg"></span></div></div></div></div>'; // Country of pep form

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="rolePep">Role of PEP</label></div><div class="col-lg-8"><div class="selectdiv"><select class="custom-select select-w-100" name="peprole[]" id="rolePep_'+counter+'"><option value="" selected="selected">Please select</option><option value="Ambassadors">Ambassadors</option><option value="Senior Military Officers">Senior Military Officers</option><option value="Political Party Leadership">Political Party Leadership</option><option value="Members of Parliament/Senate">Members of Parliament/Senate</option><option value="Heads of government agencies">Heads of government agencies</option><option value="Key leaders of state-owned enterprises">Key leaders of state-owned enterprises</option><option value="Key Senior Government Functionaries Judiciary/Legislature">Key Senior Government Functionaries Judiciary/Legislature</option><option value="Private companies, trusts or foundations owned or co-owned by PEPs, whether directly or indirectly">Private companies, trusts or foundations owned or co-owned by PEPs</option></select><div class="rolePep'+counter+'-alert" id="rolePep'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="rolePep'+counter+'-alert-msg"></span></div></div></div></div>'; // Role of pep form

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="pepFrom">PEP since</label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><select class="custom-select select-w-100" id="pepFrom_'+counter+'" name="pepFrom[]_'+counter+'"><option value="" selected>From</option><option value="2018">2018</option><option value="2017">2017</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option></select><div class="pepFrom'+counter+'-alert" id="pepFrom'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="pepFrom'+counter+'-alert-msg"></span></div></div><div class="col-sm"><select class="custom-select select-w-100" id="pepTo_'+counter+'" name="pepTo_'+counter+'"><option value="">To</option><option value="2018">2018</option><option value="2017">2017</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option></select><div class="pepTo'+counter+'-alert" id="pepTo'+counter+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="pepTo'+counter+'-alert-msg"></span></div></div></div></div></div>';
                     fieldHTML += '</div>'; // end of directorpepform 
                     fieldHTML += '</div>'; // end of background-grey2  
                     fieldHTML += '</div>'; // end of row mt-5 
                     fieldHTML += '</div>'; // end of divclass  
                                 
		    $('body').find('#additionalRole:last').before(fieldHTML);
                    
     
		}else{
		    alert('Maximum '+maxGroup+' Director are allowed.');
		}

                counter++; 
                $('#addroleSelector').prop('selectedIndex',0);
                $("#additionalRole").show(); 
	}

        else if(getvalue == 'Shareholder') {

		if(counter1 <= maxGroup){

                     var fieldHTML = '<div id="shareholder_'+counter1+'" class="divclass">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Individual Shareholder #'+counter1+'</h5></div></div>'; // end of background-grey1

                     fieldHTML += '<div class="col-lg-12 background-grey-2">';
                     fieldHTML += '<div class="form-group row mt-4" id="names_'+counter1+'"><div class="col-lg-4"><label class="col-form-label required" for="fullName">Full Name</label></div><div class="col-lg-8"><input name="shareholdersurname[]" class="form-control" id="shareholdersurname_'+counter1+'" placeholder="" type="text"><div class="shareholderfullName'+counter1+'-alert" id="shareholderfullName'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderfullName'+counter1+'-alert-msg"></span></div></div></div>'; // end of fullname 

                     fieldHTML += '<div class="form-group row"><label class="col-lg-4 col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="A Beneficial Owner means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or assets and undertakings of the customer or otherwise exercises control over the management of the customer.">Are you the Beneficial Owner <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input id="shareholderbowner_'+counter1+'" name="shareholderbowner_'+counter1+'[]" value="Yes" checked="checked" type="radio"><span class="custom-radio-label">Yes</span></label></div><div class="col-sm"><label class="custom-radio-field"><input id="shareholderbowner_'+counter1+'" name="shareholderbowner_'+counter1+'[]" value="No" type="radio"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // bowner    

                     fieldHTML += '<div class="form-group row" id="shareholderstatus_'+counter1+'"><div class="col-lg-4"><label class="col-form-label required" for="residencyStatus">Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="shareholderresstatus_'+counter1+'" name="shareholderresidencyStatus[]" onchange="shareholderresidencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="shareholdernonresident_'+counter1+'" rel="nonresident">Non-resident</option><option value="shareholdercitizenpr_'+counter1+'" rel="citizenpr">Singapore Citizen / PR</option><option value="shareholderpassholder_'+counter1+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="shareholderresidencyStatus'+counter1+'-alert" id="shareholderresidencyStatus'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderresidencyStatus'+counter1+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                     fieldHTML += '<div id="shareholderpassholder_'+counter1+'" class="choose-file" style="display: none;"><div class="row"><div class="col-lg-4"><label class="col-form-label" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholderuploadPassport[]" class="custom-file-input" id="shareholderuploadPassport_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div><div class="shareholderuploadPassport'+counter1+'-alert" id="shareholderuploadPassport'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderuploadPassport'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#shareholderpreviewpassportfinmanually_'+counter1+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div><div class="row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholderfincardfront[]" class="custom-file-input" id="shareholderfincardfront_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">FIN Card Front</label></div></div><div class="shareholderfincardfront'+counter1+'-alert" id="shareholderfincardfront'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderfincardfront'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholderfincardback[]" class="custom-file-input" id="shareholderfincardback_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">FIN Card Back</label></div></div><div class="shareholderfincardback'+counter1+'-alert" id="shareholderfincardback'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderfincardback'+counter1+'-alert-msg"></span></div></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholderaddress[]" class="custom-file-input" id="shareholderaddress_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="shareholderaddress'+counter1+'-alert" id="shareholderaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderaddress'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="shareholderresaddress[]" id="shareholderresaddress_'+counter1+'" rows="2" placeholder="Please key in residential address"></textarea><div class="shareholderresaddress'+counter1+'-alert" id="shareholderresaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderresaddress'+counter1+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="shareholdercitizenpr_'+counter1+'" class="choose-file" style="display: none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernricfront[]" class="custom-file-input" id="shareholdernricfront_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">NRIC Front</label></div></div><div class="shareholdernricfront'+counter1+'-alert" id="shareholdernricfront'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernricfront'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernricback[]" class="custom-file-input" id="shareholdernricback_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">NRIC Back</label></div></div><div class="shareholdernricback'+counter1+'-alert" id="shareholdernricback'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernricback'+counter1+'-alert-msg"></span></div></div></div><div class="row"><div class="col-sm-6"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#shareholderpreviewnricmanually_'+counter1+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="shareholdernonresident_'+counter1+'" class="choose-file" style="display:none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernonresidentuploadPassport[]" class="custom-file-input" id="shareholdernonresidentuploadpassport_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#shareholdernonresidentpreviewpassportmanually_'+counter1+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div class="col-sm-6"><div class="shareholderuploadpassport'+counter1+'-alert" id="shareholderuploadpassport'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderuploadpassport'+counter1+'-alert-msg"></span></div><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#shareholdernonresidentpreviewpassport_'+counter1+'" style="display:none;"><i class="fa fa-id-card-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="shareholdernonresidentaddress[]" class="custom-file-input" id="shareholdernonresidentaddress_'+counter1+'" type="file"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="shareholderaddress'+counter1+'-alert" id="shareholderaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderaddress'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="shareholdernonresidentresaddress[]" id="shareholdernonresidentresaddress_'+counter1+'" rows="2" placeholder="Please key in residential address"></textarea><div class="shareholdernonresidentresaddress'+counter1+'-alert" id="shareholdernonresidentresaddress'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdernonresidentresaddress'+counter1+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident

                     fieldHTML += '<div class="form-group row" id="shareholderphnumbs_'+counter1+'"><label class="col-lg-4 col-form-label required" for="contactNumber">Contact Numbers</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="shareholderhandphone_'+counter1+'" name="shareholderhandPhone[]" class="form-control mb-2" placeholder="Hand phone *" required="" type="text"><div class="shareholdercontactNumber'+counter1+'-alert" id="shareholdercontactNumber'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdercontactNumber'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><input id="shareholderoffnumber_'+counter1+'" name="shareholderofficeNumber[]" class="form-control mb-2" placeholder="Office number" type="text"></div></div></div> </div>'; // end of contact numbers

                     fieldHTML += '<div class="form-group row" id="shareholderemailportion_'+counter1+'"><label class="col-lg-4 col-form-label required" for="emailAddress">Email Address(es) </label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="shareholderbusinessemail_'+counter1+'" name="shareholderbusinessEmail[]" class="form-control mb-2" placeholder="Business email *" required="" type="text"><div class="shareholderbizEmail'+counter1+'-alert" id="shareholderbizEmail'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderbizEmail'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><input id="shareholderpersonalemail_'+counter1+'" name="shareholderpersonalEmail[]" class="form-control mb-2" placeholder="Personal email" type="text"></div></div></div></div>'; // end of email addresses  

                     fieldHTML += '<div class="form-group row" id="shareholderpoliticallyexposed_'+counter1+'"><label class="col-lg-4 col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="Politically Exposed Persons (PEPs) are individuals who are or have been entrusted with prominent public functions in a foreign country, for example Heads of State or of government, senior politicians, senior government, judicial or High Ranking military officials, senior executives of state owned corporations, important political party officials. Business relationships with family members or close associates of PEP.">Are you a Politically Exposed Person (PEP) <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input name="shareholderpep_'+counter1+'" id="shareholderpep_'+counter1+'" value="shareholderpepyes_'+counter1+'" type="radio" onclick="ShowHidesDiv(this.value)"><span class="custom-radio-label">Yes</span></label><div class="shareholderpoliticallyExposed'+counter1+'-alert" id="shareholderpoliticallyExposed'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="politicallyExposed'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><label class="custom-radio-field"><input id="shareholderpep_'+counter1+'" name="shareholderpep_'+counter1+'" value="notshareholderpep_'+counter1+'" checked="checked" type="radio" onclick="ShowHidesDiv(this.value)"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // end of pep form 

                     fieldHTML += '<div class="show-when-shareholderpep_'+counter1+'" id="shareholderpepform_'+counter1+'" style="display:none;">'; // start of shareholder pep form 
                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="countryPep">PEP in which country</label></div><div class="col-lg-8"><div class="selectdiv"><select name="shareholdercountrypep[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="shareholdercountrypep_'+counter1+'"><option value="">Please select</option></select><div class="shareholdercountrypep'+counter1+'-alert" id="shareholdercountrypep'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholdercountrypep'+counter1+'-alert-msg"></span></div></div></div></div>'; // Country of pep form

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="rolePep">Role of PEP</label></div><div class="col-lg-8"><div class="selectdiv"><select class="custom-select select-w-100" name="shareholderpeprole[]" id="shareholderrolePep_'+counter1+'"><option value="" selected="selected">Please select</option><option value="Ambassadors">Ambassadors</option><option value="Senior Military Officers">Senior Military Officers</option><option value="Political Party Leadership">Political Party Leadership</option><option value="Members of Parliament/Senate">Members of Parliament/Senate</option><option value="Heads of government agencies">Heads of government agencies</option><option value="Key leaders of state-owned enterprises">Key leaders of state-owned enterprises</option><option value="Key Senior Government Functionaries Judiciary/Legislature">Key Senior Government Functionaries Judiciary/Legislature</option><option value="Private companies, trusts or foundations owned or co-owned by PEPs, whether directly or indirectly">Private companies, trusts or foundations owned or co-owned by PEPs</option></select><div class="shareholderrolePep'+counter1+'-alert" id="shareholderrolePep'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderrolePep'+counter1+'-alert-msg"></span></div></div></div></div>'; // Role of pep form

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="shareholderpepFrom">PEP since</label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><select class="custom-select select-w-100" id="shareholderpepFrom_'+counter1+'" name="shareholderpepFrom[]_'+counter1+'"><option value="" selected>From</option><option value="2018">2018</option><option value="2017">2017</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option></select><div class="pepFrom'+counter1+'-alert" id="pepFrom'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="pepFrom'+counter1+'-alert-msg"></span></div></div><div class="col-sm"><select class="custom-select select-w-100" id="shareholderpepTo_'+counter1+'" name="shareholderpepTo_'+counter1+'"><option value="">To</option><option value="2018">2018</option><option value="2017">2017</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option></select><div class="shareholderpepTo'+counter1+'-alert" id="shareholderpepTo'+counter1+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="shareholderpepTo'+counter1+'-alert-msg"></span></div></div></div></div></div>';
                     fieldHTML += '</div>'; // end of shareholderpepform 
                     fieldHTML += '</div>'; // end of background-grey2  
                     fieldHTML += '</div>'; // end of row mt-5 
                     fieldHTML += '</div>'; // end of divclass  
		    $('body').find('#additionalRole:last').before(fieldHTML);
                      
		}else{
		    alert('Maximum '+maxGroup+' Shareholder are allowed.');
		}
                counter1++;
                $('#addroleSelector').prop('selectedIndex',0);   
                $("#additionalRole").show(); 
	}

        else if(getvalue == 'Director and Shareholder') {

		if(counter2 <= maxGroup){
		     var fieldHTML = '<div id="dishareholder_'+counter2+'" class="divclass">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Director and Individual Shareholder #'+counter2+'</h5></div></div>'; // end of background-grey1

                     fieldHTML += '<div class="col-lg-12 background-grey-2">';
                     fieldHTML += '<div class="form-group row mt-4" id="names_'+counter2+'"><div class="col-lg-4"><label class="col-form-label required" for="fullName">Full Name</label></div><div class="col-lg-8"><input name="dishareholdersurname[]" class="form-control" id="dishareholdersurname_'+counter2+'" placeholder="" type="text"><div class="dishareholderfullName'+counter2+'-alert" id="dishareholderfullName'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderfullName'+counter2+'-alert-msg"></span></div></div></div>'; // end of fullname 

                     fieldHTML += '<div class="form-group row"><label class="col-lg-4 col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="A Beneficial Owner means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or assets and undertakings of the customer or otherwise exercises control over the management of the customer.">Are you the Beneficial Owner <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input id="dishareholderbowner_'+counter2+'" name="dishareholderbowner_'+counter2+'[]" value="Yes" checked="checked" type="radio"><span class="custom-radio-label">Yes</span></label></div><div class="col-sm"><label class="custom-radio-field"><input id="dishareholderbowner_'+counter2+'" name="dishareholderbowner_'+counter2+'[]" value="No" type="radio"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // bowner    

                     fieldHTML += '<div class="form-group row" id="dishareholderstatus_'+counter2+'"><div class="col-lg-4"><label class="col-form-label required" for="residencyStatus">Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="dishareholderresstatus_'+counter2+'" name="dishareholderresidencyStatus[]" onchange="dishareholderresidencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="dishareholdernonresident_'+counter2+'" rel="nonresident">Non-resident</option><option value="dishareholdercitizenpr_'+counter2+'" rel="citizenpr">Singapore Citizen / PR</option><option value="dishareholderpassholder_'+counter2+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="dishareholderresidencyStatus'+counter2+'-alert" id="dishareholderresidencyStatus'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderresidencyStatus'+counter2+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                     fieldHTML += '<div id="dishareholderpassholder_'+counter2+'" class="choose-file" style="display: none;"><div class="row"><div class="col-lg-4"><label class="col-form-label" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholderuploadPassport[]" class="custom-file-input" id="dishareholderuploadPassport_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div><div class="dishareholderuploadPassport'+counter2+'-alert" id="dishareholderuploadPassport'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderuploadPassport'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#dishareholderpreviewpassportfinmanually_'+counter2+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div><div class="row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholderfincardfront[]" class="custom-file-input" id="dishareholderfincardfront_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">FIN Card Front</label></div></div><div class="dishareholderfincardfront'+counter2+'-alert" id="dishareholderfincardfront'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderfincardfront'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholderfincardback[]" class="custom-file-input" id="dishareholderfincardback_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">FIN Card Back</label></div></div><div class="dishareholderfincardback'+counter2+'-alert" id="dishareholderfincardback'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderfincardback'+counter2+'-alert-msg"></span></div></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholderaddress[]" class="custom-file-input" id="dishareholderaddress_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="dishareholderaddress'+counter2+'-alert" id="dishareholderaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderaddress'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="dishareholderresaddress[]" id="dishareholderresaddress_'+counter2+'" rows="2" placeholder="Please key in residential address"></textarea><div class="dishareholderresaddress'+counter2+'-alert" id="dishareholderresaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderresaddress'+counter2+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="dishareholdercitizenpr_'+counter2+'" class="choose-file" style="display: none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholdernricfront[]" class="custom-file-input" id="dishareholdernricfront_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">NRIC Front</label></div></div><div class="dishareholdernricfront'+counter2+'-alert" id="dishareholdernricfront'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholdernricfront'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholdernricback[]" class="custom-file-input" id="dishareholdernricback_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">NRIC Back</label></div></div><div class="dishareholdernricback'+counter2+'-alert" id="dishareholdernricback'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholdernricback'+counter2+'-alert-msg"></span></div></div></div><div class="row"><div class="col-sm-6"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#dishareholderpreviewnricmanually_'+counter2+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="dishareholdernonresident_'+counter2+'" class="choose-file" style="display:none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholdernonresidentuploadPassport[]" class="custom-file-input" id="dishareholdernonresidentuploadpassport_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#dishareholdernonresidentpreviewpassportmanually_'+counter2+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div class="col-sm-6"><div class="dishareholderuploadpassport'+counter2+'-alert" id="dishareholderuploadpassport'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderuploadpassport'+counter2+'-alert-msg"></span></div><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#dishareholdernonresidentpreviewpassport_'+counter2+'" style="display:none;"><i class="fa fa-id-card-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="dishareholdernonresidentaddress[]" class="custom-file-input" id="dishareholdernonresidentaddress_'+counter2+'" type="file"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="dishareholderaddress'+counter2+'-alert" id="dishareholderaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderaddress'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="dishareholdernonresidentresaddress[]" id="dishareholdernonresidentresaddress_'+counter2+'" rows="2" placeholder="Please key in residential address"></textarea><div class="dishareholdernonresidentresaddress'+counter2+'-alert" id="dishareholdernonresidentresaddress'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholdernonresidentresaddress'+counter2+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident

                     fieldHTML += '<div class="form-group row" id="dishareholderphnumbs_'+counter2+'"><label class="col-lg-4 col-form-label required" for="contactNumber">Contact Numbers</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="dishareholderhandphone_'+counter2+'" name="dishareholderhandPhone[]" class="form-control mb-2" placeholder="Hand phone *" required="" type="text"><div class="dishareholdercontactNumber'+counter2+'-alert" id="dishareholdercontactNumber'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholdercontactNumber'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><input id="dishareholderoffnumber_'+counter2+'" name="dishareholderofficeNumber[]" class="form-control mb-2" placeholder="Office number" type="text"></div></div></div> </div>'; // end of contact numbers

                     fieldHTML += '<div class="form-group row" id="dishareholderemailportion_'+counter2+'"><label class="col-lg-4 col-form-label required" for="emailAddress">Email Address(es) </label><div class="col-lg-8"><div class="row"><div class="col-sm"><input id="dishareholderbusinessemail_'+counter2+'" name="dishareholderbusinessEmail[]" class="form-control mb-2" placeholder="Business email *" required="" type="text"><div class="dishareholderbizEmail'+counter2+'-alert" id="dishareholderbizEmail'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderbizEmail'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><input id="dishareholderpersonalemail_'+counter2+'" name="dishareholderpersonalEmail[]" class="form-control mb-2" placeholder="Personal email" type="text"></div></div></div></div>'; // end of email addresses  

                     fieldHTML += '<div class="form-group row" id="dishareholderpoliticallyexposed_'+counter2+'"><label class="col-lg-4 col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="Politically Exposed Persons (PEPs) are individuals who are or have been entrusted with prominent public functions in a foreign country, for example Heads of State or of government, senior politicians, senior government, judicial or High Ranking military officials, senior executives of state owned corporations, important political party officials. Business relationships with family members or close associates of PEP.">Are you a Politically Exposed Person (PEP) <i class="fa fa-question-circle"></i></label><div class="col-lg-8"><div class="row mb-0"><div class="col-sm"><label class="custom-radio-field"><input name="dishareholderpep_'+counter2+'" id="dishareholderpep_'+counter2+'" value="dishareholderpepyes_'+counter2+'" type="radio" onclick="ShowHidesdDiv(this.value)"><span class="custom-radio-label">Yes</span></label><div class="dishareholderpoliticallyExposed'+counter2+'-alert" id="dishareholderpoliticallyExposed'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="politicallyExposed'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><label class="custom-radio-field"><input id="dishareholderpep_'+counter2+'" name="dishareholderpep_'+counter2+'" value="notdishareholderpep_'+counter2+'" checked="checked" type="radio" onclick="ShowHidesdDiv(this.value)"><span class="custom-radio-label">No</span></label></div></div></div></div>'; // end of pep form 

                     fieldHTML += '<div class="show-when-dishareholderpep_'+counter2+'" id="dishareholderpepform_'+counter2+'" style="display:none;">'; // start of dishareholder pep form 
                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="countryPep">PEP in which country</label></div><div class="col-lg-8"><div class="selectdiv"><select name="dishareholdercountrypep[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="dishareholdercountrypep_'+counter2+'"><option value="">Please select</option></select><div class="dishareholdercountrypep'+counter2+'-alert" id="dishareholdercountrypep'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholdercountrypep'+counter2+'-alert-msg"></span></div></div></div></div>'; // Country of pep form

                     fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="rolePep">Role of PEP</label></div><div class="col-lg-8"><div class="selectdiv"><select class="custom-select select-w-100" name="dishareholderpeprole[]" id="dishareholderrolePep_'+counter2+'"><option value="" selected="selected">Please select</option><option value="Ambassadors">Ambassadors</option><option value="Senior Military Officers">Senior Military Officers</option><option value="Political Party Leadership">Political Party Leadership</option><option value="Members of Parliament/Senate">Members of Parliament/Senate</option><option value="Heads of government agencies">Heads of government agencies</option><option value="Key leaders of state-owned enterprises">Key leaders of state-owned enterprises</option><option value="Key Senior Government Functionaries Judiciary/Legislature">Key Senior Government Functionaries Judiciary/Legislature</option><option value="Private companies, trusts or foundations owned or co-owned by PEPs, whether directly or indirectly">Private companies, trusts or foundations owned or co-owned by PEPs</option></select><div class="dishareholderrolePep'+counter2+'-alert" id="dishareholderrolePep'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderrolePep'+counter2+'-alert-msg"></span></div></div></div></div>'; // Role of pep form

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="dishareholderpepFrom">PEP since</label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><select class="custom-select select-w-100" id="dishareholderpepFrom_'+counter2+'" name="dishareholderpepFrom[]_'+counter2+'"><option value="" selected>From</option><option value="2018">2018</option><option value="2017">2017</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option></select><div class="pepFrom'+counter2+'-alert" id="pepFrom'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="pepFrom'+counter2+'-alert-msg"></span></div></div><div class="col-sm"><select class="custom-select select-w-100" id="dishareholderpepTo_'+counter2+'" name="dishareholderpepTo_'+counter2+'"><option value="">To</option><option value="2018">2018</option><option value="2017">2017</option><option value="2016">2016</option><option value="2015">2015</option><option value="2014">2014</option><option value="2013">2013</option><option value="2012">2012</option><option value="2011">2011</option><option value="2010">2010</option><option value="2009">2009</option><option value="2008">2008</option><option value="2007">2007</option><option value="2006">2006</option><option value="2005">2005</option><option value="2004">2004</option><option value="2003">2003</option><option value="2002">2002</option><option value="2001">2001</option><option value="2000">2000</option><option value="1999">1999</option><option value="1998">1998</option><option value="1997">1997</option><option value="1996">1996</option><option value="1995">1995</option><option value="1994">1994</option><option value="1993">1993</option><option value="1992">1992</option><option value="1991">1991</option><option value="1990">1990</option></select><div class="dishareholderpepTo'+counter2+'-alert" id="dishareholderpepTo'+counter2+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="dishareholderpepTo'+counter2+'-alert-msg"></span></div></div></div></div></div>';
                     fieldHTML += '</div>'; // end of dishareholderpepform 
                     fieldHTML += '</div>'; // end of background-grey2  
                     fieldHTML += '</div>'; // end of row mt-5 
                     fieldHTML += '</div>'; // end of divclass 
		    $('body').find('#additionalRole:last').before(fieldHTML);
		}else{
		    alert('Maximum '+maxGroup+' Director and Shareholder are allowed.');
		}

                counter2++; 
                $('#addroleSelector').prop('selectedIndex',0);
                $("#additionalRole").show(); 

	}

        else if(getvalue == 'Corporate Shareholder') {

		if(counter3 <= maxGroup){
		    var fieldHTML = '<div id="corporateshareholder_'+counter3+'">';
                     fieldHTML += '<div class="row mt-5">';
                     fieldHTML += '<div class="col-lg-12 background-grey-1 rounded-border-top">							<div class="section-header"><h5 class="font-lato">Particulars of Corporate Shareholder #'+counter3+'</h5></div></div>'; // end of background-grey1
		    fieldHTML += '<div class="col-lg-12 background-grey-2">';
                    fieldHTML += '<div class="form-group row mt-4"><div class="col-lg-4"><label class="col-form-label required" for="">Name of Corporate Shareholder</label></div><div class="col-lg-8"><input type="text" name="corporatename[]" class="form-control" id="corporatename_'+counter3+'"><div class="corporatename'+counter3+'-alert" id="corporatename'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatename'+counter3+'-alert-msg"></span></div></div></div>'; //name of corporate shareholder

                    fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="">Country of Incorporation</label></div><div class="col-lg-8"><div class="selectdiv"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country select-w-100" id="corporatecountry_'+counter3+'" name="corporatecountry[]"></select><div class="corporatecountry'+counter3+'-alert" id="corporatecountry'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatecountry'+counter3+'-alert-msg"></span></div></div></div></div>'; // Country of Incorporation

                   fieldHTML += '<div class="form-group row"><div class="col-lg-4"><label class="col-form-label" for="">Upload Document</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input type="file" name="corporatecertificate[]" class="custom-file-input" id="corporatecertificate_'+counter3+'" ><label class="custom-file-label dashed-lines" for=""></label></div></div><div class="corporatecertificate'+counter3+'-alert" id="corporatecertificate'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatecertificate'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input type="file" name="corporatecertifcate1[]" class="custom-file-input" id="corporatecertificate1_'+counter3+'" ><label class="custom-file-label dashed-lines" for=""></label></div></div><div class="corporatecertificate1'+counter3+'-alert" id="corporatecertificate1'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatecertificate1'+counter3+'-alert-msg"></span></div></div></div><div class="row"><div class="col-sm"><p class="small font-italic">Certificate of Incorporation</span></div><div class="col-sm"><p class="small font-italic">Extract of the companys details from the Registrar of Companies - Business Profile / Certificate of Incumbency  Registry of Shareholders</span></div></div></div><div class="col-sm-12 border-bottom mb-3">&nbsp;</div></div>'; // Upload Document Section

                    fieldHTML += '<div class="form-group row mt-4"><div class="col-lg-4"><label class="col-form-label" for="">Name of Corporate Representative</label></div><div class="col-lg-8"><input type="text" name="representativename[]" class="form-control" id="representativename_'+counter3+'"><div class="representativename'+counter3+'-alert" id="representativename'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="representativename'+counter3+'-alert-msg"></span></div></div></div>'; // Corporate Representative

                    fieldHTML += '<div class="form-group row" id="corporatestatus_'+counter3+'"><div class="col-lg-4"><label class="col-form-label" for="residencyStatus">Current Residency Status</label></div><div class="col-lg-8"><div class="selectdiv"><select id="corporateresstatus_'+counter3+'" name="corporateresidencyStatus[]" onchange="corporateresidencystatus(this.value)" class="custom-select select-w-100" data-placeholder="Please select..."><option value="">Please select</option><option value="corporatenonresident_'+counter3+'" rel="nonresident">Non-resident</option><option value="corporatecitizenpr_'+counter3+'" rel="citizenpr">Singapore Citizen / PR</option><option value="corporatepassholder_'+counter3+'" rel="passholder">FIN Card (Work Passes)</option></select><div class="corporateresidencyStatus'+counter3+'-alert" id="corporateresidencyStatus'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateresidencyStatus'+counter3+'-alert-msg"></span></div></div></div></div>'; // end of residency status

                     fieldHTML += '<div id="corporatepassholder_'+counter3+'" class="choose-file" style="display: none;"><div class="row"><div class="col-lg-4"><label class="col-form-label" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporateuploadPassport[]" class="custom-file-input" id="corporateuploadPassport_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div><div class="corporateuploadPassport'+counter3+'-alert" id="corporateuploadPassport'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateuploadPassport'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#corporatepreviewpassportfinmanually_'+counter3+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport/FIN information manually</button></div></div></div></div><div class="row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload FIN</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatefincardfront[]" class="custom-file-input" id="corporatefincardfront_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">FIN Card Front</label></div></div><div class="corporatefincardfront'+counter3+'-alert" id="corporatefincardfront'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatefincardfront'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatefincardback[]" class="custom-file-input" id="corporatefincardback_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">FIN Card Back</label></div></div><div class="corporatefincardback'+counter3+'-alert" id="corporatefincardback'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatefincardback'+counter3+'-alert-msg"></span></div></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporateaddress[]" class="custom-file-input" id="corporateaddress_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="corporateaddress'+counter3+'-alert" id="corporateaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateaddress'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="corporateresaddress[]" id="corporateresaddress_'+counter3+'" rows="2" placeholder="Please key in residential address"></textarea><div class="corporateresaddress'+counter3+'-alert" id="corporateresaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateresaddress'+counter3+'-alert-msg"></span></div></div></div></div></div></div>'; // end for passholder

                     fieldHTML +='<div id="corporatecitizenpr_'+counter3+'" class="choose-file" style="display: none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="">Upload NRIC</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatenricfront[]" class="custom-file-input" id="corporatenricfront_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">NRIC Front</label></div></div><div class="corporatenricfront'+counter3+'-alert" id="corporatenricfront'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenricfront'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatenricback[]" class="custom-file-input" id="corporatenricback_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">NRIC Back</label></div></div><div class="corporatenricback'+counter3+'-alert" id="corporatenricback'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenricback'+counter3+'-alert-msg"></span></div></div></div><div class="row"><div class="col-sm-6"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#corporatepreviewnricmanually_'+counter3+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button></div></div></div></div></div>'; // end for citizen PR

                     fieldHTML += '<div id="corporatenonresident_'+counter3+'" class="choose-file" style="display:none;"><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="uploadPassport">Upload Passport</label></div><div class="col-lg-8"><div class="row mb-1"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatenonresidentuploadPassport[]" class="custom-file-input" id="corporatenonresidentuploadpassport_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">Passport</label></div></div></div><div class="col-sm"><button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#corporatenonresidentpreviewpassportmanually_'+counter3+'"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button></div></div><div class="row"><div class="col-sm-6"><div class="corporateuploadpassport'+counter3+'-alert" id="corporateuploadpassport'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateuploadpassport'+counter3+'-alert-msg"></span></div><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#corporatenonresidentpreviewpassport_'+counter3+'" style="display:none;"><i class="fa fa-id-card-o pr-2" aria-hidden="true"></i> Preview your passport details</button></div></div></div></div><div class="form-group row"><div class="col-lg-4"><label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label></div><div class="col-lg-8"><div class="row"><div class="col-sm"><div class="input-group"><div class="custom-file"><input name="corporatenonresidentaddress[]" class="custom-file-input" id="corporatenonresidentaddress_'+counter3+'" type="file"><label class="custom-file-label dashed-lines" for="">Address</label></div></div><div class="corporateaddress'+counter3+'-alert" id="corporateaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporateaddress'+counter3+'-alert-msg"></span></div></div><div class="col-sm"><textarea class="form-control col-sm-12" name="corporatenonresidentresaddress[]" id="corporatenonresidentresaddress_'+counter3+'" rows="2" placeholder="Please key in residential address"></textarea><div class="corporatenonresidentresaddress'+counter3+'-alert" id="corporatenonresidentresaddress'+counter3+'-alert" style="color:#B90D0F;"><span class="font-italic small" id="corporatenonresidentresaddress'+counter3+'-alert-msg"></span></div></div></div></div></div></div>'; // end for nonresident

                    fieldHTML += '<div class="form-group row"><label class="col-lg-4 col-form-label" for="contactNumber">Contact Details</label><div class="col-lg-8"><div class="row"><div class="col-sm"><input type="text" name="corporateemail[]" class="form-control mb-2" id="corporateemail_'+counter3+'" placeholder="Email" required /></div><div class="col-sm"><input type="text" name="corporatecontactnumber[]" class="form-control mb-2" id="corporatecontactnumber_'+counter3+'" placeholder="Contact number"/></div></div></div></div>'; // Contact Details

                    fieldHTML += '<div class="form-group row notice"><div class="col-lg-8">We will need to know the individual ultimate beneficiary owner of the company. Please upload shareholding chart.<a href="#" class="color-red font-italic" data-toggle="modal" data-target="#shareholdingchart">(See sample)</a>.</div><div class="col-lg-4"><label for="file-upload" class="custom-file-upload-button btn btn-next btn-lg select-w-100"><i class="fa fa-cloud-upload"></i> Upload Chart</label><input id="file-upload" type="file" class="btn-upload-chart" /></div></div>'; // Chart Portions
                    fieldHTML += '</div>'; // end of background-grey2 
                    fieldHTML += '</div>'; // end of row mt-5 
                    fieldHTML += '</div>'; // end of divclass 
		    $('body').find('#additionalRole:last').before(fieldHTML);
		}else{
		    alert('Maximum '+maxGroup+' Corporate Shareholder are allowed.');
		}
                counter3++; 
                $('#addroleSelector').prop('selectedIndex',0);
                $("#additionalRole").show(); 

	}
        else if(getvalue == 'Nomore') {

              alert("Are you ok to proceed further as you selected No More Additional Role")
              $("#additionalRole").show(); 

        }else {

             //$("#additionalRole").hide(); 

        }
        

}

// For Director only
function residencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "directornonresident_"+arr[1]) {
          //alert("fddf");

	   $("#directornonresident_"+arr[1]).css("display", "block");
	   $("#directorcitizenpr_"+arr[1]).css("display", "none");
	   $("#directorpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "directorcitizenpr_"+arr[1]) {

	   $("#directornonresident_"+arr[1]).css("display", "none");
	   $("#directorcitizenpr_"+arr[1]).css("display", "block");
	   $("#directorpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "directorpassholder_"+arr[1]) {

	   $("#directornonresident_"+arr[1]).css("display", "none");
	   $("#directorcitizenpr_"+arr[1]).css("display", "none");
	   $("#directorpassholder_"+arr[1]).css("display", "block");
	}
        else {
 
           uploadFields.hide();

        }

}


// For Shareholder only
function shareholderresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "shareholdernonresident_"+arr[1]) {
          //alert("fddf");

	   $("#shareholdernonresident_"+arr[1]).css("display", "block");
	   $("#shareholdercitizenpr_"+arr[1]).css("display", "none");
	   $("#shareholderpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "shareholdercitizenpr_"+arr[1]) {

	   $("#shareholdernonresident_"+arr[1]).css("display", "none");
	   $("#shareholdercitizenpr_"+arr[1]).css("display", "block");
	   $("#shareholderpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "shareholderpassholder_"+arr[1]) {

	   $("#shareholdernonresident_"+arr[1]).css("display", "none");
	   $("#shareholdercitizenpr_"+arr[1]).css("display", "none");
	   $("#shareholderpassholder_"+arr[1]).css("display", "block");
	}
        else {
 
            uploadFields.hide();

        }

}


// For director & Shareholder only
function dishareholderresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "dishareholdernonresident_"+arr[1]) {
          //alert("fddf");

	   $("#dishareholdernonresident_"+arr[1]).css("display", "block");
	   $("#dishareholdercitizenpr_"+arr[1]).css("display", "none");
	   $("#dishareholderpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "dishareholdercitizenpr_"+arr[1]) {

	   $("#dishareholdernonresident_"+arr[1]).css("display", "none");
	   $("#dishareholdercitizenpr_"+arr[1]).css("display", "block");
	   $("#sdirectorpassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "dishareholderpassholder_"+arr[1]) {

	   $("#dishareholdernonresident_"+arr[1]).css("display", "none");
	   $("#dishareholdercitizenpr_"+arr[1]).css("display", "none");
	   $("#dishareholderpassholder_"+arr[1]).css("display", "block");
	}
        else {
 
           uploadFields.hide();
  
        }

}

// For Corporate only
function corporateresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "corporatenonresident_"+arr[1]) {
          //alert("fddf");

	   $("#corporatenonresident_"+arr[1]).css("display", "block");
	   $("#corporatecitizenpr_"+arr[1]).css("display", "none");
	   $("#corporatepassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "corporatecitizenpr_"+arr[1]) {

	   $("#corporatenonresident_"+arr[1]).css("display", "none");
	   $("#corporatecitizenpr_"+arr[1]).css("display", "block");
	   $("#corporatepassholder_"+arr[1]).css("display", "none");
	}

	else if(getvalue == "corporatepassholder_"+arr[1]) {

	   $("#corporatenonresident_"+arr[1]).css("display", "none");
	   $("#corporatecitizenpr_"+arr[1]).css("display", "none");
	   $("#corporatepassholder_"+arr[1]).css("display", "block");
	}
        else {
 
           //alert("ddf");
            uploadFields.hide();
           
        }

}


function ShowHidedDiv(getdirector){

var arr = getdirector.split('_'); 

	if (getdirector == "directorpepyes_"+arr[1]) {
	   $(".show-when-pep_"+arr[1]).css("display", "block");
	} else {
	   $(".show-when-pep_"+arr[1]).css("display", "none");
	}

}


function ShowHidesDiv(getshareholder){

var arr = getshareholder.split('_'); 

	if (getshareholder == "shareholderpepyes_"+arr[1]) {
                $(".show-when-shareholderpep_"+arr[1]).css("display", "block");
	} else {
		$(".show-when-shareholderpep_"+arr[1]).css("display", "none");
	}

}

function ShowHidesdDiv(getdshareholder){

var arr = getdshareholder.split('_'); 

	if (getdshareholder == "dishareholderpepyes_"+arr[1]) {
                $(".show-when-dishareholderpep_"+arr[1]).css("display", "block");
	} else {
		$(".show-when-dishareholderpep_"+arr[1]).css("display", "none");
	}

}
</script>

<?php include('includes/previewpopup.php');?>
