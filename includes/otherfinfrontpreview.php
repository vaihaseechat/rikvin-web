<div class="modal fade"  data-keyboard="true"   id="otherfinfrontpreview" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">WorkPass Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">
					<div class="row mt-3">
						<div class="col-sm">
							<div class="form-group row">
								<div class="col-sm-12">
									Please check details below.
								</div>
								<div class="col-sm-12">
									<div class="passport-scan-copy" id="otherfinfrontimage"><img src="" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="employer">Employer</label>
								<div class="col-sm-12"><input name="otherfinfrontemployer" class="form-control" id="otherfinfrontemployer"  value="" type="text"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="givenName">Occupation</label>
								<div class="col-sm-12"><input name="otherfinfrontoccupation" class="form-control" id="otherfinfrontoccupation"  value="" type="text"></div>
							</div>
						</div>                                               
                              
					</div>	
                                        <div class="row">
                                             <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label" for="dateofissue">Date of Issue</label>
								<div class="col-sm-12">
                                                                 <div class="input-group dateicon">
                                                                  <input name="otherfinfrontdoi" class="form-control" id="otherfinfrontdoi"  value="" type="text" placeholder="DD-MM-YYYY" maxlength="10">
                                                                </div></div>
							</div>
					     </div>
                                             <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label" for="dateofissue">Date of Expiry</label>
								<div class="col-sm-12">
                                                                 <div class="input-group dateicon">
                                                                  <input name="otherfinfrontdoe" class="form-control" id="otherfinfrontdoe"  value="" type="text" placeholder="DD-MM-YYYY" maxlength="10">
                                                                </div></div>
							</div>
						</div>
                                        </div>				
					<div class="row">			
						
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="finnumber">FIN Number</label>
								<div class="col-sm-12">
									<div class="input-group">
										<input id="otherfinfrontnumber" class="form-control" name="otherfinfrontnumber" value="" type="text">
										
									</div>
								</div>								
							</div>
						</div>
                                                
						
					</div>
					
					
					
					<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
                                                                <input type="hidden" name="otherfinfrontfilename" id="otherfinfrontfilename" value="">   
								<button class="btn btn-next" onclick="savefinfront();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savefinfront(){

if( $('#otherfinfrontpreview').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#otherfinfrontpreview').css("overflow","auto");
}

var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

var otherfinfrontfilename = document.getElementById("otherfinfrontfilename").value;
var otherfinfrontemployer = document.getElementById("otherfinfrontemployer").value;
var otherfinfrontoccupation = document.getElementById("otherfinfrontoccupation").value;
var otherwfinfrontdoi = document.getElementById("otherfinfrontdoi").value;
var otherwfinfrontdoe = document.getElementById("otherfinfrontdoe").value;
var otherfinfrontnumber = document.getElementById("otherfinfrontnumber").value;

var getfincardfrontdateissue = formatingdata(otherwfinfrontdoi);
var getfincardfrontdateexpiry = formatingdata(otherwfinfrontdoe);

//var startdate = new Date(otherwfinfrontdoi.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(otherwfinfrontdoe.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
var startdate = $('#otherfinfrontdoi').datepicker('getDate');
var enddate = $('#otherfinfrontdoe').datepicker('getDate');

        if(otherfinfrontemployer == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(otherfinfrontoccupation == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(otherwfinfrontdoi != '' && getfincardfrontdateissue == true){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(otherwfinfrontdoe != '' && getfincardfrontdateexpiry == true){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(getfincardfrontdateissue == true){
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(getfincardfrontdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if((otherwfinfrontdoi != '') && (otherwfinfrontdoi.length < 10)){
              $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if((otherwfinfrontdoe != '') && (otherwfinfrontdoe.length < 10)){
              $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if((otherwfinfrontdoi != '') && (otherwfinfrontdoe != '') && (startdate >= enddate)){
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        }       
        else if(otherfinfrontnumber == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("WorkPass Information");
             $("#displaysuccessmsg .modal-body").html("FIN Number cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else{
	   //alert("Saved Succesfully");
	    $('#otherfinfrontpreview').modal('hide');
            $("#displaysuccessmsg .modal-title").html("WorkPass Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#displaysuccessmsg").modal("show");
	}

localStorage.setItem("otherfinfrontemployer",otherfinfrontemployer);
localStorage.setItem("otherfinfrontoccupation",otherfinfrontoccupation);
localStorage.setItem("otherwfinfrontdoi",otherwfinfrontdoi);
localStorage.setItem("otherwfinfrontdoe",otherwfinfrontdoe);
localStorage.setItem("otherfinfrontnumber",otherfinfrontnumber);


}

</script>

