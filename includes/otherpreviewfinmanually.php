<div class="modal fade"  data-keyboard="true"   id="otherpreviewpassportfinmanually" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">Passport and Work Pass Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">				
					
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="passportNumber">Passport Number</label>
								<div class="col-sm-12"><input name="othermfinpassportNumber" class="form-control" id="othermfinpassportNumber" placeholder="" value="" type="text"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="nationality">Nationality</label>
								<div class="col-sm-12">
                                                                 <select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othermfinnationality" name="othermfinnationality"></select>
                                                                 </div>
                                                        </div>	
						</div>						
					</div>
					<div class="row">
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Country of Issue</label>
								<div class="col-sm-12">
                                                                <select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othermfincntryissue" name="othermfincntryissue"></select>
                                                                </div>
									
							</div>
						</div>						
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Gender</label>
								<div class="col-sm-12">
									<label class="radio-inline mr-3">
						 <input name="othermfinoptradio" class="radio-red" type="radio" value="Male" checked > Male
									</label>
									<label class="radio-inline">
									  <input name="othermfinoptradio" class="radio-red" type="radio" value="Female"> Female
									</label>
								</div>
							</div>
						</div>	
                                          </div>
                                          <div class="row">
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfIssue">Date of Issue</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
										<input id="othermfindoi" class="form-control" name="othermfindoi" placeholder="DD/MM/YY" type="text">
										
									</div>
								</div>
							</div>
						</div>

                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfExpiry">Date of Expiry</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
									<input id="othermfindoe" class="form-control" name="othermfindoe" placeholder="DD/MM/YY" type="text">
									
								</div>	
								</div>							
							</div>
						</div>					
					</div>					
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfBirth">Date of Birth</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
									<input id="othermfindob" class="form-control" name="othermfindob" placeholder="DD/MM/YYYY" type="text">
									
								</div>	
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="placeOfBirth">Place of Birth</label>
								<div class="col-sm-12">
								 <select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othermfinpob" name="othermfinpob"></select>	
								</div>
							</div>
						</div>
					</div>
					<div class="row my-3">
						<div class="col-sm">
							<h4 class="font-weight-otherld">Work Pass</h4>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Employer <span class="small color-pale-red"><sup>1</sup></span></label>
								<div class="col-sm-12"><input name="othermfinemployer" class="form-control" id="othermfinemployer" placeholder="" value="" type="text"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Occupation <span class="small color-pale-red"><sup>1</sup></span></label>
								<div class="col-sm-12"><input name="othermfinoccupation" class="form-control" id="othermfinoccupation" placeholder="" value="" type="text"></div>
							</div>
						</div>
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Date of Issue</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
									<input id="othermwfindoi" class="form-control" name="othermwfindoi" placeholder="DD/MM/YYYY" type="text">
									
								</div>	
								</div>
							</div>
						</div>
					</div>
					<div class="row mb-2">
						<div class="col-sm">
							<span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span>
						</div>
					</div>
					<div class="row">						
						<div class="col-sm">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Date of Expiry</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
									<input id="othermwfindoe" class="form-control" name="othermwfindoe" placeholder="DD/MM/YY" type="text">
									
								</div>	
								</div>
							</div>
						</div>
                                                <div class="col-sm">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">FIN Number</label>
								<div class="col-sm-12"><input name="othermfinnumber" class="form-control" id="othermfinnumber" placeholder="" value="" type="text"></div>
							</div>
						</div>
					</div>
		        		<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
								<button class="btn btn-next" onclick="savefinmanual();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savefinmanual(){

if( $('#otherpreviewpassportfinmanually').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#otherpreviewpassportfinmanually').css("overflow","auto");
}


var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

var othermfinpassportno = document.getElementById("othermfinpassportNumber").value;
var othermfinnationality = $('#othermfinnationality_chosen span').text();
var othermfincountry = $('#othermfincntryissue_chosen span').text();
var othermfindatetimepicker = document.getElementById("othermfindoi").value;
var othermfingender = $("input[name='othermfinoptradio']:checked").val();
var othermfindob = document.getElementById("othermfindob").value;
var othermfindateexpiry = document.getElementById("othermfindoe").value;
var othermfinpob = $('#othermfinpob_chosen span').text();

var othermfinemployer = document.getElementById("othermfinemployer").value;
var othermfinoccupation = document.getElementById("othermfinoccupation").value;
var othermwfindoi = document.getElementById("othermwfindoi").value;
var othermwfindoe = document.getElementById("othermwfindoe").value;
var othermfinnumber = document.getElementById("othermfinnumber").value;

var getfincarddatetimepicker = formatingdata(othermfindatetimepicker);
var getfincarddateexpiry = formatingdata(othermfindateexpiry);
var getfincarddateBirth = formatingdata(othermfindob);
var getworkpassdateissue = formatingdata(othermwfindoi);
var getworkpassdateexpiry = formatingdata(othermwfindoe);

/*var startdate = new Date(othermfindatetimepicker.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
var enddate =  new Date(othermfindateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var startdate1 = new Date(othermwfindoi.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
var enddate1 =  new Date(othermwfindoe.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var cur_date = new Date();
var dob =  new Date(othermfindob.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));*/

//alert(othermfingender);
var cur_date = new Date();
var dob = $('#othermfindob').datepicker('getDate');
var startdate = $('#othermfindoi').datepicker('getDate');
var enddate = $('#othermfindoe').datepicker('getDate');
var startdate1 = $('#othermwfindoi').datepicker('getDate');
var enddate1 = $('#othermwfindoe').datepicker('getDate');

       if(othermfinpassportno == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(othermfinnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(othermfincountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(othermfincountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(othermfindatetimepicker == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}        
        else if(othermfindateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}
        else if(othermfindob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(othermfinpob == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(othermfinpob == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
            $("#displaysuccessmsg").modal("show");
	} 
        else if(othermfinemployer == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(othermfinoccupation == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}         
        else if(othermfinnumber == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information");
             $("#displaysuccessmsg .modal-body").html("FIN Number cannot be empty");
             $("#displaysuccessmsg").modal("show");
	} 
        else if(othermfindatetimepicker != '' && getfincarddatetimepicker == true){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(othermfindateexpiry != '' && getfincarddateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(othermfindob != '' && getfincarddateBirth == true){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(othermwfindoi != "" && getworkpassdateissue == true){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(othermwfindoe != "" && getworkpassdateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(startdate >= enddate){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        } else if((othermwfindoi != "") && (othermwfindoe != "") && (startdate1 >= enddate1)){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        } else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Passport/Fin - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        }           
	else{
	   //alert("Saved Succesfully");
            localStorage.setItem(MANUAL_OTHER_FIN_PASSPORT, true);
	    $('#otherpreviewpassportfinmanually').modal('hide');
            $("#displaysuccessmsg .modal-title").html("Passport Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#displaysuccessmsg").modal("show");
	}


localStorage.setItem("othermfinpassportno",othermfinpassportno);
localStorage.setItem("othermfinnationality",othermfinnationality);
localStorage.setItem("othermfincountry",othermfincountry);
localStorage.setItem("othermfindatetimepicker",othermfindatetimepicker);
localStorage.setItem("othermfindateexpiry",othermfindateexpiry);
localStorage.setItem("othermfindob",othermfindob);
localStorage.setItem("othermfinpob",othermfinpob);
localStorage.setItem("othermfinemployer",othermfinemployer);
localStorage.setItem("othermfinoccupation",othermfinoccupation);
localStorage.setItem("othermwfindoi",othermwfindoi);
localStorage.setItem("othermwfindoe",othermwfindoe);
localStorage.setItem("othermfinnumber",othermfinnumber);
localStorage.setItem("othermfingender",othermfingender);

}

</script>
