<script type="text/javascript">
const ROLE_DIRECTOR = "Director";
const ROLE_SHAREHOLDER = "Shareholder";
const ROLE_DIRECTOR_SHAREHOLDER = "Director and Shareholder";
const ROLE_CORPORATE_SHAREHOLDER = "Corporate Shareholder";
const ADDRESS_POSTAL_CODE = "postal-code";
const ADDRESS_STREET_NAME = "street-name";
const ADDRESS_FLOOR = "floor";
const ADDRESS_UNIT = "unit";

const DIRECTOR_PREVIEW = "Director - Preview";
const SHAREHOLDER_PREVIEW = "Shareholder - Preview";
const DISHAREHOLDER_PREVIEW = "Director & Shareholder - Preview";
const CORPORATE_PREVIEW = "Corporate - Preview";

const ERR_MSG_PROPOSED_COMPANYNAME_EMPTY = "Proposed company name should not be empty";
const ERR_MSG_ADDRESS_TYPE_EMPTY = "Choose anyone address type";
const ERR_MSG_POSTALCODE_EMPTY = "Please enter your postal code";
const ERR_MSG_POSTALCODE_INVALID = "Please enter valid postal code";
const ERR_MSG_STREETNAME_EMPTY = "Please enter your street name";
const ERR_MSG_FLOOR_EMPTY = "Enter floor";
const ERR_MSG_FLOOR_INVALID = "Enter valid floor number";
const ERR_MSG_UNIT_EMPTY = "Enter unit";

const ERR_MSG_BUSINESS_ACTIVITY_EMPTY = "Business description should not be empty";
const ERR_MSG_COUNTRIES_OPERATION_EMPTY = "Please select countries of operation";
const ERR_MSG_ROLE_EMPTY = "Choose a role";
const ERR_MSG_FULLNAME_EMPTY = "You should provide your full name";
const ERR_MSG_RESIDENTIAL_STATUS_EMPTY = "Choose your residential status";
const ERR_MSG_PASSPORT_EMPTY = "Upload your passport";
const ERR_MSG_PROOFADDRESS_EMPTY = "Kindly upload your Proof of Address";
const ERR_MSG_ADDRESS_EMPTY = "Address field should be filled up";
const ERR_MSG_NRIC_FRONT_EMPTY = "Upload your NRIC front";
const ERR_MSG_NRIC_BACK_EMPTY = "Upload your NRIC back";
const ERR_MSG_FIN_FRONT_EMPTY = "Upload your FIN front";
const ERR_MSG_FIN_BACK_EMPTY = "Upload your FIN back";
const ERR_MSG_HANDPHONE_EMPTY = "hand phone should not be empty";
const ERR_MSG_HANDPHONE_OFFICEPHONE_EMPTY = "Please add your Phone number";
const ERR_MSG_PHONE_NAN = "Not a valid phone number";
const ERR_MSG_BUSINESS_PERSONAL_EMAIL_EMPTY = "Please enter your Email Address";
const ERR_MSG_EMAIL_FORMAT = "Invalid email";
const ERR_MSG_PEP_COUNTRY_EMPTY = "Select a country";
const ERR_MSG_PEP_ROLE_EMPTY = "Select a role";
const ERR_MSG_PEP_FROM_EMPTY = "Select an year";
const ERR_MSG_PEP_TO_EMPTY = "Select an year";
const ERR_MSG_PEP_FROM_TO_WRONG_RANGE = "Select the correct from and to years";
const ERR_MSG_ARE_YOU_OWNER_EMPTY = "Answer for the above question";
const ERR_MSG_SHAREHOLDER_NAME_EMPTY = "Share holder name should not be empty";
const ERR_MSG_COUNTRY_INCORPORATION_EMPTY = "Choose a country";
const ERR_MSG_ADD_ROLE_EMPTY = "Add a role (or) Choose 'No more additional role'";
const ERR_MSG_CUSTOMER_NAME_EMPTY = "Choose a customer name";
const ERR_MSG_INVALID_NUMBER = "Invalid Number";

const MANUAL_DIRECTOR_NONRESIDENT_PASSPORT = "director - passport informtaion manual entry";
const MANUAL_DIRECTOR_CITIZENPR_NRIC = "director - nric informtaion manual entry";
const MANUAL_DIRECTOR_FIN_PASSPORT = "director - fin/passport informtaion manual entry";

const MANUAL_SHAREHOLDER_NONRESIDENT_PASSPORT = "shareholder - passport informtaion manual entry";
const MANUAL_SHAREHOLDER_CITIZENPR_NRIC = "shareholder - nric informtaion manual entry";
const MANUAL_SHAREHOLDER_FIN_PASSPORT = "shareholder - fin/passport informtaion manual entry";

const MANUAL_DISHAREHOLDER_NONRESIDENT_PASSPORT = "dishareholder - passport informtaion manual entry";
const MANUAL_DISHAREHOLDER_CITIZENPR_NRIC = "dishareholder - nric informtaion manual entry";
const MANUAL_DISHAREHOLDER_FIN_PASSPORT = "dishareholder - fin/passport informtaion manual entry";

const MANUAL_CORPORATE_NONRESIDENT_PASSPORT = "corporate - passport informtaion manual entry";
const MANUAL_CORPORATE_CITIZENPR_NRIC = "corporate - nric informtaion manual entry";
const MANUAL_CORPORATE_FIN_PASSPORT = "corporate - fin/passport informtaion manual entry";

const MANUAL_OTHER_NONRESIDENT_PASSPORT = "manual other non resident passport entry";
const MANUAL_OTHER_CITIZEN_NRIC = "manual other citizen NRIC entry";
const MANUAL_OTHER_FIN_PASSPORT = "manual other fin passport entry";

const MANUAL_BO_NONRESIDENT_PASSPORT = "manual bo non resident passport entry";
const MANUAL_BO_CITIZEN_NRIC = "manual bo citizen NRIC entry";
const MANUAL_BO_FIN_PASSPORT = "manual bo fin passport entry";
const MANUAL_BO_CITIZENPR_NRIC = "manual bo citizen NRIC entry";

const CHART_SUCCESS_UPLOAD = "Chart has been uploaded in the following name";

/*const DEFAULT_IMAGE_PATH = "C:/fakepath/default.jpg";*/
const DEFAULT_IMAGE_PATH = "";

</script>

<?php
$path = getcwd();

define("BASE_URL", "https://www.winimy.ai/rikvinnew");
define("BASE_OCR_URL", "https://www.winimy.ai/rikvinapi/ocr");

define("DIRECTOR_SERVER_FILE_PATH", "https://winimy.ai/rikvinnew/directordocuments/");
define("SHAREHOLDER_SERVER_FILE_PATH", "https://winimy.ai/rikvinnew/shareholderdocuments/");
define("DISHAREHOLDER_SERVER_FILE_PATH", "https://winimy.ai/rikvinnew/didocuments/");
define("CORPORATE_SERVER_FILE_PATH", "https://winimy.ai/rikvinnew/corporatedocuments/");
define("OTHER_SERVER_FILE_PATH", "https://winimy.ai/rikvinnew/otherdocuments/");
define("BO_SERVER_FILE_PATH", "https://winimy.ai/rikvinnew/bodocuments/");
define("CHART_SERVER_FILE_PATH", "https://winimy.ai/rikvinnew/chartdocuments/");

define("SERVER_OTHER_FILE_PATH", $_SERVER["DOCUMENT_ROOT"]."/rikvinnew/otherdocuments/");
define("SERVER_BO_FILE_PATH", $_SERVER["DOCUMENT_ROOT"]."/rikvinnew/bodocuments/");
define("PDF_FILE_PATH", $_SERVER['DOCUMENT_ROOT']."/rikvinnew/attachments/");
//define("PDF_FILE_PATH", $path."/attachments/");
define("SERVER_CHART_FILE_PATH", $_SERVER['DOCUMENT_ROOT']."/rikvinnew/chartdocuments/");

define("FROM_EMAIL_JSON", "digitalmarketing@rikvin.com");
define("FROM_NAME_JSON", "InCorp Asia");
define("TO_EMAIL1_JSON", "tech@winimy.com");
define("TO_NAME1_JSON", "Winimy Tech team");

#define("TO_EMAIL2_JSON", "portal@incorp.asia");
define("TO_EMAIL2_JSON", "sugumarmbs@gmail.com");
define("TO_NAME2_JSON", "InCorp Tech team");

#define("CC_EMAIL1_JSON", "bhargavi@rikvin.com");
#define("CC_NAME1_JSON", "Bhargavi");
define("CC_EMAIL1_JSON", "mnkumar6@gmail.com");
define("CC_NAME1_JSON", "Mnkumar");

#define("BCC_EMAIL1_JSON", "glaiza@rikvin.com");
#define("BCC_NAME1_JSON", "Glaiza");
define("BCC_EMAIL1_JSON", "venkadesh.excito@gmail.com");
define("BCC_NAME1_JSON", "venki");

define("SUBJECT_JSON", "InCorp JSON");

define("FROM_EMAIL_PDF", "digitalmarketing@rikvin.com");
define("FROM_NAME_PDF", "InCorp Asia");
#define("TO_EMAIL1_PDF", "rikvin.marketing@gmail.com");
define("TO_EMAIL1_PDF", "swami@winimy.com");
define("TO_NAME1_PDF", "Swami");
define("TO_EMAIL2_PDF", "tech@winimy.com");
define("TO_NAME2_PDF", "Winimy Tech team");
define("TO_EMAIL3_PDF", "anirudh@winimy.com");
define("TO_NAME3_PDF", "Anirudh");

define("CC_EMAIL1_PDF", "beejay@winimy.com");
define("CC_NAME1_PDF", "Beejay");
define("BCC_EMAIL1_PDF", "shiva@winimy.com");
define("BCC_NAME1_PDF", "Shiva");

define("SUBJECT_PDF", "InCorp PDF - JSON");

define("SMTP_GMAIL", "smtp.gmail.com");
define("SMTP_USERNAME", "developer@winimy.com");
define("SMTP_PASSWORD", "wInImY321!");
define("SMTP_PORT", "587");

define("DIRECTOR_SERVER_FILE_PATH", "https://winimy.ai/rikvincopy/directordocuments/");
define("SHAREHOLDER_SERVER_FILE_PATH", "https://winimy.ai/rikvincopy/shareholderdocuments/");
define("DISHAREHOLDER_SERVER_FILE_PATH", "https://winimy.ai/rikvincopy/didocuments/");
define("CORPORATE_SERVER_FILE_PATH", "https://winimy.ai/rikvincopy/corporatedocuments/");
define("OTHER_SERVER_FILE_PATH", "https://winimy.ai/rikvincopy/otherdocuments/");
define("BO_SERVER_FILE_PATH", "https://winimy.ai/rikvincopy/bodocuments/");
?>
