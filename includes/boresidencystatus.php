<div id="bopassholder" class="choose-file" style="display:none;">
	<div class="row">
		<div class="col-lg-4">
			<label class="col-form-label" for="uploadPassport">Upload Passport</label>
		</div>
		<div class="col-lg-8">
			<div class="row mb-1">
				<div class="col-sm">
					<div class="input-group">
						<div class="custom-file">
							<input name="finuploadPassport" class="custom-file-input" id="finuploadPassport" type="file" onchange="checkPdfParse(this.files,1,bofinuploadpassportfile);">
							<label class="custom-file-label dashed-lines" id="finuploadPassportlabel">Passport</label>
						</div>
					</div>
				</div>
				<div class="col-sm">
					<button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#bopreviewpassportfinmanually"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button>
				</div>
			</div>
                        <div class="row" id="bofinloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
			<div class="row">
				<div class="col-sm-6" id="bopassportfin" style="display:none;">
					<button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bopreviewfinpassport"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button>
				</div>
			</div>
	         </div>
	</div>
<div class="row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="">Upload FIN</label>
	</div>
	<div class="col-lg-8">
		<div class="row mb-1">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="finfrontupload" class="custom-file-input" id="finfrontupload" type="file" onchange="checkPdfParse(this.files,1,bofinfrontfile);">
						<label class="custom-file-label dashed-lines" id="bofinfrontuploadlabel">FIN Card Front</label>
					</div>
				</div>
			</div>
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="finbackupload" class="custom-file-input" id="finbackupload" type="file" onchange="checkPdfParse(this.files,1,bofinbackfile);">
						<label class="custom-file-label dashed-lines" id="bofinbackuploadlabel">FIN Card Back</label>
					</div>
				</div>
			</div>
		</div>
                <div class="row" id="bofincardloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
                <div class="row">
		        <div id="bofincardfrontpreview" class="col-sm-6" style="display:none;">
		          <button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bofinfrontpreview" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your FIN Front details</button>
		         </div>
		         <div id="bofincardbackpreview" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bofinbackpreview" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your FIN Back details</button>
		         </div>
                </div>
	</div>
</div>

<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label>
	</div>
	<div class="col-lg-8">
		<div class="row">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="finaddress" class="custom-file-input" id="finaddress" type="file" onchange="bofinaddress(this.files);">
						<label class="custom-file-label dashed-lines" id="finaddresslabel">Address</label>
					</div>
				</div>
			</div>
			<div class="col-sm">
				<textarea class="form-control col-sm-12" name="finresaddress" id="finresaddress" rows="2" placeholder="Please key in residential address" ></textarea>
			</div>
		</div>
	</div>
</div>
</div>

<?php //Citizen PR ?>

<div id="bocitizenpr" class="choose-file" style="display: none;">
<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="">Upload NRIC</label>
	</div>
	<div class="col-lg-8">
		<div class="row mb-1">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="bonricfrontup" class="custom-file-input" id="bonricfrontup" type="file" onchange="checkPdfParse(this.files,1,bonricfrontupload);">
						<label class="custom-file-label dashed-lines" id="bonricfrontlabel">NRIC Front</label>
					</div>
				</div>
			</div>
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="bonricbackup" class="custom-file-input" id="bonricbackup" type="file" onchange="checkPdfParse(this.files,1,bonricbackupload);">
						<label class="custom-file-label dashed-lines" id="bonricbacklabel">NRIC Back</label>
					</div>
				</div>
			</div>
		</div>
                <div class="row" id="bonricloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
                <div class="row">
		        <div id="bonriccardfrontpreview" class="col-sm-6" style="display: none;">
		          <button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bonricfrontpreview" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Front details</button>
		         </div>
		         <div id="bonriccardbackpreview" class="col-sm-6" style="display: none;"><button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bonricbackpreview" style="margin-bottom:5px;"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your NRIC Back details</button>
		         </div>
                </div>
		<div class="row">
			<div class="col-sm-6">
				<button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#bopreviewnricmanually"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter NRIC information manually</button>
			</div>
		</div>

	</div>
</div>
</div>

<?php // Non resident ?>

<div id="bononresident" class="choose-file" style="display:none;">
<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="uploadPassport">Upload Passport</label>
	</div>
	<div class="col-lg-8">
		<div class="row mb-1">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="bouploadpassport" class="custom-file-input" id="bouploadpassport" type="file" onchange="checkPdfParse(this.files,1,bononuploadpassportfile);">
						<label id="bononresidentlabel" class="custom-file-label dashed-lines" for="">Passport</label>
					</div>
				</div>
			</div>
			<div class="col-sm">
				<button type="button" class="btn btn-style-1" data-toggle="modal" data-target="#bopreviewpassportmanually"><i class="fa fa-pencil pr-2" aria-hidden="true"></i> Enter Passport information manually</button>
			</div>
		</div>
                <div class="row" id="bononpassportloadingImage" style="display: none;"><div class="col-sm-12 text-center"><img src="images/image-loading.gif"></div></div>
		<div class="row">
			<div class="col-sm-6" id="bononpassport" style="display:none;">
				<button type="button" class="btn btn-style-1 btn-enabled" data-toggle="modal" data-target="#bopreviewpassport"><i class="fa fa-file-image-o pr-2" aria-hidden="true"></i> Preview your passport details</button>
			</div>
		</div>
	</div>
</div>
<div class="form-group row">
	<div class="col-lg-4">
		<label class="col-form-label required" for="" data-toggle="tooltip" data-placement="right" title="" data-original-title="i.e. Utility bill, bank statement, tenancy agreement, etc. (Latest within last two months)">Upload proof of address <i class="fa fa-question-circle"></i></label>
	</div>
	<div class="col-lg-8">
		<div class="row">
			<div class="col-sm">
				<div class="input-group">
					<div class="custom-file">
						<input name="boaddress" class="custom-file-input" id="boaddress" type="file" onchange="boaddress(this.files);">
						<label class="custom-file-label dashed-lines" id="boaddresslabel">Address</label>
					</div>
				</div>
			</div>
			<div class="col-sm">
				<textarea class="form-control col-sm-12" name="" id="" rows="2" placeholder="Please key in residential address" ></textarea>
			</div>
		</div>
	</div>
</div>
</div>

<script>

// For BO Residencystatus only
function getboresidencystatus(getvalue){

//alert(getvalue);
var arr = getvalue.split('_'); 
var uploadFields = $('.choose-file');

	if(getvalue == "bononresident") {
          //alert("fddf");

	   $("#bononresident").css("display", "block");
	   $("#bocitizenpr").css("display", "none");
	   $("#bopassholder").css("display", "none");
	}

	else if(getvalue == "bocitizenpr") {

	   $("#bononresident").css("display", "none");
	   $("#bocitizenpr").css("display", "block");
	   $("#bopassholder").css("display", "none");
	}

	else if(getvalue == "bopassholder") {

	   $("#bononresident").css("display", "none");
	   $("#bocitizenpr").css("display", "none");
	   $("#bopassholder").css("display", "block");
	}
        else {
 
           uploadFields.hide();

        }

}

var addrcount = 20;

function boaddress(getdata){

//alert(getdata);
var boposition = localStorage.getItem("bo_insert_position");
var bokey = localStorage.getItem("bo_insert_key_name");

var myImage = getdata[0];
var filename = myImage.name;
var finallabel = filename.slice(0, addrcount) + (filename.length > addrcount ? "..." : "");
var extension=getdata[0].name.split('.').pop().toLowerCase();
//alert(filename);
 $("#boaddresslabel").text(finallabel);
if((extension == "png") || (extension == "jpg") || (extension == "jpeg") || (extension == "gif") || (extension == "pdf") || (extension == "doc") || (extension == "docx") || (extension == "tiff"))
{
	var file = getdata[0]; 
	//var filename = file.name;
	var form = new FormData();
	form.append('image', file);
	$.ajax({
	    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
	    type: 'post',
	    data: form,
	    contentType: false,
	    processData: false,
	    success: function(response){

	    },
	});
}else{
   $("#displaysuccessmsg .modal-title").html("Error");
   $("#displaysuccessmsg .modal-body").html("Invalid File type");
   $("#displaysuccessmsg").modal("show");
}
}

function bofinaddress(getdata){

//alert(getdata);
var boposition = localStorage.getItem("bo_insert_position");
var bokey = localStorage.getItem("bo_insert_key_name");

var myImage = getdata[0];
var filename = myImage.name;
var finallabel = filename.slice(0, addrcount) + (filename.length > addrcount ? "..." : "");
var extension=getdata[0].name.split('.').pop().toLowerCase();
//alert(filename);

if((extension == "png") || (extension == "jpg") || (extension == "jpeg") || (extension == "gif") || (extension == "pdf") || (extension == "doc") || (extension == "docx") || (extension == "tiff"))
{

 $("#finaddresslabel").text(finallabel);

        var file = getdata[0]; 
	//var filename = file.name;
	var form = new FormData();
	form.append('image', file);
	$.ajax({
	    url: '<?php echo BASE_URL;?>/includes/uploadbodocuments.php',
	    type: 'post',
	    data: form,
	    contentType: false,
	    processData: false,
	    success: function(response){

	    },
	});
}else{
   $("#displaysuccessmsg .modal-title").html("Error");
   $("#displaysuccessmsg .modal-body").html("Invalid File type");
   $("#displaysuccessmsg").modal("show");
}


}
</script>

<?php include('includes/bouploadimages.php');?>
