<?php
	/* other form validations for declaration.php */
?>
<script type="text/javascript">

var customerName_alert = '<span class="font-italic small" id="residencyStatus-alert-msg">'+ERR_MSG_CUSTOMER_NAME_EMPTY+'</span>';
var alert_contact = '<div class="contactNumber1-alert" id="contactNumber1-alert" style="color: rgb(185, 13, 15);"><span class="font-italic small" id="contactNumber1-alert-msg">'+ERR_MSG_HANDPHONE_OFFICEPHONE_EMPTY+'</span></div>';
var bizEmail1_alert = '<span class="font-italic small" id="bizEmail1-alert-msg">'+ERR_MSG_BUSINESS_PERSONAL_EMAIL_EMPTY+'</span>';
var bizEmail1_invalid_alert = '<span class="font-italic small" id="bizEmail1-alert-msg">'+ERR_MSG_EMAIL_FORMAT+'</span>';
var full_name_alert = '<span class="font-italic small" id="fullName1-alert-msg">'+ERR_MSG_FULLNAME_EMPTY+'</span>';
var residencyStatus_alert = '<span class="font-italic small" id="residencyStatus-alert-msg">'+ERR_MSG_RESIDENTIAL_STATUS_EMPTY+'</span>';
var alert_invalid_contact = '<div class="contactNumber1-alert" id="contactNumber1-alert" style="color: rgb(185, 13, 15);"><span class="font-italic small" id="contactNumber1-alert-msg">'+ERR_MSG_PHONE_NAN+'</span></div>';


var isValidated = true;

$("#customer_dropdown").on('change', function(e){
		if (this.value.trim() != "") {
			$("#customerName-alert").html("");
		}
});
$("#full_name_2").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		$("#fullName-alert-2").html("");
	}
});

$("#email_2").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		$("#bizEmail1-alert-2").html("");
	}
});

$("#contactnumber_2").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		$("#contact-alert-2").html("");
	}
});
$("#residencyStatus").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		$("#residencyStatus-alert").html("");
	}
});
function hasManualEntry(manual_type){
	var value = localStorage.getItem(manual_type);
	console.log("manual item: "+manual_type+" val: "+value);
	return value;
}

function showPassportEmpty(show){
	var alert;
	var alertMsg;
	alert = $('#otheruploadpassport-alert');
	alertMsg = $('#otheruploadpassport-alert-msg');
		
	if (show == true) {
		alertMsg.text(ERR_MSG_PASSPORT_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showAddressProofEmpty(show){
	//console.log("showAddressProofEmpty: " + show);
	var alert;
	var alertMsg;
	alert = $('#otheraddress-alert');
	alertMsg = $('#otheraddress-alert-msg');	
	if (show == true) {
		//console.log("showing alert....");
		alertMsg.text(ERR_MSG_PROOFADDRESS_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showAddressEmpty(show){
	var alert;
	var alertMsg;
	alert = $('#otherresaddress-alert');
	alertMsg = $('#otherresaddress-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_ADDRESS_EMPTY);
		alert.show().parent().find('textarea').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().parent().find('textarea').css('border','').css('border-radius','');
	}
}
function showNRICEmpty(isFront, show){
	console.log("showNRICEmpty: "+isFront+", "+show);
	
	var alert;
	var alertMsg;
	if(isFront == true){
		alert = $('#othernricfrontup-alert');
		alertMsg = $('#othernricfrontup-alert-msg');
	}else{
		alert = $('#othernricbackup-alert');
		alertMsg = $('#othernricbackup-alert-msg');
	}
		
	if (show == true) {
		if(isFront == true){
			console.log("showing....");
			alertMsg.text(ERR_MSG_NRIC_FRONT_EMPTY);	
		}else{
			alertMsg.text(ERR_MSG_NRIC_BACK_EMPTY);	
		}		
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showFinPassportEmpty(show){
	var alert;
	var alertMsg;
	alert = $('#finuploadPassport-alert');
	alertMsg = $('#finuploadPassport-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_PASSPORT_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showFINCardEmpty(isFront, show){
	var alert;
	var alertMsg;
	if(isFront == true){
		alert = $('#finfrontupload-alert');
		alertMsg = $('#finfrontupload-alert-msg');
	}else{
		alert = $('#finbackupload-alert');
		alertMsg = $('#finbackupload-alert-msg');
	}	
	
	if (show == true) {
		if(isFront == true){
			alertMsg.text(ERR_MSG_FIN_FRONT_EMPTY);	
		}else{
			alertMsg.text(ERR_MSG_FIN_BACK_EMPTY);	
		}		
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showFinAddressProofEmpty(show){
	var alert;
	var alertMsg;
	alert = $('#finaddress-alert');
	alertMsg = $('#finaddress-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_PROOFADDRESS_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showFinAddressEmpty(show){
	var alert;
	var alertMsg;
	alert = $('#finresaddress-alert');
	alertMsg = $('#finresaddress-alert-msg');

	if (show == true) {
		alertMsg.text(ERR_MSG_ADDRESS_EMPTY);
                alert.show().parent().find('textarea').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().parent().find('textarea').css('border','').css('border-radius','');
	}
}
/*Other Nonresident*/
/*Upload Passpor*/
$("#otheruploadpassport").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showPassportEmpty(false);	
}
});
/*Upload proof of address*/
$("#otheraddress").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showAddressProofEmpty(false);	
}
});
/*Upload proof of address - textarea*/
$("#otherresaddress").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showAddressEmpty(false);	
}
});

/*SG*/
/*SG Upload NRIC - front*/
$("#othernricfrontup").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showNRICEmpty(true, false);	
}
});
/*Upload NRIC - back*/
$("#othernricbackup").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showNRICEmpty(false, false);	
}
});
/*FIN*/
/*Upload Passport*/

$("#finuploadPassport").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
		showFinPassportEmpty(false);
	}
});

/*Upload FIN - front*/

$("#finfrontupload").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showFINCardEmpty(true, false);	
}
});

/*Upload FIN - back*/

$("#finbackupload").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showFINCardEmpty(false, false);	
}
});

/*Upload proof of address*/
$("#finaddress").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showFinAddressProofEmpty(false);	
}
});
/*Upload proof of address - textarea*/
$("#finresaddress").on('change keyup paste', function(e){
	if (this.value.trim() != "") {
	showFinAddressEmpty(false);	
}
});
function validateOtherForm(){
	isValidated = true;
	var fullName; 
	var contactNo; 
	var residencyStatus;
	var email;
	var currency;

	var passport;
	var addressProof;
	var address;
	var finPassport;
	var finCardFront;
	var finCardBack;
	var finAddressProof;
	var finAddress;
	var nricfront;
	var nricback;

	fullName = document.getElementById("full_name_2").value;
	email = document.getElementById("email_2").value;
	contactNo = document.getElementById("contactnumber_2").value;
	residencyStatus = document.getElementById("residencyStatus").value;

	passport = $("#otheruploadpassport").val();
	addressProof = $("#otheraddress").val();
	address = $("#otherresaddress").val();	
	finPassport = $("#finuploadPassport").val();
	finCardFront = $("#finfrontupload").val();
	finCardBack = $("#finbackupload").val();
	finAddressProof = $("#finaddress").val();
	finAddress = $("#finresaddress").val();
	nricfront = $("#othernricfrontup").val();
	nricback = $("#othernricbackup").val();

	console.log("otheruploadpassport: "+passport);
	console.log("addressProof: "+addressProof);
	console.log("address: "+address);
	console.log("finPassport: "+finPassport);
	console.log("finCardFront: "+finCardFront);
	console.log("finCardBack: "+finCardBack);
	console.log("finAddressProof: "+finAddressProof);
	console.log("finAddress: "+finAddress);
	console.log("nricfront: "+nricfront);
	console.log("nricback: "+nricback);
	
	if(fullName=='') {
		$("#fullName-alert-2").html(full_name_alert);
		isValidated = false;
	} 
	if(residencyStatus == '') {
		$("#residencyStatus-alert").html(residencyStatus_alert);
		isValidated = false;
	}
	if(residencyStatus == "othernonresident"){
		if(hasManualEntry(MANUAL_OTHER_NONRESIDENT_PASSPORT) != "true"){
			if(passport == undefined || passport == ""){
				showPassportEmpty(true);	
				isValidated = false;
			}
		}
		if(address == undefined || address == ""){
			showAddressEmpty(true);	
			isValidated = false;
		} 
		if(addressProof == undefined || addressProof == ""){
			showAddressProofEmpty(true);	
			isValidated = false;
		}
	}
	if(residencyStatus == "othercitizenpr"){
		if(hasManualEntry(MANUAL_OTHER_CITIZEN_NRIC) != "true"){
			if(nricfront == undefined || nricfront == ""){
				showNRICEmpty(true, true);	
				isValidated = false;
			} if(nricback == undefined || nricback == ""){
				showNRICEmpty(false, true);	
				isValidated = false;
			}
		}
	}
	if(residencyStatus == "otherpassholder"){

		if(hasManualEntry(MANUAL_OTHER_FIN_PASSPORT) != "true"){
			if(finPassport == undefined || finPassport == ""){
				showFinPassportEmpty(true);	
				isValidated = false;
			} if(finCardFront == undefined || finCardFront == ""){
				showFINCardEmpty(true, true);	
				isValidated = false;
			} if(finCardBack == undefined || finCardBack == ""){
				showFINCardEmpty(false, true);	
				isValidated = false;
			}
		} 
		if(finAddressProof == undefined || finAddressProof == ""){
			showFinAddressProofEmpty(true);	
			isValidated = false;
		} if(finAddress == undefined || finAddress == ""){
			showFinAddressEmpty(true);	
			isValidated = false;
		}
	}
	if(email==''){
		console.log('email empty');
		$("#bizEmail1-alert-2").html(bizEmail1_alert);
		isValidated = false;
	}else if (!ValidateEmail(email)){
		$("#bizEmail1-alert-2").html(bizEmail1_invalid_alert);
		isValidated = false;
	}
	if(contactNo=='') {
		console.log('contact number empty');
		$("#contact-alert-2").html(alert_contact);
		isValidated = false;
	}
	if(!(contactNo == undefined || contactNo == "")){
		if(isNaN(contactNo)|| contactNo.trim().length < 8){
                        $("#contact-alert-2").html(alert_invalid_contact); 
			isValidated = false;
		}	
	} 
		

        
	localStorage.setItem("otheruploadpassport", passport);
	localStorage.setItem("otheraddress", addressProof);
	localStorage.setItem("otherresaddress", address);
	localStorage.setItem("otherfinuploadPassport", finPassport);
	localStorage.setItem("otherfinfrontupload", finCardFront);
	localStorage.setItem("otherfinbackupload", finCardBack);
	localStorage.setItem("otherfinaddress", finAddressProof);
	localStorage.setItem("otherfinresaddress", finAddress);
	localStorage.setItem("othernricfrontup", nricfront);
	localStorage.setItem("othernricbackup", nricback);
        localStorage.setItem("otherresidencystatus", residencyStatus);
	

	
	return isValidated;
}

</script>
