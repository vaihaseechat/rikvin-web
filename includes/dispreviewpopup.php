<script>
var parsecount = 20;

function dinonuploadpassportfile(getdata,getcounter,filename){

$("#dishareuploadpassportfileloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
//alert(filename);

        $.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error; 


                         if(Errorstatus == 'Poor Image Quality') {                           
                           
                           $("#displaysuccessmsg .modal-title").html("DI Shareholder - Error");
		           $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		           $("#displaysuccessmsg").modal("show");
                           $("#bdinonresidentpreviewpassport_"+getcounter).css("display", "none");
                           $("#dishareuploadpassportfileloadingImage_"+getcounter).css("display", "none");
                           $("#dinonresidentlabel_"+getcounter).text();

                         } else{
                            //alert("fdfdfdf");
                            $("#bdinonresidentpreviewpassport_"+getcounter).css("display", "block");
                            $("#dishareuploadpassportfileloadingImage_"+getcounter).css("display", "none");
                            $("#dinonresidentlabel_"+getcounter).text(finallabel);


                            var fieldHTML ='<div class="modal fade" id="dinonresidentpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fieldHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fieldHTML += '<div class="modal-content row">';
				fieldHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - Non Resident - Director and Shareholder #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fieldHTML += '<div class="col-lg-12 background-grey-2">';
				fieldHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="dinonresidentpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="dinonresidentsurname[]" class="form-control" id="dinonresidentpsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="dinonresidentgivenName[]" class="form-control" id="dinonresidentpgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="dinonresidentpassportNumber[]" class="form-control" id="dinonresidentppassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="dinonresidentnationality[]" id="dinonresidentpnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="dinonresidentpcountry_'+getcounter+'" name="dinonresidentcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="dinonresidentmgender_'+getcounter+'" name="dinonresidentpgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="dinonresidentpgender[]" id="dinonresidentfgender_'+getcounter+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="dinonresidentpdob_'+getcounter+'" class="form-control" name="dinonresidentdob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="dinonresidentpdateexpiry_'+getcounter+'" class="form-control" name="dinonresidentdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 /*fieldHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="dinonresidentdob_'+getcounter+'" class="form-control" name="dinonresidentdob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="dinonresidentplaceofBirth_'+getcounter+'" name="dinonresidentplaceofBirth[]"></select></div></div></div></div>';*/

                                 fieldHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="dishareholderpopup('+getcounter+',\'dinonresident\');">Save</a></div></div></div>';
				fieldHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fieldHTML);

                                 //Passport Preview - Non resident

				$('#dinonresidentpnationality_'+getcounter).html(countriesOption);
				$('#dinonresidentpnationality_'+getcounter).chosen(); 

				$('#dinonresidentpcountry_'+getcounter).html(countriesOption);
				$('#dinonresidentpcountry_'+getcounter).chosen(); 

				//$('#dinonresidentplaceofBirth_'+getcounter).html(countriesOption);
				//$('#dinonresidentplaceofBirth_'+getcounter).chosen(); 

                                 // Declaring variables for preview
                                $('#dinonresidentpdob_'+getcounter).datepicker({   
		                     beforeShow:function () {
		                     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		                     }
	                         });

                                 $('#dinonresidentpdateexpiry_'+getcounter).datepicker({   
					beforeShow:function () {
					setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
					}
				});  
                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = ''; 
                                 var PlaceofBirth = '';                 
		                 //alert(sex);

                                 // Displaying values from OCR - API

                                 $('#dinonresidentpgivenName_'+getcounter).val(gname);
                                 $('#dinonresidentpsurname_'+getcounter).val(Surname);
                                 $('#dinonresidentppassportNumber_'+getcounter).val(passportno);
                                 //$('#dinonresidentdatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#dinonresidentpdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#dinonresidentpdob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#dinonresidentmgender_'+getcounter).prop('checked', true);
                                  $('#dinonresidentfgender_'+getcounter).prop('checked', false); 
                                 }
                                 else if(sex == 'Female'){
                                  $('#dinonresidentfgender_'+getcounter).prop('checked', true);
                                  $('#dinonresidentmgender_'+getcounter).prop('checked', false);
                                 }
                                 $('#dinonresidentpnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#dinonresidentpcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 //$('#dinonresidentplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth); 
                                 $('#dinonresidentpreviewpassport_'+getcounter).modal('show'); 

                         

                         }     

                    }

        }); 
 
}

//NRIC front

function dinricfront(getdata,getcounter,filename){

$("#dishareuploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

	$.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("DI Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bdinricfrontpreview_"+getcounter).css("display", "none");
                    $("#dishareuploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#dinricfrontlabel_"+getcounter).text();

		 } else{

                   $("#bdinricfrontpreview_"+getcounter).css("display", "block");
                   $("#dishareuploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#dinricfrontlabel_"+getcounter).text(finallabel);  

                   var nricfrontfield = '<div class="modal fade" id="dinricfrontpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfrontfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfrontfield += '<div class="modal-content row">';
                     nricfrontfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Front Information - Director and Shareholder#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfrontfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="dinricfrontimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="dipreviewfrontnricnumber[]" class="form-control" id="dipreviewfrontnricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="dipreviewnricfrontmgender_'+getcounter+'" name="dipreviewnricfrontgender[]" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input id="dipreviewnricfrontfgender_'+getcounter+'" name="dipreviewnricfrontgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricfrontfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="dipreviewnricfrontdob_'+getcounter+'" class="form-control" name="dipreviewnricfrontdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="dipreviewnricfrontnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="dipreviewnricfrontnationality_'+getcounter+'"></select></div></div></div></div>';

                     nricfrontfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dipreviewnricfrontpostcode_'+getcounter+'" name="dipreviewnricfrontpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="dipreviewnricfrontstreetname[]" id="dipreviewnricfrontstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dipreviewnricfrontfloor_'+getcounter+'" name="dipreviewnricfrontfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dipreviewnricfrontunit_'+getcounter+'" name="dipreviewnricfrontunit[]" placeholder="Unit" type="text"></div></div>';

                     nricfrontfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="dishareholderpopup('+getcounter+',\'dinricfront\');">Save</a></div></div></div>';

                     nricfrontfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricfrontfield);

                     $('#dipreviewnricfrontnationality_'+getcounter).html(countriesOption);
                     $('#dipreviewnricfrontnationality_'+getcounter).chosen();   

                     $('#dipreviewnricfrontdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      });
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         if(sex == 'Male'){
                          $('#dipreviewnricfrontmgender_'+getcounter).prop('checked', true);
                          $('#dipreviewnricfrontfgender_'+getcounter).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#dipreviewnricfrontfgender_'+getcounter).prop('checked', true);
                          $('#dipreviewnricfrontmgender_'+getcounter).prop('checked', false);
                         }

                         $('#dipreviewfrontnricnumber_'+getcounter).val(nricno);
                         $('#dipreviewnricfrontnationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#dipreviewnricfrontdob_'+getcounter).val(dobs);
                         $('#dinricfrontpreview_'+getcounter).modal('show');
                      

		 }
	    }
	}); 
}

//NRIC back

function dinricback(getdata,getcounter,filename){

$("#dishareuploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("DI Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bdinricbackpreview_"+getcounter).css("display", "none");
                    $("#dishareuploadnricloadingImage_"+getcounter).css("display", "none"); 
                    $("#dinricbacklabel_"+getcounter).text();

		 } else{

                   $("#bdinricbackpreview_"+getcounter).css("display", "block"); 
                   $("#dishareuploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#dinricbacklabel_"+getcounter).text(finallabel);

                   var nricbackfield = '<div class="modal fade" id="dinricbackpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricbackfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricbackfield += '<div class="modal-content row">';
                     nricbackfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Back Information - Director and Shareholder #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricbackfield += '<div class="col-lg-12 background-grey-2">'; 
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="dinricbackimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="dipreviewbacknricnumber[]" class="form-control" id="dipreviewbacknricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="dipreviewnricbackmgender_'+getcounter+'" name="dipreviewnricbackgender[]" class="radio-red" type="radio" checked> Male</label><label class="radio-inline"><input id="dipreviewnricbackfgender_'+getcounter+'" name="dipreviewnricbackgender[]" class="radio-red" type="radio"> Female</label></div></div></div></div>';

                     nricbackfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="dipreviewnricbackdob_'+getcounter+'" class="form-control" name="dipreviewnricbackdob[]" placeholder="DD/MM/YYYY" type="text"><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="dipreviewnricbacknationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="dipreviewnricbacknationality_'+getcounter+'"></select></div></div></div></div>';

                     nricbackfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dipreviewnricbackpostcode_'+getcounter+'" name="dipreviewnricbackpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="dipreviewnricbackstreetname[]" id="dipreviewnricbackstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dipreviewnricbackfloor_'+getcounter+'" name="dipreviewnricbackfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="dipreviewnricbackunit_'+getcounter+'" name="dipreviewnricbackunit[]" placeholder="Unit" type="text"></div></div>';

                     nricbackfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="dishareholderpopup('+getcounter+',\'dinricback\');">Save</a></div></div></div>';

                     nricbackfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricbackfield);

                     $('#dipreviewnricbacknationality_'+getcounter).html(countriesOption);
                     $('#dipreviewnricbacknationality_'+getcounter).chosen();  

                      $('#dipreviewnricbackdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      });
 
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;

                         if(sex == 'Male'){
                          $('#dipreviewnricbackmgender_'+getcounter).prop('checked', true);
                          $('#dipreviewnricbackfgender_'+getcounter).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#dipreviewnricbackfgender_'+getcounter).prop('checked', true);
                          $('#dipreviewnricbackmgender_'+getcounter).prop('checked', false);
                         }

                         $('#dipreviewbacknricnumber_'+getcounter).val(nricno);
                         $('#dipreviewnricbacknationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#dipreviewnricbackdob_'+getcounter).val(dobs);
                         $('#dinricbackpreview_'+getcounter).modal('show');
 
		 }
	    }
   }); 

}

//FIn Passport

function difinuploadpassportfile(getdata,getcounter,filename){

$("#dishareuploadPassportloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/passport/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("DI Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bdifincardpreviewpassport_"+getcounter).css("display", "none");
                    $("#dishareuploadPassportloadingImage_"+getcounter).css("display", "none"); 
                    $("#difinpassportlabel_"+getcounter).text();

		 } else{

                   $("#bdifincardpreviewpassport_"+getcounter).css("display", "block"); 
                   $("#dishareuploadPassportloadingImage_"+getcounter).css("display", "none");
                   $("#difinpassportlabel_"+getcounter).text(finallabel);


                    var fincardHTML ='<div class="modal fade" id="difincardpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fincardHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fincardHTML += '<div class="modal-content row">';
				fincardHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - WorkPass - Director and Shareholder #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fincardHTML += '<div class="col-lg-12 background-grey-2">';
				fincardHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="difincardpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="difincardsurname[]" class="form-control" id="difincardsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="difincardgivenName[]" class="form-control" id="difincardgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="difincardpassportNumber[]" class="form-control" id="difincardppassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="difincardpnationality[]" id="difincardpnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="difincardpcountry_'+getcounter+'" name="difincardpcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="difincardmgender_'+getcounter+'" name="difincardgender[]" class="radio-red" checked>Male</label><label class="radio-inline"><input type="radio" name="difincardgender[]" id="difincardfgender_'+getcounter+'" class="radio-red" value="female"> Female</label></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="difincarddatetimepicker_'+getcounter+'" class="form-control" name="difincarddatetimepicker[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="difincardpdateexpiry_'+getcounter+'" class="form-control" name="difincarddateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                                 fincardHTML +='<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group"><input type="text" id="difincarddob_'+getcounter+'" class="form-control" name="difincarddob[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Place of Birth</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="difincardplaceofBirth_'+getcounter+'" name="difincardplaceofBirth[]"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="dishareholderpopup('+getcounter+',\'difinpassport\');">Save</a></div></div></div>';
				fincardHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fincardHTML);

                                 //Passport Preview - Work Pass

				$('#difincardpnationality_'+getcounter).html(countriesOption);
				$('#difincardpnationality_'+getcounter).chosen(); 

				$('#difincardpcountry_'+getcounter).html(countriesOption);
				$('#difincardpcountry_'+getcounter).chosen(); 

				$('#difincardplaceofBirth_'+getcounter).html(countriesOption);
				$('#difincardplaceofBirth_'+getcounter).chosen(); 

                                 // Declaring variables for preview
                                $('#difincarddob_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 });  

                                 $('#difincarddatetimepicker_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 });

                                 $('#difincardpdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 });

		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;  
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = ''; 
                                 var PlaceofBirth = '';                 
		                 

                                 // Displaying values from OCR - API

                                 $('#difincardgivenName_'+getcounter).val(gname);
                                 $('#difincardsurname_'+getcounter).val(Surname);
                                 $('#difincardppassportNumber_'+getcounter).val(passportno);
                                 $('#difincarddatetimepicker_'+getcounter).val(DateofIssue);
                                 $('#difincardpdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#difincarddob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#difincardmgender_'+getcounter).prop('checked', true);
                                  $('#difincardfgender_'+getcounter).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#difincardfgender_'+getcounter).prop('checked', true);
                                  $('#difincardmgender_'+getcounter).prop('checked', false); 
                                 }
                                 $('#difincardpnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#difincardpcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 $('#difincardplaceofBirth_'+getcounter+'_chosen span').text(PlaceofBirth);  
                                 $('#difincardpreviewpassport_'+getcounter).modal('show');     
 
		 }
	    }
   }); 

}

// Fin front

function difincardfront(getdata,getcounter,filename){

$("#dishareuploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("DI Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bdifincardfrontpreview_"+getcounter).css("display", "none"); 
                    $("#dishareuploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#difincardfrontlabel_"+getcounter).text();

		 } else{

                   $("#bdifincardfrontpreview_"+getcounter).css("display", "block"); 
                   $("#dishareuploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#difincardfrontlabel_"+getcounter).text(finallabel);


                var fincardfrontHTML ='<div class="modal fade" id="difincardfrontpreview_'+getcounter+'" tabindex="-1" role="dialog">';
		fincardfrontHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincardfrontHTML += '<div class="modal-content row">';
		fincardfrontHTML += '<div class="modal-header"><h5 class="modal-title" id="difincardfrontpreview">Director and Shareholder #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		 fincardfrontHTML += '<div class="col-lg-12 background-grey-2">';

                 fincardfrontHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="difincardfrontpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="difincardfrontemployer[]" class="form-control" id="difincardfrontemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="difincardfrontoccupation[]" class="form-control" id="difincardfrontoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

                  fincardfrontHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="difincardfrontdateissue_'+getcounter+'" class="form-control" name="difincardfrontdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="difincardfrontdateexpiry_'+getcounter+'" class="form-control" name="difincardfrontdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="diwfincardfrontNumber[]" class="form-control" id="diwfincardfrontNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

                 fincardfrontHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="dishareholderpopup('+getcounter+',\'difincardfront\');">Save</a></div></div></div>';
		fincardfrontHTML += '</div></div></div></div>';   
                   
                 $('body').find('#additionalRole:last').after(fincardfrontHTML);

                 $('#difincardfrontdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
		 }); 

                 $('#difincardfrontdateexpiry_'+getcounter).datepicker({   
			     beforeShow:function () {
			     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
			     }
		 });

                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#difincardfrontdateissue_'+getcounter).val(DateofIssue);
                         $('#difincardfrontpreview_'+getcounter).modal('show');
 
		 }
	    }
   }); 

}

// Fin back

function difincardback(getdata,getcounter,filename){

$("#dishareuploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		 if(Errorstatus == 'Poor Image Quality') { 

                    $("#displaysuccessmsg .modal-title").html("DI Shareholder - Error");
		    $("#displaysuccessmsg .modal-body").html("Poor Image Quality");
		    $("#displaysuccessmsg").modal("show");
                    $("#bdifincardbackpreview_"+getcounter).css("display", "none"); 
                    $("#dishareuploadfincardloadingImage_"+getcounter).css("display", "none"); 
                    $("#difincardbacklabel_"+getcounter).text();

		 } else{

                   $("#bdifincardbackpreview_"+getcounter).css("display", "block");  
                   $("#dishareuploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#difincardbacklabel_"+getcounter).text(finallabel);


                   var fincardbackHTML ='<div class="modal fade" id="difincardbackpreview_'+getcounter+'" tabindex="-1" role="dialog">';
			fincardbackHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
			fincardbackHTML += '<div class="modal-content row">';
			fincardbackHTML += '<div class="modal-header"><h5 class="modal-title" id="difincardbackpreview">di #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
			 fincardbackHTML += '<div class="col-lg-12 background-grey-2">';

                         fincardbackHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="difincardbackpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
		     
		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="difincardbackemployer[]" class="form-control" id="difincardbackemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="difincardbackoccupation[]" class="form-control" id="difincardbackoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

		          fincardbackHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

		          fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Issue</label><div class="col-sm-12"><div class="input-group"><input type="text" id="difincardbackdateissue_'+getcounter+'" class="form-control" name="difincardbackdateissue[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Expiry</label><div class="col-sm-12"><div class="input-group"><input type="text" id="difincardbackdateexpiry_'+getcounter+'" class="form-control" name="difincardbackdateexpiry[]" value=""><label class="input-group-addon btn date-picker"><span class="fa fa-calendar open-datetimepicker"></span></label></div></div></div></div></div>';

		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="diwfincardbackNumber[]" class="form-control" id="diwfincardbackNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

		         fincardbackHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="dishareholderpopup('+getcounter+',\'difincardback\');">Save</a></div></div></div>';
			fincardbackHTML += '</div></div></div></div>';   
		           
		         $('body').find('#additionalRole:last').after(fincardbackHTML);
                         
                         $('#difincardbackdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         $('#difincardbackdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#difincardbackdateissue_'+getcounter).val(DateofIssue);
                         $('#difincardbackpreview_'+getcounter).modal('show');
 
		 }
	    }
   }); 

}



</script>
