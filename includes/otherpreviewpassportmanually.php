<div class="modal fade"  data-keyboard="true"   id="otherpreviewpassportmanually" tabindex="-1" role="dialog" style="display: none;" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">Passport Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">					
					
					<div class="row mt-3">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="passportNumber">Passport Number</label>
								<div class="col-sm-12"><input name="othermpassportNumber" class="form-control" id="othermpassportNumber" placeholder="" value="" type="text"></div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-12 col-form-label required" for="nationality">Nationality</label>
								<select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othermnationality" name="othermnationality"></select>
							</div>
						</div>                                               
						
					</div>
                                        <div class="row">
                                           <div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-12 col-form-label required" for="">Country of Issue</label>
                                                                <select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othermcntryissue" name="othermcntryissue"></select>							
							</div>
					   </div>
                                            <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="">Gender</label>
								<div class="col-sm-12">
									<label class="radio-inline mr-3">
									  <input value="Male" name="othermoptradio" class="radio-red" type="radio" checked> Male
									</label>
									<label class="radio-inline">
									  <input value="Female" name="othermoptradio" class="radio-red" type="radio"> Female
									</label>
								</div>
							</div>
					     </div>   
                                        </div>
					<div class="row">
						
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfIssue">Date of Issue</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
										<input id="othermdatetimepicker" class="form-control" name="othermdatetimepicker" placeholder="DD/MM/YYYY" type="text" maxlength="10">
										
									</div>
								</div>
							</div>
						</div>
                                                <div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfExpiry">Date of Expiry</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
									<input id="othermdateexpiry" class="form-control" name="othermdateexpiry" placeholder="DD/MM/YYYY" type="text" maxlength="10">
									
								</div>	
								</div>							
							</div>
						</div>
						
					</div>			
					<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label required" for="dateOfBirth">Date of Birth</label>
								<div class="col-sm-12">
									<div class="input-group dateicon">
									<input id="othermdob" class="form-control" name="othermdob" placeholder="DD/MM/YYYY" type="text" maxlength="10">
									
								</div>	
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
							<label class="col-sm-12 col-form-label required" for="placeOfBirth">Place of Birth</label><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="othermpob" name="othermpob"></select>
						</div>
						</div>
					</div>
					<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
								<button class="btn btn-next" onclick="savepreviewmanually();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<script>
function savepreviewmanually(){

if( $('#otherpreviewpassportmanually').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#otherpreviewpassportmanually').css("overflow","auto");
}

var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

//var othermsurname = document.getElementById("othermsurname").value;
//var othermgivenname = document.getElementById("othermgivenName").value;
var othermpassportno = document.getElementById("othermpassportNumber").value;
var othermnationality = $('#othermnationality_chosen span').text();
var othermcountry = $('#othermcntryissue_chosen span').text();
var othermgender = $("input[name='othermoptradio[]']:checked").val();
var othermdatetimepicker = document.getElementById("othermdatetimepicker").value;
var othermdob = document.getElementById("othermdob").value;
var othermdateexpiry = document.getElementById("othermdateexpiry").value;
var othermpob = $('#othermpob_chosen span').text();

var getnondatetimepicker = formatingdata(othermdatetimepicker);
var getnondateexpiry = formatingdata(othermdateexpiry);
var getnondateBirth = formatingdata(othermdob);

//var startdate = new Date(othermdatetimepicker.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));
//var enddate =  new Date(othermdateexpiry.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

//var cur_date = new Date();
//var dob =  new Date(othermdob.replace( /(\d{2})\/(\d{2})\/(\d{2})/, "$2/$1/$3"));

var cur_date = new Date();
var dob = $('#othermdob').datepicker('getDate');
var startdate = $('#othermdatetimepicker').datepicker('getDate');
var enddate = $('#othermdateexpiry').datepicker('getDate');

        if(othermpassportno == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(othermnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(othermcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(othermdatetimepicker == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(othermdateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");

	} 
        else if(othermdob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(othermpob == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
            $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(othermdatetimepicker != '' && getnondatetimepicker == true){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        } 
        else if(othermdateexpiry != '' && getnondateexpiry == true){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(othermdob != '' && getnondateBirth == true){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid");
             $("#displaysuccessmsg").modal("show"); 
        }else if(othermdob.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Birth is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if(othermdateexpiry.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }else if(othermdatetimepicker.length < 10){
              $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue is Invalid, Please enter the date in following format DD-MM-YYYY");
             $("#displaysuccessmsg").modal("show"); 
        }
        else if(startdate >= enddate){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be later or equal than expiry date");
             $("#displaysuccessmsg").modal("show"); 
        } else if(dob >= cur_date){
             $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
             $("#displaysuccessmsg .modal-body").html("Date of birth cannot exceed current date");
             $("#displaysuccessmsg").modal("show"); 
        }        
	else{
	   //alert("Saved Succesfully");
            localStorage.setItem(MANUAL_OTHER_NONRESIDENT_PASSPORT, true);
	    $('#otherpreviewpassportmanually').modal('hide');
            $("#displaysuccessmsg .modal-title").html("Passport Information - Manual");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#displaysuccessmsg").modal("show");
	}

localStorage.setItem("othermdatetimepicker",othermdatetimepicker);
localStorage.setItem("othermpob",othermpob);
localStorage.setItem("othermpassortno",othermpassportno);
localStorage.setItem("othermnationality",othermnationality);
localStorage.setItem("othermcntry",othermcountry);
localStorage.setItem("othermgender",othermgender);
localStorage.setItem("othermdob",othermdob);
localStorage.setItem("othermdateexpiry",othermdateexpiry);

}

</script>
