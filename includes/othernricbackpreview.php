<div class="modal fade"  data-keyboard="true"   id="othernricbackpreview" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-dialog-centered" role="document">
			 <div class="modal-content row">
				<div class="modal-header">
					<h5 class="modal-title" id="">NRIC  Back Information</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
					</button>
				</div>
				<div class="col-lg-12 background-grey-2 popheight">					
					<div class="row mt-3">
						<div class="col-sm">
							<div class="form-group row">
								<div class="col-sm-12">
									Please check details below.
								</div>
								<div class="col-sm-12">
									<div class="passport-scan-copy" id="othernricbackimage"><img src="" alt=""></div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-3" style="display:none;">
						<div class="col-md-4">
							<div class="form-group row">
						        <label class="col-sm-12 col-form-label required">NRIC Number</label>
							<div class="col-sm-12">
							<input name="otherpreviewbacknricnumber" class="form-control" id="otherpreviewbacknricnumber" value="" type="text">
							</div>
						</div>
						</div>
						<div class="col-md-4">
							<div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12">
							<label class="radio-inline mr-3"><input id="otherpreviewnricbackmgender" name="otherpreviewbacknricgender" class="radio-red" type="radio" value="Male"> Male</label>
							<label class="radio-inline"><input id="otherpreviewnricbackfgender" name="otherpreviewbacknricgender" class="radio-red" checked type="radio" value="Female"> Female</label>
							</div>
						</div>
						</div>
						<div class="col-md-4">
							<div class="form-group row">
								<label class="col-sm-12 col-form-label" for="dateofissue">Date of Issue</label>
								<div class="col-sm-12">
                                                                 <div class="input-group dateicon">
                                                                  <input name="otherpreviewbacknricdoi" class="form-control" id="otherpreviewbacknricdoi"  value="" type="text" >
                                                                </div></div>
							</div>
						</div>                                              
					</div>
                                        <div class="row" style="display:none;">
		                                  <div class="col-md-4">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Country of Birth</label>
							<div class="col-sm-12">
		                                        <select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="otherpreviewbacknriccob" name="otherpreviewbacknriccob"></select>
		                                        </div>
							</div>
						   </div>
                                        </div>
                                        <div class="row my-3">
						<div class="col-sm">
							<h4 class="font-weight-otherld">Residencial Address</h4>
						</div>
					</div>				
					<div class="row">
		                                  <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Postcode</label>
								<div class="col-sm-12">	
                                                                <input class="form-control" id="otherpreviewbacknricpostcode" name="otherpreviewbacknricpostcode" placeholder="Postal Code" type="text">			                                
				                                </div>
							</div>
						   </div>

                                                    <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Streetname</label>
								<div class="col-sm-12">
				                                <textarea name="otherpreviewbacknricstreetname" id="otherpreviewbacknricstreetname" class="form-control" rows="3" placeholder="Street Name"></textarea> 
				                                </div>
							</div>
					            </div>
                                             	   </div>
                                         <div class="row">
                                                   <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Floor</label>
								<div class="col-sm-12">
				                                <input class="form-control" id="otherpreviewbacknricfloor" name="otherpreviewbacknricfloor" placeholder="Floor" type="text">
				                                </div>
							</div>
						   </div>
                                                   <div class="col-md-6">
							<div class="form-group row">
							<label class="col-sm-12 col-form-label required">Unit</label>
								<div class="col-sm-12">
				                                <input class="form-control" id="otherpreviewbacknricunit" name="otherpreviewbacknricunit" placeholder="Unit" type="text">
				                                </div>
							</div>
						   </div>
                                        </div>
		        		<div class="row my-5">						
						<div class="col-sm-12"> 
							<div class="text-center">
                                                                <input type="hidden" name="othernricbackfilename" id="othernricbackfilename" value="">   
								<button class="btn btn-next" onclick="savenricback();">Save</button> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

<script>
function savenricback(){

if( $('#othernricbackpreview').hasClass('modal fade show') ) {
	console.log("Modal is open");
	$('body').css("overflow","hidden");
	$('#othernricbackpreview').css("overflow","auto");
}

var otherposition = localStorage.getItem("other_insert_position");
var otherkey = localStorage.getItem("other_insert_key_name");

var othernricbackfilename = document.getElementById("othernricbackfilename").value;
var otherpreviewbacknricnumber = document.getElementById("otherpreviewbacknricnumber").value;
//var othergivenname = document.getElementById("othergivenName").value;
var otherpreviewbacknricdoi = document.getElementById("otherpreviewbacknricdoi").value;
var otherpreviewbacknriccob = $('#otherpreviewbacknriccob_chosen span').text();
var otherpreviewbacknricpostcode = document.getElementById("otherpreviewbacknricpostcode").value;
var otherpreviewbacknricstreetname = document.getElementById("otherpreviewbacknricstreetname").value;
var otherpreviewbacknricfloor = document.getElementById("otherpreviewbacknricfloor").value;
var otherpreviewbacknricunit = document.getElementById("otherpreviewbacknricunit").value;
var otherpreviewbacknricgender = $("input[name='otherpreviewbacknricgender']:checked").val();


        /*if(otherpreviewbacknricnumber == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewbacknricdoi == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
	else if(otherpreviewbacknriccob == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}else if(otherpreviewfrontnricpostcode != ''){          
    	if(isNaN(otherpreviewfrontnricpostcode)){
	        $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
	        $("#displaysuccessmsg .modal-body").html("Invalid Postcode");
	        $("#displaysuccessmsg").modal("show");
    	}
	}else if(otherpreviewfrontnricfloor != ''){
        if(isNaN(otherpreviewfrontnricfloor)){  
            $("#displaysuccessmsg .modal-title").html("NRIC Front Information");
            $("#displaysuccessmsg .modal-body").html("Invalid Floor");
            $("#displaysuccessmsg").modal("show");
        }    
	}*/
    if(otherpreviewbacknricpostcode == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}else if(isNaN(otherpreviewbacknricpostcode)){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Invalid Postcode");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewbacknricstreetname == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewbacknricfloor == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}else if(isNaN(otherpreviewbacknricfloor)){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Invalid Floor");
            $("#displaysuccessmsg").modal("show");

	}
        else if(otherpreviewbacknricunit == ''){
          
            //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
            $("#displaysuccessmsg").modal("show");

	} 
        else{
	   //alert("Saved Succesfully");
	    $('#othernricbackpreview').modal('hide');
            $("#displaysuccessmsg .modal-title").html("NRIC back Information");
            $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
            $('body').css("overflow","auto");
            $("#othernricmanual").prop('disabled', true);
            $("#displaysuccessmsg").modal("show");
	}


localStorage.setItem("otherpreviewbacknricnumber",otherpreviewbacknricnumber);
localStorage.setItem("otherpreviewbacknricdoi",otherpreviewbacknricdoi);
localStorage.setItem("otherpreviewbacknriccob",otherpreviewbacknriccob);
localStorage.setItem("otherpreviewbacknricpostcode",otherpreviewbacknricpostcode);
localStorage.setItem("otherpreviewbacknricstreetname",otherpreviewbacknricstreetname);
localStorage.setItem("otherpreviewbacknricfloor",otherpreviewbacknricfloor);
localStorage.setItem("otherpreviewbacknricunit",otherpreviewbacknricunit);
localStorage.setItem("otherpreviewbacknricgender",otherpreviewbacknricgender);


}

</script>
