<script>
function shareholderpopup(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);

//Storing shareholder Non Resident

if(getresidencystatus == 'shareholdernonresident'){

var shareholdernonresidentsurname = document.getElementById("shareholdernonresidentsurname_"+getcounter).value;
var shareholdernonresidentgivenname = document.getElementById("shareholdernonresidentgivenName_"+getcounter).value;
var shareholdernonresidentpassportno = document.getElementById("shareholdernonresidentpassportNumber_"+getcounter).value;
var shareholdernonresidentnationality = $('#shareholdernonresidentnationality_'+getcounter+'_chosen span').text();
var shareholdernonresidentcountry = $('#shareholdernonresidentcountry_'+getcounter+'_chosen span').text();
var shareholdernonresidentgender = $("input[name='shareholdernonresidentpgender[]']:checked").val();
//var shareholdernonresidentdatetimepicker = document.getElementById("shareholdernonresidentdatetimepicker_"+getcounter).value;
var shareholdernonresidentdateexpiry = document.getElementById("shareholdernonresidentdateexpiry_"+getcounter).value;
var shareholdernonresidentdob = document.getElementById("shareholdernonresidentdateexpiry_"+getcounter).value;

//alert(shareholdersurname);

localStorage.setItem("shareholdernonresidentpopupsurname_"+getcounter,shareholdernonresidentsurname);
localStorage.setItem("shareholdernonresidentpopupgivenName_"+getcounter,shareholdernonresidentgivenname);
localStorage.setItem("shareholdernonresidentpopuppassportno_"+getcounter,shareholdernonresidentpassportno);
localStorage.setItem("shareholdernonresidentpopupnationality_"+getcounter,shareholdernonresidentnationality);
localStorage.setItem("shareholdernonresidentpopupcountry_"+getcounter,shareholdernonresidentcountry);
localStorage.setItem("shareholdernonresidentpopupgender_"+getcounter,shareholdernonresidentgender);
//localStorage.setItem("shareholdernonresidentpopupdatetimepicker_"+getcounter,shareholdernonresidentdatetimepicker);
localStorage.setItem("shareholdernonresidentpopupdateexpiry_"+getcounter,shareholdernonresidentdateexpiry);
localStorage.setItem("shareholdernonresidentpopupdob_"+getcounter,shareholdernonresidentdob);

	if(shareholdernonresidentsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(shareholdernonresidentgivenname == ''){

           alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(shareholdernonresidentpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(shareholdernonresidentnationality == ''){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(shareholdernonresidentcountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernonresidentgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernonresidentdob == ''){

             //alert("Date of Birth cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernonresidentdateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
             $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
             $("#displaysuccessmsg").modal("show");

	} 
           
	else{
	   //alert("Saved Succesfully");
	     $('#shareholdernonresidentpreviewpassport_'+getcounter).modal('hide');
             $("#displaysuccessmsg .modal-title").html("Shareholder - Preview");
             $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
             $("#displaysuccessmsg").modal("show");
	}
}

if(getresidencystatus == 'shareholdernricfront'){

var shareholderpreviewfrontnricnumber = document.getElementById("shareholderpreviewfrontnricnumber_"+getcounter).value;
var shareholderpreviewnricfrontgender = $("input[name='shareholderpreviewnricfrontgender[]']:checked").val();
var shareholderpreviewnricfrontdob = document.getElementById("shareholderpreviewnricfrontdob_"+getcounter).value;
var shareholderpreviewnricfrontnationality = $('#shareholderpreviewnricfrontnationality_'+getcounter+'_chosen span').text();
var shareholderpreviewnricfrontpostcode = document.getElementById("shareholderpreviewnricfrontpostcode_"+getcounter).value;
var shareholderpreviewnricfrontstreetname = document.getElementById("shareholderpreviewnricfrontstreetname_"+getcounter).value;
var shareholderpreviewnricfrontfloor = document.getElementById("shareholderpreviewnricfrontfloor_"+getcounter).value;
var shareholderpreviewnricfrontunit = document.getElementById("shareholderpreviewnricfrontunit_"+getcounter).value;

localStorage.setItem("shareholderpreviewpopupfrontnricnumber_"+getcounter,shareholderpreviewfrontnricnumber);
localStorage.setItem("shareholderpreviewpopupnricfrontgender_"+getcounter,shareholderpreviewnricfrontgender);
localStorage.setItem("shareholderpreviewpopupnricfrontdob_"+getcounter,shareholderpreviewnricfrontdob);
localStorage.setItem("shareholderpreviewpopupnricfrontnationality_"+getcounter,shareholderpreviewnricfrontnationality);
localStorage.setItem("shareholderpreviewpopupnricfrontpostcode_"+getcounter,shareholderpreviewnricfrontpostcode);
localStorage.setItem("shareholderpreviewpopupnricfrontstreetname_"+getcounter,shareholderpreviewnricfrontstreetname);
localStorage.setItem("shareholderpreviewpopupnricfrontfloor_"+getcounter,shareholderpreviewnricfrontfloor);
localStorage.setItem("shareholderpreviewnrpopupicfrontunit_"+getcounter,shareholderpreviewnricfrontunit);


        if(shareholderpreviewfrontnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(shareholderpreviewnricfrontgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderpreviewnricfrontdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderpreviewnricfrontnationality == ''){

           //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(shareholderpreviewnricfrontpostcode == ''){

           //alert("Postcode cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(shareholderpreviewnricfrontstreetname == ''){

           //alert("Streetname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(shareholderpreviewnricfrontfloor == ''){

           //alert("Floor cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(shareholderpreviewnricfrontunit == ''){
           
           //alert("Unit cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#shareholdernricfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}


}

if(getresidencystatus == 'shareholdernricback'){

var shareholderpreviewbacknricnumber = document.getElementById("shareholderpreviewbacknricnumber_"+getcounter).value;
var shareholderpreviewnricbackgender = $("input[name='shareholderpreviewnricbackgender[]']:checked").val();
var shareholderpreviewnricbackdob = document.getElementById("shareholderpreviewnricbackdob_"+getcounter).value;
var shareholderpreviewnricbacknationality = $('#shareholderpreviewnricbacknationality_'+getcounter+'_chosen span').text();
var shareholderpreviewnricbackpostcode = document.getElementById("shareholderpreviewnricbackpostcode_"+getcounter).value;
var shareholderpreviewnricbackstreetname = document.getElementById("shareholderpreviewnricbackstreetname_"+getcounter).value;
var shareholderpreviewnricbackfloor = document.getElementById("shareholderpreviewnricbackfloor_"+getcounter).value;
var shareholderpreviewnricbackunit = document.getElementById("shareholderpreviewnricbackunit_"+getcounter).value;

localStorage.setItem("shareholderpreviewpopupbacknricnumber_"+getcounter,shareholderpreviewbacknricnumber);
localStorage.setItem("shareholderpreviewpopupnricbackgender_"+getcounter,shareholderpreviewnricbackgender);
localStorage.setItem("shareholderpreviewpopupnricbackdob_"+getcounter,shareholderpreviewnricbackdob);
localStorage.setItem("shareholderpreviewpopupnricbacknationality_"+getcounter,shareholderpreviewnricbacknationality);
localStorage.setItem("shareholderpreviewpopupnricbackpostcode_"+getcounter,shareholderpreviewnricbackpostcode);
localStorage.setItem("shareholderpreviewpopupnricbackstreetname_"+getcounter,shareholderpreviewnricbackstreetname);
localStorage.setItem("shareholderpreviewpopupnricbackfloor_"+getcounter,shareholderpreviewnricbackfloor);
localStorage.setItem("shareholderpreviewnrpopupicbackunit_"+getcounter,shareholderpreviewnricbackunit);

        if(shareholderpreviewbacknricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(shareholderpreviewnricbackgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderpreviewnricbackdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderpreviewnricbacknationality == ''){

           //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(shareholderpreviewnricbackpostcode == ''){

           //alert("Postcode cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(shareholderpreviewnricbackstreetname == ''){

           //alert("Streetname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Streetname cannot be empty");
           $("#displaysuccessmsg").modal("show");  
	}
        else if(shareholderpreviewnricbackfloor == ''){

           //alert("Floor cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
        else if(shareholderpreviewnricbackunit == ''){
           
           //alert("Unit cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}           
	else{
	   //alert("Saved Succesfully");
	   $('#shareholdernricbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}


}
if(getresidencystatus == 'shareholderfinpassport'){


var shareholderfincardsurname = document.getElementById("shareholderfincardsurname_"+getcounter).value;
var shareholderfincardgivenname = document.getElementById("shareholderfincardgivenName_"+getcounter).value;
var shareholderfincardpassportno = document.getElementById("shareholderfincardppassportNumber_"+getcounter).value;
var shareholderfincardnationality = $('#shareholderfincardpnationality_'+getcounter+'_chosen span').text();
var shareholderfincardcountry = $('#shareholderfincardpcountry_'+getcounter+'_chosen span').text();
var shareholderfincardgender = $("input[name='shareholderfincardpgender[]']:checked").val();
//var shareholderfincarddatetimepicker = document.getElementById("shareholderfincarddatetimepicker_"+getcounter).value;
var shareholderfincarddateexpiry = document.getElementById("shareholderfincardpdateexpiry_"+getcounter).value;
//var shareholderfincardplaceofBirth = $('#shareholderfincardplaceofBirth_'+getcounter+'_chosen span').text();
var shareholderfincarddob = document.getElementById("shareholderfincardpdob_"+getcounter).value;
//alert(shareholderfincardnationality);return false;

//alert(shareholdersurname);

localStorage.setItem("shareholderfincardpopupsurname_"+getcounter,shareholderfincardsurname);
localStorage.setItem("shareholderfincardpopupgivenName_"+getcounter,shareholderfincardgivenname);
localStorage.setItem("shareholderfincardpopuppassportno_"+getcounter,shareholderfincardpassportno);
localStorage.setItem("shareholderfincardpopupnationality_"+getcounter,shareholderfincardnationality);
localStorage.setItem("shareholderfincardpopupcountry_"+getcounter,shareholderfincardcountry);
localStorage.setItem("shareholderfincardpopupgender_"+getcounter,shareholderfincardgender);
//localStorage.setItem("shareholderfincardpopupdatetimepicker_"+getcounter,shareholderfincarddatetimepicker);
localStorage.setItem("shareholderfincardpopupdateexpiry_"+getcounter,shareholderfincarddateexpiry);
//localStorage.setItem("shareholderfincardpopupplaceofBirth_"+getcounter,shareholderfincardplaceofBirth);
localStorage.setItem("shareholderfincardpopupdob_"+getcounter,shareholderfincarddob);

        if(shareholderfincardsurname == ''){

           //alert("Surname cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Surname cannot be empty");
           $("#displaysuccessmsg").modal("show");
	}
	else if(shareholderfincardgivenname == ''){

           //alert("Given name cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Given name cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderfincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(shareholderfincardnationality == ''){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(shareholderfincardcountry == ''){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(shareholderfincardgender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
             $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
             $("#displaysuccessmsg").modal("show");
	}        
        else if(shareholderfincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show");  

	} 
        else if(shareholderfincarddob == ''){

             //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
              $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}    
	else{
	   //alert("Saved Succesfully");
	   $('#shareholderfincardpreviewpassport_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == 'shareholderfincardfront'){

var shareholderfincardfrontemployer = document.getElementById("shareholderfincardfrontemployer_"+getcounter).value;
var shareholderfincardfrontoccupation = document.getElementById("shareholderfincardfrontoccupation_"+getcounter).value;
var shareholderfincardfrontdateissue = document.getElementById("shareholderfincardfrontdateissue_"+getcounter).value;
var shareholderfincardfrontdateexpiry = document.getElementById("shareholderfincardfrontdateexpiry_"+getcounter).value;
var shareholderwfincardfrontNumber = document.getElementById("shareholderwfincardfrontNumber_"+getcounter).value;

localStorage.setItem("shareholderfincardfrontemployer_"+getcounter,shareholderfincardfrontemployer);
localStorage.setItem("shareholderfincardfrontoccupation_"+getcounter,shareholderfincardfrontoccupation);
localStorage.setItem("shareholderfincardfrontdateissue_"+getcounter,shareholderfincardfrontdateissue);
localStorage.setItem("shareholderfincardfrontdateexpiry_"+getcounter,shareholderfincardfrontdateexpiry);
localStorage.setItem("shareholderwfincardfrontNumber_"+getcounter,shareholderwfincardfrontNumber);

        if(shareholderfincardfrontemployer == ''){
           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(shareholderfincardfrontoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderfincardfrontdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderfincardfrontdateexpiry == ''){

           //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(shareholderwfincardfrontNumber == ''){

           // alert("Fin Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#shareholderfincardfrontpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

if(getresidencystatus == 'shareholderfincardback'){

var shareholderfincardbackemployer = document.getElementById("shareholderfincardbackemployer_"+getcounter).value;
var shareholderfincardbackoccupation = document.getElementById("shareholderfincardbackoccupation_"+getcounter).value;
var shareholderfincardbackdateissue = document.getElementById("shareholderfincardbackdateissue_"+getcounter).value;
var shareholderfincardbackdateexpiry = document.getElementById("shareholderfincardbackdateexpiry_"+getcounter).value;
var shareholderwfincardbackNumber = document.getElementById("shareholderwfincardbackNumber_"+getcounter).value;

localStorage.setItem("shareholderfincardbackemployer_"+getcounter,shareholderfincardbackemployer);
localStorage.setItem("shareholderfincardbackoccupation_"+getcounter,shareholderfincardbackoccupation);
localStorage.setItem("shareholderfincardbackdateissue_"+getcounter,shareholderfincardbackdateissue);
localStorage.setItem("shareholderfincardbackdateexpiry_"+getcounter,shareholderfincardbackdateexpiry);
localStorage.setItem("shareholderwfincardbackNumber_"+getcounter,shareholderwfincardbackNumber);

        if(shareholderfincardbackemployer == ''){
           //alert("Employer cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(shareholderfincardbackoccupation == ''){

           //alert("Occupation cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderfincardbackdateissue == ''){
          
           //alert("Date of Issue cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderfincardbackdateexpiry == ''){

           //alert("Date of Expiry cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
           $("#displaysuccessmsg").modal("show"); 

	}
        else if(shareholderwfincardbackNumber == ''){

           // alert("Fin Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
          
	else{
	   //alert("Saved Succesfully");
	   $('#shareholderfincardbackpreview_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Shareholder Passport  - Preview");
           $("#displaysuccessmsg .modal-body").html("Data has been saved successfully");
           $("#displaysuccessmsg").modal("show"); 
	}

}

}
</script>
