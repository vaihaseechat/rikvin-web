<script>
function shareholderstoredata(getcounter,getresidencystatus){

//alert(getcounter);
//alert(getresidencystatus);


if(getresidencystatus == 'shareholderpassportmanually'){


	var shareholdernonpassportno = document.getElementById("shareholderpassportNumber_"+getcounter).value;
	var shareholdernonnationality = $('#shareholdernonnationality_'+getcounter+'_chosen span').text();
	var shareholdernoncountry = $('#shareholdernoncountry_'+getcounter+'_chosen span').text();
	var shareholdernongender = $("input[name='shareholdernongender[]']:checked").val();
	var shareholdernondatetimepicker = document.getElementById("shareholdernondateissue_"+getcounter).value;
	var shareholdernondateexpiry = document.getElementById("shareholdernondateexpiry_"+getcounter).value;
	var shareholdernonplaceofBirth = $('#shareholdernondateplace_'+getcounter+'_chosen span').text();
        var shareholdernondateBirth = document.getElementById("shareholdernondatebirth_"+getcounter).value;

	localStorage.setItem("shareholdernonmanuallypassportno_"+getcounter,shareholdernonpassportno);
	localStorage.setItem("shareholdernonmanuallynationality_"+getcounter,shareholdernonnationality);
	localStorage.setItem("shareholdernonmanuallycountry_"+getcounter,shareholdernoncountry);
	localStorage.setItem("shareholdernonmanuallygender_"+getcounter,shareholdernongender);
	localStorage.setItem("shareholdernonmanuallydatetimepicker_"+getcounter,shareholdernondatetimepicker);
	localStorage.setItem("shareholdernonmanuallydateexpiry_"+getcounter,shareholdernondateexpiry);
	localStorage.setItem("shareholdernonmanuallyplaceofBirth_"+getcounter,shareholdernonplaceofBirth);
        localStorage.setItem("shareholdernonmanuallydateBirth_"+getcounter,shareholdernondateBirth);

        if(shareholdernonpassportno == ''){
          
           //alert("Pass port Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	   $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
	   $("#displaysuccessmsg").modal("show");

	}
	else if(shareholdernonnationality == 'Please select'){

            //alert("Nationality cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	   $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
	   $("#displaysuccessmsg").modal("show");


	}
        else if(shareholdernoncountry == 'Please select'){

           //alert("Country cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	   $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
	   $("#displaysuccessmsg").modal("show");

	}
        else if(shareholdernongender == ''){

             //alert("Please select any one of the gender");
             $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	     $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
	     $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernondatetimepicker == ''){

             //alert("Date of Issue cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	     $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
	     $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernondateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
	      $("#displaysuccessmsg").modal("show");

	} 
        else if(shareholdernonplaceofBirth == 'Please select'){

              //alert("Place of Birth cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	      $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show"); 
	}
        else if(shareholdernondateBirth == 'Please select'){

              //alert("Date of Birth cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	      $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
	      $("#displaysuccessmsg").modal("show");    
	}    
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_SHAREHOLDER_NONRESIDENT_PASSPORT, true);
           showPassportEmpty(ROLE_SHAREHOLDER, getcounter, false);
	   $('#shareholdernonresidentpreviewpassportmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Shareholder - Manual");
	   $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
	   $("#displaysuccessmsg").modal("show");   
	}

	
}

if(getresidencystatus == 'shareholdernricmanually'){

var shareholdernonresidentnricnumber = document.getElementById("shareholdernonresidentnricnumber_"+getcounter).value;
var shareholdernonresidentgender = $("input[name='shareholdernonresidentgender[]']:checked").val();
var shareholdernonresidentdob = document.getElementById("shareholdernonresidentdob_"+getcounter).value;
var shareholdernricnationality = $('#shareholdernricnationality_'+getcounter+'_chosen span').text();
var shareholdernricpostcode = document.getElementById("shareholdernricpostcode_"+getcounter).value;
var shareholdernricstreetname = document.getElementById("shareholdernricstreetname_"+getcounter).value;
var shareholdernricfloor = document.getElementById("shareholdernricfloor_"+getcounter).value;
var shareholdernricunit = document.getElementById("shareholdernricunit_"+getcounter).value;

localStorage.setItem("shareholderpreviewmanuallynricnumber_"+getcounter,shareholdernonresidentnricnumber);
localStorage.setItem("shareholderpreviewmanuallynricgender_"+getcounter,shareholdernonresidentgender);
localStorage.setItem("shareholderpreviewmanuallynricdob_"+getcounter,shareholdernonresidentdob);
localStorage.setItem("shareholderpreviewmanuallynricnationality_"+getcounter,shareholdernricnationality);
localStorage.setItem("shareholderpreviewmanuallynricpostcode_"+getcounter,shareholdernricpostcode);
localStorage.setItem("shareholderpreviewmanuallynricstreetname_"+getcounter,shareholdernricstreetname);
localStorage.setItem("shareholderpreviewmanuallynricfloor_"+getcounter,shareholdernricfloor);
localStorage.setItem("shareholderpreviewnrmanuallyicunit_"+getcounter,shareholdernricunit);

//alert(shareholdernonresidentgender);

        if(shareholdernonresidentnricnumber == ''){

           //alert("NRIC Number cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("NRIC Number cannot be empty");
           $("#displaysuccessmsg").modal("show"); 
	}
	else if(shareholdernonresidentgender == ''){

           //alert("Please select any one of the gender");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Gender cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(shareholdernonresidentdob == ''){
          
           //alert("Date of Birth cannot be empty");
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Date of birth cannot be empty");
           $("#displaysuccessmsg").modal("show");

	}
	else if(shareholdernricnationality == 'Please select'){

            //alert("Nationality cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show");

	}
        else if(shareholdernricpostcode == ''){

            //alert("Postcode cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
            $("#displaysuccessmsg .modal-body").html("Postcode cannot be empty");
            $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernricstreetname == ''){

             //alert("Streetname cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
             $("#displaysuccessmsg .modal-body").html("Street name cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernricfloor == ''){

             //alert("Floor cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
             $("#displaysuccessmsg .modal-body").html("Floor cannot be empty");
             $("#displaysuccessmsg").modal("show");
	}
        else if(shareholdernricunit == ''){
           
              //alert("Unit cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
              $("#displaysuccessmsg .modal-body").html("Unit cannot be empty");
              $("#displaysuccessmsg").modal("show");

	}           
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_SHAREHOLDER_CITIZENPR_NRIC, true);
	   showNRICEmpty(ROLE_SHAREHOLDER, getcounter, true, false);
	   showNRICEmpty(ROLE_SHAREHOLDER, getcounter, false, false);
	   $('#shareholdernonresidentpreviewnricmanually_'+getcounter).modal('hide');
           $("#displaysuccessmsg .modal-title").html("Shareholder NRIC - Manual");
           $("#displaysuccessmsg .modal-body").html("Data has been Saved Successfully");
           $("#displaysuccessmsg").modal("show");
	}

}

if(getresidencystatus == 'shareholderfincardmanually'){

var shareholderfincardpassportno = document.getElementById("shareholderfincardpassportNumber_"+getcounter).value;
var shareholderfincardnationality = $('#shareholderfincardnationality_'+getcounter+'_chosen span').text();
var shareholderfincardcountry = $('#shareholderfincardcountry_'+getcounter+'_chosen span').text();
var shareholderfincardgender = $("input[name='shareholderfincardgender[]']:checked").val();
var shareholderfincarddatetimepicker = document.getElementById("shareholderfincarddateissue_"+getcounter).value;
var shareholderfincarddateexpiry = document.getElementById("shareholderfincarddateexpiry_"+getcounter).value;
var shareholderfincardplaceofBirth = $('#shareholderfincarddateplace_'+getcounter+'_chosen span').text();
var shareholderfincarddateBirth = document.getElementById("shareholderfincarddatebirth_"+getcounter).value;

var shareholderfincardemployer = document.getElementById("shareholderfincardemployer_"+getcounter).value;
var shareholderfincardoccupation = document.getElementById("shareholderfincardoccupation_"+getcounter).value;
var shareholderworkpassdateissue = document.getElementById("shareholderworkpassdateissue_"+getcounter).value;
var shareholderworkpassdateexpiry = document.getElementById("shareholderworkpassdateexpiry_"+getcounter).value;
var shareholderwfincardNumber = document.getElementById("shareholderwfincardNumber_"+getcounter).value;

localStorage.setItem("shareholderfincardmanuallypassportno_"+getcounter,shareholderfincardpassportno);
localStorage.setItem("shareholderfincardmanuallynationality_"+getcounter,shareholderfincardnationality);
localStorage.setItem("shareholderfincardmanuallycountry_"+getcounter,shareholderfincardcountry);
localStorage.setItem("shareholderfincardmanuallygender_"+getcounter,shareholderfincardgender);
localStorage.setItem("shareholderfincardmanuallydatetimepicker_"+getcounter,shareholderfincarddatetimepicker);
localStorage.setItem("shareholderfincardmanuallydateexpiry_"+getcounter,shareholderfincarddateexpiry);
localStorage.setItem("shareholderfincardmanuallyplaceofBirth_"+getcounter,shareholderfincardplaceofBirth);
localStorage.setItem("shareholderfincardmanuallydateBirth_"+getcounter,shareholderfincarddateBirth);

localStorage.setItem("shareholderfincardmanuallyemployer_"+getcounter,shareholderfincardemployer);
localStorage.setItem("shareholderfincardmanuallyoccupation_"+getcounter,shareholderfincardoccupation);
localStorage.setItem("shareholderfincardmanuallyworkpassdateissue_"+getcounter,shareholderworkpassdateissue);
localStorage.setItem("shareholderfincardmanuallyworkpassdateexpiry_"+getcounter,shareholderworkpassdateexpiry);
localStorage.setItem("shareholderfincardmanuallywfincardnumber_"+getcounter,shareholderwfincardNumber);


        if(shareholderfincardpassportno == ''){
          
           //alert("Passport Number cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Passport Number cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
	else if(shareholderfincardnationality == 'Please select'){

            //alert("Nationality cannot be empty");
             $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Nationality cannot be empty");
            $("#displaysuccessmsg").modal("show"); 

	}
        else if(shareholderfincardcountry == 'Please select'){

            //alert("Country cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Country cannot be empty");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(shareholderfincardgender == 'undefined'){

             //alert("Please select any one of the gender");
            $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Please select any one of the gender");
            $("#displaysuccessmsg").modal("show"); 
	}
        else if(shareholderfincarddatetimepicker == ''){

            //alert("Date of Issue cannot be empty");
            $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
            $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
            $("#displaysuccessmsg").modal("show");   
	}
        else if(shareholderfincarddateexpiry == ''){
           
              //alert("Date of Expiry cannot be empty");
              $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
              $("#displaysuccessmsg").modal("show"); 

	} 
        else if(shareholderfincardplaceofBirth == 'Please select'){

               //alert("Place of Birth cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Place of Birth cannot be empty");
               $("#displaysuccessmsg").modal("show");  
	}
        else if(shareholderfincarddateBirth == ''){

              //alert("Date of Birth cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Date of Birth cannot be empty");
               $("#displaysuccessmsg").modal("show");   
	} 
        else if(shareholderfincardemployer == ''){

              //alert("Employer cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Employer cannot be empty");
              $("#displaysuccessmsg").modal("show");  
	}
        else if(shareholderfincardoccupation == ''){

              //alert("Occupation cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Occupation cannot be empty");
               $("#displaysuccessmsg").modal("show");    
	}
        else if(shareholderworkpassdateissue == ''){

              //alert("Date of Issue cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Date of Issue cannot be empty");
               $("#displaysuccessmsg").modal("show");  
	}
        else if(shareholderworkpassdateexpiry == ''){

              //alert("Date of Expiry cannot be empty"); 
               $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
               $("#displaysuccessmsg .modal-body").html("Date of Expiry cannot be empty");
               $("#displaysuccessmsg").modal("show");  
	}
        else if(shareholderwfincardNumber == ''){

              //alert("Fin Number cannot be empty"); 
              $("#displaysuccessmsg .modal-title").html("Shareholder Passport/Fin - Manual");
              $("#displaysuccessmsg .modal-body").html("Fin Number cannot be empty");
              $("#displaysuccessmsg").modal("show");    
	}    
	else{
	   //alert("Saved Succesfully");
           localStorage.setItem(MANUAL_SHAREHOLDER_FIN_PASSPORT, true);
	   showFinPassportEmpty(ROLE_SHAREHOLDER, getcounter, false);
	   showFINCardEmpty(ROLE_SHAREHOLDER, getcounter, true, false);
	   showFINCardEmpty(ROLE_SHAREHOLDER, getcounter, false, false);           
	   $('#shareholderfincardmanually_'+getcounter).modal('hide');
           $('#displaysuccessmsg').modal('show');
	}




}



}
</script>
