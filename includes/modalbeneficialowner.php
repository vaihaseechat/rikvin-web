<script>

        var bofullName, boemail, bocontactnumber;
      	var alert_contact = '<div class="bocontactnumber-alert" id="bocontactnumber-alert" style="color: rgb(185, 13, 15);">'
							+'<span class="font-italic small" id="bocontactnumber-alert-msg">Contact Number should not be empty</span>'
									+'</div>';
	var boEmail_alert = '<span class="font-italic small" id="boemail-alert-msg">Email should not be empty</span>';
	var boEmail_invalid_alert = '<span class="font-italic small" id="boemail-alert-msg">Invalid Email</span>';
	var bofull_name_alert = '<span class="font-italic small" id="bofullname-alert-msg">You should provide your full name</span>';
        var boresidencyStatus_alert = '<span class="font-italic small" id="boresidencyStatus-alert-msg">'+ERR_MSG_RESIDENTIAL_STATUS_EMPTY+'</span>';
    // Get the value of the input field with id="numb"

        var isboValidated = true;
        var alert_invalid_contact = '<span class="font-italic small" id="bocontactnumber-alert-msg">'+ERR_MSG_PHONE_NAN+'</span>'; 
	
function hasManualEntry(manual_type){
	var value = localStorage.getItem(manual_type);
	console.log("manual item: "+manual_type+" val: "+value);
	return value;
}

// Non resident - upload passport

function showBOPassportEmpty(show,rolecount,rolename){
	var alert;
	var alertMsg;
	alert = $('#bononresidentuploadpassport_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bononresidentuploadpassport_'+rolecount+'_'+rolename+'-alert-msg');
		
	if (show == true) {
		alertMsg.text(ERR_MSG_PASSPORT_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

// Non resident - upload address
	
function showBOAddressProofEmpty(show,rolecount,rolename){
	//console.log("showAddressProofEmpty: " + show);
	var alert;
	var alertMsg;
	alert = $('#bononresidentaddress_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bononresidentaddress_'+rolecount+'_'+rolename+'-alert-msg');	
	if (show == true) {
		//console.log("showing alert....");
		alertMsg.text(ERR_MSG_PROOFADDRESS_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

// Non resident - residential address

function showBOAddressEmpty(show,rolecount,rolename){
	var alert;
	var alertMsg;
	alert = $('#bononresidentresaddress_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bononresidentresaddress_'+rolecount+'_'+rolename+'-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_ADDRESS_EMPTY);
		alert.show().parent().find('textarea').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().parent().find('textarea').css('border','').css('border-radius','');
	}
}

// NRIC 

function showBONRICEmpty(isFront, show,rolecount,rolename){
	//console.log("showNRICEmpty: "+isFront+", "+show);
	
	var alert;
	var alertMsg;
	if(isFront == true){
		alert = $('#bonricfront_'+rolecount+'_'+rolename+'-alert');
		alertMsg = $('#bonricfront_'+rolecount+'_'+rolename+'-alert-msg');
	}else{
		alert = $('#bonricback_'+rolecount+'_'+rolename+'-alert');
		alertMsg = $('#bonricback_'+rolecount+'_'+rolename+'-alert-msg');
	}
		
	if (show == true) {
		if(isFront == true){
			console.log("showing....");
			alertMsg.text(ERR_MSG_NRIC_FRONT_EMPTY);	
		}else{
			alertMsg.text(ERR_MSG_NRIC_BACK_EMPTY);	
		}		
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

// FIN

function showBOFinPassportEmpty(show,rolecount,rolename){
	var alert;
	var alertMsg;
	alert = $('#bouploadPassport_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bouploadPassport_'+rolecount+'_'+rolename+'-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_PASSPORT_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showBOFINCardEmpty(isFront, show,rolecount,rolename){
	var alert;
	var alertMsg;
	if(isFront == true){
		alert = $('#bofincardfront_'+rolecount+'_'+rolename+'-alert');
		alertMsg = $('#bofincardfront_'+rolecount+'_'+rolename+'-alert-msg');
	}else{
		alert = $('#bofincardback_'+rolecount+'_'+rolename+'-alert');
		alertMsg = $('#bofincardback_'+rolecount+'_'+rolename+'-alert-msg');
	}	
	
	if (show == true) {
		if(isFront == true){
			alertMsg.text(ERR_MSG_FIN_FRONT_EMPTY);	
		}else{
			alertMsg.text(ERR_MSG_FIN_BACK_EMPTY);	
		}		
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showBOFinAddressProofEmpty(show,rolecount,rolename){
	var alert;
	var alertMsg;
	alert = $('#boaddress_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#boaddress_'+rolecount+'_'+rolename+'-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_PROOFADDRESS_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showBOFinAddressEmpty(show,rolecount,rolename){
	var alert;
	var alertMsg;
	alert = $('#boresaddress_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#boresaddress_'+rolecount+'_'+rolename+'-alert-msg');

	if (show == true) {
		alertMsg.text(ERR_MSG_ADDRESS_EMPTY);
                alert.show().parent().find('textarea').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().parent().find('textarea').css('border','').css('border-radius','');
	}
}

function showBOPepCountryEmpty(rolecount,rolename,show){
	var alert;
	var alertMsg;
	
	alert = $('#bocountrypep_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bocountrypep_'+rolecount+'_'+rolename+'-alert-msg');

	if (show == true) {
		console.log("showing...")
		alertMsg.text(ERR_MSG_PEP_COUNTRY_EMPTY);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}

function showBOPepRoleEmpty(rolecount,rolename,show){
	var alert;
	var alertMsg;
	
	alert = $('#borolePep_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#borolePep_'+rolecount+'_'+rolename+'-alert-msg');

	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_ROLE_EMPTY);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}

function showBOPepFromEmpty(rolecount,rolename,show){
	var alert;
	var alertMsg;

	alert = $('#bopepFrom_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bopepFrom_'+rolecount+'_'+rolename+'-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_FROM_EMPTY);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}
function checkPepFromToRange(rolecount, rolename, pepFrom, pepTo){
	var from = parseInt(pepFrom);
	var to = parseInt(pepTo);
	if(from >= to){
		showPepFromToRange(rolecount, rolename, true);
		return false;
	}else{
		return true;
	}
}
function showPepFromToRange(rolecount,rolename,show){
	var alert;
	var alertMsg;

	alert = $('#bopepFrom_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bopepFrom_'+rolecount+'_'+rolename+'-alert-msg');
	
	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_FROM_TO_WRONG_RANGE);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}

function showBOPepToEmpty(rolecount,rolename,show){
	var alert;
	var alertMsg;

	alert = $('#bopepTo_'+rolecount+'_'+rolename+'-alert');
	alertMsg = $('#bopepTo_'+rolecount+'_'+rolename+'-alert-msg');

	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_TO_EMPTY);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}

function myFunction(rolecount,rolename) {

//alert(rolename);
        $("#bofullname_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			$("#bofullName-alert_"+rolecount+"_"+rolename).html("");
		}
	});
	
	$("#boemail_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			$("#boemail-alert_"+rolecount+"_"+rolename).html("");
		}
	});
	
	$("#bocontactnumber_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			$("#bocontact-alert_"+rolecount+"_"+rolename).html("");
		}
	});

        $("#boresidencystatus_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			$("#boresidencyStatus-alert_"+rolecount+"_"+rolename).html("");
		}
	}); 

        /*Nonresident*/

	/*Upload Passport*/
	$("#bononresidentuploadpassport_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOPassportEmpty(false,rolecount,rolename);	
	}
	});

	/*Upload proof of address*/
	$("#bononresidentaddress_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOAddressProofEmpty(false,rolecount,rolename);	
	}
	});

	/*Upload proof of address - textarea*/
	$("#bononresidentresaddress_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOAddressEmpty(false,rolecount,rolename);	
	}
	});

        //NRIC 

        /*Upload NRIC - front*/
	$("#bonricfront_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBONRICEmpty(true, false,rolecount,rolename);	
	}
	});

	/*Upload NRIC - back*/
	$("#bonricback_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBONRICEmpty(false, false,rolecount,rolename);	
	}
	});

   
        // FIN

	/*Upload Passport*/

	$("#bouploadPassport_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showBOFinPassportEmpty(false,rolecount,rolename);
		}
	});

	/*Upload FIN - front*/

	$("#bofincardfront_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOFINCardEmpty(true, false,rolecount,rolename);	
	}
	});

	/*Upload FIN - back*/

	$("#bofincardback_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOFINCardEmpty(false, false,rolecount,rolename);	
	}
	});

	/*Upload proof of address*/
	$("#boaddress_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOFinAddressProofEmpty(false,rolecount,rolename);	
	}
	});
	/*Upload proof of address - textarea*/
	$("#boresaddress_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOFinAddressEmpty(false,rolecount,rolename);	
	}
	}); 

        /*PEP in which country*/
	$('#bocountrypep_'+rolecount+'_'+rolename+'_chosen span').on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOPepCountryEmpty(rolecount, rolename,false);	
	}
	});
	/*Role of PEP*/
	$("#borolePep_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
		showBOPepRoleEmpty(rolecount, rolename,false);	
	}
	});
	/*PEP since - from*/
	$('#bopepFrom_'+rolecount+'_'+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showBOPepFromEmpty(rolecount, rolename,false);
			showBOPepToEmpty(rolecount, rolename,false);		
		}
	});
	/*PEP since - to*/
	$("#bopepTo_"+rolecount+"_"+rolename).on('change keyup paste', function(e){
		if (this.value.trim() != "") {
			showBOPepFromEmpty(rolecount, rolename,false);
			showBOPepToEmpty(rolecount, rolename,false);	
		}
	});

	isboValidated = true;
	var bofullName; 
	var bocontactNo; 
	var boresidencyStatus;
	var boemail;

	var bopassport;
	var boaddressProof;
	var boresaddress;
	var bofinPassport;
	var bofinCardFront;
	var bofinCardBack;
	var bofinAddressProof;
	var bofinresaddress;
	var bonricfront;
	var bonricback;
        var bopep;
        var bopepCountry;
	var bopepRole;
	var bopepFrom;
	var bopepTo;
        
        fullName = document.getElementById("bofullname_"+rolecount+"_"+rolename).value;
	email = document.getElementById("boemail_"+rolecount+"_"+rolename).value;
	contactNo = document.getElementById("bocontactnumber_"+rolecount+"_"+rolename).value;
	boresidencyStatus = document.getElementById("boresidencystatus_"+rolecount+"_"+rolename).value;

        bopassport = $("#bononresidentuploadpassport_"+rolecount+"_"+rolename).val();
	boaddressProof = $("#bononresidentaddress_"+rolecount+"_"+rolename).val();
	boresaddress = $("#bononresidentresaddress_"+rolecount+"_"+rolename).val();	
	bofinPassport = $("#bouploadPassport_"+rolecount+"_"+rolename).val();
	bofinCardFront = $("#bofincardfront_"+rolecount+"_"+rolename).val();
	bofinCardBack = $("#bofincardback_"+rolecount+"_"+rolename).val();
	bofinAddressProof = $("#boaddress_"+rolecount+"_"+rolename).val();
	bofinresaddress = $("#boresaddress_"+rolecount+"_"+rolename).val();
	bonricfront = $("#bonricfront_"+rolecount+"_"+rolename).val();
	bonricback = $("#bonricback_"+rolecount+"_"+rolename).val();

        bopep = $("input[name=bopep_"+rolecount+"_"+rolename+"]:checked").val();
	//console.log("pep "+pep);
        bopepCountry = $('#bocountrypep_'+rolecount+'_'+rolename+'_chosen span').text();
	console.log("bopepCountry "+bopepCountry);
	bopepRole = $("#borolePep_"+rolecount+"_"+rolename).val();
	//console.log("pepRole "+pepRole);
        bopepFrom = $('#bopepFrom_'+rolecount+'_'+rolename+'_chosen span').text();
	//console.log("pepFrom "+pepFrom);
        bopepTo = $('#bopepTo_'+rolecount+'_'+rolename+'_chosen span').text();

        //alert(boresidencyStatus);

	if(fullName=='') {
                //alert("sds");
		$("#bofullName-alert_"+rolecount+"_"+rolename).html(bofull_name_alert);
                isboValidated = false;
	}
        if(boresidencyStatus == '') {
		$("#boresidencyStatus-alert_"+rolecount+"_"+rolename).html(boresidencyStatus_alert);
		isboValidated = false;
	}
        if(boresidencyStatus == "bononresident_"+rolecount+"_"+rolename){                
                if(hasManualEntry(MANUAL_BO_NONRESIDENT_PASSPORT) != "true"){
			if(bopassport == undefined || bopassport == ""){
				showBOPassportEmpty(true,rolecount,rolename);	
				isboValidated = false;
			}
		}
		if(boresaddress == undefined || boresaddress == ""){
			showBOAddressEmpty(true,rolecount,rolename);	
			isboValidated = false;
		} 
		if(boaddressProof == undefined || boaddressProof == ""){
			showBOAddressProofEmpty(true,rolecount,rolename);	
			isboValidated = false;
		}

        } 
        if(boresidencyStatus == "bocitizenpr_"+rolecount+"_"+rolename){
		if(hasManualEntry(MANUAL_BO_CITIZEN_NRIC) != "true"){
			if(bonricfront == undefined || bonricfront == ""){
				showBONRICEmpty(true, true,rolecount,rolename);	
				isboValidated = false;
			} if(bonricback == undefined || bonricback == ""){
				showBONRICEmpty(false, true,rolecount,rolename);	
				isboValidated = false;
			}
		}
	}
        if(boresidencyStatus == "bopassholder_"+rolecount+"_"+rolename){

		if(hasManualEntry(MANUAL_BO_FIN_PASSPORT) != "true"){
			if(bofinPassport == undefined || bofinPassport == ""){
				showBOFinPassportEmpty(true,rolecount,rolename);	
				isboValidated = false;
			} if(bofinCardFront == undefined || bofinCardFront == ""){
				showBOFINCardEmpty(true, true,rolecount,rolename);	
				isboValidated = false;
			} if(bofinCardBack == undefined || bofinCardBack == ""){
				showBOFINCardEmpty(false, true,rolecount,rolename);	
				isboValidated = false;
			}
		} 
		if(bofinAddressProof == undefined || bofinAddressProof == ""){
			showBOFinAddressProofEmpty(true,rolecount,rolename);	
			isboValidated = false;
		} if(bofinresaddress == undefined || bofinresaddress == ""){
			showBOFinAddressEmpty(true,rolecount,rolename);	
			isboValidated = false;
		}
	}
        if(email==''){
                //alert("sdsdsd");
		$("#boemail-alert_"+rolecount+"_"+rolename).html(boEmail_alert);
                isboValidated = false;
	} 

        if (!ValidateEmail(email)){
		$("#boemail-alert_"+rolecount+"_"+rolename).html(boEmail_invalid_alert);
                isboValidated = false; 
	} 

        if(contactNo=='') {
		console.log('contact number empty');
		$("#bocontact-alert_"+rolecount+"_"+rolename).html(alert_contact);
                 isboValidated = false; 
	}
        if(!(contactNo == undefined || contactNo == "")){
		if(isNaN(contactNo)|| contactNo.trim().length < 8){
                        $("#bocontact-alert_"+rolecount+"_"+rolename).html(alert_invalid_contact); 
			isboValidated = false;
		}	
	}  

        if(bopep == "bopepyes_"+rolecount+"_"+rolename){
		if(bopepCountry == undefined || bopepCountry == "" || bopepCountry == "Please select"){
			showBOPepCountryEmpty(rolecount, rolename,true);
			isboValidated = false;
		} if(bopepRole == undefined || bopepRole == ""){
			showBOPepRoleEmpty(rolecount, rolename,true);
			isboValidated = false;
		} if(bopepFrom == undefined || bopepFrom == "" || bopepFrom == "From"){
			showBOPepFromEmpty(rolecount,rolename, true);
			isboValidated = false;
		} if(bopepTo == undefined || bopepTo == "" || bopepTo == "To"){
			showBOPepToEmpty(rolecount,rolename, true);
			isboValidated = false;
		}
		if((bopepFrom != undefined && bopepFrom != "") && (bopepTo != undefined && bopepTo != "")){
			if(checkPepFromToRange(rolecount, rolename, bopepFrom, bopepTo) != true){
				isboValidated = false;
			}
		}				
	}

        //alert(isboValidated);

        if(isboValidated == true) {

                //alert(rolecount);
                //alert(rolename);
		var pos = localStorage.getItem("bo_insert_position");
		var key = localStorage.getItem("bo_insert_key_name");

		localStorage.setItem("bo_fullName_"+pos+"_"+key, fullName);
		localStorage.setItem("bo_email_"+pos+"_"+key, email);
		localStorage.setItem("bo_contact_no_"+pos+"_"+key, contactNo);
                localStorage.setItem("bo_residencystatus_"+rolecount+"_"+rolename, boresidencyStatus);
 
                 $("#displaysuccessmsg .modal-title").html("Ultimate Beneficial Owner");
		 $("#displaysuccessmsg").modal("show");
		 $("#modalbeneficialowner_"+rolecount+"_"+rolename).modal("hide");
                 $('body').css("overflow","auto");

		 localStorage.setItem("bouploadpassport_"+rolecount+"_"+rolename, bopassport);
		 localStorage.setItem("boaddress_"+rolecount+"_"+rolename, boaddressProof);
		 localStorage.setItem("boresaddress_"+rolecount+"_"+rolename, boresaddress);
		 localStorage.setItem("bofinuploadPassport_"+rolecount+"_"+rolename, bofinPassport);
		 localStorage.setItem("bofinfrontupload_"+rolecount+"_"+rolename, bofinCardFront);
		 localStorage.setItem("bofinbackupload_"+rolecount+"_"+rolename, bofinCardBack);
		 localStorage.setItem("bofinaddress_"+rolecount+"_"+rolename, bofinAddressProof);
		 localStorage.setItem("bofinresaddress_"+rolecount+"_"+rolename, bofinresaddress);
		 localStorage.setItem("bonricfront_"+rolecount+"_"+rolename, bonricfront);
		 localStorage.setItem("bonricback_"+rolecount+"_"+rolename, bonricback);

                 localStorage.setItem("bopep_"+rolecount+"_"+rolename, bopep);
                 localStorage.setItem("bopepCountry_"+rolecount+"_"+rolename, bopepCountry);
                 localStorage.setItem("bopepRole_"+rolecount+"_"+rolename, bopepRole);
                 localStorage.setItem("bopepFrom_"+rolecount+"_"+rolename, bopepFrom);
                 localStorage.setItem("bopepTo_"+rolecount+"_"+rolename, bopepTo); 

                 if(rolename == 'dishareholder'){
                   localStorage.setItem("getbodiname", rolename);
                 }else{             
		   localStorage.setItem("getboname", rolename);
                 }     

        }
      
	return isboValidated;	

        
                
	
	for (var i = 0; i < localStorage.length; i++){
    		console.log(localStorage.key(i)+" ==> "+localStorage.getItem(localStorage.key(i)));
		}
    // If x is Not a Number or less than one or greater than 10


        
   
}

</script>


