<?php
session_start();
$error=array();
$extension=array("jpeg","jpg","png","gif","PDF","pdf");

$_SESSION['id'] = session_id();
if(!is_dir("shareholderdocuments/")) {
	mkdir("shareholderdocuments/");
}
if(!is_dir("shareholderdocuments/". $_SESSION["id"] ."/")) {
	mkdir("shareholderdocuments/". $_SESSION['id'] ."/");
}
// Upload section for shareholder nonresident

if($_FILES["shareholdernonresidentuploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["shareholdernonresidentuploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholdernonresidentuploadPassport"]["name"][$key];
	$file_tmp = $_FILES["shareholdernonresidentuploadPassport"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for shareholder non resident address

if($_FILES["shareholdernonresidentaddress"]["tmp_name"] != '') {

	foreach($_FILES["shareholdernonresidentaddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholdernonresidentaddress"]["name"][$key];
	$file_tmp = $_FILES["shareholdernonresidentaddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for shareholder fin passport

if($_FILES["shareholderuploadPassport"]["tmp_name"] != '') {

	foreach($_FILES["shareholderuploadPassport"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholderuploadPassport"]["name"][$key];
	$file_tmp = $_FILES["shareholderuploadPassport"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for shareholder fin front

if($_FILES["shareholderfincardfront"]["tmp_name"] != '') {

	foreach($_FILES["shareholderfincardfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholderfincardfront"]["name"][$key];
	$file_tmp = $_FILES["shareholderfincardfront"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for shareholder fin back
if($_FILES["shareholderfincardback"]["tmp_name"] != '') {

	foreach($_FILES["shareholderfincardback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholderfincardback"]["name"][$key];
	$file_tmp = $_FILES["shareholderfincardback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for shareholder address

if($_FILES["shareholderaddress"]["tmp_name"] != '') {

	foreach($_FILES["shareholderaddress"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholderaddress"]["name"][$key];
	$file_tmp = $_FILES["shareholderaddress"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for shareholder NRIC front

if($_FILES["shareholdernricfront"]["tmp_name"] != '') {

	foreach($_FILES["shareholdernricfront"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholdernricfront"]["name"][$key];
	$file_tmp = $_FILES["shareholdernricfront"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}

// Upload section for shareholder NRIC back

if($_FILES["shareholdernricback"]["tmp_name"] != '') {

	foreach($_FILES["shareholdernricback"]["tmp_name"] as $key=>$tmp_name)
	{

	$file_name = $_FILES["shareholdernricback"]["name"][$key];
	$file_tmp = $_FILES["shareholdernricback"]["tmp_name"][$key];
	$ext = strtolower(pathinfo($file_name,PATHINFO_EXTENSION));

		if(in_array($ext,$extension))
		{
		    if(!file_exists("shareholderdocuments/". $_SESSION['id'] ."/".$file_name))
		    {
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$file_name);
		    }
		    else
		    {
			$filename=basename($file_name,$ext);
			$newFileName=$filename.time().".".$ext;
			move_uploaded_file($file_tmp,"shareholderdocuments/". $_SESSION['id'] ."/".$newFileName);
		    }
		}
		else
		{
		    array_push($error,"$file_name, ");
		}
	}

}
?>

