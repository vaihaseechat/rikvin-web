<script src='<?php echo BASE_URL;?>/js/pdfcore2.js'></script>
<script>
var parsecount = 20;
function checkPdfParse(myImages,getcounter,callbackfunction,rolename)
{

//pdfjsLib.GlobalWorkerOptions.workerSrc="https://mozilla.github.io/pdf.js/build/pdf.worker.js";

var myImage = myImages[0];
var filename = myImage.name;
var fileReader = new FileReader();
var scale = 1.0;
var ext=myImages[0].name.split('.').pop().toLowerCase();
if(ext=="pdf")
{
try{	
console.log(" Getting image data from pdf");

fileReader.onload = function() {

	//Step 4:turn array buffer into typed array
	var typedarray = new Uint8Array(this.result);

	//Step 5:PDFJS should be able to read this
	pdfjsLib.getDocument(typedarray).then(function(pdf) {
		 pdf.getPage(1).then(function(page) {
	 
			var viewport = page.getViewport(scale);
			var canvas = document.createElement('canvas') , ctx = canvas.getContext('2d');
			var renderContext = { canvasContext: ctx, viewport: viewport };
			canvas.height = viewport.height;
			canvas.width = viewport.width;
			page.render(renderContext).then(function() {									
				var file_blob=[];
				file_blob.push(dataURItoBlob(canvas.toDataURL()));
				callbackfunction(file_blob,getcounter,filename,rolename);	
			});
		});
	});


};
fileReader.readAsArrayBuffer(myImage);
}catch (err) {

	console.log(err);
	callbackfunction(myImages,getcounter,filename,rolename);
}
}
else{
	var extension =myImages[0].name.split('.').pop().toLowerCase(); 
        console.log(extension);
        if((extension == "png") || (extension == "jpg") || (extension == "jpeg") || (extension == "gif"))
        {
	  callbackfunction(myImages,getcounter,filename,rolename);
        }else{
           $("#displaysuccessmsg .modal-title").html("Error");
	   $("#displaysuccessmsg .modal-body").html("Invalid File type");
	   $("#displaysuccessmsg").modal("show");
        }
}
	
}




function dataURItoBlob(dataURI) {
// convert base64/URLEncoded data component to raw binary data held in a string
var byteString;
if (dataURI.split(',')[0].indexOf('base64') >= 0)
    byteString = atob(dataURI.split(',')[1]);
else
    byteString = unescape(dataURI.split(',')[1]);
// separate out the mime component
var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
// write the bytes of the string to a typed array
var ia = new Uint8Array(byteString.length);
for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
}
return new Blob([ia], {type:mimeString});
}



function nonuploadpassportfile(getdata,getcounter,filename,rolename){

$("#"+rolename+"uploadpassportfileloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
var form = new FormData();
form.append('image', file);

var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

        $.ajax({
		    url: "<?php echo BASE_OCR_URL;?>/passport/",
		    type: "POST",
		    contentType: false,
		    cache: false,
		    processData:false,
		    data:form,
		    success: function(response){
                  
                         console.log(response);
		         var obj = JSON.parse(response);                         
		         var Errorstatus = obj.Error;
 
                            $("#b"+rolename+"nonresidentpreviewpassport_"+getcounter).css("display", "block");
                            $("#"+rolename+"uploadpassportfileloadingImage_"+getcounter).css("display", "none");
                            $("#"+rolename+"nonresidentlabel_"+getcounter).text(finallabel);

var fieldHTML ='<div class="modal fade"  data-keyboard="true"  id="'+rolename+'nonresidentpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fieldHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fieldHTML += '<div class="modal-content row">';
				fieldHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - Non Resident - '+rolename+' #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fieldHTML += '<div class="col-lg-12 background-grey-2 popheight">';
				fieldHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="'+rolename+'nonresidentpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="'+rolename+'nonresidentsurname[]" class="form-control" id="'+rolename+'nonresidentpsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="'+rolename+'nonresidentgivenName[]" class="form-control" id="'+rolename+'nonresidentpgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="'+rolename+'nonresidentpassportNumber[]" class="form-control" id="'+rolename+'nonresidentppassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="'+rolename+'nonresidentnationality[]" id="'+rolename+'nonresidentpnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="'+rolename+'nonresidentpcountry_'+getcounter+'" name="'+rolename+'nonresidentcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="'+rolename+'nonresidentmgender_'+getcounter+'" name="'+rolename+'nonresidentpgender[]" class="radio-red">Male</label><label class="radio-inline"><input type="radio" name="'+rolename+'nonresidentpgender[]" id="'+rolename+'nonresidentfgender_'+getcounter+'" class="radio-red" value="female" checked> Female</label></div></div></div></div>';

                                 fieldHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'nonresidentpdob_'+getcounter+'" class="form-control" placeholder="DD-MM-YYYY" name="'+rolename+'nonresidentpdob[]" value=""   maxlength="10"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'nonresidentpdateexpiry_'+getcounter+'" class="form-control" placeholder="DD-MM-YYYY" name="'+rolename+'nonresidentdateexpiry[]" value=""   maxlength="10"></div></div></div></div></div>';


                                 fieldHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaypreviewpopup(\''+rolename+'\','+getcounter+',\''+rolename+'nonresident\');">Save</a></div></div></div>';
				fieldHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fieldHTML);

                                 //Passport Preview - Non resident

				$('#'+rolename+'nonresidentpnationality_'+getcounter).html(countriesOption);
				$('#'+rolename+'nonresidentpnationality_'+getcounter).chosen(); 

				$('#'+rolename+'nonresidentpcountry_'+getcounter).html(countriesOption);
				$('#'+rolename+'nonresidentpcountry_'+getcounter).chosen(); 

		                 $('#'+rolename+'nonresidentpdob_'+getcounter).datepicker({   
		                     beforeShow:function () {
		                     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		                     }
	                         });

                                 $('#'+rolename+'nonresidentpdateexpiry_'+getcounter).datepicker({   
		                     beforeShow:function () {
		                     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		                     }
	                         });

                                 // Declaring variables for preview

                                 
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = obj.DateofIssue; 
                                 var PlaceofBirth = obj.PlaceofBirth;                 
		                 

                                 // Displaying values from OCR - API

                                 $('#'+rolename+'nonresidentpgivenName_'+getcounter).val(gname);
                                 $('#'+rolename+'nonresidentpsurname_'+getcounter).val(Surname);
                                 $('#'+rolename+'nonresidentppassportNumber_'+getcounter).val(passportno);
                                 $('#'+rolename+'nonresidentpdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#'+rolename+'nonresidentpdob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#'+rolename+'nonresidentmgender_'+getcounter).prop('checked', true);
                                  $('#'+rolename+'nonresidentfgender_'+getcounter).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#'+rolename+'nonresidentfgender_'+getcounter).prop('checked', true);
                                  $('#'+rolename+'nonresidentmgender_'+getcounter).prop('checked', false);
                                 }
                                 $('#'+rolename+'nonresidentpnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#'+rolename+'nonresidentpcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 /*$('#'+rolename+'nonresidentpreviewpassport_'+getcounter).modal({backdrop: 'static'});*/ 
                                 $('#'+rolename+'nonresidentpreviewpassport_'+getcounter).modal('show');
              
                    },// end of success call
		 error: function (error) {

		        $("#displaysuccessmsg .modal-title").html(""+rolename+" - Error"); 
		        $("#displaysuccessmsg .modal-body").html("We are having trouble parsing the image. Please enter your passport information manually.");  
		        $("#displaysuccessmsg").modal("show");
                        $("#b"+rolename+"nonresidentpreviewpassport_"+getcounter).css("display", "none");
                        $("#"+rolename+"uploadpassportfileloadingImage_"+getcounter).css("display", "none");
                        $("#"+rolename+"nonresidentlabel_"+getcounter).text();                             
		 } 


        }); //end of ajax call
 
} // end of function

//NRIC front

function nricfront(getdata,getcounter,filename,rolename){

$("#"+rolename+"uploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

	$.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		console.log(URL.createObjectURL(file));
		 

                   $("#b"+rolename+"nricfrontpreview_"+getcounter).css("display", "block");
                   $("#"+rolename+"uploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#"+rolename+"nricfrontlabel_"+getcounter).text(finallabel);  

                   var nricfrontfield = '<div class="modal fade"  data-keyboard="true"  id="'+rolename+'nricfrontpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricfrontfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricfrontfield += '<div class="modal-content row">';
                     nricfrontfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Front Information - '+rolename+'#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricfrontfield += '<div class="col-lg-12 background-grey-2 popheight">'; 
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="'+rolename+'nricfrontimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricfrontfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="'+rolename+'previewfrontnricnumber[]" class="form-control" id="'+rolename+'previewfrontnricnumber_'+getcounter+'" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="'+rolename+'previewnricfrontmgender_'+getcounter+'" name="'+rolename+'previewnricfrontgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="'+rolename+'previewnricfrontfgender_'+getcounter+'" name="'+rolename+'previewnricfrontgender[]" class="radio-red" type="radio" checked> Female</label></div></div></div></div>';

                     nricfrontfield += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input id="'+rolename+'previewnricfrontdob_'+getcounter+'" class="form-control" name="'+rolename+'previewnricfrontdob[]" placeholder="DD-MM-YYYY" type="text"   maxlength="10"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="'+rolename+'previewnricfrontnationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="'+rolename+'previewnricfrontnationality_'+getcounter+'"></select></div></div></div></div>';

                     nricfrontfield += '<div class="form-group row"><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="'+rolename+'previewnricfrontpostcode_'+getcounter+'" name="'+rolename+'previewnricfrontpostcode[]" placeholder="Postal Code" type="hidden"></div><div class="col-sm-8 mb-2"><textarea name="'+rolename+'previewnricfrontstreetname[]" id="'+rolename+'previewnricfrontstreetname_'+getcounter+'" class="form-control" style="display:none;" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="'+rolename+'previewnricfrontfloor_'+getcounter+'" name="'+rolename+'previewnricfrontfloor[]" placeholder="Floor" type="hidden"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="'+rolename+'previewnricfrontunit_'+getcounter+'" name="'+rolename+'previewnricfrontunit[]" placeholder="Unit" type="hidden"></div></div>';

                     nricfrontfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaypreviewpopup(\''+rolename+'\','+getcounter+',\''+rolename+'nricfront\');">Save</a></div></div></div>';

                     nricfrontfield += '</div></div></div></div>'; 
                    //TODO made before to after
		     $('body').find('#additionalRole:last').after(nricfrontfield);

                     $('#'+rolename+'previewnricfrontnationality_'+getcounter).html(countriesOption);
                     $('#'+rolename+'previewnricfrontnationality_'+getcounter).chosen();  

                      $('#'+rolename+'previewnricfrontdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      }); 
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;
                         var streetname = obj.Street;
                         var postcode = obj.Postal;
                         var flooring = obj.Floor;
                         var unit = obj.Unit; 

                         $('#'+rolename+'nricfrontimage_'+getcounter+' img').attr("src", srcfilename);  
                         if(sex == 'Male'){
                          $('#'+rolename+'previewnricfrontmgender_'+getcounter).prop('checked', true);
                          $('#'+rolename+'previewnricfrontfgender_'+getcounter).prop('checked', false); 
                         }
                         else if(sex == 'Female'){
                          $('#'+rolename+'previewnricfrontfgender_'+getcounter).prop('checked', true);
                          $('#'+rolename+'previewnricfrontmgender_'+getcounter).prop('checked', false);
                         }
                         $('#'+rolename+'previewfrontnricnumber_'+getcounter).val(nricno);
                         $('#'+rolename+'previewnricfrontnationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#'+rolename+'previewnricfrontdob_'+getcounter).val(dobs);                         
                         /*$('#'+rolename+'nricfrontpreview_'+getcounter).modal({backdrop: 'static'});*/ 
                         $('#'+rolename+'nricfrontpreview_'+getcounter).modal('show');
                         

	               },// end of success form
		 error: function (error) {

		        $("#displaysuccessmsg .modal-title").html(""+rolename+" - Error"); 
		        $("#displaysuccessmsg .modal-body").html("We are having trouble parsing the image. Please enter your passport information manually.");  
		        $("#displaysuccessmsg").modal("show");
                        $("#b"+rolename+"nricfrontpreview_"+getcounter).css("display", "none");
                        $("#"+rolename+"uploadnricloadingImage_"+getcounter).css("display", "none");
                        $("#"+rolename+"nricfrontlabel_"+getcounter).text();  
		    
		 } 
	}); 
}

//NRIC back

function nricback(getdata,getcounter,filename,rolename){

$("#"+rolename+"uploadnricloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/nric-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		   $("#b"+rolename+"nricbackpreview_"+getcounter).css("display", "block"); 
                   $("#"+rolename+"uploadnricloadingImage_"+getcounter).css("display", "none");
                   $("#"+rolename+"nricbacklabel_"+getcounter).text(finallabel);

                   var nricbackfield = '<div class="modal fade"  data-keyboard="true"  id="'+rolename+'nricbackpreview_'+getcounter+'" tabindex="-1" role="dialog" style="display:none;" aria-hidden="true">';
                     nricbackfield += '<div class="modal-dialog modal-dialog-centered" role="document">';
                     nricbackfield += '<div class="modal-content row">';
                     nricbackfield += '<div class="modal-header"><h5 class="modal-title" id="">NRIC Back Information - '+rolename+'#'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
                      nricbackfield += '<div class="col-lg-12 background-grey-2 popheight">'; 
                      nricbackfield += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="'+rolename+'nricbackimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                      nricbackfield += '<div class="row mt-3" style="display:none;"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">NRIC Number</label><div class="col-sm-12"><input name="'+rolename+'previewbacknricnumber[]" class="form-control" id="'+rolename+'previewbacknricnumber_'+getcounter+'" value="" type="hidden"></div></div></div><div class="col-sm" style="display:none;"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input id="'+rolename+'previewnricbackmgender_'+getcounter+'" name="'+rolename+'previewnricbackgender[]" class="radio-red" type="radio"> Male</label><label class="radio-inline"><input id="'+rolename+'previewnricbackfgender_'+getcounter+'" name="'+rolename+'previewnricbackgender[]" class="radio-red" type="radio" checked> Female</label></div></div></div></div>';

                     nricbackfield += '<div class="row" style="display:none;"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Date of Birth</label><div class="col-sm-12"><div class="input-group"><input id="'+rolename+'previewnricbackdob_'+getcounter+'" class="form-control" name="'+rolename+'previewnricbackdob[]" placeholder="DD/MM/YY" type="hidden"></div></div></div></div><div class="col-sm" style="display:none;"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Country of Birth</label><div class="col-sm-12"><select name="'+rolename+'previewnricbacknationality[]" data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="'+rolename+'previewnricbacknationality_'+getcounter+'"></select></div></div></div></div>';

                     nricbackfield += '<div class="form-group row"><div class="col-sm-12"><label class="col-form-label required">Residencial Address</label></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="'+rolename+'previewnricbackpostcode_'+getcounter+'" name="'+rolename+'previewnricbackpostcode[]" placeholder="Postal Code" type="text"></div><div class="col-sm-8 mb-2"><textarea name="'+rolename+'previewnricbackstreetname[]" id="'+rolename+'previewnricbackstreetname_'+getcounter+'" class="form-control" rows="1" placeholder="Street Name"></textarea></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="'+rolename+'previewnricbackfloor_'+getcounter+'" name="'+rolename+'previewnricbackfloor[]" placeholder="Floor" type="text"></div><div class="col-sm-4 form-bg-2 mb-2"><input class="form-control" id="'+rolename+'previewnricbackunit_'+getcounter+'" name="'+rolename+'previewnricbackunit[]" placeholder="Unit" type="text"></div></div>';

                     nricbackfield += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaypreviewpopup(\''+rolename+'\','+getcounter+',\''+rolename+'nricback\');">Save</a></div></div></div>';

                     nricbackfield += '</div></div></div></div>'; 
                     $('body').find('#additionalRole:last').before(nricbackfield);

                     $('#'+rolename+'previewnricbacknationality_'+getcounter).html(countriesOption);
                     $('#'+rolename+'previewnricbacknationality_'+getcounter).chosen(); 

                      $('#'+rolename+'previewnricbackdob_'+getcounter).datepicker({   
		             beforeShow:function () {
		             setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
		             }
                      });   
                
                         var nricno = obj.NRICnumber;
		         var dateofissue = obj.DateofIssue;
		         var idcard = obj.IdCardNumber;
		         var fullname = obj.Name;
		         var race = obj.Race; 
		         var dobs = obj.DateofBirth; 
		         var Cntryofbirth = obj.CountryofBirth;
		         var sex = obj.Sex;
                         var streetname = obj.Street;
                         var postcode = obj.Postal;
                         var flooring = obj.Floor;
                         var unit = obj.Unit; 

                         if(sex == 'Male'){
                          $('#'+rolename+'previewnricbackmgender_'+getcounter).prop('checked', true);
                          $('#'+rolename+'previewnricbackfgender_'+getcounter).prop('checked', false);
                         }
                         else if(sex == 'Female'){
                          $('#'+rolename+'previewnricbackfgender_'+getcounter).prop('checked', true);
                          $('#'+rolename+'previewnricbackmgender_'+getcounter).prop('checked', false);
                         }

                         $('#'+rolename+'nricbackimage_'+getcounter+' img').attr("src", srcfilename);
                         $('#'+rolename+'previewbacknricnumber_'+getcounter).val(nricno);
                         $('#'+rolename+'previewnricbacknationality_'+getcounter+'_chosen span').text(Cntryofbirth); 
                         $('#'+rolename+'previewnricbackdob_'+getcounter).val(dobs);
                         $('#'+rolename+'previewnricbackpostcode_'+getcounter).val(postcode);
                         $('#'+rolename+'previewnricbackstreetname_'+getcounter).val(streetname);
                         $('#'+rolename+'previewnricbackfloor_'+getcounter).val(flooring);
                         $('#'+rolename+'previewnricbackunit_'+getcounter).val(unit);
                         /*$('#'+rolename+'nricbackpreview_'+getcounter).modal({backdrop: 'static'});*/ 
                         $('#'+rolename+'nricbackpreview_'+getcounter).modal('show');
                         
 
		
	         },// end of success form
		 error: function (error) {

		        $("#displaysuccessmsg .modal-title").html(""+rolename+" - Error"); 
		        $("#displaysuccessmsg .modal-body").html("We are having trouble parsing the image. Please enter your passport information manually.");  
		        $("#displaysuccessmsg").modal("show");
                        $("#b"+rolename+"nricbackpreview_"+getcounter).css("display", "none");
                        $("#"+rolename+"uploadnricloadingImage_"+getcounter).css("display", "none");
                        $("#"+rolename+"nricbacklabel_"+getcounter).text();  
		    
		 } 
   }); //end of ajax call

}//end of function

//FIn Passport

function finuploadpassportfile(getdata,getcounter,filename,rolename){

$("#"+rolename+"uploadPassportloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0];  
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/passport/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		   $("#b"+rolename+"fincardpreviewpassport_"+getcounter).css("display", "block"); 
                   $("#"+rolename+"uploadPassportloadingImage_"+getcounter).css("display", "none");
                   $("#"+rolename+"finpassportlabel_"+getcounter).text(finallabel);


                    var fincardHTML ='<div class="modal fade"  data-keyboard="true"  id="'+rolename+'fincardpreviewpassport_'+getcounter+'" tabindex="-1" role="dialog">';
				fincardHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
				fincardHTML += '<div class="modal-content row">';
				fincardHTML += '<div class="modal-header"><h5 class="modal-title" id="">Passport Information - WorkPass - '+rolename+' #'+getcounter+'</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
				fincardHTML += '<div class="col-lg-12 background-grey-2 popheight">';
				fincardHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="'+rolename+'fincardpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Surname</label><div class="col-sm-12"><input type="text" name="'+rolename+'fincardsurname[]" class="form-control" id="'+rolename+'fincardpsurname_'+getcounter+'" placeholder="" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Given name</label><div class="col-sm-12"><input type="text" name="'+rolename+'fincardgivenName[]" class="form-control" id="'+rolename+'fincardpgivenName_'+getcounter+'"  value="" /></div></div></div></div>';	

				 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Passport Number</label><div class="col-sm-12"><input type="text" name="'+rolename+'fincardpassportNumber[]" class="form-control" id="'+rolename+'fincardppassportNumber_'+getcounter+'" value="" /></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Nationality</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" name="'+rolename+'fincardpnationality[]" id="'+rolename+'fincardpnationality_'+getcounter+'"></select></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Country of Issue</label><div class="col-sm-12"><select data-placeholder="Please select" class="chosen-select custom-select chosen-select-country" id="'+rolename+'fincardpcountry_'+getcounter+'" name="'+rolename+'fincardpcountry[]"></select></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Gender</label><div class="col-sm-12"><label class="radio-inline mr-3"><input type="radio" id="'+rolename+'fincardmgender_'+getcounter+'" name="'+rolename+'fincardpgender[]" class="radio-red">Male</label><label class="radio-inline"><input type="radio" name="'+rolename+'fincardpgender[]" id="'+rolename+'fincardfgender_'+getcounter+'" class="radio-red" value="female" checked> Female</label></div></div></div></div>';

                                 fincardHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Birth</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'fincardpdob_'+getcounter+'" class="form-control" name="'+rolename+'fincarddob[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'fincardpdateexpiry_'+getcounter+'" class="form-control" name="'+rolename+'fincarddateexpiry[]" value=""   maxlength="10" placeholder="DD-MM-YYYY"></div></div></div></div></div>';

                                
                                 fincardHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaypreviewpopup(\''+rolename+'\','+getcounter+',\''+rolename+'finpassport\');">Save</a></div></div></div>';
				fincardHTML += '</div></div></div></div>';
                                 
                                 $('body').find('#additionalRole:last').after(fincardHTML);

                                 //Passport Preview - Work Pass

				$('#'+rolename+'fincardpnationality_'+getcounter).html(countriesOption);
				$('#'+rolename+'fincardpnationality_'+getcounter).chosen(); 

				$('#'+rolename+'fincardpcountry_'+getcounter).html(countriesOption);
				$('#'+rolename+'fincardpcountry_'+getcounter).chosen(); 

				$('#'+rolename+'fincardplaceofBirth_'+getcounter).html(countriesOption);
				$('#'+rolename+'fincardplaceofBirth_'+getcounter).chosen(); 

                                 // Declaring variables for preview
                                 $('#'+rolename+'fincardpdob_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 }); 

                                 $('#'+rolename+'fincardpdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
				 });     
		                 var gname = obj.GivenName;
                                 var Surname = obj.Surname; 
                                 var passportno = obj.PassportNumber;
		                 var DateofExpiry = obj.DateofExpiry;
		                 var DateofBirth = obj.DateofBirth;                                 
		                 var sex = obj.Sex;
                                 var CountryofIssue = obj.CountryofIssue;
		                 var Nationality = obj.Nationality;  
                                 var DateofIssue = obj.DateofIssue; 
                                 var PlaceofBirth = obj.PlaceofBirth;                 
		                 

                                 // Displaying values from OCR - API

                                 $('#'+rolename+'fincardpgivenName_'+getcounter).val(gname);
                                 $('#'+rolename+'fincardpsurname_'+getcounter).val(Surname);
                                 $('#'+rolename+'fincardppassportNumber_'+getcounter).val(passportno);
                                 $('#'+rolename+'fincardpdateexpiry_'+getcounter).val(DateofExpiry);
                                 $('#'+rolename+'fincardpdob_'+getcounter).val(DateofBirth);                           
                                 if(sex == 'Male'){
                                  $('#'+rolename+'fincardmgender_'+getcounter).prop('checked', true);
                                  $('#'+rolename+'fincardfgender_'+getcounter).prop('checked', false);
                                 }
                                 else if(sex == 'Female'){
                                  $('#'+rolename+'fincardfgender_'+getcounter).prop('checked', true);
                                  $('#'+rolename+'fincardmgender_'+getcounter).prop('checked', false); 
                                 }
                                 $('#'+rolename+'fincardpnationality_'+getcounter+'_chosen span').text(Nationality);
                                 $('#'+rolename+'fincardpcountry_'+getcounter+'_chosen span').text(CountryofIssue);
                                 /*$('#'+rolename+'fincardpreviewpassport_'+getcounter).modal({backdrop: 'static'}); */ 
                                 $('#'+rolename+'fincardpreviewpassport_'+getcounter).modal('show');      
 
		
	    },// end of success form
		 error: function (error) {

		        $("#displaysuccessmsg .modal-title").html(""+rolename+" - Error"); 
		        $("#displaysuccessmsg .modal-body").html("We are having trouble parsing the image. Please enter your passport information manually.");  
		        $("#displaysuccessmsg").modal("show");
                        $("#b"+rolename+"fincardpreviewpassport_"+getcounter).css("display", "none"); 
                        $("#"+rolename+"uploadPassportloadingImage_"+getcounter).css("display", "none");
                        $("#"+rolename+"finpassportlabel_"+getcounter).text();
		    
		 } 
   }); //end of ajax call

}//end of function

// Fin front

function fincardfront(getdata,getcounter,filename,rolename){

$("#"+rolename+"uploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-front/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

                   $("#b"+rolename+"fincardfrontpreview_"+getcounter).css("display", "block"); 
                   $("#"+rolename+"uploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#"+rolename+"fincardfrontlabel_"+getcounter).text(finallabel);


                var fincardfrontHTML ='<div class="modal fade"  data-keyboard="true"  id="'+rolename+'fincardfrontpreview_'+getcounter+'" tabindex="-1" role="dialog">';
		fincardfrontHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
		fincardfrontHTML += '<div class="modal-content row">';
		fincardfrontHTML += '<div class="modal-header"><h5 class="modal-title" id="'+rolename+'fincardfrontpreview">'+rolename+' #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
		 fincardfrontHTML += '<div class="col-lg-12 background-grey-2 popheight">';

                 fincardfrontHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="'+rolename+'fincardfrontpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="'+rolename+'fincardfrontemployer[]" class="form-control" id="'+rolename+'fincardfrontemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="'+rolename+'fincardfrontoccupation[]" class="form-control" id="'+rolename+'fincardfrontoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

                  fincardfrontHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

                  fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label ">Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'fincardfrontdateissue_'+getcounter+'" placeholder="DD-MM-YYYY" class="form-control" name="'+rolename+'fincardfrontdateissue[]" value=""   maxlength="10"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'fincardfrontdateexpiry_'+getcounter+'" placeholder="DD-MM-YYYY" class="form-control" name="'+rolename+'fincardfrontdateexpiry[]" value=""   maxlength="10"></div></div></div></div></div>';

                 fincardfrontHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="'+rolename+'wfincardfrontNumber[]" class="form-control" id="'+rolename+'wfincardfrontNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

                 fincardfrontHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaypreviewpopup(\''+rolename+'\','+getcounter+',\''+rolename+'fincardfront\');">Save</a></div></div></div>';
		fincardfrontHTML += '</div></div></div></div>';   
                   
                 $('body').find('#additionalRole:last').after(fincardfrontHTML);


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#'+rolename+'fincardfrontdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 }); 

                         $('#'+rolename+'fincardfrontdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         $('#'+rolename+'fincardfrontemployer_'+getcounter).val(Employer);
                         $('#'+rolename+'fincardfrontoccupation_'+getcounter).val(Occupation);
                         $('#'+rolename+'fincardfrontdateissue_'+getcounter).val(DateofIssue);
                         $('#'+rolename+'fincardfrontdateexpiry_'+getcounter).val(DateofExpiry);
                         $('#'+rolename+'wfincardfrontNumber_'+getcounter).val(FIN); 
                         /*$('#'+rolename+'fincardfrontpreview_'+getcounter).modal({backdrop: 'static'});*/  
                         $('#'+rolename+'fincardfrontpreview_'+getcounter).modal('show');
 
		
	   },// end of success form
		 error: function (error) {

		        $("#displaysuccessmsg .modal-title").html(""+rolename+" - Error"); 
		        $("#displaysuccessmsg .modal-body").html("We are having trouble parsing the image. Please enter your passport information manually.");  
		        $("#displaysuccessmsg").modal("show");
                         $("#b"+rolename+"fincardfrontpreview_"+getcounter).css("display", "none"); 
                         $("#"+rolename+"uploadfincardloadingImage_"+getcounter).css("display", "none");
                         $("#"+rolename+"fincardfrontlabel_"+getcounter).text();
		    
		 } 
   }); //end of ajax call

}//end of function

// Fin back

function fincardback(getdata,getcounter,filename,rolename){

$("#"+rolename+"uploadfincardloadingImage_"+getcounter).css("display","block");
//alert(getcounter);
var file = getdata[0]; 
//var filename = file.name;
var form = new FormData();
form.append('image', file);
var finallabel = filename.slice(0, parsecount) + (filename.length > parsecount ? "..." : "");
var srcfilename = URL.createObjectURL(file);

   $.ajax({
	    url: "<?php echo BASE_OCR_URL;?>/ep-back/",
	    type: "POST",
	    contentType: false,
	    cache: false,
	    processData:false,
	    data:form,
	    success: function(response){
	  
		 console.log(response);
		 var obj = JSON.parse(response);                         
		 var Errorstatus = obj.Error; 

		   $("#b"+rolename+"fincardbackpreview_"+getcounter).css("display", "block");  
                   $("#"+rolename+"uploadfincardloadingImage_"+getcounter).css("display", "none");
                   $("#"+rolename+"fincardbacklabel_"+getcounter).text(finallabel);


                   var fincardbackHTML ='<div class="modal fade"  data-keyboard="true"  id="'+rolename+'fincardbackpreview_'+getcounter+'" tabindex="-1" role="dialog">';
			fincardbackHTML += '<div class="modal-dialog modal-dialog-centered" role="document">';
			fincardbackHTML += '<div class="modal-content row">';
			fincardbackHTML += '<div class="modal-header"><h5 class="modal-title" id="'+rolename+'fincardbackpreview">'+rolename+' #'+getcounter+' WorkPass Information</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><i class="fa fa-times-circle float-right" aria-hidden="true"></i></button></div>';
			 fincardbackHTML += '<div class="col-lg-12 background-grey-2 popheight">';

                         fincardbackHTML += '<div class="row mt-3"><div class="col-sm"><div class="form-group row"><div class="col-sm-12">Please check details below.</div><div class="col-sm-12"><div class="passport-scan-copy" id="'+rolename+'fincardbackpassportimage_'+getcounter+'"><img src="'+URL.createObjectURL(file)+'" alt="" /></div></div></div></div></div>';
		     
		         fincardbackHTML += '<div class="row" style="display:none;"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Employer <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="'+rolename+'fincardbackemployer[]" class="form-control" id="'+rolename+'fincardbackemployer_'+getcounter+'" placeholder="" value="" type="text"></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required">Occupation <span class="small color-pale-red"><sup>1</sup></span></label><div class="col-sm-12"><input name="'+rolename+'fincardbackoccupation[]" class="form-control" id="'+rolename+'fincardbackoccupation_'+getcounter+'" value="" type="text"></div></div></div></div>'; 

		          fincardbackHTML += '<div class="row mb-2"><div class="col-sm"><span class="small font-italic color-pale-red"><sup>1</sup>Not required for Dependent pass holders</span></div></div>';

		          fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Issue</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'fincardbackdateissue_'+getcounter+'" placeholder="DD-MM-YYYY" class="form-control" name="'+rolename+'fincardbackdateissue[]" value=""   maxlength="10"></div></div></div></div><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label" >Date of Expiry</label><div class="col-sm-12"><div class="input-group dateicon"><input type="text" id="'+rolename+'fincardbackdateexpiry_'+getcounter+'" class="form-control" placeholder="DD-MM-YYYY" name="'+rolename+'fincardbackdateexpiry[]" value=""   maxlength="10"></div></div></div></div></div>';

		         fincardbackHTML += '<div class="row"><div class="col-sm"><div class="form-group row"><label class="col-sm-12 col-form-label required" >Fin Number</label><div class="col-sm-6"><input type="text" name="'+rolename+'wfincardbackNumber[]" class="form-control" id="'+rolename+'wfincardbackNumber_'+getcounter+'"  value="" /></div></div></div></div>';        

		         fincardbackHTML += '<div class="row my-5"><div class="col-sm-12"><div class="text-center"><a class="btn btn-next btn-lg" onclick="displaypreviewpopup(\''+rolename+'\','+getcounter+',\''+rolename+'fincardback\');">Save</a></div></div></div>';
			fincardbackHTML += '</div></div></div></div>';   
		           
		         $('body').find('#additionalRole:last').after(fincardbackHTML);


                         var Occupation = obj.Occupation;
	                 var Name = obj.Name;
	                 var DateofIssue = obj.DateofIssue;
	                 var DateofApplication = obj.DateofApplication;
                         var DateofExpiry = obj.DateofExpiry;
	                 var FIN = obj.FIN; 		               
	                 var Employer = obj.Employer;

                         $('#'+rolename+'fincardbackdateissue_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         $('#'+rolename+'fincardbackdateexpiry_'+getcounter).datepicker({   
				     beforeShow:function () {
				     setTimeout(function () { $('.ui-datepicker').css('z-index', 1150);}, 0);
				     }
			 });

                         $('#'+rolename+'fincardbackemployer_'+getcounter).val(Employer);
                         $('#'+rolename+'fincardbackoccupation_'+getcounter).val(Occupation);
                         $('#'+rolename+'fincardbackdateissue_'+getcounter).val(DateofIssue);
                         $('#'+rolename+'fincardbackdateexpiry_'+getcounter).val(DateofExpiry);
                         $('#'+rolename+'wfincardbackNumber_'+getcounter).val(FIN); 
                         /*$('#'+rolename+'fincardbackpreview_'+getcounter).modal({backdrop: 'static'});*/  
                         $('#'+rolename+'fincardbackpreview_'+getcounter).modal('show');
 
		 },// end of success form                 
		 error: function (error) {

                        $("#displaysuccessmsg .modal-title").html(""+rolename+" - Error"); 
                        $("#displaysuccessmsg .modal-body").html("We are having trouble parsing the image. Please enter your passport information manually.");  
                        $("#displaysuccessmsg").modal("show");
                        $("#b"+rolename+"fincardbackpreview_"+getcounter).css("display", "none");  
                        $("#"+rolename+"uploadfincardloadingImage_"+getcounter).css("display", "none");
                        $("#"+rolename+"fincardbacklabel_"+getcounter).text();
		    
		 }
   }); //end of ajax call

}//end of function



</script>
