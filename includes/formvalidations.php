
<?php 
/* formvalidations js functions */
?>
<script type="text/javascript">
$(document).ready(function(){
	localStorage.clear();
});

var isValidated = true;


function dataURItoBlob(dataURI) {
  // convert base64/URLEncoded data component to raw binary data held in a string
  var byteString;
  if (dataURI.split(',')[0].indexOf('base64') >= 0)
    byteString = atob(dataURI.split(',')[1]);
  else
    byteString = unescape(dataURI.split(',')[1]);
  // separate out the mime component
  var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
  // write the bytes of the string to a typed array
  var ia = new Uint8Array(byteString.length);
  for (var i = 0; i < byteString.length; i++) {
    ia[i] = byteString.charCodeAt(i);
  }
  return new Blob([ia], {
    type: mimeString
  });
}

// TODO Handle uploading of pdf files
function updatePdfImages() {

return new Promise(function(resolve, reject) {
  $("#form1").each(function () {
    var allinput = $(this).find(':input');

    for (var inputtype in allinput) {


      if (allinput[inputtype].type === "file") {

      if(allinput[inputtype].files===undefined || allinput[inputtype].files.length ===0){
	console.log("###################" + allinput[inputtype].name);
	continue;	
	}
        if (allinput[inputtype].files[0].name.split('.').pop() == "pdf") {

          var fileReader = new FileReader();
          try {
            console.log(" Parsing pdf for file upload");

            fileReader.onload = function () {

                console.log(" File reader started");
              //Step 4:turn array buffer into typed array
              var typedarray = new Uint8Array(this.result);

              //Step 5:PDFJS should be able to read this
              pdfjsLib.getDocument(typedarray).then(function (pdf) {
                pdf.getPage(1).then(function (page) {

		console.log(" Extracted the first page");

                  var viewport = page.getViewport(scale);
                  var canvas = document.createElement('canvas'),
                    ctx = canvas.getContext('2d');
                  var renderContext = {
                    canvasContext: ctx,
                    viewport: viewport
                  };
                  canvas.height = viewport.height;
                  canvas.width = viewport.width;
                  page.render(renderContext).then(function () {

		console.log("rendered page context getting fileblob");
                    var file_blob = [];
                    file_blob.push(dataURItoBlob(canvas.toDataURL()));

                    var form = new FormData();
		console.log("$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$"+allinput[inputtype].name);		    
                    form.append(allinput[inputtype].name, file);

                    $.ajax({
                      url: "/uploadPDFDocuments.php",
                      type: "POST",
                      contentType: false,
                      cache: false,
                      processData: false,
                      data: form,
                      success: function (response) {

                        console.log(response);
			resolve();
                      },
                      error: function (response) {

                        console.log(response);
                        resolve();
                      }

                    });
                  });
                });


              });
            };

            fileReader.readAsArrayBuffer(allinput[inputtype].files[0]);


          } catch (err) {

            console.log(err);
            resolve();
          }


        }

      }

    }
  });

});
}





function hasManualEntry(manual_type){
	var value = localStorage.getItem(manual_type);
	console.log("manual item: "+manual_type+" val: "+value);
	return value;
}

function showProposedCompanyEmpty(show){
	var proposedCompanyAlert = $('.proposed-company-alert');
	var proposedCompanyAlertMsg = $('#proposed-company-alert-msg');
	if (show == true) {
		proposedCompanyAlertMsg.text(ERR_MSG_PROPOSED_COMPANYNAME_EMPTY);
		proposedCompanyAlert.removeClass('d-none').prev().find('input, textarea').css('border-color','red');
	} else {
		proposedCompanyAlert.addClass('d-none').prev().find('input, textarea').css('border-color','');
	}
}
function showOwnAddressInvalid(field, show){
	var proposedCompanyAddressAlert = $('#proposed-company-address-alert');
	var proposedCompanyAddressAlertMsg = $('#proposed-company-address-alert-msg');
	var inputField = "";

	if (show == true) {
		if(field == ADDRESS_POSTAL_CODE){
			proposedCompanyAddressAlertMsg.text(ERR_MSG_POSTALCODE_INVALID);
			inputField = "#postal-code";	
		}else if(field == ADDRESS_FLOOR){
			proposedCompanyAddressAlertMsg.text(ERR_MSG_FLOOR_INVALID);	
			inputField = "#floor";
		}		
		proposedCompanyAddressAlert.show();
		$(inputField).css('border-color', 'red');
        $("#serviceaddress").css('border-color','');
	} else {
		proposedCompanyAddressAlert.hide().prev().find('input').css('border-color','');
	}
}
/*function showProposedCompanyAddressEmpty(show){
	var proposedCompanyAddressAlert = $('#proposed-company-address-alert');
	var proposedCompanyAddressAlertMsg = $('#proposed-company-address-alert-msg');
	if (show == true) {
		proposedCompanyAddressAlertMsg.text(ERR_MSG_ADDRESS_TYPE_EMPTY);
		proposedCompanyAddressAlert.show();
	} else {
		proposedCompanyAddressAlert.hide();
	}
}
function showOwnAddressEmpty(field, show){
	var proposedCompanyAddressAlert = $('#proposed-company-address-alert');
	var proposedCompanyAddressAlertMsg = $('#proposed-company-address-alert-msg');
	if (show == true) {
		if(field == ADDRESS_POSTAL_CODE){
			proposedCompanyAddressAlertMsg.text(ERR_MSG_POSTALCODE_EMPTY);	
		}else if(field == ADDRESS_STREET_NAME){
			proposedCompanyAddressAlertMsg.text(ERR_MSG_STREETNAME_EMPTY);	
		}else if(field == ADDRESS_FLOOR){
			proposedCompanyAddressAlertMsg.text(ERR_MSG_FLOOR_EMPTY);	
		}else if(field == ADDRESS_UNIT){
			proposedCompanyAddressAlertMsg.text(ERR_MSG_UNIT_EMPTY);	
		}
		
		proposedCompanyAddressAlert.show().prev().find('input, textarea').css('border-color','red');
                $("#serviceaddress").css('border-color','');
	} else {
		proposedCompanyAddressAlert.hide().prev().find('input, textarea').css('border-color','');
	}
}
function showBussinessActivitiesEmpty(show){
	var businessActivitiesAlert = $('#businessActivities-alert');
	var businessActivitiesAlertMsg = $('#businessActivities-alert-msg');
	if (show == true) {
		businessActivitiesAlertMsg.text(ERR_MSG_BUSINESS_ACTIVITY_EMPTY);
		businessActivitiesAlert.show().prev().find('input, textarea').css('border-color','red');
	} else {
		businessActivitiesAlert.hide().prev().find('input, textarea').css('border-color','');
	}
}
function showCountriesOperationEmpty(show){
	var alert = $('#countriesOperation-alert');
	var alertMsg = $('#countriesOperation-alert-msg');
	if (show == true) {
		alertMsg.text(ERR_MSG_COUNTRIES_OPERATION_EMPTY);
		alert.show();
                $('#countriesOperation_chosen').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide();
                $('#countriesOperation_chosen').css('border','0px').css('border-radius','0.25rem');
	}
}*/
function showRoleSelectorEmpty(show){
	var roleSelectorAlert = $('#roleSelector-alert');
	var roleSelectorAlertMsg = $('#roleSelector-alert-msg');
	if (show == true) {
		roleSelectorAlertMsg.text(ERR_MSG_ROLE_EMPTY);
                roleSelectorAlert.prev().find('select').css('border-color','red');
	} else {
		roleSelectorAlert.hide().prev().find('input, textarea').css('border-color','');
	}
}
function showAddRoleSelectorEmpty(show){
	var addRoleSelectorAlert = $('#addRoleSelector-alert');
	var addRoleSelectorAlertMsg = $('#addRoleSelector-alert-msg');
	if (show == true) {
		addRoleSelectorAlertMsg.text(ERR_MSG_ADD_ROLE_EMPTY);
		addRoleSelectorAlert.show().prev().find('input').css('border-color','red');
	} else {
		addRoleSelectorAlert.hide().prev().find('input').css('border-color','');
	}
}
function showFullNameEmpty(role, formNo, show){
	var fullNameAlert;
	var fullNameAlertMsg;

	if(role == ROLE_DIRECTOR){
		fullNameAlert = $('#fullName'+ formNo +'-alert');
		fullNameAlertMsg = $('#fullName'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		fullNameAlert = $('#shareholderfullName'+ formNo +'-alert');
		fullNameAlertMsg = $('#shareholderfullName'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		fullNameAlert = $('#dishareholderfullName'+ formNo +'-alert');
		fullNameAlertMsg = $('#dishareholderfullName'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		fullNameAlert = $('#corporatename'+ formNo +'-alert');
		fullNameAlertMsg = $('#corporatename'+ formNo +'-alert-msg');
	} 
	if (show == true) {
		fullNameAlertMsg.text(ERR_MSG_FULLNAME_EMPTY);
		fullNameAlert.show().prev().css('border-color','red');
	} else {
		fullNameAlert.hide().prev().css('border-color','');
	}
}



function showResStatusEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#residencyStatus'+ formNo +'-alert');
		alertMsg = $('#residencyStatus'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderresidencyStatus'+ formNo +'-alert');
		alertMsg = $('#shareholderresidencyStatus'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderresidencyStatus'+ formNo +'-alert');
		alertMsg = $('#dishareholderresidencyStatus'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporateresidencyStatus'+ formNo +'-alert');
		alertMsg = $('#corporateresidencyStatus'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_RESIDENTIAL_STATUS_EMPTY);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}

function showPassportEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directornonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#directornonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdernonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#shareholdernonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dinonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#dinonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatenonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#corporatenonresidentuploadpassport'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PASSPORT_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}


function showPassportDataEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directornonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#directornonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdernonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#shareholdernonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dinonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#dinonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatenonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#corporatenonresidentuploadpassport'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text("Please verify your passport details");
		alert.show().prev().find('.custom-file');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}


function showPassportDataManualEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directornonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#directornonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdernonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#shareholdernonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dinonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#dinonresidentuploadpassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatenonresidentuploadpassport'+ formNo +'-alert');
		alertMsg = $('#corporatenonresidentuploadpassport'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text("Please upload your passport or verify your manual passport details");
		alert.show().prev().find('.custom-file');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

function showNRICDataManualEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directornricfront'+ formNo +'-alert');
		alertMsg = $('#directornricfront'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdernricfront'+ formNo +'-alert');
		alertMsg = $('#shareholdernricfront'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dinricfront'+ formNo +'-alert');
		alertMsg = $('#dinricfront'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatenricfront'+ formNo +'-alert');
		alertMsg = $('#corporatenricfront'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text("Please upload your NRIC documents or verify your manual NRIC details");
		alert.show().prev().find('.custom-file');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

function showFINDataManualEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directorfincardfront'+ formNo +'-alert');
		alertMsg = $('#directorfincardfront'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderfincardfront'+ formNo +'-alert');
		alertMsg = $('#shareholderfincardfront'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#difincardfront'+ formNo +'-alert');
		alertMsg = $('#difincardfront'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatefincardfront'+ formNo +'-alert');
		alertMsg = $('#corporatefincardfront'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text("Please upload you Passport and FIN documents or verify your manual Passport/FIN details");
		alert.show().prev().find('.custom-file');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}



function showAddressProofEmpty(role, formNo, show){
	//console.log("showAddressProofEmpty: " + show);
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directornonresidentaddress'+ formNo +'-alert');
		alertMsg = $('#directornonresidentaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		//console.log('#shareholdernonresidentaddress'+ formNo +'-alert......');
		alert = $('#shareholdernonresidentaddress'+ formNo +'-alert');
		alertMsg = $('#shareholdernonresidentaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dinonresidentaddress'+ formNo +'-alert');
		alertMsg = $('#dinonresidentaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatenonresidentaddress'+ formNo +'-alert');
		alertMsg = $('#corporatenonresidentaddress'+ formNo +'-alert-msg');
	}
	if (show == true) {
		//console.log("showing alert....");
		alertMsg.text(ERR_MSG_PROOFADDRESS_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showAddressEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directornonresidentresaddress'+ formNo +'-alert');
		alertMsg = $('#directornonresidentresaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdernonresidentresaddress'+ formNo +'-alert');
		alertMsg = $('#shareholdernonresidentresaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dinonresidentresaddress'+ formNo +'-alert');
		alertMsg = $('#dinonresidentresaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatenonresidentresaddress'+ formNo +'-alert');
		alertMsg = $('#corporatenonresidentresaddress'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_ADDRESS_EMPTY);
		alert.show().parent().find('textarea').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().parent().find('textarea').css('border','').css('border-radius','');
	}
}
function showFinPassportEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directoruploadPassport'+ formNo +'-alert');
		alertMsg = $('#directoruploadPassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderuploadPassport'+ formNo +'-alert');
		alertMsg = $('#shareholderuploadPassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#diuploadPassport'+ formNo +'-alert');
		alertMsg = $('#diuploadPassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporateuploadPassport'+ formNo +'-alert');
		alertMsg = $('#corporateuploadPassport'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PASSPORT_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

function showFinPassportDataEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directoruploadPassport'+ formNo +'-alert');
		alertMsg = $('#directoruploadPassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderuploadPassport'+ formNo +'-alert');
		alertMsg = $('#shareholderuploadPassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#diuploadPassport'+ formNo +'-alert');
		alertMsg = $('#diuploadPassport'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporateuploadPassport'+ formNo +'-alert');
		alertMsg = $('#corporateuploadPassport'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text("Please verify your passport details");
		alert.show().prev().find('.custom-file');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

function showFINCardEmpty(role, formNo, isFront, show){
	var alert;
	var alertMsg;
	if(role == ROLE_DIRECTOR){
		if(isFront == true){
			alert = $('#directorfincardfront'+ formNo +'-alert');
			alertMsg = $('#directorfincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#directorfincardback'+ formNo +'-alert');
			alertMsg = $('#directorfincardback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_SHAREHOLDER){
		if(isFront == true){
			alert = $('#shareholderfincardfront'+ formNo +'-alert');
			alertMsg = $('#shareholderfincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#shareholderfincardback'+ formNo +'-alert');
			alertMsg = $('#shareholderfincardback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		if(isFront == true){
			alert = $('#difincardfront'+ formNo +'-alert');
			alertMsg = $('#difincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#difincardback'+ formNo +'-alert');
			alertMsg = $('#difincardback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		if(isFront == true){
			alert = $('#corporatefincardfront'+ formNo +'-alert');
			alertMsg = $('#corporatefincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#corporatefincardback'+ formNo +'-alert');
			alertMsg = $('#corporatefincardback'+ formNo +'-alert-msg');
		}
	}
	
	if (show == true) {
		if(isFront == true){
			alertMsg.text(ERR_MSG_NRIC_FRONT_EMPTY);	
		}else{
			alertMsg.text(ERR_MSG_NRIC_BACK_EMPTY);	
		}		
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

function showFINCardDataEmpty(role, formNo, isFront, show){
	var alert;
	var alertMsg;
	if(role == ROLE_DIRECTOR){
		if(isFront == true){
			alert = $('#directorfincardfront'+ formNo +'-alert');
			alertMsg = $('#directorfincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#directorfincardback'+ formNo +'-alert');
			alertMsg = $('#directorfincardback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_SHAREHOLDER){
		if(isFront == true){
			alert = $('#shareholderfincardfront'+ formNo +'-alert');
			alertMsg = $('#shareholderfincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#shareholderfincardback'+ formNo +'-alert');
			alertMsg = $('#shareholderfincardback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		if(isFront == true){
			alert = $('#difincardfront'+ formNo +'-alert');
			alertMsg = $('#difincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#difincardback'+ formNo +'-alert');
			alertMsg = $('#difincardback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		if(isFront == true){
			alert = $('#corporatefincardfront'+ formNo +'-alert');
			alertMsg = $('#corporatefincardfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#corporatefincardback'+ formNo +'-alert');
			alertMsg = $('#corporatefincardback'+ formNo +'-alert-msg');
		}
	}
	
	if (show == true) {
		if(isFront == true){
			alertMsg.text("Please verify your FIN card front details");	
		}else{
			alertMsg.text("Please verify your FIN card back details");	
		}		
		alert.show().prev().find('.custom-file');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

function showFinAddressProofEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directoraddress'+ formNo +'-alert');
		alertMsg = $('#directoraddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderaddress'+ formNo +'-alert');
		alertMsg = $('#shareholderaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#diaddress'+ formNo +'-alert');
		alertMsg = $('#diaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporateaddress'+ formNo +'-alert');
		alertMsg = $('#corporateaddress'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PROOFADDRESS_EMPTY);
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showFinAddressEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#directorresaddress'+ formNo +'-alert');
		alertMsg = $('#directorresaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderresaddress'+ formNo +'-alert');
		alertMsg = $('#shareholderresaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#diresaddress'+ formNo +'-alert');
		alertMsg = $('#diresaddress'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporateresaddress'+ formNo +'-alert');
		alertMsg = $('#corporateresaddress'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_ADDRESS_EMPTY);
                alert.show().parent().find('textarea').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().parent().find('textarea').css('border','').css('border-radius','');
	}
}
function showNRICEmpty(role, formNo, isFront, show){
	console.log("showNRICEmpty: "+role+", "+formNo+", "+isFront+", "+show);
	
	var alert;
	var alertMsg;
	if(role == ROLE_DIRECTOR){
		if(isFront == true){
			alert = $('#directornricfront'+ formNo +'-alert');
			alertMsg = $('#directornricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#directornricback'+ formNo +'-alert');
			alertMsg = $('#directornricback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_SHAREHOLDER){
		if(isFront == true){
			console.log('#shareholdernricfront'+ formNo +'-alert');
			console.log('#shareholdernricfront'+ formNo +'-alert-msg');
			alert = $('#shareholdernricfront'+ formNo +'-alert');
			alertMsg = $('#shareholdernricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#shareholdernricback'+ formNo +'-alert');
			alertMsg = $('#shareholdernricback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		if(isFront == true){
			console.log('#dinricfront'+ formNo +'-alert');
			console.log('#dinricfront'+ formNo +'-alert-msg');
			alert = $('#dinricfront'+ formNo +'-alert');
			alertMsg = $('#dinricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#dinricback'+ formNo +'-alert');
			alertMsg = $('#dinricback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		if(isFront == true){
			alert = $('#corporatenricfront'+ formNo +'-alert');
			alertMsg = $('#corporatenricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#corporatenricback'+ formNo +'-alert');
			alertMsg = $('#corporatenricback'+ formNo +'-alert-msg');
		}
	}
	
	if (show == true) {
		if(isFront == true){
			console.log("showing....");
			alertMsg.text(ERR_MSG_NRIC_FRONT_EMPTY);	
		}else{
			alertMsg.text(ERR_MSG_NRIC_BACK_EMPTY);	
		}		
		alert.show().prev().find('.custom-file').css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}

function showNRICDataEmpty(role, formNo, isFront, show){
	console.log("showNRICEmpty: "+role+", "+formNo+", "+isFront+", "+show);
	
	var alert;
	var alertMsg;
	if(role == ROLE_DIRECTOR){
		if(isFront == true){
			alert = $('#directornricfront'+ formNo +'-alert');
			alertMsg = $('#directornricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#directornricback'+ formNo +'-alert');
			alertMsg = $('#directornricback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_SHAREHOLDER){
		if(isFront == true){
			console.log('#shareholdernricfront'+ formNo +'-alert');
			console.log('#shareholdernricfront'+ formNo +'-alert-msg');
			alert = $('#shareholdernricfront'+ formNo +'-alert');
			alertMsg = $('#shareholdernricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#shareholdernricback'+ formNo +'-alert');
			alertMsg = $('#shareholdernricback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		if(isFront == true){
			console.log('#dinricfront'+ formNo +'-alert');
			console.log('#dinricfront'+ formNo +'-alert-msg');
			alert = $('#dinricfront'+ formNo +'-alert');
			alertMsg = $('#dinricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#dinricback'+ formNo +'-alert');
			alertMsg = $('#dinricback'+ formNo +'-alert-msg');
		}
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		if(isFront == true){
			alert = $('#corporatenricfront'+ formNo +'-alert');
			alertMsg = $('#corporatenricfront'+ formNo +'-alert-msg');
		}else{
			alert = $('#corporatenricback'+ formNo +'-alert');
			alertMsg = $('#corporatenricback'+ formNo +'-alert-msg');
		}
	}
	
	if (show == true) {
		if(isFront == true){
			console.log("showing....");
			alertMsg.text("Please verify your nric front details");	
		}else{
			alertMsg.text("Please verify your nric back details");	
		}		
		alert.show().prev().find('.custom-file');
	} else {
		alert.hide().prev().find('.custom-file').css('border','').css('border-radius','');
	}
}
function showContactNumbersEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#contactNumber'+ formNo +'-alert');
		alertMsg = $('#contactNumber'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdercontactNumber'+ formNo +'-alert');
		alertMsg = $('#shareholdercontactNumber'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholdercontactNumber'+ formNo +'-alert');
		alertMsg = $('#dishareholdercontactNumber'+ formNo +'-alert-msg');
	}
        

	if (show == true) {
		alertMsg.text(ERR_MSG_HANDPHONE_OFFICEPHONE_EMPTY);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function showContactNumbersInvalid(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#contactNumber'+ formNo +'-alert');
		alertMsg = $('#contactNumber'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdercontactNumber'+ formNo +'-alert');
		alertMsg = $('#shareholdercontactNumber'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholdercontactNumber'+ formNo +'-alert');
		alertMsg = $('#dishareholdercontactNumber'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatecontactNumber'+ formNo +'-alert');
		alertMsg = $('#corporatecontactNumber'+ formNo +'-alert-msg');
	}

	if (show == true) {
		alertMsg.text(ERR_MSG_PHONE_NAN);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function showOfficeNumberInvalid(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#officeNumber'+ formNo +'-alert');
		alertMsg = $('#officeNumber'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderofficeNumber'+ formNo +'-alert');
		alertMsg = $('#shareholderofficeNumber'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderofficeNumber'+ formNo +'-alert');
		alertMsg = $('#dishareholderofficeNumber'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PHONE_NAN);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}

function showBizEmailEmpty(role, formNo, show){
	//console.log("role: "+role+", formNo: "+formNo+", show: "+show);
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#bizEmail'+ formNo +'-alert');
		alertMsg = $('#bizEmail'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderbizEmail'+ formNo +'-alert');
		alertMsg = $('#shareholderbizEmail'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderbizEmail'+ formNo +'-alert');
		alertMsg = $('#dishareholderbizEmail'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_BUSINESS_PERSONAL_EMAIL_EMPTY);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function showBizEmailInvalidFormat(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#bizEmail'+ formNo +'-alert');
		alertMsg = $('#bizEmail'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderbizEmail'+ formNo +'-alert');
		alertMsg = $('#shareholderbizEmail'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderbizEmail'+ formNo +'-alert');
		alertMsg = $('#dishareholderbizEmail'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatebizEmail'+ formNo +'-alert');
		alertMsg = $('#corporatebizEmail'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_EMAIL_FORMAT);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function showPersonalEmailInvalidFormat(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#personalEmail'+ formNo +'-alert');
		alertMsg = $('#personalEmail'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderpersonalEmail'+ formNo +'-alert');
		alertMsg = $('#shareholderpersonalEmail'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderpersonalEmail'+ formNo +'-alert');
		alertMsg = $('#dishareholderpersonalEmail'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_EMAIL_FORMAT);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function showCountryEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_CORPORATE_SHAREHOLDER){
		alert = $('#corporatecountry'+ formNo +'-alert');
		alertMsg = $('#corporatecountry'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_COUNTRY_INCORPORATION_EMPTY);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}
function showPepCountryEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#countrypep'+ formNo +'-alert');
		alertMsg = $('#countrypep'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholdercountrypep'+ formNo +'-alert');
		alertMsg = $('#shareholdercountrypep'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){

		alert = $('#dishareholdercountrypep'+ formNo +'-alert');
		alertMsg = $('#dishareholdercountrypep'+ formNo +'-alert-msg');
		console.log('#dishareholdercountrypep'+ formNo +'-alert');
	}

	if (show == true) {
		console.log("showing...")
		alertMsg.text(ERR_MSG_PEP_COUNTRY_EMPTY);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function showPepRoleEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#rolePep'+ formNo +'-alert');
		alertMsg = $('#rolePep'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderrolePep'+ formNo +'-alert');
		alertMsg = $('#shareholderrolePep'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderrolePep'+ formNo +'-alert');
		alertMsg = $('#dishareholderrolePep'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_ROLE_EMPTY);
		alert.show().prev().css('border-color','red');
	} else {
		alert.hide().prev().css('border-color','');
	}
}
function showPepFromEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#pepFrom'+ formNo +'-alert');
		alertMsg = $('#pepFrom'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderpepFrom'+ formNo +'-alert');
		alertMsg = $('#shareholderpepFrom'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderpepFrom'+ formNo +'-alert');
		alertMsg = $('#dishareholderpepFrom'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_FROM_EMPTY);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}
function checkPepFromToRange(role, pepFrom, pepTo, formNo){
	var from = parseInt(pepFrom);
	var to = parseInt(pepTo);
	if(from >= to){
		showPepFromToRange(role, formNo, true);
		return false;
	}else{
		return true;
	}
}
function showPepFromToRange(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#pepFrom'+ formNo +'-alert');
		alertMsg = $('#pepFrom'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderpepFrom'+ formNo +'-alert');
		alertMsg = $('#shareholderpepFrom'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderpepFrom'+ formNo +'-alert');
		alertMsg = $('#dishareholderpepFrom'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_FROM_TO_WRONG_RANGE);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}
function showPepToEmpty(role, formNo, show){
	var alert;
	var alertMsg;

	if(role == ROLE_DIRECTOR){
		alert = $('#pepTo'+ formNo +'-alert');
		alertMsg = $('#pepTo'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		alert = $('#shareholderpepTo'+ formNo +'-alert');
		alertMsg = $('#shareholderpepTo'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		alert = $('#dishareholderpepTo'+ formNo +'-alert');
		alertMsg = $('#dishareholderpepTo'+ formNo +'-alert-msg');
	}
	if (show == true) {
		alertMsg.text(ERR_MSG_PEP_TO_EMPTY);
		alert.show().prev().css('border','1px solid red').css('border-radius','0.25rem');
	} else {
		alert.hide().prev().css('border','').css('border-radius','');
	}
}

function validateForm(){
	isValidated = true;	
	var proposedCompanyName = [];
	var intendedAddress = "";
	var serviceAddress;
	var postalCode;
	var streetName;
	var floor;
	var unit;
	var businessActivities = "";
	var roleSelector = "";
	var addRole;
	var countriesOperation;
	var sourceFunds;

	intendedAddress = $("input[name=intendedAddress]:checked").val();
	//console.log("intendedAddress "+intendedAddress);

	if(intendedAddress == "ownAddress"){
		postalCode = $("#postal-code").val();
		//console.log("postalCode "+postalCode);
		streetName = $("#street-name").val();
		//console.log("streetName "+streetName);
		floor = $("#floor").val();
		//console.log("floor "+floor);
		unit = $("#unit").val();
		//console.log("unit "+unit);		
	} else {
		serviceAddress = $("#serviceaddress").val();
		//console.log("serviceAddress "+serviceAddress);		
	}
	

	businessActivities = $("#businessActivities").val();
	//console.log("businessActivities "+businessActivities);

	roleSelector = $("#roleSelector").val();
	//console.log("roleSelector "+roleSelector);

	
	

	var proposedCompanyNames = document.querySelectorAll("input[name='proposedCompanyName']");
	for (i = 0; i < proposedCompanyNames.length; i++) {
    	if(proposedCompanyNames[i].value == ""){
    		showProposedCompanyEmpty(true);
    		isValidated = false;
    	}else{
    		localStorage.setItem("proposedCompanyName"+(i+1), proposedCompanyNames[i].value);
    	}
	}	

	countriesOperation = $("#countriesOperation").val();
	//console.log("countriesOperation: "+countriesOperation);
	sourceFunds = $("#sourceFunds").val();
	//console.log("sourceFunds: "+sourceFunds);

	/*if(intendedAddress == undefined){
		showProposedCompanyAddressEmpty(true);
		isValidated = false;
	}
	else*/ 
	if(intendedAddress == "ownAddress"){
		if(postalCode != undefined && postalCode != "" && isNaN(postalCode)){
			showOwnAddressInvalid(ADDRESS_POSTAL_CODE, true);	
			isValidated = false;
		}else if(floor != undefined && floor != "" && isNaN(floor)){
			showOwnAddressInvalid(ADDRESS_FLOOR, true);
			isValidated = false;
		}
	} 		
	/*if(intendedAddress == "ownAddress"){
		if(postalCode != undefined && postalCode != ""){
			showOwnAddressEmpty(ADDRESS_POSTAL_CODE, true);	
			isValidated = false;
		} 
		else if(streetName == undefined || streetName == ""){
			showOwnAddressEmpty(ADDRESS_STREET_NAME, true);
			isValidated = false;
		}else if(floor != undefined || floor != ""){
			showOwnAddressEmpty(ADDRESS_FLOOR, true);
			isValidated = false;
		}else if(unit == undefined || unit == ""){
			showOwnAddressEmpty(ADDRESS_UNIT, true);
			isValidated = false;
		}
	}*/ 
	/*if(businessActivities == undefined || businessActivities == ""){
		showBussinessActivitiesEmpty(true);
		isValidated = false;
	} if(countriesOperation == undefined || countriesOperation == ""){
		showCountriesOperationEmpty(true);
		isValidated = false;	
	}*/ 
	if(roleSelector == undefined || roleSelector == ""){
		showRoleSelectorEmpty(true);
		isValidated = false;	
	}  
	if(isValidated)
	{

			for(c=1;c<=counter;c++){
				console.log("counter");
				try{
					if(validateDirectorRoleForm(c) == false){
						isValidated = false;		
					}
				}catch(e){}
			}
			for(c=1;c<=counter1;c++){
				console.log("counter1");
				try{		
					if(validateShareholderRoleForm(c) == false){
						isValidated = false;		
					}
				}catch(e){}
			}
			for(c=1;c<=counter2;c++){
				console.log("counter2");
				try{
					if(validateDirectorShareholderRoleForm(c) == false){
						isValidated = false;		
					}
				}catch(e){}
			}
			for(c=1;c<=counter3;c++){
				console.log("counter3");
				try{
					if(validateCorporateShareholderRoleForm(c) == false){
						isValidated = false;		
					}
				}catch(e){}
			}				
	}
	
	localStorage.setItem("intendedAddress", intendedAddress);	
	localStorage.setItem("serviceAddress", serviceAddress);
	localStorage.setItem("postal-code", postalCode);
	localStorage.setItem("street-name", streetName);
	localStorage.setItem("floor", floor);
	localStorage.setItem("unit", unit);
	localStorage.setItem("businessActivities", businessActivities);
	localStorage.setItem("countriesOperation", countriesOperation);
	localStorage.setItem("sourceFunds", sourceFunds);
	localStorage.setItem("roleSelector", roleSelector);
	localStorage.setItem(ROLE_DIRECTOR + "_COUNT", counter);
	localStorage.setItem(ROLE_SHAREHOLDER + "_COUNT", counter1);
	localStorage.setItem(ROLE_DIRECTOR_SHAREHOLDER + "_COUNT", counter2);
	localStorage.setItem(ROLE_CORPORATE_SHAREHOLDER + "_COUNT", counter3);

	return isValidated;
}
function validateDirectorRoleForm(formNo){
	console.log(ROLE_DIRECTOR + formNo);
	startEventsDirector(formNo);
	var surname;
	var directorresstatus;
	var address;
	var handphone;
	var officephone;
	var businessemail;
	var personalemail;
	var directorpep;
	var passport;
	var addressProof;

	var finPassport;
	var finCardFront;
	var finCardBack;
	var finAddressProof;
	var finAddress;

	var nricfront;
	var nricback;
	var pepCountry;
	var pepRole;
	var pepFrom;
	var pepTo;
	
	var role_val="director";
	
	surname = $("#surname_"+formNo).val();
	//console.log("surname "+surname);
	directorresstatus = $("#directorresstatus_"+formNo).val();
	//console.log("directorresstatus "+directorresstatus);
	passport = $("#directornonresidentuploadpassport_"+formNo).val();
	//console.log("passport "+passport);
	addressProof = $("#directornonresidentaddress_"+formNo).val();
	//console.log("addressProof "+addressProof);
	address = $("#directornonresidentresaddress_"+formNo).val();
	//console.log("directornonresidentresaddress "+address);	

	/* finCard */
	finPassport = $("#directoruploadPassport_"+formNo).val();
	//console.log("finPassport "+finPassport);
	finCardFront = $("#directorfincardfront_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finCardBack = $("#directorfincardback_"+formNo).val();
	//console.log("finCardBack "+finCardBack);
	finAddressProof = $("#directoraddress_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finAddress = $("#directorresaddress_"+formNo).val();
	//console.log("finAddress "+finAddress);	
	/*  */

	nricfront = $("#directornricfront_"+formNo).val();
	//console.log("nricfront "+nricfront);
	nricback = $("#directornricback_"+formNo).val();
	//console.log("nricback "+nricback);		
	handphone = $("#directorhandphone_"+formNo).val();
	//console.log("handphone "+handphone);
	officephone = $("#offnumber_"+formNo).val();
	//console.log("officephone "+officephone);
	businessemail = $("#directorbusinessemail_"+formNo).val();
	//console.log("businessemail "+businessemail);
	personalemail = $("#personalemail_"+formNo).val();
	//console.log("personalemail "+personalemail);

	directorpep = $("input[name=directorpep_"+formNo+"]:checked").val();
	//console.log("directorpep "+directorpep);
	addRole = $("#addroleSelector").val();
	//console.log("addRole "+addRole);
	pepCountry = $("#countrypep_"+formNo).val();
	console.log("pepCountry "+pepCountry);
	pepRole = $("#rolePep_"+formNo).val();
	console.log("pepRole "+pepRole);
	pepFrom = $("#pepFrom_"+formNo).val();
	console.log("pepFrom "+pepFrom);
	pepTo = $("#pepTo_"+formNo).val();
	console.log("pepTo "+pepTo);
	
	
	
	
	if(surname == undefined || surname == ""){
		showFullNameEmpty(ROLE_DIRECTOR, formNo, true);		
		isValidated = false;
	}
	if(directorresstatus == undefined || directorresstatus == ""){
		showResStatusEmpty(ROLE_DIRECTOR, formNo, true);	
		isValidated = false;
	}

	if(directorresstatus == "directornonresident_"+formNo){
		console.log("non residential passport......");
		
		if(passport !==undefined && passport !=="")
		{
			console.log("Validating passport");
			if(!displaypreviewpopup(role_val, formNo, role_val+"nonresident",true))
			{
				console.log("Validating passport ocr");
				showPassportDataEmpty(ROLE_DIRECTOR, formNo, true);	
				isValidated = false;
			}
			
		}else if(!displaymanualpopup(role_val, formNo, role_val+"passportmanually",true))
		{
			console.log("Validating passport manual");
			showPassportDataManualEmpty(ROLE_DIRECTOR, formNo, true);	
			isValidated = false;
			
		}
		
		if(address == undefined || address == ""){
			showAddressEmpty(ROLE_DIRECTOR, formNo, true);	
			isValidated = false;
		} 
		if(addressProof == undefined || addressProof == ""){
			showAddressProofEmpty(ROLE_DIRECTOR, formNo, true);	
			isValidated = false;
		}
	}  if(directorresstatus == "directorcitizenpr_"+formNo){
		console.log("Validating nric");
		console.log(nricfront);
		console.log(nricback);
		if((nricfront !==undefined && nricfront !=="" )&& (nricback !== undefined && nricback !== ""))
		{
			console.log("Validating nric auto");
			if(!displaypreviewpopup(role_val, formNo, role_val+"nricfront",true) )
			{
				showNRICDataEmpty(ROLE_DIRECTOR, formNo,true, true);	
					
				isValidated = false;
			}
			
			if(!displaypreviewpopup(role_val, formNo, role_val+"nricback",true) )
			{
				showNRICDataEmpty(ROLE_DIRECTOR, formNo,false, true);
				isValidated = false;
				
			}
			
		}else if(!displaymanualpopup(role_val, formNo,role_val+ "nricmanually",true))
		{
			
			console.log("Validating nric manual");
			showNRICDataManualEmpty(ROLE_DIRECTOR, formNo, true);	
				
			isValidated = false;
			
		}
		
		
		

	} if(directorresstatus == "directorpassholder_"+formNo){

		console.log("Validating fin");
		
		if((finCardFront !==undefined && finCardFront !=="" )&& (finCardBack !== undefined && finCardBack !== "") && (finPassport !== undefined && finPassport !== ""))
		{
			console.log("Validating auto");
			if(!displaypreviewpopup(role_val, formNo, role_val+"finpassport",true)  )
			{
				
				showFinPassportDataEmpty(ROLE_DIRECTOR, formNo, true);				
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo,role_val+ "fincardfront",true))
			{
				
				
				showFINCardDataEmpty(ROLE_DIRECTOR, formNo, true,true);
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo, role_val+"fincardback",true))
			{
				showFINCardDataEmpty(ROLE_DIRECTOR, formNo, false,true);
				isValidated = false;
				
			}
			
			
			
		}else if(!displaymanualpopup(role_val, formNo, role_val+"fincardmanually",true))
		{
			console.log("Validating manual");
			showFINDataManualEmpty(ROLE_DIRECTOR, formNo,true);
						
			isValidated = false;
			
		}
		
		
		
		if(finAddressProof == undefined || finAddressProof == ""){
			showFinAddressProofEmpty(ROLE_DIRECTOR, formNo, true);	
			isValidated = false;
		} if(finAddress == undefined || finAddress == ""){
			showFinAddressEmpty(ROLE_DIRECTOR, formNo, true);	
			isValidated = false;
		}
	}

	/*if(handphone == undefined || handphone == "" ){
		showContactNumbersEmpty(ROLE_DIRECTOR, formNo, true);
		isValidated = false;
	}
	if(isNaN(handphone) || handphone.trim().length < 8){
		console.log("handphone: "+handphone+", len: "+handphone.trim().length);
		showContactNumbersInvalid(ROLE_DIRECTOR, formNo, true);
		isValidated = false;
	} if(businessemail == undefined || businessemail == ""){
		showBizEmailEmpty(ROLE_DIRECTOR, formNo, true);
		isValidated = false;
	}else if(ValidateEmail(businessemail) == false){
		showBizEmailInvalidFormat(ROLE_DIRECTOR, formNo, true);
		isValidated = false;
	} */

	if((handphone == undefined || handphone == "") && (officephone == undefined || officephone == "")){
		if(handphone == undefined || handphone == "" ){
			showContactNumbersEmpty(ROLE_DIRECTOR, formNo, true);
			isValidated = false;
		}  
	}else{
		if(!(handphone == undefined || handphone == "")){
			if(isNaN(handphone)|| handphone.trim().length < 8){
				showContactNumbersInvalid(ROLE_DIRECTOR, formNo, true);
				isValidated = false;
			}	
		} 
		if(!(officephone == undefined || officephone == "")){
			if(isNaN(officephone)|| officephone.trim().length < 8){
				showOfficeNumberInvalid(ROLE_DIRECTOR, formNo, true);
				isValidated = false;
			}	
		}
	}
	if((businessemail == undefined || businessemail == "") && (personalemail == undefined || personalemail == "")){
		if(businessemail == undefined || businessemail == ""){
			showBizEmailEmpty(ROLE_DIRECTOR, formNo, true);
			isValidated = false;
		} 
	}else{
		if(!(businessemail == undefined || businessemail == "")){
			if(ValidateEmail(businessemail) == false){
				showBizEmailInvalidFormat(ROLE_DIRECTOR, formNo, true);
				isValidated = false;
			}
		}
		if(!(personalemail == undefined || personalemail == "")){
			if(ValidateEmail(personalemail) == false){
				showPersonalEmailInvalidFormat(ROLE_DIRECTOR, formNo, true);
				isValidated = false;
			}	
		}
	}

	if(directorpep == "directorpepyes_"+formNo){
		if(pepCountry == undefined || pepCountry == ""){
			showPepCountryEmpty(ROLE_DIRECTOR, formNo, true);
			isValidated = false;
		} if(pepRole == undefined || pepRole == ""){
			showPepRoleEmpty(ROLE_DIRECTOR, formNo, true);
			isValidated = false;
		} if(pepFrom == undefined || pepFrom == ""){
			showPepFromEmpty(ROLE_DIRECTOR, formNo, true);
			isValidated = false;
		}
		if(pepTo == undefined || pepTo == ""){
			showPepToEmpty(ROLE_DIRECTOR, formNo, true);
			isValidated = false;
		}
		if((pepFrom != undefined && pepFrom != "") && (pepTo != undefined && pepTo != "")){
			if(checkPepFromToRange(ROLE_DIRECTOR, pepFrom, pepTo, formNo) != true){
				isValidated = false;
			}
		} 
	}
	{
		localStorage.setItem("directorSurname_"+formNo, surname);
		localStorage.setItem("directorresstatus_"+formNo, directorresstatus);

		if(hasManualEntry(MANUAL_DIRECTOR_NONRESIDENT_PASSPORT) == "true"){

			localStorage.setItem("directornonresidentuploadpassport_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("directornonresidentuploadpassport_"+formNo, passport);
		}

		localStorage.setItem("directornonresidentaddress_"+formNo, addressProof);
		localStorage.setItem("directornonresidentresaddress_"+formNo, address);

		if(hasManualEntry(MANUAL_DIRECTOR_FIN_PASSPORT) == "true"){
			localStorage.setItem("directorFINuploadPassport_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("directorFINuploadPassport_"+formNo, finPassport);	
		}
		
		localStorage.setItem("directorfincardfront_"+formNo, finCardFront);
		localStorage.setItem("directorfincardback_"+formNo, finCardBack);
		localStorage.setItem("directorFINaddress_"+formNo, finAddressProof);
		localStorage.setItem("directorFINresaddress_"+formNo, finAddress);

		if(hasManualEntry(MANUAL_DIRECTOR_CITIZENPR_NRIC) == "true"){
			localStorage.setItem("directorSGnricfront_"+formNo, DEFAULT_IMAGE_PATH);
			localStorage.setItem("directorSGnricback_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("directorSGnricfront_"+formNo, nricfront);
			localStorage.setItem("directorSGnricback_"+formNo, nricback);	
		}		

		localStorage.setItem("directorhandphone_"+formNo, handphone);
		localStorage.setItem("directorOfficePhone_"+formNo, officephone);
		localStorage.setItem("directorbusinessemail_"+formNo, businessemail);
		localStorage.setItem("directorpersonalemail_"+formNo, personalemail);
		localStorage.setItem("directorpep_"+formNo, directorpep);
		localStorage.setItem("directorCountrypep_"+formNo, pepCountry);
		localStorage.setItem("directorRolePep_"+formNo, pepRole);
		localStorage.setItem("directorPepFrom_"+formNo, pepFrom);
		localStorage.setItem("directorPepTo_"+formNo, pepTo);		
	}

	return isValidated;
}
function validateShareholderRoleForm(formNo){
	console.log(ROLE_SHAREHOLDER + formNo);
	startEventsShareHolder(formNo);
	var surname;
	var areYouBenificalOwner;
	var resStatus;
	var address;
	var handphone;
	var officephone;
	var businessemail;
	var personalemail;
	var pep;
	var passport;
	var addressProof;

	var finPassport;
	var finCardFront;
	var finCardBack;
	var finAddressProof;
	var finAddress;

	var nricfront;
	var nricback;
	var pepCountry;
	var pepRole;
	var pepFrom;
	var pepTo;

	var role_val="shareholder";
	surname = $("#shareholdersurname_"+formNo).val();
	//console.log("surname "+surname);
	areYouBenificalOwner = $("input[name=shareholderbowner_"+formNo+"]:checked").val();
	//console.log("areYouBenificalOwner "+areYouBenificalOwner);	
	resStatus = $("#shareholderresstatus_"+formNo).val();
	//console.log("resStatus "+resStatus);
	passport = $("#shareholdernonresidentuploadpassport_"+formNo).val();
	//console.log("passport "+passport);
	addressProof = $("#shareholdernonresidentaddress_"+formNo).val();
	//console.log("addressProof "+addressProof);
	address = $("#shareholdernonresidentresaddress_"+formNo).val();
	//console.log("address "+address);	

	/* finCard */
	finPassport = $("#shareholderuploadPassport_"+formNo).val();	
	//console.log("finPassport "+finPassport);
	finCardFront = $("#shareholderfincardfront_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finCardBack = $("#shareholderfincardback_"+formNo).val();
	//console.log("finCardBack "+finCardBack);
	finAddressProof = $("#shareholderaddress_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finAddress = $("#shareholderresaddress_"+formNo).val();
	//console.log("finAddress "+finAddress);	
	/*  */

	nricfront = $("#shareholdernricfront_"+formNo).val();
	//console.log("nricfront "+nricfront);
	nricback = $("#shareholdernricback_"+formNo).val();
	//console.log("nricback "+nricback);		
	handphone = $("#shareholderhandphone_"+formNo).val();
	//console.log("handphone "+handphone);
	officephone = $("#shareholderoffnumber_"+formNo).val();
	//console.log("officephone "+officephone);
	businessemail = $("#shareholderbusinessemail_"+formNo).val();
	//console.log("businessemail "+businessemail);
	personalemail = $("#shareholderpersonalemail_"+formNo).val();
	//console.log("personalemail "+personalemail);
	pep = $("input[name=shareholderpep_"+formNo+"]:checked").val();
	//console.log("pep "+pep);
	pepCountry = $("#shareholdercountrypep_"+formNo).val();
	//console.log("pepCountry "+pepCountry);
	pepRole = $("#shareholderrolePep_"+formNo).val();
	//console.log("pepRole "+pepRole);
	pepFrom = $("#shareholderpepFrom_"+formNo).val();
	//console.log("pepFrom "+pepFrom);
	pepTo = $("#shareholderpepTo_"+formNo).val();
	//console.log("pepTo "+pepTo);
	console.log("shareholder modal");
	
	if(surname == undefined || surname == ""){
		showFullNameEmpty(ROLE_SHAREHOLDER, formNo, true);		
		isValidated = false;
	} if(resStatus == undefined || resStatus == ""){
		showResStatusEmpty(ROLE_SHAREHOLDER, formNo, true);	
		isValidated = false;
	} if(resStatus == "shareholdernonresident_"+formNo){
			
			console.log("shareholder modal pass");
		if(passport !==undefined && passport !=="")
		{
			console.log("shareholder modal pass auto");
			if(!displaypreviewpopup("shareholder", formNo, "shareholdernonresident",true))
			{
				showPassportDataEmpty(ROLE_SHAREHOLDER, formNo, true);	
				isValidated = false;
			}
			
		}else if(!displaymanualpopup("shareholder", formNo, "shareholderpassportmanually",true))
		{
			console.log("shareholder modal pass manual");
			showPassportDataManualEmpty(ROLE_SHAREHOLDER, formNo, true);	
			isValidated = false;
			
		}
		
		
		if(address == undefined || address == ""){
			showAddressEmpty(ROLE_SHAREHOLDER, formNo, true);	
			isValidated = false;
		} if(addressProof == undefined || addressProof == ""){
			//console.log("addressProof empty...");
			showAddressProofEmpty(ROLE_SHAREHOLDER, formNo, true);	
			isValidated = false;
		}
		
	} else if(resStatus == "shareholdercitizenpr_"+formNo){
		if((nricfront !==undefined && nricfront !=="" )&& (nricback !== undefined && nricback !== ""))
		{
			if(!displaypreviewpopup("shareholder", formNo, "shareholdernricfront",true) )
			{
				showNRICDataEmpty(ROLE_SHAREHOLDER, formNo, true,true);
				
				isValidated = false;
			}
			if(!displaypreviewpopup("shareholder", formNo, "shareholdernricback",true) ){
				showNRICDataEmpty(ROLE_SHAREHOLDER, formNo,false, true);
				isValidated = false;
			}
			
		}else if(!displaymanualpopup("shareholder", formNo, "shareholdernricmanually",true))
		{
			showNRICDataManualEmpty(ROLE_SHAREHOLDER, formNo, true);
				
			isValidated = false;
			
		}
	}else if(resStatus == "shareholderpassholder_"+formNo){
		
		if((finCardFront !==undefined && finCardFront !=="" )&& (finCardBack !== undefined && finCardBack !== "") && (finPassport !== undefined && finPassport !== ""))
		{
			if(!displaypreviewpopup(role_val, formNo, role_val+"finpassport",true)  )
			{
				
				showFinPassportDataEmpty(ROLE_SHAREHOLDER, formNo, true);				
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo,role_val+ "fincardfront",true))
			{
				
				
				showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo, true,true);
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo, role_val+"fincardback",true))
			{
				showFINCardDataEmpty(ROLE_SHAREHOLDER, formNo, false,true);
				isValidated = false;
				
			}
			
		}else if(!displaymanualpopup("shareholder", formNo, "shareholderfincardmanually",true))
		{
			showFINDataManualEmpty(ROLE_SHAREHOLDER, formNo,true);
					
			isValidated = false;
			
		}
		
		
		if(finAddressProof == undefined || finAddressProof == ""){
			showFinAddressProofEmpty(ROLE_SHAREHOLDER, formNo, true, true);	
			isValidated = false;
		} if(finAddress == undefined || finAddress == ""){
			showFinAddressEmpty(ROLE_SHAREHOLDER, formNo, true, true);	
			isValidated = false;
		}
	}

	/*if(handphone == undefined || handphone == "" ){
		showContactNumbersEmpty(ROLE_SHAREHOLDER, formNo, true);
		isValidated = false;
	} if(isNaN(handphone)|| handphone.trim().length < 8){
		showContactNumbersInvalid(ROLE_SHAREHOLDER, formNo, true);
		isValidated = false;
	} if(businessemail == undefined || businessemail == ""){
		showBizEmailEmpty(ROLE_SHAREHOLDER, formNo, true);
		isValidated = false;
	}else if(ValidateEmail(businessemail) == false){
		showBizEmailInvalidFormat(ROLE_SHAREHOLDER, formNo, true);
		isValidated = false;
	}*/ 

	if((handphone == undefined || handphone == "") && (officephone == undefined || officephone == "")){
		if(handphone == undefined || handphone == "" ){
			showContactNumbersEmpty(ROLE_SHAREHOLDER, formNo, true);
			isValidated = false;
		}  
	}else{
		if(!(handphone == undefined || handphone == "")){
			if(isNaN(handphone)|| handphone.trim().length < 8){
				showContactNumbersInvalid(ROLE_SHAREHOLDER, formNo, true);
				isValidated = false;
			}	
		}
		if(!(officephone == undefined || officephone == "")){
			if(isNaN(officephone)|| officephone.trim().length < 8){
				showOfficeNumberInvalid(ROLE_SHAREHOLDER, formNo, true);
				isValidated = false;
			}	
		}
	}
	if((businessemail == undefined || businessemail == "") && (personalemail == undefined || personalemail == "")){
		if(businessemail == undefined || businessemail == ""){
			showBizEmailEmpty(ROLE_SHAREHOLDER, formNo, true);
			isValidated = false;
		} 
	}else{
		if(!(businessemail == undefined || businessemail == "")){
			if(ValidateEmail(businessemail) == false){
				showBizEmailInvalidFormat(ROLE_SHAREHOLDER, formNo, true);
				isValidated = false;
			}
		} 
		if(!(personalemail == undefined || personalemail == "")){
			if(ValidateEmail(personalemail) == false){
				showPersonalEmailInvalidFormat(ROLE_SHAREHOLDER, formNo, true);
				isValidated = false;
			}	
		}
	}

	if(pep == "shareholderpepyes_"+formNo){
		if(pepCountry == undefined || pepCountry == ""){
			showPepCountryEmpty(ROLE_SHAREHOLDER, formNo, true);
			isValidated = false;
		} if(pepRole == undefined || pepRole == ""){
			showPepRoleEmpty(ROLE_SHAREHOLDER, formNo, true);
			isValidated = false;
		} if(pepFrom == undefined || pepFrom == ""){
			showPepFromEmpty(ROLE_SHAREHOLDER, formNo, true);
			isValidated = false;
		} if(pepTo == undefined || pepTo == ""){
			showPepToEmpty(ROLE_SHAREHOLDER, formNo, true);
			isValidated = false;
		}
		if((pepFrom != undefined && pepFrom != "") && (pepTo != undefined && pepTo != "")){
			if(checkPepFromToRange(ROLE_SHAREHOLDER, pepFrom, pepTo, formNo) != true){
				isValidated = false;
			}
		}				
	}
	{
		localStorage.setItem("shareholdersurname_"+formNo, surname);
		localStorage.setItem("shareholderbowner_"+formNo, areYouBenificalOwner);
		localStorage.setItem("shareholderresstatus_"+formNo, resStatus);
		if(hasManualEntry(MANUAL_SHAREHOLDER_NONRESIDENT_PASSPORT) == "true"){
			localStorage.setItem("shareholdernonresidentuploadpassport_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("shareholdernonresidentuploadpassport_"+formNo, passport);
		}
		
		localStorage.setItem("shareholdernonresidentaddress_"+formNo, addressProof);
		localStorage.setItem("shareholdernonresidentresaddress_"+formNo, address);
		if(hasManualEntry(MANUAL_SHAREHOLDER_FIN_PASSPORT) == "true"){
			localStorage.setItem("shareholderFINuploadPassport_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("shareholderFINuploadPassport_"+formNo, finPassport);
		}
		
		localStorage.setItem("shareholderfincardfront_"+formNo, finCardFront);
		localStorage.setItem("shareholderfincardback_"+formNo, finCardBack);
		localStorage.setItem("shareholderFINaddress_"+formNo, finAddressProof);
		localStorage.setItem("shareholderFINresaddress_"+formNo, finAddress);

		if(hasManualEntry(MANUAL_SHAREHOLDER_CITIZENPR_NRIC) == "true"){
			localStorage.setItem("shareholderSGnricfront_"+formNo, DEFAULT_IMAGE_PATH);
			localStorage.setItem("shareholderSGnricback_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("shareholderSGnricfront_"+formNo, nricfront);
			localStorage.setItem("shareholderSGnricback_"+formNo, nricback);
		}	
		

		localStorage.setItem("shareholderhandphone_"+formNo, handphone);
		localStorage.setItem("shareholderOfficephone_"+formNo, officephone);
		localStorage.setItem("shareholderbusinessemail_"+formNo, businessemail);
		localStorage.setItem("shareholderPersonalemail_"+formNo, personalemail);
		localStorage.setItem("shareholderpep_"+formNo, pep);
		localStorage.setItem("shareholdercountrypep_"+formNo, pepCountry);
		localStorage.setItem("shareholderrolePep_"+formNo, pepRole);
		localStorage.setItem("shareholderpepFrom_"+formNo, pepFrom);
		localStorage.setItem("shareholderpepTo_"+formNo, pepTo);
	}
	return isValidated;
}
function validateDirectorShareholderRoleForm(formNo){
	console.log(ROLE_DIRECTOR_SHAREHOLDER + formNo);
	startEventsDirectorShareholder(formNo);
	var surname;
	var areYouBenificalOwner;
	var resStatus;
	var address;
	var handphone;
	var officephone;
	var businessemail;
	var personalemail;
	var pep;
	var passport;
	var addressProof;
	
	var finPassport;
	var finCardFront;
	var finCardBack;
	var finAddressProof;
	var finAddress;

	var nricfront;
	var nricback;
	var pepCountry;
	var pepRole;
	var pepFrom;
	var pepTo;

	var role_val="di";
	surname = $("#dishareholdersurname_"+formNo).val();
	//console.log("surname "+surname);
	areYouBenificalOwner = $("input[name=dishareholderbowner_"+formNo+"]:checked").val();
	//console.log("areYouBenificalOwner "+areYouBenificalOwner);	
	resStatus = $("#dishareholderresstatus_"+formNo).val();
	console.log("resStatus "+resStatus);
	passport = $("#dinonresidentuploadpassport_"+formNo).val();
	console.log("passport "+passport);
	addressProof = $("#dinonresidentaddress_"+formNo).val();
	console.log("addressProof "+addressProof);
	address = $("#dinonresidentresaddress_"+formNo).val();
	console.log("address "+address);


	/* finCard */
	finPassport = $("#diuploadPassport_"+formNo).val();	
	//console.log("finPassport "+finPassport);
	finCardFront = $("#difincardfront_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finCardBack = $("#difincardback_"+formNo).val();
	//console.log("finCardBack "+finCardBack);
	finAddressProof = $("#diaddress_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finAddress = $("#diresaddress_"+formNo).val();
	//console.log("finAddress "+finAddress);	
	/*  */

	nricfront = $("#dinricfront_"+formNo).val();
	console.log("nricfront "+nricfront);
	nricback = $("#dinricback_"+formNo).val();
	//console.log("nricback "+nricback);		
	handphone = $("#dishareholderhandphone_"+formNo).val();
	//console.log("handphone "+handphone);
	officephone = $("#dishareholderoffnumber_"+formNo).val();
	//console.log("officephone "+officephone);
	businessemail = $("#dishareholderbusinessemail_"+formNo).val();
	//console.log("businessemail "+businessemail);
	personalemail = $("#dishareholderpersonalemail_"+formNo).val();
	//console.log("personalemail "+personalemail);
	pep = $("input[name=dishareholderpep_"+formNo+"]:checked").val();
	//console.log("shareholderpep "+pep);
	pepCountry = $("#dishareholdercountrypep_"+formNo).val();
	console.log("pepCountry "+pepCountry);
	pepRole = $("#dishareholderrolePep_"+formNo).val();
	//console.log("pepRole "+pepRole);
	pepFrom = $("#dishareholderpepFrom_"+formNo).val();
	//console.log("pepFrom "+pepFrom);
	pepTo = $("#dishareholderpepTo_"+formNo).val();
	//console.log("pepTo "+pepTo);
	
	
	if(surname == undefined || surname == ""){
		showFullNameEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);		
		isValidated = false;
	} if(resStatus == undefined || resStatus == ""){
		showResStatusEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);	
		isValidated = false;
	} if(resStatus == "dinonresident_"+formNo){
		
		
		if(passport !==undefined && passport !=="")
		{
			if(!displaypreviewpopup("di", formNo, "dinonresident",true))
			{
				showPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);	
				isValidated = false;
			}
			
		}else if(!displaymanualpopup("di", formNo, "dipassportmanually",true))
		{
			showPassportDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);	
			isValidated = false;
			
		}
		
		if(address == undefined || address == ""){
			showAddressEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);	
			isValidated = false;
		} if(addressProof == undefined || addressProof == ""){
			showAddressProofEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);	
			isValidated = false;
		}
	} else if(resStatus == "dicitizenpr_"+formNo){
		if((nricfront !==undefined && nricfront !=="" )&& (nricback !== undefined && nricback !== ""))
		{
			if(!displaypreviewpopup("di", formNo, "dinricfront",true) )
			{
				showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true,true);
				
				isValidated = false;
			}
			if(!displaypreviewpopup("di", formNo, "dinricback",true) ){
				showNRICDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo,false, true);
				isValidated = false;
			}
		}else if(!displaymanualpopup("di", formNo, "dinricmanually",true))
		{
			showNRICDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
				
			isValidated = false;
			
		}
	}else if(resStatus == "dipassholder_"+formNo){
		if((finCardFront !==undefined && finCardFront !=="" )&& (finCardBack !== undefined && finCardBack !== "") && (finPassport !== undefined && finPassport !== ""))
		{
			if(!displaypreviewpopup(role_val, formNo, role_val+"finpassport",true)  )
			{
				
				showFinPassportDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);				
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo,role_val+ "fincardfront",true))
			{
				
				
				showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true,true);
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo, role_val+"fincardback",true))
			{
				showFINCardDataEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, false,true);
				isValidated = false;
				
			}
			
		}else if(!displaymanualpopup("di", formNo, "difincardmanually",true))
		{
			showFINDataManualEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
					
			isValidated = false;
			
		}
		 
		if(finAddressProof == undefined || finAddressProof == ""){
			showFinAddressProofEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true, true);	
			isValidated = false;
		} if(finAddress == undefined || finAddress == ""){
			showFinAddressEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true, true);	
			isValidated = false;
		}
	}
	if((handphone == undefined || handphone == "") && (officephone == undefined || officephone == "")){
		if(handphone == undefined || handphone == "" ){
			showContactNumbersEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
			isValidated = false;
		}  
	}else{
		if(!(handphone == undefined || handphone == "")){
			if(isNaN(handphone)|| handphone.trim().length < 8){
				showContactNumbersInvalid(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
				isValidated = false;
			}	
		}
		if(!(officephone == undefined || officephone == "")){
			if(isNaN(officephone)|| officephone.trim().length < 8){
				showOfficeNumberInvalid(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
				isValidated = false;
			}	
		}
	}
	if((businessemail == undefined || businessemail == "") && (personalemail == undefined || personalemail == "")){
		if(businessemail == undefined || businessemail == ""){
			showBizEmailEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
			isValidated = false;
		} 
	}else{
		if(!(businessemail == undefined || businessemail == "")){
			if(ValidateEmail(businessemail) == false){
				showBizEmailInvalidFormat(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
				isValidated = false;
			}
		}
		if(!(personalemail == undefined || personalemail == "")){
			if(ValidateEmail(personalemail) == false){
				showPersonalEmailInvalidFormat(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
				isValidated = false;
			}	
		}
	}
	if(pep == "dishareholderpepyes_"+formNo){
		console.log("pep into one");
		if(pepCountry == undefined || pepCountry == ""){
			showPepCountryEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
			isValidated = false;
		} if(pepRole == undefined || pepRole == ""){
			showPepRoleEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
			isValidated = false;
		} if(pepFrom == undefined || pepFrom == ""){
			showPepFromEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
			isValidated = false;
		} if(pepTo == undefined || pepTo == ""){
			showPepToEmpty(ROLE_DIRECTOR_SHAREHOLDER, formNo, true);
			isValidated = false;
		}
		if((pepFrom != undefined && pepFrom != "") && (pepTo != undefined && pepTo != "")){
			if(checkPepFromToRange(ROLE_DIRECTOR_SHAREHOLDER, pepFrom, pepTo, formNo) != true){
				isValidated = false;
			}
		}				
	}
	{
		localStorage.setItem("dishareholdersurname_"+formNo, surname);
		localStorage.setItem("dishareholderbowner_"+formNo, areYouBenificalOwner);
		localStorage.setItem("dishareholderresstatus_"+formNo, resStatus);
		if(hasManualEntry(MANUAL_DISHAREHOLDER_NONRESIDENT_PASSPORT) == "true"){
			localStorage.setItem("dishareholdernonresidentuploadpassport_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("dishareholdernonresidentuploadpassport_"+formNo, passport);
		}
		
		localStorage.setItem("dishareholdernonresidentaddress_"+formNo, addressProof);
		localStorage.setItem("dishareholdernonresidentresaddress_"+formNo, address);

		if(hasManualEntry(MANUAL_DISHAREHOLDER_FIN_PASSPORT) == "true"){
			localStorage.setItem("dishareholderFINuploadPassport_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("dishareholderFINuploadPassport_"+formNo, finPassport);
		}
		
		localStorage.setItem("dishareholderfincardfront_"+formNo, finCardFront);
		localStorage.setItem("dishareholderfincardback_"+formNo, finCardBack);
		localStorage.setItem("dishareholderFINaddress_"+formNo, finAddressProof);
		localStorage.setItem("dishareholderFINresaddress_"+formNo, finAddress);
		
		if(hasManualEntry(MANUAL_DISHAREHOLDER_CITIZENPR_NRIC) == "true"){
			localStorage.setItem("dishareholderSGnricfront_"+formNo, DEFAULT_IMAGE_PATH);
			localStorage.setItem("dishareholderSGnricback_"+formNo, DEFAULT_IMAGE_PATH);
		}else{
			localStorage.setItem("dishareholderSGnricfront_"+formNo, nricfront);
			localStorage.setItem("dishareholderSGnricback_"+formNo, nricback);
		}
		
		localStorage.setItem("dishareholderhandphone_"+formNo, handphone);
		localStorage.setItem("dishareholderOfficephone_"+formNo, officephone);
		localStorage.setItem("dishareholderbusinessemail_"+formNo, businessemail);
		localStorage.setItem("dishareholderPersonalemail_"+formNo, personalemail);
		localStorage.setItem("dishareholderpoliticallyExposed"+formNo, pep);
		localStorage.setItem("dishareholdercountrypep_"+formNo, pepCountry);
		localStorage.setItem("dishareholderrolePep_"+formNo, pepRole);
		localStorage.setItem("dishareholderpepFrom_"+formNo, pepFrom);
		localStorage.setItem("dishareholderpepTo_"+formNo, pepTo);

	
	}

	return isValidated;
}
function validateCorporateShareholderRoleForm(formNo){
	console.log(ROLE_CORPORATE_SHAREHOLDER + formNo);
	startEventsCorporateShareholder(formNo);
	var surname;
	var corporateCountry;
	var corporateCertificate;
	var corporateCertificate1;
	var representativeName;
	var businessemail;
	var contactNumber;
	var uploadChat;

	var resStatus;
	var address;
	var passport;
	var addressProof;

	var finPassport;
	var finCardFront;
	var finCardBack;
	var finAddressProof;
	var finAddress;

	var nricfront;
	var nricback;
	var corporatechart;

	
	var role_val="corporate";
	surname = $("#corporatename_"+formNo).val();
	//console.log("surname "+surname);
	corporateCountry = $("#corporatecountry_"+formNo).val();
	//console.log("corporateCountry "+corporateCountry);
	corporateCertificate = $("#corporatecertificate_"+formNo).val();
	//console.log("corporateCertificate "+corporateCertificate);
	corporateCertificate1 = $("#corporatecertificate1_"+formNo).val();
	//console.log("corporateCertificate1 "+corporateCertificate);	
	representativeName = $("#representativename_"+formNo).val();
	//console.log("representativeName "+representativeName);
	businessemail = $("#corporateemail_"+formNo).val();
	//console.log("businessemail "+businessemail);
	contactNumber = $("#corporatecontactnumber_"+formNo).val();
	//console.log("contactNumber "+contactNumber);	

	resStatus = $("#corporateresstatus_"+formNo).val();
	//console.log("corporateresstatus_ "+resStatus);
	passport = $("#corporatenonresidentuploadpassport_"+formNo).val();
	//console.log("passport "+passport);
	addressProof = $("#corporatenonresidentaddress_"+formNo).val();
	//console.log("addressProof "+addressProof);
	address = $("#corporatenonresidentresaddress_"+formNo).val();
	//console.log("address "+address);
	nricfront = $("#corporatenricfront_"+formNo).val();
	//console.log("nricfront "+nricfront);
	nricback = $("#corporatenricback_"+formNo).val();
	//console.log("nricback "+nricback);	

	/* finCard */
	finPassport = $("#corporateuploadPassport_"+formNo).val();	
	//console.log("finPassport "+finPassport);
	finCardFront = $("#corporatefincardfront_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finCardBack = $("#corporatefincardback_"+formNo).val();
	//console.log("finCardBack "+finCardBack);
	finAddressProof = $("#corporateaddress_"+formNo).val();
	//console.log("finCardFront "+finCardFront);
	finAddress = $("#corporateresaddress_"+formNo).val();
	//console.log("finAddress "+finAddress);	
	/*  */	
	corporatechart = $("#corporatechartname_"+formNo).val();

	if(surname == undefined || surname == ""){
		showFullNameEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true);		
		isValidated = false;
	} if(corporateCountry == undefined || corporateCountry == ""){
		showCountryEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true);
		isValidated = false;
	} if(resStatus == "corporatenonresident_"+formNo){
		if(passport !==undefined && passport !=="")
		{
			if(!displaypreviewpopup("corporate", formNo, "corporatenonresident",true))
			{
				showPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true);	
				isValidated = false;
			}
			
		}else if(!displaymanualpopup("corporate", formNo, "corporatepassportmanually",true))
		{
			showPassportDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true);	
			isValidated = false;
			
		}
		if(address == undefined || address == ""){
			showAddressEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true);	
			isValidated = false;
		} if(addressProof == undefined || addressProof == ""){
			showAddressProofEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true);	
			isValidated = false;
		}
	}else if(resStatus == "corporatecitizenpr_"+formNo){
		if((nricfront !==undefined && nricfront !=="" )&& (nricback !== undefined && nricback !== ""))
		{
			if(!displaypreviewpopup("corporate", formNo, "corporatenricfront",true) )
			{
				showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true,true);
				
				isValidated = false;
			}
			if(!displaypreviewpopup("corporate", formNo, "corporatenricback",true) ){
				showNRICDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,false, true);
				isValidated = false;
			}
			
		}else if(!displaymanualpopup("corporate", formNo, "corporatenricmanually",true))
		{
			showNRICDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true);
				
			isValidated = false;
			
		}
	}else if(resStatus == "corporatepassholder_"+formNo){
		if((finCardFront !==undefined && finCardFront !=="" )&& (finCardBack !== undefined && finCardBack !== "") && (finPassport !== undefined && finPassport !== ""))
		{
			if(!displaypreviewpopup(role_val, formNo, role_val+"finpassport",true)  )
			{
				
				showFinPassportDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true);				
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo,role_val+ "fincardfront",true))
			{
				
				
				showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true,true);
				isValidated = false;
			}
			
			if( !displaypreviewpopup(role_val, formNo, role_val+"fincardback",true))
			{
				showFINCardDataEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, false,true);
				isValidated = false;
				
			}
			
		}else if(!displaymanualpopup("corporate", formNo, "corporatefincardmanually",true))
		{
			showFINDataManualEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo,true);
				
			isValidated = false;
			
		}
		if(finAddressProof == undefined || finAddressProof == ""){
			showFinAddressProofEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true, true);	
			isValidated = false;
		} if(finAddress == undefined || finAddress == ""){
			showFinAddressEmpty(ROLE_CORPORATE_SHAREHOLDER, formNo, true, true);	
			isValidated = false;
		}
	}
	if(!(businessemail == undefined || businessemail == "")){
		if(ValidateEmail(businessemail) == false){
			showBizEmailInvalidFormat(ROLE_CORPORATE_SHAREHOLDER, formNo, true);
			isValidated = false;
		}
	}		
	if(!(contactNumber == undefined || contactNumber == "")){
		if(isNaN(contactNumber)|| contactNumber.trim().length < 8){
			showContactNumbersInvalid(ROLE_CORPORATE_SHAREHOLDER, formNo, true);
			isValidated = false;
		}	
	}
     
    localStorage.setItem("corporate_chart_"+formNo,corporatechart);
	localStorage.setItem("CorporateShareholderName_"+formNo,surname);
	localStorage.setItem("CorporateCountry_"+formNo,corporateCountry);
	if(corporateCertificate == undefined || corporateCertificate == ""){
		localStorage.setItem("CorporateCertificate_"+formNo,DEFAULT_IMAGE_PATH);
	}else{
		localStorage.setItem("CorporateCertificate_"+formNo,corporateCertificate);	
	}
	if(corporateCertificate1 == undefined || corporateCertificate1 == ""){
		localStorage.setItem("CorporateCertificate1_"+formNo,DEFAULT_IMAGE_PATH);
	}else{
		localStorage.setItem("CorporateCertificate1_"+formNo,corporateCertificate1);
	}	
	localStorage.setItem("CorporateRepresentativeName_"+formNo,representativeName);
	localStorage.setItem("CorporateEmail_"+formNo,businessemail);
	localStorage.setItem("CorporatecontactNumber_"+formNo,contactNumber);
	localStorage.setItem("corporateresstatus_"+formNo,resStatus);
	if(hasManualEntry(MANUAL_CORPORATE_NONRESIDENT_PASSPORT) == "true"){
		localStorage.setItem("corporatenonresidentuploadpassport_"+formNo, DEFAULT_IMAGE_PATH);
	}else{
		localStorage.setItem("corporatenonresidentuploadpassport_"+formNo, passport);
	}
		
	localStorage.setItem("corporatenonresidentaddress_"+formNo, addressProof);
	localStorage.setItem("corporatenonresidentresaddress_"+formNo, address);
	if(hasManualEntry(MANUAL_CORPORATE_FIN_PASSPORT) == "true"){
		localStorage.setItem("corporateFINuploadPassport_"+formNo, DEFAULT_IMAGE_PATH);
	}else{
		localStorage.setItem("corporateFINuploadPassport_"+formNo, finPassport);
	}	
	localStorage.setItem("corporatefincardfront_"+formNo, finCardFront);
	localStorage.setItem("corporatefincardback_"+formNo, finCardBack);
	localStorage.setItem("corporateFINaddress_"+formNo, finAddressProof);
	localStorage.setItem("corporateFINresaddress_"+formNo, finAddress);
	if(hasManualEntry(MANUAL_CORPORATE_CITIZENPR_NRIC) == "true"){
		localStorage.setItem("corporateSGnricfront_"+formNo, DEFAULT_IMAGE_PATH);
		localStorage.setItem("corporateSGnricback_"+formNo, DEFAULT_IMAGE_PATH);
	}else{
		localStorage.setItem("corporateSGnricfront_"+formNo, nricfront);
		localStorage.setItem("corporateSGnricback_"+formNo, nricback);
	}	

	return isValidated;
}
function validateAddRole(){
	var addRole = $("#addroleSelector").val();
	//console.log("addRole "+addRole);
	if(addRole != "Nomore" || addRole == ""){
		//console.log("addRole - final");		
		showAddRoleSelectorEmpty(true);
		return false;
	}
	return true;
}
function ValidateEmail(mail) 
{
	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail))
	{
		return true;
	}
	return false;
}

function doNext(){
	/*localStorage.clear();*/
	console.log("==========All Values==========");
	for (var i = 0; i < localStorage.length; i++){
	    console.log(localStorage.key(i)+" ==> "+localStorage.getItem(localStorage.key(i)));
	}
	console.log("=============================");
	if(validateForm() == true){
		if(validateAddRole() == true){
			
			return true;
			/*document.form1.submit();
			document.location.href = "https://www.winimy.ai/rikvinnew/declaration.php";*/
		}
	}else{
		return false;
		console.log("form submit failed - validation error");
	}	
	return false;
}

//-----------------------------------------------------------------------------
// Show generic error
//-----------------------------------------------------------------------------

function showResidentStatusEmpty(role, formNo, show){
	var fullNameAlert;
	var fullNameAlertMsg;

	if(role == ROLE_DIRECTOR){
		fullNameAlert = $('#fullName'+ formNo +'-alert');
		fullNameAlertMsg = $('#fullName'+ formNo +'-alert-msg');
	}else if(role == ROLE_SHAREHOLDER){
		fullNameAlert = $('#shareholderfullName'+ formNo +'-alert');
		fullNameAlertMsg = $('#shareholderfullName'+ formNo +'-alert-msg');
	}else if(role == ROLE_DIRECTOR_SHAREHOLDER){
		fullNameAlert = $('#dishareholderfullName'+ formNo +'-alert');
		fullNameAlertMsg = $('#dishareholderfullName'+ formNo +'-alert-msg');
	}else if(role == ROLE_CORPORATE_SHAREHOLDER){
		fullNameAlert = $('#corporatename'+ formNo +'-alert');
		fullNameAlertMsg = $('#corporatename'+ formNo +'-alert-msg');
	} 
	if (show == true) {
		fullNameAlertMsg.text(ERR_MSG_FULLNAME_EMPTY);
		fullNameAlert.show().prev().css('border-color','red');
	} else {
		fullNameAlert.hide().prev().css('border-color','');
	}
}

</script>
