		$(function () {
			var countries = ['', 'Afghanistan', 'Aland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia, Plurinational State of', 'Bonaire, Sint Eustatius and Saba', 'Bosnia and Herzegovina', 'Botswana', 'Bouvet Island', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'China', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo', 'Congo, The Democratic Republic of The', 'Cook Islands', 'Costa Rica', 'Cote D\'ivoire', 'Croatia', 'Cuba', 'Curacao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands (Malvinas)', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Territories', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea-bissau', 'Guyana', 'Haiti', 'Heard Island and Mcdonald Islands', 'Holy See (Vatican City State)', 'Honduras', 'Hong Kong', 'Hungary', 'Iceland', 'India', 'Indonesia', 'Iran, Islamic Republic of', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea, Democratic People\'s Republic of', 'Korea, Republic of', 'Kuwait', 'Kyrgyzstan', 'Lao People\'s Democratic Republic', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macao', 'Macedonia, The Former Yugoslav Republic of', 'Madagascar', 'Malawi', 'Malaysia', 'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia, Federated States of', 'Moldova, Republic of', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestinian Territory, Occupied', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Philippines', 'Pitcairn', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Reunion', 'Romania', 'Russian Federation', 'Rwanda', 'Saint Barthelemy', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin (French part)', 'Saint Pierre and Miquelon', 'Saint Vincent and The Grenadines', 'Samoa', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Singapore', 'Sint Maarten (Dutch part)', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Georgia and The South Sandwich Islands', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Swaziland', 'Sweden', 'Switzerland', 'Syrian Arab Republic', 'Taiwan, Province of China', 'Tajikistan', 'Tanzania, United Republic of', 'Thailand', 'Timor-leste', 'Togo', 'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Arab Emirates', 'United Kingdom', 'United States', 'United States Minor Outlying Islands', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela, Bolivarian Republic of', 'Viet Nam', 'Virgin Islands, British', 'Virgin Islands, U.S.', 'Wallis and Futuna', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe'];
			
			var countriesOption = countries.map(function(country){
				return '<option value="' + country + '">' + country + '</option>'
			});

			$('.chosen-select').html(countriesOption);
			$('[data-toggle="tooltip"]').tooltip();
			$(".chosen-select").chosen();
			$(".show-when-pep").hide();
			$(".show-when-corporatepep").hide();
			
			//Intended address toggle
			$(document).on('click', 'input[name="intendedAddress"]', function(e){
				if (this.value == "serviceProviderAddress") {
					$(".show-service-address").toggle();
					$(".show-own-address").hide();
				} else if (this.value == "ownAddress") {
					$(".show-own-address").toggle();
					$(".show-service-address").hide();
				} else {
					$(".show-service-address, .show-own-address").hide();
				}
			});
			
			//Select role
			$(document).on('change', 'select[name="roleSelector"]', function(e){
				var rolesContent = $('.choose-role');
				var rel = $('option:selected', this).attr('rel');

				rolesContent.removeClass('active').hide();

				if (rel) {
					$('#' + rel).addClass('active').show();
				}
			});
				
			//Current residency status
			$(document).on('change', 'select[name="residencyStatus"]', function(e){
				var uploadFields = $('.choose-file');
				var rel = $('option:selected', this).attr('rel');
				var activeRoleContainer = $('.role-container.active');

				uploadFields.hide();

				if (rel) {
					$('#' + rel, activeRoleContainer).show();
				}
			});
			
			//Politically Exposed Person
			$(document).on('click', 'input[name="directorpep"]', function(e){
				if (this.value == "pep") {
					$(".show-when-pep").show();
				} else {
					$(".show-when-pep").hide();
				}
			});
			
			//Politically Exposed Person - Shareholder
			$(document).on('click', 'input[name="shareholderpep"]', function(e){
				if (this.value == "pepshareholder") {
					$(".show-when-shareholderpep").show();
				} else {
					$(".show-when-shareholderpep").hide();
				}
			});
			$(document).on('click', 'input[name="corporatePep"]', function(e){
				if (this.value == "notCorporatePep") {
					$(".show-when-corporatepep").show();
				} else {
					$(".show-when-corporatepep").hide();
				}
			});
			
			
			//Corporate Representative
			$(document).on('click', 'input[name="corporateRepresentative"]', function(e){
				if (this.value == "notCorporateRepresentative") {
					$(".show-when-cr").show();
				} else {
					$(".show-when-cr").hide();
				}
			});
			
			//Beneficial Owner
			$(document).on('click', 'input[name="beneficialowner"]', function(e){
				if (this.value == "NotBeneficialOwner") {
					$(".show-when-not-bo").show();
				} else {
					$(".show-when-not-bo").hide();
				}
			});
			//Current residency status of beneficial owener
			$(document).on('change', 'select[name="beneficialOwnerResidencyStatus"]', function(e){
				var uploadFields = $('.bo-choose-file');
				var rel = $('option:selected', this).attr('rel');

				uploadFields.hide();

				if (rel) {
					$('#' + rel).show();
				}
			});
});

