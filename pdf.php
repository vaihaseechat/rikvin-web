<!DOCTYPE html>
<!-- saved from url=(0039)https://www.winimy.ai/rikvinnew/pdf.php -->
<html lang="en" class="gr__winimy_ai"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>InCorp - On Boarding Customer Due Diligence Form</title>
</head>
	
<body data-gr-c-s-loaded="true" style="font-family: 'Roboto', sans-serif !important;">


	<div class="container" style="width:800px; margin:0 auto">
	
				<img src="images/logo.png" style="margin:25px 0">
					<h3 style="margin:25px 0">Customer Due Diligence Acceptance Form</h3>
					<p>Key definitions used in the document</p>
					<ul style="line-height:25px;">
						<li>A "Beneficial Owner", means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or exercises control over the management.</li>
						<li>A "Politcally Exposed Person" (PEP) is a term describing someone who has been entrusted with any prominent public function in Singapore.</li>
					</ul>
					
					<h4 style="border-bottom:2px solid #000; padding-bottom:10px;">Proposed Company Information</h4>
		
	
		<table  cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; border-bottom:none; width:100%;">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Proposed company name</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">1 Tech Solutions Pte Ltd<br>One Tech Solutions Pte Ltd<br>Wan Tek Solutions Pte Ltd</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Registered address of company</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">60 Albert Street,<br>
						#08-07 OG Albert Complex, <br>
						Singapore 189969</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Business activities</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">General Wholesale Trade (Including General Importers and Exporters)</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Countries of operation</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Singapore, UAE, Cambodia, Vietnam, Hong kong, Malaysia</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Source of Funds</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Bank loans/own cash</span></td>
					</tr>
				</tbody>
			</table>
		
					<h4 class="mb-3 mt-5 font-weight-bold">Particulars of all Directors, Shareholders and Beneficial Owners</h4>
					<h4 style="background:#ddd;padding:15px;margin:  0;border:  1px solid #ccc;border-bottom: 0;">Director #1 - Wee Seok Hua Jacqueline</h4>
		
				<table border="0" cellpadding="0" cellspacing="0" style="width:100%; vertical-align:top;  border:1px solid #ccc;">
						<tbody>
							<tr>
							<td width="50%" valign="top"> 
							<table border="0" cellpadding="5" cellspacing="0" style=" width:100%; border-bottom:0; border-right:1px solid #ccc;">
						<tbody>
							<tr>
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Wee Seok Hua Jacqueline</span></td>
							</tr>
							<tr>								
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Female</span></td>
							</tr>
							<tr>								
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">S778888H</span></td>
							</tr>
							<tr>								
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">03/05/1977</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Singapore</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">97968454</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Email </span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">jacqueline@one.com</span></td>
							</tr>
							<tr>
								<td style=" padding:10px 10px"><span>PEP</span></td>
								<td style=""><span style="font-weight:bold">No</span></td>
							</tr>
						</tbody>
					</table></td>
								<td width="50%" style="text-align:center"><div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-front.jpg" class="img-fluid" alt=""></div>
						<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-back.jpg" class="img-fluid" alt=""></div></td>
							</tr>
							</tbody>							
							</table>

					<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents NRIC front and back, nationality Singaporean)</p>		
		
					<h4 style="background:#ddd;padding:15px;margin:  0;border:  1px solid #ccc;border-bottom: 0;">Director #2</h4>
				<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; border-bottom:none; width:100%;">
						<tbody>
							<tr>
								<td style="border-bottom:1px solid #ccc;"><span>Full name</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Wee Seok Hua Jacqueline </span></td>
								<td style="border-bottom:1px solid #ccc;"><span>Gender</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Female</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc;"><span>NRIC</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">S778888H</span></td>
								<td style="border-bottom:1px solid #ccc;"><span>Nationality</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td style="border-bottom:1px solid #ccc;"><span>Birthdate</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">03/05/1977</span></td>
								<td style="border-bottom:1px solid #ccc;"><span>Birthplace</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Singapore</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc;"><span>Address</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
								<td style="border-bottom:1px solid #ccc;"><span>Contact No.</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">97968454</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc;"><span>Email </span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">jacqueline@one.com</span></td>
								<td style="border-bottom:1px solid #ccc;"><span>PEP</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">No</span></td>
							</tr>
						</tbody>
					</table>
					<p style="font-size:13px; font-style:italic;">(Sample without documents - entered manually, Singaporean)</p>
			
		
					
					<h4 style="background:#ddd;padding:15px;margin:  0;border:  1px solid #ccc;border-bottom: 0;">Individual Shareholder #1 </h4>
					
					<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
					  <tbody>
						<tr>
							<td width="50%" valign="top">	
						<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
					  <tbody>
						<tr>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Whakaaturanga Fred John</span></td>
						</tr>
						<tr>								
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Male</span></td>
						</tr>
						<tr>								
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">New Zealand</span></td>
						</tr>
						<tr>
						 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
						  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">L00001148</span></td>								
						</tr>
						  <tr>
						    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
						   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">New Zealand</span></td>								
						</tr>
						<tr>
						  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
						  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">05/10/2009</span></td>								
						</tr>
						<tr>
						 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
						  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">05/10/2014</span></td>								
						</tr>
						<tr>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">25/02/1964</span></td>
						</tr>
						<tr>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Taihape</span></td>
						</tr>
						<tr>
							<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
						</tr>
						<tr>								
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Diamond Pte Ltd</span></td>
						</tr>
						<tr>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
						 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Managing Director</span></td>								
						</tr>
						<tr>								
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">G12345678</span></td>
						</tr>
						<tr>								
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Type </span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Employment Pass</span></td>
						</tr>
						<tr>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">01/02/2018</span></td>
						</tr>
						<tr>								
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
							<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">01/02/2019</span></td>
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Hougang Street 11, <br>#10-34 The Minton, <br>Singapore 534079</span></td>
						</tr>
											
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">97968454</span></td>
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">fred@one.com</span></td>
						</tr>
						<tr>
							<td style=" padding:10px 10px"><span>PEP</span></td>
							<td style="padding:10px 10px"><span style="font-weight:bold">No</span></td>
						</tr>
					</tbody>
				  	</table></td>
							<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
					  <tbody>
						 <tr>
							<td style="border-bottom:1px solid #ccc"><span>Beneficial Owner</span></td>
							<td style="border-bottom:1px solid #ccc"><span style="font-weight:bold">No</span></td>
						</tr>
						<tr>
							<td style="border-bottom: 0 !important"><span>Uploaded documents:</span></td>
							<td style="border-bottom: 0 !important"><span></span></td>
						</tr>
						<tr>								
							<td colspan="2" style="border-bottom:1px solid #ccc">
							<div class="uploaded-image"><img src="images/Ana_Sayfa.png" class="img-fluid" alt=""></div>
						<div class="uploaded-image"><img src="images/Ana_Sayfa.png" class="img-fluid" alt=""></div>
						<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/ep-front.jpg" class="img-fluid" alt=""></div></td>
						</tr>
						 <tr>
							<td style="border-bottom: 0 !important"><span>Proof of address</span></td>
							 <td style="border-bottom: 0 !important"><span style="font-weight:bold"><a href="https://www.winimy.ai/rikvinnew/pdf.php#">fred-utility-address.pdf</a></span></td>
						</tr>	
						</tbody>
					</table></td>
						</tr>
						  </tbody>
						</table>
									
					
					<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>				
				
						<h4 style="background:#ddd;padding:15px;margin:  0;border:  1px solid #ccc;border-bottom: 0;">Individual Shareholder #2</h4>
				
					<table border="0" cellpadding="5" cellspacing="0" style=" border:1px solid #ccc; border-bottom:none; width:100%;">
						<tbody>
						<tr>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span>Full name</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">Whakaaturanga Fred John</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span>Beneficial Owner</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">No</span></td>
						</tr>
						<tr>								
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span>Gender</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">Male</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span>Nationality</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">New Zealand</span></td>
						</tr>
						<tr>								
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span>Passport</span></td>
						<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">L00001148</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span>Country of Issue</span></td>
							<td style=" border-bottom:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">New Zealand</span></td>
						</tr>
						<tr>								
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Issue date</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">05/10/2009</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Expiry date</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">05/10/2014</span></td>
						</tr>
						<tr>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Birthdate</span></td>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">25/02/1964</span></td>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Birthplace</span></td>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Taihape</span></td>
						</tr>
						<tr>								
							<td colspan="4" style="padding:15px 10px; background:#ddd; border-bottom:1px solid #ccc;"><span style="font-weight:bold;">FIN Details</span></td>
						</tr>
						<tr>								
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Employer</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Diamond Pte Ltd</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Occupation</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Managing Director</span></td>
						</tr>
						<tr>								
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>FIN </span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">G12345678</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Type</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Employment Pass</span></td>
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Issue date</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">11/12/2015</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Expiry date</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">01/12/2018</span></td>
						</tr>
						<tr>								
						<td colspan="4" style="padding:15px 10px;background:#ddd; border-bottom:1px solid #ccc;"><span style="font-weight:bold;" >Contact Details</span></td>
						</tr>
						<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">Hougang Street 11,<br>#10-34 The Minton,<br>Singapore 534079</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Proof of address</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold"><a href="https://www.winimy.ai/rikvinnew/pdf.php#"></a></span></td>
						</tr>
						<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Email</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">fred@one.com</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Contact No.</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">00121345641</span></td>
						</tr>
						
					</tbody>
					</table>
					<p style="font-size:13px; font-style:italic;">(Sample without uploaded documents, information entered manually, Foreigner - EP)</p>
				
				
				
			
				
				  <h4 style="background:#ddd;padding:15px;margin:  0;border:  1px solid #ccc;border-bottom: 0;">Individual Shareholder #3 </h4>
				  
				   <table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc; ">
					  <tbody>
						<tr>
							<td width="50%" valign="top"> <table border="0" cellpadding="5" cellspacing="0" style="width:100%; border-right:1px solid #ccc;">
					  <tbody>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Full name</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Mark David Aaron Base</span></td>
						</tr>
						<tr>								
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Gender</span></td>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Male</span></td>
						</tr>
						<tr>								
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Nationality</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">British Citizen</span></td>
						</tr>
						<tr>
						  <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Passport No.</span></td>
						  <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">704902166</span></td>								
						</tr>
						  <tr>
						    <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Country of issue</span></td>
						    <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">UK</span></td>								
						</tr>
						<tr>
						  <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Issue date</span></td>
						 <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">05/10/2009</span></td>								
						</tr>
						<tr>
						 <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Expiry date</span></td>
						 <td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">04/03/2004</span></td>								
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Birthdate</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">08/03/1965</span></td>
						  </tr>
						  <tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Birthplace</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Montreal</span></td>
						</tr>
						  <tr>								
							<td style=" border-bottom:1px solid #ccc; background:#f1f1f1; padding:15px 10px;" colspan="4"><span style="font-weight:bold">Contact Details</span></td>
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Residential address</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Hougang Street 11,<br>#10-34 The Minton,<br>Singapore 534079</span></td>
						  </tr>
						 
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Email</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">base@one.com</span></td>
							</tr>
						  <tr>
							<td style="  padding:10px 10px;"><span>Contact No.</span></td>
							<td style="  padding:10px 10px;"><span style="font-weight:bold">00121345641</span></td>
						</tr>
					</tbody>
				  </table></td>
							<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style=" width:100%;">
					  <tbody>
						  <tr>
							<td style="border-bottom:1px solid #ccc;"><span>Beneficial Owner</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">No</span></td>
						</tr>
						<tr>
							<td style="border-bottom: 0 !important"><span>Uploaded documents:</span></td>
							<td style="border-bottom: 0 !important"><span></span></td>
						</tr>
						<tr>								
							<td colspan="2"><div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-4.jpg" class="img-fluid" alt=""></div>
						</td>
						</tr>
						
						</tbody>
					</table></td>
						</tr>
					</tbody>
					</table>					
				  
				  
				<br>
					
					<table border="0" cellpadding="5" cellspacing="0" style=" border:1px solid #ccc; border-bottom:none; width:100%;">
					<tbody><tr>								
							<td colspan="4" style="border-bottom:1px solid #ccc; padding:20px 10px"><span style="font-weight:bold;">Politically Exposed Person</span></td>
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Yes</span></td>
						  
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP in which country</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">UK</span></td>
						</tr>
						<tr>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Role of PEP</span></td>
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Political Party Leadership</span></td>
						 
							<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP since</span></td>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">2000 - 2005</span></td>
						</tr>
					</tbody></table>
					<p style="font-size:13px; font-style:italic;">(Sample with uploaded passport but without proof of address, Foreigner)</p>
				
				
			
				<h4 style="background:#ddd;padding:15px;margin:  0;border:  1px solid #ccc;border-bottom: 0;">Corporate Shareholder #1</h4>
					<table border="0" cellpadding="5" cellspacing="0" style=" border:1px solid #ccc; border-bottom:0px; width:100%;">
						<tbody>
							<tr>
								<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Name</span></td>
								<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">One Tech Solutions Sdn Bhd</span></td>
								
							</tr>
							<tr>
								<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Country of Incorporation</span></td>
								<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Malaysia</span></td>
							</tr>
							<tr>
								<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Documents Uploaded</span></td>
								<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold;color:#0056b3"><a href="https://www.winimy.ai/rikvinnew/pdf.php#">certificate-of-incorporation.pdf</a><br><a href="https://www.winimy.ai/rikvinnew/pdf.php#">extract-of-company.pdf</a></span></td>
							</tr>
						</tbody>
					</table>					
				
			<br>
					<table border="0" cellpadding="5" cellspacing="0" style=" border:1px solid #ccc; border-bottom:0; width:100%;">
						<tbody>
							<tr class="table-active">
								<td style="background:#ddd; border-bottom:1px solid #ccc; padding:15px 10px; " colspan="4"><span style="font-weight:bold"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Corporate Representative</span></td>
							</tr>
							<tr>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span>Full name</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">Cheryl Lee</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;padding:10px 10px;"><span>Gender</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;padding:10px 10px;"><span style="font-weight:bold">Female</span></td>
							</tr>
							<tr>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span>NRIC</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">S010101</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span>Nationality</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span>Birthdate</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">03/05/1977</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span>Birthplace</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Singapore</span></td>
							</tr>
							<tr>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;  padding:10px 10px;"><span>Address</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;  padding:10px 10px;"><span>Contact No.</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;  padding:10px 10px;"><span style="font-weight:bold">97968454</span></td>
							</tr>
							<tr>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;  padding:10px 10px;"><span>Email </span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;  padding:10px 10px;"><span style="font-weight:bold">cheryl@one.com</span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;   padding:10px 10px;"><span></span></td>
								<td style=" border-bottom:1px solid #ccc; border-right:1px solid #ccc;  padding:10px 10px;"><span style="font-weight:bold"></span></td>
							</tr>
						</tbody>
					</table>					
				
				
			
				
				<hr style="background: #666; height: 2px; width: 100%; margin:25px 0 0 0;">
					<h3 style="margin:25px 0 10px 0">Shareholding Structure</h3>				
					<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; border-right:0; width:100%;">
						<tbody>
							<tr>
								<td style="border-right:1px solid #ccc"><span>Currency</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">Singapore Dollar</span></td>
								<td style="border-right:1px solid #ccc"><span>Paid Up capital</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">100,000</span></td>
								<td style="border-right:1px solid #ccc"><span>No. of shares</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">100</span></td>
							</tr>							
						</tbody>
					</table>
				
			
				<h3 style="margin:25px 0 10px 0">List of Shareholders</h3>				
					<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; border-bottom:0; width:100%;">
						<tbody style="border: 1px solid #e9ecef !important;">
							<tr class="table-secondary">
								<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Shareholders</span></td>
								<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Paid up capital</span></td>
								<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>No. of shares</span></td>
								<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Beneficial owner</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Whakaaturanga Fred Wiremu John</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">50,000</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">100</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">No</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Mark David Aaron Base</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">50,000</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">100</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">Yes</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">One Tech Solutions Sdn Bhd</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">50,000</span></td>
								<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">100</span></td>
								<td style="border-bottom:1px solid #ccc;"><span>&nbsp;</span></td>
							</tr>
						</tbody>
					</table>
				
				<br>
					<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; border-bottom:0; border-right:none; width:100%;">
						<tbody>
							<tr class="table-active">
								<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="4"><span style="font-weight:bold"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> Whakaaturanga Fred Wiremu John Ultimate Beneficial Owner</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc; "><span>Full name</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc; "><span style="font-weight:bold">Satish Bakhda </span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>Gender</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">Male</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>NRIC</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">S123245678</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>Nationality</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">Singapore Citizen</span></td>
							</tr>
							<tr>								
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>Birthdate</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">03/05/1977</span></td>
								<td style="border-bottom:1px solid #ccc;border-right:1px solid #ccc; "><span>Birthplace</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">Singapore</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>Address</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">Hougang Street 11,<br>#10-34 The Minton, <br>Singapore 534079</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>Contact No.</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">97968454</span></td>
							</tr>
							<tr>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>Email </span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">satish@one.com</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span>PEP</span></td>
								<td style="border-bottom:1px solid #ccc; border-right:1px solid #ccc;"><span style="font-weight:bold">No</span></td>
							</tr>
						</tbody>
					</table>
			
				
				
					<h3 class="mb-5" style="border-bottom:2px solid #000; display: block; width: 100%; margin:35px 0 0 0;
					padding-bottom: 10px;">Declaration</h3>
					<p>I declare that the information provided in this form is true and correct. I am aware that I may be subject to prosecution if I am found to have made any false statement which I know to be false or intentionally suppressed any material fact.</p>
			
				<table width="100%">
						<tbody>
							<tr>
								<td width="270"><span>Name of Customer/Agent</span></td>
								<td><span style="font-weight:bold">Cheryl Lee</span></td>
							</tr>					
						</tbody>
					</table>
			
	<br><br><br><br><br><br><br><br><br><br><br>
	



</body></html>
