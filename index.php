<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>InCorp - On Boarding Customer Due Diligence Form</title>
	
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Roboto" rel="stylesheet">	
	<link rel="stylesheet" href="css/style.css?v7">	
	<link rel="stylesheet" href="css/bootstrap.css?v1">
	<link rel="stylesheet" href="css/chosen.css?v1">
	<link rel="stylesheet" href="css/prism.css">	
	<link rel="stylesheet" href="css/font-awesome.min.css">

	
</head>
	
<body>
	<nav id="mainNav">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-10">
					<a href="#page-top"><img src="images/logo.png" alt="" /></a>			
				</div>
			</div>
		</div>
	</nav>
	
	<header class="masthead text-white bg-header">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-10">
        			<h1 class="text-center">On Boarding Customer Due Diligence Form</h1>
				</div>
			</div>
		</div>
	</header>
	
	<header class="bg-intro border-bottom">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10 col-sm-12">
					<div class="col-lg-8 col-sm-12 float-left">
						<p>If you have a complex business structure we request you to download offline version of this form.</p>
					</div>
					<div class="col-lg-4 col-sm-12 float-left">
						<a class="btn btn-block cdd-download" href="#">Download offline version</a>
					</div>
				</div>
			</div>
		</div>
	</header>
	
	<div class="container">
		<form name="form1" id="form1" method="post" action="declaration.php" enctype="multipart/form-data" onsubmit="return doNext();">
			<div class="row justify-content-center">
				<div class="col-sm-10"><h3 class="mb-5 mt-5">Proposed Company Information</h3></div>
			</div>
			<div class="row justify-content-center">
				<div class="col-sm-10">
					<div class="form-group row">
						<div class="col-sm-4">
							<label class="col-form-label required " for="companyName">Proposed company name</label>
							
						</div>
						<div class="col-sm-8 proposed-company-wrapper">
							<div class="form-group mb-2 clearfix proposed-company-item row">
								<input type="text" class="input-proposed col-sm-auto form-bg-2" name="proposedCompanyName" placeholder="Enter proposed company name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter proposed company name'" />
								<button class="btn btn-add add-more col-sm-auto" type="button"><i class="fa fa-plus" aria-hidden="true"></i></button>
								<button class="btn remove-item d-none col-sm-auto" type="button"><i class="fa fa-minus" aria-hidden="true"></i></button>
							</div>
						</div>
					</div>
					<div class="row d-none proposed-company-alert" id="proposed-company-alert">
						<div class="col-sm-8 offset-sm-4"><p class="font-italic small" id="proposed-company-alert-msg">Maximum of 3 proposed company names are allowed.</p></div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4">
							<label class="col-form-label" for="AddressProposedCompany">Registered address of company</label>
						</div>
						<div class="col-sm-8">
							<div class="row">
								<div class="col-sm-auto">
									<label class="radio-inline">
										<input type="radio" class="radio-red" name="intendedAddress" value="serviceProviderAddress"> Service provider's address
									</label>
								</div>
								<div class="col-sm-auto">
									<label class="radio-inline">
										<input type="radio" class="radio-red" name="intendedAddress" value="ownAddress"> I will use my own address
									</label>
								</div>
								<div class="col-sm-12">
									<div class="show-service-address">
									<textarea id="serviceaddress" class="form-control form-bg-2" rows="2" readonly>30 Cecil Street, #19-08 Prudential Tower, Singapore 049712</textarea>
									</div>
								</div>
								<div class="col-sm-12">
									<div class="show-own-address">
										<div class="row">
											<div class="col-sm-3 form-bg-2 mb-2"><input type="text" class="form-control form-bg-2" id="postal-code" placeholder="Postal Code" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Postal Code'"></div>
											<div class="col-sm-9 mb-2">
												<textarea class="form-control form-bg-2" rows="1" id="street-name" placeholder="Street Name" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Please type the complete intended address of proposed company'"></textarea>
											</div>
											
											<div class="col-sm-3 form-bg-2 mb-2"><input type="text" class="form-control form-bg-2" id="floor" placeholder="Floor" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Floor'"></div>
											
											<div class="col-sm-3 form-bg-2 mb-2"><input type="text" class="form-control form-bg-2" id="unit" placeholder="Unit" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Unit'"></div>
											
											
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row proposed-company-address-alert" id="proposed-company-address-alert" style="color:#B90D0F;">
						<div class="col-sm-8 offset-sm-4"><p class="font-italic small" id="proposed-company-address-alert-msg"></p></div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4">
							<label class="col-form-label" for="businessActivities">Business activities</label>
						</div>
						<div class="col-sm-8">
							<textarea class="form-control form-bg-2 col-sm-12" name="businessActivities" id="businessActivities" rows="2" placeholder="Please give brief description" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Please give brief description'"></textarea>
						</div>
					</div>
					<div class="row businessActivities-alert" id="businessActivities-alert" style="color:#B90D0F;">
						<div class="col-sm-8 offset-sm-4"><p class="font-italic small" id="businessActivities-alert-msg"></p></div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4">
							<label class="col-form-label" for="countriesOperation">Countries of operation</label>
						</div>
						<div class="col-sm-8">							
							<select data-placeholder="List of countries where majority of your clients/suppliers are based" class="chosen-select chosen-select-country chosen-textarea" id="countriesOperation" multiple tabindex="6"></select>
						</div>
					</div>
					<div class="row countriesOperation-alert" id="countriesOperation-alert" style="color:#B90D0F;">
						<div class="col-sm-8 offset-sm-4"><p class="font-italic small" id="countriesOperation-alert-msg"></p></div>
					</div>
					<div class="form-group row">
						<div class="col-sm-4">
							<label class="col-form-label" for="sourceFunds">Source of Funds</label>
						</div>
						<div class="col-sm-8">
							<textarea class="form-control form-bg-2 col-sm-12" name="sourceFunds" id="sourceFunds" rows="2" placeholder="In relation to business activities" onfocus="this.placeholder = ''" onblur="this.placeholder = 'In relation to business activities'"></textarea>
						</div>
					</div>

                            					
					<!-- start Role -->
					<div class="form-group row mt-10 mb-5">
						<h3>Particulars of all Directors, Shareholders and Beneficial Owners</h3>
					</div>
					
					<div class="form-group row">
						<div class="col-sm-4">
							<label class="col-form-label required" for="roleSelector">Role</label>
						</div>
						<div class="col-sm-8">
							<div class="selectdiv">
								<select id="roleSelector" name="roleSelector" data-placeholder="Please select..." class="form-bg-2 custom-select select-w-100" onchange="additionalselector(this,true);">
									<option value="" selected="selected">Please select</option>
									<option value="Director" rel="director">Director</option>
									<option value="Shareholder" rel="shareholder">Individual Shareholder only</option>
									<option value="Director and Shareholder" rel="directorshareholder">Director and Shareholder</option>
									<option value="Corporate Shareholder" rel="corporateshareholder">Corporate Shareholder</option>
								</select>
							</div>
						</div>
					</div>
					<div class="row roleSelector-alert" id="roleSelector-alert" style="color:#B90D0F;">
						<div class="col-sm-8 offset-sm-4"><p class="font-italic small" id="roleSelector-alert-msg"></p></div>
					</div>
                                         <?php //include ('includes/director.php');?>

                                          <?php //include ('includes/shareholder.php');?>

                                          <?php //include ('includes/directorshareholder.php');?>

                                          <?php //include ('includes/corporateshareholder.php');?>
		
                                         <!-- add additional role -->					
                                         <div id="firstrole"></div>	
                                         <div  id="additionalRole" style="display:none">
                                         <div class="form-group row mt-5 mb-10"> 
						<div class="col-sm-4">
							<label class="col-form-label required" for="addroleSelector">Add Role</label>
						</div>
						<div class="col-sm-8">
							<div class="selectdiv">
								<select id="addroleSelector" name="addroleSelector" onchange="additionalselector(this,false);" data-placeholder="Please select..." class="form-bg-2 custom-select select-w-100">
									<option value="" selected="selected">Please select</option>
                                                                        									<option value="Nomore" rel="director">No more additional role</option> 
									<option value="Director" rel="director">Director</option>
									<option value="Shareholder" rel="shareholder">Individual Shareholder only</option>
									<option value="Director and Shareholder" rel="directorshareholder">Director and Shareholder</option>
									<option value="Corporate Shareholder" rel="corporateshareholder">Corporate Shareholder</option>
								</select>
							</div>
						</div>
						<div class="row addRoleSelector-alert" id="addRoleSelector-alert" style="color:#B90D0F;">
							<div class="col-sm-8 offset-sm-4"><p class="font-italic small" id="addRoleSelector-alert-msg"></p></div>
						</div>
                                        </div>
                        
					</div>  
                                         <div class="form-group row mt-5 mb-10">					
						<div class="col-sm"> 
							<div class="text-center float-right">
								<!--<a href="javascript:void(0);" class="btn btn-next btn-lg" onclick="document.getElementById('form1').submit();">Next</a>-->
								<input type="submit" class="btn btn-next btn-lg" value="Next"/>
								<!-- <a class="btn btn-next btn-lg" href="declaration.html">Next</a> -->
							</div>
						</div>
					 </div>
					<div class="form-group row"><br/><br/><br/><br/><br/><br/><br/></div>
				</div>
			</div>
		</form>
	</div><!-- /.container -->
	
	
	
	<!-- Footer -->
	<footer class="site-footer pt-4 mt-4">
		<div class="container text-center text-md-left">
			<p class="text-center"><a href="https://www.incorp.asia/" rel="home">In.Corp Global Pte Ltd</a>. <span style="white-space: pre;">All rights reserved.</span><span style="margin: 0 8px;">·</span><a href="/privacy/">Privacy Policy</a><span style="margin: 0 8px;">·</span><a href="/terms/">Terms of Use</a><span style="margin: 0 8px;">·</span><a href="/sitemap/">Sitemap</a></p>
		</div><!-- /.container -->		
	</footer>
	
	
	<!-- Bootstrap core JavaScript -->	
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script src="js/chosen.jquery.js"></script>
	<script src="js/prism.js"></script>
	<script>
		var countries = [];
                var countriesOption;                  
                var yearfromOption;
                var yeartoOption; 
        
		$(function () {			

			countries = ['', 'Singapore','Malaysia','Indonesia', 'India', 'China','Hong Kong','Philippines','Thailand','Vietnam','United Arab Emirates','Afghanistan', 'Aland Islands', 'Albania', 'Algeria', 'American Samoa', 'Andorra', 'Angola', 'Anguilla', 'Antarctica', 'Antigua and Barbuda', 'Argentina', 'Armenia', 'Aruba', 'Australia', 'Austria', 'Azerbaijan', 'Bahamas', 'Bahrain', 'Bangladesh', 'Barbados', 'Belarus', 'Belgium', 'Belize', 'Benin', 'Bermuda', 'Bhutan', 'Bolivia, Plurinational State of', 'Bonaire, Sint Eustatius and Saba', 'Bosnia and Herzegovina', 'Botswana', 'Bouvet Island', 'Brazil', 'British Indian Ocean Territory', 'Brunei Darussalam', 'Bulgaria', 'Burkina Faso', 'Burundi', 'Cambodia', 'Cameroon', 'Canada', 'Cape Verde', 'Cayman Islands', 'Central African Republic', 'Chad', 'Chile', 'Christmas Island', 'Cocos (Keeling) Islands', 'Colombia', 'Comoros', 'Congo', 'Congo, The Democratic Republic of The', 'Cook Islands', 'Costa Rica', 'Cote D\'ivoire', 'Croatia', 'Cuba', 'Curacao', 'Cyprus', 'Czech Republic', 'Denmark', 'Djibouti', 'Dominica', 'Dominican Republic', 'Ecuador', 'Egypt', 'El Salvador', 'Equatorial Guinea', 'Eritrea', 'Estonia', 'Ethiopia', 'Falkland Islands (Malvinas)', 'Faroe Islands', 'Fiji', 'Finland', 'France', 'French Guiana', 'French Polynesia', 'French Southern Territories', 'Gabon', 'Gambia', 'Georgia', 'Germany', 'Ghana', 'Gibraltar', 'Greece', 'Greenland', 'Grenada', 'Guadeloupe', 'Guam', 'Guatemala', 'Guernsey', 'Guinea', 'Guinea-bissau', 'Guyana', 'Haiti', 'Heard Island and Mcdonald Islands', 'Holy See (Vatican City State)', 'Honduras',  'Hungary', 'Iceland', 'Iran, Islamic Republic of', 'Iraq', 'Ireland', 'Isle of Man', 'Israel', 'Italy', 'Jamaica', 'Japan', 'Jersey', 'Jordan', 'Kazakhstan', 'Kenya', 'Kiribati', 'Korea, Democratic People\'s Republic of', 'Korea, Republic of', 'Kuwait', 'Kyrgyzstan', 'Lao People\'s Democratic Republic', 'Latvia', 'Lebanon', 'Lesotho', 'Liberia', 'Libya', 'Liechtenstein', 'Lithuania', 'Luxembourg', 'Macao', 'Macedonia, The Former Yugoslav Republic of', 'Madagascar', 'Malawi',  'Maldives', 'Mali', 'Malta', 'Marshall Islands', 'Martinique', 'Mauritania', 'Mauritius', 'Mayotte', 'Mexico', 'Micronesia, Federated States of', 'Moldova, Republic of', 'Monaco', 'Mongolia', 'Montenegro', 'Montserrat', 'Morocco', 'Mozambique', 'Myanmar', 'Namibia', 'Nauru', 'Nepal', 'Netherlands', 'New Caledonia', 'New Zealand', 'Nicaragua', 'Niger', 'Nigeria', 'Niue', 'Norfolk Island', 'Northern Mariana Islands', 'Norway', 'Oman', 'Pakistan', 'Palau', 'Palestinian Territory, Occupied', 'Panama', 'Papua New Guinea', 'Paraguay', 'Peru', 'Pitcairn', 'Poland', 'Portugal', 'Puerto Rico', 'Qatar', 'Reunion', 'Romania', 'Russian Federation', 'Rwanda', 'Saint Barthelemy', 'Saint Helena, Ascension and Tristan da Cunha', 'Saint Kitts and Nevis', 'Saint Lucia', 'Saint Martin (French part)', 'Saint Pierre and Miquelon', 'Saint Vincent and The Grenadines', 'Samoa', 'San Marino', 'Sao Tome and Principe', 'Saudi Arabia', 'Senegal', 'Serbia', 'Seychelles', 'Sierra Leone', 'Sint Maarten (Dutch part)', 'Slovakia', 'Slovenia', 'Solomon Islands', 'Somalia', 'South Africa', 'South Georgia and The South Sandwich Islands', 'South Sudan', 'Spain', 'Sri Lanka', 'Sudan', 'Suriname', 'Svalbard and Jan Mayen', 'Swaziland', 'Sweden', 'Switzerland', 'Syrian Arab Republic', 'Taiwan, Province of China', 'Tajikistan', 'Tanzania, United Republic of',  'Timor-leste', 'Togo', 'Tokelau', 'Tonga', 'Trinidad and Tobago', 'Tunisia', 'Turkey', 'Turkmenistan', 'Turks and Caicos Islands', 'Tuvalu', 'Uganda', 'Ukraine', 'United Kingdom', 'United States', 'United States Minor Outlying Islands', 'Uruguay', 'Uzbekistan', 'Vanuatu', 'Venezuela, Bolivarian Republic of', 'Virgin Islands, British', 'Virgin Islands, U.S.', 'Wallis and Futuna', 'Western Sahara', 'Yemen', 'Zambia', 'Zimbabwe'];

                        
			
			countriesOption = countries.map(function(country){
				return '<option value="' + country + '">' + country + '</option>'
			});

                        years1 = ['','2018','2017','2016','2015','2014','2013','2012','2011','2010','2009','2008','2007','2006','2005','2004','2003','2002','2001','2000','1999','1998','1997','1996','1995','1994','1993','1992','1991','1990'];
                         
                        years = ['','1990','1991','1992','1993','1994','1995','1996','1997','1998','1999','2000','2001','2002','2003','2004','2005','2006','2007','2008','2009','2010','2011','2012','2013','2014','2015','2016','2017','2018']; 

                        yearfromOption = years.map(function(years){
				return '<option value="' + years + '">' + years + '</option>'
			});
                        

			yeartoOption = years1.map(function(years1){
				return '<option value="' + years1 + '">' + years1 + '</option>'
			});

                        $('.chosen-select-country').html(countriesOption);
			$('[data-toggle="tooltip"]').tooltip();
			$(".chosen-select").chosen();
			
			
			//proposed name
			$(document).on('click', '.add-more', function(e){
				e.preventDefault();
				var proposedCompanyItem = $('.proposed-company-item');

				//add new company name input
				if (proposedCompanyItem.length < 3) {
					$('.proposed-company-wrapper').append(proposedCompanyItem.get(0).outerHTML);
				}

				toggleCompanyDetails();
			});

			$(document).on('click', '.proposed-company-wrapper .remove-item', function(e){
				e.preventDefault();
				var $this = $(this);

				$this.parent('.proposed-company-item').remove();

				toggleCompanyDetails();
			});

			function toggleCompanyDetails() {
				var proposedCompanyItem = $('.proposed-company-item');
				var proposedCompanyAlert = $('.proposed-company-alert');
				var removeBtn = $('.proposed-company-wrapper .remove-item');

				//show or hide the alert of maximum proposed company name
				if (proposedCompanyItem.length == 3) {
					proposedCompanyAlert.removeClass('d-none');
				} else {
					proposedCompanyAlert.addClass('d-none');
				}

				//show or hide the remove button
				if (proposedCompanyItem.length == 1) {
					removeBtn.addClass('d-none');
				} else {
					removeBtn.removeClass('d-none');
				}
			}


                        //intended address toggle
			$(document).on('click', 'input[name="intendedAddress"]', function(e){
				if (this.value == "serviceProviderAddress") {
					$(".show-service-address").toggle();
					$(".show-own-address").hide();
				} else if (this.value == "ownAddress") {
					$(".show-own-address").toggle();
					$(".show-service-address").hide();
				} else {
					$(".show-service-address, .show-own-address").hide();
				}
			});


                        //Select role
			/*$(document).on('change', 'select[name="roleSelector"]', function(e){
				var rolesContent = $('.choose-role');
				var rel = $('option:selected', this).attr('rel');

				rolesContent.removeClass('active').hide();

				if (rel) {
					setTimeout(function(){
						$('#' + rel).addClass('active').show();
					}, 200);
				}
                $("#additionalRole").show(); 
			});*/


			
	

		});
		
	</script>

<?php include('includes/constants.php');?>	
<?php include('includes/formfields.php');?>
<?php include('includes/formvalidations.php');?>
<?php include('includes/formEventhandler.php');?>
<div class="modal fade" id="shareholdingchart" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg modal-dialog-centered" role="document">
			 <div class="container-fluid">
    			 <div class="row">
					<div class="col-lg-12">
						<div class="modal-content modal-shareholdingchart">
							<div class="modal-header">
								<h5 class="modal-title" id="exampleModalLabel">Sample Shareholding Chart for Multi-layer Structures</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
								<i class="fa fa-times-circle float-right" aria-hidden="true"></i>
								</button>
							</div>
							<div class="modal-body">
								<img src="images/chart.jpeg" alt="" class="img-fluid">
							</div>
						</div>
					</div>
				 </div>
			</div>
		</div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="displaysuccessmsg" style="position:fixed;top:23%;">
  <div class="modal-dialog modal-dialog-centered modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header" style="background-color:#bb2121">
        <h5 class="modal-title">Success</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Data has been saved Successfully.</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Ok</button>
      </div>
    </div>
  </div>
</div>



</body>

</html>
