<?php

ob_start(); 

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');
include("includes/constants.php");

require_once 'class.phpmailer.php';
$jsonMail = new PHPMailer();
$pdfMail = new PHPMailer();

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
//$pdf->SetAuthor('Nicola Asuni');
$pdf->SetTitle('In Corp - Rikvin');
//$pdf->SetSubject('TCPDF Tutorial');
//$pdf->SetKeywords('TCPDF, PDF, example, test, guide');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
$pdf->setFooterData(array(0,64,0), array(0,64,128));

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);


// set auto page breaks
//$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// convert TTF font to TCPDF format and store it on the fonts folder
//$fontname = TCPDF_FONTS::addTTFfont('../fonts/Roboto-Bold.ttf', 'TrueTypeUnicode', '', 96);
//$fontnameb = TCPDF_FONTS::addTTFfont('../fonts/Roboto-Bold.ttf', 'TrueTypeUnicode', '', 96);

// use the font
//$pdf->SetFont($fontname, '', 10, '', false);
//$pdf->SetFont($fontnameb, '', 10, '', false);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
//$pdf->SetFont('helvetica', '', 10, '', true);
$pdf->SetFont('roboto', '', 10);
//$pdf->SetFont('times', 'BI', 10);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage('P', 'A4');

// set text shadow effect
//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

//print_r($_POST);

$proposedCompanyName1 = $_POST['proposedCompanyName1'];
$proposedCompanyName2 = $_POST['proposedCompanyName2'];
$proposedCompanyName3 = $_POST['proposedCompanyName3'];
$intendedAddress = $_POST['intendedAddress'];
$streetName = $_POST['streetName'];
$floor = $_POST['floor'];
$unit = $_POST['unit'];
$postalCode = $_POST['postalCode'];
$businessActivities = $_POST['businessActivities'];
$countriesOperation = $_POST['countriesOperation'];
$sourceFunds = $_POST['sourceFunds'];
$address = $_POST['address'];
$counter = $_POST['counter'];
$counter1 = $_POST['counter1'];
$counter2 = $_POST['counter2'];
$counter3 = $_POST['counter3'];
$bocounter = $_POST['bocounter'];
$bodicounter = $_POST['bodicounter'];

//File to write JSON content and send to mail
$jsonformat = $_POST['jsonFormat'];
$filename = "json_".time().".txt";
file_put_contents($filename, $jsonformat);

if (!function_exists('set_magic_quotes_runtime')) {
    function set_magic_quotes_runtime($new_setting) {
        return true;
    }
}

$jsonMail->From = FROM_EMAIL_JSON;
$jsonMail->FromName = FROM_NAME_JSON;
$jsonMail->addAddress(TO_EMAIL1_JSON, TO_NAME1_JSON);
$jsonMail->addAddress(TO_EMAIL2_JSON, TO_NAME2_JSON);
$jsonMail->addCC(CC_EMAIL1_JSON,CC_NAME1_JSON);
$jsonMail->addBCC(BCC_EMAIL1_JSON, BCC_NAME1_JSON);

//Provide file path and name of the attachments
$jsonMail->addAttachment($filename, $filename);        
$jsonMail->isHTML(false);
$jsonMail->Subject = SUBJECT_JSON;
$jsonMail->Body = "Thank you for filling out (".$proposedCompanyName1.") information!. \n\n We’ve sent you an email with the information that you just filled at the email address. Our relationship manager will get over the details and will get back to you, if any more documents are required.";

if(!$jsonMail->send()) 
{
    echo "Mailer Error: " . $jsonMail->ErrorInfo;
} 
else 
{
    echo "Message has been sent successfully";
}


/* Director array vars */
$director_surname = array();
$director_resstatus = array();
$director_surname_preview = array();
$director_givenname_preview = array();
$director_name = array();
$director_gender_preview = array();
$director_passport_preview = array();
$director_nationality_preview = array();
$director_country_preview = array();
$director_expirydate_preview = array();
$director_dob_preview = array();
$director_gender_manual = array();
$director_passport_manual = array();
$director_nationality_manual = array();
$director_country_manual = array();
$director_dateissued_manual = array();
$director_expirydate_manual = array();
$director_birthplace_manual = array();
$director_dob_manual = array();
$director_nric_front_no = array();
$director_nric_front_gender = array();
$director_nric_front_dob = array();
$director_nric_front_nation = array();
$director_nric_front_postal = array();
$director_nric_front_street = array();
$director_nric_front_floor = array();
$director_nric_front_unit = array();
$director_nric_front_address = array();
$director_nric_back_no = array();
$director_nric_back_gender = array();
$director_nric_back_dob = array();
$director_nric_back_nation = array();
$director_nric_back_postal = array();
$director_nric_back_street = array();
$director_nric_back_floor = array();
$director_nric_back_unit = array();
$director_nric_no_manual = array();
$director_nric_gender_manual = array();
$director_nric_dob_manual = array();
$director_nric_nation_manual = array();
$director_nric_postal_manual = array();
$director_nric_street_manual = array();
$director_nric_floor_manual = array();
$director_nric_unit_manual = array();
$director_nric_address_manual = array();
$director_fin_surname_preview = array();
$director_fin_givenname_preview = array();
$director_fin_name = array();
$director_fin_passport_preview = array();
$director_fin_nationality_preview = array();
$director_fin_country_preview = array();
$director_fin_gender_preview = array();
$director_fin_expirydate_preview = array();
$director_fin_dob_preview = array();
$finAddress = array();
$director_fin_front_employer = array();
$director_fin_front_occupation = array();
$director_fin_front_issueddate = array();
$director_fin_front_expirydate = array();
$director_fin_front_finno = array();
$director_fin_passport_manual = array();
$director_fin_nationality_manual = array();
$director_fin_country_manual = array();
$director_fin_gender_manual = array();
$director_fin_issuedate_manual = array();
$director_fin_expirydate_manual = array();
$director_fin_birthplace_manual = array();
$director_fin_dob_manual = array();
$director_fin_employer_manual = array();
$director_fin_occupation_manual = array();
$director_fin_workpassissueddate_manual = array();
$director_fin_workpassexpirydate_manual = array();
$director_fin_finno_manual = array();
$handphone = array();
$officephone = array();
$businessemail = array();
$personalemail = array();
$directorpep = array();
$pepCountry = array();
$pepRole = array();
$pepFrom = array();
$pepTo = array();
$director_passport = array();
$director_addressProof = array();
$director_finPassport = array();
$director_finCardFront = array();
$director_finCardBack = array();
$director_finAddressProof = array();
$director_nricfront = array();
$director_nricback = array();

/* DirectorShareHolder vars */
$shareholder_surname = array();
$shareholder_resstatus = array();
$shareholder_bowner = array();
$shareholder_address = array();
$shareholder_finAddress = array();
$shareholder_surname_preview = array();
$shareholder_givenname_preview = array();

$shareholder_name = array();
$shareholder_gender_preview = array();
$shareholder_passport_preview = array();
$shareholder_nationality_preview = array();
$shareholder_country_preview = array();
$shareholder_expirydate_preview = array();
$shareholder_dob_preview = array();

$shareholder_gender_manual = array();
$shareholder_passport_manual = array();
$shareholder_nationality_manual = array();
$shareholder_country_manual = array();
$shareholder_dateissued_manual = array();
$shareholder_expirydate_manual = array();
$shareholder_birthplace_manual = array();
$shareholder_dob_manual = array();

$shareholder_nric_front_no = array();
$shareholder_nric_front_gender = array();
$shareholder_nric_front_dob = array();
$shareholder_nric_front_nation = array();
$shareholder_nric_front_postal = array();
$shareholder_nric_front_street = array();
$shareholder_nric_front_floor = array();
$shareholder_nric_front_unit = array();
$shareholder_nric_front_address = array();

$shareholder_nric_back_no = array();
$shareholder_nric_back_gender = array();
$shareholder_nric_back_dob = array();
$shareholder_nric_back_nation = array();
$shareholder_nric_back_postal = array();
$shareholder_nric_back_street = array();
$shareholder_nric_back_floor = array();
$shareholder_nric_back_unit = array();

$shareholder_nric_no_manual = array();
$shareholder_nric_gender_manual = array();
$shareholder_nric_dob_manual = array();
$shareholder_nric_nation_manual = array();
$shareholder_nric_postal_manual = array();
$shareholder_nric_street_manual = array();
$shareholder_nric_floor_manual = array();
$shareholder_nric_unit_manual = array();
$shareholder_nric_address_manual = array();

$shareholder_fin_surname_preview = array();
$shareholder_fin_givenname_preview = array();

$shareholder_fin_name = array();

$shareholder_fin_passport_preview = array();
$shareholder_fin_nationality_preview = array();
$shareholder_fin_country_preview = array();
$shareholder_fin_gender_preview = array();
$shareholder_fin_expirydate_preview = array();
$shareholder_fin_dob_preview = array();
$shareholder_finAddress = array();

$shareholder_fin_front_employer = array();
$shareholder_fin_front_occupation = array();
$shareholder_fin_front_issueddate = array();
$shareholder_fin_front_expirydate = array();
$shareholder_fin_front_finno = array();

$shareholder_fin_passport_manual = array();
$shareholder_fin_nationality_manual = array();
$shareholder_fin_country_manual = array();
$shareholder_fin_gender_manual = array();
$shareholder_fin_issuedate_manual = array();
$shareholder_fin_expirydate_manual = array();
$shareholder_fin_birthplace_manual = array();
$shareholder_fin_dob_manual = array();

$shareholder_fin_employer_manual = array();
$shareholder_fin_occupation_manual = array();
$shareholder_fin_workpassissueddate_manual = array();
$shareholder_fin_workpassexpirydate_manual = array();
$shareholder_fin_finno_manual = array();

$shareholderhandphone = array();
$shareholderOfficephone = array();
$shareholderbusinessemail = array();
$shareholderPersonalemail = array();
$shareholderpep = array();
$shareholdercountrypep = array();
$shareholderrolePep = array();
$shareholderpepFrom = array();
$shareholderpepTo = array();

$shareholder_nricpicfront = array();
$shareholder_nricpicback = array();

$shareholdernonresidentuploadpassport = array();
$shareholdernonresidentaddress = array();
$shareholderFINuploadPassport = array();
$shareholderfincardfront = array();
$shareholderfincardback = array();
$shareholderFINaddress = array();
$shareholderSGnricfront = array();
$shareholderSGnricback = array();

$di_surname = array();
$di_resstatus = array();
$di_bowner = array();
$di_address = array();
$di_finAddress = array();

$di_surname_preview = array();
$di_givenname_preview = array();

$di_name = array();
$di_gender_preview = array();
$di_passport_preview = array();
$di_nationality_preview = array();
$di_country_preview = array();
$di_expirydate_preview = array();
$di_dob_preview = array();

$di_gender_manual = array();
$di_passport_manual = array();
$di_nationality_manual = array();
$di_country_manual = array();
$di_dateissued_manual = array();
$di_expirydate_manual = array();
$di_birthplace_manual = array();
$di_dob_manual = array();

$di_nric_front_no = array();
$di_nric_front_gender = array();
$di_nric_front_dob = array();
$di_nric_front_nation = array();
$di_nric_front_postal = array();
$di_nric_front_street = array();
$di_nric_front_floor = array();
$di_nric_front_unit = array();
$di_nric_front_address = array();

$di_nric_back_no = array();
$di_nric_back_gender = array();
$di_nric_back_dob = array();
$di_nric_back_nation = array();
$di_nric_back_postal = array();
$di_nric_back_street = array();
$di_nric_back_floor = array();
$di_nric_back_unit = array();

$di_nric_no_manual = array();
$di_nric_gender_manual = array();
$di_nric_dob_manual = array();
$di_nric_nation_manual = array();
$di_nric_postal_manual = array();
$di_nric_street_manual = array();
$di_nric_floor_manual = array();
$di_nric_unit_manual = array();
$di_nric_address_manual = array();

$di_fin_surname_preview = array();
$di_fin_givenname_preview = array();

$di_fin_name = array();

$di_fin_passport_preview = array();
$di_fin_nationality_preview = array();
$di_fin_country_preview = array();
$di_fin_gender_preview = array();
$di_fin_expirydate_preview = array();
$di_fin_dob_preview = array();
$di_finAddress = array();

$di_fin_front_employer = array();
$di_fin_front_occupation = array();
$di_fin_front_issueddate = array();
$di_fin_front_expirydate = array();
$di_fin_front_finno = array();

$di_fin_passport_manual = array();
$di_fin_nationality_manual = array();
$di_fin_country_manual = array();
$di_fin_gender_manual = array();
$di_fin_issuedate_manual = array();
$di_fin_expirydate_manual = array();
$di_fin_birthplace_manual = array();
$di_fin_dob_manual = array();

$di_fin_employer_manual = array();
$di_fin_occupation_manual = array();
$di_fin_workpassissueddate_manual = array();
$di_fin_workpassexpirydate_manual = array();
$di_fin_finno_manual = array();

$dihandphone = array();
$diOfficephone = array();
$dibusinessemail = array();
$diPersonalemail = array();
$dipep = array();
$dicountrypep = array();
$dirolePep = array();
$dipepFrom = array();
$dipepTo = array();

$dishareholdernonresidentuploadpassport = array();
$dishareholdernonresidentaddress = array();
$dishareholderFINuploadPassport = array();
$dishareholderfincardfront = array();
$dishareholderfincardback = array();
$dishareholderFINaddress = array();
$dishareholderSGnricfront = array();
$dishareholderSGnricback = array();

$corporate_surname = array();
$corporate_country = array();
$corporate_contact = array();
$corporate_email = array();
$corporate_address = array();
$corporate_resaddress = array();
$corporate_finAddress = array();
$corporate_resstatus = array();
$corporate_certificate = array();
$corporate_certificate1 = array();
$corporate_representname = array();

$corporate_surname_preview = array();
$corporate_givenname_preview = array();

$corporate_name = array();
$corporate_gender_preview = array();
$corporate_passport_preview = array();
$corporate_nationality_preview = array();
$corporate_country_preview = array();
$corporate_expirydate_preview = array();
$corporate_dob_preview = array();
$corporate_birthplace_preview = array();

$corporate_gender_manual = array();
$corporate_passport_manual = array();
$corporate_nationality_manual = array();
$corporate_country_manual = array();
$corporate_dateissued_manual = array();
$corporate_expirydate_manual = array();
$corporate_birthplace_manual = array();
$corporate_dob_manual = array();

$corporate_nric_front_no = array();
$corporate_nric_front_gender = array();
$corporate_nric_front_dob = array();
$corporate_nric_front_nation = array();
$corporate_nric_front_postal = array();
$corporate_nric_front_street = array();
$corporate_nric_front_floor = array();
$corporate_nric_front_unit = array();
$corporate_nric_front_address = array();

$corporate_nric_back_no = array();
$corporate_nric_back_gender = array();
$corporate_nric_back_dob = array();
$corporate_nric_back_nation = array();
$corporate_nric_back_postal = array();
$corporate_nric_back_street = array();
$corporate_nric_back_floor = array();
$corporate_nric_back_unit = array();

$corporate_nric_no_manual = array();
$corporate_nric_gender_manual = array();
$corporate_nric_dob_manual = array();
$corporate_nric_nation_manual = array();
$corporate_nric_postal_manual = array();
$corporate_nric_street_manual = array();
$corporate_nric_floor_manual = array();
$corporate_nric_unit_manual = array();
$corporate_nric_address_manual = array();

$corporate_fin_surname_preview = array();
$corporate_fin_givenname_preview = array();
$corporate_fin_name = array();
$corporate_fin_passport_preview = array();
$corporate_fin_nationality_preview = array();
$corporate_fin_country_preview = array();
$corporate_fin_gender_preview = array();
$corporate_fin_expirydate_preview = array();
$corporate_fin_dob_preview = array();
$corporate_finresAddress = array();

$corporate_fin_front_employer = array();
$corporate_fin_front_occupation = array();
$corporate_fin_front_issueddate = array();
$corporate_fin_front_expirydate = array();
$corporate_fin_front_finno = array();

$corporate_fin_passport_manual = array();
$corporate_fin_nationality_manual = array();
$corporate_fin_country_manual = array();
$corporate_fin_gender_manual = array();
$corporate_fin_issuedate_manual = array();
$corporate_fin_expirydate_manual = array();
$corporate_fin_birthplace_manual = array();
$corporate_fin_dob_manual = array();
$corporate_fin_employer_manual = array();
$corporate_fin_occupation_manual = array();
$corporate_fin_workpassissueddate_manual = array();
$corporate_fin_workpassexpirydate_manual = array();
$corporate_fin_finno_manual = array();

$director_paid_up_capital = array();
$director_number_of_shares = array();
$individual_shareholder_paid_up_capital = array();
$individual_shareholder_number_of_shares = array();
$director_shareholder_paid_up_capital = array();
$director_shareholder_number_of_shares = array();
$corporate_paid_up_capital = array();
$corporate_number_of_shares = array();

$CorporateCertificate = array();
$CorporateCertificate1 = array();
$corporatenonresidentuploadpassport = array();
$corporatenonresidentaddress = array();
$corporateFINuploadPassport = array();
$corporatefincardfront = array();
$corporatefincardback = array();
$corporateFINaddress = array();
$corporateSGnricfront = array();
$corporateSGnricback = array();

for($c = 0;$c < $counter; $c++){

	$director_surname[$c] = ucwords($_POST['directorSurname'.($c+1)]);
	//directornonresident_1 directorcitizenpr_1  directorpassholder_1
	$director_resstatus[$c] = $_POST['directorresstatus'.($c+1)];

	//director 1 passport preview details
	$director_surname_preview[$c] = $_POST['directornonresidentpopupsurname'.($c+1)];
	$director_givenname_preview[$c] = $_POST['directornonresidentpopupgivenName'.($c+1)];

	$director_name[$c] = $director_surname_preview[$c].' '.$director_givenname_preview[$c];
	$director_gender_preview[$c] = $_POST['directornonresidentpopupgender'.($c+1)];
	if($director_gender_preview[$c] == 'on'){
		$director_gender_preview[$c] = 'Male';
	}else{
		$director_gender_preview[$c] = 'Female';
	}
	$director_passport_preview[$c] = $_POST['directornonresidentpopuppassportno'.($c+1)];
	$director_nationality_preview[$c] = $_POST['directornonresidentpopupnationality'.($c+1)];
	$director_country_preview[$c] = $_POST['directornonresidentpopupcountry'.($c+1)];
	$director_expirydate_preview[$c] = $_POST['directornonresidentpopupdateexpiry'.($c+1)];
	$director_dob_preview[$c] = $_POST['directornonresidentpopupdob'.($c+1)];

	//director 1 passport manual details
	$director_gender_manual[$c] = $_POST['directornonmanuallygender'.($c+1)];
	if($director_gender_manual[$c] == 'on'){
		$director_gender_manual[$c] = 'Male';
	}else{
		$director_gender_manual[$c] = 'Female';
	}
	$director_passport_manual[$c] = $_POST['directornonmanuallypassportno'.($c+1)];
	$director_nationality_manual[$c] = $_POST['directornonmanuallynationality'.($c+1)];
	$director_country_manual[$c] = $_POST['directornonmanuallycountry'.($c+1)];
	$director_dateissued_manual[$c] = $_POST['directornonmanuallydatetimepicker'.($c+1)];
	$director_expirydate_manual[$c] = $_POST['directornonmanuallydateexpiry'.($c+1)];
	$director_birthplace_manual[$c] = $_POST['directornonmanuallyplaceofBirth'.($c+1)];
	$director_dob_manual[$c] = $_POST['directornonmanuallydateBirth'.($c+1)];

	//director 1 nric front details
	$director_nric_front_no[$c] = $_POST['directorpreviewpopupfrontnricnumber'.($c+1)];
	$director_nric_front_gender[$c] = $_POST['directorpreviewpopupnricfrontgender'.($c+1)];
	if($director_nric_front_gender[$c] == 'on'){
		$director_nric_front_gender[$c] = 'Male';
	}else{
		$director_nric_front_gender[$c] = 'Female';
	}
	$director_nric_front_dob[$c] = $_POST['directorpreviewpopupnricfrontdob'.($c+1)];
	$director_nric_front_nation[$c] = $_POST['directorpreviewpopupnricfrontnationality'.($c+1)];
	$director_nric_front_postal[$c] = $_POST['directorpreviewpopupnricfrontpostcode'.($c+1)];
	$director_nric_front_street[$c] = $_POST['directorpreviewpopupnricfrontstreetname'.($c+1)];
	$director_nric_front_floor[$c] = $_POST['directorpreviewpopupnricfrontfloor'.($c+1)];
	$director_nric_front_unit[$c] = $_POST['directorpreviewnrpopupicfrontunit'.($c+1)];
	$director_nric_front_address[$c] = $director_nric_front_street[$c].' <br>'.$director_nric_front_floor[$c].' '.$director_nric_front_unit[$c].' <br>'.$director_nric_front_postal[$c];

	//director 1 nric back details
	$director_nric_back_no[$c] = $_POST['directorpreviewpopupbacknricnumber'.($c+1)];
	$director_nric_back_gender[$c] = $_POST['directorpreviewpopupnricbackgender'.($c+1)];
	$director_nric_back_dob[$c] = $_POST['directorpreviewpopupnricbackdob'.($c+1)];
	$director_nric_back_nation[$c] = $_POST['directorpreviewpopupnricbacknationality'.($c+1)];
	$director_nric_back_postal[$c] = $_POST['directorpreviewpopupnricbackpostcode'.($c+1)];
	$director_nric_back_street[$c] = $_POST['directorpreviewpopupnricbackstreetname'.($c+1)];
	$director_nric_back_floor[$c] = $_POST['directorpreviewpopupnricbackfloor'.($c+1)];
	$director_nric_back_unit[$c] = $_POST['directorpreviewnrpopupicbackunit'.($c+1)];

	//director 1 nric manual details
	$director_nric_no_manual[$c] = $_POST['directorpreviewmanuallynricnumber'.($c+1)];
	$director_nric_gender_manual[$c] = $_POST['directorpreviewmanuallynricgender'.($c+1)];
	if($director_nric_gender_manual[$c] == 'on'){
		$director_nric_gender_manual[$c] = 'Male';
	}else{
		$director_nric_gender_manual[$c] = 'Female';
	}
	$director_nric_dob_manual[$c] = $_POST['directorpreviewmanuallynricdob'.($c+1)];
	$director_nric_nation_manual[$c] = $_POST['directorpreviewmanuallynricnationality'.($c+1)];
	$director_nric_postal_manual[$c] = $_POST['directorpreviewmanuallynricpostcode'.($c+1)];
	$director_nric_street_manual[$c] = $_POST['directorpreviewmanuallynricstreetname'.($c+1)];
	$director_nric_floor_manual[$c] = $_POST['directorpreviewmanuallynricfloor'.($c+1)];
	$director_nric_unit_manual[$c] = $_POST['directorpreviewnrmanuallyicunit'.($c+1)];
	$director_nric_address_manual[$c] = $director_nric_street_manual[$c].' <br>'.$director_nric_floor_manual[$c].' '.$director_nric_unit_manual[$c].' <br>'.$director_nric_postal_manual[$c];

	//director 1 fin popup details
	$director_fin_surname_preview[$c] = $_POST['directorfincardpopupsurname'.($c+1)];
	$director_fin_givenname_preview[$c] = $_POST['directorfincardpopupgivenName'.($c+1)];

	$director_fin_name[$c] = $director_fin_surname_preview[$c].' '.$director_fin_givenname_preview[$c];

	$director_fin_passport_preview[$c] = $_POST['directorfincardpopuppassportno'.($c+1)];
	$director_fin_nationality_preview[$c] = $_POST['directorfincardpopupnationality'.($c+1)];
	$director_fin_country_preview[$c] = $_POST['directorfincardpopupcountry'.($c+1)];
	$director_fin_gender_preview[$c] = $_POST['directorfincardpopupgender'.($c+1)];
	if($director_fin_gender_preview[$c] == 'on'){
		$director_fin_gender_preview[$c] = 'Male';
	}else{
		$director_fin_gender_preview[$c] = 'Female';
	}
	$director_fin_expirydate_preview[$c] = $_POST['directorfincardpopupdateexpiry'.($c+1)];
	$director_fin_dob_preview[$c] = $_POST['directorfincardpopupdob'.($c+1)];
	$finAddress[$c] = $_POST['finAddress'.($c+1)];

	//director 1 fin front details
	$director_fin_front_employer[$c] = $_POST['directorfincardpopupfrontemployer'.($c+1)];
	$director_fin_front_occupation[$c] = $_POST['directorfincardpopupfrontoccupation'.($c+1)];
	$director_fin_front_issueddate[$c] = $_POST['directorfincardpopupfrontdateissue'.($c+1)];
	$director_fin_front_expirydate[$c] = $_POST['directorfincardpopupfrontdateexpiry'.($c+1)];
	$director_fin_front_finno[$c] = $_POST['directorwfincardpopupfrontNumber'.($c+1)];

	//director 1 fin manual details
	$director_fin_passport_manual[$c] = $_POST['directorfincardmanuallypassportno'.($c+1)];
	$director_fin_nationality_manual[$c] = $_POST['directorfincardmanuallynationality'.($c+1)];
	$director_fin_country_manual[$c] = $_POST['directorfincardmanuallycountry'.($c+1)];
	$director_fin_gender_manual[$c] = $_POST['directorfincardmanuallygender'.($c+1)];
	if($director_fin_gender_manual[$c] == 'on'){
		$director_fin_gender_manual[$c] = 'Male';
	}else{
		$director_fin_gender_manual[$c] = 'Female';
	}
	$director_fin_issuedate_manual[$c] = $_POST['directorfincardmanuallydatetimepicker'.($c+1)];
	$director_fin_expirydate_manual[$c] = $_POST['directorfincardmanuallydateexpiry'.($c+1)];
	$director_fin_birthplace_manual[$c] = $_POST['directorfincardmanuallyplaceofBirth'.($c+1)];
	$director_fin_dob_manual[$c] = $_POST['directorfincardmanuallydateBirth'.($c+1)];

	$director_fin_employer_manual[$c] = $_POST['directorfincardmanuallyemployer'.($c+1)];
	$director_fin_occupation_manual[$c] = $_POST['directorfincardmanuallyoccupation'.($c+1)];
	$director_fin_workpassissueddate_manual[$c] = $_POST['directorfincardmanuallyworkpassdateissue'.($c+1)];
	$director_fin_workpassexpirydate_manual[$c] = $_POST['directorfincardmanuallyworkpassdateexpiry'.($c+1)];
	$director_fin_finno_manual[$c] = $_POST['directorfincardmanuallywfincardnumber'.($c+1)];

	//contact details
	$handphone[$c] = $_POST['handphone'.($c+1)];
	$officephone[$c] = $_POST['officephone'.($c+1)];
	$businessemail[$c] = $_POST['businessemail'.($c+1)];
	$personalemail[$c] = $_POST['personalemail'.($c+1)];
	$directorpep[$c] = $_POST['directorpep'.($c+1)];
	if($directorpep[$c] == 'directorpepyes_'.($c+1)){
		$directorpep[$c] = 'Yes';
	}else{
		$directorpep[$c] = 'No';
	}
	$pepCountry[$c] = $_POST['pepCountry'.($c+1)];
	$pepRole[$c] = $_POST['pepRole'.($c+1)];
	$pepFrom[$c] = $_POST['pepFrom'.($c+1)];
	$pepTo[$c] = $_POST['pepTo'.($c+1)];

	$director_passport[$c] = $_POST["directornonresidentuploadpassport".($c+1)];
	$director_addressProof[$c] = $_POST["directornonresidentaddress".($c+1)];
	$director_finPassport[$c] = $_POST["directorFINuploadPassport".($c+1)];
	$director_finCardFront[$c] = $_POST["directorfincardfront".($c+1)];
	$director_finCardBack[$c] = $_POST["directorfincardback".($c+1)];
	$director_finAddressProof[$c] = $_POST["directorFINaddress".($c+1)];
	$director_nricfront[$c] = $_POST["directorSGnricfront".($c+1)];
	$director_nricback[$c] = $_POST["directorSGnricback".($c+1)];

	$director_paid_up_capital[$c] = $_POST['director_paid_up_capital'.($c+1)];
	$director_number_of_shares[$c] = $_POST['director_number_of_shares'.($c+1)];

}

//==============share holders details starts here===================//

for($c = 0;$c < $counter1; $c++){

	$shareholder_surname[$c] = ucwords($_POST['shareholdersurname'.($c+1)]);
	$shareholder_resstatus[$c] = $_POST['shareholderresstatus'.($c+1)];
	$shareholder_bowner[$c] = $_POST['shareholderbowner'.($c+1)];
	$shareholder_address[$c] = $_POST['shareholdernonresidentresaddress'.($c+1)];
	$shareholder_finAddress[$c] = $_POST['shareholderFINaddress'.($c+1)];	

	//shareholder 1 passport preview details
	$shareholder_surname_preview[$c] = $_POST['shareholdernonresidentpopupsurname'.($c+1)];
	$shareholder_givenname_preview[$c] = $_POST['shareholdernonresidentpopupgivenName'.($c+1)];

	$shareholder_name[$c] = $shareholder_surname_preview[$c].' '.$shareholder_givenname_preview[$c];
	$shareholder_gender_preview[$c] = $_POST['shareholdernonresidentpopupgender'.($c+1)];
	if($shareholder_gender_preview[$c] == 'on'){
		$shareholder_gender_preview[$c] = 'Male';
	}else{
		$shareholder_gender_preview[$c] = 'Female';
	}	
	
	$shareholder_passport_preview[$c] = $_POST['shareholdernonresidentpopuppassportno'.($c+1)];
	$shareholder_nationality_preview[$c] = $_POST['shareholdernonresidentpopupnationality'.($c+1)];
	$shareholder_country_preview[$c] = $_POST['shareholdernonresidentpopupcountry'.($c+1)];
	$shareholder_expirydate_preview[$c] = $_POST['shareholdernonresidentpopupdateexpiry'.($c+1)];
	$shareholder_dob_preview[$c] = $_POST['shareholdernonresidentpopupdob'.($c+1)];

	//shareholder 1 passport manual details
	$shareholder_gender_manual[$c] = $_POST['shareholdernonmanuallygender'.($c+1)];
	if($shareholder_gender_manual[$c] == 'on'){
		$shareholder_gender_manual[$c] = 'Male';
	}else{
		$shareholder_gender_manual[$c] = 'Female';
	}
	$shareholder_passport_manual[$c] = $_POST['shareholdernonmanuallypassportno'.($c+1)];
	$shareholder_nationality_manual[$c] = $_POST['shareholdernonmanuallynationality'.($c+1)];
	$shareholder_country_manual[$c] = $_POST['shareholdernonmanuallycountry'.($c+1)];
	$shareholder_dateissued_manual[$c] = $_POST['shareholdernonmanuallydatetimepicker'.($c+1)];
	$shareholder_expirydate_manual[$c] = $_POST['shareholdernonmanuallydateexpiry'.($c+1)];
	$shareholder_birthplace_manual[$c] = $_POST['shareholdernonmanuallyplaceofBirth'.($c+1)];
	$shareholder_dob_manual[$c] = $_POST['shareholdernonmanuallydateBirth'.($c+1)];

	//shareholder 1 nric front details
	$shareholder_nric_front_no[$c] = $_POST['shareholderpreviewpopupfrontnricnumber'.($c+1)];
	$shareholder_nric_front_gender[$c] = $_POST['shareholderpreviewpopupnricfrontgender'.($c+1)];
	if($shareholder_nric_front_gender[$c] == 'on'){
		$shareholder_nric_front_gender[$c] = 'Male';
	}else{
		$shareholder_nric_front_gender[$c] = 'Female';
	}
	$shareholder_nric_front_dob[$c] = $_POST['shareholderpreviewpopupnricfrontdob'.($c+1)];
	$shareholder_nric_front_nation[$c] = $_POST['shareholderpreviewpopupnricfrontnationality'.($c+1)];
	$shareholder_nric_front_postal[$c] = $_POST['shareholderpreviewpopupnricfrontpostcode'.($c+1)];
	$shareholder_nric_front_street[$c] = $_POST['shareholderpreviewpopupnricfrontstreetname'.($c+1)];
	$shareholder_nric_front_floor[$c] = $_POST['shareholderpreviewpopupnricfrontfloor'.($c+1)];
	$shareholder_nric_front_unit[$c] = $_POST['shareholderpreviewnrpopupicfrontunit'.($c+1)];
	$shareholder_nric_front_address[$c] = $shareholder_nric_front_street[$c].' <br>'.$shareholder_nric_front_floor[$c].' '.$shareholder_nric_front_unit[$c].' <br>'.$shareholder_nric_front_postal[$c];

	//shareholder 1 nric back details
	$shareholder_nric_back_no[$c] = $_POST['shareholderpreviewpopupbacknricnumber'.($c+1)];
	$shareholder_nric_back_gender[$c] = $_POST['shareholderpreviewpopupnricbackgender'.($c+1)];
	$shareholder_nric_back_dob[$c] = $_POST['shareholderpreviewpopupnricbackdob'.($c+1)];
	$shareholder_nric_back_nation[$c] = $_POST['shareholderpreviewpopupnricbacknationality'.($c+1)];
	$shareholder_nric_back_postal[$c] = $_POST['shareholderpreviewpopupnricbackpostcode'.($c+1)];
	$shareholder_nric_back_street[$c] = $_POST['shareholderpreviewpopupnricbackstreetname'.($c+1)];
	$shareholder_nric_back_floor[$c] = $_POST['shareholderpreviewpopupnricbackfloor'.($c+1)];
	$shareholder_nric_back_unit[$c] = $_POST['shareholderpreviewnrpopupicbackunit'.($c+1)];

	//shareholder 1 nric manual details
	$shareholder_nric_no_manual[$c] = $_POST['shareholderpreviewmanuallynricnumber'.($c+1)];
	$shareholder_nric_gender_manual[$c] = $_POST['shareholderpreviewmanuallynricgender'.($c+1)];
	if($shareholder_nric_gender_manual[$c] == 'on'){
		$shareholder_nric_gender_manual[$c] = 'Male';
	}else{
		$shareholder_nric_gender_manual[$c] = 'Female';
	}
	$shareholder_nric_dob_manual[$c] = $_POST['shareholderpreviewmanuallynricdob'.($c+1)];
	$shareholder_nric_nation_manual[$c] = $_POST['shareholderpreviewmanuallynricnationality'.($c+1)];
	$shareholder_nric_postal_manual[$c] = $_POST['shareholderpreviewmanuallynricpostcode'.($c+1)];
	$shareholder_nric_street_manual[$c] = $_POST['shareholderpreviewmanuallynricstreetname'.($c+1)];
	$shareholder_nric_floor_manual[$c] = $_POST['shareholderpreviewmanuallynricfloor'.($c+1)];
	$shareholder_nric_unit_manual[$c] = $_POST['shareholderpreviewnrmanuallyicunit'.($c+1)];
	$shareholder_nric_address_manual[$c] = $shareholder_nric_street_manual[$c].' <br>'.$shareholder_nric_floor_manual[$c].' '.$shareholder_nric_unit_manual[$c].' <br>'.$shareholder_nric_postal_manual[$c];

	//shareholder 1 fin popup details
	$shareholder_fin_surname_preview[$c] = $_POST['shareholderfincardpopupsurname'.($c+1)];
	$shareholder_fin_givenname_preview[$c] = $_POST['shareholderfincardpopupgivenName'.($c+1)];

	$shareholder_fin_name[$c] = $shareholder_fin_surname_preview[$c].' '.$shareholder_fin_givenname_preview[$c];

	$shareholder_fin_passport_preview[$c] = $_POST['shareholderfincardpopuppassportno'.($c+1)];
	$shareholder_fin_nationality_preview[$c] = $_POST['shareholderfincardpopupnationality'.($c+1)];
	$shareholder_fin_country_preview[$c] = $_POST['shareholderfincardpopupcountry'.($c+1)];
	$shareholder_fin_gender_preview[$c] = $_POST['shareholderfincardpopupgender'.($c+1)];
	if($shareholder_fin_gender_preview[$c] == 'on'){
		$shareholder_fin_gender_preview[$c] = 'Male';
	}else{
		$shareholder_fin_gender_preview[$c] = 'Female';
	}
	$shareholder_fin_expirydate_preview[$c] = $_POST['shareholderfincardpopupdateexpiry'.($c+1)];
	$shareholder_fin_dob_preview[$c] = $_POST['shareholderfincardpopupdob'.($c+1)];
	$shareholder_finAddress[$c] = $_POST['shareholderFINresaddress'.($c+1)];

	//shareholder 1 fin front details
	$shareholder_fin_front_employer[$c] = $_POST['shareholderfincardfrontemployer'.($c+1)];
	$shareholder_fin_front_occupation[$c] = $_POST['shareholderfincardfrontoccupation'.($c+1)];
	$shareholder_fin_front_issueddate[$c] = $_POST['shareholderfincardfrontdateissue'.($c+1)];
	$shareholder_fin_front_expirydate[$c] = $_POST['shareholderfincardfrontdateexpiry'.($c+1)];
	$shareholder_fin_front_finno[$c] = $_POST['shareholderwfincardfrontNumber'.($c+1)];

	//shareholder 1 fin manual details
	$shareholder_fin_passport_manual[$c] = $_POST['shareholderfincardmanuallypassportno'.($c+1)];
	$shareholder_fin_nationality_manual[$c] = $_POST['shareholderfincardmanuallynationality'.($c+1)];
	$shareholder_fin_country_manual[$c] = $_POST['shareholderfincardmanuallycountry'.($c+1)];
	$shareholder_fin_gender_manual[$c] = $_POST['shareholderfincardmanuallygender'.($c+1)];
	if($shareholder_fin_gender_manual[$c] == 'on'){
		$shareholder_fin_gender_manual[$c] = 'Male';
	}else{
		$shareholder_fin_gender_manual[$c] = 'Female';
	}
	$shareholder_fin_issuedate_manual[$c] = $_POST['shareholderfincardmanuallydatetimepicker'.($c+1)];
	$shareholder_fin_expirydate_manual[$c] = $_POST['shareholderfincardmanuallydateexpiry'.($c+1)];
	$shareholder_fin_birthplace_manual[$c] = $_POST['shareholderfincardmanuallyplaceofBirth'.($c+1)];
	$shareholder_fin_dob_manual[$c] = $_POST['shareholderfincardmanuallydateBirth'.($c+1)];

	$shareholder_fin_employer_manual[$c] = $_POST['shareholderfincardmanuallyemployer'.($c+1)];
	$shareholder_fin_occupation_manual[$c] = $_POST['shareholderfincardmanuallyoccupation'.($c+1)];
	$shareholder_fin_workpassissueddate_manual[$c] = $_POST['shareholderfincardmanuallyworkpassdateissue'.($c+1)];
	$shareholder_fin_workpassexpirydate_manual[$c] = $_POST['shareholderfincardmanuallyworkpassdateexpiry'.($c+1)];
	$shareholder_fin_finno_manual[$c] = $_POST['shareholderfincardmanuallywfincardnumber'.($c+1)];

	//contact details
	$shareholderhandphone[$c] = $_POST['shareholderhandphone'.($c+1)];
	$shareholderOfficephone[$c] = $_POST['shareholderOfficephone'.($c+1)];
	$shareholderbusinessemail[$c] = $_POST['shareholderbusinessemail'.($c+1)];
	$shareholderPersonalemail[$c] = $_POST['shareholderPersonalemail'.($c+1)];
	$shareholderpep[$c] = $_POST['shareholderpep'.($c+1)];
	if($shareholderpep[$c] == 'shareholderpepyes_'.($c+1)){
		$shareholderpep[$c] = 'Yes';
	}else{
		$shareholderpep[$c] = 'No';
	}
	$shareholdercountrypep[$c] = $_POST['shareholdercountrypep'.($c+1)];
	$shareholderrolePep[$c] = $_POST['shareholderrolePep'.($c+1)];
	$shareholderpepFrom[$c] = $_POST['shareholderpepFrom'.($c+1)];
	$shareholderpepTo[$c] = $_POST['shareholderpepTo'.($c+1)];

	$shareholder_nricpicfront[$c] = $_POST['shareholderSGnricfront'.($c+1)];
	$shareholder_nricpicback[$c] = $_POST['shareholderSGnricback'.($c+1)];

	$individual_shareholder_paid_up_capital[$c] = $_POST['individual_shareholder_paid_up_capital'.($c+1)];
	$individual_shareholder_number_of_shares[$c] = $_POST['individual_shareholder_number_of_shares'.($c+1)];

	$shareholdernonresidentuploadpassport[$c] = $_POST["shareholdernonresidentuploadpassport".($c+1)];
	$shareholdernonresidentaddress[$c] = $_POST["shareholdernonresidentaddress".($c+1)];
	$shareholderFINuploadPassport[$c] = $_POST["shareholderFINuploadPassport".($c+1)];
	$shareholderfincardfront[$c] = $_POST["shareholderfincardfront".($c+1)];
	$shareholderfincardback[$c] = $_POST["shareholderfincardback".($c+1)];
	$shareholderFINaddress[$c] = $_POST["shareholderFINaddress".($c+1)];
	$shareholderSGnricfront[$c] = $_POST["shareholderSGnricfront".($c+1)];
	$shareholderSGnricback[$c] = $_POST["shareholderSGnricback".($c+1)];

}

//==============director ad share holders details starts here===================//

for($c = 0;$c < $counter2; $c++){

	$di_surname[$c] = ucwords($_POST['dishareholdersurname'.($c+1)]);
	$di_resstatus[$c] = $_POST['dishareholderresstatus'.($c+1)];
	$di_bowner[$c] = $_POST['dishareholderbowner'.($c+1)];
	$di_address[$c] = $_POST['dishareholdernonresidentresaddress'.($c+1)];
	$di_finAddress[$c] = $_POST['dishareholderFINaddress'.($c+1)];

	//di 1 passport preview details
	$di_surname_preview[$c] = $_POST['dinonresidentpopupsurname'.($c+1)];
	$di_givenname_preview[$c] = $_POST['dinonresidentpopupgivenName'.($c+1)];

	$di_name[$c] = $di_surname_preview[$c].' '.$di_givenname_preview[$c];
	$di_gender_preview[$c] = $_POST['dinonresidentpopupgender'.($c+1)];
	if($di_gender_preview[$c] == 'on'){
		$di_gender_preview[$c] = 'Male';
	}else{
		$di_gender_preview[$c] = 'Female';
	}
	$di_passport_preview[$c] = $_POST['dinonresidentpopuppassportno'.($c+1)];
	$di_nationality_preview[$c] = $_POST['dinonresidentpopupnationality'.($c+1)];
	$di_country_preview[$c] = $_POST['dinonresidentpopupcountry'.($c+1)];
	$di_expirydate_preview[$c] = $_POST['dinonresidentpopupdateexpiry'.($c+1)];
	$di_dob_preview[$c] = $_POST['dinonresidentpopupdob'.($c+1)];

	//di 1 passport manual details
	$di_gender_manual[$c] = $_POST['dinonmanuallygender'.($c+1)];
	if($di_gender_manual[$c] == 'on'){
		$di_gender_manual[$c] = 'Male';
	}else{
		$di_gender_manual[$c] = 'Female';
	}
	$di_passport_manual[$c] = $_POST['dinonmanuallypassportno'.($c+1)];
	$di_nationality_manual[$c] = $_POST['dinonmanuallynationality'.($c+1)];
	$di_country_manual[$c] = $_POST['dinonmanuallycountry'.($c+1)];
	$di_dateissued_manual[$c] = $_POST['dinonmanuallydatetimepicker'.($c+1)];
	$di_expirydate_manual[$c] = $_POST['dinonmanuallydateexpiry'.($c+1)];
	$di_birthplace_manual[$c] = $_POST['dinonmanuallyplaceofBirth'.($c+1)];
	$di_dob_manual[$c] = $_POST['dinonmanuallydateBirth'.($c+1)];

	//di 1 nric front details
	$di_nric_front_no[$c] = $_POST['dipreviewpopupfrontnricnumber'.($c+1)];
	$di_nric_front_gender[$c] = $_POST['dipreviewpopupnricfrontgender'.($c+1)];
	if($di_nric_front_gender[$c] == 'on'){
		$di_nric_front_gender[$c] = 'Male';
	}else{
		$di_nric_front_gender[$c] = 'Female';
	}
	$di_nric_front_dob[$c] = $_POST['dipreviewpopupnricfrontdob'.($c+1)];
	$di_nric_front_nation[$c] = $_POST['dipreviewpopupnricfrontnationality'.($c+1)];
	$di_nric_front_postal[$c] = $_POST['dipreviewpopupnricfrontpostcode'.($c+1)];
	$di_nric_front_street[$c] = $_POST['dipreviewpopupnricfrontstreetname'.($c+1)];
	$di_nric_front_floor[$c] = $_POST['dipreviewpopupnricfrontfloor'.($c+1)];
	$di_nric_front_unit[$c] = $_POST['dipreviewnrpopupicfrontunit'.($c+1)];
	$di_nric_front_address[$c] = $di_nric_front_street[$c].' <br>'.$di_nric_front_floor[$c].' '.$di_nric_front_unit[$c].' <br>'.$di_nric_front_postal[$c];

	//di 1 nric back details
	$di_nric_back_no[$c] = $_POST['dipreviewpopupbacknricnumber'.($c+1)];
	$di_nric_back_gender[$c] = $_POST['dipreviewpopupnricbackgender'.($c+1)];
	$di_nric_back_dob[$c] = $_POST['dipreviewpopupnricbackdob'.($c+1)];
	$di_nric_back_nation[$c] = $_POST['dipreviewpopupnricbacknationality'.($c+1)];
	$di_nric_back_postal[$c] = $_POST['dipreviewpopupnricbackpostcode'.($c+1)];
	$di_nric_back_street[$c] = $_POST['dipreviewpopupnricbackstreetname'.($c+1)];
	$di_nric_back_floor[$c] = $_POST['dipreviewpopupnricbackfloor'.($c+1)];
	$di_nric_back_unit[$c] = $_POST['dipreviewnrpopupicbackunit'.($c+1)];

	//di 1 nric manual details
	$di_nric_no_manual[$c] = $_POST['dipreviewmanuallynricnumber'.($c+1)];
	$di_nric_gender_manual[$c] = $_POST['dipreviewmanuallynricgender'.($c+1)];
	if($di_nric_gender_manual[$c] == 'on'){
		$di_nric_gender_manual[$c] = 'Male';
	}else{
		$di_nric_gender_manual[$c] = 'Female';
	}
	$di_nric_dob_manual[$c] = $_POST['dipreviewmanuallynricdob'.($c+1)];
	$di_nric_nation_manual[$c] = $_POST['dipreviewmanuallynricnationality'.($c+1)];
	$di_nric_postal_manual[$c] = $_POST['dipreviewmanuallynricpostcode'.($c+1)];
	$di_nric_street_manual[$c] = $_POST['dipreviewmanuallynricstreetname'.($c+1)];
	$di_nric_floor_manual[$c] = $_POST['dipreviewmanuallynricfloor'.($c+1)];
	$di_nric_unit_manual[$c] = $_POST['dipreviewnrmanuallyicunit'.($c+1)];
	$di_nric_address_manual[$c] = $di_nric_street_manual[$c].' <br>'.$di_nric_floor_manual[$c].' '.$di_nric_unit_manual[$c].' <br>'.$di_nric_postal_manual[$c];

	//di 1 fin popup details
	$di_fin_surname_preview[$c] = $_POST['difincardpopupsurname'.($c+1)];
	$di_fin_givenname_preview[$c] = $_POST['difincardpopupgivenName'.($c+1)];

	$di_fin_name[$c] = $di_fin_surname_preview[$c].' '.$di_fin_givenname_preview[$c];

	$di_fin_passport_preview[$c] = $_POST['difincardpopuppassportno'.($c+1)];
	$di_fin_nationality_preview[$c] = $_POST['difincardpopupnationality'.($c+1)];
	$di_fin_country_preview[$c] = $_POST['difincardpopupcountry'.($c+1)];
	$di_fin_gender_preview[$c] = $_POST['difincardpopupgender'.($c+1)];
	if($di_fin_gender_preview[$c] == 'on'){
		$di_fin_gender_preview[$c] = 'Male';
	}else{
		$di_fin_gender_preview[$c] = 'Female';
	}
	$di_fin_expirydate_preview[$c] = $_POST['difincardpopupdateexpiry'.($c+1)];
	$di_fin_dob_preview[$c] = $_POST['difincardpopupdob'.($c+1)];
	$di_finAddress[$c] = $_POST['dishareholderFINresaddress'.($c+1)];

	//di 1 fin front details
	$di_fin_front_employer[$c] = $_POST['difincardfrontemployer'.($c+1)];
	$di_fin_front_occupation[$c] = $_POST['difincardfrontoccupation'.($c+1)];
	$di_fin_front_issueddate[$c] = $_POST['difincardfrontdateissue'.($c+1)];
	$di_fin_front_expirydate[$c] = $_POST['difincardfrontdateexpiry'.($c+1)];
	$di_fin_front_finno[$c] = $_POST['diwfincardfrontNumber'.($c+1)];

	//di 1 fin manual details
	$di_fin_passport_manual[$c] = $_POST['difincardmanuallypassportno'.($c+1)];
	$di_fin_nationality_manual[$c] = $_POST['difincardmanuallynationality'.($c+1)];
	$di_fin_country_manual[$c] = $_POST['difincardmanuallycountry'.($c+1)];
	$di_fin_gender_manual[$c] = $_POST['difincardmanuallygender'.($c+1)];
	if($di_fin_gender_manual[$c] == 'on'){
		$di_fin_gender_manual[$c] = 'Male';
	}else{
		$di_fin_gender_manual[$c] = 'Female';
	}
	$di_fin_issuedate_manual[$c] = $_POST['difincardmanuallydatetimepicker'.($c+1)];
	$di_fin_expirydate_manual[$c] = $_POST['difincardmanuallydateexpiry'.($c+1)];
	$di_fin_birthplace_manual[$c] = $_POST['difincardmanuallyplaceofBirth'.($c+1)];
	$di_fin_dob_manual[$c] = $_POST['difincardmanuallydateBirth'.($c+1)];

	$di_fin_employer_manual[$c] = $_POST['difincardmanuallyemployer'.($c+1)];
	$di_fin_occupation_manual[$c] = $_POST['difincardmanuallyoccupation'.($c+1)];
	$di_fin_workpassissueddate_manual[$c] = $_POST['difincardmanuallyworkpassdateissue'.($c+1)];
	$di_fin_workpassexpirydate_manual[$c] = $_POST['difincardmanuallyworkpassdateexpiry'.($c+1)];
	$di_fin_finno_manual[$c] = $_POST['difincardmanuallywfincardnumber'.($c+1)];

	//contact details
	$dihandphone[$c] = $_POST['dishareholderhandphone'.($c+1)];
	$diOfficephone[$c] = $_POST['dishareholderOfficephone'.($c+1)];
	$dibusinessemail[$c] = $_POST['dishareholderbusinessemail'.($c+1)];
	$diPersonalemail[$c] = $_POST['dishareholderPersonalemail'.($c+1)];
	$dipep[$c] = $_POST['dishareholderpep'.($c+1)];
	if($dipep[$c] == 'dishareholderpepyes_'.($c+1)){
		$dipep[$c] = 'Yes';
	}else{
		$dipep[$c] = 'No';
	}
	$dicountrypep[$c] = $_POST['dishareholdercountrypep'.($c+1)];
	$dirolePep[$c] = $_POST['dishareholderrolePep'.($c+1)];
	$dipepFrom[$c] = $_POST['dishareholderpepFrom'.($c+1)];
	$dipepTo[$c] = $_POST['dishareholderpepTo'.($c+1)];

	$director_shareholder_paid_up_capital[$c] = $_POST['director_shareholder_paid_up_capital'.($c+1)];
	$director_shareholder_number_of_shares[$c] = $_POST['director_shareholder_number_of_shares'.($c+1)];

	$dishareholdernonresidentuploadpassport[$c] = $_POST["dishareholdernonresidentuploadpassport".($c+1)];
	$dishareholdernonresidentaddress[$c] = $_POST["dishareholdernonresidentaddress".($c+1)];
	$dishareholderFINuploadPassport[$c] = $_POST["dishareholderFINuploadPassport".($c+1)];
	$dishareholderfincardfront[$c] = $_POST["dishareholderfincardfront".($c+1)];
	$dishareholderfincardback[$c] = $_POST["dishareholderfincardback".($c+1)];
	$dishareholderFINaddress[$c] = $_POST["dishareholderFINaddress".($c+1)];
	$dishareholderSGnricfront[$c] = $_POST["dishareholderSGnricfront".($c+1)];
	$dishareholderSGnricback[$c] = $_POST["dishareholderSGnricback".($c+1)];

}

//==============corporate shareholders details starts here===================//
for($c = 0;$c < $counter3; $c++){

	$corporate_surname[$c] = ucwords($_POST['CorporateShareholderName'.($c+1)]);//this is full name in form
	$corporate_country[$c] = $_POST['CorporateCountry'.($c+1)];//corporatenonresident_1 corporatecitizenpr_1 corporatepassholder_1
	$corporate_contact[$c] = $_POST['CorporatecontactNumber'.($c+1)];
	$corporate_email[$c] = $_POST['CorporateEmail'.($c+1)];
	$corporate_address[$c] = $_POST['corporatenonresidentaddress'.($c+1)];
	$corporate_resaddress[$c] = $_POST['corporatenonresidentresaddress'.($c+1)];
	$corporate_finAddress[$c] = $_POST['corporateFINresaddress'.($c+1)];
	$corporate_resstatus[$c] = $_POST['corporateresstatus1'.($c+1)];
	$corporate_certificate[$c] = $_POST['CorporateCertificate'.($c+1)];
	$corporate_certificate1[$c] = $_POST['CorporateCertificate1_'.($c+1)];
	$corporate_representname[$c] = $_POST['CorporateRepresentativeName'.($c+1)];

	//corporate 1 passport preview details
	$corporate_surname_preview[$c] = $_POST['corporatenonresidentpopupsurname'.($c+1)];
	$corporate_givenname_preview[$c] = $_POST['corporatenonresidentpopupgivenName'.($c+1)];

	$corporate_name[$c] = $corporate_surname_preview[$c].' '.$corporate_givenname_preview[$c];
	$corporate_gender_preview[$c] = $_POST['corporatenonresidentpopupgender'.($c+1)];
	if($corporate_gender_preview[$c] == 'on'){
		$corporate_gender_preview[$c] = 'Male';
	}else{
		$corporate_gender_preview[$c] = 'Female';
	}
	$corporate_passport_preview[$c] = $_POST['corporatenonresidentpopuppassportno'.($c+1)];
	$corporate_nationality_preview[$c] = $_POST['corporatenonresidentpopupnationality'.($c+1)];
	$corporate_country_preview[$c] = $_POST['corporatenonresidentpopupcountry'.($c+1)];
	$corporate_expirydate_preview[$c] = $_POST['corporatenonresidentpopupdateexpiry'.($c+1)];
	$corporate_dob_preview[$c] = $_POST['corporatenonresidentpopupdob'.($c+1)];
	$corporate_birthplace_preview[$c] = $_POST['corporatenonresidentpopupplaceofBirth'.($c+1)];

	//corporate 1 passport manual details
	$corporate_gender_manual[$c] = $_POST['corporatenonmanuallygender'.($c+1)];
	if($corporate_gender_manual[$c] == 'on'){
		$corporate_gender_manual[$c] = 'Male';
	}else{
		$corporate_gender_manual[$c] = 'Female';
	}
	$corporate_passport_manual[$c] = $_POST['corporatenonmanuallypassportno'.($c+1)];
	$corporate_nationality_manual[$c] = $_POST['corporatenonmanuallynationality'.($c+1)];
	$corporate_country_manual[$c] = $_POST['corporatenonmanuallycountry'.($c+1)];
	$corporate_dateissued_manual[$c] = $_POST['corporatenonmanuallydatetimepicker'.($c+1)];
	$corporate_expirydate_manual[$c] = $_POST['corporatenonmanuallydateexpiry'.($c+1)];
	$corporate_birthplace_manual[$c] = $_POST['corporatenonmanuallyplaceofBirth'.($c+1)];
	$corporate_dob_manual[$c] = $_POST['corporatenonmanuallydateBirth'.($c+1)];

	//corporate shareholder 1 nric front details
	$corporate_nric_front_no[$c] = $_POST['corporatepreviewpopupfrontnricnumber'.($c+1)];
	$corporate_nric_front_gender[$c] = $_POST['corporatepreviewpopupnricfrontgender'.($c+1)];
	if($corporate_nric_front_gender[$c] == 'on'){
		$corporate_nric_front_gender[$c] = 'Male';
	}else{
		$corporate_nric_front_gender[$c] = 'Female';
	}
	$corporate_nric_front_dob[$c] = $_POST['corporatepreviewpopupnricfrontdob'.($c+1)];
	$corporate_nric_front_nation[$c] = $_POST['corporatepreviewpopupnricfrontnationality'.($c+1)];
	$corporate_nric_front_postal[$c] = $_POST['corporatepreviewpopupnricfrontpostcode'.($c+1)];
	$corporate_nric_front_street[$c] = $_POST['corporatepreviewpopupnricfrontstreetname'.($c+1)];
	$corporate_nric_front_floor[$c] = $_POST['corporatepreviewpopupnricfrontfloor'.($c+1)];
	$corporate_nric_front_unit[$c] = $_POST['corporatepreviewnrpopupicfrontunit'.($c+1)];
	$corporate_nric_front_address[$c] = $corporate_nric_front_street[$c].' <br>'.$corporate_nric_front_floor[$c].' '.$corporate_nric_front_unit[$c].' <br>'.$corporate_nric_front_postal[$c];

	//corporate shareholder 1 nric back details
	$corporate_nric_back_no[$c] = $_POST['corporatepreviewpopupbacknricnumber'.($c+1)];
	$corporate_nric_back_gender[$c] = $_POST['corporatepreviewpopupnricbackgender'.($c+1)];
	$corporate_nric_back_dob[$c] = $_POST['corporatepreviewpopupnricbackdob'.($c+1)];
	$corporate_nric_back_nation[$c] = $_POST['corporatepreviewpopupnricbacknationality'.($c+1)];
	$corporate_nric_back_postal[$c] = $_POST['corporatepreviewpopupnricbackpostcode'.($c+1)];
	$corporate_nric_back_street[$c] = $_POST['corporatepreviewpopupnricbackstreetname'.($c+1)];
	$corporate_nric_back_floor[$c] = $_POST['corporatepreviewpopupnricbackfloor'.($c+1)];
	$corporate_nric_back_unit[$c] = $_POST['corporatepreviewnrpopupicbackunit'.($c+1)];

	//corporate shareholder 1 nric manual details
	$corporate_nric_no_manual[$c] = $_POST['corporatepreviewmanuallynricnumber'.($c+1)];
	$corporate_nric_gender_manual[$c] = $_POST['corporatepreviewmanuallynricgender'.($c+1)];
	if($corporate_nric_gender_manual[$c] == 'on'){
		$corporate_nric_gender_manual[$c] = 'Male';
	}else{
		$corporate_nric_gender_manual[$c] = 'Female';
	}
	$corporate_nric_dob_manual[$c] = $_POST['corporatepreviewmanuallynricdob'.($c+1)];
	$corporate_nric_nation_manual[$c] = $_POST['corporatepreviewmanuallynricnationality'.($c+1)];
	$corporate_nric_postal_manual[$c] = $_POST['corporatepreviewmanuallynricpostcode'.($c+1)];
	$corporate_nric_street_manual[$c] = $_POST['corporatepreviewmanuallynricstreetname'.($c+1)];
	$corporate_nric_floor_manual[$c] = $_POST['corporatepreviewmanuallynricfloor'.($c+1)];
	$corporate_nric_unit_manual[$c] = $_POST['corporatepreviewnrmanuallyicunit'.($c+1)];
	$corporate_nric_address_manual[$c] = $corporate_nric_street_manual[$c].' <br>'.$corporate_nric_floor_manual[$c].' '.$corporate_nric_unit_manual[$c].' <br>'.$corporate_nric_postal_manual[$c];

	//corporate shareholder 1 fin popup details
	$corporate_fin_surname_preview[$c] = $_POST['corporatefincardpopupsurname'.($c+1)];
	$corporate_fin_givenname_preview[$c] = $_POST['corporatefincardpopupgivenName'.($c+1)];
	$corporate_fin_name[$c] = $corporate_fin_surname_preview[$c].' '.$corporate_fin_givenname_preview[$c];
	$corporate_fin_passport_preview[$c] = $_POST['corporatefincardpopuppassportno'.($c+1)];
	$corporate_fin_nationality_preview[$c] = $_POST['corporatefincardpopupnationality'.($c+1)];
	$corporate_fin_country_preview[$c] = $_POST['corporatefincardpopupcountry'.($c+1)];
	$corporate_fin_gender_preview[$c] = $_POST['corporatefincardpopupgender'.($c+1)];
	if($corporate_fin_gender_preview[$c] == 'on'){
		$corporate_fin_gender_preview[$c] = 'Male';
	}else{
		$corporate_fin_gender_preview[$c] = 'Female';
	}
	$corporate_fin_expirydate_preview[$c] = $_POST['corporatefincardpopupdateexpiry'.($c+1)];
	$corporate_fin_dob_preview[$c] = $_POST['corporatefincardpopupdob'.($c+1)];
	$corporate_finresAddress[$c] = $_POST['corporateshareholderFINresaddress'.($c+1)];

	//corporate shareholder 1 fin front details
	$corporate_fin_front_employer[$c] = $_POST['corporatefincardfrontemployer'.($c+1)];
	$corporate_fin_front_occupation[$c] = $_POST['corporatefincardfrontoccupation'.($c+1)];
	$corporate_fin_front_issueddate[$c] = $_POST['corporatefincardfrontdateissue'.($c+1)];
	$corporate_fin_front_expirydate[$c] = $_POST['corporatefincardfrontdateexpiry'.($c+1)];
	$corporate_fin_front_finno[$c] = $_POST['corporatewfincardfrontNumber'.($c+1)];

	//corporate shareholder 1 fin manual details
	$corporate_fin_passport_manual[$c] = $_POST['corporatefincardmanuallypassportno'.($c+1)];
	$corporate_fin_nationality_manual[$c] = $_POST['corporatefincardmanuallynationality'.($c+1)];
	$corporate_fin_country_manual[$c] = $_POST['corporatefincardmanuallycountry'.($c+1)];
	$corporate_fin_gender_manual[$c] = $_POST['corporatefincardmanuallygender'.($c+1)];
	if($corporate_fin_gender_manual[$c] == 'on'){
		$corporate_fin_gender_manual[$c] = 'Male';
	}else{
		$corporate_fin_gender_manual[$c] = 'Female';
	}
	$corporate_fin_issuedate_manual[$c] = $_POST['corporatefincardmanuallydatetimepicker'.($c+1)];
	$corporate_fin_expirydate_manual[$c] = $_POST['corporatefincardmanuallydateexpiry'.($c+1)];
	$corporate_fin_birthplace_manual[$c] = $_POST['corporatefincardmanuallyplaceofBirth'.($c+1)];
	$corporate_fin_dob_manual[$c] = $_POST['corporatefincardmanuallydateBirth'.($c+1)];
	$corporate_fin_employer_manual[$c] = $_POST['corporatefincardmanuallyemployer'.($c+1)];
	$corporate_fin_occupation_manual[$c] = $_POST['corporatefincardmanuallyoccupation'.($c+1)];
	$corporate_fin_workpassissueddate_manual[$c] = $_POST['corporatefincardmanuallyworkpassdateissue'.($c+1)];
	$corporate_fin_workpassexpirydate_manual[$c] = $_POST['corporatefincardmanuallyworkpassdateexpiry'.($c+1)];
	$corporate_fin_finno_manual[$c] = $_POST['corporatefincardmanuallywfincardnumber'.($c+1)];

	$corporate_paid_up_capital[$c] = $_POST['corporate_paid_up_capital'.($c+1)];
	$corporate_number_of_shares[$c] = $_POST['corporate_number_of_shares'.($c+1)];

	$CorporateCertificate[$c] = $_POST["CorporateCertificate".($c+1)];
	$CorporateCertificate1[$c] = $_POST["CorporateCertificate1_".($c+1)];
	$corporatenonresidentuploadpassport[$c] = $_POST["corporatenonresidentuploadpassport".($c+1)];	
	$corporatenonresidentaddress[$c] = $_POST["corporatenonresidentaddress".($c+1)];
	$corporateFINuploadPassport[$c] = $_POST["corporateFINuploadPassport".($c+1)];
	$corporatefincardfront[$c] = $_POST["corporatefincardfront".($c+1)];
	$corporatefincardback[$c] = $_POST["corporatefincardback".($c+1)];
	$corporateFINaddress[$c] = $_POST["corporateFINaddress".($c+1)];
	$corporateSGnricfront[$c] = $_POST["corporateSGnricfront".($c+1)];
	$corporateSGnricback[$c] = $_POST["corporateSGnricback".($c+1)];

}

	//declaration page details
	$currency = $_POST['currency'];
	$paid_up_capital = $_POST['paid_up_capital'];
	$number_of_shares = $_POST['number_of_shares'];
	$agent_full_name = $_POST['agent_full_name'];
	$agent_email = $_POST['agent_email'];
	$agent_contact_no = $_POST['agent_contact_no'];

	//to check manual entry
	$manual_director_passport = $_POST['manual_director_passport'];
	$manual_director_nric = $_POST['manual_director_nric'];
	$manual_director_fin = $_POST['manual_director_fin'];

//BO shareholder role section for loop
for($c = 0;$c < $bocounter; $c++){

	$bo_getboname = $_POST['getboname'];
	$bo_resstatus[$c] = $_POST['borestatus'.($c+1)];

	//bo passport preview details
	$bo_surname_preview[$c] = $_POST['bosurname'.($c+1)];
	$bo_givenname_preview[$c] = $_POST['bogivenname'.($c+1)];

	$bo_name[$c] = $bo_surname_preview[$c].' '.$bo_givenname_preview[$c];
	$bo_gender_preview[$c] = $_POST['bogender'.($c+1)];
	if($bo_gender_preview[$c] == 'on'){
		$bo_gender_preview[$c] = 'Male';
	}else{
		$bo_gender_preview[$c] = 'Female';
	}
	$bo_passport_preview[$c] = $_POST['bopassportno'.($c+1)];
	$bo_nationality_preview[$c] = $_POST['bonationality'.($c+1)];
	$bo_country_preview[$c] = $_POST['bocountry'.($c+1)];
	$bo_expirydate_preview[$c] = $_POST['bodateexpiry'.($c+1)];
	$bo_dob_preview[$c] = $_POST['bodob'.($c+1)];

	//bo passport manual details
	$bo_gender_manual[$c] = $_POST['bomgender'.($c+1)];
	if($bo_gender_manual[$c] == 'on'){
		$bo_gender_manual[$c] = 'Male';
	}else{
		$bo_gender_manual[$c] = 'Female';
	}
	$bo_passport_manual[$c] = $_POST['bompassportno'.($c+1)];
	$bo_nationality_manual[$c] = $_POST['bomnationality'.($c+1)];
	$bo_country_manual[$c] = $_POST['bomcountry'.($c+1)];
	$bo_dateissued_manual[$c] = $_POST['bomdatetimepicker'.($c+1)];
	$bo_expirydate_manual[$c] = $_POST['bomdateexpiry'.($c+1)];
	$bo_birthplace_manual[$c] = $_POST['bompob'.($c+1)];
	$bo_dob_manual[$c] = $_POST['bomdob'.($c+1)];

	//bo nric front details
	$bo_nric_front_no[$c] = $_POST['bopreviewfrontnricnumber'.($c+1)];
	$bo_nric_front_gender[$c] = $_POST['bopreviewfrontnricgender'.($c+1)];
	if($bo_nric_front_gender[$c] == 'on'){
		$bo_nric_front_gender[$c] = 'Male';
	}else{
		$bo_nric_front_gender[$c] = 'Female';
	}
	$bo_nric_front_dob[$c] = $_POST['bopreviewfrontnricdoi'.($c+1)];
	$bo_nric_front_nation[$c] = $_POST['bopreviewfrontnriccob'.($c+1)];
	$bo_nric_front_postal[$c] = $_POST['bopreviewfrontnricpostcode'.($c+1)];
	$bo_nric_front_street[$c] = $_POST['bopreviewfrontnricstreetname'.($c+1)];
	$bo_nric_front_floor[$c] = $_POST['bopreviewfrontnricfloor'.($c+1)];
	$bo_nric_front_unit[$c] = $_POST['bopreviewfrontnricunit'.($c+1)];
	$bo_nric_front_address[$c] = $bo_nric_front_street[$c].' <br>'.$bo_nric_front_floor[$c].' '.$bo_nric_front_unit[$c].' <br>'.$bo_nric_front_postal[$c];

	//bo nric back details
	$bo_nric_back_no[$c] = $_POST['bopreviewbacknricnumber'.($c+1)];
	$bo_nric_back_gender[$c] = $_POST['bopreviewbacknricgender'.($c+1)];
	$bo_nric_back_dob[$c] = $_POST['bopreviewbacknricdoi'.($c+1)];
	$bo_nric_back_nation[$c] = $_POST['bopreviewbacknriccob'.($c+1)];
	$bo_nric_back_postal[$c] = $_POST['bopreviewbacknricpostcode'.($c+1)];
	$bo_nric_back_street[$c] = $_POST['bopreviewbacknricstreetname'.($c+1)];
	$bo_nric_back_floor[$c] = $_POST['bopreviewbacknricfloor'.($c+1)];
	$bo_nric_back_unit[$c] = $_POST['bopreviewbacknricunit'.($c+1)];

	//bo nric manual details
	$bo_nric_no_manual[$c] = $_POST['bopreviewnricnumber'.($c+1)];
	$bo_nric_gender_manual[$c] = $_POST['bopreviewnricgender'.($c+1)];
	if($bo_nric_gender_manual[$c] == 'on'){
		$bo_nric_gender_manual[$c] = 'Male';
	}else{
		$bo_nric_gender_manual[$c] = 'Female';
	}
	$bo_nric_dob_manual[$c] = $_POST['bopreviewnricdoi'.($c+1)];
	$bo_nric_nation_manual[$c] = $_POST['bopreviewnriccob'.($c+1)];
	$bo_nric_postal_manual[$c] = $_POST['bopreviewnricpostcode'.($c+1)];
	$bo_nric_street_manual[$c] = $_POST['bopreviewnricstreetname'.($c+1)];
	$bo_nric_floor_manual[$c] = $_POST['bopreviewnricfloor'.($c+1)];
	$bo_nric_unit_manual[$c] = $_POST['bopreviewnricunit'.($c+1)];
	$bo_nric_address_manual[$c] = $bo_nric_street_manual[$c].' <br>'.$bo_nric_floor_manual[$c].' '.$bo_nric_unit_manual[$c].' <br>'.$bo_nric_postal_manual[$c];

	//bo fin popup details
	$bo_fin_surname_preview[$c] = $_POST['bofinsurname'.($c+1)];
	$bo_fin_givenname_preview[$c] = $_POST['bofingivenname'.($c+1)];

	$bo_fin_name[$c] = $bo_fin_surname_preview[$c].' '.$bo_fin_givenname_preview[$c];

	$bo_fin_passport_preview[$c] = $_POST['bofinpassportno'.($c+1)];
	$bo_fin_nationality_preview[$c] = $_POST['bofinnationality'.($c+1)];
	$bo_fin_country_preview[$c] = $_POST['bofincountry'.($c+1)];
	$bo_fin_gender_preview[$c] = $_POST['bofingender'.($c+1)];
	if($bo_fin_gender_preview[$c] == 'on'){
		$bo_fin_gender_preview[$c] = 'Male';
	}else{
		$bo_fin_gender_preview[$c] = 'Female';
	}
	$bo_fin_expirydate_preview[$c] = $_POST['bofindateexpiry'.($c+1)];
	$bo_fin_dob_preview[$c] = $_POST['bofindob'.($c+1)];
	
	//bo fin front details
	$bo_fin_front_employer[$c] = $_POST['bofinfrontemployer'.($c+1)];
	$bo_fin_front_occupation[$c] = $_POST['bofinfrontoccupation'.($c+1)];
	$bo_fin_front_issueddate[$c] = $_POST['bowfinfrontdoi'.($c+1)];
	$bo_fin_front_expirydate[$c] = $_POST['bowfinfrontdoe'.($c+1)];
	$bo_fin_front_finno[$c] = $_POST['bofinfrontnumber'.($c+1)];

	//bo fin manual details
	$bo_fin_passport_manual[$c] = $_POST['bomfinpassportno'.($c+1)];
	$bo_fin_nationality_manual[$c] = $_POST['bomfinnationality'.($c+1)];
	$bo_fin_country_manual[$c] = $_POST['bomfincountry'.($c+1)];
	$bo_fin_gender_manual[$c] = $_POST['bomfingender'.($c+1)];
	if($bo_fin_gender_manual[$c] == 'on'){
		$bo_fin_gender_manual[$c] = 'Male';
	}else{
		$bo_fin_gender_manual[$c] = 'Female';
	}
	$bo_fin_issuedate_manual[$c] = $_POST['bomfindatetimepicker'.($c+1)];
	$bo_fin_expirydate_manual[$c] = $_POST['bomfindateexpiry'.($c+1)];
	$bo_fin_birthplace_manual[$c] = $_POST['bomfinpob'.($c+1)];
	$bo_fin_dob_manual[$c] = $_POST['bomfindob'.($c+1)];

	$bo_fin_employer_manual[$c] = $_POST['bomfinemployer'.($c+1)];
	$bo_fin_occupation_manual[$c] = $_POST['bomfinoccupation'.($c+1)];
	$bo_fin_workpassissueddate_manual[$c] = $_POST['bomwfindoi'.($c+1)];
	$bo_fin_workpassexpirydate_manual[$c] = $_POST['bomwfindoe'.($c+1)];
	$bo_fin_finno_manual[$c] = $_POST['bomfinnumber'.($c+1)];

	//bo contact details
	$bofullname[$c] = $_POST['bofullname'.($c+1)];
	$bocontactno[$c] = $_POST['bocontactno'.($c+1)];
	$boemail[$c] = $_POST['boemail'.($c+1)];
	//bo document uploads
	$bo_passport[$c] = $_POST["bopassport".($c+1)];
	$bo_addressProof[$c] = $_POST["boaddressProof".($c+1)];
	$bo_resaddress[$c] = $_POST["boresaddress".($c+1)];
	$bo_finPassport[$c] = $_POST["bofinPassport".($c+1)];
	$bo_finCardFront[$c] = $_POST["bofinCardFront".($c+1)];
	$bo_finCardBack[$c] = $_POST["bofinCardBack".($c+1)];
	$bo_finAddressProof[$c] = $_POST["bofinAddressProof".($c+1)];
	$bo_finresaddress[$c] = $_POST["bofinresaddress".($c+1)];
	$bo_nricfront[$c] = $_POST["bonricfront".($c+1)];
	$bo_nricback[$c] = $_POST["bonricback".($c+1)];

}

//BO shareholder role section for loop
for($c = 0;$c < $bodicounter; $c++){

	$bodi_getboname = $_POST['getbodiname'];
	$bodi_resstatus[$c] = $_POST['bodirestatus'.($c+1)];

	//bo passport preview details
	$bodi_surname_preview[$c] = $_POST['bodisurname'.($c+1)];
	$bodi_givenname_preview[$c] = $_POST['bodigivenname'.($c+1)];

	$bodi_name[$c] = $bodi_surname_preview[$c].' '.$bodi_givenname_preview[$c];
	$bodi_gender_preview[$c] = $_POST['bodigender'.($c+1)];
	if($bodi_gender_preview[$c] == 'on'){
		$bodi_gender_preview[$c] = 'Male';
	}else{
		$bodi_gender_preview[$c] = 'Female';
	}
	$bodi_passport_preview[$c] = $_POST['bodipassportno'.($c+1)];
	$bodi_nationality_preview[$c] = $_POST['bodinationality'.($c+1)];
	$bodi_country_preview[$c] = $_POST['bodicountry'.($c+1)];
	$bodi_expirydate_preview[$c] = $_POST['bodidateexpiry'.($c+1)];
	$bodi_dob_preview[$c] = $_POST['bodidob'.($c+1)];

	//bo passport manual details
	$bodi_gender_manual[$c] = $_POST['bodimgender'.($c+1)];
	if($bodi_gender_manual[$c] == 'on'){
		$bodi_gender_manual[$c] = 'Male';
	}else{
		$bodi_gender_manual[$c] = 'Female';
	}
	$bodi_passport_manual[$c] = $_POST['bodimpassportno'.($c+1)];
	$bodi_nationality_manual[$c] = $_POST['bodimnationality'.($c+1)];
	$bodi_country_manual[$c] = $_POST['bodimcountry'.($c+1)];
	$bodi_dateissued_manual[$c] = $_POST['bodimdatetimepicker'.($c+1)];
	$bodi_expirydate_manual[$c] = $_POST['bodimdateexpiry'.($c+1)];
	$bodi_birthplace_manual[$c] = $_POST['bodimpob'.($c+1)];
	$bodi_dob_manual[$c] = $_POST['bodimdob'.($c+1)];

	//bo nric front details
	$bodi_nric_front_no[$c] = $_POST['bodipreviewfrontnricnumber'.($c+1)];
	$bodi_nric_front_gender[$c] = $_POST['bodipreviewfrontnricgender'.($c+1)];
	if($bodi_nric_front_gender[$c] == 'on'){
		$bodi_nric_front_gender[$c] = 'Male';
	}else{
		$bodi_nric_front_gender[$c] = 'Female';
	}
	$bodi_nric_front_dob[$c] = $_POST['bodipreviewfrontnricdoi'.($c+1)];
	$bodi_nric_front_nation[$c] = $_POST['bodipreviewfrontnriccob'.($c+1)];
	$bodi_nric_front_postal[$c] = $_POST['bodipreviewfrontnricpostcode'.($c+1)];
	$bodi_nric_front_street[$c] = $_POST['bodipreviewfrontnricstreetname'.($c+1)];
	$bodi_nric_front_floor[$c] = $_POST['bodipreviewfrontnricfloor'.($c+1)];
	$bodi_nric_front_unit[$c] = $_POST['bodipreviewfrontnricunit'.($c+1)];
	$bodi_nric_front_address[$c] = $bodi_nric_front_street[$c].' <br>'.$bodi_nric_front_floor[$c].' '.$bodi_nric_front_unit[$c].' <br>'.$bodi_nric_front_postal[$c];

	//bo nric back details
	$bodi_nric_back_no[$c] = $_POST['bodipreviewbacknricnumber'.($c+1)];
	$bodi_nric_back_gender[$c] = $_POST['bodipreviewbacknricgender'.($c+1)];
	$bodi_nric_back_dob[$c] = $_POST['bodipreviewbacknricdoi'.($c+1)];
	$bodi_nric_back_nation[$c] = $_POST['bodipreviewbacknriccob'.($c+1)];
	$bodi_nric_back_postal[$c] = $_POST['bodipreviewbacknricpostcode'.($c+1)];
	$bodi_nric_back_street[$c] = $_POST['bodipreviewbacknricstreetname'.($c+1)];
	$bodi_nric_back_floor[$c] = $_POST['bodipreviewbacknricfloor'.($c+1)];
	$bodi_nric_back_unit[$c] = $_POST['bodipreviewbacknricunit'.($c+1)];

	//bo nric manual details
	$bodi_nric_no_manual[$c] = $_POST['bodipreviewnricnumber'.($c+1)];
	$bodi_nric_gender_manual[$c] = $_POST['bodipreviewnricgender'.($c+1)];
	if($bodi_nric_gender_manual[$c] == 'on'){
		$bodi_nric_gender_manual[$c] = 'Male';
	}else{
		$bodi_nric_gender_manual[$c] = 'Female';
	}
	$bodi_nric_dob_manual[$c] = $_POST['bodipreviewnricdoi'.($c+1)];
	$bodi_nric_nation_manual[$c] = $_POST['bodipreviewnriccob'.($c+1)];
	$bodi_nric_postal_manual[$c] = $_POST['bodipreviewnricpostcode'.($c+1)];
	$bodi_nric_street_manual[$c] = $_POST['bodipreviewnricstreetname'.($c+1)];
	$bodi_nric_floor_manual[$c] = $_POST['bodipreviewnricfloor'.($c+1)];
	$bodi_nric_unit_manual[$c] = $_POST['bodipreviewnricunit'.($c+1)];
	$bodi_nric_address_manual[$c] = $bodi_nric_street_manual[$c].' <br>'.$bodi_nric_floor_manual[$c].' '.$bodi_nric_unit_manual[$c].' <br>'.$bodi_nric_postal_manual[$c];

	//bo fin popup details
	$bodi_fin_surname_preview[$c] = $_POST['bodifinsurname'.($c+1)];
	$bodi_fin_givenname_preview[$c] = $_POST['bodifingivenname'.($c+1)];

	$bodi_fin_name[$c] = $bodi_fin_surname_preview[$c].' '.$bodi_fin_givenname_preview[$c];

	$bodi_fin_passport_preview[$c] = $_POST['bodifinpassportno'.($c+1)];
	$bodi_fin_nationality_preview[$c] = $_POST['bodifinnationality'.($c+1)];
	$bodi_fin_country_preview[$c] = $_POST['bodifincountry'.($c+1)];
	$bodi_fin_gender_preview[$c] = $_POST['bodifingender'.($c+1)];
	if($bodi_fin_gender_preview[$c] == 'on'){
		$bodi_fin_gender_preview[$c] = 'Male';
	}else{
		$bodi_fin_gender_preview[$c] = 'Female';
	}
	$bodi_fin_expirydate_preview[$c] = $_POST['bodifindateexpiry'.($c+1)];
	$bodi_fin_dob_preview[$c] = $_POST['bodifindob'.($c+1)];
	
	//bo fin front details
	$bodi_fin_front_employer[$c] = $_POST['bodifinfrontemployer'.($c+1)];
	$bodi_fin_front_occupation[$c] = $_POST['bodifinfrontoccupation'.($c+1)];
	$bodi_fin_front_issueddate[$c] = $_POST['bodiwfinfrontdoi'.($c+1)];
	$bodi_fin_front_expirydate[$c] = $_POST['bodiwfinfrontdoe'.($c+1)];
	$bodi_fin_front_finno[$c] = $_POST['bodifinfrontnumber'.($c+1)];

	//bo fin manual details
	$bodi_fin_passport_manual[$c] = $_POST['bodimfinpassportno'.($c+1)];
	$bodi_fin_nationality_manual[$c] = $_POST['bodimfinnationality'.($c+1)];
	$bodi_fin_country_manual[$c] = $_POST['bodimfincountry'.($c+1)];
	$bodi_fin_gender_manual[$c] = $_POST['bodimfingender'.($c+1)];
	if($bodi_fin_gender_manual[$c] == 'on'){
		$bodi_fin_gender_manual[$c] = 'Male';
	}else{
		$bodi_fin_gender_manual[$c] = 'Female';
	}
	$bodi_fin_issuedate_manual[$c] = $_POST['bodimfindatetimepicker'.($c+1)];
	$bodi_fin_expirydate_manual[$c] = $_POST['bodimfindateexpiry'.($c+1)];
	$bodi_fin_birthplace_manual[$c] = $_POST['bodimfinpob'.($c+1)];
	$bodi_fin_dob_manual[$c] = $_POST['bodimfindob'.($c+1)];

	$bodi_fin_employer_manual[$c] = $_POST['bodimfinemployer'.($c+1)];
	$bodi_fin_occupation_manual[$c] = $_POST['bodimfinoccupation'.($c+1)];
	$bodi_fin_workpassissueddate_manual[$c] = $_POST['bodimwfindoi'.($c+1)];
	$bodi_fin_workpassexpirydate_manual[$c] = $_POST['bodimwfindoe'.($c+1)];
	$bodi_fin_finno_manual[$c] = $_POST['bodimfinnumber'.($c+1)];

	//bo contact details
	$bofullname[$c] = $_POST['bodifullname'.($c+1)];
	$bocontactno[$c] = $_POST['bodicontactno'.($c+1)];		
	$boemail[$c] = $_POST['bodiemail'.($c+1)];
	//bo document uploads
	$bodi_passport[$c] = $_POST["bodipassport".($c+1)];
	$bodi_addressProof[$c] = $_POST["bodiaddressProof".($c+1)];
	$bodi_resaddress[$c] = $_POST["bodiresaddress".($c+1)];
	$bodi_finPassport[$c] = $_POST["bodifinPassport".($c+1)];
	$bodi_finCardFront[$c] = $_POST["bodifinCardFront".($c+1)];
	$bodi_finCardBack[$c] = $_POST["bodifinCardBack".($c+1)];
	$bodi_finAddressProof[$c] = $_POST["bodifinAddressProof".($c+1)];
	$bodi_finresaddress[$c] = $_POST["bodifinresaddress".($c+1)];
	$bodi_nricfront[$c] = $_POST["bodinricfront".($c+1)];
	$bodi_nricback[$c] = $_POST["bodinricback".($c+1)];

}

//if conditions for EOD

//proposed company names section
if($proposedCompanyName2 !='null'){
	$proposed_co_2 = '<br>'.$proposedCompanyName2;
}
if($proposedCompanyName3 !='null'){
	$proposed_co_3 = '<br>'.$proposedCompanyName3;
}

//registerd company address section
if($intendedAddress == 'serviceProviderAddress'){
	$reg_co_address = '<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">30 Cecil Street,<br>
			#19-08 Prudential Tower,<br>
			Singapore 049712.</span></td>';
}else{
	$reg_co_address = '<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$floor.' '.$streetName.',<br>
				'.$unit.',<br>Singapore '.$postalCode.'.</span></td>';
}



$director_nr_pass_preview = array();
$director_nr_pass_manual = array();
$director_nric_preview = array();
$director_nric_manual = array();
$director_fin_preview = array();
$director_fin_manual = array();
$director_contact = array();

for($c = 0 ;$c <= $counter; $c++){

	//echo "dir: ".($c).'<br>';
	//director non resident passport preview section
	if($director_resstatus[$c] == 'directornonresident_'.($c+1)){
		if($director_surname_preview[$c] != '' && $director_surname_preview[$c] != 'null'){
		$mail_name = $director_surname[$c];
		$director_nr_pass_preview[$c] = '<h4>Director #'.($c+1).' - '.$director_surname[$c].'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_dob_preview[$c].'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$address[$c].'</span></td>
			</tr>';
		//director non resident passport manual section
		}elseif($director_passport_manual[$c] != '' && $director_passport_manual[$c] != 'null'){
		$mail_name = $director_surname[$c];
		$director_nr_pass_manual[$c] = '<h4>Director #'.($c+1).' - '.$director_surname[$c].'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_gender_manual[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nationality_manual[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_passport_manual[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_country_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_dateissued_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_expirydate_manual[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_birthplace_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_dob_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$address[$c].'</span></td>
			</tr>';
		}
	}

	//director NRIC preview section
	if($director_resstatus[$c] == 'directorcitizenpr_'.($c+1)){
		if($director_nric_front_no[$c] != '' && $director_nric_front_no[$c] != 'null'){
		$mail_name = $director_surname[$c];
		$director_nric_preview[$c] =  '<h4>Director #'.($c+1).' - '.$director_surname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_front_gender[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_front_no[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_front_nation[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_front_dob[$c].'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_front_address[$c].'</span></td>					
			</tr>';
		//director nric manual
		}elseif($director_nric_no_manual[$c] != '' && $director_nric_no_manual[$c] != 'null'){
		$mail_name = $director_surname[$c];
		$director_nric_manual[$c] = '<h4>Director #'.($c+1).' - '.$director_surname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_no_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_nation_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_dob_manual[$c].'</span></td>
				</tr>
				<tr>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_nric_address_manual[$c].'</span></td>					
				</tr>';
		}
	}

	//director FIN preview
	if($director_resstatus[$c] == 'directorpassholder_'.($c+1)){
		if($director_fin_surname_preview[$c] != '' && $director_fin_surname_preview[$c] != 'null'){
		$mail_name = $director_fin_name[$c];

		$director_fin_preview[$c] = '<h4>Director #'.($c+1).' - '.$director_surname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_dob_preview[$c].'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$finAddress[$c].'</span></td>
			</tr>

			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_front_employer[$c].'</span></td>
			</tr>
			<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_front_occupation[$c].'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_front_finno[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_front_issueddat[$c].'e</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_front_expirydate[$c].'</span></td>
			</tr>';
		//director fin manual
		}elseif($director_fin_passport_manual[$c] != '' && $director_fin_passport_manual[$c] != 'null'){
		$mail_name = $director_surname[$c];
		$director_fin_manual[$c] = '<h4>Director #'.($c+1).' - '.$director_surname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_nationality_manual[$c].'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_passport_manual[$c].'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_country_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_expirydate_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_dob_manual[$c].'</span></td>								
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$finAddress[$c].'</span></td>
				</tr>		
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_employer_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_occupation_manual[$c].'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_finno_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_workpassissueddate_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$director_fin_workpassexpirydate_manual[$c].'</span></td>
				</tr>';
		}
	}

	//director contact details
	if($director_resstatus[$c] == 'directornonresident_'.($c+1) || $director_resstatus[$c] == 'directorcitizenpr_'.($c+1) || $director_resstatus[$c] == 'directorpassholder_'.($c+1)){
		$director_contact[$c] = '<tr class="row">
				<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$handphone[$c].' <br>'.$officephone[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$businessemail[$c].' <br>'.$personalemail[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$directorpep[$c].'</span></td>
			</tr>';
		
		if($directorpep[$c] == 'Yes'){
			$director_contact[$c] .= '<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP in which country</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$pepCountry[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Role of PEP</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$pepRole[$c].'</span></td>
			</tr>
			<tr>					 
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP since</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$pepFrom[$c].' - '.$pepTo[$c].'</span></td>
			</tr>';
		}
		
		$director_contact[$c] .= '</tbody>
					  	</table></td>
					<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
			  <tbody>
				<tr>
					<td><span>Uploaded documents:</span></td>
					<td><span></span></td>
				</tr>
				<tr>								
					<td colspan="2" style="border-bottom:1px solid #ccc" valign="top">';
			if($director_passport[$c] != null && $director_passport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_passport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($director_addressProof[$c] != null && $director_addressProof[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_addressProof[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($director_finPassport[$c] != null && $director_finPassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_finPassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($director_finCardFront[$c] != null && $director_finCardFront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_finCardFront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($director_finCardBack[$c] != null && $director_finCardBack[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_finCardBack[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($director_finAddressProof[$c] != null && $director_finAddressProof[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_finAddressProof[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($director_nricfront[$c] != null && $director_nricfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_nricfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($director_nricback[$c] != null && $director_nricback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $director_nricback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DIRECTOR_SERVER_FILE_PATH.$img;
				$director_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}

		$director_contact[$c] .= '</td>
				</tr>	
				</tbody>
			</table></td>
				</tr>
				  </tbody>
				</table>	
			<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';
	}

}

$shareholder_nr_pass_preview = array();
$shareholder_nr_pass_manual = array();
$shareholder_nric_preview = array();
$shareholder_nric_manual = array();
$shareholder_fin_preview = array();
$shareholder_fin_manual = array();
$shareholder_contact = array();

for($c = 0; $c < $counter1; $c++){
	//shareholders non resident passport preview section
	if($shareholder_resstatus[$c] == 'shareholdernonresident_'.($c+1)){
		if($shareholder_surname_preview[$c] != '' && $shareholder_surname_preview[$c] != 'null'){
		$mail_name = $shareholder_name[$c];
		$shareholder_nr_pass_preview[$c] = '<h4>Individual Shareholder #'.($c+1).' - '.$shareholder_surname[$c].'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_dob_preview[$c].'</span></td>
			</tr>';
		//shareholder non resident passport manual section
		}elseif($shareholder_passport_manual[$c] != '' && $shareholder_passport_manual[$c] != 'null'){
		$mail_name = $shareholder_surname[$c];
		$shareholder_nr_pass_manual[$c] = '<h4>Shareholder #'.($c+1).' - '.$shareholder_surname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_gender_manual[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nationality_manual[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_passport_manual[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_country_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_dateissued_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_expirydate_manual[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_birthplace_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_dob_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_address[$c].'</span></td>
			</tr>';
		}
	}

	//shareholder nric preview section
	if($shareholder_resstatus[$c] == 'shareholdercitizenpr_'.($c+1)){
		if($shareholder_nric_front_no[$c] != '' && $shareholder_nric_front_no[$c] != 'null'){
		$mail_name = $shareholder_surname[$c];
		$shareholder_nric_preview[$c] = '<h4>Individual Shareholder #'.($c+1).' - '.$shareholder_surname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_front_gender[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_front_no[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_front_nation[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_front_dob[$c].'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_front_address[$c].'</span></td>					
			</tr>';
		
		//shareholder nric manual
		}elseif($shareholder_nric_no_manual[$c] != '' && $shareholder_nric_no_manual[$c] != 'null'){
		$mail_name = $shareholder_surname[$c];
		$shareholder_nric_manual[$c] = '<h4>Individual Shareholder #'.($c+1).' - '.$shareholder_surname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_no_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_nation_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_dob_manual[$c].'</span></td>
				</tr>
				<tr>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_nric_address_manual[$c].'</span></td>					
				</tr>';
		}
	}

	//shareholders fin part starts here
	if($shareholder_resstatus[$c] == 'shareholderpassholder_'.($c+1)){

		if($shareholder_fin_surname_preview[$c] != '' && $shareholder_fin_surname_preview[$c] != 'null'){
		$mail_name = $shareholder_fin_name[$c];			
		$shareholder_fin_preview[$c] = '<h4>Individual Shareholder #'.($c+1).' - '.$shareholder_fin_name[$c].'</h4>
			
				<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
				  <tbody>
					<tr>
						<td width="50%" valign="top">	
					<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
				  <tbody>
					<tr>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_name[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_gender_preview[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_nationality_preview[$c].'</span></td>
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
					  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_passport_preview[$c].'</span></td>								
					</tr>
					  <tr>
					    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
					   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_country_preview[$c].'</span></td>								
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_expirydate_preview[$c].'</span></td>								
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
					  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_dob_preview[$c].'</span></td>								
					</tr>
					<tr>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_finAddress[$c].'</span></td>
					</tr>
			
					<tr class="row">
						<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_front_employer[$c].'</span></td>
					</tr>
					<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_front_occupation[$c].'</span></td>								
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_front_finno[$c].'</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_front_issueddate[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_front_expirydate[$c].'</span></td>
					</tr>';

		}elseif($shareholder_fin_passport_manual[$c] != '' && $shareholder_fin_passport_manual[$c] != 'null'){
			$mail_name = $shareholder_surname[$c];
			$shareholder_fin_manual[$c] = '<h4>Individual Shareholder #'.($c+1).' - '.$shareholder_surname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_nationality_manual[$c].'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_passport_manual[$c].'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_country_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_expirydate_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_dob_manual[$c].'</span></td>								
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_finAddress[$c].'</span></td>
				</tr>
			
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_employer_manual[$c].'</span></td>
				</tr>
				<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_occupation_manual[$c].'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_finno_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_workpassissueddate_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholder_fin_workpassexpirydate_manual[$c].'</span></td>
				</tr>';
		}

	}
	//shareholders fin part ends here

	//shareholders contact details
	if($shareholder_resstatus[$c] == 'shareholdernonresident_'.($c+1) || $shareholder_resstatus[$c] == 'shareholdercitizenpr_'.($c+1) || $shareholder_resstatus[$c] == 'shareholderpassholder_'.($c+1)){
			$shareholder_contact[$c] = '<tr class="row">
					<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholderhandphone[$c].' <br>'.$shareholderOfficephone[$c].'</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$shareholderbusinessemail[$c].' <br>'.$shareholderPersonalemail[$c].'</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholderpep[$c].'</span></td>
				</tr>';
				
			if($shareholderpep == 'Yes'){
			$shareholder_contact[$c] .= '<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP in which country</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholdercountrypep[$c].'</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Role of PEP</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholderrolePep[$c].'</span></td>
				</tr>
				<tr>					 
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP since</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$shareholderpepFrom[$c].' - '.$shareholderpepTo[$c].'</span></td>
				</tr>';
			}			

			$shareholder_contact[$c] .= '</tbody>
					  	</table></td>
					<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
			  <tbody>
			  	<tr>
					<td style="border-bottom:1px solid #ccc"><span>Beneficial Owner</span></td>
					<td style="border-bottom:1px solid #ccc"><span style="font-weight:bold">'.$shareholder_bowner[$c].'</span></td>
				</tr>
				<tr>
					<td><span>Uploaded documents:</span></td>
					<td><span></span></td>
				</tr>
				<tr>								
					<td colspan="2" style="border-bottom:1px solid #ccc" valign="top">';
			if($shareholdernonresidentuploadpassport[$c] != null && $shareholdernonresidentuploadpassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholdernonresidentuploadpassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($shareholdernonresidentaddress[$c] != null && $shareholdernonresidentaddress[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholdernonresidentaddress[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($shareholderFINuploadPassport[$c] != null && $shareholderFINuploadPassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholderFINuploadPassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($shareholderfincardfront[$c] != null && $shareholderfincardfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholderfincardfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($shareholderfincardback[$c] != null && $shareholderfincardback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholderfincardback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($shareholderFINaddress[$c] != null && $shareholderFINaddress[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholderFINaddress[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($shareholderSGnricfront[$c] != null && $shareholderSGnricfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholderSGnricfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($shareholderSGnricback[$c] != null && $shareholderSGnricback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $shareholderSGnricback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = SHAREHOLDER_SERVER_FILE_PATH.$img;
				$shareholder_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}

		$shareholder_contact[$c] .= '</td>
				</tr>	
				</tbody>
			</table></td>
				</tr>
				  </tbody>
				</table>	
			<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';
	}

}

//=================director and shareholder part starts here======================//
//director and shareholders non resident passport preview section

$di_nr_pass_preview = array();
$di_nr_pass_manual = array();
$di_nric_preview = array();
$di_nric_manual = array();
$di_fin_preview = array();
$di_fin_manual = array();
$di_contact = array();

for($c = 0; $c < $counter2; $c++){

	if($di_resstatus[$c] == 'dinonresident_'.($c+1)){
		if($di_surname_preview[$c] != '' && $di_surname_preview[$c] != 'null'){
		$mail_name = $di_surname[$c];
		$di_nr_pass_preview[$c] = '<h4>Director and Shareholder #'.($c+1).' - '.$di_surname[$c].'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_dob_preview[$c].'</span></td>
			</tr>';
		//director and shareholder non resident passport manual section
		}elseif($di_passport_manual[$c] != '' && $di_passport_manual[$c] != 'null'){
		$mail_name = $di_surname[$c];
		$di_nr_pass_manual[$c] = '<h4>Director and Shareholder #'.($c+1).' - '.$di_surname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_gender_manual[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nationality_manual[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_passport_manual[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_country_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_dateissued_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_expirydate_manual[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_birthplace_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_dob_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_address[$c].'</span></td>
			</tr>';
		}
	}

	//director and shareholder nric preview section
	if($di_resstatus[$c] == 'dicitizenpr_'.($c+1)){
		if($di_nric_front_no[$c] != '' && $di_nric_front_no[$c] != 'null'){
		$mail_name = $di_surname[$c];
		$di_nric_preview[$c] = '<h4>Director and Shareholder #'.($c+1).' - '.$di_surname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_front_gender[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_front_no[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_front_nation[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_front_dob[$c].'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_front_address[$c].'</span></td>					
			</tr>';
		
		//director and shareholder nric manual
		}elseif($di_nric_no_manual[$c] != '' && $di_nric_no_manual[$c] != 'null'){
		$mail_name = $di_surname[$c];
		$di_nric_manual[$c] = '<h4>Director and Shareholder #'.($c+1).' - '.$di_surname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_no_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_nation_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_dob_manual[$c].'</span></td>
				</tr>
				<tr>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_nric_address_manual[$c].'</span></td>					
				</tr>';
		}
	}

	//director and shareholders fin part starts here
	if($di_resstatus[$c] == 'dipassholder_'.($c+1)){

		if($di_fin_surname_preview[$c] != '' && $di_fin_surname_preview[$c] != 'null'){
			$mail_name = $di_fin_name[$c];		
			$di_fin_preview[$c] = '<h4>Director and Shareholder #'.($c+1).' - '.$di_surname[$c].'</h4>
			
				<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
				  <tbody>
					<tr>
						<td width="50%" valign="top">	
					<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
				  <tbody>
					<tr>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_name[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_gender_preview[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_nationality_preview[$c].'</span></td>
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
					  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_passport_preview[$c].'</span></td>								
					</tr>
					  <tr>
					    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
					   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_country_preview[$c].'</span></td>								
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_expirydate_preview[$c].'</span></td>								
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
					  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_dob_preview[$c].'</span></td>								
					</tr>
					<tr>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
						<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_finAddress[$c].'</span></td>
					</tr>
			
					<tr class="row">
						<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_front_employer[$c].'</span></td>
					</tr>
					<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_front_occupation[$c].'</span></td>								
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_front_finno[$c].'</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_front_issueddate[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_front_expirydate[$c].'</span></td>
					</tr>';

		}elseif($di_fin_passport_manual[$c] != '' && $di_fin_passport_manual[$c] != 'null'){
			$mail_name = $di_surname[$c];
			$di_fin_manual[$c] = '<h4>Director and Shareholder #'.($c+1).' - '.$di_surname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_nationality_manual[$c].'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_passport_manual[$c].'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_country_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_expirydate_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_dob_manual[$c].'</span></td>								
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_finAddress[$c].'</span></td>
				</tr>
			
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_employer_manual[$c].'</span></td>
				</tr>
				<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_occupation_manual[$c].'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_finno_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_workpassissueddate_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$di_fin_workpassexpirydate_manual[$c].'</span></td>
				</tr>';
		}

	}

	//director and shareholders contact details
	if($di_resstatus[$c] == 'dinonresident_'.($c+1) || $di_resstatus[$c] == 'dicitizenpr_'.($c+1) || $di_resstatus[$c] == 'dipassholder_'.($c+1)){
			$di_contact[$c] = '<tr class="row">
					<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$dihandphone[$c].' <br>'.$diOfficephone[$c].'</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$dibusinessemail[$c].' <br>'.$diPersonalemail[$c].'</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dipep[$c].'</span></td>
				</tr>';
				
			if($dipep[$c] == 'Yes'){
			$di_contact[$c] .= '<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP in which country</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dicountrypep[$c].'</span></td>
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>Role of PEP</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dirolePep[$c].'</span></td>
				</tr>
				<tr>					 
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span>PEP since</span></td>
					<td style=" border-bottom:1px solid #ccc; padding:10px 10px;"><span style="font-weight:bold">'.$dipepFrom[$c].' - '.$dipepTo[$c].'</span></td>
				</tr>';
			}

			$di_contact[$c] .= '</tbody>
					  	</table></td>
					<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
			  <tbody>
			  	<tr>
					<td style="border-bottom:1px solid #ccc"><span>Beneficial Owner</span></td>
					<td style="border-bottom:1px solid #ccc"><span style="font-weight:bold">'.$di_bowner[$c].'</span></td>
				</tr>
				<tr>
					<td><span>Uploaded documents:</span></td>
					<td><span></span></td>
				</tr>
				<tr>								
					<td colspan="2" style="border-bottom:1px solid #ccc" valign="top">';
			if($dishareholdernonresidentuploadpassport[$c] != null && $dishareholdernonresidentuploadpassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholdernonresidentuploadpassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($dishareholdernonresidentaddress[$c] != null && $dishareholdernonresidentaddress[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholdernonresidentaddress[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($dishareholderFINuploadPassport[$c] != null && $dishareholderFINuploadPassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholderFINuploadPassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($dishareholderfincardfront[$c] != null && $dishareholderfincardfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholderfincardfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($dishareholderfincardback[$c] != null && $dishareholderfincardback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholderfincardback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($dishareholderFINaddress[$c] != null && $dishareholderFINaddress[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholderFINaddress[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($dishareholderSGnricfront[$c] != null && $dishareholderSGnricfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholderSGnricfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($dishareholderSGnricback[$c] != null && $dishareholderSGnricback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $dishareholderSGnricback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = DISHAREHOLDER_SERVER_FILE_PATH.$img;
				$di_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
		
		$di_contact[$c] .= '</td>
				</tr>	
				</tbody>
			</table></td>
				</tr>
				  </tbody>
				</table>	
			<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';
	}

}

//=================corporate shareholder part starts here======================//
//corporate shareholders non resident passport preview section

$corporate_nr_pass_preview = array();
$corporate_nr_pass_manual = array();
$corporate_nric_preview = array();
$corporate_nric_manual = array();
$corporate_fin_preview = array();
$corporate_fin_manual = array();
$corporate_contact = array();

for($c = 0; $c < $counter3; $c++){

	$corporate_info[$c] = '<h4>Corporate Shareholder #'.($c+1).' - '.$corporate_surname[$c].'</h4>
		
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of Incorporation</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_country[$c].'</span></td>
			</tr>';

	if($corporate_resstatus[$c] == 'corporatenonresident_'.($c+1)){
		if($corporate_surname_preview[$c] != '' && $corporate_surname_preview[$c] != 'null'){
		$mail_name = $corporate_surname[$c];
		$corporate_nr_pass_preview[$c] = '<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_certificate[$c].' '.$corporate_certificate1[$c].'</span></td>
			</tr>
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_representname[$c].'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_birthplace_preview[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_dob_preview[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_resaddress[$c].'</span></td>
			</tr>';
		//corporate shareholder non resident passport manual section
		}elseif($corporate_passport_manual[$c] != '' && $corporate_passport_manual[$c] != 'null'){
		$mail_name = $corporate_surname[$c];
		$corporate_nr_pass_manual[$c] = '<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_certificate[$c].' '.$corporate_certificate1[$c].'</span></td>
			</tr>
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_representname[$c].'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_gender_manual[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nationality_manual[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_passport_manual[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_country_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_dateissued_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_expirydate_manual[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_birthplace_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_dob_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_resaddress[$c].'</span></td>
			</tr>';
		}
	}

	//corporate shareholder nric preview section
	if($corporate_resstatus[$c] == 'corporatecitizenpr_'.($c+1)){
		if($corporate_nric_front_no[$c] != '' && $corporate_nric_front_no[$c] != 'null'){
		$mail_name = $corporate_surname[$c];
		$corporate_nric_preview[$c] = '<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_certificate[$c].' '.$corporate_certificate1[$c].'</span></td>
			</tr>
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_representname[$c].'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_front_gender[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_front_no[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_front_nation[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_front_dob[$c].'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_front_address[$c].'</span></td>					
			</tr>';
		
		//director and shareholder nric manual
		}elseif($corporate_nric_no_manual != '' && $corporate_nric_no_manual != 'null'){
		$mail_name = $corporate_surname[$c];
		$corporate_nric_manual[$c] = '<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_certificate[$c].' '.$corporate_certificate1[$c].'</span></td>
			</tr>
			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_representname[$c].'</span></td>								
			</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_no_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_nation_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_dob_manual[$c].'</span></td>
				</tr>
				<tr>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_nric_address_manual[$c].'</span></td>					
				</tr>';
		}
	}

	//corporate shareholders fin part starts here
	if($corporate_resstatus[$c] == 'corporatepassholder_'.($c+1)){

		if($corporate_fin_front_finno != '' && $corporate_fin_front_finno != 'null'){
			$mail_name = $corporate_surname[$c];			
			$corporate_fin_preview[$c] = '<tr>								
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
						<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_certificate[$c].' '.$corporate_certificate1[$c].'</span></td>
					</tr>
					<tr class="row">
						<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_representname[$c].'</span></td>								
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Gender</span></td>
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_gender_preview[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Nationality</span></td>
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_nationality_preview[$c].'</span></td>
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Passport</span></td>
					  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_passport_preview[$c].'</span></td>								
					</tr>
					  <tr>
					    <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Country of issue</span></td>
					   <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_country_preview[$c].'</span></td>								
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
					  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_expirydate_preview[$c].'</span></td>								
					</tr>
					<tr>
					 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Birth date</span></td>
					  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_dob_preview[$c].'</span></td>								
					</tr>
					<tr>
						<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Address</span></td>
						<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_finAddress[$c].'</span></td>
					</tr>
			
					<tr class="row">
						<td style="border-bottom:1px solid #ccc; background:#ddd; padcorporateng:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Employer</span></td>
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_front_employer[$c].'</span></td>
					</tr>
					<tr>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Occupation</span></td>
					 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_front_occupation[$c].'</span></td>								
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>FIN </span></td>
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_front_finno[$c].'</span></td>
					</tr>
					<tr>								
					<tr>
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Issue date</span></td>
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_front_issueddate[$c].'</span></td>
					</tr>
					<tr>								
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
						<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_front_expirydate[$c].'</span></td>
					</tr>';

		}elseif($corporate_fin_passport_manual != '' && $corporate_fin_passport_manual != 'null'){
			$mail_name = $corporate_surname[$c];
			$corporate_fin_manual[$c] = '<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Documents Uploaded</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_certificate[$c].' '.$corporate_certificate1[$c].'</span></td>
				</tr>
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">Corporate Representative</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_representname[$c].'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_nationality_manual[$c].'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_passport_manual[$c].'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_country_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_expirydate_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_dob_manual[$c].'</span></td>								
				</tr>
				<tr>
					<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Address</span></td>
					<td style=" border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_finAddress[$c].'</span></td>
				</tr>
			
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padcorporateng:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_employer_manual[$c].'</span></td>
				</tr>
				<tr>
				<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Occupation</span></td>
				 <td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_occupation_manual[$c].'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_finno_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_workpassissueddate_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padcorporateng:10px 10px"><span style="font-weight:bold">'.$corporate_fin_workpassexpirydate_manual[$c].'</span></td>
				</tr>';
		}

	}

	//corporate shareholder contact details
	/*if($corporate_resstatus[$c] == 'corporatenonresident_'.($c+1) || $corporate_resstatus[$c] == 'corporatecitizenpr_'.($c+1) || $corporate_resstatus[$c] == 'corporatepassholder_'.($c+1)){*/
		$corporate_contact[$c] = '<tr class="row">
				<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_contact[$c].'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$corporate_email[$c].'</span></td>
			</tr>';
			/*</tbody>
		</table></td>
					<td colspan="2" width="50%" style="text-align:center"><div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-front.jpg" class="img-fluid" alt=""></div>
			<div class="uploaded-image"><img src="http://incorp.asia/forms/pdf/images/passport-nric-back.jpg" class="img-fluid" alt=""></div></td>
				</tr>
				</tbody>							
				</table>

		<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents NRIC front and back, nationality Singaporean)</p>';*/

		$corporate_contact[$c] .= '</tbody>
					  	</table></td>
					<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
			  <tbody>
			  	<tr>
					<td style="border-bottom:1px solid #ccc"><span>Beneficial Owner</span></td>
					<td style="border-bottom:1px solid #ccc"><span style="font-weight:bold">'.$corporate_bowner[$c].'</span></td>
				</tr>
				<tr>
					<td><span>Uploaded documents:</span></td>
					<td><span></span></td>
				</tr>
				<tr>								
					<td colspan="2" style="border-bottom:1px solid #ccc" valign="top">';
			if($CorporateCertificate[$c] != null && $CorporateCertificate[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $CorporateCertificate[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($CorporateCertificate1[$c] != null && $CorporateCertificate1[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $CorporateCertificate1[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporatenonresidentuploadpassport[$c] != null && $corporatenonresidentuploadpassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporatenonresidentuploadpassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporatenonresidentaddress[$c] != null && $corporatenonresidentaddress[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporatenonresidentaddress[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporateFINuploadPassport[$c] != null && $corporateFINuploadPassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporateFINuploadPassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporatefincardfront[$c] != null && $corporatefincardfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporatefincardfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporatefincardback[$c] != null && $corporatefincardback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporatefincardback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporateFINaddress[$c] != null && $corporateFINaddress[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporateFINaddress[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporateSGnricfront[$c] != null && $corporateSGnricfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporateSGnricfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($corporateSGnricback[$c] != null && $corporateSGnricback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $corporateSGnricback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = CORPORATE_SERVER_FILE_PATH.$img;
				$corporate_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			
		
		$corporate_contact[$c] .= '</td>
				</tr>	
				</tbody>
			</table></td>
				</tr>
				  </tbody>
				</table>	
			<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';
	/*}*/
}

$bo_nr_pass_preview = array();
$bo_nr_pass_manual = array();
$bo_nric_preview = array();
$bo_nric_manual = array();
$bo_fin_preview = array();
$bo_fin_manual = array();
$bo_contact = array();

for($c = 0 ;$c <= $bocounter; $c++){

	//bo non resident passport preview section
	if($bo_resstatus[$c] == 'bononresident_'.($c+1).'_'.$bo_getboname){
		if($bo_surname_preview[$c] != '' && $bo_surname_preview[$c] != 'null'){
		$bo_nr_pass_preview[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_dob_preview[$c].'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$address[$c].'</span></td>
			</tr>';
		//bo non resident passport manual section
		}elseif($bo_passport_manual[$c] != '' && $bo_passport_manual[$c] != 'null'){
		$bo_nr_pass_manual[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_gender_manual[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nationality_manual[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_passport_manual[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_country_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_dateissued_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_expirydate_manual[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_birthplace_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_dob_manual[$c].'</span></td>
			</tr>';
		}
	}

	//bo NRIC preview section
	if($bo_resstatus[$c] == 'bocitizenpr_'.($c+1).'_'.$bo_getboname){
		if($bo_nric_front_no[$c] != '' && $bo_nric_front_no[$c] != 'null'){
		$bo_nric_preview[$c] =  '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_front_gender[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_front_no[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_front_nation[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_front_dob[$c].'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_front_address[$c].'</span></td>					
			</tr>';
		//bo nric manual
		}elseif($bo_nric_no_manual[$c] != '' && $bo_nric_no_manual[$c] != 'null'){
		$bo_nric_manual[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_no_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_nation_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_dob_manual[$c].'</span></td>
				</tr>
				<tr>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_nric_address_manual[$c].'</span></td>					
				</tr>';
		}
	}

	//bo FIN preview
	if($bo_resstatus[$c] == 'bopassholder_'.($c+1).'_'.$bo_getboname){
		if($bo_fin_surname_preview[$c] != '' && $bo_fin_surname_preview[$c] != 'null'){
		$bo_fin_preview[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_dob_preview[$c].'</span></td>								
			</tr>
			

			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_front_employer[$c].'</span></td>
			</tr>
			<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_front_occupation[$c].'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_front_finno[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_front_issueddat[$c].'e</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_front_expirydate[$c].'</span></td>
			</tr>';
		//bo fin manual
		}elseif($bo_fin_passport_manual[$c] != '' && $bo_fin_passport_manual[$c] != 'null'){
		$bo_fin_manual[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_nationality_manual[$c].'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_passport_manual[$c].'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_country_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_expirydate_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_dob_manual[$c].'</span></td>								
				</tr>
						
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_employer_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_occupation_manual[$c].'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_finno_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_workpassissueddate_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bo_fin_workpassexpirydate_manual[$c].'</span></td>
				</tr>';
		}
	}

	//bo contact details
	if($bo_resstatus[$c] == 'bononresident_'.($c+1).'_'.$bo_getboname || $bo_resstatus[$c] == 'bocitizenpr_'.($c+1).'_'.$bo_getboname || $bo_resstatus[$c] == 'bopassholder_'.($c+1).'_'.$bo_getboname){
		$bo_contact[$c] = '<tr class="row">
				<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bocontactno[$c].' </span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$boemail[$c].'</span></td>
			</tr>';		

		$bo_contact[$c] .= '</tbody>
					  	</table></td>
					<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
			  <tbody>
				<tr>
					<td><span>Uploaded documents:</span></td>
					<td><span></span></td>
				</tr>
				<tr>								
					<td colspan="2" style="border-bottom:1px solid #ccc" valign="top">';
			if($bo_passport[$c] != null && $bo_passport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_passport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bo_addressProof[$c] != null && $bo_addressProof[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_addressProof[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bo_finPassport[$c] != null && $bo_finPassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_finPassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bo_finCardFront[$c] != null && $bo_finCardFront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_finCardFront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bo_finCardBack[$c] != null && $bo_finCardBack[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_finCardBack[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bo_finAddressProof[$c] != null && $bo_finAddressProof[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_finAddressProof[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bo_nricfront[$c] != null && $bo_nricfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_nricfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bo_nricback[$c] != null && $bo_nricback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bo_nricback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bo_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}

		$bo_contact[$c] .= '</td>
				</tr>	
				</tbody>
			</table></td>
				</tr>
				  </tbody>
				</table>	
			<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';
	}

}

$bodi_nr_pass_preview = array();
$bodi_nr_pass_manual = array();
$bodi_nric_preview = array();
$bodi_nric_manual = array();
$bodi_fin_preview = array();
$bodi_fin_manual = array();
$bodi_contact = array();

for($c = 0 ;$c <= $bodicounter; $c++){

	//bo non resident passport preview section
	if($bodi_resstatus[$c] == 'bononresident_'.($c+1).'_'.$bodi_getboname){
		if($bodi_surname_preview[$c] != '' && $bodi_surname_preview[$c] != 'null'){
		$bodi_nr_pass_preview[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_dob_preview[$c].'</span></td>								
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$address[$c].'</span></td>
			</tr>';
		//bo non resident passport manual section
		}elseif($bodi_passport_manual[$c] != '' && $bodi_passport_manual[$c] != 'null'){
		$bodi_nr_pass_manual[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_gender_manual[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nationality_manual[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_passport_manual[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_country_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_dateissued_manual[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_expirydate_manual[$c].'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_birthplace_manual[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_dob_manual[$c].'</span></td>
			</tr>';
		}
	}

	//bo NRIC preview section
	if($bodi_resstatus[$c] == 'bocitizenpr_'.($c+1).'_'.$bodi_getboname){
		if($bodi_nric_front_no[$c] != '' && $bodi_nric_front_no[$c] != 'null'){
		$bodi_nric_preview[$c] =  '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_surname[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_front_gender[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_front_no[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_front_nation[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_front_dob[$c].'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_front_address[$c].'</span></td>					
			</tr>';
		//bo nric manual
		}elseif($bodi_nric_no_manual[$c] != '' && $bodi_nric_no_manual[$c] != 'null'){
		$bodi_nric_manual[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_no_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_nation_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_dob_manual[$c].'</span></td>
				</tr>
				<tr>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_nric_address_manual[$c].'</span></td>					
				</tr>';
		}
	}

	//bo FIN preview
	if($bodi_resstatus[$c] == 'bopassholder_'.($c+1).'_'.$bodi_getboname){
		if($bodi_fin_surname_preview[$c] != '' && $bodi_fin_surname_preview[$c] != 'null'){
		$bodi_fin_preview[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_name[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_gender_preview[$c].'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_nationality_preview[$c].'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_passport_preview[$c].'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_country_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_expirydate_preview[$c].'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_dob_preview[$c].'</span></td>								
			</tr>
			

			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_front_employer[$c].'</span></td>
			</tr>
			<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_front_occupation[$c].'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_front_finno[$c].'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_front_issueddat[$c].'e</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_front_expirydate[$c].'</span></td>
			</tr>';
		//bo fin manual
		}elseif($bodi_fin_passport_manual[$c] != '' && $bodi_fin_passport_manual[$c] != 'null'){
		$bodi_fin_manual[$c] = '<h4>Beneficial Owner - '.$bofullname[$c].'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_surname[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_gender_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_nationality_manual[$c].'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_passport_manual[$c].'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_country_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_expirydate_manual[$c].'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_dob_manual[$c].'</span></td>								
				</tr>
						
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_employer_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_occupation_manual[$c].'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_finno_manual[$c].'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_workpassissueddate_manual[$c].'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bodi_fin_workpassexpirydate_manual[$c].'</span></td>
				</tr>';
		}
	}

	//bo contact details
	if($bodi_resstatus[$c] == 'bononresident_'.($c+1).'_'.$bodi_getboname || $bodi_resstatus[$c] == 'bocitizenpr_'.($c+1).'_'.$bodi_getboname || $bodi_resstatus[$c] == 'bopassholder_'.($c+1).'_'.$bodi_getboname){
		$bodi_contact[$c] = '<tr class="row">
				<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$bocontactno[$c].' </span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$boemail[$c].'</span></td>
			</tr>';		

		$bodi_contact[$c] .= '</tbody>
					  	</table></td>
					<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
			  <tbody>
				<tr>
					<td><span>Uploaded documents:</span></td>
					<td><span></span></td>
				</tr>
				<tr>								
					<td colspan="2" style="border-bottom:1px solid #ccc" valign="top">';
			if($bodi_passport[$c] != null && $bodi_passport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_passport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bodi_addressProof[$c] != null && $bodi_addressProof[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_addressProof[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bodi_finPassport[$c] != null && $bodi_finPassport[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_finPassport[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bodi_finCardFront[$c] != null && $bodi_finCardFront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_finCardFront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bodi_finCardBack[$c] != null && $bodi_finCardBack[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_finCardBack[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bodi_finAddressProof[$c] != null && $bodi_finAddressProof[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_finAddressProof[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bodi_nricfront[$c] != null && $bodi_nricfront[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_nricfront[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}
			if($bodi_nricback[$c] != null && $bodi_nricback[$c] != ""){
				$img = str_replace("C:/fakepath/", "", $bodi_nricback[$c]);
				$img = str_replace("C:\\fakepath\\", "", $img);
				$img = BO_SERVER_FILE_PATH.$img;
				$bodi_contact[$c] .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
			}

		$bodi_contact[$c] .= '</td>
				</tr>	
				</tbody>
			</table></td>
				</tr>
				  </tbody>
				</table>	
			<p style="font-size:13px; font-style:italic;">(Sample with uploaded documents, Foreigner - EP)</p>';
	}

}

//from other dropdown starts here
$otherresstatus = $_POST['otherresstatus'];
//other passport preview
$othersurname = $_POST['othersurname'];
$othergivenname = $_POST['othergivenname'];
$othername = $othersurname.' '.$othergivenname;
$otherpassport = $_POST['otherpassport'];
$othernationality = $_POST['othernationality'];
$othercountry = $_POST['othercountry'];
$othergender = $_POST['othergender'];
if($othergender == 'on'){
	$othergender = 'Male';
}else{
	$othergender = 'Female';
}
$otherdob = $_POST['otherdob'];
$otherdateexpiry = $_POST['otherdateexpiry'];

//other passport manual
$othermdatepick = $_POST['othermdatepick'];
$othermpob = $_POST['othermpob'];
$othermpassport = $_POST['othermpassport'];
$othermnationality = $_POST['othermnationality'];
$othermcountry = $_POST['othermcountry'];
$othermgender = $_POST['othermgender'];
if($othermgender == 'on'){
	$othermgender = 'Male';
}else{
	$othermgender = 'Female';
}
$othermdob = $_POST['othermdob'];
$othermdateexpiry = $_POST['othermdateexpiry'];

//other nric manual
$otherpreviewnricno = $_POST['otherpreviewnricno'];
$otherpreviewnricgender = $_POST['otherpreviewnricgender'];
if($otherpreviewnricgender == 'on'){
	$otherpreviewnricgender = 'Male';
}else{
	$otherpreviewnricgender = 'Female';
}
$otherpreviewnricdoi = $_POST['otherpreviewnricdoi'];
$otherpreviewnriccob = $_POST['otherpreviewnriccob'];
$otherpreviewnricpostal = $_POST['otherpreviewnricpostal'];
$otherpreviewnricstreet = $_POST['otherpreviewnricstreet'];
$otherpreviewnricfloor = $_POST['otherpreviewnricfloor'];
$otherpreviewnricunit = $_POST['otherpreviewnricunit'];
$otherpreviewnricaddress = $otherpreviewnricstreet.' <br>'.$otherpreviewnricfloor.' '.$otherpreviewnricunit.' <br>'.$otherpreviewnricpostal;

//other nric front previvew
$otherpreviewfrontnricno = $_POST['otherpreviewfrontnricno'];
$otherpreviewfrontnricgender = $_POST['otherpreviewfrontnricgender'];
if($otherpreviewfrontnricgender == 'on'){
	$otherpreviewfrontnricgender = 'Male';
}else{
	$otherpreviewfrontnricgender = 'Female';
}
$otherpreviewfrontnricdoi = $_POST['otherpreviewfrontnricdoi'];
$otherpreviewfrontnriccob = $_POST['otherpreviewfrontnriccob'];
$otherpreviewfrontnricpostal = $_POST['otherpreviewfrontnricpostal'];
$otherpreviewfrontnricstreet = $_POST['otherpreviewfrontnricstreet'];
$otherpreviewfrontnricfloor = $_POST['otherpreviewfrontnricfloor'];
$otherpreviewfrontnricunit = $_POST['otherpreviewfrontnricunit'];
$otherpreviewfrontnricaddress = $otherpreviewfrontnricstreet.' <br>'.$otherpreviewfrontnricfloor.' '.$otherpreviewfrontnricunit.' <br>'.$otherpreviewfrontnricpostal;

//other nric back preview
$otherpreviewbacknricno = $_POST['otherpreviewbacknricno'];
$otherpreviewbacknricgender = $_POST['otherpreviewbacknricgender'];
$otherpreviewbacknricdoi = $_POST['otherpreviewbacknricdoi'];
$otherpreviewbacknriccob = $_POST['otherpreviewbacknriccob'];
$otherpreviewbacknricpostal = $_POST['otherpreviewbacknricpostal'];
$otherpreviewbacknricstreet = $_POST['otherpreviewbacknricstreet'];
$otherpreviewbacknricfloor = $_POST['otherpreviewbacknricfloor'];
$otherpreviewbacknricunit = $_POST['otherpreviewbacknricunit'];

//other agent FIN preview
$otherfinsurname = $_POST['otherfinsurname'];
$otherfingivenname = $_POST['otherfingivenname'];
$otherfinname = $otherfinsurname.' '.$otherfingivenname;
$otherfingender = $_POST['otherfingender'];
if($otherfingender == 'on'){
	$otherfingender = 'Male';
}else{
	$otherfingender = 'Female';
}
$otherfinpassport = $_POST['otherfinpassport'];
$otherfinnationality = $_POST['otherfinnationality'];
$otherfincountry = $_POST['otherfincountry'];
$otherfintimepick = $_POST['otherfintimepick'];
$otherfindateexpiry = $_POST['otherfindateexpiry'];
$otherfindob = $_POST['otherfindob'];
$otherfinpob = $_POST['otherfinpob'];

$otherfinfrontemp = $_POST['otherfinfrontemp'];
$otherfinfrontocc = $_POST['otherfinfrontocc'];
$otherfinfrontdoi = $_POST['otherfinfrontdoi'];
$otherfinfrontdoe = $_POST['otherfinfrontdoe'];
$otherfinfrontno = $_POST['otherfinfrontno'];

$otherfinbackemp = $_POST['otherfinbackemp'];
$otherfinbackocc = $_POST['otherfinbackocc'];
$otherfinbackdoi = $_POST['otherfinbackdoi'];
$otherfinbackdoe = $_POST['otherfinbackdoe'];
$otherfinbackno = $_POST['otherfinbackno'];

//other fin manual
$othermfinpassportno = $_POST['othermfinpassportno'];
$othermfinnationality = $_POST['othermfinnationality'];
$othermfincountry = $_POST['othermfincountry'];
$othermfindatetimepicker = $_POST['othermfindatetimepicker'];
$othermfindateexpiry = $_POST['othermfindateexpiry'];
$othermfindob = $_POST['othermfindob'];
$othermfinpob = $_POST['othermfinpob'];
$othermfinemployer = $_POST['othermfinemployer'];
$othermfinocc = $_POST['othermfinoccupation'];
$othermwfindoi = $_POST['othermwfindoi'];
$othermwfindoe = $_POST['othermwfindoe'];
$othermfinnumber = $_POST['othermfinnumber'];
$othermfingender = $_POST['othermfingender'];
if($othermfingender == 'on'){
	$othermfingender = 'Male';
}else{
	$othermfingender = 'Female';
}

//other image fields
$otheruploadpassport = $_POST['otheruploadpassport'];
$otheraddress = $_POST['otheraddress'];
$otherresaddress = $_POST['otherresaddress'];
$otherfinuploadPassport = $_POST['otherfinuploadPassport'];
$otherfinfrontupload = $_POST['otherfinfrontupload'];
$otherfinbackupload = $_POST['otherfinbackupload'];
$otherfinaddress = $_POST['otherfinaddress'];
$otherfinresaddress = $_POST['otherfinresaddress'];
$othernricfrontup = $_POST['othernricfrontup'];
$othernricbackup = $_POST['othernricbackup'];


$other_nr_pass_preview = "";
$other_nr_pass_manual = "";
$other_nric_preview = "";
$other_nric_manual = "";
$other_fin_preview = "";
$other_fin_manual = "";
$other_contact = "";
	//other agent passport preview
	if($otherresstatus == 'othernonresident'){	
		if($othersurname != '' && $othersurname != 'null'){
		$other_nr_pass_preview = '<h4>Other Agent - '.$othername.'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_full_name.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othergender.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othernationality.'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpassport.'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othercountry.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherdateexpiry.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherdob.'</span></td>								
			</tr>';
		//other agent non resident passport manual section
		}elseif($othermpassport != '' && $othermpassport != 'null'){
		$other_nr_pass_manual = '<h4>Other Agent - '.$othername.'</h4>
			
		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_full_name.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermgender.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermnationality.'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermpassport.'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermcountry.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issued date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermdatepick.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermdateexpiry.'</span></td>								
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermpob.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermdob.'</span></td>
			</tr>';
		}
	}

	if($otherresstatus == 'othercitizenpr'){	
		//other agent NRIC preview section	
		if($otherpreviewfrontnricno != '' && $otherpreviewfrontnricno != 'null'){
		$other_nric_preview =  '<h4>Other Agent - '.$agent_full_name.'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_full_name.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewfrontnricgender.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewfrontnricno.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewfrontnriccob.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewfrontnricdoi.'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewfrontnricaddress.'</span></td>					
			</tr>';
		//other agent nric manual
		}elseif($otherpreviewnricno != '' && $otherpreviewnricno != 'null'){
		$other_nric_manual =  '<h4>Other Agent - '.$agent_full_name.'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_full_name.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewnricgender.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>NRIC</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewnricno.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthplace</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewnriccob.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birthdate</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewnricdoi.'</span></td>
			</tr>
			<tr>
			 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Address</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherpreviewnricaddress.'</span></td>					
			</tr>';
		}
	}

	if($otherresstatus == 'otherpassholder'){
		//other agent FIN preview
		if($otherfinsurname != '' && $otherfinsurname != 'null'){
		$other_fin_preview = '<h4>Other Agent - '.$otherfinname.'</h4>

		<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
		  <tbody>
			<tr>
				<td width="50%" valign="top">	
			<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
		  <tbody>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_full_name.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfingender.'</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfinnationality.'</span></td>
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfinpassport.'</span></td>								
			</tr>
			  <tr>
			    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
			   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfincountry.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfindateexpiry.'</span></td>								
			</tr>
			<tr>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
			  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfindob.'</span></td>								
			</tr>
			

			<tr class="row">
				<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfinfrontemp.'</span></td>
			</tr>
			<tr>
			<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
			 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfinfrontocc.'</span></td>								
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfinfrontno.'</span></td>
			</tr>
			<tr>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfinfrontdoi.'e</span></td>
			</tr>
			<tr>								
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$otherfinfrontdoe.'</span></td>
			</tr>';
		//other agent fin manual
		}elseif($othermfinpassportno != '' && $othermfinpassportno != 'null'){
		$other_fin_manual = '<h4>Other Agent - '.$otherfinname.'</h4>
			
			<table border="0" cellpadding="0" cellspacing="0" style=" width:100%; border:1px solid #ccc;">
			  <tbody>
				<tr>
					<td width="50%" valign="top">	
				<table border="0" cellpadding="5" cellspacing="0" style="border-bottom:none; width:100%; border-right:1px solid #ccc;">
			  <tbody>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Full name</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_full_name.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Gender</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfingender.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Nationality</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfinnationality.'</span></td>
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Passport</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfinpassportno.'</span></td>								
				</tr>
				  <tr>
				    <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Country of issue</span></td>
				   <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfincountry.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfindateexpiry.'</span></td>								
				</tr>
				<tr>
				 <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Birth date</span></td>
				  <td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfindob.'</span></td>								
				</tr>
						
				<tr class="row">
					<td style="border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px"colspan="2"><span style="font-weight:bold">FIN Details</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Employer</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfinemployer.'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Occupation</span></td>
				 	<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfinocc.'</span></td>								
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>FIN </span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermfinnumber.'</span></td>
				</tr>
				<tr>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Issue date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermwfindoi.'</span></td>
				</tr>
				<tr>								
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span>Expiry date</span></td>
					<td style="border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$othermwfindoe.'</span></td>
				</tr>';
		}
	}

		if($otherresstatus == 'othernonresident' || $otherresstatus == 'othercitizenpr' || $otherresstatus == 'otherpassholder'){
			$other_contact = '<tr class="row">
				<td style=" border-bottom:1px solid #ccc; background:#ddd; padding:15px 10px" colspan="2"><span style="font-weight:bold;">Contact Details</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Contact No.</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_contact_no.'</span></td>
			</tr>
			<tr>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span>Email</span></td>
				<td style=" border-bottom:1px solid #ccc; padding:10px 10px"><span style="font-weight:bold">'.$agent_email.'</span></td>
			</tr></tbody>
					  	</table></td>
					<td width="50%" valign="top"><table border="0" cellpadding="10" cellspacing="0" style="width:100%;">
			  <tbody>
				<tr>
					<td><span>Uploaded documents:</span></td>
					<td><span></span></td>
				</tr>
				<tr>								
					<td colspan="2" style="border-bottom:1px solid #ccc" valign="top">';

				if($otheruploadpassport != null && $otheruploadpassport != ""){
					$img = str_replace("C:/fakepath/", "", $otheruploadpassport);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
				if($otheraddress != null && $otheraddress != ""){
					$img = str_replace("C:/fakepath/", "", $otheraddress);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
				if($otherfinuploadPassport != null && $otherfinuploadPassport != ""){
					$img = str_replace("C:/fakepath/", "", $otherfinuploadPassport);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
				if($otherfinfrontupload != null && $otherfinfrontupload != ""){
					$img = str_replace("C:/fakepath/", "", $otherfinfrontupload);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
				if($otherfinbackupload != null && $otherfinbackupload != ""){
					$img = str_replace("C:/fakepath/", "", $otherfinbackupload);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
				if($otherfinaddress != null && $otherfinaddress != ""){
					$img = str_replace("C:/fakepath/", "", $otherfinaddress);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
				if($othernricfrontup != null && $othernricfrontup != ""){
					$img = str_replace("C:/fakepath/", "", $othernricfrontup);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
				if($othernricbackup != null && $othernricbackup != ""){
					$img = str_replace("C:/fakepath/", "", $othernricbackup);
					$img = str_replace("C:\\fakepath\\", "", $img);
					$img = OTHER_SERVER_FILE_PATH.$img;
					$other_contact .= '<div class="uploaded-image"><img src="'.$img.'" class="img-fluid" alt=""></div>';
				}
			
			$other_contact .= '</td>
				</tr>	
				</tbody>
			</table></td>
				</tr>
				  </tbody>
				</table>';
		}
	
// Set some content to print
$html = <<<EOD
<!DOCTYPE html>
<!-- saved from url=(0039)https://www.winimy.ai/rikvincopy/pdf.php -->
<html lang="en" class="gr__winimy_ai"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>InCorp - On Boarding Customer Due Diligence Form</title>
	<style>
  .row{
   background-color:#999999;
   -webkit-print-color-adjust: exact; 
   }
 </style>
</head>
	
<body data-gr-c-s-loaded="true" style="font-family: 'Roboto', sans-serif !important;">


	<div class="container" style="width:800px; margin:0 auto">
	
				<img src="images/logo.png" style="margin:25px 0">
					<h3 style="margin:25px 0">Customer Due Diligence Acceptance Form</h3>
					<p>Key definitions used in the document</p>
					<ul style="line-height:25px;">
						<li>A "Beneficial Owner", means an individual who ultimately owns or controls (whether through direct or indirect ownership or control) more than 25% of the shares or voting rights or exercises control over the management.</li>
						<li>A "Politcally Exposed Person" (PEP) is a term describing someone who has been entrusted with any prominent public function in Singapore.</li>
					</ul>
					
					<h4 style="border-bottom:2px solid #000; padding-bottom:10px;">Proposed Company Information</h4>
		
	
		<table  cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; border-bottom:none; width:100%;">
				<tbody>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Proposed company name</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$proposedCompanyName1
EOD;
					$html .= $proposed_co_2;
					$html .= $proposed_co_3;
	
					$html .= <<<EOD
					</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Registered address of company</span></td>
EOD;

					$html .= $reg_co_address;

					$html .= <<<EOD
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Business activities</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$businessActivities</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Countries of operation</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$countriesOperation</span></td>
					</tr>
					<tr>
						<td style="border-bottom:1px solid #ccc;"><span>Source of Funds</span></td>
						<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">$sourceFunds</span></td>
					</tr>
				</tbody>
			</table>
		
					<h4 class="mb-3 mt-5 font-weight-bold">Particulars of all Directors, Shareholders and Beneficial Owners</h4>
EOD;

			for($c = 0 ;$c <= $counter; $c++){
				$html .= $director_nr_pass_preview[$c];

				$html .= $director_nr_pass_manual[$c];

				$html .= $director_nric_preview[$c];	
				
				$html .= $director_nric_manual[$c];	

				$html .= $director_fin_preview[$c];

				$html .= $director_fin_manual[$c];
					
				$html .= $director_contact[$c];	
			}		

			for($c = 0 ;$c <= $counter1; $c++){
				$html .= $shareholder_nr_pass_preview[$c];
					
				$html .= $shareholder_nr_pass_manual[$c];
					
				$html .= $shareholder_nric_preview[$c];
					
				$html .= $shareholder_nric_manual[$c];
					
				$html .= $shareholder_fin_preview[$c];

				$html .= $shareholder_fin_manual[$c];

				$html .= $shareholder_contact[$c];
			}
			for($c = 0 ;$c <= $counter2; $c++){

				$html .= $di_nr_pass_preview[$c];

				$html .= $di_nr_pass_manual[$c];

				$html .= $di_nric_preview[$c];

				$html .= $di_nric_manual[$c];

				$html .= $di_fin_preview[$c];

				$html .= $di_fin_manual[$c];

				$html .= $di_contact[$c];
			}
			for($c = 0 ;$c <= $counter3; $c++){

				$html .= $corporate_info[$c];

				$html .= $corporate_nr_pass_preview[$c];

				$html .= $corporate_nr_pass_manual[$c];

				$html .= $corporate_nric_preview[$c];

				$html .= $corporate_nric_manual[$c];

				$html .= $corporate_fin_preview[$c];

				$html .= $corporate_fin_manual[$c];

				$html .= $corporate_contact[$c];
			}
				

				$html .= <<<EOD
					<br><br>
				<hr style="background: #666; height: 2px; width: 100%; margin:25px 0 0 0;">
				<br><br>
					<h3 style="margin:25px 0 10px 0">Shareholding Structure</h3>				
					<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; width:100%;">
						<tbody>
							<tr>
								<td style="border-right:1px solid #ccc"><span>Currency</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">$currency</span></td>
								<td style="border-right:1px solid #ccc"><span>Paid Up capital</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">$paid_up_capital</span></td>
								<td style="border-right:1px solid #ccc"><span>No. of shares</span></td>
								<td style="border-right:1px solid #ccc"><span style="font-weight:bold">$number_of_shares</span></td>
							</tr>							
						</tbody>
					</table>
				
			
				
EOD;
		
					//$html .= $d1_listofshare; 
					$listofshare = '
				<h3 style="margin:25px 0 10px 0">List of Shareholders</h3>				
				<table border="0" cellpadding="10" cellspacing="0" style=" border:1px solid #ccc; width:100%;">
				<tbody style="border: 1px solid #e9ecef !important;">
				<tr class="table-secondary row">
					<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Shareholders</span></td>
					<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Paid up capital</span></td>
					<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>No. of shares</span></td>
					<td style="border-bottom:1px solid #ccc; background:#ddd;"><span>Beneficial owner</span></td>
				</tr>';
						

					for($c = 0; $c < $counter; $c++){
						$listofshare .= '<tr>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$director_surname[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$director_paid_up_capital[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$director_number_of_shares[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold"></span></td>
						</tr>';
					}
					for($c = 0; $c < $counter1; $c++){
						$listofshare .= '<tr>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$shareholder_surname[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$individual_shareholder_paid_up_capital[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$individual_shareholder_number_of_shares[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$shareholder_bowner[$c].'</span></td>
						</tr>';
					}
					for($c = 0; $c < $counter2; $c++){
						$listofshare .= '<tr>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$di_surname[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$director_shareholder_paid_up_capital[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$director_shareholder_number_of_shares[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$di_bowner[$c].'</span></td>
						</tr>';
					}
					for($c = 0; $c <= $counter3; $c++){
						$listofshare .= '<tr>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$corporate_surname[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$corporate_paid_up_capital[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold">'.$corporate_number_of_shares[$c].'</span></td>
							<td style="border-bottom:1px solid #ccc;"><span style="font-weight:bold"></span></td>
						</tr>';
					}	
					$listofshare .= '</tbody></table>';							

					$html .= $listofshare;

						$html .= <<<EOD
						</tbody>
					</table>
EOD;

				for($c = 0 ;$c <= $bocounter; $c++){
					$html .= $bo_nr_pass_preview[$c];

					$html .= $bo_nr_pass_manual[$c];

					$html .= $bo_nric_preview[$c];	
				
					$html .= $bo_nric_manual[$c];	

					$html .= $bo_fin_preview[$c];

					$html .= $bo_fin_manual[$c];
					
					$html .= $bo_contact[$c];	
				}

				for($c = 0 ;$c <= $bodicounter; $c++){
					$html .= $bodi_nr_pass_preview[$c];

					$html .= $bodi_nr_pass_manual[$c];

					$html .= $bodi_nric_preview[$c];	
				
					$html .= $bodi_nric_manual[$c];	

					$html .= $bodi_fin_preview[$c];

					$html .= $bodi_fin_manual[$c];
					
					$html .= $bodi_contact[$c];	
				}

				$html .= $other_nr_pass_preview;

				$html .= $other_nr_pass_manual;

				$html .= $other_nric_preview;

				$html .= $other_nric_manual;

				$html .= $other_fin_preview;

				$html .= $other_fin_manual;

				$html .= $other_contact;
				
				
					$html .= <<<EOD
						<br>
				
					<h3 class="mb-5" style="border-bottom:2px solid #000; display: block; width: 100%; margin:35px 0 0 0;
					padding-bottom: 10px;">Declaration</h3>
					<table width="100%">
						<p>I declare that the information provided in this form is true and correct. I am aware that I may be subject to prosecution if I am found to have made any false statement which I know to be false or intentionally suppressed any material fact.</p>
					</table>
				<table width="100%">
						<tbody>
							<tr>
								<td width="270"><span>Name of Customer/Agent</span></td>
								<td><span style="font-weight:bold">$agent_full_name</span></td>
							</tr>					
						</tbody>
					</table>
					
			
	<br>
</body></html>
EOD;

// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
// ---------------------------------------------------------
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
ob_end_clean();
$ro = PDF_FILE_PATH;
$pdfname = "incorp_".time().".pdf";
$doc = $pdf->Output($ro . $pdfname, 'F');
$fileattname = $pdfname; //name that you want to use to send or you can use the same name
/*echo $html;*/

//PDF Email settings
$pdfMail->From = FROM_EMAIL_PDF;
$pdfMail->FromName = FROM_NAME_PDF;
$pdfMail->addAddress(TO_EMAIL1_PDF, TO_NAME1_PDF);
$pdfMail->addAddress(TO_EMAIL2_PDF, TO_NAME2_PDF);
$pdfMail->addCC(CC_EMAIL1_PDF,CC_NAME1_PDF);
$pdfMail->addBCC(BCC_EMAIL1_PDF, BCC_NAME1_PDF);

//Provide file path and name of the attachments
$pdfMail->addAttachment($fileattname, $fileattname);        
$pdfMail->isHTML(false);
$pdfMail->Subject = SUBJECT_PDF;
$pdfMail->Body = "Thank you for filling out (".$proposedCompanyName1.") information!. \n\n We’ve sent you an email with the information that you just filled at the email address. Our relationship manager will get over the details and will get back to you, if any more documents are required.";

if(!$pdfMail->send()) 
{
    echo "Mailer Error: " . $pdfMail->ErrorInfo;
} 
else 
{
    echo "Message has been sent successfully";
    header("Location: thankyou.php");	
}

//============================================================+
// END OF FILE
//============================================================+

?>
