<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1, shrink-to-fit=no" name="viewport">
	<meta content="" name="description">
	<meta content="" name="author">
	<title>InCorp - On Boarding Customer Due Diligence Form</title>
	
	<link href="https://fonts.googleapis.com/css?family=Lato:400,700,900|Roboto" rel="stylesheet">	
	<link rel="stylesheet" href="css/style.css?v2">	
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/chosen.css?v1">
	<link rel="stylesheet" href="css/prism.css">	
	<link rel="stylesheet" href="css/font-awesome.min.css">
</head>
	
<body>
	<nav id="mainNav">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-10">
					<a href="#page-top"><img src="images/logo.png" alt="" /></a>			
				</div>
			</div>
		</div>
	</nav>
	
	<header class="masthead text-white bg-header">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-sm-10">
        			<h1 class="text-center">On Boarding Customer Due Diligence Form</h1>
				</div>
			</div>
		</div>
	</header>
	
	<header class="bg-intro border-bottom">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-lg-10 col-sm-12">
					<div class="col-lg-8 col-sm-12 float-left">
						<p>If you have a complex business structure we request you to download offline version of this form.</p>
					</div>
					<div class="col-lg-4 col-sm-12 float-left">
						<a class="btn btn-block cdd-download" href="#">Download offline version</a>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div class="container">
		
			
			<div class="form-group row my-5">
				<div class="col-sm-12">
					<h3 class="text-uppercase mb-3">Thank You</h3>
					<p>Thank you for filling out (<span id="propose_name1"></span>) information!</p>
					<p>We’ve sent you an email with the information that you just filled at the email address. Our relationship manager will get over the details and will get back to you, if any more documents are required.</p>	
				</div>					
			</div>
		</div>
	</div>
	
	<!-- Footer -->
	<footer class="site-footer pt-4 mt-4">
		<div class="container text-center text-md-left">
			<p class="text-center"><a href="https://www.incorp.asia/" rel="home">In.Corp Global Pte Ltd</a>. <span style="white-space: pre;">All rights reserved.</span><span style="margin: 0 8px;">·</span><a href="/privacy/">Privacy Policy</a><span style="margin: 0 8px;">·</span><a href="/terms/">Terms of Use</a><span style="margin: 0 8px;">·</span><a href="/sitemap/">Sitemap</a></p>
		</div><!-- /.container -->		
	</footer>

<!-- Bootstrap core JavaScript -->	
	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.bundle.min.js"></script>
	<script>
		var params = "";
		var proposedCompanyName1 = localStorage.getItem('proposedCompanyName1');
		$(document).ready(function () {
		    $("#propose_name1").text(proposedCompanyName1);
		});
		var proposedCompanyName2 = localStorage.getItem('proposedCompanyName2');
		var proposedCompanyName3 = localStorage.getItem('proposedCompanyName3');
		var intendedAddress = localStorage.getItem("intendedAddress");
		var postalCode = localStorage.getItem("postal-code");
		var streetName = localStorage.getItem("street-name");
		var floor = localStorage.getItem("floor");
		var unit = localStorage.getItem("unit");
		var businessActivities = localStorage.getItem('businessActivities');
		var countriesOperation = localStorage.getItem("countriesOperation");
		var sourceFunds = localStorage.getItem("sourceFunds");
		var roleSelector = localStorage.getItem("roleSelector");
		var counter = localStorage.getItem("Director_COUNT");
		var counter1 = localStorage.getItem("Shareholder_COUNT");
		var counter2 = localStorage.getItem("Director and Shareholder_COUNT");
		var counter3 = localStorage.getItem("Corporate Shareholder_COUNT");
		var bocounter = localStorage.getItem("bosharescount");
		var bodicounter = localStorage.getItem("bodisharescount");
	
		params += 'proposedCompanyName1='+proposedCompanyName1+'&proposedCompanyName2='+proposedCompanyName2+'&proposedCompanyName3='+proposedCompanyName3+'&intendedAddress='+intendedAddress+'&postalCode='+postalCode+'&streetName='+streetName+'&floor='+floor+'&unit='+unit+'&businessActivities='+businessActivities+'&countriesOperation='+countriesOperation+'&sourceFunds='+sourceFunds;
		
		params += '&counter='+counter+'&counter1='+counter1+'&counter2='+counter2+'&counter3='+counter3+'&bocounter='+bocounter+'&bodicounter='+bodicounter;

		for(c=1;c<=counter;c++){
			var directorSurname1 = localStorage.getItem("directorSurname_"+c);		
			var directorresstatus1 = localStorage.getItem("directorresstatus_"+c);
			var passport = localStorage.getItem("directornonresidentuploadpassport_"+c);
			var addressProof = localStorage.getItem("directornonresidentaddress_"+c);
			var address = localStorage.getItem("directornonresidentresaddress_"+c);
			var finPassport = localStorage.getItem("directorFINuploadPassport_"+c);
			var finCardFront = localStorage.getItem("directorfincardfront_"+c);
			var finCardBack = localStorage.getItem("directorfincardback_"+c);
			var finAddressProof = localStorage.getItem("directorFINaddress_"+c);
			var finAddress = localStorage.getItem("directorFINresaddress_"+c);
			var nricfront = localStorage.getItem("directorSGnricfront_"+c);
			var nricback = localStorage.getItem("directorSGnricback_"+c);
			var handphone = localStorage.getItem("directorhandphone_"+c);
			var officephone = localStorage.getItem("directorOfficePhone_"+c);
			var businessemail = localStorage.getItem("directorbusinessemail_"+c);
			var personalemail = localStorage.getItem("directorpersonalemail_"+c);
			var directorpep = localStorage.getItem("directorpep_"+c);
			var pepCountry = localStorage.getItem("directorCountrypep_"+c);
			var pepRole = localStorage.getItem("directorRolePep_"+c);
			var pepFrom = localStorage.getItem("directorPepFrom_"+c);
			var pepTo = localStorage.getItem("directorPepTo_"+c);

			//director non resident preview popup
			var directornonresidentpopupsurname = localStorage.getItem("directornonresidentpopupsurname_"+c);
			var directornonresidentpopupgivenName = localStorage.getItem("directornonresidentpopupgivenName_"+c);
			var directornonresidentpopuppassportno = localStorage.getItem("directornonresidentpopuppassportno_"+c);
			var directornonresidentpopupnationality = localStorage.getItem("directornonresidentpopupnationality_"+c);
			var directornonresidentpopupcountry = localStorage.getItem("directornonresidentpopupcountry_"+c);
			var directornonresidentpopupgender = localStorage.getItem("directornonresidentpopupgender_"+c);
			var directornonresidentpopupdateexpiry = localStorage.getItem("directornonresidentpopupdateexpiry_"+c);
			var directornonresidentpopupdob = localStorage.getItem("directornonresidentpopupdob_"+c);

			//director non resident manual popup
			var directornonmanuallypassportno = localStorage.getItem("directornonmanuallypassportno_"+c);
			var directornonmanuallynationality = localStorage.getItem("directornonmanuallynationality_"+c);
			var directornonmanuallycountry = localStorage.getItem("directornonmanuallycountry_"+c);
			var directornonmanuallygender = localStorage.getItem("directornonmanuallygender_"+c);
			var directornonmanuallydatetimepicker = localStorage.getItem("directornonmanuallydatetimepicker_"+c);
			var directornonmanuallydateexpiry = localStorage.getItem("directornonmanuallydateexpiry_"+c);
			var directornonmanuallyplaceofBirth = localStorage.getItem("directornonmanuallyplaceofBirth_"+c);
			var directornonmanuallydateBirth = localStorage.getItem("directornonmanuallydateBirth_"+c);

			//director nric front preview popup
			var directorpreviewpopupfrontnricnumber = localStorage.getItem("directorpreviewpopupfrontnricnumber_"+c);
			var directorpreviewpopupnricfrontgender = localStorage.getItem("directorpreviewpopupnricfrontgender_"+c);
			var directorpreviewpopupnricfrontdob = localStorage.getItem("directorpreviewpopupnricfrontdob_"+c);
			var directorpreviewpopupnricfrontnationality = localStorage.getItem("directorpreviewpopupnricfrontnationality_"+c);
			
			//director nric back preview popup
			var directorpreviewpopupnricbackpostcode = localStorage.getItem("directorpreviewpopupnricbackpostcode_"+c);
			var directorpreviewpopupnricbackstreetname = localStorage.getItem("directorpreviewpopupnricbackstreetname_"+c);
			var directorpreviewpopupnricbackfloor = localStorage.getItem("directorpreviewpopupnricbackfloor_"+c);
			var directorpreviewnrpopupicbackunit = localStorage.getItem("directorpreviewnrpopupicbackunit_"+c);

			//director nric manual popup
			var directorpreviewmanuallynricnumber = localStorage.getItem("directorpreviewmanuallynricnumber_"+c);
			var directorpreviewmanuallynricgender = localStorage.getItem("directorpreviewmanuallynricgender_"+c);
			var directorpreviewmanuallynricdob = localStorage.getItem("directorpreviewmanuallynricdob_"+c);
			var directorpreviewmanuallynricnationality = localStorage.getItem("directorpreviewmanuallynricnationality_"+c);
			var directorpreviewmanuallynricpostcode = localStorage.getItem("directorpreviewmanuallynricpostcode_"+c);
			var directorpreviewmanuallynricstreetname = localStorage.getItem("directorpreviewmanuallynricstreetname_"+c);
			var directorpreviewmanuallynricfloor = localStorage.getItem("directorpreviewmanuallynricfloor_"+c);
			var directorpreviewnrmanuallyicunit = localStorage.getItem("directorpreviewnrmanuallyicunit_"+c);

			//director fin passport preview popup
			var directorfincardpopupsurname = localStorage.getItem("directorfincardpopupsurname_"+c);
			var directorfincardpopupgivenName = localStorage.getItem("directorfincardpopupgivenName_"+c);
			var directorfincardpopuppassportno = localStorage.getItem("directorfincardpopuppassportno_"+c);
			var directorfincardpopupnationality = localStorage.getItem("directorfincardpopupnationality_"+c);
			var directorfincardpopupcountry = localStorage.getItem("directorfincardpopupcountry_"+c);
			var directorfincardpopupgender = localStorage.getItem("directorfincardpopupgender_"+c);
			var directorfincardpopupdateexpiry = localStorage.getItem("directorfincardpopupdateexpiry_"+c);
			var directorfincardpopupdob = localStorage.getItem("directorfincardpopupdob_"+c);

			//director fincard front preview popup
			var directorfincardpopupfrontemployer = localStorage.getItem("directorfincardpopupfrontemployer_"+c);
			var directorfincardpopupfrontoccupation = localStorage.getItem("directorfincardpopupfrontoccupation_"+c);
			var directorfincardpopupfrontdateissue = localStorage.getItem("directorfincardpopupfrontdateissue_"+c);
			var directorfincardpopupfrontdateexpiry = localStorage.getItem("directorfincardpopupfrontdateexpiry_"+c);
			var directorwfincardpopupfrontNumber = localStorage.getItem("directorwfincardpopupfrontNumber_"+c);

			//director fincard back preview popup
			var directorfincardpopupbackdateissue = localStorage.getItem("directorfincardpopupbackdateissue_"+c);
			var directorfincardpopupbackdateexpiry = localStorage.getItem("directorfincardpopupbackdateexpiry_"+c);
			var directorwfincardpopupbackNumber = localStorage.getItem("directorwfincardpopupbackNumber_"+c);

			//director fincard  manual popup
			var directorfincardmanuallypassportno = localStorage.getItem("directorfincardmanuallypassportno_"+c);
			var directorfincardmanuallynationality = localStorage.getItem("directorfincardmanuallynationality_"+c);
			var directorfincardmanuallycountry = localStorage.getItem("directorfincardmanuallycountry_"+c);
			var directorfincardmanuallygender = localStorage.getItem("directorfincardmanuallygender_"+c);
			var directorfincardmanuallydatetimepicker = localStorage.getItem("directorfincardmanuallydatetimepicker_"+c);
			var directorfincardmanuallydateexpiry = localStorage.getItem("directorfincardmanuallydateexpiry_"+c);
			var directorfincardmanuallyplaceofBirth = localStorage.getItem("directorfincardmanuallyplaceofBirth_"+c);
			var directorfincardmanuallydateBirth = localStorage.getItem("directorfincardmanuallydateBirth_"+c);
		
			var directorfincardmanuallyemployer = localStorage.getItem("directorfincardmanuallyemployer_"+c);
			var directorfincardmanuallyoccupation = localStorage.getItem("directorfincardmanuallyoccupation_"+c);
			var directorfincardmanuallyworkpassdateissue = localStorage.getItem("directorfincardmanuallyworkpassdateissue_"+c);
			var directorfincardmanuallyworkpassdateexpiry = localStorage.getItem("directorfincardmanuallyworkpassdateexpiry_"+c);
			var directorfincardmanuallywfincardnumber = localStorage.getItem("directorfincardmanuallywfincardnumber_"+c);

			var director_paid_up_capital = localStorage.getItem("director_paid_up_capital_"+c);
			var director_number_of_shares = localStorage.getItem("director_number_of_shares_"+c);


			params += '&directorSurname'+c+'='+directorSurname1+'&directorresstatus'+c+'='+directorresstatus1+'&address'+c+'='+address+'&finAddress'+c+'='+finAddress+'&directornonresidentpopupsurname'+c+'='+directornonresidentpopupsurname+'&directornonresidentpopupgivenName'+c+'='+directornonresidentpopupgivenName+'&directornonresidentpopuppassportno'+c+'='+directornonresidentpopuppassportno+'&directornonresidentpopupnationality'+c+'='+directornonresidentpopupnationality+'&directornonresidentpopupcountry'+c+'='+directornonresidentpopupcountry+'&directornonresidentpopupgender'+c+'='+directornonresidentpopupgender+'&directornonresidentpopupdob'+c+'='+directornonresidentpopupdob+'&directornonresidentpopupdateexpiry'+c+'='+directornonresidentpopupdateexpiry+'&directornonmanuallypassportno'+c+'='+directornonmanuallypassportno+'&directornonmanuallynationality'+c+'='+directornonmanuallynationality+'&directornonmanuallycountry'+c+'='+directornonmanuallycountry+'&directornonmanuallygender'+c+'='+directornonmanuallygender+'&directornonmanuallydatetimepicker'+c+'='+directornonmanuallydatetimepicker+'&directornonmanuallydateexpiry'+c+'='+directornonmanuallydateexpiry+'&directornonmanuallyplaceofBirth'+c+'='+directornonmanuallyplaceofBirth+'&directornonmanuallydateBirth'+c+'='+directornonmanuallydateBirth+'&handphone'+c+'='+handphone+'&officephone'+c+'='+officephone+'&businessemail'+c+'='+businessemail+'&personalemail'+c+'='+personalemail+'&directorpep'+c+'='+directorpep+'&pepCountry'+c+'='+pepCountry+'&pepRole'+c+'='+pepRole+'&pepFrom'+c+'='+pepFrom+'&pepTo'+c+'='+pepTo+'&directorpreviewpopupfrontnricnumber'+c+'='+directorpreviewpopupfrontnricnumber+'&directorpreviewpopupnricfrontgender'+c+'='+directorpreviewpopupnricfrontgender+'&directorpreviewpopupnricfrontdob'+c+'='+directorpreviewpopupnricfrontdob+'&directorpreviewpopupnricfrontnationality'+c+'='+directorpreviewpopupnricfrontnationality+'&directorpreviewpopupnricbackpostcode'+c+'='+directorpreviewpopupnricbackpostcode+'&directorpreviewpopupnricbackstreetname'+c+'='+directorpreviewpopupnricbackstreetname+'&directorpreviewpopupnricbackfloor'+c+'='+directorpreviewpopupnricbackfloor+'&directorpreviewnrpopupicbackunit'+c+'='+directorpreviewnrpopupicbackunit+'&directorpreviewmanuallynricnumber'+c+'='+directorpreviewmanuallynricnumber+'&directorpreviewmanuallynricgender'+c+'='+directorpreviewmanuallynricgender+'&directorpreviewmanuallynricdob'+c+'='+directorpreviewmanuallynricdob+'&directorpreviewmanuallynricnationality'+c+'='+directorpreviewmanuallynricnationality+'&directorpreviewmanuallynricpostcode'+c+'='+directorpreviewmanuallynricpostcode+'&directorpreviewmanuallynricstreetname'+c+'='+directorpreviewmanuallynricstreetname+'&directorpreviewmanuallynricfloor'+c+'='+directorpreviewmanuallynricfloor+'&directorpreviewnrmanuallyicunit'+c+'='+directorpreviewnrmanuallyicunit+'&directorfincardpopupsurname'+c+'='+directorfincardpopupsurname+'&directorfincardpopupgivenName'+c+'='+directorfincardpopupgivenName+'&directorfincardpopuppassportno'+c+'='+directorfincardpopuppassportno+'&directorfincardpopupnationality'+c+'='+directorfincardpopupnationality+'&directorfincardpopupcountry'+c+'='+directorfincardpopupcountry+'&directorfincardpopupgender'+c+'='+directorfincardpopupgender+'&directorfincardpopupdateexpiry'+c+'='+directorfincardpopupdateexpiry+'&directorfincardpopupdob'+c+'='+directorfincardpopupdob+'&directorfincardpopupfrontemployer'+c+'='+directorfincardpopupfrontemployer+'&directorfincardpopupfrontoccupation'+c+'='+directorfincardpopupfrontoccupation+'&directorfincardpopupfrontdateissue'+c+'='+directorfincardpopupfrontdateissue+'&directorfincardpopupfrontdateexpiry'+c+'='+directorfincardpopupfrontdateexpiry+'&directorwfincardpopupfrontNumber'+c+'='+directorwfincardpopupfrontNumber+'&directorfincardpopupbackdateissue'+c+'='+directorfincardpopupbackdateissue+'&directorfincardpopupbackdateexpiry'+c+'='+directorfincardpopupbackdateexpiry+'&directorwfincardpopupbackNumber'+c+'='+directorwfincardpopupbackNumber+'&directorfincardmanuallypassportno'+c+'='+directorfincardmanuallypassportno+'&directorfincardmanuallynationality'+c+'='+directorfincardmanuallynationality+'&directorfincardmanuallycountry'+c+'='+directorfincardmanuallycountry+'&directorfincardmanuallygender'+c+'='+directorfincardmanuallygender+'&directorfincardmanuallydatetimepicker'+c+'='+directorfincardmanuallydatetimepicker+'&directorfincardmanuallydateexpiry'+c+'='+directorfincardmanuallydateexpiry+'&directorfincardmanuallyplaceofBirth'+c+'='+directorfincardmanuallyplaceofBirth+'&directorfincardmanuallydateBirth'+c+'='+directorfincardmanuallydateBirth+'&directorfincardmanuallyemployer'+c+'='+directorfincardmanuallyemployer+'&directorfincardmanuallyoccupation'+c+'='+directorfincardmanuallyoccupation+'&directorfincardmanuallyworkpassdateissue'+c+'='+directorfincardmanuallyworkpassdateissue+'&directorfincardmanuallyworkpassdateexpiry'+c+'='+directorfincardmanuallyworkpassdateexpiry+'&directorfincardmanuallywfincardnumber'+c+'='+directorfincardmanuallywfincardnumber+'&director_paid_up_capital'+c+'='+director_paid_up_capital+'&director_number_of_shares'+c+'='+director_number_of_shares+'&directornonresidentuploadpassport'+c+'='+passport+'&directornonresidentaddress'+c+'='+addressProof+'&directorFINuploadPassport'+c+'='+finPassport+'&directorfincardfront'+c+'='+finCardFront+'&directorfincardback'+c+'='+finCardBack+'&directorFINaddress'+c+'='+finAddressProof+'&directorSGnricfront'+c+'='+nricfront+'&directorSGnricback'+c+'='+nricback;
		}

		for(c=1;c<=counter1;c++){

			var shareholdersurname1 = localStorage.getItem("shareholdersurname_"+c);
			var shareholderbowner1 = localStorage.getItem("shareholderbowner_"+c);
			var shareholderresstatus1 = localStorage.getItem("shareholderresstatus_"+c);
			var shareholdernonresidentuploadpassport = localStorage.getItem("shareholdernonresidentuploadpassport_"+c);
			var shareholdernonresidentaddress = localStorage.getItem("shareholdernonresidentaddress_"+c);
			var shareholdernonresidentresaddress = localStorage.getItem("shareholdernonresidentresaddress_"+c);
			var shareholderFINuploadPassport = localStorage.getItem("shareholderFINuploadPassport_"+c);
			var shareholderfincardfront = localStorage.getItem("shareholderfincardfront_"+c);
			var shareholderfincardback = localStorage.getItem("shareholderfincardback_"+c);
			var shareholderFINaddress = localStorage.getItem("shareholderFINaddress_"+c);
			var shareholderFINresaddress = localStorage.getItem("shareholderFINresaddress_"+c);
			var shareholderSGnricfront = localStorage.getItem("shareholderSGnricfront_"+c);
			var shareholderSGnricback = localStorage.getItem("shareholderSGnricback_"+c);
			var shareholderhandphone = localStorage.getItem("shareholderhandphone_"+c); 
			var shareholderOfficephone = localStorage.getItem("shareholderOfficephone_"+c); 
			var shareholderbusinessemail = localStorage.getItem("shareholderbusinessemail_"+c);
			var shareholderPersonalemail = localStorage.getItem("shareholderPersonalemail_"+c);
			var shareholderpep = localStorage.getItem("shareholderpep_"+c);
			var shareholdercountrypep = localStorage.getItem("shareholdercountrypep_"+c);
			var shareholderrolePep = localStorage.getItem("shareholderrolePep_"+c);
			var shareholderpepFrom = localStorage.getItem("shareholderpepFrom_"+c);
			var shareholderpepTo = localStorage.getItem("shareholderpepTo_"+c);

			//============= Individual Shareholders section ================//
			//shareholder non resident preview popup
			var shareholdernonresidentpopupsurname = localStorage.getItem("shareholdernonresidentpopupsurname_"+c);
			var shareholdernonresidentpopupgivenName = localStorage.getItem("shareholdernonresidentpopupgivenName_"+c);
			var shareholdernonresidentpopuppassportno = localStorage.getItem("shareholdernonresidentpopuppassportno_"+c);
			var shareholdernonresidentpopupnationality = localStorage.getItem("shareholdernonresidentpopupnationality_"+c);
			var shareholdernonresidentpopupcountry = localStorage.getItem("shareholdernonresidentpopupcountry_"+c);
			var shareholdernonresidentpopupgender = localStorage.getItem("shareholdernonresidentpopupgender_"+c);
			var shareholdernonresidentpopupdateexpiry = localStorage.getItem("shareholdernonresidentpopupdateexpiry_"+c);
			var shareholdernonresidentpopupdob = localStorage.getItem("shareholdernonresidentpopupdob_"+c);

			//shareholder non resident manual popup
			var shareholdernonmanuallypassportno = localStorage.getItem("shareholdernonmanuallypassportno_"+c);
			var shareholdernonmanuallynationality = localStorage.getItem("shareholdernonmanuallynationality_"+c);
			var shareholdernonmanuallycountry = localStorage.getItem("shareholdernonmanuallycountry_"+c);
			var shareholdernonmanuallygender = localStorage.getItem("shareholdernonmanuallygender_"+c);
			var shareholdernonmanuallydatetimepicker = localStorage.getItem("shareholdernonmanuallydatetimepicker_"+c);
			var shareholdernonmanuallydateexpiry = localStorage.getItem("shareholdernonmanuallydateexpiry_"+c);
			var shareholdernonmanuallyplaceofBirth = localStorage.getItem("shareholdernonmanuallyplaceofBirth_"+c);
			var shareholdernonmanuallydateBirth = localStorage.getItem("shareholdernonmanuallydateBirth_"+c);

			//shareholder nric front preview popup
			var shareholderpreviewpopupfrontnricnumber = localStorage.getItem("shareholderpreviewpopupfrontnricnumber_"+c);
			var shareholderpreviewpopupnricfrontgender = localStorage.getItem("shareholderpreviewpopupnricfrontgender_"+c);
			var shareholderpreviewpopupnricfrontdob = localStorage.getItem("shareholderpreviewpopupnricfrontdob_"+c);
			var shareholderpreviewpopupnricfrontnationality = localStorage.getItem("shareholderpreviewpopupnricfrontnationality_"+c);
			
			//shareholder nric back preview popup
			var shareholderpreviewpopupnricbackpostcode = localStorage.getItem("shareholderpreviewpopupnricbackpostcode_"+c);
			var shareholderpreviewpopupnricbackstreetname = localStorage.getItem("shareholderpreviewpopupnricbackstreetname_"+c);
			var shareholderpreviewpopupnricbackfloor = localStorage.getItem("shareholderpreviewpopupnricbackfloor_"+c);
			var shareholderpreviewnrpopupicbackunit = localStorage.getItem("shareholderpreviewnrpopupicbackunit_"+c);

			//shareholder nric manual popup
			var shareholderpreviewmanuallynricnumber = localStorage.getItem("shareholderpreviewmanuallynricnumber_"+c);
			var shareholderpreviewmanuallynricgender = localStorage.getItem("shareholderpreviewmanuallynricgender_"+c);
			var shareholderpreviewmanuallynricdob = localStorage.getItem("shareholderpreviewmanuallynricdob_"+c);
			var shareholderpreviewmanuallynricnationality = localStorage.getItem("shareholderpreviewmanuallynricnationality_"+c);
			var shareholderpreviewmanuallynricpostcode = localStorage.getItem("shareholderpreviewmanuallynricpostcode_"+c);
			var shareholderpreviewmanuallynricstreetname = localStorage.getItem("shareholderpreviewmanuallynricstreetname_"+c);
			var shareholderpreviewmanuallynricfloor = localStorage.getItem("shareholderpreviewmanuallynricfloor_"+c);
			var shareholderpreviewnrmanuallyicunit = localStorage.getItem("shareholderpreviewnrmanuallyicunit_"+c);

			//shareholder fin passport preview popup
			var shareholderfincardpopupsurname = localStorage.getItem("shareholderfincardpopupsurname_"+c);
			var shareholderfincardpopupgivenName = localStorage.getItem("shareholderfincardpopupgivenName_"+c);
			var shareholderfincardpopuppassportno = localStorage.getItem("shareholderfincardpopuppassportno_"+c);
			var shareholderfincardpopupnationality = localStorage.getItem("shareholderfincardpopupnationality_"+c);
			var shareholderfincardpopupcountry = localStorage.getItem("shareholderfincardpopupcountry_"+c);
			var shareholderfincardpopupgender = localStorage.getItem("shareholderfincardpopupgender_"+c);
			var shareholderfincardpopupdateexpiry = localStorage.getItem("shareholderfincardpopupdateexpiry_"+c);
			var shareholderfincardpopupdob = localStorage.getItem("shareholderfincardpopupdob_"+c);

			//shareholder fincard front preview 
			var shareholderfincardfrontemployer = localStorage.getItem("shareholderfincardfrontemployer_"+c);
			var shareholderfincardfrontoccupation = localStorage.getItem("shareholderfincardfrontoccupation_"+c);
			var shareholderfincardfrontdateissue = localStorage.getItem("shareholderfincardfrontdateissue_"+c);
			var shareholderfincardfrontdateexpiry = localStorage.getItem("shareholderfincardfrontdateexpiry_"+c);
			var shareholderwfincardfrontNumber = localStorage.getItem("shareholderwfincardfrontNumber_"+c);

			//shareholder fincard back preview 
			var shareholderfincardbackdateissue = localStorage.getItem("shareholderfincardbackdateissue_"+c);
			var shareholderfincardbackdateexpiry = localStorage.getItem("shareholderfincardbackdateexpiry_"+c);
			var shareholderwfincardbackNumber = localStorage.getItem("shareholderwfincardbackNumber_"+c);

			//shareholder fincard  manual popup
			var shareholderfincardmanuallypassportno = localStorage.getItem("shareholderfincardmanuallypassportno_"+c);
			var shareholderfincardmanuallynationality = localStorage.getItem("shareholderfincardmanuallynationality_"+c);
			var shareholderfincardmanuallycountry = localStorage.getItem("shareholderfincardmanuallycountry_"+c);
			var shareholderfincardmanuallygender = localStorage.getItem("shareholderfincardmanuallygender_"+c);
			var shareholderfincardmanuallydatetimepicker = localStorage.getItem("shareholderfincardmanuallydatetimepicker_"+c);
			var shareholderfincardmanuallydateexpiry = localStorage.getItem("shareholderfincardmanuallydateexpiry_"+c);
			var shareholderfincardmanuallyplaceofBirth = localStorage.getItem("shareholderfincardmanuallyplaceofBirth_"+c);
			var shareholderfincardmanuallydateBirth = localStorage.getItem("shareholderfincardmanuallydateBirth_"+c);
		
			var shareholderfincardmanuallyemployer = localStorage.getItem("shareholderfincardmanuallyemployer_"+c);
			var shareholderfincardmanuallyoccupation = localStorage.getItem("shareholderfincardmanuallyoccupation_"+c);
			var shareholderfincardmanuallyworkpassdateissue = localStorage.getItem("shareholderfincardmanuallyworkpassdateissue_"+c);
			var shareholderfincardmanuallyworkpassdateexpiry = localStorage.getItem("shareholderfincardmanuallyworkpassdateexpiry_"+c);
			var shareholderfincardmanuallywfincardnumber = localStorage.getItem("shareholderfincardmanuallywfincardnumber_"+c);

			var individual_shareholder_paid_up_capital = localStorage.getItem("individual_shareholder_paid_up_capital_"+c);
			var individual_shareholder_number_of_shares = localStorage.getItem("individual_shareholder_number_of_shares_"+c);

			params += '&shareholdernonresidentresaddress'+c+'='+shareholdernonresidentresaddress+'&shareholderFINresaddress'+c+'='+shareholderFINresaddress+'&shareholdersurname'+c+'='+shareholdersurname1+'&shareholderresstatus'+c+'='+shareholderresstatus1+'&shareholderbowner'+c+'='+shareholderbowner1+'&shareholdernonresidentpopupsurname'+c+'='+shareholdernonresidentpopupsurname+'&shareholdernonresidentpopupgivenName'+c+'='+shareholdernonresidentpopupgivenName+'&shareholdernonresidentpopuppassportno'+c+'='+shareholdernonresidentpopuppassportno+'&shareholdernonresidentpopupnationality'+c+'='+shareholdernonresidentpopupnationality+'&shareholdernonresidentpopupcountry'+c+'='+shareholdernonresidentpopupcountry+'&shareholdernonresidentpopupgender'+c+'='+shareholdernonresidentpopupgender+'&shareholdernonresidentpopupdateexpiry'+c+'='+shareholdernonresidentpopupdateexpiry+'&shareholdernonresidentpopupdob'+c+'='+shareholdernonresidentpopupdob+'&shareholdernonmanuallypassportno'+c+'='+shareholdernonmanuallypassportno+'&shareholdernonmanuallynationality'+c+'='+shareholdernonmanuallynationality+'&shareholdernonmanuallycountry'+c+'='+shareholdernonmanuallycountry+'&shareholdernonmanuallygender'+c+'='+shareholdernonmanuallygender+'&shareholdernonmanuallydatetimepicker'+c+'='+shareholdernonmanuallydatetimepicker+'&shareholdernonmanuallydateexpiry'+c+'='+shareholdernonmanuallydateexpiry+'&shareholdernonmanuallyplaceofBirth'+c+'='+shareholdernonmanuallyplaceofBirth+'&shareholdernonmanuallydateBirth'+c+'='+shareholdernonmanuallydateBirth+'&shareholderhandphone'+c+'='+shareholderhandphone+'&shareholderOfficephone'+c+'='+shareholderOfficephone+'&shareholderbusinessemail'+c+'='+shareholderbusinessemail+'&shareholderPersonalemail'+c+'='+shareholderPersonalemail+'&shareholderpep'+c+'='+shareholderpep+'&shareholdercountrypep'+c+'='+shareholdercountrypep+'&shareholderrolePep'+c+'='+shareholderrolePep+'&shareholderpepFrom'+c+'='+shareholderpepFrom+'&shareholderpepTo'+c+'='+shareholderpepTo+'&shareholderpreviewpopupfrontnricnumber'+c+'='+shareholderpreviewpopupfrontnricnumber+'&shareholderpreviewpopupnricfrontgender'+c+'='+shareholderpreviewpopupnricfrontgender+'&shareholderpreviewpopupnricfrontdob'+c+'='+shareholderpreviewpopupnricfrontdob+'&shareholderpreviewpopupnricfrontnationality'+c+'='+shareholderpreviewpopupnricfrontnationality+'&shareholderpreviewpopupnricbackpostcode'+c+'='+shareholderpreviewpopupnricbackpostcode+'&shareholderpreviewpopupnricbackstreetname'+c+'='+shareholderpreviewpopupnricbackstreetname+'&shareholderpreviewpopupnricbackfloor'+c+'='+shareholderpreviewpopupnricbackfloor+'&shareholderpreviewnrpopupicbackunit'+c+'='+shareholderpreviewnrpopupicbackunit+'&shareholderpreviewmanuallynricnumber'+c+'='+shareholderpreviewmanuallynricnumber+'&shareholderpreviewmanuallynricgender'+c+'='+shareholderpreviewmanuallynricgender+'&shareholderpreviewmanuallynricdob'+c+'='+shareholderpreviewmanuallynricdob+'&shareholderpreviewmanuallynricnationality'+c+'='+shareholderpreviewmanuallynricnationality+'&shareholderpreviewmanuallynricpostcode'+c+'='+shareholderpreviewmanuallynricpostcode+'&shareholderpreviewmanuallynricstreetname'+c+'='+shareholderpreviewmanuallynricstreetname+'&shareholderpreviewmanuallynricfloor'+c+'='+shareholderpreviewmanuallynricfloor+'&shareholderpreviewnrmanuallyicunit'+c+'='+shareholderpreviewnrmanuallyicunit+'&shareholderSGnricfront'+c+'='+shareholderSGnricfront+'&shareholderSGnricback'+c+'='+shareholderSGnricfront+'&shareholderfincardpopupsurname'+c+'='+shareholderfincardpopupsurname+'&shareholderfincardpopupgivenName'+c+'='+shareholderfincardpopupgivenName+'&shareholderfincardpopuppassportno'+c+'='+shareholderfincardpopuppassportno+'&shareholderfincardpopupnationality'+c+'='+shareholderfincardpopupnationality+'&shareholderfincardpopupcountry'+c+'='+shareholderfincardpopupcountry+'&shareholderfincardpopupgender'+c+'='+shareholderfincardpopupgender+'&shareholderfincardpopupdateexpiry'+c+'='+shareholderfincardpopupdateexpiry+'&shareholderfincardpopupdob'+c+'='+shareholderfincardpopupdob+'&shareholderfincardfrontemployer'+c+'='+shareholderfincardfrontemployer+'&shareholderfincardfrontoccupation'+c+'='+shareholderfincardfrontoccupation+'&shareholderfincardfrontdateissue'+c+'='+shareholderfincardfrontdateissue+'&shareholderfincardfrontdateexpiry'+c+'='+shareholderfincardfrontdateexpiry+'&shareholderwfincardfrontNumber'+c+'='+shareholderwfincardfrontNumber+'&shareholderfincardbackdateissue'+c+'='+shareholderfincardbackdateissue+'&shareholderfincardbackdateexpiry'+c+'='+shareholderfincardbackdateexpiry+'&shareholderwfincardbackNumber'+c+'='+shareholderwfincardbackNumber+'&shareholderfincardmanuallypassportno'+c+'='+shareholderfincardmanuallypassportno+'&shareholderfincardmanuallynationality'+c+'='+shareholderfincardmanuallynationality+'&shareholderfincardmanuallycountry'+c+'='+shareholderfincardmanuallycountry+'&shareholderfincardmanuallygender'+c+'='+shareholderfincardmanuallygender+'&shareholderfincardmanuallydatetimepicker'+c+'='+shareholderfincardmanuallydatetimepicker+'&shareholderfincardmanuallydateexpiry'+c+'='+shareholderfincardmanuallydateexpiry+'&shareholderfincardmanuallyplaceofBirth'+c+'='+shareholderfincardmanuallyplaceofBirth+'&shareholderfincardmanuallydateBirth'+c+'='+shareholderfincardmanuallydateBirth+'&shareholderfincardmanuallyemployer'+c+'='+shareholderfincardmanuallyemployer+'&shareholderfincardmanuallyoccupation'+c+'='+shareholderfincardmanuallyoccupation+'&shareholderfincardmanuallyworkpassdateissue'+c+'='+shareholderfincardmanuallyworkpassdateissue+'&shareholderfincardmanuallyworkpassdateexpiry'+c+'='+shareholderfincardmanuallyworkpassdateexpiry+'&shareholderfincardmanuallywfincardnumber'+c+'='+shareholderfincardmanuallywfincardnumber+'&individual_shareholder_paid_up_capital'+c+'='+individual_shareholder_paid_up_capital+'&individual_shareholder_number_of_shares'+c+'='+individual_shareholder_number_of_shares+'&shareholdernonresidentuploadpassport'+c+'='+shareholdernonresidentuploadpassport+'&shareholdernonresidentaddress'+c+'='+shareholdernonresidentaddress+'&shareholderFINuploadPassport'+c+'='+shareholderFINuploadPassport+'&shareholderfincardfront'+c+'='+shareholderfincardfront+'&shareholderfincardback'+c+'='+shareholderfincardback+'&shareholderFINaddress'+c+'='+shareholderFINaddress+'&shareholderSGnricfront'+c+'='+shareholderSGnricfront+'&shareholderSGnricback'+c+'='+shareholderSGnricback;
		}

		for(c=1;c<=counter2;c++){

			var dishareholdersurname1 = localStorage.getItem("dishareholdersurname_"+c);
			var dishareholderbowner1 = localStorage.getItem("dishareholderbowner_"+c);
			var dishareholderresstatus1 = localStorage.getItem("dishareholderresstatus_"+c);
			var dishareholdernonresidentuploadpassport = localStorage.getItem("dishareholdernonresidentuploadpassport_"+c);
			var dishareholdernonresidentaddress = localStorage.getItem("dishareholdernonresidentaddress_"+c);
			var dishareholdernonresidentresaddress = localStorage.getItem("dishareholdernonresidentresaddress_"+c);
			var dishareholderFINuploadPassport = localStorage.getItem("dishareholderFINuploadPassport_"+c);
			var dishareholderfincardfront = localStorage.getItem("dishareholderfincardfront_"+c);
			var dishareholderfincardback = localStorage.getItem("dishareholderfincardback_"+c);
			var dishareholderFINaddress = localStorage.getItem("dishareholderFINaddress_"+c);
			var dishareholderFINresaddress = localStorage.getItem("dishareholderFINresaddress_"+c);
			var dishareholderSGnricfront = localStorage.getItem("dishareholderSGnricfront_"+c);
			var dishareholderSGnricback = localStorage.getItem("dishareholderSGnricback_"+c);
			var dishareholderhandphone = localStorage.getItem("dishareholderhandphone_"+c);
			var dishareholderOfficephone = localStorage.getItem("dishareholderOfficephone_"+c);
			var dishareholderbusinessemail = localStorage.getItem("dishareholderbusinessemail_"+c);
			var dishareholderPersonalemail = localStorage.getItem("dishareholderPersonalemail_"+c);
			var dishareholderpep = localStorage.getItem("dishareholderpoliticallyExposed"+c);//underscore missing 1st aug 2018
			var dishareholdercountrypep = localStorage.getItem("dishareholdercountrypep_"+c);
			var dishareholderrolePep = localStorage.getItem("dishareholderrolePep_"+c);
			var dishareholderpepFrom = localStorage.getItem("dishareholderpepFrom_"+c);
			var dishareholderpepTo = localStorage.getItem("dishareholderpepTo_"+c);

			//============= Director and Shareholders section ================//
			//di non resident preview popup
			var dinonresidentpopupsurname = localStorage.getItem("dinonresidentpopupsurname_"+c);
			var dinonresidentpopupgivenName = localStorage.getItem("dinonresidentpopupgivenName_"+c);
			var dinonresidentpopuppassportno = localStorage.getItem("dinonresidentpopuppassportno_"+c);
			var dinonresidentpopupnationality = localStorage.getItem("dinonresidentpopupnationality_"+c);
			var dinonresidentpopupcountry = localStorage.getItem("dinonresidentpopupcountry_"+c);
			var dinonresidentpopupgender = localStorage.getItem("dinonresidentpopupgender_"+c);
			var dinonresidentpopupdateexpiry = localStorage.getItem("dinonresidentpopupdateexpiry_"+c);
			var dinonresidentpopupdob = localStorage.getItem("dinonresidentpopupdob_"+c);

			//di non resident manual popup
			var dinonmanuallypassportno = localStorage.getItem("dinonmanuallypassportno_"+c);
			var dinonmanuallynationality = localStorage.getItem("dinonmanuallynationality_"+c);
			var dinonmanuallycountry = localStorage.getItem("dinonmanuallycountry_"+c);
			var dinonmanuallygender = localStorage.getItem("dinonmanuallygender_"+c);
			var dinonmanuallydatetimepicker = localStorage.getItem("dinonmanuallydatetimepicker_"+c);
			var dinonmanuallydateexpiry = localStorage.getItem("dinonmanuallydateexpiry_"+c);
			var dinonmanuallyplaceofBirth = localStorage.getItem("dinonmanuallyplaceofBirth_"+c);
			var dinonmanuallydateBirth = localStorage.getItem("dinonmanuallydateBirth_"+c);

			//di nric front preview popup
			var dipreviewpopupfrontnricnumber = localStorage.getItem("dipreviewpopupfrontnricnumber_"+c);
			var dipreviewpopupnricfrontgender = localStorage.getItem("dipreviewpopupnricfrontgender_"+c);
			var dipreviewpopupnricfrontdob = localStorage.getItem("dipreviewpopupnricfrontdob_"+c);
			var dipreviewpopupnricfrontnationality = localStorage.getItem("dipreviewpopupnricfrontnationality_"+c);
			
			//di nric back preview popup
			var dipreviewpopupnricbackpostcode = localStorage.getItem("dipreviewpopupnricbackpostcode_"+c);
			var dipreviewpopupnricbackstreetname = localStorage.getItem("dipreviewpopupnricbackstreetname_"+c);
			var dipreviewpopupnricbackfloor = localStorage.getItem("dipreviewpopupnricbackfloor_"+c);
			var dipreviewnrpopupicbackunit = localStorage.getItem("dipreviewnrpopupicbackunit_"+c);

			//di nric manual popup
			var dipreviewmanuallynricnumber = localStorage.getItem("dipreviewmanuallynricnumber_"+c);
			var dipreviewmanuallynricgender = localStorage.getItem("dipreviewmanuallynricgender_"+c);
			var dipreviewmanuallynricdob = localStorage.getItem("dipreviewmanuallynricdob_"+c);
			var dipreviewmanuallynricnationality = localStorage.getItem("dipreviewmanuallynricnationality_"+c);
			var dipreviewmanuallynricpostcode = localStorage.getItem("dipreviewmanuallynricpostcode_"+c);
			var dipreviewmanuallynricstreetname = localStorage.getItem("dipreviewmanuallynricstreetname_"+c);
			var dipreviewmanuallynricfloor = localStorage.getItem("dipreviewmanuallynricfloor_"+c);
			var dipreviewnrmanuallyicunit = localStorage.getItem("dipreviewnrmanuallyicunit_"+c);

			//di fin passport preview popup
			var difincardpopupsurname = localStorage.getItem("difincardpopupsurname_"+c);
			var difincardpopupgivenName = localStorage.getItem("difincardpopupgivenName_"+c);
			var difincardpopuppassportno = localStorage.getItem("difincardpopuppassportno_"+c);
			var difincardpopupnationality = localStorage.getItem("difincardpopupnationality_"+c);
			var difincardpopupcountry = localStorage.getItem("difincardpopupcountry_"+c);
			var difincardpopupgender = localStorage.getItem("difincardpopupgender_"+c);
			var difincardpopupdateexpiry = localStorage.getItem("difincardpopupdateexpiry_"+c);
			var difincardpopupdob = localStorage.getItem("difincardpopupdob_"+c);

			//di fincard front preview 
			var difincardfrontemployer = localStorage.getItem("difincardfrontemployer_"+c);
			var difincardfrontoccupation = localStorage.getItem("difincardfrontoccupation_"+c);
			var difincardfrontdateissue = localStorage.getItem("difincardfrontdateissue_"+c);
			var difincardfrontdateexpiry = localStorage.getItem("difincardfrontdateexpiry_"+c);
			var diwfincardfrontNumber = localStorage.getItem("diwfincardfrontNumber_"+c);

			//di fincard back preview 
			var difincardbackdateissue = localStorage.getItem("difincardbackdateissue_"+c);
			var difincardbackdateexpiry = localStorage.getItem("difincardbackdateexpiry_"+c);
			var diwfincardbackNumber = localStorage.getItem("diwfincardbackNumber_"+c);

			//di fincard  manual popup
			var difincardmanuallypassportno = localStorage.getItem("difincardmanuallypassportno_"+c);
			var difincardmanuallynationality = localStorage.getItem("difincardmanuallynationality_"+c);
			var difincardmanuallycountry = localStorage.getItem("difincardmanuallycountry_"+c);
			var difincardmanuallygender = localStorage.getItem("difincardmanuallygender_"+c);
			var difincardmanuallydatetimepicker = localStorage.getItem("difincardmanuallydatetimepicker_"+c);
			var difincardmanuallydateexpiry = localStorage.getItem("difincardmanuallydateexpiry_"+c);
			var difincardmanuallyplaceofBirth = localStorage.getItem("difincardmanuallyplaceofBirth_"+c);
			var difincardmanuallydateBirth = localStorage.getItem("difincardmanuallydateBirth_"+c);
		
			var difincardmanuallyemployer = localStorage.getItem("difincardmanuallyemployer_"+c);
			var difincardmanuallyoccupation = localStorage.getItem("difincardmanuallyoccupation_"+c);
			var difincardmanuallyworkpassdateissue = localStorage.getItem("difincardmanuallyworkpassdateissue_"+c);
			var difincardmanuallyworkpassdateexpiry = localStorage.getItem("difincardmanuallyworkpassdateexpiry_"+c);
			var difincardmanuallywfincardnumber = localStorage.getItem("difincardmanuallywfincardnumber_"+c);

			var director_shareholder_paid_up_capital = localStorage.getItem("director_shareholder_paid_up_capital_"+c);
			var director_shareholder_number_of_shares = localStorage.getItem("director_shareholder_number_of_shares_"+c);

			params += '&dishareholdernonresidentresaddress'+c+'='+dishareholdernonresidentresaddress+'&dishareholderFINresaddress'+c+'='+dishareholderFINresaddress+'&dishareholdersurname'+c+'='+dishareholdersurname1+'&dishareholderresstatus'+c+'='+dishareholderresstatus1+'&dishareholderbowner'+c+'='+dishareholderbowner1+'&dinonresidentpopupsurname'+c+'='+dinonresidentpopupsurname+'&dinonresidentpopupgivenName'+c+'='+dinonresidentpopupgivenName+'&dinonresidentpopuppassportno'+c+'='+dinonresidentpopuppassportno+'&dinonresidentpopupnationality'+c+'='+dinonresidentpopupnationality+'&dinonresidentpopupcountry'+c+'='+dinonresidentpopupcountry+'&dinonresidentpopupgender'+c+'='+dinonresidentpopupgender+'&dinonresidentpopupdateexpiry'+c+'='+dinonresidentpopupdateexpiry+'&dinonresidentpopupdob'+c+'='+dinonresidentpopupdob+'&dinonmanuallypassportno'+c+'='+dinonmanuallypassportno+'&dinonmanuallynationality'+c+'='+dinonmanuallynationality+'&dinonmanuallycountry'+c+'='+dinonmanuallycountry+'&dinonmanuallygender'+c+'='+dinonmanuallygender+'&dinonmanuallydatetimepicker'+c+'='+dinonmanuallydatetimepicker+'&dinonmanuallydateexpiry'+c+'='+dinonmanuallydateexpiry+'&dinonmanuallyplaceofBirth'+c+'='+dinonmanuallyplaceofBirth+'&dinonmanuallydateBirth'+c+'='+dinonmanuallydateBirth+'&dishareholderhandphone'+c+'='+dishareholderhandphone+'&dishareholderOfficephone'+c+'='+dishareholderOfficephone+'&dishareholderbusinessemail'+c+'='+dishareholderbusinessemail+'&dishareholderPersonalemail'+c+'='+dishareholderPersonalemail+'&dishareholderpep'+c+'='+dishareholderpep+'&dishareholdercountrypep'+c+'='+dishareholdercountrypep+'&dishareholderrolePep'+c+'='+dishareholderrolePep+'&dishareholderpepFrom'+c+'='+dishareholderpepFrom+'&dishareholderpepTo'+c+'='+dishareholderpepTo+'&dipreviewpopupfrontnricnumber'+c+'='+dipreviewpopupfrontnricnumber+'&dipreviewpopupnricfrontgender'+c+'='+dipreviewpopupnricfrontgender+'&dipreviewpopupnricfrontdob'+c+'='+dipreviewpopupnricfrontdob+'&dipreviewpopupnricfrontnationality'+c+'='+dipreviewpopupnricfrontnationality+'&dipreviewpopupnricbackpostcode'+c+'='+dipreviewpopupnricbackpostcode+'&dipreviewpopupnricbackstreetname'+c+'='+dipreviewpopupnricbackstreetname+'&dipreviewpopupnricbackfloor'+c+'='+dipreviewpopupnricbackfloor+'&dipreviewnrpopupicbackunit'+c+'='+dipreviewnrpopupicbackunit+'&dipreviewmanuallynricnumber'+c+'='+dipreviewmanuallynricnumber+'&dipreviewmanuallynricgender'+c+'='+dipreviewmanuallynricgender+'&dipreviewmanuallynricdob'+c+'='+dipreviewmanuallynricdob+'&dipreviewmanuallynricnationality'+c+'='+dipreviewmanuallynricnationality+'&dipreviewmanuallynricpostcode'+c+'='+dipreviewmanuallynricpostcode+'&dipreviewmanuallynricstreetname'+c+'='+dipreviewmanuallynricstreetname+'&dipreviewmanuallynricfloor'+c+'='+dipreviewmanuallynricfloor+'&dipreviewnrmanuallyicunit'+c+'='+dipreviewnrmanuallyicunit+'&difincardpopupsurname'+c+'='+difincardpopupsurname+'&difincardpopupgivenName'+c+'='+difincardpopupgivenName+'&difincardpopuppassportno'+c+'='+difincardpopuppassportno+'&difincardpopupnationality'+c+'='+difincardpopupnationality+'&difincardpopupcountry'+c+'='+difincardpopupcountry+'&difincardpopupgender'+c+'='+difincardpopupgender+'&difincardpopupdateexpiry'+c+'='+difincardpopupdateexpiry+'&difincardpopupdob'+c+'='+difincardpopupdob+'&difincardfrontemployer'+c+'='+difincardfrontemployer+'&difincardfrontoccupation'+c+'='+difincardfrontoccupation+'&difincardfrontdateissue'+c+'='+difincardfrontdateissue+'&difincardfrontdateexpiry'+c+'='+difincardfrontdateexpiry+'&diwfincardfrontNumber'+c+'='+diwfincardfrontNumber+'&difincardbackdateissue'+c+'='+difincardbackdateissue+'&difincardbackdateexpiry'+c+'='+difincardbackdateexpiry+'&diwfincardbackNumber'+c+'='+diwfincardbackNumber+'&difincardmanuallypassportno'+c+'='+difincardmanuallypassportno+'&difincardmanuallynationality'+c+'='+difincardmanuallynationality+'&difincardmanuallycountry'+c+'='+difincardmanuallycountry+'&difincardmanuallygender'+c+'='+difincardmanuallygender+'&difincardmanuallydatetimepicker'+c+'='+difincardmanuallydatetimepicker+'&difincardmanuallydateexpiry'+c+'='+difincardmanuallydateexpiry+'&difincardmanuallyplaceofBirth'+c+'='+difincardmanuallyplaceofBirth+'&difincardmanuallydateBirth'+c+'='+difincardmanuallydateBirth+'&difincardmanuallyemployer'+c+'='+difincardmanuallyemployer+'&difincardmanuallyoccupation'+c+'='+difincardmanuallyoccupation+'&difincardmanuallyworkpassdateissue'+c+'='+difincardmanuallyworkpassdateissue+'&difincardmanuallyworkpassdateexpiry'+c+'='+difincardmanuallyworkpassdateexpiry+'&difincardmanuallywfincardnumber'+c+'='+difincardmanuallywfincardnumber+'&director_shareholder_paid_up_capital'+c+'='+director_shareholder_paid_up_capital+'&director_shareholder_number_of_shares'+c+'='+director_shareholder_number_of_shares+'&dishareholdernonresidentuploadpassport'+c+'='+dishareholdernonresidentuploadpassport+'&dishareholdernonresidentaddress'+c+'='+dishareholdernonresidentaddress+'&dishareholderFINuploadPassport'+c+'='+dishareholderFINuploadPassport+'&dishareholderfincardfront'+c+'='+dishareholderfincardfront+'&dishareholderfincardback'+c+'='+dishareholderfincardback+'&dishareholderFINaddress'+c+'='+dishareholderFINaddress+'&dishareholderSGnricfront'+c+'='+dishareholderSGnricfront+'&dishareholderSGnricback'+c+'='+dishareholderSGnricback;
		}

		for(c=1;c<=counter3;c++){

			var CorporateShareholderName = localStorage.getItem("CorporateShareholderName_"+c);
			var CorporateCountry = localStorage.getItem("CorporateCountry_"+c);
			var CorporateCertificate = localStorage.getItem("CorporateCertificate_"+c);
			var CorporateCertificate1 = localStorage.getItem("CorporateCertificate1_"+c);
			var CorporateRepresentativeName = localStorage.getItem("CorporateRepresentativeName_"+c);
			var CorporateEmail = localStorage.getItem("CorporateEmail_"+c);
			var CorporatecontactNumber = localStorage.getItem("CorporatecontactNumber_"+c);
			var corporateresstatus = localStorage.getItem("corporateresstatus_"+c);
			var corporatenonresidentuploadpassport = localStorage.getItem("corporatenonresidentuploadpassport_"+c);	
			var corporatenonresidentaddress = localStorage.getItem("corporatenonresidentaddress_"+c);
			var corporatenonresidentresaddress = localStorage.getItem("corporatenonresidentresaddress_"+c);
			var corporateFINuploadPassport = localStorage.getItem("corporateFINuploadPassport_"+c);
			var corporatefincardfront = localStorage.getItem("corporatefincardfront_"+c);
			var corporatefincardback = localStorage.getItem("corporatefincardback_"+c);
			var corporateFINaddress = localStorage.getItem("corporateFINaddress_"+c);
			var corporateFINresaddress = localStorage.getItem("corporateFINresaddress_"+c);
			var corporateSGnricfront = localStorage.getItem("corporateSGnricfront_"+c);
			var corporateSGnricback = localStorage.getItem("corporateSGnricback_"+c);
		
			//=================== Corporate shareholder section =================== //
			//corporate non resident preview popup
			var corporatenonresidentpopupsurname = localStorage.getItem("corporatenonresidentpopupsurname_"+c);
			var corporatenonresidentpopupgivenName = localStorage.getItem("corporatenonresidentpopupgivenName_"+c);
			var corporatenonresidentpopuppassportno = localStorage.getItem("corporatenonresidentpopuppassportno_"+c);
			var corporatenonresidentpopupnationality = localStorage.getItem("corporatenonresidentpopupnationality_"+c);
			var corporatenonresidentpopupcountry = localStorage.getItem("corporatenonresidentpopupcountry_"+c);
			var corporatenonresidentpopupgender = localStorage.getItem("corporatenonresidentpopupgender_"+c);
			var corporatenonresidentpopupdatetimepicker = localStorage.getItem("corporatenonresidentpopupdatetimepicker_"+c);	
			var corporatenonresidentpopupdateexpiry = localStorage.getItem("corporatenonresidentpopupdateexpiry_"+c);
			var corporatenonresidentpopupplaceofBirth = localStorage.getItem("corporatenonresidentpopupplaceofBirth_"+c);
			var corporatenonresidentpopupdob = localStorage.getItem("corporatenonresidentpopupdob_"+c);

			//corporate non resident manual popup
			var corporatenonmanuallypassportno = localStorage.getItem("corporatenonmanuallypassportno_"+c);
			var corporatenonmanuallynationality = localStorage.getItem("corporatenonmanuallynationality_"+c);
			var corporatenonmanuallycountry = localStorage.getItem("corporatenonmanuallycountry_"+c);
			var corporatenonmanuallygender = localStorage.getItem("corporatenonmanuallygender_"+c);
			var corporatenonmanuallydatetimepicker = localStorage.getItem("corporatenonmanuallydatetimepicker_"+c);
			var corporatenonmanuallydateexpiry = localStorage.getItem("corporatenonmanuallydateexpiry_"+c);
			var corporatenonmanuallyplaceofBirth = localStorage.getItem("corporatenonmanuallyplaceofBirth_"+c);
			var corporatenonmanuallydateBirth = localStorage.getItem("corporatenonmanuallydateBirth_"+c);

			//corporate nric front preview popup
			var corporatepreviewpopupfrontnricnumber = localStorage.getItem("corporatepreviewpopupfrontnricnumber_"+c);
			var corporatepreviewpopupnricfrontgender = localStorage.getItem("corporatepreviewpopupnricfrontgender_"+c);
			var corporatepreviewpopupnricfrontdob = localStorage.getItem("corporatepreviewpopupnricfrontdob_"+c);
			var corporatepreviewpopupnricfrontnationality = localStorage.getItem("corporatepreviewpopupnricfrontnationality_"+c);
			
			//corporate nric back preview popup
			var corporatepreviewpopupnricbackpostcode = localStorage.getItem("corporatepreviewpopupnricbackpostcode_"+c);
			var corporatepreviewpopupnricbackstreetname = localStorage.getItem("corporatepreviewpopupnricbackstreetname_"+c);
			var corporatepreviewpopupnricbackfloor = localStorage.getItem("corporatepreviewpopupnricbackfloor_"+c);
			var corporatepreviewnrpopupicbackunit = localStorage.getItem("corporatepreviewnrpopupicbackunit_"+c);

			//corporate nric manual popup
			var corporatepreviewmanuallynricnumber = localStorage.getItem("corporatepreviewmanuallynricnumber_"+c);
			var corporatepreviewmanuallynricgender = localStorage.getItem("corporatepreviewmanuallynricgender_"+c);
			var corporatepreviewmanuallynricdob = localStorage.getItem("corporatepreviewmanuallynricdob_"+c);
			var corporatepreviewmanuallynricnationality = localStorage.getItem("corporatepreviewmanuallynricnationality_"+c);
			var corporatepreviewmanuallynricpostcode = localStorage.getItem("corporatepreviewmanuallynricpostcode_"+c);
			var corporatepreviewmanuallynricstreetname = localStorage.getItem("corporatepreviewmanuallynricstreetname_"+c);
			var corporatepreviewmanuallynricfloor = localStorage.getItem("corporatepreviewmanuallynricfloor_"+c);
			var corporatepreviewnrmanuallyicunit = localStorage.getItem("corporatepreviewnrmanuallyicunit_"+c);

			//corporate fin passport preview popup
			var corporatefincardpopupsurname = localStorage.getItem("corporatefincardpopupsurname_"+c);
			var corporatefincardpopupgivenName = localStorage.getItem("corporatefincardpopupgivenName_"+c);
			var corporatefincardpopuppassportno = localStorage.getItem("corporatefincardpopuppassportno_"+c);
			var corporatefincardpopupnationality = localStorage.getItem("corporatefincardpopupnationality_"+c);
			var corporatefincardpopupcountry = localStorage.getItem("corporatefincardpopupcountry_"+c);
			var corporatefincardpopupgender = localStorage.getItem("corporatefincardpopupgender_"+c);
			var corporatefincardpopupdateexpiry = localStorage.getItem("corporatefincardpopupdateexpiry_"+c);
			var corporatefincardpopupdob = localStorage.getItem("corporatefincardpopupdob_"+c);

			//corporate fincard front preview popup
			var corporatefincardfrontemployer = localStorage.getItem("corporatefincardfrontemployer_"+c);
			var corporatefincardfrontoccupation = localStorage.getItem("corporatefincardfrontoccupation_"+c);
			var corporatefincardfrontdateissue = localStorage.getItem("corporatefincardfrontdateissue_"+c);
			var corporatefincardfrontdateexpiry = localStorage.getItem("corporatefincardfrontdateexpiry_"+c);
			var corporatewfincardfrontNumber = localStorage.getItem("corporatewfincardfrontNumber_"+c);

			//corporate fincard back preview popup
			var corporatefincardbackdateissue = localStorage.getItem("corporatefincardbackdateissue_"+c);
			var corporatefincardbackdateexpiry = localStorage.getItem("corporatefincardbackdateexpiry_"+c);
			var corporatewfincardbackNumber = localStorage.getItem("corporatewfincardbackNumber_"+c);

			//corporate fincard  manual popup
			var corporatefincardmanuallypassportno = localStorage.getItem("corporatefincardmanuallypassportno_"+c);
			var corporatefincardmanuallynationality = localStorage.getItem("corporatefincardmanuallynationality_"+c);
			var corporatefincardmanuallycountry = localStorage.getItem("corporatefincardmanuallycountry_"+c);
			var corporatefincardmanuallygender = localStorage.getItem("corporatefincardmanuallygender_"+c);
			var corporatefincardmanuallydatetimepicker = localStorage.getItem("corporatefincardmanuallydatetimepicker_"+c);
			var corporatefincardmanuallydateexpiry = localStorage.getItem("corporatefincardmanuallydateexpiry_"+c);
			var corporatefincardmanuallyplaceofBirth = localStorage.getItem("corporatefincardmanuallyplaceofBirth_"+c);
			var corporatefincardmanuallydateBirth = localStorage.getItem("corporatefincardmanuallydateBirth_"+c);
		
			var corporatefincardmanuallyemployer = localStorage.getItem("corporatefincardmanuallyemployer_"+c);
			var corporatefincardmanuallyoccupation = localStorage.getItem("corporatefincardmanuallyoccupation_"+c);
			var corporatefincardmanuallyworkpassdateissue = localStorage.getItem("corporatefincardmanuallyworkpassdateissue_"+c);
			var corporatefincardmanuallyworkpassdateexpiry = localStorage.getItem("corporatefincardmanuallyworkpassdateexpiry_"+c);
			var corporatefincardmanuallywfincardnumber = localStorage.getItem("corporatefincardmanuallywfincardnumber_"+c);	

			var corporate_paid_up_capital = localStorage.getItem("corporate_paid_up_capital_"+c);
			var corporate_number_of_shares = localStorage.getItem("corporate_number_of_shares_"+c);
			var corporate_chart = localStorage.getItem("corporate_chart_"+c);

			params += '&corporatenonresidentaddress'+c+'='+corporatenonresidentaddress+'&corporatenonresidentresaddress'+c+'='+corporatenonresidentresaddress+'&corporateFINresaddress'+c+'='+corporateFINresaddress+'&corporateresstatus'+c+'='+corporateresstatus+'&corporatenonresidentpopupsurname'+c+'='+corporatenonresidentpopupsurname+'&corporatenonresidentpopupgivenName'+c+'='+corporatenonresidentpopupgivenName+'&corporatenonresidentpopuppassportno'+c+'='+corporatenonresidentpopuppassportno+'&corporatenonresidentpopupnationality'+c+'='+corporatenonresidentpopupnationality+'&corporatenonresidentpopupcountry'+c+'='+corporatenonresidentpopupcountry+'&corporatenonresidentpopupgender'+c+'='+corporatenonresidentpopupgender+'&corporatenonresidentpopupdatetimepicker'+c+'='+corporatenonresidentpopupdatetimepicker+'&corporatenonresidentpopupdateexpiry'+c+'='+corporatenonresidentpopupdateexpiry+'&corporatenonresidentpopupplaceofBirth'+c+'='+corporatenonresidentpopupplaceofBirth+'&corporatenonresidentpopupdob'+c+'='+corporatenonresidentpopupdob+'&corporatenonmanuallypassportno'+c+'='+corporatenonmanuallypassportno+'&corporatenonmanuallynationality'+c+'='+corporatenonmanuallynationality+'&corporatenonmanuallycountry'+c+'='+corporatenonmanuallycountry+'&corporatenonmanuallygender'+c+'='+corporatenonmanuallygender+'&corporatenonmanuallydatetimepicker'+c+'='+corporatenonmanuallydatetimepicker+'&corporatenonmanuallydateexpiry'+c+'='+corporatenonmanuallydateexpiry+'&corporatenonmanuallyplaceofBirth'+c+'='+corporatenonmanuallyplaceofBirth+'&corporatenonmanuallydateBirth'+c+'='+corporatenonmanuallydateBirth+'&CorporateShareholderName'+c+'='+CorporateShareholderName+'&CorporateCountry'+c+'='+CorporateCountry+'&CorporatecontactNumber'+c+'='+CorporatecontactNumber+'&CorporateEmail'+c+'='+CorporateEmail+'&CorporateCertificate'+c+'='+CorporateCertificate+'&CorporateCertificate1_'+c+'='+CorporateCertificate1+'&CorporateRepresentativeName'+c+'='+CorporateRepresentativeName+'&corporatepreviewpopupfrontnricnumber'+c+'='+corporatepreviewpopupfrontnricnumber+'&corporatepreviewpopupnricfrontgender'+c+'='+corporatepreviewpopupnricfrontgender+'&corporatepreviewpopupnricfrontdob'+c+'='+corporatepreviewpopupnricfrontdob+'&corporatepreviewpopupnricfrontnationality'+c+'='+corporatepreviewpopupnricfrontnationality+'&corporatepreviewpopupnricbackpostcode'+c+'='+corporatepreviewpopupnricbackpostcode+'&corporatepreviewpopupnricbackstreetname'+c+'='+corporatepreviewpopupnricbackstreetname+'&corporatepreviewpopupnricbackfloor'+c+'='+corporatepreviewpopupnricbackfloor+'&corporatepreviewnrpopupicbackunit'+c+'='+corporatepreviewnrpopupicbackunit+'&corporatepreviewmanuallynricnumber'+c+'='+corporatepreviewmanuallynricnumber+'&corporatepreviewmanuallynricgender'+c+'='+corporatepreviewmanuallynricgender+'&corporatepreviewmanuallynricdob'+c+'='+corporatepreviewmanuallynricdob+'&corporatepreviewmanuallynricnationality'+c+'='+corporatepreviewmanuallynricnationality+'&corporatepreviewmanuallynricpostcode'+c+'='+corporatepreviewmanuallynricpostcode+'&corporatepreviewmanuallynricstreetname'+c+'='+corporatepreviewmanuallynricstreetname+'&corporatepreviewmanuallynricfloor'+c+'='+corporatepreviewmanuallynricfloor+'&corporatepreviewnrmanuallyicunit'+c+'='+corporatepreviewnrmanuallyicunit+'&corporatefincardpopupsurname'+c+'='+corporatefincardpopupsurname+'&corporatefincardpopupgivenName'+c+'='+corporatefincardpopupgivenName+'&corporatefincardpopuppassportno'+c+'='+corporatefincardpopuppassportno+'&corporatefincardpopupnationality'+c+'='+corporatefincardpopupnationality+'&corporatefincardpopupcountry'+c+'='+corporatefincardpopupcountry+'&corporatefincardpopupgender'+c+'='+corporatefincardpopupgender+'&corporatefincardpopupdateexpiry'+c+'='+corporatefincardpopupdateexpiry+'&corporatefincardpopupdob'+c+'='+corporatefincardpopupdob+'&corporatefincardfrontemployer'+c+'='+corporatefincardfrontemployer+'&corporatefincardfrontoccupation'+c+'='+corporatefincardfrontoccupation+'&corporatefincardfrontdateissue'+c+'='+corporatefincardfrontdateissue+'&corporatefincardfrontdateexpiry'+c+'='+corporatefincardfrontdateexpiry+'&corporatewfincardfrontNumber'+c+'='+corporatewfincardfrontNumber+'&corporatefincardbackdateissue'+c+'='+corporatefincardbackdateissue+'&corporatefincardbackdateexpiry'+c+'='+corporatefincardbackdateexpiry+'&corporatewfincardbackNumber'+c+'='+corporatewfincardbackNumber+'&corporatefincardmanuallypassportno'+c+'='+corporatefincardmanuallypassportno+'&corporatefincardmanuallynationality'+c+'='+corporatefincardmanuallynationality+'&corporatefincardmanuallycountry'+c+'='+corporatefincardmanuallycountry+'&corporatefincardmanuallygender'+c+'='+corporatefincardmanuallygender+'&corporatefincardmanuallydatetimepicker'+c+'='+corporatefincardmanuallydatetimepicker+'&corporatefincardmanuallydateexpiry'+c+'='+corporatefincardmanuallydateexpiry+'&corporatefincardmanuallyplaceofBirth'+c+'='+corporatefincardmanuallyplaceofBirth+'&corporatefincardmanuallydateBirth'+c+'='+corporatefincardmanuallydateBirth+'&corporatefincardmanuallyemployer'+c+'='+corporatefincardmanuallyemployer+'&corporatefincardmanuallyoccupation'+c+'='+corporatefincardmanuallyoccupation+'&corporatefincardmanuallyworkpassdateissue'+c+'='+corporatefincardmanuallyworkpassdateissue+'&corporatefincardmanuallyworkpassdateexpiry'+c+'='+corporatefincardmanuallyworkpassdateexpiry+'&corporatefincardmanuallywfincardnumber'+c+'='+corporatefincardmanuallywfincardnumber+'&corporate_paid_up_capital'+c+'='+corporate_paid_up_capital+'&corporate_number_of_shares'+c+'='+corporate_number_of_shares+'&CorporateCertificate'+c+'='+CorporateCertificate+'&CorporateCertificate1_'+c+'='+CorporateCertificate1+'&corporatenonresidentuploadpassport'+c+'='+corporatenonresidentuploadpassport+'&corporatenonresidentaddress'+c+'='+corporatenonresidentaddress+'&corporateFINuploadPassport'+c+'='+corporateFINuploadPassport+'&corporatefincardfront'+c+'='+corporatefincardfront+'&corporatefincardback'+c+'='+corporatefincardback+'&corporateFINaddress'+c+'='+corporateFINaddress+'&corporateSGnricfront'+c+'='+corporateSGnricfront+'&corporateSGnricback'+c+'='+corporateSGnricback+'&corporate_chart'+c+'='+corporate_chart;	
		}
		//declaration page
		var currency = localStorage.getItem('currency');
		var paid_up_capital = localStorage.getItem('paid_up_capital');
		var number_of_shares = localStorage.getItem('number_of_shares');

		var agent_full_name = localStorage.getItem('agent_full_name');
		var agent_email = localStorage.getItem('agent_email');
		var agent_contact_no = localStorage.getItem('agent_contact_no');

		var manual_director_passport = localStorage.getItem("MANUAL_DIRECTOR_NONRESIDENT_PASSPORT");
		var manual_director_nric = localStorage.getItem("MANUAL_DIRECTOR_CITIZENPR_NRIC");
		var manual_director_fin = localStorage.getItem("MANUAL_DIRECTOR_FIN_PASSPORT");

		params += '&currency='+currency+'&paid_up_capital='+paid_up_capital+'&number_of_shares='+number_of_shares+'&agent_full_name='+agent_full_name+'&agent_email='+agent_email+'&agent_contact_no='+agent_contact_no+'&manual_director_passport='+manual_director_passport+'&manual_director_nric='+manual_director_nric+'&manual_director_fin='+manual_director_fin;

		for(c=1;c<=bocounter;c++){

			//=================== BO section =================== //
			//bo non resident preview popup

			var getboname = localStorage.getItem("getboname");
			var borestatus = localStorage.getItem("bo_residencystatus_"+c+"_"+getboname);
			//bo non passport upload
			var bosurname = localStorage.getItem("bosurname_"+c+"_"+getboname);
			var bogivenname = localStorage.getItem("bogivenName_"+c+"_"+getboname);
			var bopassportno = localStorage.getItem("bopassortno_"+c+"_"+getboname);
			var bonationality = localStorage.getItem("bonationality_"+c+"_"+getboname);
			var bocountry = localStorage.getItem("bocntry_"+c+"_"+getboname);
			var bogender = localStorage.getItem("bogender_"+c+"_"+getboname);
			var bodob = localStorage.getItem("bodob_"+c+"_"+getboname);
			var bodateexpiry = localStorage.getItem("bodateexpiry_"+c+"_"+getboname);
			//bo non passport manual
			var bomdatetimepicker = localStorage.getItem("bononmanuallydatetimepicker_"+c+"_"+getboname);
			var bompob = localStorage.getItem("bononmanuallyplaceofBirth_"+c+"_"+getboname);
			var bompassportno = localStorage.getItem("bononmanuallypassportno_"+c+"_"+getboname);
			var bomnationality = localStorage.getItem("bononmanuallynationality_"+c+"_"+getboname);
			var bomcountry = localStorage.getItem("bononmanuallycountry_"+c+"_"+getboname);
			var bomgender = localStorage.getItem("bononmanuallygender_"+c+"_"+getboname);
			var bomdob = localStorage.getItem("bononmanuallydateBirth_"+c+"_"+getboname);
			var bomdateexpiry = localStorage.getItem("bononmanuallydateexpiry_"+c+"_"+getboname);
			//bo nric manual
			var bopreviewnricnumber = localStorage.getItem("bopreviewmanuallynricnumber_"+c+"_"+getboname);
			var bopreviewnricgender = localStorage.getItem("bopreviewmanuallynricgender_"+c+"_"+getboname);
			var bopreviewnricdoi = localStorage.getItem("bopreviewmanuallynricdob_"+c+"_"+getboname);
			var bopreviewnriccob = localStorage.getItem("bopreviewmanuallynricnationality_"+c+"_"+getboname);
			var bopreviewnricpostcode = localStorage.getItem("bopreviewmanuallynricpostcode_"+c+"_"+getboname);
			var bopreviewnricstreetname = localStorage.getItem("bopreviewmanuallynricstreetname_"+c+"_"+getboname);
			var bopreviewnricfloor = localStorage.getItem("bopreviewmanuallynricfloor_"+c+"_"+getboname);
			var bopreviewnricunit = localStorage.getItem("bopreviewnrmanuallyicunit_"+c+"_"+getboname);
			//bo nric front upload
			var bopreviewfrontnricnumber = localStorage.getItem("bopreviewnricfrontnumber_"+c+"_"+getboname);
			var bopreviewfrontnricdoi = localStorage.getItem("bopreviewnricfrontdoi_"+c+"_"+getboname);
			var bopreviewfrontnriccob = localStorage.getItem("bopreviewnricfrontcob_"+c+"_"+getboname);
			var bopreviewfrontnricgender = localStorage.getItem("bopreviewnricfrontgender_"+c+"_"+getboname);
			//bo nric back upload
			var bopreviewbacknricpostcode = localStorage.getItem("bopreviewnricbackpostcode_"+c+"_"+getboname);
			var bopreviewbacknricstreetname = localStorage.getItem("bopreviewnricbackstreetname_"+c+"_"+getboname);
			var bopreviewbacknricfloor = localStorage.getItem("bopreviewnricbackfloor_"+c+"_"+getboname);
			var bopreviewbacknricunit = localStorage.getItem("bopreviewnricbackunit_"+c+"_"+getboname);
			//bo fin popup
			var bofinsurname = localStorage.getItem("bofincardsurname_"+c+"_"+getboname);
			var bofingivenname = localStorage.getItem("bofincardgivenName_"+c+"_"+getboname);
			var bofinpassportno = localStorage.getItem("bofincardpassportno_"+c+"_"+getboname);
			var bofinnationality = localStorage.getItem("bofincardnationality_"+c+"_"+getboname);
			var bofincountry = localStorage.getItem("bofincardcountry_"+c+"_"+getboname);
			var bofingender = localStorage.getItem("bofincardgender_"+c+"_"+getboname);
			//var bofindatetimepicker = localStorage.getItem("bofindatetimepicker_"+c+"_"+getboname);
			var bofindateexpiry = localStorage.getItem("bofincarddateexpiry_"+c+"_"+getboname);
			var bofindob = localStorage.getItem("bofincarddob_"+c+"_"+getboname);
			//var bofinpob = localStorage.getItem("bofinpob_"+c+"_"+getboname);

			var bofinfrontemployer = localStorage.getItem("bofincardfrontemployer_"+c+"_"+getboname);
			var bofinfrontoccupation = localStorage.getItem("bofincardfrontoccupation_"+c+"_"+getboname);
			var bowfinfrontdoi = localStorage.getItem("bofincardfrontdateissue_"+c+"_"+getboname);
			var bowfinfrontdoe = localStorage.getItem("bofincardfrontdateexpiry_"+c+"_"+getboname);
			var bofinfrontnumber = localStorage.getItem("bowfincardfrontNumber_"+c+"_"+getboname);

			var bowfinbackdoi = localStorage.getItem("bofincardbackdateissue_"+c+"_"+getboname);
			var bowfinbackdoe = localStorage.getItem("bofincardbackdateexpiry_"+c+"_"+getboname);
			var bofinbacknumber = localStorage.getItem("bowfincardbackNumber_"+c+"_"+getboname);

			//bo fin manual
			var bomfinpassportno = localStorage.getItem("bofincardmanuallypassportno_"+c+"_"+getboname);
			var bomfinnationality = localStorage.getItem("bofincardmanuallynationality_"+c+"_"+getboname);
			var bomfincountry = localStorage.getItem("bofincardmanuallycountry_"+c+"_"+getboname);
			var bomfingender = localStorage.getItem("bofincardmanuallygender_"+c+"_"+getboname);
			var bomfindatetimepicker = localStorage.getItem("bofincardmanuallydatetimepicker_"+c+"_"+getboname);
			var bomfindateexpiry = localStorage.getItem("bofincardmanuallydateexpiry_"+c+"_"+getboname);
			var bomfindob = localStorage.getItem("bofincardmanuallydateBirth_"+c+"_"+getboname);
			var bomfinpob = localStorage.getItem("bofincardmanuallyplaceofBirth_"+c+"_"+getboname);
			var bomfinemployer = localStorage.getItem("bofincardmanuallyemployer_"+c+"_"+getboname);
			var bomfinoccupation = localStorage.getItem("bofincardmanuallyoccupation_"+c+"_"+getboname);
			var bomwfindoi = localStorage.getItem("bofincardmanuallyworkpassdateissue_"+c+"_"+getboname);
			var bomwfindoe = localStorage.getItem("bofincardmanuallyworkpassdateexpiry_"+c+"_"+getboname);
			var bomfinnumber = localStorage.getItem("bofincardmanuallywfincardnumber_"+c+"_"+getboname);
	
			var bofullname = localStorage.getItem("bo_fullName_"+c+"_"+getboname);
			var boemail = localStorage.getItem("bo_email_"+c+"_"+getboname);
			var bocontactno = localStorage.getItem("bo_contact_no_"+c+"_"+getboname);

			//uploaded document fields
			var bopassport = localStorage.getItem("bouploadpassport_"+c+"_"+getboname);
			var boaddressProof = localStorage.getItem("boaddress_"+c+"_"+getboname);
			var boresaddress = localStorage.getItem("boresaddress_"+c+"_"+getboname);
			var bofinPassport = localStorage.getItem("bofinuploadPassport_"+c+"_"+getboname);
			var bofinCardFront = localStorage.getItem("bofinfrontupload_"+c+"_"+getboname);
			var bofinCardBack = localStorage.getItem("bofinbackupload_"+c+"_"+getboname);
			var bofinAddressProof = localStorage.getItem("bofinaddress_"+c+"_"+getboname);
			var bofinresaddress = localStorage.getItem("bofinresaddress_"+c+"_"+getboname);
			var bonricfront = localStorage.getItem("bonricfront_"+c+"_"+getboname);
			var bonricback = localStorage.getItem("bonricback_"+c+"_"+getboname);	

			var bopep = localStorage.getItem("bopep_"+c+"_"+getboname);
		        var bopepCountry = localStorage.getItem("bopepCountry_"+c+"_"+getboname);
		        var bopepRole = localStorage.getItem("bopepRole_"+c+"_"+getboname);
		        var bopepFrom = localStorage.getItem("bopepFrom_"+c+"_"+getboname);
		        var bopepTo = localStorage.getItem("bopepTo_"+c+"_"+getboname);			

			params += '&bosurname'+c+'='+bosurname+'&bogivenname'+c+'='+bogivenname+'&bopassportno'+c+'='+bopassportno+'&bonationality'+c+'='+bonationality+'&bocountry'+c+'='+bocountry+'&bogender'+c+'='+bogender+'&bodob'+c+'='+bodob+'&bodateexpiry'+c+'='+bodateexpiry+'&bomdatetimepicker'+c+'='+bomdatetimepicker+'&bompob'+c+'='+bompob+'&bompassportno'+c+'='+bompassportno+'&bomnationality'+c+'='+bomnationality+'&bomcountry'+c+'='+bomcountry+'&bomgender'+c+'='+bomgender+'&bomdob'+c+'='+bomdob+'&bomdateexpiry'+c+'='+bomdateexpiry+'&bopreviewnricnumber'+c+'='+bopreviewnricnumber+'&bopreviewnricgender'+c+'='+bopreviewnricgender+'&bopreviewnricdoi'+c+'='+bopreviewnricdoi+'&bopreviewnriccob'+c+'='+bopreviewnriccob+'&bopreviewnricpostcode'+c+'='+bopreviewnricpostcode+'&bopreviewnricstreetname'+c+'='+bopreviewnricstreetname+'&bopreviewnricfloor'+c+'='+bopreviewnricfloor+'&bopreviewnricunit'+c+'='+bopreviewnricunit+'&bomfinpassportno'+c+'='+bomfinpassportno+'&bomfinnationality'+c+'='+bomfinnationality+'&bomfincountry'+c+'='+bomfincountry+'&bomfingender'+c+'='+bomfingender+'&bomfindatetimepicker'+c+'='+bomfindatetimepicker+'&bomfindateexpiry'+c+'='+bomfindateexpiry+'&bomfindob'+c+'='+bomfindob+'&bomfinpob'+c+'='+bomfinpob+'&bomfinemployer'+c+'='+bomfinemployer+'&bomfinoccupation'+c+'='+bomfinoccupation+'&bomwfindoi'+c+'='+bomwfindoi+'&bomwfindoe'+c+'='+bomwfindoe+'&bomfinnumber'+c+'='+bomfinnumber+'&bopreviewfrontnricnumber'+c+'='+bopreviewfrontnricnumber+'&bopreviewfrontnricdoi'+c+'='+bopreviewfrontnricdoi+'&bopreviewfrontnriccob'+c+'='+bopreviewfrontnriccob+'&bopreviewfrontnricgender'+c+'='+bopreviewfrontnricgender+'&bopreviewbacknricpostcode'+c+'='+bopreviewbacknricpostcode+'&bopreviewbacknricstreetname'+c+'='+bopreviewbacknricstreetname+'&bopreviewbacknricfloor'+c+'='+bopreviewbacknricfloor+'&bopreviewbacknricunit'+c+'='+bopreviewbacknricunit+'&bofinsurname'+c+'='+bofinsurname+'&bofingivenname'+c+'='+bofingivenname+'&bofinpassportno'+c+'='+bofinpassportno+'&bofinnationality'+c+'='+bofinnationality+'&bofincountry'+c+'='+bofincountry+'&bofindateexpiry'+c+'='+bofindateexpiry+'&bofindob'+c+'='+bofindob+'&bofinfrontemployer'+c+'='+bofinfrontemployer+'&bofinfrontoccupation'+c+'='+bofinfrontoccupation+'&bowfinfrontdoi'+c+'='+bowfinfrontdoi+'&bowfinfrontdoe'+c+'='+bowfinfrontdoe+'&bofinfrontnumber'+c+'='+bofinfrontnumber+'&bowfinbackdoi'+c+'='+bowfinbackdoi+'&bowfinbackdoe'+c+'='+bowfinbackdoe+'&bofinbacknumber'+c+'='+bofinbacknumber+'&bofullname'+c+'='+bofullname+'&boemail'+c+'='+boemail+'&bocontactno'+c+'='+bocontactno+'&bopassport'+c+'='+bopassport+'&boaddressProof'+c+'='+boaddressProof+'&boresaddress'+c+'='+boresaddress+'&bofinPassport'+c+'='+bofinPassport+'&bofinCardFront'+c+'='+bofinCardFront+'&bofinCardBack'+c+'='+bofinCardBack+'&bofinAddressProof'+c+'='+bofinAddressProof+'&bofinresaddress'+c+'='+bofinresaddress+'&bonricfront'+c+'='+bonricfront+'&bonricback'+c+'='+bonricback+'&borestatus'+c+'='+borestatus+'&bopep'+c+'='+bopep+'&bopepCountry'+c+'='+bopepCountry+'&bopepRole'+c+'='+bopepRole+'&bopepFrom'+c+'='+bopepFrom+'&bopepTo'+c+'='+bopepTo+'&getboname='+getboname;	
			//+'&bofindatetimepicker'+c+'='+bofindatetimepicker+'&bofinpob'+c+'='+bofinpob
		}

		for(c=1;c<=bodicounter;c++){

			//=================== BO section =================== //
			//bo non resident preview popup

			var getbodiname = localStorage.getItem("getbodiname");
			var bodirestatus = localStorage.getItem("bo_residencystatus_"+c+"_"+getbodiname);
			//bo non passport upload
			var bodisurname = localStorage.getItem("bosurname_"+c+"_"+getbodiname);
			var bodigivenname = localStorage.getItem("bogivenName_"+c+"_"+getbodiname);
			var bodipassportno = localStorage.getItem("bopassortno_"+c+"_"+getbodiname);
			var bodinationality = localStorage.getItem("bonationality_"+c+"_"+getbodiname);
			var bodicountry = localStorage.getItem("bocntry_"+c+"_"+getbodiname);
			var bodigender = localStorage.getItem("bogender_"+c+"_"+getbodiname);
			var bodidob = localStorage.getItem("bodob_"+c+"_"+getbodiname);
			var bodidateexpiry = localStorage.getItem("bodateexpiry_"+c+"_"+getbodiname);
			//bo non passport manual
			var bodimdatetimepicker = localStorage.getItem("bononmanuallydatetimepicker_"+c+"_"+getbodiname);
			var bodimpob = localStorage.getItem("bononmanuallyplaceofBirth_"+c+"_"+getbodiname);
			var bodimpassportno = localStorage.getItem("bononmanuallypassportno_"+c+"_"+getbodiname);
			var bodimnationality = localStorage.getItem("bononmanuallynationality_"+c+"_"+getbodiname);
			var bodimcountry = localStorage.getItem("bononmanuallycountry_"+c+"_"+getbodiname);
			var bodimgender = localStorage.getItem("bononmanuallygender_"+c+"_"+getbodiname);
			var bodimdob = localStorage.getItem("bononmanuallydateBirth_"+c+"_"+getbodiname);
			var bodimdateexpiry = localStorage.getItem("bononmanuallydateexpiry_"+c+"_"+getbodiname);
			//bo nric manual
			var bodipreviewnricnumber = localStorage.getItem("bopreviewmanuallynricnumber_"+c+"_"+getbodiname);
			var bodipreviewnricgender = localStorage.getItem("bopreviewmanuallynricgender_"+c+"_"+getbodiname);
			var bodipreviewnricdoi = localStorage.getItem("bopreviewmanuallynricdob_"+c+"_"+getbodiname);
			var bodipreviewnriccob = localStorage.getItem("bopreviewmanuallynricnationality_"+c+"_"+getbodiname);
			var bodipreviewnricpostcode = localStorage.getItem("bopreviewmanuallynricpostcode_"+c+"_"+getbodiname);
			var bodipreviewnricstreetname = localStorage.getItem("bopreviewmanuallynricstreetname_"+c+"_"+getbodiname);
			var bodipreviewnricfloor = localStorage.getItem("bopreviewmanuallynricfloor_"+c+"_"+getbodiname);
			var bodipreviewnricunit = localStorage.getItem("bopreviewnrmanuallyicunit_"+c+"_"+getbodiname);
			//bo nric front upload
			var bodipreviewfrontnricnumber = localStorage.getItem("bopreviewnricfrontnumber_"+c+"_"+getbodiname);
			var bodipreviewfrontnricdoi = localStorage.getItem("bopreviewnricfrontdoi_"+c+"_"+getbodiname);
			var bodipreviewfrontnriccob = localStorage.getItem("bopreviewnricfrontcob_"+c+"_"+getbodiname);
			var bodipreviewfrontnricgender = localStorage.getItem("bopreviewnricfrontgender_"+c+"_"+getbodiname);
			//bo nric back upload
			var bodipreviewbacknricpostcode = localStorage.getItem("bopreviewnricbackpostcode_"+c+"_"+getbodiname);
			var bodipreviewbacknricstreetname = localStorage.getItem("bopreviewnricbackstreetname_"+c+"_"+getbodiname);
			var bodipreviewbacknricfloor = localStorage.getItem("bopreviewnricbackfloor_"+c+"_"+getbodiname);
			var bodipreviewbacknricunit = localStorage.getItem("bopreviewnricbackunit_"+c+"_"+getbodiname);
			//bo fin popup
			var bodifinsurname = localStorage.getItem("bofincardsurname_"+c+"_"+getbodiname);
			var bodifingivenname = localStorage.getItem("bofincardgivenName_"+c+"_"+getbodiname);
			var bodifinpassportno = localStorage.getItem("bofincardpassportno_"+c+"_"+getbodiname);
			var bodifinnationality = localStorage.getItem("bofincardnationality_"+c+"_"+getbodiname);
			var bodifincountry = localStorage.getItem("bofincardcountry_"+c+"_"+getbodiname);
			var bodifingender = localStorage.getItem("bofincardgender_"+c+"_"+getbodiname);
			//var bodifindatetimepicker = localStorage.getItem("bofindatetimepicker_"+c+"_"+getbodiname);
			var bodifindateexpiry = localStorage.getItem("bofincarddateexpiry_"+c+"_"+getbodiname);
			var bodifindob = localStorage.getItem("bofincarddob_"+c+"_"+getbodiname);
			//var bodifinpob = localStorage.getItem("bofinpob_"+c+"_"+getbodiname);

			var bodifinfrontemployer = localStorage.getItem("bofincardfrontemployer_"+c+"_"+getbodiname);
			var bodifinfrontoccupation = localStorage.getItem("bofincardfrontoccupation_"+c+"_"+getbodiname);
			var bodiwfinfrontdoi = localStorage.getItem("bofincardfrontdateissue_"+c+"_"+getbodiname);
			var bodiwfinfrontdoe = localStorage.getItem("bofincardfrontdateexpiry_"+c+"_"+getbodiname);
			var bodifinfrontnumber = localStorage.getItem("bowfincardfrontNumber_"+c+"_"+getbodiname);

			var bodiwfinbackdoi = localStorage.getItem("bofincardbackdateissue_"+c+"_"+getbodiname);
			var bodiwfinbackdoe = localStorage.getItem("bofincardbackdateexpiry_"+c+"_"+getbodiname);
			var bodifinbacknumber = localStorage.getItem("bowfincardbackNumber_"+c+"_"+getbodiname);

			//bo fin manual
			var bodimfinpassportno = localStorage.getItem("bofincardmanuallypassportno_"+c+"_"+getbodiname);
			var bodimfinnationality = localStorage.getItem("bofincardmanuallynationality_"+c+"_"+getbodiname);
			var bodimfincountry = localStorage.getItem("bofincardmanuallycountry_"+c+"_"+getbodiname);
			var bodimfingender = localStorage.getItem("bofincardmanuallygender_"+c+"_"+getbodiname);
			var bodimfindatetimepicker = localStorage.getItem("bofincardmanuallydatetimepicker_"+c+"_"+getbodiname);
			var bodimfindateexpiry = localStorage.getItem("bofincardmanuallydateexpiry_"+c+"_"+getbodiname);
			var bodimfindob = localStorage.getItem("bofincardmanuallydateBirth_"+c+"_"+getbodiname);
			var bodimfinpob = localStorage.getItem("bofincardmanuallyplaceofBirth_"+c+"_"+getbodiname);
			var bodimfinemployer = localStorage.getItem("bofincardmanuallyemployer_"+c+"_"+getbodiname);
			var bodimfinoccupation = localStorage.getItem("bofincardmanuallyoccupation_"+c+"_"+getbodiname);
			var bodimwfindoi = localStorage.getItem("bofincardmanuallyworkpassdateissue_"+c+"_"+getbodiname);
			var bodimwfindoe = localStorage.getItem("bofincardmanuallyworkpassdateexpiry_"+c+"_"+getbodiname);
			var bodimfinnumber = localStorage.getItem("bofincardmanuallywfincardnumber_"+c+"_"+getbodiname);
	
			var bodifullname = localStorage.getItem("bo_fullName_"+c+"_"+getbodiname);
			var bodiemail = localStorage.getItem("bo_email_"+c+"_"+getbodiname);
			var bodicontactno = localStorage.getItem("bo_contact_no_"+c+"_"+getbodiname);

			//uploaded document fields
			var bodipassport = localStorage.getItem("bouploadpassport_"+c+"_"+getbodiname);
			var bodiaddressProof = localStorage.getItem("boaddress_"+c+"_"+getbodiname);
			var bodiresaddress = localStorage.getItem("boresaddress_"+c+"_"+getbodiname);
			var bodifinPassport = localStorage.getItem("bofinuploadPassport_"+c+"_"+getbodiname);
			var bodifinCardFront = localStorage.getItem("bofinfrontupload_"+c+"_"+getbodiname);
			var bodifinCardBack = localStorage.getItem("bofinbackupload_"+c+"_"+getbodiname);
			var bodifinAddressProof = localStorage.getItem("bofinaddress_"+c+"_"+getbodiname);
			var bodifinresaddress = localStorage.getItem("bofinresaddress_"+c+"_"+getbodiname);
			var bodinricfront = localStorage.getItem("bonricfront_"+c+"_"+getbodiname);
			var bodinricback = localStorage.getItem("bonricback_"+c+"_"+getbodiname);

			var bodipep = localStorage.getItem("bodipep_"+c+"_"+getbodiname);
		        var bodipepCountry = localStorage.getItem("bodipepCountry_"+c+"_"+getbodiname);
		        var bodipepRole = localStorage.getItem("bodipepRole_"+c+"_"+getbodiname);
		        var bodipepFrom = localStorage.getItem("bodipepFrom_"+c+"_"+getbodiname);
		        var bodipepTo = localStorage.getItem("bodipepTo_"+c+"_"+getbodiname);			

			params += '&bodisurname'+c+'='+bodisurname+'&bodigivenname'+c+'='+bodigivenname+'&bodipassportno'+c+'='+bodipassportno+'&bodinationality'+c+'='+bodinationality+'&bodicountry'+c+'='+bodicountry+'&bodigender'+c+'='+bodigender+'&bodidob'+c+'='+bodidob+'&bodidateexpiry'+c+'='+bodidateexpiry+'&bodimdatetimepicker'+c+'='+bodimdatetimepicker+'&bodimpob'+c+'='+bodimpob+'&bodimpassportno'+c+'='+bodimpassportno+'&bodimnationality'+c+'='+bodimnationality+'&bodimcountry'+c+'='+bodimcountry+'&bodimgender'+c+'='+bodimgender+'&bodimdob'+c+'='+bodimdob+'&bodimdateexpiry'+c+'='+bodimdateexpiry+'&bodipreviewnricnumber'+c+'='+bodipreviewnricnumber+'&bodipreviewnricgender'+c+'='+bodipreviewnricgender+'&bodipreviewnricdoi'+c+'='+bodipreviewnricdoi+'&bodipreviewnriccob'+c+'='+bodipreviewnriccob+'&bodipreviewnricpostcode'+c+'='+bodipreviewnricpostcode+'&bodipreviewnricstreetname'+c+'='+bodipreviewnricstreetname+'&bodipreviewnricfloor'+c+'='+bodipreviewnricfloor+'&bodipreviewnricunit'+c+'='+bodipreviewnricunit+'&bodimfinpassportno'+c+'='+bodimfinpassportno+'&bodimfinnationality'+c+'='+bodimfinnationality+'&bodimfincountry'+c+'='+bodimfincountry+'&bodimfingender'+c+'='+bodimfingender+'&bodimfindatetimepicker'+c+'='+bodimfindatetimepicker+'&bodimfindateexpiry'+c+'='+bodimfindateexpiry+'&bodimfindob'+c+'='+bodimfindob+'&bodimfinpob'+c+'='+bodimfinpob+'&bodimfinemployer'+c+'='+bodimfinemployer+'&bodimfinoccupation'+c+'='+bodimfinoccupation+'&bodimwfindoi'+c+'='+bodimwfindoi+'&bodimwfindoe'+c+'='+bodimwfindoe+'&bodimfinnumber'+c+'='+bodimfinnumber+'&bodipreviewfrontnricnumber'+c+'='+bodipreviewfrontnricnumber+'&bodipreviewfrontnricdoi'+c+'='+bodipreviewfrontnricdoi+'&bodipreviewfrontnriccob'+c+'='+bodipreviewfrontnriccob+'&bodipreviewfrontnricgender'+c+'='+bodipreviewfrontnricgender+'&bodipreviewbacknricpostcode'+c+'='+bodipreviewbacknricpostcode+'&bodipreviewbacknricstreetname'+c+'='+bodipreviewbacknricstreetname+'&bodipreviewbacknricfloor'+c+'='+bodipreviewbacknricfloor+'&bodipreviewbacknricunit'+c+'='+bodipreviewbacknricunit+'&bodifinsurname'+c+'='+bodifinsurname+'&bodifingivenname'+c+'='+bodifingivenname+'&bodifinpassportno'+c+'='+bodifinpassportno+'&bodifinnationality'+c+'='+bodifinnationality+'&bodifincountry'+c+'='+bodifincountry+'&bodifindateexpiry'+c+'='+bodifindateexpiry+'&bodifindob'+c+'='+bodifindob+'&bodifinfrontemployer'+c+'='+bodifinfrontemployer+'&bodifinfrontoccupation'+c+'='+bodifinfrontoccupation+'&bodiwfinfrontdoi'+c+'='+bodiwfinfrontdoi+'&bodiwfinfrontdoe'+c+'='+bodiwfinfrontdoe+'&bodifinfrontnumber'+c+'='+bodifinfrontnumber+'&bodiwfinbackdoi'+c+'='+bodiwfinbackdoi+'&bodiwfinbackdoe'+c+'='+bodiwfinbackdoe+'&bodifinbacknumber'+c+'='+bodifinbacknumber+'&bodifullname'+c+'='+bodifullname+'&bodiemail'+c+'='+bodiemail+'&bodicontactno'+c+'='+bodicontactno+'&bodipassport'+c+'='+bodipassport+'&bodiaddressProof'+c+'='+bodiaddressProof+'&bodiresaddress'+c+'='+bodiresaddress+'&bodifinPassport'+c+'='+bodifinPassport+'&bodifinCardFront'+c+'='+bodifinCardFront+'&bodifinCardBack'+c+'='+bodifinCardBack+'&bodifinAddressProof'+c+'='+bodifinAddressProof+'&bodifinresaddress'+c+'='+bodifinresaddress+'&bodinricfront'+c+'='+bodinricfront+'&bodinricback'+c+'='+bodinricback+'&bodirestatus'+c+'='+bodirestatus+'&bodipep'+c+'='+bodipep+'&bodipepCountry'+c+'='+bodipepCountry+'&bodipepRole'+c+'='+bodipepRole+'&bodipepFrom'+c+'='+bodipepFrom+'&bodipepTo'+c+'='+bodipepTo+'&getbodiname='+getbodiname;	
			//+'&bodifindatetimepicker'+c+'='+bodifindatetimepicker+'&bodifinpob'+c+'='+bodifinpob
		}	

		var otherresstatus = localStorage.getItem("otherresidencystatus");
		//other agent fields
		var othersurname = localStorage.getItem("othersurname");
		var othergivenname = localStorage.getItem("othergivenName");
		var otherpassport = localStorage.getItem("otherpassportnumber");
		var othernationality = localStorage.getItem("othernationality");
		var othercountry = localStorage.getItem("othercntry");
		var othergender = localStorage.getItem("othergender");
		var otherdob = localStorage.getItem("otherdob");
		var otherdateexpiry = localStorage.getItem("otherdateexpiry");

		var othermdatepick = localStorage.getItem("othermdatetimepicker");
		var othermpob = localStorage.getItem("othermpob");
		var othermpassport = localStorage.getItem("othermpassortno");
		var othermnationality = localStorage.getItem("othermnationality");
		var othermcountry = localStorage.getItem("othermcntry");
		var othermgender = localStorage.getItem("othermgender");
		var othermdob = localStorage.getItem("othermdob");
		var othermdateexpiry = localStorage.getItem("othermdateexpiry");

		//other nric manual
		var otherpreviewnricno = localStorage.getItem("otherpreviewnricnumber");
		var otherpreviewnricdoi = localStorage.getItem("otherpreviewnricdoi");
		var otherpreviewnriccob = localStorage.getItem("otherpreviewnriccob");
		var otherpreviewnricpostal = localStorage.getItem("otherpreviewnricpostcode");
		var otherpreviewnricstreet = localStorage.getItem("otherpreviewnricstreetname");
		var otherpreviewnricfloor = localStorage.getItem("otherpreviewnricfloor");
		var otherpreviewnricunit = localStorage.getItem("otherpreviewnricunit");
		var otherpreviewnricgender = localStorage.getItem("otherpreviewnricgender");

		//other nric preview front
		var otherpreviewfrontnricno = localStorage.getItem("otherpreviewfrontnricnumber");
		var otherpreviewfrontnricdoi = localStorage.getItem("otherpreviewfrontnricdoi");
		var otherpreviewfrontnriccob = localStorage.getItem("otherpreviewfrontnriccob");
		var otherpreviewfrontnricgender = localStorage.getItem("otherpreviewfrontnricgender");

		//other nric preview back
		var otherpreviewbacknricpostal = localStorage.getItem("otherpreviewbacknricpostcode");
		var otherpreviewbacknricstreet = localStorage.getItem("otherpreviewbacknricstreetname");
		var otherpreviewbacknricfloor = localStorage.getItem("otherpreviewbacknricfloor");
		var otherpreviewbacknricunit = localStorage.getItem("otherpreviewbacknricunit");
		
		//other fin preview
		var otherfinsurname = localStorage.getItem("otherfinsurname");
		var otherfingivenname = localStorage.getItem("otherfingivenname");
		var otherfinpassport = localStorage.getItem("otherfinpassportno");
		var otherfinnationality = localStorage.getItem("otherfinnationality");
		var otherfincountry = localStorage.getItem("otherfincountry");
		var otherfintimepick = localStorage.getItem("otherfindatetimepicker");
		var otherfindateexpiry = localStorage.getItem("otherfindateexpiry");
		var otherfindob = localStorage.getItem("otherfindob");
		var otherfinpob = localStorage.getItem("otherfinpob");
		var otherfingender = localStorage.getItem("otherfingender");

		var otherfinfrontemp = localStorage.getItem("otherfinfrontemployer");
		var otherfinfrontocc = localStorage.getItem("otherfinfrontoccupation");
		var otherfinfrontdoi = localStorage.getItem("otherwfinfrontdoi");
		var otherfinfrontdoe = localStorage.getItem("otherwfinfrontdoe");
		var otherfinfrontno = localStorage.getItem("otherfinfrontnumber");

		var otherfinbackdoi = localStorage.getItem("otherwfinbackdoi");
		var otherfinbackdoe = localStorage.getItem("otherwfinbackdoe");
		var otherfinbackno = localStorage.getItem("otherfinbacknumber");

		//other fin manual
		var othermfinpassportno = localStorage.getItem("othermfinpassportno");
		var othermfinnationality = localStorage.getItem("othermfinnationality");
		var othermfincountry = localStorage.getItem("othermfincountry");
		var othermfindatetimepicker = localStorage.getItem("othermfindatetimepicker");
		var othermfindateexpiry = localStorage.getItem("othermfindateexpiry");
		var othermfindob = localStorage.getItem("othermfindob");
		var othermfinpob = localStorage.getItem("othermfinpob");
		var othermfinemployer = localStorage.getItem("othermfinemployer");
		var othermfinoccupation = localStorage.getItem("othermfinoccupation");
		var othermwfindoi = localStorage.getItem("othermwfindoi");
		var othermwfindoe = localStorage.getItem("othermwfindoe");
		var othermfinnumber = localStorage.getItem("othermfinnumber");
		var othermfingender = localStorage.getItem("othermfingender");

		//uploaded document fields
		var otheruploadpassport = localStorage.getItem("otheruploadpassport");
		var otheraddress = localStorage.getItem("otheraddress");
		var otherresaddress = localStorage.getItem("otherresaddress");
		var otherfinuploadPassport = localStorage.getItem("otherfinuploadPassport");
		var otherfinfrontupload = localStorage.getItem("otherfinfrontupload");
		var otherfinbackupload = localStorage.getItem("otherfinbackupload");
		var otherfinaddress = localStorage.getItem("otherfinaddress");
		var otherfinresaddress = localStorage.getItem("otherfinresaddress");
		var othernricfrontup = localStorage.getItem("othernricfrontup");
		var othernricbackup = localStorage.getItem("othernricbackup");

		params += '&othersurname='+othersurname+'&othergivenname='+othergivenname+'&otherpassport='+otherpassport+'&othernationality='+othernationality+'&othercountry='+othercountry+'&othergender='+othergender+'&otherdob='+otherdob+'&otherdateexpiry='+otherdateexpiry+'&othermdatepick='+othermdatepick+'&othermpob='+othermpob+'&othermpassport='+othermpassport+'&othermnationality='+othermnationality+'&othermcountry='+othermcountry+'&othermgender='+othermgender+'&othermdob='+othermdob+'&othermdateexpiry='+othermdateexpiry+'&otherpreviewnricno='+otherpreviewnricno+'&otherpreviewnricdoi='+otherpreviewnricdoi+'&otherpreviewnriccob='+otherpreviewnriccob+'&otherpreviewnricpostal='+otherpreviewnricpostal+'&otherpreviewnricstreet='+otherpreviewnricstreet+'&otherpreviewnricfloor='+otherpreviewnricfloor+'&otherpreviewnricunit='+otherpreviewnricunit+'&otherpreviewnricgender='+otherpreviewnricgender+'&otherpreviewfrontnricno='+otherpreviewfrontnricno+'&otherpreviewfrontnricdoi='+otherpreviewfrontnricdoi+'&otherpreviewfrontnriccob='+otherpreviewfrontnriccob+'&otherpreviewfrontnricgender='+otherpreviewfrontnricgender+'&otherpreviewbacknricpostal='+otherpreviewbacknricpostal+'&otherpreviewbacknricstreet='+otherpreviewbacknricstreet+'&otherpreviewbacknricfloor='+otherpreviewbacknricfloor+'&otherpreviewbacknricunit='+otherpreviewbacknricunit+'&otherfinsurname='+otherfinsurname+'&otherfingivenname='+otherfingivenname+'&otherfinpassport='+otherfinpassport+'&otherfinnationality='+otherfinnationality+'&otherfincountry='+otherfincountry+'&otherfintimepick='+otherfintimepick+'&otherfindateexpiry='+otherfindateexpiry+'&otherfindob='+otherfindob+'&otherfinpob='+otherfinpob+'&otherfingender='+otherfingender+'&otherfinfrontemp='+otherfinfrontemp+'&otherfinfrontocc='+otherfinfrontocc+'&otherfinfrontdoi='+otherfinfrontdoi+'&otherfinfrontdoe='+otherfinfrontdoe+'&otherfinfrontno='+otherfinfrontno+'&otherfinbackdoi='+otherfinbackdoi+'&otherfinbackdoe='+otherfinbackdoe+'&otherfinbackno='+otherfinbackno+'&othermfinpassportno='+othermfinpassportno+'&othermfinnationality='+othermfinnationality+'&othermfincountry='+othermfincountry+'&othermfindatetimepicker='+othermfindatetimepicker+'&othermfindateexpiry='+othermfindateexpiry+'&othermfindob='+othermfindob+'&othermfinpob='+othermfinpob+'&othermfinemployer='+othermfinemployer+'&othermfinoccupation='+othermfinoccupation+'&othermwfindoi='+othermwfindoi+'&othermwfindoe='+othermwfindoe+'&othermfinnumber='+othermfinnumber+'&othermfingender='+othermfingender+'&otheruploadpassport='+otheruploadpassport+'&otheraddress='+otheraddress+'&otherresaddress='+otherresaddress+'&otherfinuploadPassport='+otherfinuploadPassport+'&otherfinfrontupload='+otherfinfrontupload+'&otherfinbackupload='+otherfinbackupload+'&otherfinaddress='+otherfinaddress+'&otherfinresaddress='+otherfinresaddress+'&othernricfrontup='+othernricfrontup+'&othernricbackup='+othernricbackup+'&otherresstatus='+otherresstatus;		

		for (var i = 0; i < localStorage.length; i++){
    			console.log(localStorage.key(i)+" ==> "+localStorage.getItem(localStorage.key(i)));
		}

		var jsonStr = "{";
		var jsonVal = '';
		for (var i = 0; i < localStorage.length; i++){
			jsonVal = localStorage.getItem(localStorage.key(i));
			if(jsonVal == 'undefined'){
				jsonVal = '';
			}
			jsonStr += '"' + localStorage.key(i) + '" : "' + jsonVal + '"' +", ";
		}
		jsonStr = jsonStr.substring(0, jsonStr.length-1);
		jsonStr += "}"

		//alert(jsonStr);

		params += '&jsonFormat='+jsonStr;

		console.log("params: "+params);
			
		$.ajax({
			url: "pdftest.php",
			data: params,
			type: "POST",
			success:function(data){
				//console.log(data);
			},
			error:function (){}
		});
		

	</script>

</body>
</html>
